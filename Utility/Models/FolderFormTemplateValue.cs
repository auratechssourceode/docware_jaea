//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Utility.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class FolderFormTemplateValue
    {
        public long Id { get; set; }
        public long AssignFormTemplateId { get; set; }
        public long AssignedFormControlId { get; set; }
        public string Value { get; set; }
        public long UserId { get; set; }
        public System.DateTime FilledDate { get; set; }
        public Nullable<int> FilledDateInt { get; set; }
    }
}
