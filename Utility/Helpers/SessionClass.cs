﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility.Helpers
{
    public class SessionClass
    {
        public static string UserName = "UserName";
        public static string UserID = "UserID";
        public static string UserRoleID = "UserRoleID";
        public static string RoleName = "RoleName";
        public static string Email = "Email";
        public static string ProfilePic = "ProfilePic";
        public static string Menu = "Menu";
        public static string FullName = "FullName";
        public static string CabinetID = "CabinetID";
        public static string CabinetName = "CabinetName";
        public static string ApplicationID = "ApplicationID";
        public static string RolePermissionList = "RolePermissionList";
        public static string NoPermission = "NoPermission";
        public static string HomeScreen = "HomeScreen";
        public static string LinkPermissionList = "LinkPermissionList";
        public static string ApplicationName = "ApplicationName";
        public static string DownloadItems = "DownloadItems";
    }
}
