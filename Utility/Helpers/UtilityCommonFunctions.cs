﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
//using System.Web.Mvc;
using System.Data;
using System.Web.Configuration;
using Utility.Models;
using System.Web;
using static Utility.Helpers.EnumList;
using System.Web.Mvc;

namespace Utility.Helpers
{
    public class UtilityCommonFunctions
    {
        #region --> Encryption | Add | Jaydeep shah | 06-02-2018
        public static string Encrypt(string StringToEncode)
        {
            try
            {
                byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(StringToEncode);
                string str = Convert.ToBase64String(data);
                str = str + "@";
                return str;
            }
            catch (Exception e)
            {
                throw;
            }
        }
        #endregion

        #region --> Decryption | Add | Jaydeep shah | 06-02-2018
        public static string Decrypt(string stringToDecode)
        {
            string str = string.Empty;
            try
            {
                stringToDecode = stringToDecode.Replace("@", "");
                byte[] data = Convert.FromBase64String(stringToDecode);
                str = System.Text.ASCIIEncoding.ASCII.GetString(data);
                return str;
            }
            catch (Exception e)
            {
                return null;
            }
        }
        #endregion

        #region Remove Special Characters | Add | Jaydeep shah | 06-02-2018
        /// <summary>
        /// Function to remove special characters from the user document file before uploading
        /// </summary>
        public static string RemoveSpecialCharacters(string str)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '.' || c == '_' || c == ' ')
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }
        #endregion

        #region Get enum description | Add | Jaydeep shah | 06-02-2018
        public static string GetEnumDescription(Enum enumConstant)
        {
            FieldInfo fi = enumConstant.GetType().GetField(enumConstant.ToString());
            DescriptionAttribute[] attr = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (attr.Length > 0)
            {
                return attr[0].Description;
            }
            else
            {
                return enumConstant.ToString();
            }
        }
        #endregion

        #region Get enum name from description | Add | Jaydeep shah | 06-02-2018
        public static T GetValueFromDescription<T>(string description)
        {
            var type = typeof(T);
            if (!type.IsEnum) throw new InvalidOperationException();
            foreach (var field in type.GetFields())
            {
                var attribute = Attribute.GetCustomAttribute(field,
                    typeof(DescriptionAttribute)) as DescriptionAttribute;
                if (attribute != null)
                {
                    if (attribute.Description == description)
                        return (T)field.GetValue(null);
                }
                else
                {
                    if (field.Name == description)
                        return (T)field.GetValue(null);
                }
            }
            throw new ArgumentException("Not found.", "description");
            // or return default(T);
        }
        #endregion

        #region --> Random String generator
        public class RandomStringGenerator
        {
            public const string ALPHANUMERIC_CAPS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            public const string ALPHA_CAPS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            public const string NUMERIC = "1234567890";

            Random rand = new Random();
            public string GetRandomString(int length, params char[] chars)
            {
                string s = "";
                for (int i = 0; i < length; i++)
                    s += chars[rand.Next() % chars.Length];

                return s;
            }
        }

        #endregion

        #region Send Email via Templete | Add | jaydeep shah | 08022018
        public static bool SendEmailTemplete(int templeteID, Dictionary<string, string> replacements, string toEmail, string fromEmail, int mailType = 0)
        {
            bool result = false;
            string sub = string.Empty;
            string body = string.Empty;
            if (string.IsNullOrEmpty(fromEmail))
            {
                fromEmail = WebConfigurationManager.AppSettings["SendEmailInfo"].ToString();
            }
            try
            {
                using (DBEntities db = new DBEntities())
                {
                    var templete = db.EmailTemplates.Where(s => s.Id == templeteID).FirstOrDefault();

                    if (templete.IsActive)
                    {
                        if (templete != null)
                        {
                            sub = templete.Subject;
                            body = templete.Body;
                            replacements.ToList().ForEach(x =>
                            {
                                body = body.Replace(x.Key, Convert.ToString(x.Value));
                            });
                        }

                        SendMail _SendMail = new SendMail();
                        bool IsSent = false;
                        _SendMail.Subject = sub;
                        _SendMail.To = toEmail;
                        _SendMail.From = fromEmail;
                        _SendMail.Body = body;
                        _SendMail.IsHtmlBody = true;
                        IsSent = _SendMail.Send();

                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }
            }
            catch (Exception e)
            {
                return result;
            }

            return result;
        }
        //Send Email Templete | Add | jaydeep shah | 08022018
        #endregion

        #region --> SaveExceptionLog | Sweta Patel |2018-03-06 | Copy from VGL      
        public static bool SaveExceptionLog(ExceptionLog data)
        {
            try
            {
                using (DBEntities db = new DBEntities())
                {
                    string ip = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                    if (string.IsNullOrWhiteSpace(ip))
                    {
                        data.IPAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                    }
                    else
                    {
                        data.IPAddress = "IP not Found";
                    }
                    data.LogDate = DateTime.Now;
                    data.IsMobile = HttpContext.Current.Request.Browser.IsMobileDevice;
                    data.Browser = HttpContext.Current.Request.Browser.Browser;
                    data.Version = HttpContext.Current.Request.Browser.Version;
                    db.ExceptionLogs.Add(data);
                    db.SaveChanges();

                    return true;
                }
            }
            catch
            {
                return false;

            }
        }
        #endregion

        #region SaveAuditTrailsLogs | Add | Sweta Patel | Copy From Loews
        public static bool SaveAuditTrailsLogs(AuditrialLog data)
        {
            try
            {
                using (DBEntities db = new DBEntities())
                {
                    string ip = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                    if (string.IsNullOrWhiteSpace(ip))
                    {
                        data.IPAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                    }
                    else
                    {
                        data.IPAddress = "IP not Found";
                    }

                    data.AuditDate = DateTime.Now;
                    data.IsMobile = HttpContext.Current.Request.Browser.IsMobileDevice;
                    data.Browser = HttpContext.Current.Request.Browser.Browser;
                    data.Version = HttpContext.Current.Request.Browser.Version;
                    db.AuditrialLogs.Add(data);
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion
    }
}
