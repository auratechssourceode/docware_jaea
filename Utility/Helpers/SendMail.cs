﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Utility.Models;
using static Utility.Helpers.EnumList;

namespace Utility.Helpers
{
    public class SendMail
    {
        DBEntities db = new DBEntities();
        #region Variables
        MailMessage MailObj;
        #endregion

        #region Properties

        /// <summary>
        /// Mail Address to whome the mail has to send
        /// </summary>
        public String To
        {
            set
            {
                MailObj.To.Clear();
                if (value.ToString().Length > 0)
                {
                    string[] ToAddresses = value.ToString().Split(',');
                    for (int i = 0; i < ToAddresses.Length; i++)
                    {
                        MailObj.To.Add(new MailAddress(ToAddresses[i].ToString()));
                    }
                }
                //MailObj.To.Add(new MailAddress("vijay@webmyne.com"));
                //MailObj.To.Add(new MailAddress("vijaysutaria@gmail.com"));
            }
        }

        /// <summary>
        /// Mail Address to whome the mail has to send
        /// </summary>
        public String CC
        {
            set
            {
                MailObj.CC.Clear();
                if (value.ToString().Length > 0)
                {
                    string[] ToAddresses = value.ToString().Split(',');
                    for (int i = 0; i < ToAddresses.Length; i++)
                    {
                        MailObj.CC.Add(new MailAddress(ToAddresses[i].ToString()));
                    }
                }
            }
        }

        /// <summary>
        /// Mail Address to whome the mail has to send
        /// </summary>
        public String BCC
        {
            set
            {
                MailObj.Bcc.Clear();
                if (value.ToString().Length > 0)
                {
                    string[] ToAddresses = value.ToString().Split(',');
                    for (int i = 0; i < ToAddresses.Length; i++)
                    {
                        MailObj.Bcc.Add(new MailAddress(ToAddresses[i].ToString()));
                    }
                }
            }
        }

        /// <summary>
        /// Mail Address by whome the mail has send
        /// </summary>
        public String From
        {
            get { return MailObj.From.ToString(); }
            set
            {
                MailObj.From = new MailAddress(value.ToString());
            }
        }

        /// <summary>
        /// Mail Body
        /// </summary>
        public String Body
        {
            get { return MailObj.Body; }
            set { MailObj.Body = value; }
        }

        /// <summary>
        /// This will Help You to Send a Whole Webpage as a Mail
        /// Set the URL of the Page to send it
        /// </summary>
        public String WebPageAsBody
        {
            set
            {
                MailObj.Body = ScreenScrapeHtml(value);
                MailObj.IsBodyHtml = true;
            }
        }

        /// <summary>
        /// Mail Subject
        /// </summary>
        public String Subject
        {
            get { return MailObj.Subject; }
            set { MailObj.Subject = value; }
        }

        /// <summary>
        /// Set True if the Mail Body is HTML
        /// </summary>
        public bool IsHtmlBody
        {
            set
            {
                MailObj.IsBodyHtml = value;
            }

        }

        #endregion

        public AttachmentCollection Attachments
        {
            get { return MailObj.Attachments; }
        }

        #region Constructor

        public SendMail()
        {
            MailObj = new MailMessage();
            MailObj.BodyEncoding = System.Text.Encoding.GetEncoding("utf-8");
        }

        #endregion

        #region Method
        /// <summary>
        /// Used to send mail
        /// </summary>
        /// <returns>returns true if mail sent successfully</returns>

        public Boolean Send()
        {
            try
            {

                string username = ConfigurationManager.AppSettings["SendEmailInfo"].ToString();
                string password = ConfigurationManager.AppSettings["EmailPassword"].ToString();

                #region SMTP Detail From Table | Kiran | 05032020
                
                string smtpserver = string.Empty;
                string smtpusessl = string.Empty;
                string smtpserverport = string.Empty;
                string sendusername = string.Empty;
                string sendpassword = string.Empty;
                var Emaildetail = db.SettingsMasterDetails.Where(x => x.SettingsId == (int)EnumSettingsId.Email).ToList();
                foreach (var item in Emaildetail)
                {
                    if (item.FieldName == "smtpserver")
                    {
                        smtpserver = item.FieldValue;
                    }
                    if (item.FieldName == "IsSSL")
                    {
                        smtpusessl = item.FieldValue;
                    }
                    if (item.FieldName == "smtpserverport")
                    {
                        smtpserverport = item.FieldValue;
                    }
                    if (item.FieldName == "sendusername")
                    {
                        sendusername = item.FieldValue;
                    }
                    if (item.FieldName == "sendpassword")
                    {
                        sendpassword = item.FieldValue;
                    }
                }
                #endregion

                #region MILIND || 02-03-2017 || SEND SMS

                bool fSSL = true;

                System.Web.Mail.MailMessage message = new System.Web.Mail.MailMessage();

                //message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserver", "smtp.gmail.com");
                //message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", "2");
                //message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserverport", "465");
                //if (fSSL)
                //    message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpusessl", "true");
                //message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");

                //message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", username);
                //message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", password);

                message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserver", smtpserver);
                message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", "2");
                message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserverport", smtpserverport);
                if (fSSL)
                    message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpusessl", smtpusessl);
                message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");

                message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", sendusername);
                message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", sendpassword);

                message.From = MailObj.From.ToString();
                message.To = MailObj.To.ToString();
                message.Subject = MailObj.Subject.ToString();
                message.Cc = MailObj.CC.ToString();
                message.BodyFormat = System.Web.Mail.MailFormat.Html;
                message.Bcc = MailObj.Bcc.ToString();
                //message.Bcc = "dhrumil2471991@gmail.com";
                message.Body = MailObj.Body.ToString();
                //System.Web.Mail.SmtpMail.SmtpServer = "smtp.gmail.com";
                System.Web.Mail.SmtpMail.Send(message);
                return true;

                #endregion
            }
            catch (Exception ex)
            {
                if (ex.HelpLink == null) { ex.HelpLink = "SendMail101>> " + ex.Message; }
                else { ex.HelpLink = "SendMail101>> " + ex.HelpLink; }
                throw ex;
            }
        }

        private static string ScreenScrapeHtml(string url)
        {
            System.Net.WebRequest objRequest = System.Net.HttpWebRequest.Create(url);
            System.IO.StreamReader sr = new System.IO.StreamReader(objRequest.GetResponse().GetResponseStream());
            string result = sr.ReadToEnd();
            sr.Close();
            return result;
        }

        #endregion
    }
}
