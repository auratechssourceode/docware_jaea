﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility.Helpers
{
    public class EnumList
    {
        public enum Permission
        {
            CreateFolder = 1,
            CreateDocument = 2,
            ShareDocument = 3,
            CreateTemplate = 4,
            WorkFlowCreation = 5
        }

        public enum EmailTemplate
        {
            ForgotPassword = 2,
            ConfirmForgotPassword = 7,
            SendDocumentLink = 10,
            WorkFlowRequest = 11
        }

        public enum Language
        {
            English = 1,
            Arabic = 2
        }

        public enum Priority
        {
            Low = 1,
            Medium = 2,
            High = 3
        }

        //----------------------------------------------------------------------------------------------------------
        // New Enumlist as per new structure define by 2018-02-01 as below
        //----------------------------------------------------------------------------------------------------------

        /// <summary>
        /// When user login into system it can be in admin panel or user panel at a single time with single cabinet
        /// </summary>
        public enum Application  // in which panel user wants to login
        {
            SuperAdmin = 1,  // for admin panel have single user as super admin
            Admin = 2, // for admin panel can have multiple
            User = 3 // for user panel can have multiple
        }

        public enum EnumLogType
        {
            Login = 1,
            Logout = 2,
            ForgotPassword = 3,
            ResetPassword = 4,
            SaveData = 5,
            UpdateData = 6,
            DeleteData = 7,
            ViewData = 8,
            CloneData = 9
        }

        public enum EnumMasterType
        {
            Cabinet = 1,
            Role = 2,
            Department = 3,
            Section = 4,
            Position = 5,
            Category = 6,
            SubCategory = 7
        }

        public enum EnumPermissionType
        {
            Menu = 1,
            Add = 2,
            Edit = 3,
            Status = 4,
            DocShare = 5
        }

        public enum EnumNotificationType
        {
            Other = 1,
            Commnet = 2,
            Share = 3,
            Version = 4,
            Group = 5,
            ApprovalReq = 6,
            Workflow = 7
        }

        public enum EnumActionType
        {
            Initiate = 0,
            Close = 1,
            Save = 2,
            SaveAndSend = 3,
            Approve = 4,
            RequestModification = 5,
            Decline = 6
        }
        public enum EnumInstanceType
        {
            FromDocware = 1,
            OtherSource = 2
        }
        public enum EnumSettingsId
        {
            Email = 1
           
        }
        
    }
}
