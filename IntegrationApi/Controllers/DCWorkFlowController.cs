﻿using IntegrationApi.Helper;
using IntegrationApi.Helpers;
using IntegrationApi.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Utility.Helpers;
using Utility.Models;

namespace IntegrationApi.Controllers
{
    public class DCWorkFlowController : ApiController
    {
        Utility.Helpers.DAL objDAL = new Utility.Helpers.DAL();
        /// <summary>
        /// GetFormKeyValueList | Add | Kiran Sawant | 22022021
        /// </summary>
        /// <returns></returns>        
        [HttpGet]
        public ResponseDataDC2<UserWorkflowFormAPIVM> GetFormKeyValueList(long WFInstanceId = 0)
        {
            ResponseDataDC2<UserWorkflowFormAPIVM> response = new ResponseDataDC2<UserWorkflowFormAPIVM>();
            List<UserWorkflowFormAPIVM> record = new List<UserWorkflowFormAPIVM>();
            SortedList slProject = new SortedList();
            DataSet ds = new DataSet();

            try
            {
                using (DBEntities db = new DBEntities())
                {
                    if (WFInstanceId > 0)
                    {
                        var WFdata = db.WF_Instance.Where(x => x.Id == WFInstanceId).FirstOrDefault();
                        if (WFdata != null)
                        { 
                            var wfmaster = db.WF_Master.Where(x => x.Id == WFdata.WFId).FirstOrDefault();
                            if (wfmaster.AssignFormTemplateId > 0)
                            {
                                var dynamicrespose = db.AssignedFormControlProperties.Where(x => x.AssignedFormTemplateId == wfmaster.AssignFormTemplateId).ToList();
                                if (dynamicrespose.Count > 0)
                                {
                                    foreach (var item in dynamicrespose)
                                    {
                                        
                                        UserWorkflowFormAPIVM keyval = new UserWorkflowFormAPIVM();
                                        keyval.FiledId = item.FieldId;
                                        var dynamicresposevalue = db.DynamicFormResponses.Where(x => x.FormTemplateId == wfmaster.AssignFormTemplateId && x.AttachedId == WFdata.Id && x.FiledId == item.FieldId).FirstOrDefault();
                                        keyval.Value = "";
                                        if (dynamicresposevalue != null)
                                        {
                                            keyval.Value = dynamicresposevalue.Value;
                                        }
                                        record.Add(keyval);
                                    }
                                    response.ResponseData = record;
                                    response.ResponseCode = (int)StatusEnum.Success;
                                    response.ResponseMsg = StatusEnum.Success.ToString();
                                }
                                else
                                {
                                    response.ResponseCode = (int)StatusEnum.Fail;
                                    response.ResponseMsg = "Data not found";
                                }
                                //var dynamicrespose = db.DynamicFormResponses.Where(x => x.FormTemplateId == wfmaster.AssignFormTemplateId && x.AttachedId == WFdata.Id).ToList();
                                //if (dynamicrespose.Count > 0)
                                //{
                                //    foreach (var item in dynamicrespose)
                                //    {
                                //        UserWorkflowFormAPIVM keyval = new UserWorkflowFormAPIVM();
                                //        keyval.FiledId = item.FiledId;
                                //        keyval.Value = item.Value;
                                //        record.Add(keyval);
                                //    }
                                //    response.ResponseData = record;
                                //    response.ResponseCode = (int)StatusEnum.Success;
                                //    response.ResponseMsg = StatusEnum.Success.ToString();
                                //}
                                //else
                                //{
                                //    response.ResponseCode = (int)StatusEnum.Fail;
                                //    response.ResponseMsg = "Data not found";
                                //}
                            }
                            else
                            {
                                response.ResponseCode = (int)StatusEnum.Fail;
                                response.ResponseMsg = "Data not found";
                            }

                        }
                        else
                        {
                            response.ResponseCode = (int)StatusEnum.Fail;
                            response.ResponseMsg = "Data not found";
                        }
                    }
                    else
                    {
                        response.ResponseCode = (int)StatusEnum.Fail;
                        response.ResponseMsg = "WFInstanceid id is required";
                    }
                }

            }
            catch (Exception ex)
            {
                // Get stack trace for the exception with source file information
                var st = new StackTrace(ex, true);
                // Get the top stack frame
                var frame = st.GetFrame(0);
                // Get the line number from the stack frame
                var line = frame.GetFileLineNumber();
                response.ResponseCode = (int)StatusEnum.Error;
                response.ResponseMsg = line + ":" + ex.Message;
            }

            return response;
        }

        [HttpGet]
        public ResponseDataDC2<UserWorkflowDetailVM> GetWorkflowDetailByWFInstanceId(long WFInstanceId = 0)
        {
            ResponseDataDC2<UserWorkflowDetailVM> response = new ResponseDataDC2<UserWorkflowDetailVM>();
            List<UserWorkflowDetailVM> record = new List<UserWorkflowDetailVM>();
            SortedList slProject = new SortedList(); 
            DataSet ds = new DataSet();

            try
            {
                using (DBEntities db = new DBEntities())
                {
                    if (WFInstanceId > 0)
                    {
                        var WFdata = db.WF_Instance.Where(x => x.Id == WFInstanceId).FirstOrDefault();
                        if (WFdata != null)
                        {
                            var wfmaster = db.WF_Master.Where(x => x.Id == WFdata.WFId).FirstOrDefault();                            
                            if (wfmaster != null)
                            {
                                slProject.Add("@WFInstanceId", WFInstanceId);                                
                                ds = objDAL.GetDataSet("SP_Get_WorkFlowStastusDetailBy_InstanceId", slProject);
                                if (ds != null && ds.Tables.Count > 0)
                                {
                                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                                    {
                                        record = objDAL.ConvertToList<UserWorkflowDetailVM>(ds.Tables[0]);
                                        response.ResponseData.AddRange(record);
                                        response.ResponseCode = (int)StatusEnum.Success;
                                        response.ResponseMsg = StatusEnum.Success.ToString();
                                    }
                                    else
                                    {
                                        response.ResponseCode = (int)StatusEnum.Invalid;
                                        response.ResponseMsg = "Data is Empty";
                                    }
                                }
                                else
                                {
                                    response.ResponseCode = (int)StatusEnum.Invalid;
                                    response.ResponseMsg = "Data is Empty";

                                }
                            }
                            else
                            {
                                response.ResponseCode = (int)StatusEnum.Invalid;
                                response.ResponseMsg = "Data not found";
                            }

                        }
                        else
                        {
                            response.ResponseCode = (int)StatusEnum.Invalid;
                            response.ResponseMsg = "Data not found";
                        }
                    }
                    else
                    {
                        response.ResponseCode = (int)StatusEnum.Fail;
                        response.ResponseMsg = "WFInstanceid id is required";
                    }
                }

            }
            catch (Exception ex)
            {
                // Get stack trace for the exception with source file information
                var st = new StackTrace(ex, true);
                // Get the top stack frame
                var frame = st.GetFrame(0);
                // Get the line number from the stack frame
                var line = frame.GetFileLineNumber();
                response.ResponseCode = (int)StatusEnum.Error;
                response.ResponseMsg = line + ":" + ex.Message;
            }

            return response;
        }

        //[HttpPost]
        //public async Task<HttpResponseMessage> InitiateWorkFlow()
        //{
        //    ResponseDC responsemesg = new ResponseDC();
        //    UserWorkflowAPIVM detail = new UserWorkflowAPIVM();
        //    try
        //    {


        //        using (DBEntities db = new DBEntities())
        //        {
        //            // Check if the request contains multipart/form-data.
        //            if (!Request.Content.IsMimeMultipartContent())
        //            {
        //                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
        //            }

        //            var provider = await Request.Content.ReadAsMultipartAsync<Helper.InMemoryMultipartFormDataStreamProvider>(new Helper.InMemoryMultipartFormDataStreamProvider());
        //            //access form data
        //            NameValueCollection formData = provider.FormData;
        //            //access files
        //            List<HttpContent> files = provider.Files;
        //            var data = formData.GetValues("Data")[0];
        //            if (data != null)
        //            {
        //                //detail = data;
        //                detail = Newtonsoft.Json.JsonConvert.DeserializeObject<UserWorkflowAPIVM>(data);
        //                if (detail != null)
        //                {
        //                    long instacneid = 0;
        //                    long folderid = 0;
        //                    try
        //                    {

        //                        if (detail.TokenId == "fi9VcGxvYWRzL0RvY3VtZW50LzE0LzEvRE9DMTAwMzhfMi5qcGc")
        //                        {
        //                            var wfMasterdetail = db.WF_Master.Where(x => x.Id == detail.WFId && x.IsActive && !x.IsDelete).FirstOrDefault();
        //                            if (wfMasterdetail != null)
        //                            {
        //                                if (!string.IsNullOrEmpty(detail.WFInitiateName))
        //                                {
        //                                    if (detail.Priority > 0)
        //                                    {
        //                                        if (detail.UserId > 0)
        //                                        {
        //                                            #region Iniate Work Flow

        //                                            var SecId = db.WF_Instance.Where(x => x.WFInitiateName.Contains(detail.WFInitiateName.Trim()) && x.WFId == detail.WFId).Select(x => x.Id).FirstOrDefault();
        //                                            if (SecId > 0)
        //                                            {
        //                                                responsemesg.message = "WFInitiateName is exist";
        //                                            }
        //                                            else
        //                                            {

        //                                                var wfDatail = db.WF_Details.Where(x => x.WFId == detail.WFId && x.WFStep == 1).FirstOrDefault();

        //                                                ////Flow Automated Insert data(table are WF_Instance, WF_Transactions)
        //                                                WF_Instance instance = new WF_Instance();
        //                                                instance.WFId = detail.WFId;
        //                                                instance.WFInitiateName = detail.WFInitiateName;
        //                                                instance.CurrentStep = 1;
        //                                                instance.InitiatorUser = detail.UserId;
        //                                                instance.IsOpen = true;
        //                                                instance.Priority = detail.Priority;
        //                                                instance.StartDateTime = System.DateTime.Now;
        //                                                instance.IsStart = true;
        //                                                instance.IsOutSource = (Int16)EnumList.EnumInstanceType.OtherSource;
        //                                                db.WF_Instance.Add(instance);
        //                                                db.SaveChanges();

        //                                                WF_Transactions transactions = new WF_Transactions();
        //                                                transactions.WFInstanceId = instance.Id;
        //                                                transactions.WFStep = 1;
        //                                                transactions.FromUser = detail.UserId;
        //                                                transactions.ToUser = wfDatail.AssignedUserID;
        //                                                transactions.SubsidiaryId = 0;
        //                                                transactions.ActionId = 3;
        //                                                transactions.IsCurrentStep = true;
        //                                                transactions.StartDateTime = System.DateTime.Now;
        //                                                db.WF_Transactions.Add(transactions);
        //                                                db.SaveChanges();
        //                                                instacneid = instance.Id;

        //                                                ////When the intiate that time need to add folder id based on intiate
        //                                                if (instacneid > 0)
        //                                                {
        //                                                    WF_FolderDetail folderDetail = new WF_FolderDetail();
        //                                                    string UDID = CommonFunctions.GetUDIDForFolderWorkFlow();

        //                                                    folderDetail.UDID = UDID;
        //                                                    folderDetail.WFInstanceId = instacneid;
        //                                                    folderDetail.ParentFolderId = 0;
        //                                                    folderDetail.AssignFormTemplateId = 0;
        //                                                    folderDetail.CabinetId = wfMasterdetail.CabinetId;
        //                                                    folderDetail.Name = detail.WFInitiateName.Trim();
        //                                                    folderDetail.Notes = "";
        //                                                    folderDetail.Keywords = "";
        //                                                    folderDetail.Description = "";
        //                                                    folderDetail.PhysicalLocation = "";
        //                                                    folderDetail.IsPublic = false;
        //                                                    folderDetail.IsActive = true;
        //                                                    folderDetail.IsDelete = false;
        //                                                    folderDetail.CreatedBy = detail.UserId.ToString();
        //                                                    folderDetail.CreatedDate = DateTime.Now;
        //                                                    db.WF_FolderDetail.Add(folderDetail);
        //                                                    db.SaveChanges();
        //                                                    folderid = folderDetail.WFFolderDetailId;
        //                                                }

        //                                                #region Insert dyanmic form value

        //                                                UtilityCommonFunctions.RandomStringGenerator random = new UtilityCommonFunctions.RandomStringGenerator();
        //                                                string newResponseCode = random.GetRandomString(5, UtilityCommonFunctions.RandomStringGenerator.ALPHANUMERIC_CAPS.ToCharArray());

        //                                                if (wfMasterdetail.AssignFormTemplateId > 0)
        //                                                {
        //                                                    if (detail.FormValues.Count > 0)
        //                                                    {
        //                                                        List<DynamicFormResponse> responselst = new List<DynamicFormResponse>();
        //                                                        var resp = db.DynamicFormResponses.Where(x => x.FormTemplateId == wfMasterdetail.AssignFormTemplateId
        //                                                                   && x.AttachedType == "WorkFlow" && x.AttachedId == instacneid
        //                                                                   ).ToList();

        //                                                        db.DynamicFormResponses.RemoveRange(resp);

        //                                                        foreach (var res in detail.FormValues)
        //                                                        {
        //                                                            var data1 = db.AssignedFormControlProperties.Where(x => x.AssignedFormTemplateId == wfMasterdetail.AssignFormTemplateId && x.FieldId == res.FiledId).FirstOrDefault();
        //                                                            if (data1 != null)
        //                                                            {
        //                                                                DynamicFormResponse response = new DynamicFormResponse();
        //                                                                response.FormTemplateId = wfMasterdetail.AssignFormTemplateId;
        //                                                                response.ResponseCode = newResponseCode;
        //                                                                response.AttachedType = "Workflow"; // should be  Folder/File/Workflow 
        //                                                                response.AttachedId = instacneid; //  should be workflow attached ID                   
        //                                                                response.FiledId = res.FiledId;
        //                                                                response.Type = data1.FieldType;
        //                                                                response.Value = res.Value;
        //                                                                response.IsSelected = (res.Value == null || res.Value == "" ? false : true);// res.IsSelected;
        //                                                                response.CreatedBy = detail.UserId.ToString();
        //                                                                response.CreatedDate = DateTime.Now;
        //                                                                responselst.Add(response);
        //                                                            }
        //                                                        }


        //                                                        db.DynamicFormResponses.AddRange(responselst);
        //                                                        db.SaveChanges();
        //                                                    }

        //                                                }
        //                                                #endregion

        //                                                #region File Upload

        //                                                if (files.Count > 0)
        //                                                {

        //                                                    string fName = "";
        //                                                    string nameFile = "";
        //                                                    var UDID1 = CommonFunctions.GetUDIDForDocumentWorkFlow();
        //                                                    var Name = UDID1 + "_1";
        //                                                    decimal docSize = 0;
        //                                                    string path = "";
        //                                                    try
        //                                                    {
        //                                                        foreach (var fileName in files)
        //                                                        {
        //                                                            //HttpPostedFileBase file = Request.Files[fileName];                                                               
        //                                                            HttpContent file = fileName;
        //                                                            Stream currentinput = await file.ReadAsStreamAsync();
        //                                                            Stream input = await file.ReadAsStreamAsync();
        //                                                            UtilityCommonFunctions.RandomStringGenerator random1 = new UtilityCommonFunctions.RandomStringGenerator();
        //                                                            string rdmText = random1.GetRandomString(4, UtilityCommonFunctions.RandomStringGenerator.ALPHANUMERIC_CAPS.ToCharArray());
        //                                                            fName = file.Headers.ContentDisposition.FileName.Trim('\"');
        //                                                            var result = fName.Substring(fName.LastIndexOf('.') + 1);
        //                                                            nameFile = Path.GetFileNameWithoutExtension(fName) + "_" + rdmText;

        //                                                            var instancedetail = db.WF_Instance.Find(instacneid);

        //                                                            //var uriPath = ConfigurationManager.AppSettings["PathForDocUpload"] + "/WorkFlow/" + instancedetail.InitiatorUser.ToString() + "/" + cabinetId.ToString() + "/" + instancedetail.Id + "/";
        //                                                            var savePath = ConfigurationManager.AppSettings["PathForSave"] + "WorkFlow/" + instancedetail.WFId.ToString() + "/" + instancedetail.Id + "/" + detail.UserId.ToString() + "/";
        //                                                            var uriPath = ConfigurationManager.AppSettings["PathForDocUpload"] + "/" + instancedetail.WFId.ToString() + "/" + instancedetail.Id + "/" + detail.UserId.ToString() + "/";
        //                                                            //var uriPath = ConfigurationManager.AppSettings["PathForDocUpload"] + "/10007/10034/14/";
        //                                                            string folderPath = uriPath;

        //                                                            if (!Directory.Exists(folderPath))
        //                                                            {
        //                                                                Directory.CreateDirectory(folderPath.ToString());
        //                                                            }
        //                                                            try
        //                                                            {
        //                                                                path = folderPath + Name + "." + result;
        //                                                                using (Stream file1 = File.Create(path))
        //                                                                {
        //                                                                    await currentinput.CopyToAsync(file1);
        //                                                                    currentinput.Close();



        //                                                                    //docSize = Math.Round((Convert.ToDecimal(file.ContentLength) / Convert.ToDecimal(1024)), 2);
        //                                                                    var parentFolder = db.FolderDetails.Find(folderid);

        //                                                                    var documentDetail = new WF_DocumentDetail();
        //                                                                    documentDetail.UDID = UDID1;
        //                                                                    documentDetail.WF_FolderId = folderid;
        //                                                                    documentDetail.Name = nameFile;
        //                                                                    documentDetail.CategoryId = 1;
        //                                                                    documentDetail.SubCategoryId = 1;
        //                                                                    documentDetail.AssignFormTemplateId = 0;
        //                                                                    documentDetail.IsActive = true;
        //                                                                    documentDetail.IsDelete = false;
        //                                                                    documentDetail.IsCheckOut = false;
        //                                                                    documentDetail.CreatedBy = detail.UserId.ToString();
        //                                                                    documentDetail.CreatedDate = DateTime.Now;
        //                                                                    documentDetail.IsCheckOut = false;
        //                                                                    documentDetail.WFStep = 0;
        //                                                                    db.WF_DocumentDetail.Add(documentDetail);
        //                                                                    db.SaveChanges();

        //                                                                    var documentVersion = new WF_DocumentVersionDetail();
        //                                                                    documentVersion.Notes = "default";
        //                                                                    documentVersion.Keywords = "default";
        //                                                                    documentVersion.Language = 1;
        //                                                                    documentVersion.Confidentiality = 0;
        //                                                                    documentVersion.WFDocumentId = documentDetail.WFDocumentId;
        //                                                                    documentVersion.DocumentName = Name;
        //                                                                    documentVersion.DocumentType = result;
        //                                                                    documentVersion.DocumentSize = docSize;
        //                                                                    documentVersion.DocumentDisplaySize = docSize > 1024 ? (docSize / 1024).ToString("0.00") + " MB" : docSize.ToString("0.00") + " KB";
        //                                                                    documentVersion.DocumentVersion = 1;
        //                                                                    documentVersion.PhysicalLocation = "default";
        //                                                                    documentVersion.Description = "default";
        //                                                                    documentVersion.IsCurrent = true;
        //                                                                    //documentVersion.IsLocked = false;
        //                                                                    documentVersion.DocumentPath = savePath + Name + "." + result;
        //                                                                    documentVersion.IsActive = true;
        //                                                                    documentVersion.IsDelete = false;
        //                                                                    if (parentFolder != null)
        //                                                                    {
        //                                                                        if (parentFolder.IsPublic)
        //                                                                        {
        //                                                                            documentVersion.IsPublic = true;
        //                                                                        }
        //                                                                        else
        //                                                                        {
        //                                                                            documentVersion.IsPublic = false;
        //                                                                        }
        //                                                                    }
        //                                                                    documentVersion.CreatedBy = detail.UserId.ToString();
        //                                                                    documentVersion.CreatedDate = DateTime.Now;
        //                                                                    db.WF_DocumentVersionDetail.Add(documentVersion);
        //                                                                    db.SaveChanges();
        //                                                                }
        //                                                            }
        //                                                            catch
        //                                                            {
        //                                                                throw;
        //                                                            }


        //                                                        }
        //                                                    }
        //                                                    catch (Exception ex)
        //                                                    {

        //                                                    }
        //                                                }
        //                                                #endregion

        //                                                responsemesg.message = "Success";
        //                                                responsemesg.InstanceId = instacneid;
        //                                            }

        //                                            #endregion
        //                                        }
        //                                        else
        //                                        {
        //                                            responsemesg.message = "UserId is required";
        //                                        }
        //                                    }
        //                                    else
        //                                    {
        //                                        responsemesg.message = "Priority is required";
        //                                    }
        //                                }
        //                                else
        //                                {
        //                                    responsemesg.message = "WFInitiateName is required";
        //                                }
        //                            }
        //                            else
        //                            {
        //                                responsemesg.message = "Failed";
        //                            }
        //                        }
        //                        else
        //                        {
        //                            responsemesg.message = "Invalid TokenId";
        //                        }


        //                    }
        //                    catch (Exception e)
        //                    {
        //                        responsemesg.message = e.Message.ToString();
        //                    }
        //                }
        //                else
        //                {
        //                    responsemesg.message = "Data is null";
        //                }
        //            }
        //            else
        //            {
        //                responsemesg.message = "Data is null";
        //            }
        //        }


        //    }
        //    catch (Exception ex)
        //    {
        //        // Get stack trace for the exception with source file information
        //        var st = new StackTrace(ex, true);
        //        // Get the top stack frame
        //        var frame = st.GetFrame(0);
        //        // Get the line number from the stack frame
        //        var line = frame.GetFileLineNumber();
        //        responsemesg.message = line + ":" + ex.Message;
        //    }

        //    return Request.CreateResponse(HttpStatusCode.OK, responsemesg);
        //}
    }
}
