﻿using IntegrationApi.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using Utility.Helpers;
using Utility.Models;

namespace IntegrationApi.Helper
{
    public class CommonFunctions
    {
        #region --> Get UDID for Document WorkFlow | Add | kiiran | 09022021
        public static string GetUDIDForDocumentWorkFlow()
        {
            string UDID = string.Empty;
            try
            {
                using (DBEntities db = new DBEntities())
                {
                    var documentLst = db.WF_DocumentDetail.ToList();

                    if (documentLst.Count > 0)
                    {
                        var lastUDID = documentLst.OrderByDescending(j => j.WFDocumentId).FirstOrDefault().UDID;

                        lastUDID = lastUDID.Substring(3);

                        long Id = Convert.ToInt32(lastUDID) + 1;

                        UDID = "DOC" + Id.ToString();
                    }
                    else
                    {
                        UDID = "DOC10001";
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return UDID;
        }
        #endregion
        #region --> Get UDID for Folder WorkFlow | Add | kiiran | 09022021
        public static string GetUDIDForFolderWorkFlow()
        {
            string UDID = string.Empty;
            try
            {
                using (DBEntities db = new DBEntities())
                {
                    var folderLst = db.WF_FolderDetail.ToList();

                    if (folderLst.Count > 0)
                    {
                        var lastUDID = folderLst.OrderByDescending(j => j.WFFolderDetailId).FirstOrDefault().UDID;

                        lastUDID = lastUDID.Substring(3);

                        long Id = Convert.ToInt32(lastUDID) + 1;

                        UDID = "FOL" + Id.ToString();
                    }
                    else
                    {
                        UDID = "FOL10001";
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return UDID;
        }
        #endregion       
        #region --> Save Notification | Kiran Sawant |  02032021     

        /// <summary>
        /// Save Notification of WorkFlow | Kiran Sawant |  02032021
        /// </summary>
        /// <param name="detail"></param>
        public static void SaveNotificationWorkFlow(SaveNotificationWorkFlowVM detail)
        {
            try
            {
                DAL objDAL = new DAL();
                SortedList sl = new SortedList();
                sl.Add("@UserId", detail.UserId);
                sl.Add("@CabinetId", detail.CabinetId);
                sl.Add("@NotificationType", detail.NotificationType);
                sl.Add("@SendUserId", detail.SendUserId);
                sl.Add("@InstanceId", detail.InstanceId);
                sl.Add("@Comment", detail.Comment);

                objDAL.ExecuteScaler("uspSaveNotificationWorkFlow", sl);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion
    }
}