﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntegrationApi.Helpers
{
    public enum StatusEnum
    {
        Fail = 0,

        Success = 1,

        Invalid = 2,

        Error = 3,

        unauthorized = 5
    }    
}