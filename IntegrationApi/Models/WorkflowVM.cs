﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntegrationApi.Models
{
    public class UserWorkflowAPIVM
    {
        public string TokenId { get; set; }
        public long UserId { get; set; }
        public long WFId { get; set; }
        public long Priority { get; set; }
        public string WFInitiateName { get; set; }
        public bool SkipNextStep { get; set; }
        public string StepNotes { get; set; }
        public List<UserWorkflowFormAPIVM> FormValues { get; set; }
        public UserWorkflowAPIVM()
        {
            FormValues = new List<UserWorkflowFormAPIVM>();
            SkipNextStep = false;
        }
    }

    public class UserWorkflowFormAPIVM
    {
        public string FiledId { get; set; }
        public string Value { get; set; }
    }
    public class FormKeyValueDC
    {
        public long WFInstanceid { get; set; }
        public string FormID { get; set; }
    }

    public class UserWorkflowDetailVM
    {
        public string InitiateName { get; set; }
        public string WFStatus { get; set; }
        public string CurrentStep { get; set; }
        public string PreviousStep { get; set; }
        public string Previousnote { get; set; }
        
    }
    public class SaveNotificationWorkFlowVM
    {
        public long UserId { get; set; }
        public long CabinetId { get; set; }
        public int NotificationType { get; set; }
        public string Comment { get; set; }
        public string @SendUserId { get; set; }
        public long @InstanceId { get; set; }
    }

}