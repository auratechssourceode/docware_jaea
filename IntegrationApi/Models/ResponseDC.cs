﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntegrationApi.Models
{
    public class ResponseDC
    {
        public long InstanceId { get; set; }
        public string message { get; set; }
    }
    /// <summary>
    /// ResponseDC1 | Add | Kiran Sawant | 08-04-2020
    /// </summary>
    public class ResponseDC1
    {
        public int ResponseCode { get; set; }

        public string ResponseMsg { get; set; }

        public Dictionary<string, string> ResponseData { get; set; }

    }
    public class ResponseDataDC2<T>
    {
        public int ResponseCode { get; set; }

        public string ResponseMsg { get; set; }

        private List<T> _Data = new List<T>();

        public List<T> ResponseData
        {
            get
            {
                return _Data;
            }
            set
            {
                _Data = value;
            }
        }
    }
}