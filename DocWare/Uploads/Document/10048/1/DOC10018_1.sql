
DECLARE @start_date [date] = CAST('2017-10-05' as [date])
DECLARE @end_date [date] = CAST('2017-10-10' as [date])
SELECT
DATEADD(day, [v].[number], @start_date) AS DateList,
CONVERT(VARCHAR(10), DATEADD(day, [v].[number], @start_date), 112) AS DateInt,
-- For Cash Paymnent

ISNULL((SELECT  sum(PaidAmount) from BillReceipt CCT INNER JOIN ConsumerMaster CM ON CCT.ConsumerNumber=CM.ConsumerNumber WHERE CCT.IsDelete=0 AND  TransactionDateInt=CONVERT(VARCHAR(10), DATEADD(day, [v].[number], @start_date), 112) AND PaymentTypeId=1  AND CM.AccountTypeId=1),0) AS AmountOFCash11, 
ISNULL((SELECT  sum(PaidAmount) from BillReceipt CCT INNER JOIN ConsumerMaster CM ON CCT.ConsumerNumber=CM.ConsumerNumber WHERE CCT.IsDelete=0 AND  TransactionDateInt=CONVERT(VARCHAR(10), DATEADD(day, [v].[number], @start_date), 112) AND PaymentTypeId=1  AND CM.AccountTypeId=2),0) AS AmountOFCash12 ,
ISNULL((SELECT  sum(PaidAmount) from BillReceipt CCT INNER JOIN ConsumerMaster CM ON CCT.ConsumerNumber=CM.ConsumerNumber WHERE CCT.IsDelete=0 AND  TransactionDateInt=CONVERT(VARCHAR(10), DATEADD(day, [v].[number], @start_date), 112) AND PaymentTypeId=1  AND CM.AccountTypeId=3),0) AS AmountOFCash13 ,
ISNULL((SELECT  sum(PaidAmount) from BillReceipt CCT INNER JOIN ConsumerMaster CM ON CCT.ConsumerNumber=CM.ConsumerNumber WHERE CCT.IsDelete=0 AND  TransactionDateInt=CONVERT(VARCHAR(10), DATEADD(day, [v].[number], @start_date), 112) AND PaymentTypeId=1  AND CM.AccountTypeId=4),0) AS AmountOFCash14 ,
ISNULL((SELECT  sum(PaidAmount) from BillReceipt CCT INNER JOIN ConsumerMaster CM ON CCT.ConsumerNumber=CM.ConsumerNumber WHERE CCT.IsDelete=0 AND  TransactionDateInt=CONVERT(VARCHAR(10), DATEADD(day, [v].[number], @start_date), 112) AND PaymentTypeId=1  AND CM.AccountTypeId=6),0) AS AmountOFCash16,
ISNULL((SELECT  sum(PaidAmount) from BillReceipt CCT INNER JOIN ConsumerMaster CM ON CCT.ConsumerNumber=CM.ConsumerNumber WHERE CCT.IsDelete=0 AND  TransactionDateInt=CONVERT(VARCHAR(10), DATEADD(day, [v].[number], @start_date), 112) AND PaymentTypeId=1  AND CM.AccountTypeId=7),0) AS AmountOFCash17,

-- For Non CTS Cheque
ISNULL((SELECT  sum(PaidAmount) from BillReceipt CCT INNER JOIN ConsumerMaster CM ON CCT.ConsumerNumber=CM.ConsumerNumber WHERE CCT.IsDelete=0 AND  TransactionDateInt=CONVERT(VARCHAR(10), DATEADD(day, [v].[number], @start_date), 112) AND PaymentTypeId=2  AND CM.AccountTypeId=1),0) AS AmountOF_NONCTC11, 
ISNULL((SELECT  sum(PaidAmount) from BillReceipt CCT INNER JOIN ConsumerMaster CM ON CCT.ConsumerNumber=CM.ConsumerNumber WHERE CCT.IsDelete=0 AND  TransactionDateInt=CONVERT(VARCHAR(10), DATEADD(day, [v].[number], @start_date), 112) AND PaymentTypeId=2  AND CM.AccountTypeId=2),0) AS AmountOF_NONCT12 ,
ISNULL((SELECT  sum(PaidAmount) from BillReceipt CCT INNER JOIN ConsumerMaster CM ON CCT.ConsumerNumber=CM.ConsumerNumber WHERE CCT.IsDelete=0 AND  TransactionDateInt=CONVERT(VARCHAR(10), DATEADD(day, [v].[number], @start_date), 112) AND PaymentTypeId=2  AND CM.AccountTypeId=3),0) AS AmountOF_NONCT13 ,
ISNULL((SELECT  sum(PaidAmount) from BillReceipt CCT INNER JOIN ConsumerMaster CM ON CCT.ConsumerNumber=CM.ConsumerNumber WHERE CCT.IsDelete=0 AND  TransactionDateInt=CONVERT(VARCHAR(10), DATEADD(day, [v].[number], @start_date), 112) AND PaymentTypeId=2  AND CM.AccountTypeId=4),0) AS AmountOF_NONCT14 ,
ISNULL((SELECT  sum(PaidAmount) from BillReceipt CCT INNER JOIN ConsumerMaster CM ON CCT.ConsumerNumber=CM.ConsumerNumber WHERE CCT.IsDelete=0 AND  TransactionDateInt=CONVERT(VARCHAR(10), DATEADD(day, [v].[number], @start_date), 112) AND PaymentTypeId=2  AND CM.AccountTypeId=6),0) AS AmountOF_NONCT16,
ISNULL((SELECT  sum(PaidAmount) from BillReceipt CCT INNER JOIN ConsumerMaster CM ON CCT.ConsumerNumber=CM.ConsumerNumber WHERE CCT.IsDelete=0 AND  TransactionDateInt=CONVERT(VARCHAR(10), DATEADD(day, [v].[number], @start_date), 112) AND PaymentTypeId=2  AND CM.AccountTypeId=7),0) AS AmountOF_NONCT17,

-- For CTC Cheque

ISNULL((SELECT  sum(PaidAmount) from BillReceipt CCT INNER JOIN ConsumerMaster CM ON CCT.ConsumerNumber=CM.ConsumerNumber WHERE CCT.IsDelete=0 AND  TransactionDateInt=CONVERT(VARCHAR(10), DATEADD(day, [v].[number], @start_date), 112) AND PaymentTypeId=3  AND CM.AccountTypeId=1),0) AS AmountOF_CTCChque11, 
ISNULL((SELECT  sum(PaidAmount) from BillReceipt CCT INNER JOIN ConsumerMaster CM ON CCT.ConsumerNumber=CM.ConsumerNumber WHERE CCT.IsDelete=0 AND  TransactionDateInt=CONVERT(VARCHAR(10), DATEADD(day, [v].[number], @start_date), 112) AND PaymentTypeId=3  AND CM.AccountTypeId=2),0) AS AmountOF_CTCChque12 ,
ISNULL((SELECT  sum(PaidAmount) from BillReceipt CCT INNER JOIN ConsumerMaster CM ON CCT.ConsumerNumber=CM.ConsumerNumber WHERE CCT.IsDelete=0 AND  TransactionDateInt=CONVERT(VARCHAR(10), DATEADD(day, [v].[number], @start_date), 112) AND PaymentTypeId=3  AND CM.AccountTypeId=3),0) AS AmountOF_CTCChque13 ,
ISNULL((SELECT  sum(PaidAmount) from BillReceipt CCT INNER JOIN ConsumerMaster CM ON CCT.ConsumerNumber=CM.ConsumerNumber WHERE CCT.IsDelete=0 AND  TransactionDateInt=CONVERT(VARCHAR(10), DATEADD(day, [v].[number], @start_date), 112) AND PaymentTypeId=3  AND CM.AccountTypeId=4),0) AS AmountOF_CTCChque14 ,
ISNULL((SELECT  sum(PaidAmount) from BillReceipt CCT INNER JOIN ConsumerMaster CM ON CCT.ConsumerNumber=CM.ConsumerNumber WHERE CCT.IsDelete=0 AND  TransactionDateInt=CONVERT(VARCHAR(10), DATEADD(day, [v].[number], @start_date), 112) AND PaymentTypeId=3  AND CM.AccountTypeId=6),0) AS AmountOF_CTCChque16,
ISNULL((SELECT  sum(PaidAmount) from BillReceipt CCT INNER JOIN ConsumerMaster CM ON CCT.ConsumerNumber=CM.ConsumerNumber WHERE CCT.IsDelete=0 AND  TransactionDateInt=CONVERT(VARCHAR(10), DATEADD(day, [v].[number], @start_date), 112) AND PaymentTypeId=3  AND CM.AccountTypeId=7),0) AS AmountOF_CTCChque17


FROM
[master].[dbo].[spt_values] [v]
WHERE
[v].[type] = 'P' AND

DATEADD(day, [v].[number], @start_date) <= @end_date
