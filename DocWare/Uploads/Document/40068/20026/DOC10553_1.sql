USE HRWare
DECLARE @MonthLimit AS DECIMAL(18,2)=10;
DECLARE @DayLimit AS DECIMAL(18,2)=2;

DECLARE @TotalApprovedHourPerDay AS DECIMAL(18,2)=0;
DECLARE @TotalApprovedHourPerMonth AS DECIMAL(18,2)=0;

DECLARE @TotalBalanceHourPerDay AS DECIMAL(18,2)=0;
DECLARE @TotalBalanceHourPerMonth AS DECIMAL(18,2)=0;

-- Request param-------
DECLARE @RequestHR AS DECIMAL(18,2)=0.55;
DECLARE @RequestDate AS Datetime='2020-08-04 12:00:30.400';--GETDATE()
DECLARE @RequestDateINt AS BIGINT=convert(varchar, @RequestDate, 112);


SELECT DATEDIFF(MINUTE,FromTime,ToTime) from RequestMaster;

DECLARE @UserId AS BIGINT=1;

-- Calculate Total Approved Hrs

select  @TotalApprovedHourPerMonth= SUM(CAST((ISNULL(( dbo.CalculatHoursNumber(R.FromTime,R.ToTime)),0))  AS DECIMAL(18,2) ))  from RequestMaster R  Where R.CreatedBy=@UserId AND R.RequestStatusId=2 
AND R.IsDelete=0 AND R.IsActive=1  AND R.RequestTypeId=1 
AND MONTH(R.Date) = Month(@RequestDate)

select  @TotalApprovedHourPerDay= SUM(CAST((ISNULL(( dbo.CalculatHoursNumber(R.FromTime,R.ToTime)),0))  AS DECIMAL(18,2) ))  from RequestMaster R  Where R.CreatedBy=@UserId 
AND R.RequestStatusId=2 AND R.IsDelete=0 AND R.IsActive=1  AND R.RequestTypeId=1 
AND MONTH(R.Date) = Month(@RequestDate) AND DAY(R.Date) = DAY(@RequestDate) AND R.DateInt=@RequestDateINt


SET @TotalApprovedHourPerDay= ISNULL(@TotalApprovedHourPerDay,0)
SET @TotalApprovedHourPerMonth= ISNULL(@TotalApprovedHourPerMonth,0)

-- Calculate Total balance Hrs

SET @TotalBalanceHourPerDay=ISNULL((@DayLimit-@TotalApprovedHourPerDay),0)
SET @TotalBalanceHourPerMonth=ISNULL((@MonthLimit-@TotalApprovedHourPerMonth),0)



SELECT MONTH(@RequestDate) AS MonthNo,Day(@RequestDate) As Dayno,@MonthLimit As MonthLimit,@DayLimit As DayLimit,@TotalApprovedHourPerMonth As TotalApprovedHourPerMonth,@TotalApprovedHourPerDay as TotalApprovedHourPerDay
,@TotalBalanceHourPerDay AS TotalBalanceHourPerDay,@TotalBalanceHourPerMonth AS TotalBalanceHourPerMonth,@RequestHR AS RequestHR

IF(@RequestHR <= @TotalBalanceHourPerMonth)
BEGIN
-- SELECT 'Step 1 Clear : Check for per day Limit now'


	IF(@RequestHR <=@TotalBalanceHourPerDay)
	BEGIN
	SELECT 'Return TRUE'
	END 
	ELSE
	BEGIN
	 SELECT 'Return False. You have crossed Daily Limit.'
	END

END 
ELSE
BEGIN 
 SELECT 'Return False. You have crossed Monthly Limit.'
END

--	if(@TotalApprovedHourPerDay < @DayLimit)