USE [HRWare]
GO
/****** Object:  UserDefinedFunction [dbo].[CalculatHours]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Kiran>
-- Create date: <22072020,,>
-- Description:	<Calculat Hours,,>
-- =============================================
CREATE FUNCTION [dbo].[CalculatHours](
   @Sign_In Datetime,
	@Sign_Out Datetime
)
RETURNS VARCHAR(30)
AS 
BEGIN

--DECLARE @Sign_In datetime

--DECLARE @Sign_Out datetime

DECLARE @WorkedHours DECIMAL(18,2)

DECLARE @HOURS VARCHAR(30)

DECLARE @MINUTES VARCHAR(2)

DECLARE @SECONDS VARCHAR(30)

--SET @Sign_In =  '2009-04-03 01:35:00.000'
--SET @Sign_Out = '2009-04-03 20:05:00.000'



SET @SECONDS = ABS(DATEDIFF(SECOND, @Sign_In, @Sign_Out))

SET @HOURS = @SECONDS / 3600



SET @MINUTES = (@SECONDS - (@HOURS * 3600)) / 60



SET @SECONDS = (@SECONDS - (@HOURS * 3600) - (@MINUTES * 60))

SET @WorkedHours = @HOURS + '.' + @MINUTES 

--SELECT @HOURS + ':' + @MINUTES  AS WorkedHours
    RETURN @HOURS + ':' + @MINUTES
END;
GO
/****** Object:  Table [dbo].[AuditrialLog]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AuditrialLog](
	[AuidtrailLogId] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[LogTypeId] [bigint] NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[IPAddress] [nvarchar](max) NOT NULL,
	[Browser] [nvarchar](max) NOT NULL,
	[Version] [nvarchar](max) NOT NULL,
	[IsMobile] [bit] NOT NULL,
	[AuditDate] [datetime] NOT NULL,
	[AuditDateInt]  AS ((datepart(year,[AuditDate])*(10000)+datepart(month,[AuditDate])*(100))+datepart(day,[AuditDate])),
 CONSTRAINT [PK_AuidtrailLog] PRIMARY KEY CLUSTERED 
(
	[AuidtrailLogId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmailTemplate]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailTemplate](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[Subject] [nvarchar](200) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[Body] [nvarchar](max) NOT NULL,
	[IsEdit] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_EmailTemplate] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmailTemplateTag]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailTemplateTag](
	[EmailTemplateTagID] [bigint] IDENTITY(1,1) NOT NULL,
	[EmailTemplateTag] [varchar](50) NOT NULL,
	[Description] [varchar](500) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[TagType] [bigint] NOT NULL,
	[DisplayIndex] [int] NOT NULL,
 CONSTRAINT [PK_MessgaeTagMaster] PRIMARY KEY CLUSTERED 
(
	[EmailTemplateTagID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmailTemplateTagType]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailTemplateTagType](
	[TagTypeId] [bigint] IDENTITY(1,1) NOT NULL,
	[TypeName] [nvarchar](50) NOT NULL,
	[ViewIndex] [int] NOT NULL,
 CONSTRAINT [PK_EmailTemplateTagType] PRIMARY KEY CLUSTERED 
(
	[TagTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmployeeBankDetail]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmployeeBankDetail](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[Branch] [nvarchar](150) NOT NULL,
	[AccountNumber] [nvarchar](150) NOT NULL,
	[IBANNumber] [nvarchar](150) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_UserBankDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmployeeContactDetail]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmployeeContactDetail](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[Name] [nvarchar](70) NOT NULL,
	[RelationshipId] [bigint] NULL,
	[Mobile] [nvarchar](20) NOT NULL,
	[Telephone] [nvarchar](20) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_UserContactDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmployeeEducation]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmployeeEducation](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[EducationLevelId] [tinyint] NOT NULL,
	[Institution] [nvarchar](max) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[Title] [nvarchar](250) NOT NULL,
	[MajorId] [bigint] NOT NULL,
	[Grade] [nvarchar](150) NOT NULL,
	[SubMajorId] [bigint] NOT NULL,
	[FromDate] [datetime] NOT NULL,
	[ToDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_UserEducation] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmployeeExperience]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmployeeExperience](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[Company_name] [nvarchar](150) NOT NULL,
	[Country] [bigint] NOT NULL,
	[ExperTitle] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[FromDate] [datetime] NOT NULL,
	[ToDate] [datetime] NOT NULL,
	[Designation_Title] [nvarchar](150) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_UserExperience] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmployeeMaster]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmployeeMaster](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NULL,
	[FirstName] [nvarchar](70) NOT NULL,
	[LastName] [nvarchar](70) NOT NULL,
	[DepartmentId] [tinyint] NULL,
	[DesignationId] [tinyint] NULL,
	[ManagerId] [bigint] NULL,
	[JoinDate] [date] NULL,
	[BirthDate] [date] NULL,
	[GenderId] [tinyint] NULL,
	[MartialStatusId] [tinyint] NULL,
	[MobileNumber] [nvarchar](20) NULL,
	[TelephoneNumber] [nvarchar](20) NULL,
	[Address] [nvarchar](max) NULL,
	[CityId] [tinyint] NULL,
	[NationalityId] [tinyint] NULL,
	[PassportNo] [nvarchar](70) NULL,
	[PassportExpiryDate] [date] NULL,
	[NoOfChildren] [int] NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_UserEmpMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ExceptionLog]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExceptionLog](
	[ExceptionLogID] [bigint] IDENTITY(1,1) NOT NULL,
	[LogDate] [datetime] NOT NULL,
	[LogDateInt]  AS ((datepart(year,[LogDate])*(10000)+datepart(month,[LogDate])*(100))+datepart(day,[LogDate])),
	[IPAddress] [nvarchar](max) NULL,
	[Browser] [nvarchar](max) NULL,
	[Version] [nvarchar](max) NULL,
	[IsMobile] [bit] NOT NULL,
	[Controller] [nvarchar](max) NOT NULL,
	[Action] [nvarchar](max) NOT NULL,
	[Exception] [nvarchar](max) NOT NULL,
	[DeveloperNote] [nvarchar](max) NOT NULL,
	[UserId] [bigint] NULL,
 CONSTRAINT [PK_ExceptionLog] PRIMARY KEY CLUSTERED 
(
	[ExceptionLogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Link]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Link](
	[LinkId] [bigint] IDENTITY(1,1) NOT NULL,
	[ModuleId] [bigint] NOT NULL,
	[ApplicationId] [tinyint] NOT NULL,
	[LinkNameArabic] [nvarchar](500) NULL,
	[LinkName] [varchar](100) NOT NULL,
	[Controller] [varchar](50) NOT NULL,
	[Action] [varchar](50) NOT NULL,
	[TypeArabic] [nvarchar](50) NULL,
	[Type] [nvarchar](50) NOT NULL,
	[ViewIndex] [int] NOT NULL,
	[IsDefault] [bit] NOT NULL,
	[IsSingle] [bit] NOT NULL,
	[IsPage] [bit] NOT NULL,
	[LinkGroupId] [int] NOT NULL,
	[ParentId] [bigint] NOT NULL,
	[IsVisible] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_Link] PRIMARY KEY CLUSTERED 
(
	[LinkId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lookup_CityMaster]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lookup_CityMaster](
	[Id] [tinyint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[CountryId] [bigint] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_CityMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lookup_CompanyMaster]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lookup_CompanyMaster](
	[Id] [tinyint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_CompanyMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lookup_CountryMaster]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lookup_CountryMaster](
	[Id] [tinyint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[Nationailty] [nvarchar](150) NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_CountryMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lookup_DepartmentMaster]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lookup_DepartmentMaster](
	[Id] [tinyint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_DepartmentMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lookup_DesignationMaster]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lookup_DesignationMaster](
	[Id] [tinyint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[DepartmentId] [tinyint] NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_DesignationMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lookup_EducationLevelMaster]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lookup_EducationLevelMaster](
	[Id] [tinyint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_EducationLevelMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lookup_GenderMaster]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lookup_GenderMaster](
	[Id] [tinyint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_GenderMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lookup_LanguageMaster]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lookup_LanguageMaster](
	[Id] [tinyint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_LanguageMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lookup_MajorMaster]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lookup_MajorMaster](
	[Id] [tinyint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_MajorMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lookup_MartialStateMaster]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lookup_MartialStateMaster](
	[Id] [tinyint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_MartialStateMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lookup_RelationshipMaster]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lookup_RelationshipMaster](
	[Id] [tinyint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_RelationshipMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lookup_ReligionMaster]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lookup_ReligionMaster](
	[Id] [tinyint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_ReligionMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lookup_RequestAction]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lookup_RequestAction](
	[Id] [tinyint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_RequestAction] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lookup_RequestStatus]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lookup_RequestStatus](
	[Id] [tinyint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_RequestStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lookup_RequestType]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lookup_RequestType](
	[Id] [tinyint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_RequestType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lookup_SubMajorMaster]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lookup_SubMajorMaster](
	[Id] [tinyint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[MajorId] [tinyint] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_Lookup_SubMajorMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Module]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Module](
	[ModuleId] [bigint] IDENTITY(1,1) NOT NULL,
	[ModuleNameArabic] [nvarchar](500) NULL,
	[ModuleName] [varchar](50) NOT NULL,
	[CssClass] [varchar](100) NOT NULL,
	[ViewIndex] [int] NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_Module] PRIMARY KEY CLUSTERED 
(
	[ModuleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Notification]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Notification](
	[NotificationId] [bigint] IDENTITY(1,1) NOT NULL,
	[NotificationTypeId] [bigint] NOT NULL,
	[UserId] [bigint] NOT NULL,
	[SubjectEn] [nvarchar](max) NULL,
	[SubjectAr] [nvarchar](max) NOT NULL,
	[MessageEn] [nvarchar](max) NOT NULL,
	[MessageAr] [nvarchar](max) NOT NULL,
	[IsRead] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
	[RequestTypeId] [int] NULL,
 CONSTRAINT [PK_NotificationList] PRIMARY KEY CLUSTERED 
(
	[NotificationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NotificationType]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotificationType](
	[NotificationTypeId] [bigint] IDENTITY(1,1) NOT NULL,
	[NotificationType] [nvarchar](70) NOT NULL,
	[MessageEn] [nvarchar](max) NOT NULL,
	[MessageAr] [nvarchar](max) NOT NULL,
	[NameEn] [nvarchar](70) NULL,
	[NameAr] [nvarchar](70) NULL,
 CONSTRAINT [PK_NotificationType] PRIMARY KEY CLUSTERED 
(
	[NotificationTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PasswordHistory]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PasswordHistory](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[OldPassword] [nvarchar](200) NOT NULL,
	[ResetTime] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_PasswordHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PasswordResetRequest]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PasswordResetRequest](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[UserToken] [nvarchar](200) NOT NULL,
	[TokenTime] [datetime] NOT NULL,
	[RequestedIPAddress] [nvarchar](100) NULL,
	[TokenExpireTime] [datetime] NOT NULL,
	[IsUsed] [bit] NOT NULL,
	[Status] [nvarchar](100) NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_ResetPasswordRequest] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RequestDetail]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RequestDetail](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[RequestId] [bigint] NOT NULL,
	[RequestDescription] [nvarchar](max) NULL,
	[RequestActionId] [tinyint] NULL,
	[ActionComment] [nvarchar](max) NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[ActionBy] [nvarchar](max) NULL,
	[ActionDate] [datetime] NULL,
	[ActionDateInt]  AS ((datepart(year,[ActionDate])*(10000)+datepart(month,[ActionDate])*(100))+datepart(day,[ActionDate])),
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_UserRequestDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RequestMaster]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RequestMaster](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[RequestTypeId] [tinyint] NOT NULL,
	[FromTime] [datetime] NOT NULL,
	[FromTimeInt]  AS ((datepart(year,[FromTime])*(10000)+datepart(month,[FromTime])*(100))+datepart(day,[FromTime])),
	[ToTime] [datetime] NOT NULL,
	[ToTimeInt]  AS ((datepart(year,[ToTime])*(10000)+datepart(month,[ToTime])*(100))+datepart(day,[ToTime])),
	[Date] [date] NOT NULL,
	[DateInt]  AS ((datepart(year,[Date])*(10000)+datepart(month,[Date])*(100))+datepart(day,[Date])),
	[Comments] [nvarchar](max) NOT NULL,
	[ManagerId] [bigint] NOT NULL,
	[RequestStatusId] [tinyint] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_RequestMaster_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RoleMaster]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoleMaster](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_RoleMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RoleRight]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoleRight](
	[RoleRightId] [bigint] IDENTITY(1,1) NOT NULL,
	[RoleId] [bigint] NOT NULL,
	[LinkId] [bigint] NOT NULL,
 CONSTRAINT [PK_RoleRight] PRIMARY KEY CLUSTERED 
(
	[RoleRightId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SecretAnswer]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SecretAnswer](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[Que1] [nvarchar](400) NOT NULL,
	[Ans1] [nvarchar](400) NOT NULL,
	[Que2] [nvarchar](400) NOT NULL,
	[Ans2] [nvarchar](400) NOT NULL,
	[Que3] [nvarchar](400) NOT NULL,
	[Ans3] [nvarchar](400) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_SecretAnswer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserLoginAttemptLog]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserLoginAttemptLog](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NULL,
	[LoginStatusId] [bigint] NULL,
	[LoginDate] [datetime] NULL,
	[LoginDateInt]  AS ((datepart(year,[LoginDate])*(10000)+datepart(month,[LoginDate])*(100))+datepart(day,[LoginDate])),
	[LastLoginIP] [nvarchar](50) NULL,
	[UserLoginIP] [nvarchar](50) NULL,
	[UserComputerName] [nvarchar](150) NULL,
 CONSTRAINT [PK_UserLoginAttemptLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserLoginCredential]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserLoginCredential](
	[UserId] [bigint] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](200) NOT NULL,
	[Password] [nvarchar](200) NOT NULL,
	[EmailId] [nvarchar](200) NOT NULL,
	[LastLogin] [datetime] NULL,
	[LastLoginDateInt]  AS ((datepart(year,[LastLogin])*(10000)+datepart(month,[LastLogin])*(100))+datepart(day,[LastLogin])),
	[LastLoginIP] [nvarchar](50) NULL,
	[UserLoginIP] [nvarchar](50) NULL,
	[UserComputerName] [nvarchar](150) NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NULL,
	[Unlock] [bit] NOT NULL,
	[CanLogin] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
	[UnlockBy] [nvarchar](max) NULL,
	[UnlockDate] [datetime] NULL,
	[UnlockDateInt]  AS ((datepart(year,[UnlockDate])*(10000)+datepart(month,[UnlockDate])*(100))+datepart(day,[UnlockDate])),
	[IsUserhaveLogin] [bit] NOT NULL,
 CONSTRAINT [PK_UserLoginCredential] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserRight]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRight](
	[UserRightId] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[LinkId] [bigint] NOT NULL,
 CONSTRAINT [PK_UserRight] PRIMARY KEY CLUSTERED 
(
	[UserRightId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserRole]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRole](
	[UserRoleId] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[RoleId] [bigint] NOT NULL,
 CONSTRAINT [PK_UserRole] PRIMARY KEY CLUSTERED 
(
	[UserRoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AuditrialLog] ADD  CONSTRAINT [DF_AuidtrailLog_IsMobile]  DEFAULT ((0)) FOR [IsMobile]
GO
ALTER TABLE [dbo].[AuditrialLog] ADD  CONSTRAINT [DF_AuidtrailLog_AuditDate]  DEFAULT (getdate()) FOR [AuditDate]
GO
ALTER TABLE [dbo].[EmailTemplate] ADD  CONSTRAINT [DF_EmailTemplate_IsEdit]  DEFAULT ((1)) FOR [IsEdit]
GO
ALTER TABLE [dbo].[EmailTemplate] ADD  CONSTRAINT [DF_EmailTemplate_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[EmailTemplate] ADD  CONSTRAINT [DF_EmailTemplate_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[EmailTemplate] ADD  CONSTRAINT [DF_EmailTemplate_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[EmailTemplate] ADD  CONSTRAINT [DF_EmailTemplate_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[EmailTemplate] ADD  CONSTRAINT [DF_EmailTemplate_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[EmailTemplate] ADD  CONSTRAINT [DF_EmailTemplate_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[EmailTemplateTag] ADD  CONSTRAINT [DF_EmailTemplateTag_IsActive_1]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[EmployeeBankDetail] ADD  CONSTRAINT [DF_UserBankDetail_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[EmployeeBankDetail] ADD  CONSTRAINT [DF_UserBankDetail_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[EmployeeBankDetail] ADD  CONSTRAINT [DF_UserBankDetail_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[EmployeeBankDetail] ADD  CONSTRAINT [DF_UserBankDetail_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[EmployeeBankDetail] ADD  CONSTRAINT [DF_UserBankDetail_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[EmployeeContactDetail] ADD  CONSTRAINT [DF_UserContactDetail_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[EmployeeContactDetail] ADD  CONSTRAINT [DF_UserContactDetail_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[EmployeeContactDetail] ADD  CONSTRAINT [DF_UserContactDetail_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[EmployeeContactDetail] ADD  CONSTRAINT [DF_UserContactDetail_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[EmployeeContactDetail] ADD  CONSTRAINT [DF_UserContactDetail_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[EmployeeEducation] ADD  CONSTRAINT [DF_UserEducation_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[EmployeeEducation] ADD  CONSTRAINT [DF_Lookup_UserEducation_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[EmployeeEducation] ADD  CONSTRAINT [DF_Lookup_UserEducation_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[EmployeeEducation] ADD  CONSTRAINT [DF_Lookup_UserEducation_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[EmployeeEducation] ADD  CONSTRAINT [DF_Lookup_UserEducation_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[EmployeeEducation] ADD  CONSTRAINT [DF_Lookup_UserEducation_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[EmployeeExperience] ADD  CONSTRAINT [DF_UserExperience_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[EmployeeExperience] ADD  CONSTRAINT [DF_Lookup_UserExperience_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[EmployeeExperience] ADD  CONSTRAINT [DF_Lookup_UserExperience_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[EmployeeExperience] ADD  CONSTRAINT [DF_Lookup_UserExperience_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[EmployeeExperience] ADD  CONSTRAINT [DF_Lookup_UserExperience_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[EmployeeExperience] ADD  CONSTRAINT [DF_Lookup_UserExperience_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[EmployeeMaster] ADD  CONSTRAINT [DF_Table_1_CreatedDate]  DEFAULT (getdate()) FOR [JoinDate]
GO
ALTER TABLE [dbo].[EmployeeMaster] ADD  CONSTRAINT [DF_UserEmpMaster_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[EmployeeMaster] ADD  CONSTRAINT [DF_UserEmpMaster_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[EmployeeMaster] ADD  CONSTRAINT [DF_UserEmpMaster_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[EmployeeMaster] ADD  CONSTRAINT [DF_UserEmpMaster_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[EmployeeMaster] ADD  CONSTRAINT [DF_UserEmpMaster_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[EmployeeMaster] ADD  CONSTRAINT [DF_UserEmpMaster_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[ExceptionLog] ADD  CONSTRAINT [DF_ExceptionLog_LogDate]  DEFAULT (getdate()) FOR [LogDate]
GO
ALTER TABLE [dbo].[ExceptionLog] ADD  CONSTRAINT [DF_ExceptionLog_IsMobile]  DEFAULT ((0)) FOR [IsMobile]
GO
ALTER TABLE [dbo].[Link] ADD  CONSTRAINT [DF_Link_IsDefault]  DEFAULT ((1)) FOR [IsDefault]
GO
ALTER TABLE [dbo].[Link] ADD  CONSTRAINT [DF_Link_IsSingle]  DEFAULT ((0)) FOR [IsSingle]
GO
ALTER TABLE [dbo].[Link] ADD  CONSTRAINT [DF_Link_IsPage]  DEFAULT ((1)) FOR [IsPage]
GO
ALTER TABLE [dbo].[Link] ADD  CONSTRAINT [DF_Link_ParentId]  DEFAULT ((0)) FOR [ParentId]
GO
ALTER TABLE [dbo].[Link] ADD  CONSTRAINT [DF_Link_IsVisible]  DEFAULT ((0)) FOR [IsVisible]
GO
ALTER TABLE [dbo].[Lookup_CityMaster] ADD  CONSTRAINT [DF_CityMaster_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Lookup_CityMaster] ADD  CONSTRAINT [DF_Lookup_CityMaster_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[Lookup_CityMaster] ADD  CONSTRAINT [DF_Lookup_CityMaster_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[Lookup_CityMaster] ADD  CONSTRAINT [DF_Lookup_CityMaster_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Lookup_CityMaster] ADD  CONSTRAINT [DF_Lookup_CityMaster_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[Lookup_CityMaster] ADD  CONSTRAINT [DF_Lookup_CityMaster_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[Lookup_CompanyMaster] ADD  CONSTRAINT [DF_CompanyMaster_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Lookup_CompanyMaster] ADD  CONSTRAINT [DF_Lookup_CompanyMaster_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[Lookup_CompanyMaster] ADD  CONSTRAINT [DF_Lookup_CompanyMaster_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[Lookup_CompanyMaster] ADD  CONSTRAINT [DF_Lookup_CompanyMaster_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Lookup_CompanyMaster] ADD  CONSTRAINT [DF_Lookup_CompanyMaster_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[Lookup_CompanyMaster] ADD  CONSTRAINT [DF_Lookup_CompanyMaster_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[Lookup_CountryMaster] ADD  CONSTRAINT [DF_CountryMaster_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Lookup_CountryMaster] ADD  CONSTRAINT [DF_Lookup_CountryMaster_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[Lookup_CountryMaster] ADD  CONSTRAINT [DF_Lookup_CountryMaster_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[Lookup_CountryMaster] ADD  CONSTRAINT [DF_Lookup_CountryMaster_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Lookup_CountryMaster] ADD  CONSTRAINT [DF_Lookup_CountryMaster_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[Lookup_CountryMaster] ADD  CONSTRAINT [DF_Lookup_CountryMaster_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[Lookup_DepartmentMaster] ADD  CONSTRAINT [DF_DepartmentMaster_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Lookup_DepartmentMaster] ADD  CONSTRAINT [DF_Lookup_DepartmentMaster_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[Lookup_DepartmentMaster] ADD  CONSTRAINT [DF_Lookup_DepartmentMaster_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[Lookup_DepartmentMaster] ADD  CONSTRAINT [DF_Lookup_DepartmentMaster_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Lookup_DepartmentMaster] ADD  CONSTRAINT [DF_Lookup_DepartmentMaster_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[Lookup_DepartmentMaster] ADD  CONSTRAINT [DF_Lookup_DepartmentMaster_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[Lookup_DesignationMaster] ADD  CONSTRAINT [DF_DesignationMaster_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Lookup_DesignationMaster] ADD  CONSTRAINT [DF_Lookup_DesignationMaster_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[Lookup_DesignationMaster] ADD  CONSTRAINT [DF_Lookup_DesignationMaster_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[Lookup_DesignationMaster] ADD  CONSTRAINT [DF_Lookup_DesignationMaster_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Lookup_DesignationMaster] ADD  CONSTRAINT [DF_Lookup_DesignationMaster_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[Lookup_DesignationMaster] ADD  CONSTRAINT [DF_Lookup_DesignationMaster_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[Lookup_EducationLevelMaster] ADD  CONSTRAINT [DF_EducationLevelMaster_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Lookup_EducationLevelMaster] ADD  CONSTRAINT [DF_Lookup_EducationLevelMaster_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[Lookup_EducationLevelMaster] ADD  CONSTRAINT [DF_Lookup_EducationLevelMaster_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[Lookup_EducationLevelMaster] ADD  CONSTRAINT [DF_Lookup_EducationLevelMaster_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Lookup_EducationLevelMaster] ADD  CONSTRAINT [DF_Lookup_EducationLevelMaster_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[Lookup_EducationLevelMaster] ADD  CONSTRAINT [DF_Lookup_EducationLevelMaster_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[Lookup_GenderMaster] ADD  CONSTRAINT [DF_GenderMaster_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Lookup_GenderMaster] ADD  CONSTRAINT [DF_Lookup_GenderMaster_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[Lookup_GenderMaster] ADD  CONSTRAINT [DF_Lookup_GenderMaster_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[Lookup_GenderMaster] ADD  CONSTRAINT [DF_Lookup_GenderMaster_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Lookup_GenderMaster] ADD  CONSTRAINT [DF_Lookup_GenderMaster_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[Lookup_GenderMaster] ADD  CONSTRAINT [DF_Lookup_GenderMaster_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[Lookup_LanguageMaster] ADD  CONSTRAINT [DF_LanguageMaster_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Lookup_LanguageMaster] ADD  CONSTRAINT [DF_Lookup_LanguageMaster_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[Lookup_LanguageMaster] ADD  CONSTRAINT [DF_Lookup_LanguageMaster_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[Lookup_LanguageMaster] ADD  CONSTRAINT [DF_Lookup_LanguageMaster_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Lookup_LanguageMaster] ADD  CONSTRAINT [DF_Lookup_LanguageMaster_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[Lookup_LanguageMaster] ADD  CONSTRAINT [DF_Lookup_LanguageMaster_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[Lookup_MajorMaster] ADD  CONSTRAINT [DF_MajorMaster_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Lookup_MajorMaster] ADD  CONSTRAINT [DF_MajorMaster_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[Lookup_MajorMaster] ADD  CONSTRAINT [DF_MajorMaster_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[Lookup_MajorMaster] ADD  CONSTRAINT [DF_MajorMaster_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Lookup_MajorMaster] ADD  CONSTRAINT [DF_MajorMaster_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[Lookup_MajorMaster] ADD  CONSTRAINT [DF_MajorMaster_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[Lookup_MartialStateMaster] ADD  CONSTRAINT [DF_MartialStateMaster_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Lookup_MartialStateMaster] ADD  CONSTRAINT [DF_Lookup_MartialStateMaster_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[Lookup_MartialStateMaster] ADD  CONSTRAINT [DF_Lookup_MartialStateMaster_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[Lookup_MartialStateMaster] ADD  CONSTRAINT [DF_Lookup_MartialStateMaster_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Lookup_MartialStateMaster] ADD  CONSTRAINT [DF_Lookup_MartialStateMaster_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[Lookup_MartialStateMaster] ADD  CONSTRAINT [DF_Lookup_MartialStateMaster_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[Lookup_RelationshipMaster] ADD  CONSTRAINT [DF_RelationshipMaster_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Lookup_RelationshipMaster] ADD  CONSTRAINT [DF_Lookup_RelationshipMaster_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[Lookup_RelationshipMaster] ADD  CONSTRAINT [DF_Lookup_RelationshipMaster_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[Lookup_RelationshipMaster] ADD  CONSTRAINT [DF_Lookup_RelationshipMaster_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Lookup_RelationshipMaster] ADD  CONSTRAINT [DF_Lookup_RelationshipMaster_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[Lookup_RelationshipMaster] ADD  CONSTRAINT [DF_Lookup_RelationshipMaster_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[Lookup_ReligionMaster] ADD  CONSTRAINT [DF_ReligionMaster_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Lookup_ReligionMaster] ADD  CONSTRAINT [DF_Lookup_ReligionMaster_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[Lookup_ReligionMaster] ADD  CONSTRAINT [DF_Lookup_ReligionMaster_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[Lookup_ReligionMaster] ADD  CONSTRAINT [DF_Lookup_ReligionMaster_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Lookup_ReligionMaster] ADD  CONSTRAINT [DF_Lookup_ReligionMaster_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[Lookup_ReligionMaster] ADD  CONSTRAINT [DF_Lookup_ReligionMaster_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[Lookup_RequestAction] ADD  CONSTRAINT [DF_RequestAction_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Lookup_RequestAction] ADD  CONSTRAINT [DF_Lookup_RequestAction_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[Lookup_RequestAction] ADD  CONSTRAINT [DF_Lookup_RequestAction_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[Lookup_RequestAction] ADD  CONSTRAINT [DF_Lookup_RequestAction_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Lookup_RequestAction] ADD  CONSTRAINT [DF_Lookup_RequestAction_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[Lookup_RequestAction] ADD  CONSTRAINT [DF_Lookup_RequestAction_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[Lookup_RequestStatus] ADD  CONSTRAINT [DF_RequestStatus_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Lookup_RequestStatus] ADD  CONSTRAINT [DF_Lookup_RequestStatus_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[Lookup_RequestStatus] ADD  CONSTRAINT [DF_Lookup_RequestStatus_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[Lookup_RequestStatus] ADD  CONSTRAINT [DF_Lookup_RequestStatus_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Lookup_RequestStatus] ADD  CONSTRAINT [DF_Lookup_RequestStatus_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[Lookup_RequestStatus] ADD  CONSTRAINT [DF_Lookup_RequestStatus_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[Lookup_RequestType] ADD  CONSTRAINT [DF_RequestType_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Lookup_RequestType] ADD  CONSTRAINT [DF_Lookup_RequestType_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[Lookup_RequestType] ADD  CONSTRAINT [DF_Lookup_RequestType_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[Lookup_RequestType] ADD  CONSTRAINT [DF_Lookup_RequestType_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Lookup_RequestType] ADD  CONSTRAINT [DF_Lookup_RequestType_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[Lookup_RequestType] ADD  CONSTRAINT [DF_Lookup_RequestType_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[Lookup_SubMajorMaster] ADD  CONSTRAINT [DF_Lookup_SubMajorMaster_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Lookup_SubMajorMaster] ADD  CONSTRAINT [DF_Lookup_SubMajorMaster_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[Lookup_SubMajorMaster] ADD  CONSTRAINT [DF_Lookup_SubMajorMaster_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[Lookup_SubMajorMaster] ADD  CONSTRAINT [DF_Lookup_SubMajorMaster_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Lookup_SubMajorMaster] ADD  CONSTRAINT [DF_Lookup_SubMajorMaster_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[Lookup_SubMajorMaster] ADD  CONSTRAINT [DF_Lookup_SubMajorMaster_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[Module] ADD  CONSTRAINT [DF_Module_ViewIndex]  DEFAULT ((0)) FOR [ViewIndex]
GO
ALTER TABLE [dbo].[Module] ADD  CONSTRAINT [DF_Module_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Module] ADD  CONSTRAINT [DF_Module_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[Module] ADD  CONSTRAINT [DF_Module_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[Module] ADD  CONSTRAINT [DF_Module_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Module] ADD  CONSTRAINT [DF_Module_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[Module] ADD  CONSTRAINT [DF_Module_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[Notification] ADD  CONSTRAINT [DF_Table_1_IsActive]  DEFAULT ((1)) FOR [IsRead]
GO
ALTER TABLE [dbo].[Notification] ADD  CONSTRAINT [DF_NotificationList_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[Notification] ADD  CONSTRAINT [DF_NotificationList_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[Notification] ADD  CONSTRAINT [DF_NotificationList_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Notification] ADD  CONSTRAINT [DF_NotificationList_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[Notification] ADD  CONSTRAINT [DF_NotificationList_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[PasswordHistory] ADD  CONSTRAINT [DF_PasswordHistory_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[PasswordHistory] ADD  CONSTRAINT [DF_PasswordHistory_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[PasswordResetRequest] ADD  CONSTRAINT [DF_Table_1_IsActive_1]  DEFAULT ((0)) FOR [IsUsed]
GO
ALTER TABLE [dbo].[PasswordResetRequest] ADD  CONSTRAINT [DF_ResetPasswordRequest_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[PasswordResetRequest] ADD  CONSTRAINT [DF_ResetPasswordRequest_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[PasswordResetRequest] ADD  CONSTRAINT [DF_ResetPasswordRequest_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[PasswordResetRequest] ADD  CONSTRAINT [DF_ResetPasswordRequest_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[PasswordResetRequest] ADD  CONSTRAINT [DF_PasswordResetRequest_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[RequestDetail] ADD  CONSTRAINT [DF_UserRequestDetail_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[RequestDetail] ADD  CONSTRAINT [DF_RequestDetail_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[RequestDetail] ADD  CONSTRAINT [DF_RequestDetail_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[RequestDetail] ADD  CONSTRAINT [DF_RequestDetail_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[RequestDetail] ADD  CONSTRAINT [DF_RequestDetail_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[RequestDetail] ADD  CONSTRAINT [DF_RequestDetail_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[RequestMaster] ADD  CONSTRAINT [DF_UserRequestMaster_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[RequestMaster] ADD  CONSTRAINT [DF_RequestMaster_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[RequestMaster] ADD  CONSTRAINT [DF_RequestMaster_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[RequestMaster] ADD  CONSTRAINT [DF_RequestMaster_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[RoleMaster] ADD  CONSTRAINT [DF_RoleMaster_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[RoleMaster] ADD  CONSTRAINT [DF_RoleMaster_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[RoleMaster] ADD  CONSTRAINT [DF_RoleMaster_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[RoleMaster] ADD  CONSTRAINT [DF_RoleMaster_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[RoleMaster] ADD  CONSTRAINT [DF_RoleMaster_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[RoleMaster] ADD  CONSTRAINT [DF_RoleMaster_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[RoleRight] ADD  CONSTRAINT [DF_Table_1_UserId]  DEFAULT ((0)) FOR [RoleId]
GO
ALTER TABLE [dbo].[SecretAnswer] ADD  CONSTRAINT [DF_SecretAnswer_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[SecretAnswer] ADD  CONSTRAINT [DF_SecretAnswer_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[SecretAnswer] ADD  CONSTRAINT [DF_SecretAnswer_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[SecretAnswer] ADD  CONSTRAINT [DF_SecretAnswer_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[SecretAnswer] ADD  CONSTRAINT [DF_SecretAnswer_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[UserLoginCredential] ADD  CONSTRAINT [DF_UserLoginCredential_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[UserLoginCredential] ADD  CONSTRAINT [DF_UserLoginCredential_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[UserLoginCredential] ADD  CONSTRAINT [DF_UserLoginCredential_IsActive1]  DEFAULT ((1)) FOR [Unlock]
GO
ALTER TABLE [dbo].[UserLoginCredential] ADD  CONSTRAINT [DF_UserLoginCredential_CanLogin]  DEFAULT ((1)) FOR [CanLogin]
GO
ALTER TABLE [dbo].[UserLoginCredential] ADD  CONSTRAINT [DF_UserLoginCredential_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[UserLoginCredential] ADD  CONSTRAINT [DF_UserLoginCredential_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[UserLoginCredential] ADD  CONSTRAINT [DF_UserLoginCredential_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[UserLoginCredential] ADD  CONSTRAINT [DF_UserLoginCredential_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[UserLoginCredential] ADD  CONSTRAINT [DF_UserLoginCredential_UpdatedBy1]  DEFAULT (suser_name()) FOR [UnlockBy]
GO
ALTER TABLE [dbo].[UserLoginCredential] ADD  CONSTRAINT [DF_UserLoginCredential_UpdatedDate1]  DEFAULT (getdate()) FOR [UnlockDate]
GO
ALTER TABLE [dbo].[UserLoginCredential] ADD  CONSTRAINT [DF_UserLoginCredential_IsUserhaveLogin]  DEFAULT ((0)) FOR [IsUserhaveLogin]
GO
ALTER TABLE [dbo].[UserRight] ADD  CONSTRAINT [DF_UserRight_UserID]  DEFAULT ((0)) FOR [UserId]
GO
ALTER TABLE [dbo].[UserRole] ADD  CONSTRAINT [DF_UserRole_RoleId]  DEFAULT ((0)) FOR [RoleId]
GO
ALTER TABLE [dbo].[EmployeeBankDetail]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeBankDetail_UserLoginCredential] FOREIGN KEY([UserId])
REFERENCES [dbo].[UserLoginCredential] ([UserId])
GO
ALTER TABLE [dbo].[EmployeeBankDetail] CHECK CONSTRAINT [FK_EmployeeBankDetail_UserLoginCredential]
GO
ALTER TABLE [dbo].[EmployeeContactDetail]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeContactDetail_UserLoginCredential] FOREIGN KEY([UserId])
REFERENCES [dbo].[UserLoginCredential] ([UserId])
GO
ALTER TABLE [dbo].[EmployeeContactDetail] CHECK CONSTRAINT [FK_EmployeeContactDetail_UserLoginCredential]
GO
ALTER TABLE [dbo].[EmployeeEducation]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeEducation_UserLoginCredential] FOREIGN KEY([UserId])
REFERENCES [dbo].[UserLoginCredential] ([UserId])
GO
ALTER TABLE [dbo].[EmployeeEducation] CHECK CONSTRAINT [FK_EmployeeEducation_UserLoginCredential]
GO
ALTER TABLE [dbo].[EmployeeEducation]  WITH CHECK ADD  CONSTRAINT [FK_Lookup_UserEducation_Lookup_UserEducation] FOREIGN KEY([EducationLevelId])
REFERENCES [dbo].[Lookup_EducationLevelMaster] ([Id])
GO
ALTER TABLE [dbo].[EmployeeEducation] CHECK CONSTRAINT [FK_Lookup_UserEducation_Lookup_UserEducation]
GO
ALTER TABLE [dbo].[EmployeeExperience]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeExperience_UserLoginCredential] FOREIGN KEY([UserId])
REFERENCES [dbo].[UserLoginCredential] ([UserId])
GO
ALTER TABLE [dbo].[EmployeeExperience] CHECK CONSTRAINT [FK_EmployeeExperience_UserLoginCredential]
GO
ALTER TABLE [dbo].[EmployeeMaster]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeMaster_UserLoginCredential] FOREIGN KEY([UserId])
REFERENCES [dbo].[UserLoginCredential] ([UserId])
GO
ALTER TABLE [dbo].[EmployeeMaster] CHECK CONSTRAINT [FK_EmployeeMaster_UserLoginCredential]
GO
ALTER TABLE [dbo].[EmployeeMaster]  WITH CHECK ADD  CONSTRAINT [FK_UserMaster_CityMaster] FOREIGN KEY([CityId])
REFERENCES [dbo].[Lookup_CityMaster] ([Id])
GO
ALTER TABLE [dbo].[EmployeeMaster] CHECK CONSTRAINT [FK_UserMaster_CityMaster]
GO
ALTER TABLE [dbo].[EmployeeMaster]  WITH CHECK ADD  CONSTRAINT [FK_UserMaster_CountryMaster] FOREIGN KEY([NationalityId])
REFERENCES [dbo].[Lookup_CountryMaster] ([Id])
GO
ALTER TABLE [dbo].[EmployeeMaster] CHECK CONSTRAINT [FK_UserMaster_CountryMaster]
GO
ALTER TABLE [dbo].[EmployeeMaster]  WITH CHECK ADD  CONSTRAINT [FK_UserMaster_DepartmentMaster] FOREIGN KEY([DepartmentId])
REFERENCES [dbo].[Lookup_DepartmentMaster] ([Id])
GO
ALTER TABLE [dbo].[EmployeeMaster] CHECK CONSTRAINT [FK_UserMaster_DepartmentMaster]
GO
ALTER TABLE [dbo].[EmployeeMaster]  WITH CHECK ADD  CONSTRAINT [FK_UserMaster_GenderMaster] FOREIGN KEY([GenderId])
REFERENCES [dbo].[Lookup_GenderMaster] ([Id])
GO
ALTER TABLE [dbo].[EmployeeMaster] CHECK CONSTRAINT [FK_UserMaster_GenderMaster]
GO
ALTER TABLE [dbo].[EmployeeMaster]  WITH CHECK ADD  CONSTRAINT [FK_UserMaster_Lookup_DesignationMaster] FOREIGN KEY([DesignationId])
REFERENCES [dbo].[Lookup_DesignationMaster] ([Id])
GO
ALTER TABLE [dbo].[EmployeeMaster] CHECK CONSTRAINT [FK_UserMaster_Lookup_DesignationMaster]
GO
ALTER TABLE [dbo].[EmployeeMaster]  WITH CHECK ADD  CONSTRAINT [FK_UserMaster_MartialStateMaster] FOREIGN KEY([MartialStatusId])
REFERENCES [dbo].[Lookup_MartialStateMaster] ([Id])
GO
ALTER TABLE [dbo].[EmployeeMaster] CHECK CONSTRAINT [FK_UserMaster_MartialStateMaster]
GO
ALTER TABLE [dbo].[EmployeeMaster]  WITH CHECK ADD  CONSTRAINT [FK_UserMaster_UserMaster1] FOREIGN KEY([ManagerId])
REFERENCES [dbo].[UserLoginCredential] ([UserId])
GO
ALTER TABLE [dbo].[EmployeeMaster] CHECK CONSTRAINT [FK_UserMaster_UserMaster1]
GO
ALTER TABLE [dbo].[Link]  WITH CHECK ADD  CONSTRAINT [FK_Link_Module] FOREIGN KEY([ModuleId])
REFERENCES [dbo].[Module] ([ModuleId])
GO
ALTER TABLE [dbo].[Link] CHECK CONSTRAINT [FK_Link_Module]
GO
ALTER TABLE [dbo].[Lookup_DesignationMaster]  WITH CHECK ADD  CONSTRAINT [FK_Lookup_DesignationMaster_Lookup_DepartmentMaster] FOREIGN KEY([DepartmentId])
REFERENCES [dbo].[Lookup_DepartmentMaster] ([Id])
GO
ALTER TABLE [dbo].[Lookup_DesignationMaster] CHECK CONSTRAINT [FK_Lookup_DesignationMaster_Lookup_DepartmentMaster]
GO
ALTER TABLE [dbo].[Lookup_SubMajorMaster]  WITH CHECK ADD  CONSTRAINT [FK_Lookup_SubMajorMaster_Lookup_MajorMaster] FOREIGN KEY([MajorId])
REFERENCES [dbo].[Lookup_MajorMaster] ([Id])
GO
ALTER TABLE [dbo].[Lookup_SubMajorMaster] CHECK CONSTRAINT [FK_Lookup_SubMajorMaster_Lookup_MajorMaster]
GO
ALTER TABLE [dbo].[PasswordHistory]  WITH CHECK ADD  CONSTRAINT [FK_PasswordHistory_UserLoginCredential] FOREIGN KEY([UserId])
REFERENCES [dbo].[UserLoginCredential] ([UserId])
GO
ALTER TABLE [dbo].[PasswordHistory] CHECK CONSTRAINT [FK_PasswordHistory_UserLoginCredential]
GO
ALTER TABLE [dbo].[PasswordResetRequest]  WITH CHECK ADD  CONSTRAINT [FK_PasswordResetRequest_UserLoginCredential] FOREIGN KEY([UserId])
REFERENCES [dbo].[UserLoginCredential] ([UserId])
GO
ALTER TABLE [dbo].[PasswordResetRequest] CHECK CONSTRAINT [FK_PasswordResetRequest_UserLoginCredential]
GO
ALTER TABLE [dbo].[RequestDetail]  WITH CHECK ADD  CONSTRAINT [FK_RequestDetail_Lookup_RequestAction] FOREIGN KEY([RequestActionId])
REFERENCES [dbo].[Lookup_RequestAction] ([Id])
GO
ALTER TABLE [dbo].[RequestDetail] CHECK CONSTRAINT [FK_RequestDetail_Lookup_RequestAction]
GO
ALTER TABLE [dbo].[RequestDetail]  WITH CHECK ADD  CONSTRAINT [FK_RequestDetail_RequestMaster] FOREIGN KEY([RequestId])
REFERENCES [dbo].[RequestMaster] ([Id])
GO
ALTER TABLE [dbo].[RequestDetail] CHECK CONSTRAINT [FK_RequestDetail_RequestMaster]
GO
ALTER TABLE [dbo].[RequestMaster]  WITH CHECK ADD  CONSTRAINT [FK_RequestMaster_Lookup_RequestStatus] FOREIGN KEY([RequestStatusId])
REFERENCES [dbo].[Lookup_RequestStatus] ([Id])
GO
ALTER TABLE [dbo].[RequestMaster] CHECK CONSTRAINT [FK_RequestMaster_Lookup_RequestStatus]
GO
ALTER TABLE [dbo].[RequestMaster]  WITH CHECK ADD  CONSTRAINT [FK_RequestMaster_Lookup_RequestType] FOREIGN KEY([RequestTypeId])
REFERENCES [dbo].[Lookup_RequestType] ([Id])
GO
ALTER TABLE [dbo].[RequestMaster] CHECK CONSTRAINT [FK_RequestMaster_Lookup_RequestType]
GO
ALTER TABLE [dbo].[RoleRight]  WITH CHECK ADD  CONSTRAINT [FK_RoleRight_Link] FOREIGN KEY([LinkId])
REFERENCES [dbo].[Link] ([LinkId])
GO
ALTER TABLE [dbo].[RoleRight] CHECK CONSTRAINT [FK_RoleRight_Link]
GO
ALTER TABLE [dbo].[SecretAnswer]  WITH CHECK ADD  CONSTRAINT [FK_SecretAnswer_UserLoginCredential] FOREIGN KEY([UserId])
REFERENCES [dbo].[UserLoginCredential] ([UserId])
GO
ALTER TABLE [dbo].[SecretAnswer] CHECK CONSTRAINT [FK_SecretAnswer_UserLoginCredential]
GO
ALTER TABLE [dbo].[UserRight]  WITH CHECK ADD  CONSTRAINT [FK_UserRight_Link] FOREIGN KEY([LinkId])
REFERENCES [dbo].[Link] ([LinkId])
GO
ALTER TABLE [dbo].[UserRight] CHECK CONSTRAINT [FK_UserRight_Link]
GO
ALTER TABLE [dbo].[UserRight]  WITH CHECK ADD  CONSTRAINT [FK_UserRight_UserLoginCredential] FOREIGN KEY([UserId])
REFERENCES [dbo].[UserLoginCredential] ([UserId])
GO
ALTER TABLE [dbo].[UserRight] CHECK CONSTRAINT [FK_UserRight_UserLoginCredential]
GO
ALTER TABLE [dbo].[UserRole]  WITH CHECK ADD  CONSTRAINT [FK_UserRole_RoleMaster] FOREIGN KEY([RoleId])
REFERENCES [dbo].[RoleMaster] ([Id])
GO
ALTER TABLE [dbo].[UserRole] CHECK CONSTRAINT [FK_UserRole_RoleMaster]
GO
ALTER TABLE [dbo].[UserRole]  WITH CHECK ADD  CONSTRAINT [FK_UserRole_UserLoginCredential] FOREIGN KEY([UserId])
REFERENCES [dbo].[UserLoginCredential] ([UserId])
GO
ALTER TABLE [dbo].[UserRole] CHECK CONSTRAINT [FK_UserRole_UserLoginCredential]
GO
/****** Object:  StoredProcedure [dbo].[Usp_Get_HrLeaveList]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                
-- Author:   <Kiran Sawant>                 
-- Created date: <24-07-2020>                
-- Description:  <Get Hr Leave List>                
-- =============================================                
-- Usp_Get_HrLeaveList 3,1               
CREATE PROCEDURE [dbo].[Usp_Get_HrLeaveList]             
@RequestTypeId int,  
@Name Nvarchar(50)  = NULL,
@StatusId INT = NULL,
@FromDate Datetime = NULL ,       
@ToDate Datetime = NULL        
AS 
BEGIN  

SELECT         
(SELECT Count(*) FROM RequestMaster RM WHERE RM.RequestTypeId=@RequestTypeId AND RM.IsDelete=0 ) As TotalLeaveCount  

if(@FromDate != null)              
BEGIN  
 Select rm.Id,        
 rt.Name as RequestType,        
 RequestTypeId,        
  ISNULL(CONVERT(VARCHAR(11), CAST(FromTime AS TIME), 100),'') AS FromTime,              
 ISNULL(CONVERT(VARCHAR(11), CAST(ToTime AS TIME), 100),'') AS ToTime,           
  ISNULL((select dbo.CalculatHours(FromTime,ToTime)),'') as Numberofdays,  
   ISNULL(CONVERT(VARCHAR(11), Date, 106),'') AS Date,         
  Comments,        
  RequestStatusId,        
  rs.Name as Status,        
  ISNULL((select FirstName + ' ' + LastName as ActionBy  from EmployeeMaster where UserId =(select top 1 ActionBy from RequestDetail where RequestId = rm.Id order by Id desc)),'') as ActionBy        
  ,ISNULL((select top 1 RequestDescription from RequestDetail where RequestId = rm.Id order by Id desc),'') as RequestDescription        
  ,ISNULL((select top 1 RequestActionId from RequestDetail where RequestId = rm.Id order by Id desc),0) as RequestActionId        
  ,ISNULL((select Name from Lookup_RequestAction where id = (select top 1 RequestActionId from RequestDetail where RequestId = rm.Id order by Id desc)),'') as RequestActionName        
  ,ISNULL((select FirstName + ' ' + LastName as ActionBy  from EmployeeMaster where UserId = rm.ManagerId),'') as ManagerName  
  ,ISNULL((select FirstName + ' ' + LastName as ActionBy  from EmployeeMaster where UserId = rm.CreatedBy),'') as EmployeeName     
   from RequestMaster rm           
 INNER JOIN Lookup_RequestStatus rs on rs.Id = rm.RequestStatusId        
 INNER JOIN Lookup_RequestType rt on rt.Id = rm.RequestTypeId          
 Where rm.IsDelete = 0 
 and RequestTypeId=@RequestTypeId  
 and (rm.FromTime > Isnull(@FromDate,rm.FromTime) OR rm.ToTime < isnull(@ToDate,@FromDate))
 and rm.RequestStatusId =IIF((@StatusId IS NULL or @StatusId = 0), rm.RequestStatusId, @StatusId )      
ORDER BY rm.Id DESC             
END 
ElSE
BEGIN  
 Select rm.Id,        
 rt.Name as RequestType,        
 RequestTypeId,        
  ISNULL(CONVERT(VARCHAR(11), CAST(FromTime AS TIME), 100),'') AS FromTime,              
 ISNULL(CONVERT(VARCHAR(11), CAST(ToTime AS TIME), 100),'') AS ToTime,           
  ISNULL((select dbo.CalculatHours(FromTime,ToTime)),'') as Numberofdays,  
   ISNULL(CONVERT(VARCHAR(11), Date, 106),'') AS Date,         
  Comments,        
  RequestStatusId,        
  rs.Name as Status,        
  ISNULL((select FirstName + ' ' + LastName as ActionBy  from EmployeeMaster where UserId =(select top 1 ActionBy from RequestDetail where RequestId = rm.Id order by Id desc)),'') as ActionBy        
  ,ISNULL((select top 1 RequestDescription from RequestDetail where RequestId = rm.Id order by Id desc),'') as RequestDescription        
  ,ISNULL((select top 1 RequestActionId from RequestDetail where RequestId = rm.Id order by Id desc),0) as RequestActionId        
  ,ISNULL((select Name from Lookup_RequestAction where id = (select top 1 RequestActionId from RequestDetail where RequestId = rm.Id order by Id desc)),'') as RequestActionName        
  ,ISNULL((select FirstName + ' ' + LastName as ActionBy  from EmployeeMaster where UserId = rm.ManagerId),'') as ManagerName  
  ,ISNULL((select FirstName + ' ' + LastName as ActionBy  from EmployeeMaster where UserId = rm.CreatedBy),'') as EmployeeName     
   from RequestMaster rm           
 INNER JOIN Lookup_RequestStatus rs on rs.Id = rm.RequestStatusId        
 INNER JOIN Lookup_RequestType rt on rt.Id = rm.RequestTypeId          
 Where rm.IsDelete = 0 
 and RequestTypeId=@RequestTypeId  
 --and (rm.FromTime > Isnull(@FromDate,rm.FromTime) OR rm.ToTime < isnull(@ToDate,@FromDate))
 and rm.RequestStatusId =IIF((@StatusId IS NULL or @StatusId = 0), rm.RequestStatusId, @StatusId )      
ORDER BY rm.Id DESC             
END 
END
GO
/****** Object:  StoredProcedure [dbo].[Usp_Get_HrLeaveList_serach]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                
-- Author:   <Kiran Sawant>                 
-- Created date: <24-07-2020>                
-- Description:  <Usp_Get_HrLeaveList_serach>                
-- =============================================                
-- Usp_Get_HrLeaveList_serach 1,'ram',1,20200726,20200727            
CREATE PROCEDURE [dbo].[Usp_Get_HrLeaveList_serach]             
@RequestTypeId int,  
@Name Nvarchar(50)  = NULL,
@StatusId INT = NULL,
@FromDate bigint = NULL ,       
@ToDate bigint = NULL        
AS 
BEGIN  

SELECT         
(SELECT Count(*) FROM RequestMaster RM WHERE RM.RequestTypeId=@RequestTypeId AND RM.IsDelete=0 ) As TotalLeaveCount  
END 
 BEGIN
 Select rm.Id,        
 rt.Name as RequestType,        
 RequestTypeId,        
  ISNULL(CONVERT(VARCHAR(11), CAST(FromTime AS TIME), 100),'') AS FromTime,              
 ISNULL(CONVERT(VARCHAR(11), CAST(ToTime AS TIME), 100),'') AS ToTime,  
 FromTimeInt as fromdate,
 ToTimeInt as todate,         
  ISNULL((select dbo.CalculatHours(FromTime,ToTime)),'') as Numberofdays,  
   ISNULL(CONVERT(VARCHAR(11), Date, 106),'') AS Date,         
  Comments,        
  RequestStatusId,        
  rs.Name as Status,        
  ISNULL((select FirstName + ' ' + LastName as ActionBy  from EmployeeMaster where UserId =(select top 1 ActionBy from RequestDetail where RequestId = rm.Id order by Id desc)),'') as ActionBy        
  ,ISNULL((select top 1 RequestDescription from RequestDetail where RequestId = rm.Id order by Id desc),'') as RequestDescription        
  ,ISNULL((select top 1 RequestActionId from RequestDetail where RequestId = rm.Id order by Id desc),0) as RequestActionId        
  ,ISNULL((select Name from Lookup_RequestAction where id = (select top 1 RequestActionId from RequestDetail where RequestId = rm.Id order by Id desc)),'') as RequestActionName        
  ,ISNULL((select FirstName + ' ' + LastName as ActionBy  from EmployeeMaster where UserId = rm.ManagerId),'') as ManagerName  
  ,ISNULL((select FirstName + ' ' + LastName as ActionBy  from EmployeeMaster where UserId = rm.CreatedBy),'') as EmployeeName     
   from RequestMaster rm           
 INNER JOIN Lookup_RequestStatus rs on rs.Id = rm.RequestStatusId        
 INNER JOIN Lookup_RequestType rt on rt.Id = rm.RequestTypeId   
 INNER JOIN EmployeeMaster  EM on EM.UserId = rm.CreatedBy 
 INNER JOIN EmployeeMaster  EMM on EMM.UserId = rm.ManagerId         
 Where rm.IsDelete = 0 
 and RequestTypeId=@RequestTypeId  
 and EM.FirstName like '%' + COALESCE(@Name,EM.FirstName) --+ '%'
 and (rm.FromTimeInt >= COALESCE(@FromDate,rm.FromTimeInt) and rm.ToTimeInt <= COALESCE(@ToDate,rm.ToTimeInt))
 and rm.RequestStatusId =IIF((@StatusId IS NULL or @StatusId = 0), rm.RequestStatusId, @StatusId )      
ORDER BY rm.Id DESC             
END
GO
/****** Object:  StoredProcedure [dbo].[Usp_Get_Leave_By_RequestId]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================            
-- Author:   <Kiran Sawant>             
-- Created date: <20-07-2020>            
-- Description:  <Get Leave By RequestId>            
-- =============================================            
-- Usp_Get_Leave_By_RequestId 1,2           
CREATE PROCEDURE [dbo].[Usp_Get_Leave_By_RequestId]         
@RequestId bigint ,     
@RequestTypeId int     
AS            
BEGIN            
 Select rm.Id,    
 rt.Name as RequestType,    
 RequestTypeId,    
  ISNULL(CONVERT(VARCHAR(11), CAST(FromTime AS TIME), 100),'') AS FromTime,          
 ISNULL(CONVERT(VARCHAR(11), CAST(ToTime AS TIME), 100),'') AS ToTime,      
 -- case when (select DATEDIFF(d, FromTime, ToTime)) = 0 then 1 else (select DATEDIFF(d, FromTime, ToTime)) + 1 end  as Numberofdays,    
 ISNULL((select dbo.CalculatHours(FromTime,ToTime)),'') as Numberofdays,
   ISNULL(CONVERT(VARCHAR(11), Date, 106),'') AS Date,     
  Comments,    
  RequestStatusId,    
  rs.Name as Status,    
  ISNULL((select FirstName + ' ' + LastName  from EmployeeMaster where UserId = rm.CreatedBy),'') as EmployeeName ,   
  ISNULL((select top 1 RequestActionId from RequestDetail where RequestId = rm.Id order by Id desc),0) as ActionId   
   from RequestMaster rm       
 INNER JOIN Lookup_RequestStatus rs on rs.Id = rm.RequestStatusId    
 INNER JOIN Lookup_RequestType rt on rt.Id = rm.RequestTypeId      
 Where rm.IsDelete = 0 and rm.Id = @RequestId and RequestTypeId=@RequestTypeId    
ORDER BY rm.Id DESC         
END            
GO
/****** Object:  StoredProcedure [dbo].[Usp_Get_LeaveList_By_User]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================          
-- Author:   <Kiran Sawant>           
-- Created date: <17-07-2020>          
-- Description:  <Usp_Get_LeaveList_By_User>          
-- =============================================          
-- Usp_Get_LeaveList_By_User  1 ,1        
CREATE PROCEDURE [dbo].[Usp_Get_LeaveList_By_User]       
@UserId nvarchar(150), 
@RequestTypeId int  
AS          
BEGIN          
 Select rm.Id,  
 rt.Name as RequestType,  
 RequestTypeId,  
  ISNULL(CONVERT(VARCHAR(11), (FORMAT(FromTime,'hh:mm tt')), 106),'') AS FromTime,        
 ISNULL(CONVERT(VARCHAR(11), (FORMAT(ToTime,'hh:mm tt')), 106),'') AS ToTime,     
  case when (select DATEDIFF(d, FromTime, ToTime)) = 0 then 1 else (select DATEDIFF(d, FromTime, ToTime)) + 1 end  as Numberofdays,  
   ISNULL(CONVERT(VARCHAR(11), Date, 106),'') AS Date,   
  Comments,  
  RequestStatusId,  
  rs.Name as Status,  
  ISNULL((select FirstName + ' ' + LastName as ActionBy  from EmployeeMaster where UserId =(select top 1 ActionBy from RequestDetail order by Id desc)),'') as ActionBy  
   from RequestMaster rm     
 INNER JOIN Lookup_RequestStatus rs on rs.Id = rm.RequestStatusId  
 INNER JOIN Lookup_RequestType rt on rt.Id = rm.RequestTypeId    
 Where rm.IsDelete = 0 
 and rm.CreatedBy = @UserId  
 and RequestTypeId=@RequestTypeId 
ORDER BY rm.Id DESC       
END          
          
GO
/****** Object:  StoredProcedure [dbo].[Usp_Get_MyLeaveList_By_User]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================              
-- Author:   <Kiran Sawant>               
-- Created date: <20-07-2020>              
-- Description:  <Get Leave List by UserId>              
-- =============================================              
-- Usp_Get_MyLeaveList_By_User 3,2             
CREATE PROCEDURE [dbo].[Usp_Get_MyLeaveList_By_User]           
@UserId nvarchar(150),      
@RequestTypeId int       
AS              
BEGIN              
 Select rm.Id,      
 rt.Name as RequestType,      
 RequestTypeId,      
  ISNULL(CONVERT(VARCHAR(11), CAST(FromTime AS TIME), 100),'') AS FromTime,            
 ISNULL(CONVERT(VARCHAR(11), CAST(ToTime AS TIME), 100),'') AS ToTime,       
  --case when (select DATEDIFF(d, FromTime, ToTime)) = 0 then 1 else (select DATEDIFF(d, FromTime, ToTime)) + 1 end  as Numberofdays,      
  --ISNULL(CONVERT(bigint,CONVERT(decimal,(DATEDIFF(second, FromTime, ToTime) / 3600.0))),0) as Numberofdays, 
  ISNULL((select dbo.CalculatHours(FromTime,ToTime)),'') as Numberofdays,
   ISNULL(CONVERT(VARCHAR(11), Date, 106),'') AS Date,       
  Comments,      
  RequestStatusId,      
  rs.Name as Status,      
  ISNULL((select FirstName + ' ' + LastName as ActionBy  from EmployeeMaster where UserId =(select top 1 ActionBy from RequestDetail where RequestId = rm.Id order by Id desc)),'') as ActionBy      
  ,ISNULL((select top 1 RequestDescription from RequestDetail where RequestId = rm.Id order by Id desc),'') as RequestDescription      
  ,ISNULL((select top 1 RequestActionId from RequestDetail where RequestId = rm.Id order by Id desc),0) as RequestActionId      
  ,ISNULL((select Name from Lookup_RequestAction where id = (select top 1 RequestActionId from RequestDetail where RequestId = rm.Id order by Id desc)),'') as RequestActionName      
  ,ISNULL((select FirstName + ' ' + LastName as ActionBy  from EmployeeMaster where UserId = rm.ManagerId),'') as ManagerName    
   from RequestMaster rm         
 INNER JOIN Lookup_RequestStatus rs on rs.Id = rm.RequestStatusId      
 INNER JOIN Lookup_RequestType rt on rt.Id = rm.RequestTypeId        
 Where rm.IsDelete = 0 and rm.CreatedBy = @UserId and RequestTypeId=@RequestTypeId      
ORDER BY rm.Id DESC           
END 
GO
/****** Object:  StoredProcedure [dbo].[Usp_Get_StaffLeaveList_By_User]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                
-- Author:   <Kiran Sawant>                 
-- Created date: <17-07-2020>                
-- Description:  <Get Staff Leave List By UserId>                
-- =============================================                
-- Usp_Get_StaffLeaveList_By_User 1,2               
CREATE PROCEDURE [dbo].[Usp_Get_StaffLeaveList_By_User]             
@UserId nvarchar(150)  ,        
@RequestTypeId int         
AS                
BEGIN                
 Select rm.Id,        
 rt.Name as RequestType,        
 RequestTypeId,        
  ISNULL(CONVERT(VARCHAR(11), CAST(FromTime AS TIME), 100),'') AS FromTime,              
 ISNULL(CONVERT(VARCHAR(11), CAST(ToTime AS TIME), 100),'') AS ToTime,         
  --case when (select DATEDIFF(d, FromTime, ToTime)) = 0 then 1 else (select DATEDIFF(d, FromTime, ToTime)) + 1 end  as Numberofdays,        
 --ISNULL(CONVERT(bigint,CONVERT(decimal,(DATEDIFF(second, FromTime, ToTime) / 3600.0))),0) as Numberofdays,    
 ISNULL((select dbo.CalculatHours(FromTime,ToTime)),'') as Numberofdays,
   ISNULL(CONVERT(VARCHAR(11), Date, 106),'') AS Date,         
  Comments,        
  RequestStatusId,        
  rs.Name as Status,        
  ISNULL((select FirstName + ' ' + LastName as ActionBy  from EmployeeMaster where UserId =(select top 1 ActionBy from RequestDetail where RequestId = rm.Id order by Id desc)),'') as ActionBy        
  ,ISNULL((select top 1 RequestDescription from RequestDetail where RequestId = rm.Id order by Id desc),'') as RequestDescription        
  ,ISNULL((select top 1 RequestActionId from RequestDetail where RequestId = rm.Id order by Id desc),0) as RequestActionId        
  ,ISNULL((select Name from Lookup_RequestAction where id = (select top 1 RequestActionId from RequestDetail where RequestId = rm.Id order by Id desc)),'') as RequestActionName        
  ,ISNULL((select FirstName + ' ' + LastName  from EmployeeMaster where UserId = rm.CreatedBy),'') as EmployeeName        
  ,ISNULL((select top 1 ActionComment from RequestDetail where RequestId = rm.Id order by Id desc),'') as ActionComment        
   from RequestMaster rm           
 INNER JOIN Lookup_RequestStatus rs on rs.Id = rm.RequestStatusId        
 INNER JOIN Lookup_RequestType rt on rt.Id = rm.RequestTypeId          
 Where rm.IsDelete = 0 and rm.ManagerId = @UserId and RequestTypeId=@RequestTypeId        
ORDER BY rm.Id DESC             
END 
GO
/****** Object:  StoredProcedure [dbo].[Usp_Get_StaffVacationList_By_User]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================        
-- Author:   <Kiran Sawant>         
-- Created date: <17-07-2020>        
-- Description:  <Usp_Get_StaffVacationList_By_User>        
-- =============================================        
-- Usp_Get_StaffVacationList_By_User 1,2       
CREATE PROCEDURE [dbo].[Usp_Get_StaffVacationList_By_User]     
@UserId nvarchar(150)  ,
@RequestTypeId int 
AS        
BEGIN        
 Select rm.Id,
 rt.Name as RequestType,
 RequestTypeId,
  ISNULL(CONVERT(VARCHAR(11), FromTime, 106),'') AS FromTime,      
 ISNULL(CONVERT(VARCHAR(11), ToTime, 106),'') AS ToTime, 
  case when (select DATEDIFF(d, FromTime, ToTime)) = 0 then 1 else (select DATEDIFF(d, FromTime, ToTime)) + 1 end  as Numberofdays,
   ISNULL(CONVERT(VARCHAR(11), Date, 106),'') AS Date, 
  Comments,
  RequestStatusId,
  rs.Name as Status,
  ISNULL((select FirstName + ' ' + LastName as ActionBy  from EmployeeMaster where UserId =(select top 1 ActionBy from RequestDetail where RequestId = rm.Id order by Id desc)),'') as ActionBy
  ,ISNULL((select top 1 RequestDescription from RequestDetail where RequestId = rm.Id order by Id desc),'') as RequestDescription
  ,ISNULL((select top 1 RequestActionId from RequestDetail where RequestId = rm.Id order by Id desc),0) as RequestActionId
  ,ISNULL((select Name from Lookup_RequestAction where id = (select top 1 RequestActionId from RequestDetail where RequestId = rm.Id order by Id desc)),'') as RequestActionName
  ,ISNULL((select FirstName + ' ' + LastName  from EmployeeMaster where UserId = rm.CreatedBy),'') as EmployeeName
  ,ISNULL((select top 1 ActionComment from RequestDetail where RequestId = rm.Id order by Id desc),'') as ActionComment
   from RequestMaster rm   
 INNER JOIN Lookup_RequestStatus rs on rs.Id = rm.RequestStatusId
 INNER JOIN Lookup_RequestType rt on rt.Id = rm.RequestTypeId  
 Where rm.IsDelete = 0 and rm.ManagerId = @UserId and RequestTypeId=@RequestTypeId
ORDER BY rm.Id DESC     
END        
        
        
GO
/****** Object:  StoredProcedure [dbo].[Usp_Get_Vacation_By_RequestId]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================          
-- Author:   <Kiran Sawant>           
-- Created date: <17-07-2020>          
-- Description:  <Usp_Get_Vacation_By_RequestId>          
-- =============================================          
-- Usp_Get_Vacation_By_RequestId 1,2         
CREATE PROCEDURE [dbo].[Usp_Get_Vacation_By_RequestId]       
@RequestId bigint ,   
@RequestTypeId int   
AS          
BEGIN          
 Select rm.Id,  
 rt.Name as RequestType,  
 RequestTypeId,  
  ISNULL(CONVERT(VARCHAR(11), FromTime, 106),'') AS FromTime,        
 ISNULL(CONVERT(VARCHAR(11), ToTime, 106),'') AS ToTime,   
  case when (select DATEDIFF(d, FromTime, ToTime)) = 0 then 1 else (select DATEDIFF(d, FromTime, ToTime)) + 1 end  as Numberofdays,  
   ISNULL(CONVERT(VARCHAR(11), Date, 106),'') AS Date,   
  Comments,  
  RequestStatusId,  
  rs.Name as Status,  
  ISNULL((select FirstName + ' ' + LastName  from EmployeeMaster where UserId = rm.CreatedBy),'') as EmployeeName ,
  ISNULL((select top 1 RequestActionId from RequestDetail where RequestId = rm.Id order by Id desc),0) as ActionId  
   from RequestMaster rm     
 INNER JOIN Lookup_RequestStatus rs on rs.Id = rm.RequestStatusId  
 INNER JOIN Lookup_RequestType rt on rt.Id = rm.RequestTypeId    
 Where rm.IsDelete = 0 and rm.Id = @RequestId and RequestTypeId=@RequestTypeId  
ORDER BY rm.Id DESC       
END          
          
GO
/****** Object:  StoredProcedure [dbo].[Usp_Get_VacationHistory_By_RequestId]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================        
-- Author:   <Kiran Sawant>         
-- Created date: <17-07-2020>        
-- Description:  <Usp_Get_VacationHistory_By_RequestId>        
-- =============================================        
-- Usp_Get_VacationHistory_By_RequestId 3      
CREATE PROCEDURE [dbo].[Usp_Get_VacationHistory_By_RequestId]     
@RequestId bigint 
AS        
BEGIN        
 Select rd.Id
 ,RequestId
 ,ISNULL(RequestDescription,'') as RequestDescription
 ,ISNULL(RequestActionId,0) as RequestActionId
 ,ISNULL(ActionComment,'') as ActionComment
 ,ISNULL((select Name from Lookup_RequestAction where id = rd.RequestActionId),'') as RequestActionName
 ,ISNULL((select FirstName + ' ' + LastName   from EmployeeMaster where UserId = rd.ActionBy),'') as ActionBy
 ,ISNULL(CONVERT(VARCHAR(11), ActionDate, 106),CONVERT(VARCHAR(11), CreatedDate, 106)) AS ActionDate   
  from RequestDetail rd 
  where rd.RequestId = @RequestId 
  and (RequestDescription != '' or ActionComment != '')
ORDER BY rd.Id DESC     
END        
        
        
GO
/****** Object:  StoredProcedure [dbo].[Usp_Get_VacationList_By_User]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================          
-- Author:   <Kiran Sawant>           
-- Created date: <17-07-2020>          
-- Description:  <Usp_Get_VacationList_By_User>          
-- =============================================          
-- Usp_Get_VacationList_By_User 3,2         
CREATE PROCEDURE [dbo].[Usp_Get_VacationList_By_User]       
@UserId nvarchar(150)  ,  
@RequestTypeId int   
AS          
BEGIN          
 Select rm.Id,  
 rt.Name as RequestType,  
 RequestTypeId,  
  ISNULL(CONVERT(VARCHAR(11), FromTime, 106),'') AS FromTime,        
 ISNULL(CONVERT(VARCHAR(11), ToTime, 106),'') AS ToTime,   
  case when (select DATEDIFF(d, FromTime, ToTime)) = 0 then 1 else (select DATEDIFF(d, FromTime, ToTime)) + 1 end  as Numberofdays,  
   ISNULL(CONVERT(VARCHAR(11), Date, 106),'') AS Date,   
  Comments,  
  RequestStatusId,  
  rs.Name as Status,  
  ISNULL((select FirstName + ' ' + LastName as ActionBy  from EmployeeMaster where UserId =(select top 1 ActionBy from RequestDetail where RequestId = rm.Id order by Id desc)),'') as ActionBy  
  ,ISNULL((select top 1 RequestDescription from RequestDetail where RequestId = rm.Id order by Id desc),'') as RequestDescription  
  ,ISNULL((select top 1 RequestActionId from RequestDetail where RequestId = rm.Id order by Id desc),0) as RequestActionId  
  ,ISNULL((select Name from Lookup_RequestAction where id = (select top 1 RequestActionId from RequestDetail where RequestId = rm.Id order by Id desc)),'') as RequestActionName  
  ,ISNULL((select FirstName + ' ' + LastName as ActionBy  from EmployeeMaster where UserId = rm.ManagerId),'') as ManagerName  
   from RequestMaster rm     
 INNER JOIN Lookup_RequestStatus rs on rs.Id = rm.RequestStatusId  
 INNER JOIN Lookup_RequestType rt on rt.Id = rm.RequestTypeId    
 Where rm.IsDelete = 0 and rm.CreatedBy = @UserId and RequestTypeId=@RequestTypeId  
ORDER BY rm.Id DESC       
END          
          
GO
/****** Object:  StoredProcedure [dbo].[uspGetNotificationList]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================      
-- Author:  Kiran Sawant     
-- ALTER date:  21/07/2020      
--- Description : GetNotificationList       
-- =============================================      
--uspGetNotificationList 1      
    
CREATE  PROCEDURE [dbo].[uspGetNotificationList]        
 @UserId BIGINT=0,      
 @CountData BIGINT=0      
AS      
BEGIN       
      
      
SELECT       
(SELECT Count(*) FROM Notification N WHERE N.UserId=@UserId AND N.IsDelete=0 ) As TotalNotiCount    
    
IF @CountData>0       
BEGIN      
SELECT TOP 4  N.NotificationId      
 ,N.NotificationTypeId      
 ,N.UserId       
 ,N.SubjectEn       
 ,N.SubjectAr      
 ,NT.NotificationType       
 ,N.MessageEn AS MessageEn      
 ,N.MessageAr AS MessageAr      
 ,N.IsRead       
 ,N.IsDelete       
 ,N.CreatedBy    
 ,ISNULL((select FirstName +' '+LastName from EmployeeMaster where UserId = convert(bigint,N.CreatedBy )),'') AS Source     
 ,N.CreatedDate       
 ,N.CreatedDateInt,      
 CASE       
  WHEN  (CONVERT(VARCHAR(8), GETDATE(), 112))=N.CreatedDateInt       
   THEN 'at ' + FORMAT(CAST(N.CreatedDate AS DATETIME),'hh:mm tt')      
     ELSE       
  CONVERT(VARCHAR(10),  N.CreatedDate, 103) + ' at ' + FORMAT(CAST(N.CreatedDate AS DATETIME),'hh:mm tt')      
  END   AS DisplayTime      
 ,(select count(*) from Notification WHERE IsRead = 0 AND IsDelete = 0 AND UserId=@UserId) AS TotalNotiCount     
 ,ISNULL(RT.Name,'') as RequestType  
 ,ISNULL(N.RequestTypeId,0) as RequestTypeId
FROM Notification N      
LEFT JOIN NotificationType NT ON N.NotificationTypeId=NT.NotificationTypeId      
LEFT JOIN Lookup_RequestType RT ON RT.Id=N.RequestTypeId   
WHERE N.UserId=@UserId  AND N.IsDelete=0     
ORDER BY N.NotificationId DESC      
END    
ELSE     
BEGIN      
SELECT  N.NotificationId      
 ,N.NotificationTypeId      
 ,N.UserId       
 ,N.SubjectEn       
 ,N.SubjectAr      
 ,NT.NotificationType       
 ,N.MessageEn AS MessageEn      
 ,N.MessageAr AS MessageEn      
 ,N.IsRead       
 ,N.IsDelete       
 ,N.CreatedBy       
 ,ISNULL((select FirstName +' '+LastName from EmployeeMaster where UserId = convert(bigint,N.CreatedBy )),'') AS Source  
 ,N.CreatedDate       
 ,N.CreatedDateInt,      
 CASE       
  WHEN  (CONVERT(VARCHAR(8), GETDATE(), 112))=N.CreatedDateInt       
   THEN 'at ' + FORMAT(CAST(N.CreatedDate AS DATETIME),'hh:mm tt')      
     ELSE       
  CONVERT(VARCHAR(10),  N.CreatedDate, 103) + ' at ' + FORMAT(CAST(N.CreatedDate AS DATETIME),'hh:mm tt')      
  END   AS DisplayTime      
 ,(select count(*) from Notification WHERE IsRead = 0 AND IsDelete = 0 AND UserId=@UserId) AS TotalNotiCount     
 ,ISNULL(RT.Name,'') as RequestType 
 ,ISNULL(N.RequestTypeId,0) as RequestTypeId
FROM Notification N      
LEFT JOIN NotificationType NT ON N.NotificationTypeId=NT.NotificationTypeId    
LEFT JOIN Lookup_RequestType RT ON RT.Id=N.RequestTypeId     
WHERE N.UserId=@UserId  AND N.IsDelete=0     
ORDER BY N.NotificationId DESC      
END    
END    
    
GO
/****** Object:  StoredProcedure [dbo].[uspGetUserList]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================        
-- Author:   <Kiran Sawant>         
-- Created date: <18-06-2020>        
-- Description:  <Get User List>        
-- =============================================        
-- uspGetUserList        
CREATE PROCEDURE [dbo].[uspGetUserList]         
AS        
BEGIN        
 SELECT        
         
ulc.UserId,      
ulc.Username,      
ulc.EmailId,      
FirstName + ' ' + LastName as FullName ,      
--rm.Name Role,      
(select Name from RoleMaster where Id = (select top 1 RoleId from UserRole where UserId = ulc.UserId Order by UserRoleId desc )) as Role ,    
deptm.Name Department,      
desigm.Name Designation,      
(select FirstName + ' ' + LastName from EmployeeMaster where EmployeeMaster.UserId=um.ManagerId) Manager,       
 ISNULL(CONVERT(VARCHAR(11), BirthDate, 106),'') AS BirthDate,      
 ISNULL(CONVERT(VARCHAR(11), JoinDate, 106),'') AS JoinDate,      
gm.Name Gender,      
MobileNumber as Mobile,      
con.Nationailty as Nationality,      
Unlock HasUnlock,      
CanLogin      
FROM EmployeeMaster um      
INNER JOIN UserLoginCredential ulc on ulc.UserId = um.UserId      
--INNER JOIN RoleMaster rm on rm.Id=um.RoleId      
INNER JOIN Lookup_DepartmentMaster deptm on deptm.Id=um.DepartmentId      
INNER JOIN Lookup_DesignationMaster desigm on desigm.Id=um.DesignationId      
INNER JOIN Lookup_GenderMaster gm on gm.Id=um.GenderId      
left JOIN Lookup_CountryMaster con on con.Id=um.NationalityId      
WHERE ulc.IsDelete = 0       
ORDER BY um.Id DESC     
END        
        
        
GO
/****** Object:  StoredProcedure [dbo].[uspSaveNotification]    Script Date: 24-07-2020 17:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  Kiran Sawant    
-- ALTER  date: 21-07-2020    
-- Description:  Save Notification     
    
-- =============================================     
-- uspSaveNotification 1,1,'NewRequest','NewRequest','3'  
  
    
CREATE  PROCEDURE [dbo].[uspSaveNotification]     
 @UserId BIGINT = 0,     
 @NotificationType BIGINT=1,    
 @CreatedBy NVARCHAR(MAX)=NULL,  
 @RequestTypeId INT=0   
    
AS    
BEGIN     
INSERT INTO [dbo].[Notification]    
           ([NotificationTypeId]         
           ,[UserId]   
     ,[SubjectEn]    
           ,[SubjectAr]      
           ,[MessageEn]    
           ,[MessageAr]    
           ,[IsRead]    
           ,[IsDelete]    
           ,[CreatedBy]    
           ,[CreatedDate]  
     ,[UpdatedBy] 
	 ,[RequestTypeId] 
    )    
(    
SELECT     
 @NotificationType AS  NotificationTypeId    
,@UserId AS UserId    
, ISNULL(NT.NameEn,' ') AS SubjectEn    
, ISNULL(NT.NameAr,' ') AS SubjectAr    
, REPLACE(NT.MessageEn,'#FullName#',LC.FirstName +' '+ LC.LastName) AS MessageEn    
, REPLACE(NT.MessageAr,'#FullName#',LC.FirstName +' '+ LC.LastName)AS MessageAr    
,0 AS IsRead    
,0 AS IsDelete     
,@CreatedBy AS CreatedBy    
,GETDATE() AS CreatedDate   
,@CreatedBy AS UpdatedBy  
,@RequestTypeId as RequestTypeId 
 FROM  NotificationType NT   
LEFT JOIN EmployeeMaster LC ON LC.ID= convert(bigint,@CreatedBy)    
WHERE  NT.NotificationTypeId=@NotificationType  
)    
    
SELECT * FROM Notification    
 
END    
GO
