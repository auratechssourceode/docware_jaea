USE [SchoolManagement_Live]
GO
/****** Object:  UserDefinedFunction [dbo].[convertHtmlToString]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[convertHtmlToString] (@HTMLText VARCHAR(MAX))
RETURNS VARCHAR(MAX) AS
BEGIN
    DECLARE @Start INT
    DECLARE @End INT
    DECLARE @Length INT
    SET @Start = CHARINDEX('<',@HTMLText)
    SET @End = CHARINDEX('>',@HTMLText,CHARINDEX('<',@HTMLText))
    SET @Length = (@End - @Start) + 1
    WHILE @Start > 0 AND @End > 0 AND @Length > 0
    BEGIN
        SET @HTMLText = STUFF(@HTMLText,@Start,@Length,'')
        SET @Start = CHARINDEX('<',@HTMLText)
        SET @End = CHARINDEX('>',@HTMLText,CHARINDEX('<',@HTMLText))
        SET @Length = (@End - @Start) + 1
    END
    RETURN LTRIM(RTRIM(@HTMLText))
END



GO
/****** Object:  UserDefinedFunction [dbo].[Split]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[Split] 
(
@String nvarchar (4000),
 @Delimiter nvarchar (10)
 )
 

returns @ValueTable table ([ID] int,[Value] nvarchar(4000))
begin
 declare @NextString nvarchar(4000)
 declare @Pos int
 declare @NextPos int
 declare @CommaCheck nvarchar(1)
 
 --Initialize
 set @NextString = ''
 set @CommaCheck = right(@String,1) 
 
 --Check for trailing Comma, if not exists, INSERT
 --if (@CommaCheck <> @Delimiter )
 set @String = @String + @Delimiter
 
 --Get position of first Comma
 set @Pos = charindex(@Delimiter,@String)
 set @NextPos = 1
 
 --Loop while there is still a comma in the String of levels
Declare @sr as int
set @sr =0
 while (@pos <>  0)  
 begin
  set @NextString = substring(@String,1,@Pos - 1)
  set @sr = @sr + 1
  insert into @ValueTable ([id], [Value]) Values (@sr , @NextString)
 
  set @String = substring(@String,@pos +1,len(@String))
  
  set @NextPos = @Pos
  set @pos  = charindex(@Delimiter,@String)
 end
 
 return
end









GO
/****** Object:  Table [dbo].[AboutUsDetail]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AboutUsDetail](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Website] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_AboutUsDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AccademicYear]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccademicYear](
	[Id] [tinyint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[StartDate] [date] NOT NULL,
	[EndDate] [date] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsCurrent] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_AccademicYear] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AccademicYearWeekDetail]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccademicYearWeekDetail](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[AccademicYearId] [bigint] NOT NULL,
	[WeekNumber] [bigint] NOT NULL,
	[StartDateInt] [bigint] NOT NULL,
	[EndDateInt] [bigint] NOT NULL,
 CONSTRAINT [PK_AccademicYearWeekDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ClassMaster]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClassMaster](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[SchoolId] [bigint] NOT NULL,
	[Image] [nvarchar](max) NULL,
	[Note] [nvarchar](max) NULL,
	[IsDelete] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_ClassMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Country]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Country](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DeviceInfo]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DeviceInfo](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Mobile] [nvarchar](max) NOT NULL,
	[OTP] [int] NOT NULL,
	[DeviceId] [nvarchar](max) NOT NULL,
	[DeviceModel] [nvarchar](max) NOT NULL,
	[DeviceToken] [nvarchar](max) NOT NULL,
	[DeviceType] [int] NOT NULL,
	[MobileOS] [nvarchar](max) NOT NULL,
	[SmsStatus] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_DeviceInfo] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EmailTemplate]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailTemplate](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[Subject] [nvarchar](200) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[Body] [nvarchar](max) NOT NULL,
	[IsEdit] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_EmailTemplate] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EmailTemplateTag]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailTemplateTag](
	[EmailTemplateTagID] [bigint] IDENTITY(1,1) NOT NULL,
	[EmailTemplateTag] [varchar](50) NOT NULL,
	[Description] [varchar](500) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[TagType] [bigint] NOT NULL,
	[DisplayIndex] [int] NOT NULL,
 CONSTRAINT [PK_MessgaeTagMaster] PRIMARY KEY CLUSTERED 
(
	[EmailTemplateTagID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EmailTemplateTagType]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailTemplateTagType](
	[TagTypeId] [bigint] IDENTITY(1,1) NOT NULL,
	[TypeName] [nvarchar](50) NOT NULL,
	[ViewIndex] [int] NOT NULL,
 CONSTRAINT [PK_EmailTemplateTagType] PRIMARY KEY CLUSTERED 
(
	[TagTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GroupDetail]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GroupDetail](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[GroupId] [bigint] NOT NULL,
	[UserId] [bigint] NOT NULL,
	[Type] [tinyint] NOT NULL,
 CONSTRAINT [PK_GroupDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GroupMaster]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GroupMaster](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[SchoolId] [bigint] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_GroupMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GroupMessage]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GroupMessage](
	[GroupMessageId] [bigint] IDENTITY(1,1) NOT NULL,
	[SenderId] [bigint] NOT NULL,
	[MessageTitle] [nvarchar](max) NOT NULL,
	[MessageText] [nvarchar](max) NOT NULL,
	[SimpleText] [nvarchar](max) NOT NULL,
	[WeekNumber] [bigint] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
 CONSTRAINT [PK_GroupMessage] PRIMARY KEY CLUSTERED 
(
	[GroupMessageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GroupMessageDetail]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GroupMessageDetail](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[GroupMessageId] [bigint] NOT NULL,
	[GroupId] [bigint] NOT NULL,
	[UserId] [bigint] NOT NULL,
	[SenderId] [bigint] NOT NULL,
	[IsRead] [bit] NOT NULL,
 CONSTRAINT [PK_GroupMessageDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GuardianMaster]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GuardianMaster](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NULL,
	[MiddleName] [nvarchar](50) NULL,
	[Gender] [nvarchar](2) NULL,
	[Landline] [nvarchar](20) NULL,
	[CurrentAddress] [nvarchar](100) NULL,
	[PermanentAddress] [nvarchar](100) NULL,
	[Country] [bigint] NULL,
	[City] [nvarchar](50) NULL,
	[ZipCode] [nvarchar](10) NULL,
	[EducationLevel] [nvarchar](100) NULL,
	[Religion] [tinyint] NULL,
	[IdNumber] [nvarchar](50) NULL,
	[Notes] [nvarchar](max) NULL,
	[IsDelete] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_GuardianMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GuardianStudentDetail]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GuardianStudentDetail](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[StudentId] [bigint] NOT NULL,
	[ParentId] [bigint] NOT NULL,
	[Relationship] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_GuardianStudentDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HouseDetail]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HouseDetail](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[HouseId] [bigint] NOT NULL,
	[StudentId] [bigint] NOT NULL,
	[AccademidYearID] [tinyint] NOT NULL,
 CONSTRAINT [PK_HomeDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HouseMaster]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HouseMaster](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[SchoolId] [bigint] NOT NULL,
	[AcademicYearId] [bigint] NOT NULL,
	[Note] [nvarchar](max) NULL,
	[Image] [nvarchar](max) NULL,
	[IsDelete] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_HouseMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Link]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Link](
	[LinkId] [bigint] IDENTITY(1,1) NOT NULL,
	[ModuleId] [bigint] NOT NULL,
	[LinkNameArabic] [nvarchar](500) NULL,
	[LinkName] [varchar](100) NOT NULL,
	[Controller] [varchar](50) NOT NULL,
	[Action] [varchar](50) NOT NULL,
	[TypeArabic] [nvarchar](50) NULL,
	[Type] [nvarchar](50) NOT NULL,
	[ViewIndex] [int] NULL,
	[IsDefault] [bit] NOT NULL,
	[IsSingle] [bit] NOT NULL,
	[IsPage] [bit] NOT NULL,
	[LinkGroupId] [int] NOT NULL,
	[ParentId] [bigint] NOT NULL,
	[IsVisible] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_Link] PRIMARY KEY CLUSTERED 
(
	[LinkId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LoginCredentials]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoginCredentials](
	[UserId] [bigint] IDENTITY(1,1) NOT NULL,
	[RoleId] [bigint] NOT NULL,
	[SchoolId] [bigint] NOT NULL,
	[EmailId] [nvarchar](100) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[Mobile] [nvarchar](30) NOT NULL,
	[Address] [nvarchar](max) NULL,
	[ProfilePic] [nvarchar](max) NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_LoginCredentials] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Module]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Module](
	[ModuleId] [bigint] IDENTITY(1,1) NOT NULL,
	[ModuleNameArabic] [nvarchar](500) NULL,
	[ModuleName] [varchar](50) NOT NULL,
	[CssClass] [varchar](100) NOT NULL,
	[ViewIndex] [int] NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
 CONSTRAINT [PK_Module] PRIMARY KEY CLUSTERED 
(
	[ModuleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Notification]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Notification](
	[NotificationId] [bigint] IDENTITY(1,1) NOT NULL,
	[NotificationTypeId] [bigint] NOT NULL,
	[UserId] [bigint] NOT NULL,
	[StudentId] [bigint] NULL,
	[MessageEn] [nvarchar](max) NOT NULL,
	[MessageAr] [nvarchar](max) NOT NULL,
	[IsRead] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[WeekNumber] [bigint] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
 CONSTRAINT [PK_NotificationList] PRIMARY KEY CLUSTERED 
(
	[NotificationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NotificationType]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotificationType](
	[NotificationTypeId] [bigint] IDENTITY(1,1) NOT NULL,
	[NotificationType] [nvarchar](70) NOT NULL,
	[MessageEn] [nvarchar](max) NOT NULL,
	[MessageAr] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_NotificationType] PRIMARY KEY CLUSTERED 
(
	[NotificationTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Religion]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Religion](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Religion] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RoleMaster]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoleMaster](
	[RoleID] [bigint] IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](20) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_RoleMaster] PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RoleRight]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoleRight](
	[RoleRightId] [bigint] IDENTITY(1,1) NOT NULL,
	[RoleId] [bigint] NOT NULL,
	[LinkId] [bigint] NOT NULL,
 CONSTRAINT [PK_RoleRight] PRIMARY KEY CLUSTERED 
(
	[RoleRightId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[School]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[School](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Logo] [nvarchar](max) NULL,
	[EmailId] [nvarchar](100) NULL,
	[Website] [nvarchar](100) NULL,
	[ContactNumber1] [nvarchar](30) NULL,
	[ContactNumber2] [nvarchar](30) NULL,
	[Board] [nvarchar](max) NULL,
	[Address] [nvarchar](max) NULL,
	[Note] [nvarchar](max) NULL,
	[IsDelete] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_School] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SchoolAdminDetail]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SchoolAdminDetail](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[SchoolId] [bigint] NOT NULL,
	[SchoolAdminId] [bigint] NOT NULL,
 CONSTRAINT [PK_SchoolAdminDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SectionMaster]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SectionMaster](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[ClassId] [bigint] NOT NULL,
	[Image] [nvarchar](max) NULL,
	[Note] [nvarchar](max) NULL,
	[IsDelete] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_SectionMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[StudentMaster]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StudentMaster](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[SchoolId] [bigint] NOT NULL,
	[StudentCode] [nvarchar](max) NULL,
	[FirstName] [nvarchar](max) NOT NULL,
	[LastName] [nvarchar](max) NOT NULL,
	[MiddleName] [nvarchar](max) NOT NULL,
	[Gender] [nvarchar](1) NOT NULL,
	[BirthDate] [datetime] NULL,
	[RegisterDate] [datetime] NULL,
	[ProfilePic] [nvarchar](max) NULL,
	[BloodGroup] [nvarchar](5) NULL,
	[Address] [nvarchar](max) NULL,
	[Country] [bigint] NULL,
	[City] [nvarchar](150) NULL,
	[ZipCode] [nvarchar](10) NULL,
	[IsDelete] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_StudentMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[StudentSchoolDetail]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StudentSchoolDetail](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[StudentId] [bigint] NOT NULL,
	[JoiningDate] [datetime] NULL,
	[AcademicYearId] [bigint] NOT NULL,
	[ClassRollNo] [nvarchar](50) NULL,
	[ClassId] [bigint] NOT NULL,
	[SectionId] [bigint] NOT NULL,
	[IsCurrent] [bit] NOT NULL,
	[Note] [nvarchar](max) NULL,
	[IsDelete] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_StudentSchoolDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SubjectMaster]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubjectMaster](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[SchoolId] [bigint] NOT NULL,
	[Types] [tinyint] NULL,
	[Note] [nvarchar](max) NULL,
	[Image] [nvarchar](max) NULL,
	[IsDelete] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_SubjectMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TeacherDetail]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TeacherDetail](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TeacherId] [bigint] NULL,
	[ClassId] [bigint] NULL,
	[SectionId] [bigint] NOT NULL,
	[SubjectId] [bigint] NULL,
	[IsHomeTeacher] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_TeacherDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TeacherMaster]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TeacherMaster](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[TeacherCode] [nvarchar](50) NULL,
	[MiddleName] [nvarchar](max) NOT NULL,
	[Landline] [nvarchar](20) NULL,
	[Gender] [bit] NOT NULL,
	[BirthDate] [date] NULL,
	[HireDate] [date] NULL,
	[BloodGroup] [nvarchar](5) NULL,
	[IdType] [tinyint] NULL,
	[IdNumber] [nvarchar](50) NULL,
	[PermanentAddress] [nvarchar](max) NULL,
	[CurrentAddress] [nvarchar](max) NULL,
	[Country] [bigint] NULL,
	[City] [nvarchar](50) NULL,
	[ZipCode] [nvarchar](10) NULL,
	[University] [nvarchar](max) NULL,
	[HouseId] [bigint] NULL,
	[HomeTeacher] [bigint] NOT NULL,
	[Religion] [bigint] NULL,
	[Speciality] [bit] NULL,
	[HasAccount] [bit] NULL,
 CONSTRAINT [PK_TeacherMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserRight]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRight](
	[UserRightId] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[LinkId] [bigint] NOT NULL,
 CONSTRAINT [PK_UserRight] PRIMARY KEY CLUSTERED 
(
	[UserRightId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ValuePointDetail]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ValuePointDetail](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ValuePointId] [bigint] NOT NULL,
	[AddedBy] [bigint] NOT NULL,
	[WeekNumber] [bigint] NOT NULL,
	[AccademidYearID] [bigint] NOT NULL,
	[StudentId] [bigint] NOT NULL,
	[Note] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
 CONSTRAINT [PK_ValuePointDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ValuePointMaster]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ValuePointMaster](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[SchoolId] [bigint] NOT NULL,
	[Image] [nvarchar](max) NULL,
	[Note] [nvarchar](max) NULL,
	[IsDelete] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_ValuePointMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[AboutUsDetail] ON 

GO
INSERT [dbo].[AboutUsDetail] ([Id], [Website], [Description], [IsActive]) VALUES (1, N'http://www.iscgate.com', N'<p dir="rtl"><span style="font-size:20px;"><span style="color:#0079c1;">عن الجمعية</span></span><br />  تأسست جمعية المستثمرين في قطاع الاسكان الاردني في العام 1988 وقد بلغ عدد الاعضاء في منتصف عام 2014 حوالي (2700 )عضوا عاملا وهم يمثلون ما نسبته (80%) من العاملين في هذا القطاع ، يدير هذه الجمعية مجلس ادارة مؤلف من رئيس وثمانية اعضاء تنتخبهم الهيئه العامة لمدة ثلاث سنوات من بين الاعضاء العاملين فيها و تهدف الجمعية الى تحقيق رسالتها في رعاية العاملين و ايجاد علاقات مميزه مع جميع الاعضاء و التواصل معهم و العمل بروح الفريق الواحد لتلبية احتياجاتهم و تحقيق التكامل مع الجهات ذات العلاقه . و تعمل الجمعية على جمع و تحديث المعلومات و الاحصاءات الخاصة بقطاع الاسكان وتبويبها وتسهيل تبادلها ، و المساهمه في اجراء الدراسات و البحوث في مجال الاسكان وخاصة لذوي الدخول المحدوده . و تعمل على اعداد الاقتراحات الراميه الى تطوير التشريعات الاستثمارية و الاسكانية و العقارية .</p>    <p dir="rtl"><span style="font-size:20px;"><span style="color:#0079c1;">الرؤية</span></span></p>    <ul dir="rtl">   <li><span style="color:#44444;"><span style="font-size:16px;">خدمة الوطن من خلال المساهمة في التنمية المستدامة و ايجاد المسكن المناسب لكافة فئات المجتمع ، والارتقاء بتقديم الافضل ضمن اعلى المعايير و المواصفات .</span></span></li>   <li><span style="color:#44444;"><span style="font-size:16px;">تطوير الهيكل التنظيمى والإداري للجمعية عن طريق تنمية الموارد البشرية</span></span></li>  </ul>    <p dir="rtl"><span style="font-size:20px;"><span style="color:#0079c1;">الرسالة</span></span><br />  <span style="color:#44444;"><span style="font-size:16px;">رعاية العاملين في القطاع و ايجاد علاقات مميزة مع جميع الاعضاء و التواصل معهم و العمل بروح الفريق الواحد لتلبية احتياجاتهم و تحقيق التكامل مع الجهات ذات العلاقة .</span></span></p>    <p dir="rtl"><span style="font-size:20px;"><span style="color:#0079c1;">الاهداف </span></span><br />  <span style="color:#44444;"><span style="font-size:16px;">جمعية المستثمرين في قطاع الاسكان الاردني جمعية غير حكومية وهي منظمة غير هادفة للربح وتهدف إلى :</span></span></p>    <ul dir="rtl">   <li><span style="color:#44444;"><span style="font-size:16px;">تنمية الاستثمار والمساهمة في النمو الاقتصادي الوطني والمساهمة في تنمية لمجتمع المحلى.</span></span></li>   <li><span style="color:#44444;"><span style="font-size:16px;">حماية البيئــــــة والحفاظ عليهــــــــــا.</span></span></li>   <li><span style="color:#44444;"><span style="font-size:16px;">التعاون مع المؤسسات ذا ت العلاقة في القطاعين العام و الخاص في كل ما من شأنه خدمه وتطوير الاستثمار في قطاع الاسكان و تعزيزه والمساهمة في وضع السياسات الخاصة بتنمية الاستثمار بالتعاون مع الجهات المعنية.</span></span></li>   <li><span style="color:#44444;"><span style="font-size:16px;">تمثيل المستثمرين محلياً و دولياً و دراسة القوانين و اللوائح التى تؤثر على نشاطهم وتقديم كافة الخدمات لهم و مواجهة التحديات التى قد تظهر بين الاعضاء و الجهات الحكومية المرتبط نشاطها بالمدينة.</span></span></li>   <li><span style="color:#44444;"><span style="font-size:16px;">تشجيع و تنسيق الوفاق بين اعضائها .</span></span></li>   <li><span style="color:#44444;"><span style="font-size:16px;">تنظيم و تطوير طرق و اساليب الاستثمار في قطاع الاسكان</span></span></li>   <li><span style="color:#44444;"><span style="font-size:16px;">تطوير اساليب اداء الخدمات و الاستثمارات في مجال الاستثمار في قطاع الاسكان وتفعيلها بأحدث الاساليب</span></span></li>   <li><span style="color:#44444;"><span style="font-size:16px;">تبادل المعلومات و الخبرات بهدف رفع مستوى العمل في مجال الاستثمار بقطاع الاسكان وخدمة الاهداف المشتركه للاعضاء</span></span></li>  </ul>  ', 0)
GO
INSERT [dbo].[AboutUsDetail] ([Id], [Website], [Description], [IsActive]) VALUES (2, N'http://www.iscgate.com', N'The first interactive school counseling platform in the Arab World, which works to develop the behaviour by enhancing the human values of the school by providing communication between the school and home. It allows the Guidance Department in the school to develop educational policies, plans and behavioural evaluation tools. The program is an educational system for the conduct of school behaviour, which is based on the granting of behavioural points from teachers and parents of children distributed in groups based on their behaviour. 

The ISC platform provides the school with an important collection of DB and reports on the details of behaviour and behavioural life within the school, so that decision-makers can draw up plans and strategies on the most important aspects of school life, namely human values. The program enables departments to follow processes and plans on these important aspects by Guidance Department and the teachers, and to follow the extent of achievement achieved.
', 1)
GO
SET IDENTITY_INSERT [dbo].[AboutUsDetail] OFF
GO
SET IDENTITY_INSERT [dbo].[AccademicYear] ON 

GO
INSERT [dbo].[AccademicYear] ([Id], [Name], [StartDate], [EndDate], [IsDelete], [IsActive], [IsCurrent], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (1, N'2016-2017', CAST(N'2016-06-01' AS Date), CAST(N'2017-05-30' AS Date), 0, 1, 0, N'1', CAST(N'2018-09-26T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-09-26T13:11:45.120' AS DateTime))
GO
INSERT [dbo].[AccademicYear] ([Id], [Name], [StartDate], [EndDate], [IsDelete], [IsActive], [IsCurrent], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (2, N'2017-2018', CAST(N'2017-06-01' AS Date), CAST(N'2018-05-30' AS Date), 0, 1, 0, N'1', CAST(N'2018-09-26T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-09-26T13:11:45.120' AS DateTime))
GO
INSERT [dbo].[AccademicYear] ([Id], [Name], [StartDate], [EndDate], [IsDelete], [IsActive], [IsCurrent], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (3, N'2018-2019', CAST(N'2018-06-01' AS Date), CAST(N'2019-05-30' AS Date), 0, 1, 1, N'1', CAST(N'2018-09-26T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-09-26T13:11:45.120' AS DateTime))
GO
INSERT [dbo].[AccademicYear] ([Id], [Name], [StartDate], [EndDate], [IsDelete], [IsActive], [IsCurrent], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (4, N'2019-2020', CAST(N'2019-06-01' AS Date), CAST(N'2020-05-30' AS Date), 0, 1, 0, N'1', CAST(N'2018-09-26T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-09-26T13:11:45.120' AS DateTime))
GO
INSERT [dbo].[AccademicYear] ([Id], [Name], [StartDate], [EndDate], [IsDelete], [IsActive], [IsCurrent], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (5, N'2020-2021', CAST(N'2020-06-01' AS Date), CAST(N'2021-05-30' AS Date), 0, 1, 0, N'1', CAST(N'2018-09-26T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-09-26T13:11:45.120' AS DateTime))
GO
INSERT [dbo].[AccademicYear] ([Id], [Name], [StartDate], [EndDate], [IsDelete], [IsActive], [IsCurrent], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (6, N'2021-2022', CAST(N'2021-06-01' AS Date), CAST(N'2022-12-31' AS Date), 0, 1, 0, N'1', CAST(N'2018-09-26T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-09-26T13:11:45.120' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[AccademicYear] OFF
GO
SET IDENTITY_INSERT [dbo].[AccademicYearWeekDetail] ON 

GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (1, 3, 1, 20180601, 20180601)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (2, 3, 2, 20180602, 20180608)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (3, 3, 3, 20180609, 20180615)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (4, 3, 4, 20180616, 20180622)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (5, 3, 5, 20180623, 20180629)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (6, 3, 6, 20180630, 20180706)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (7, 3, 7, 20180707, 20180713)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (8, 3, 8, 20180714, 20180720)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (9, 3, 9, 20180721, 20180727)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (10, 3, 10, 20180728, 20180803)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (11, 3, 11, 20180804, 20180810)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (12, 3, 12, 20180811, 20180817)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (13, 3, 13, 20180818, 20180824)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (14, 3, 14, 20180825, 20180831)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (15, 3, 15, 20180901, 20180907)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (16, 3, 16, 20180908, 20180914)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (17, 3, 17, 20180915, 20180921)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (18, 3, 18, 20180922, 20180928)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (19, 3, 19, 20180929, 20181005)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (20, 3, 20, 20181006, 20181012)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (21, 3, 21, 20181013, 20181019)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (22, 3, 22, 20181020, 20181026)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (23, 3, 23, 20181027, 20181102)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (24, 3, 24, 20181103, 20181109)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (25, 3, 25, 20181110, 20181116)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (26, 3, 26, 20181117, 20181123)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (27, 3, 27, 20181124, 20181130)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (28, 3, 28, 20181201, 20181207)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (29, 3, 29, 20181208, 20181214)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (30, 3, 30, 20181215, 20181221)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (31, 3, 31, 20181222, 20181228)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (32, 3, 32, 20181229, 20190104)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (33, 3, 33, 20190105, 20190111)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (34, 3, 34, 20190112, 20190118)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (35, 3, 35, 20190119, 20190125)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (36, 3, 36, 20190126, 20190201)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (37, 3, 37, 20190202, 20190208)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (38, 3, 38, 20190209, 20190215)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (39, 3, 39, 20190216, 20190222)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (40, 3, 40, 20190223, 20190301)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (41, 3, 41, 20190302, 20190308)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (42, 3, 42, 20190309, 20190315)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (43, 3, 43, 20190316, 20190322)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (44, 3, 44, 20190323, 20190329)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (45, 3, 45, 20190330, 20190405)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (46, 3, 46, 20190406, 20190412)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (47, 3, 47, 20190413, 20190419)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (48, 3, 48, 20190420, 20190426)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (49, 3, 49, 20190427, 20190503)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (50, 3, 50, 20190504, 20190510)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (51, 3, 51, 20190511, 20190517)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (52, 3, 52, 20190518, 20190524)
GO
INSERT [dbo].[AccademicYearWeekDetail] ([Id], [AccademicYearId], [WeekNumber], [StartDateInt], [EndDateInt]) VALUES (53, 3, 53, 20190525, 20190530)
GO
SET IDENTITY_INSERT [dbo].[AccademicYearWeekDetail] OFF
GO
SET IDENTITY_INSERT [dbo].[ClassMaster] ON 

GO
INSERT [dbo].[ClassMaster] ([Id], [Name], [SchoolId], [Image], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (1, N'Grade 3', 1, NULL, N'only for dummy insert', 0, 1, N'2', CAST(N'2018-12-05T12:50:10.873' AS DateTime), N'2', CAST(N'2018-12-10T15:20:15.820' AS DateTime))
GO
INSERT [dbo].[ClassMaster] ([Id], [Name], [SchoolId], [Image], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (2, N'Grade 1', 1, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-13T19:41:05.233' AS DateTime), NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[ClassMaster] OFF
GO
SET IDENTITY_INSERT [dbo].[Country] ON 

GO
INSERT [dbo].[Country] ([Id], [Name]) VALUES (1, N'India')
GO
INSERT [dbo].[Country] ([Id], [Name]) VALUES (2, N'USA')
GO
INSERT [dbo].[Country] ([Id], [Name]) VALUES (3, N'U.K')
GO
SET IDENTITY_INSERT [dbo].[Country] OFF
GO
SET IDENTITY_INSERT [dbo].[DeviceInfo] ON 

GO
INSERT [dbo].[DeviceInfo] ([Id], [Mobile], [OTP], [DeviceId], [DeviceModel], [DeviceToken], [DeviceType], [MobileOS], [SmsStatus], [CreatedDate], [UpdatedDate]) VALUES (1, N'+91_7600018733', 150421, N'5E4F6045-FB65-48AC-8CE4-28D3476A7D86', N'iPhone 7', N'f6Eq2AhnbH0:APA91bEBJxKxrcc91GQedynqocNCCtQHgQwXuU-chHAgot7g0bAA6wqeLadlxZjlZxXUojZt9ayIx5U_WjIoFpqUCcqWQSMqzL2tLW9HUFcAHHwKFV88JH-PwHoFV1wLyNNyRqvjCmal', 2, N'11.4', N'E035-mobile number must start with 962.', CAST(N'2018-12-05T15:03:57.033' AS DateTime), CAST(N'2018-12-05T15:31:22.223' AS DateTime))
GO
INSERT [dbo].[DeviceInfo] ([Id], [Mobile], [OTP], [DeviceId], [DeviceModel], [DeviceToken], [DeviceType], [MobileOS], [SmsStatus], [CreatedDate], [UpdatedDate]) VALUES (2, N'+962_791482205', 281289, N'FFAA205C-93B2-441C-8712-64D55DB35866', N'Simulator', N'cWe6E1j0K78:APA91bGBkXfbCsHJqBrIQtYTyg62Cl3_bjHKMRZn9GtoxxgHXTJfGNxiqRPAD_Nsb4wdEtjUC-ftIB6snPcXgQeKW3l-qBHaLLxgRCyWhFa-6ugoxErNADrFBdr9KdrQGKkOWc__d8oJ', 2, N'12.0', N'I01-Job 275 queued for processing.', CAST(N'2018-12-05T15:26:40.070' AS DateTime), CAST(N'2018-12-14T11:30:17.133' AS DateTime))
GO
INSERT [dbo].[DeviceInfo] ([Id], [Mobile], [OTP], [DeviceId], [DeviceModel], [DeviceToken], [DeviceType], [MobileOS], [SmsStatus], [CreatedDate], [UpdatedDate]) VALUES (3, N'+962_780622500', 282299, N'883e67737160a9ee', N'Android SDK built for x86', N'cl5bDXz14bU:APA91bH2pqjIAMupRJrHFQxaxskGdYzRh8umSSpPI5-fN_JUwpDg0MuSH0TuseZM9teSml7KMCAJ8qP-dB2ALcQhW16ybJrkOD4RLcoWR9T6Pw1-S8PcksGFngk058Hk62KLFXT5tKeO', 1, N'OREO', N'I01-Job 275 queued for processing.', CAST(N'2018-12-05T15:38:42.623' AS DateTime), CAST(N'2018-12-14T11:45:57.867' AS DateTime))
GO
INSERT [dbo].[DeviceInfo] ([Id], [Mobile], [OTP], [DeviceId], [DeviceModel], [DeviceToken], [DeviceType], [MobileOS], [SmsStatus], [CreatedDate], [UpdatedDate]) VALUES (4, N'+962_795387536', 473772, N'88ce7cdd81c8965a', N'SM-J600F', N'dinazZzdiy8:APA91bEsxgHX9t9BAfHGvk_TBDZb-9rjHQI8Zt-6kZnMundPQAuYSEByhfKSYNFS0nD_GEnllFsU4waxGTpR6nuB9zDW14edd4kGTkNRcS6awjDKNjrHVlRYD0cwBEJX-O1LbkYFENSC', 1, N'OREO', N'I01-Job 270 queued for processing.', CAST(N'2018-12-09T14:14:36.487' AS DateTime), NULL)
GO
INSERT [dbo].[DeviceInfo] ([Id], [Mobile], [OTP], [DeviceId], [DeviceModel], [DeviceToken], [DeviceType], [MobileOS], [SmsStatus], [CreatedDate], [UpdatedDate]) VALUES (5, N'+962_799955119', 746485, N'1688A786-F4CD-4479-AD6C-3349D00A00D5', N'iPhone 6', N'cb8XbwtTQhI:APA91bGt-ou0dThPYT20_IuKGHddPz4YPAsEVfU5eGGujsPiQ7BV4IAqu_KD3BeCJ58zxYA--Kiv5nblaMqOFQpRIgGMczDt8tEyvjsyshgGzHjDoZM7lKasYeyxkVqOd2zxtb3qACIq', 2, N'12.0', N'I01-Job 274 queued for processing.', CAST(N'2018-12-09T14:52:56.397' AS DateTime), CAST(N'2018-12-11T13:26:02.483' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[DeviceInfo] OFF
GO
SET IDENTITY_INSERT [dbo].[EmailTemplate] ON 

GO
INSERT [dbo].[EmailTemplate] ([Id], [Name], [Subject], [Description], [Body], [IsEdit], [IsActive], [IsDelete], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (1, N'ForgotPassword', N'Forgot Password', NULL, N'<p><span style="font-size:14px"><span style="color:#2c3e50"><strong>Hello &nbsp;#FullName#,</strong></span></span></p>

<p><span style="font-size:20px"><span style="font-family:Comic Sans MS,cursive"><span style="color:#2980b9"><strong>Welcome to&nbsp;&nbsp;Interactive School Counciling!&nbsp;</strong></span></span></span></p>

<p><span style="font-size:14px">Your Request for the forgot password has been received successfully.We have sent you this email in response to your request to forgot your password on ISC.&nbsp;</span></p>

<p><span style="font-size:14px">Your password is&nbsp;<strong>&nbsp;#Password# .</strong></span></p>

<p><span style="font-size:14px"><strong><span style="color:#16a085">Thanks &amp; Regards&nbsp;<br />
&nbsp;</span><span style="color:#2980b9">ISC Team&nbsp;</span></strong></span></p>', 1, 1, 0, N'dbo', CAST(N'2018-02-08T00:00:00.000' AS DateTime), N'1', CAST(N'2018-04-05T15:22:55.637' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[EmailTemplate] OFF
GO
SET IDENTITY_INSERT [dbo].[EmailTemplateTag] ON 

GO
INSERT [dbo].[EmailTemplateTag] ([EmailTemplateTagID], [EmailTemplateTag], [Description], [IsActive], [TagType], [DisplayIndex]) VALUES (1, N'#UserName#', N'UserName', 1, 5, 1)
GO
INSERT [dbo].[EmailTemplateTag] ([EmailTemplateTagID], [EmailTemplateTag], [Description], [IsActive], [TagType], [DisplayIndex]) VALUES (2, N'#Password#', N'Password', 1, 5, 2)
GO
INSERT [dbo].[EmailTemplateTag] ([EmailTemplateTagID], [EmailTemplateTag], [Description], [IsActive], [TagType], [DisplayIndex]) VALUES (3, N'#ReceiverFirstName#', N'Receiver First Name', 1, 1, 1)
GO
INSERT [dbo].[EmailTemplateTag] ([EmailTemplateTagID], [EmailTemplateTag], [Description], [IsActive], [TagType], [DisplayIndex]) VALUES (4, N'#ReceiverSecondName#', N'Receiver Second Name', 1, 1, 2)
GO
INSERT [dbo].[EmailTemplateTag] ([EmailTemplateTagID], [EmailTemplateTag], [Description], [IsActive], [TagType], [DisplayIndex]) VALUES (5, N'#ReceiverLastName#', N'Receiver Last Name', 1, 1, 3)
GO
INSERT [dbo].[EmailTemplateTag] ([EmailTemplateTagID], [EmailTemplateTag], [Description], [IsActive], [TagType], [DisplayIndex]) VALUES (6, N'#CurrentTime#', N'Current Time', 1, 3, 2)
GO
INSERT [dbo].[EmailTemplateTag] ([EmailTemplateTagID], [EmailTemplateTag], [Description], [IsActive], [TagType], [DisplayIndex]) VALUES (7, N'#CurrentDate#', N'Current Date', 1, 3, 1)
GO
INSERT [dbo].[EmailTemplateTag] ([EmailTemplateTagID], [EmailTemplateTag], [Description], [IsActive], [TagType], [DisplayIndex]) VALUES (8, N'#SenderFirstName#', N'Sender First Name', 1, 2, 1)
GO
INSERT [dbo].[EmailTemplateTag] ([EmailTemplateTagID], [EmailTemplateTag], [Description], [IsActive], [TagType], [DisplayIndex]) VALUES (9, N'#SenderSecondName#', N'Sender Second Name', 1, 2, 2)
GO
INSERT [dbo].[EmailTemplateTag] ([EmailTemplateTagID], [EmailTemplateTag], [Description], [IsActive], [TagType], [DisplayIndex]) VALUES (10, N'#SenderLastName#', N'Sender Last Name', 1, 2, 3)
GO
INSERT [dbo].[EmailTemplateTag] ([EmailTemplateTagID], [EmailTemplateTag], [Description], [IsActive], [TagType], [DisplayIndex]) VALUES (11, N'#SchoolName#', N'School Name', 1, 5, 3)
GO
INSERT [dbo].[EmailTemplateTag] ([EmailTemplateTagID], [EmailTemplateTag], [Description], [IsActive], [TagType], [DisplayIndex]) VALUES (21, N'#URL#', N'Link URL', 1, 5, 8)
GO
INSERT [dbo].[EmailTemplateTag] ([EmailTemplateTagID], [EmailTemplateTag], [Description], [IsActive], [TagType], [DisplayIndex]) VALUES (22, N'#FullName#', N'Full Name', 1, 5, 9)
GO
SET IDENTITY_INSERT [dbo].[EmailTemplateTag] OFF
GO
SET IDENTITY_INSERT [dbo].[EmailTemplateTagType] ON 

GO
INSERT [dbo].[EmailTemplateTagType] ([TagTypeId], [TypeName], [ViewIndex]) VALUES (1, N'Sender', 1)
GO
INSERT [dbo].[EmailTemplateTagType] ([TagTypeId], [TypeName], [ViewIndex]) VALUES (2, N'Receiver', 2)
GO
INSERT [dbo].[EmailTemplateTagType] ([TagTypeId], [TypeName], [ViewIndex]) VALUES (3, N'Date & Time', 3)
GO
INSERT [dbo].[EmailTemplateTagType] ([TagTypeId], [TypeName], [ViewIndex]) VALUES (4, N'Workflow', 4)
GO
INSERT [dbo].[EmailTemplateTagType] ([TagTypeId], [TypeName], [ViewIndex]) VALUES (5, N'Others', 5)
GO
SET IDENTITY_INSERT [dbo].[EmailTemplateTagType] OFF
GO
SET IDENTITY_INSERT [dbo].[GroupDetail] ON 

GO
INSERT [dbo].[GroupDetail] ([Id], [GroupId], [UserId], [Type]) VALUES (2, 1, 4, 3)
GO
SET IDENTITY_INSERT [dbo].[GroupDetail] OFF
GO
SET IDENTITY_INSERT [dbo].[GroupMaster] ON 

GO
INSERT [dbo].[GroupMaster] ([Id], [Name], [SchoolId], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (1, N'Teachers Talk', 1, 0, 1, N'2', CAST(N'2018-12-05T14:52:34.873' AS DateTime), N'2', CAST(N'2018-12-10T15:27:53.650' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[GroupMaster] OFF
GO
SET IDENTITY_INSERT [dbo].[GroupMessage] ON 

GO
INSERT [dbo].[GroupMessage] ([GroupMessageId], [SenderId], [MessageTitle], [MessageText], [SimpleText], [WeekNumber], [CreatedDate]) VALUES (1, 2, N'Calling Teacher !', N'<p><b>Hi Sweta !</b></p><p><font color="#397b21"><b>Very good Noon !</b></font></p><p><img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAMDAwMDAwQEBAQFBQUFBQcHBgYHBwsICQgJCAsRCwwLCwwLEQ8SDw4PEg8bFRMTFRsfGhkaHyYiIiYwLTA+PlQBAwMDAwMDBAQEBAUFBQUFBwcGBgcHCwgJCAkICxELDAsLDAsRDxIPDg8SDxsVExMVGx8aGRofJiIiJjAtMD4+VP/CABEIAcICigMBIgACEQEDEQH/xAAeAAAABgMBAQAAAAAAAAAAAAACAwQFBgcAAQgJCv/aAAgBAQAAAAD0dMaPH6RdAVt01WnLnffHV8drLHQe0ZJp4wlDIU5zj0VreaxHyrf3IplPjnMz68kqIjQt5mACXreBwOa1rQNaa3FAyhCEBWrHObfJZ+vGvelq45O9B+Nb+7LXrc1o3WxZgqmteIc99c63mtIuV735OIqFZM5n2BLDE6RKVgs2WUHM1rNBwGBwDctQsgQB0WC28Z/Jd7uyvela45T9C+LehexHVVoZm9jzAA5q6S4T6vn+bzMR8sXnyo21S6SycdfSkWCJTJCswANYXoOsDrQcBpuXN7IDQC9auHbN5DyG7q/6TrXlf0U4o6D7EeV2xYHebEIXF3Y3lN6sAzN60j5bu7lxhrV1lE765lG8zMAkR6wOYAsOB0EINa02r25iBgAAy3RM/kPKLrhHRlc8r97cAdkdgvS3BZmAKzbdxx1t5v8AqMDMzW0HLl1czQ6u3yUzTryTCzYQ7Qcg82PnVt/5oAAYXgQBCFucGxi0EvQA2xtl8jpBe8U6CrXkvt7hPt3rp+VCFrN6Dgqnpd/r7t7MzNabuXro5rhlfSGRS/sCSizetQ/ye5ERmP8A2b6cPQAgADAaCEDW4NrDrQSwZbOmTyKlF6t9x1vy72nxv1l1vI1m8zMzMzmbVG9KXvrMCDbfzBcfOUGgkikUk7EkuC1rnjyFqkIRPcj6d9XVeglBwAQgC0ObSxhDoAQ24Jj8iXnoM+xa+5a7U50vTrGTLBZmZmZnGVt8B+rbnmZoOm7mW3udoNCJBIH3suQ7zfM3iq0knH7dX6Wdy97a0DABBoAWdzamEIA6LDbImTyQW9IMty19y12PUVo9WylVvMzMzM4H6j81vX7MzM1jdzFbNCQCHP71IezXrMzw0oBKHHY8+Xgf/YK1AA1oINAxkdGiP6LDoIbUEy+STl0fXXR8A5S7Iquz+ppaozMzMzM87evOAfVTMzMzG7mO2KJriMyVzkfZDrmQHwqTtZS9/Wu5JjJ0D63YEIQ4WHGdxZ47oIA6DagmPyRf+h6d6sr7kPuKpLO6fmJ2ZmxZmZ5m9TUZ39mZmtY380WlRtcR2Tusg7EcdZRvju+6DqTVk7tsHPN9ZOvwa0EvWBa1zNHABAEGWsJi8jpN0LTXVlfcr9vUtZfTszOzMze95ry86Hc+qszMzNN3N9mUNQbfark/djOGtVX4izOYR3sTpGC+elFRoKyQezPQxe80AGNytmjYQAAHVo7Y/JCY9AU71VXHLndFI2H0zNjd5mYLea8qrs6OtzMzMzbdzrJfGGuHi47J7C7HXYFH53Up27QN9zFqobzLYkG1C/rDuHo5UvUh03KmaNBAAAQWhti8kpl0BUHVlZ8ydwUtPOl5ybvMzBbzXlJYvoK5Zmb3myvP/wAzYcssqwqd6o9flmaDqPU3A3SRrmjySqVuDln+z8nWCE+vWm5SyxnAAAWC0MYPJiWX7U/VlY82d20VOekJwPeCEIWZvygsL0d1mC3mYX891YTpZNyaVd/oMmmaDpq4ulTw/q47yZxlBJJ2x6IMuSZAc2zF5QnMkZ0EsIA2bkf8mZVf9SdX1nzZ6B86zPo6aCzBmmjHm/KOx/RTWbFvMzXiG6usYr6t40b7J9oZoIdeZwrqmsb55mcMbPRt7jdY2EWrE2ILPIMj8dDosAQWZuN+UkuvenOr665l9EudZF0VLh4YM080wwXk9bXoEHQxi3vZPDUofllB8NUF6DXP6FbwAdcNeeHoW1+eXWNvEEd8IoLHpMvci0bFK306OR4OiwA1ZAo35Szi76c6srfnH0P56dOiXhWjyQbNNNMM8euqe0y8MNMEMY+ZYbXTpa8N5Z6+q70S3gNgjfzvAvnsiV9acNzrsOP1e5uKwtohsS6AmZjFGA6AWHVjCjHlVPrtpXqytOcvRnnw7oqYF1PL5xrWHmC8bu6ejkcg2NSZsRnAVTWzCemymtfyZ6Jp2xqjUbifB3Ovob23DujedLGxhZUcWsdRGIlZ8xmrmwRkOgBBqwhxXyzn10Uh1lV3PfopQjfekwkVXWa4NRDFPBIfHn0UtRFO8CpOHtNRVTXbApLHgVhwZEmMzWaAY3eyc/qK8mG5qwWVXBJ9MUMPV2OocbBYIyHQAByiWeKUqWWxFtCFInZoVLvcyGt8/XQtktkiAeWHTdk3lPh7UDHuL+aFmXQFllyBn8wG1kTkpkqdysb1ikIYxCeo4O+UKZYSxsi9pyJnR2qyxwIdFaD49JUpKd/JAAxPDRMLn71ghFjGMkZs1w8+uZWZj6D9PhK1e8DFvKzmr0KsySLIVKIDTXMbG1M0eYFF6+gXQE41H4ioW1FPHolqbbwQBa5mONg0AAdeHac5OKVqQGS6uKwkrev9yphA5mSbVl1cl8SJbeoB/wDaBqc3gYciPk9xx0p6KzOrpMthkk5+45aGWOMBPVPa7h0lKmlPCK5ksmGhY5rOkqZA4vsaCAINa+cspXbsgx1UuUFrFOudTvae26skLk+UxfbFQPLPfMVy9ou6ydQEMQ8nOd+u+nqFtqxTt3+RU/n9ywyMDZ370qz9AS5+IhVazRwTtzVeqYhu0TLo+AJYdA8SUpb3ItnHwOqEpgJUD2BsGFIpRb9N3dKo35t9uWAEiIPk6Ul5EKtlDIp4xuF2kPQq8hsJ5u84qUYfSm7KGuSzbZiLGtcwoo9bMiSpEaQh+AEBWgB8ViwYaEQ0lQAOTysv1Y6IiUHmdp1baM3YvML0pVHN0ccpssRM7S7JhrqCNFZ5hDi4o1JXnf5seyPRXLD3OnV3UnokUfktjlj2mh3n96RLwhKCWHxPCDDRDGCtjgxmWFelvoTWNSOVvVVObTj3nR6OlDa4Y7S5a2R5/c2apZkqjZWpnGJqgc39Pzbxt3JJjnJYaFEyoYuXYKlGsp/kjl1m9f76AABQQ+JpQNnjOzUJd0EIkxfoF6cwTm2YWtW1kT+uuF/RBaqZohKHQbSwP7qgqKrqmir7eLbb8M6CakxpZxacCZEnRYFrFWtOQ5npusStG9nemQSdFl54i7AI04Q9Qx9aoY9b7q9VWmKGnJFEqpTl7spCcuTNrjmnxJKEXLr7Mp0irTnu34Ta9vnFkJmxAXpE2RDnelZxSdMrMFvWB3aHsu9E6LLzxNJDh48MBDZczQxWZ0h6DyWRXHyGqmlj81QXp6vTlLWF7Mks2i1iJQojcSbyu1MCeHFIxc02FNEcEZCKN45XK1h5m8wvAiH2d6XgLAAHiYRo4wwWshspRxAlXcXouoXW5Rbg1Koykdo+J0WhPdbJsutLLOSt4UCBwXGJayUiaKz8y+lOsnTndguSUoPJc4ZxpoSd5hxnRHrgWAJQfElLpQeMe8iUjQRdKtm3fi1WJMSqf1scmpLPHWtpRC6guOnLMWJ0CFpTLdnFwubqXJSmmS1kSuyNP51eeS8ScQsFsGsG5e3ksLAAPiQl0aeMQtRuVIowxuz96iKxNjVH20gxSWFzlMmkclc5K8UdJJSHSICBSJZEazsDoNOEzWZmyy+EUXmbs7NZsQQC2q9XukighB4iFDCeaPNMD4kZoxIFne7gN5Uwydz+UNc4dDEyFpYGG6zIzQXSpiUhWoEA6A1op6Wkrqe3xSvmaKQ2FM0V4oby9YaMeazN+i/cAAgB4gaKEpPHmMroWzQmXD9WrOcXFUI3SZnORIG6MsqO1LQcYFyV1YMgx8PJPVVhV89s5NyxC4czEbZmqNshNagGYZmbGLNndt+iQSwleHut4sNM3jbH5WireaC9Z7n0hawBVKXbTe1MUZZWbpqXgjHHVyy3EJEml4B8t2LXN/8AHvMzLMLfjl4cEdMcbl5eFIogFh1ve82d1L6oEhAX4emAw9Tvek6NanrOb79BunJq3O2tZhSZtaY3Gou9dV4hiPm1QHtbNIuVEY/ZhNEXDUdecjsLddd8rprxr0z5oDLIJEIZma3mwi6O9ZCwFg8OTNjNNHvZbZIG+sJdv0a6clTW5ZrWikbazxeNLejV2NFWeKzb6rXVfCMtkqeBUJ1rWdTt/LFWdFdqurbL/NeqjszQdF7wYthEd1z6YEgCDwSenPYz9iEBglZVWybO9uzH1AuwOaIQNbLFWPpB5GDOE/MZv9R7o6JAvjlMoniU1i2wvzvjthXZKVcG5BdTta0IQsALewCO9AO5iQgK8DJbIIvG3R8cNBYZGVXz6Lsfvt1LWiGFMiQNcfY3m1390bmTzQ4SL7v9H4Fe4UUmSlR5rrflbzUmc2sgTufyUuU73oOBHve81ij096mKCWT4cXhIUnG6FTOH0tielUKcFPUXpuJOEb0NjZZM2V3ATcm0P6CrvnThqtvZ6Q3W8m4WoTt7bGPOjgBbadkY7H82J1IdiFveg73gRHesN/gLLL8XXtkS1k1jkM4QxNvLE9rrv9A6lgJT63J5vYDUXWkhqiUXrI7Gh76jxc7WDMDC0ZZzFB6/8+OQJFL7Fx2MpiLLNaDsexawWtCz2lnwAFBpvz+iFXti9NtagINI25ODvbCWTK1b+oDGmVoZJbF5vfvQFYSHjLsacTBRE+mQxpliUDFG1VecARx1n0mPaYJV6sQ9aDhm95mbefcsoJZQfM2i4nMTGCLI3FqTqHBCtc3z0J5qqeHtjmKT2S7nqpInaxxeR09tRYXQ1lAijXccwKNNRp4qxitWTtzNFOWKNYIlGyj9BzYsCLXRHqyAJJevEom2Vy1wjkCQv1UtSo0byb3uBATqLxmEwqLshauQWXOZlA65jjPvoa9oVd0omD86YiQNzIzqLDmZxiOo4uVhUciMUi0WisVijSPXoz2MDRJYfGawTVDNKuo7OQJmGoaShtXuEj9Y1yJsaWtOSkQNkSg0Fg0Dap1PbPtmzchqNrTmzi+16Nvbm1tVTaWHBSVzFCwGaCAWFYUxxKH9JTN2MOwHjgTqNM8uk9/3XerE4t72yPKhSMRhAyl76/vihHFIVVkVcXB0dHkrmCATi0p3MkdRSKwsUIV1zZH2pHAo4i0EIdaBrQcAEb9IMK3njIFC4HKl728Wr0Dbr28yw5Iii9PW/MX2WuA1xwsDsuC1IlUqFWNjckdYPz/ILbt59eBJiJe5YaTU7OmISEkszeUEJZBQTJC/i0AHjegTyElcqUOUvmtkdjyqr3CXO611cGJBJHh/W5GYVUlxWEvb4EoVCTNzG2RGDTizpWnbhF43TR6Fg4jEiyhM8eQPL4NM3tzW3Inh62DRfi6ovGzoEmepbZMwNnnSyuneTYSulE/nEilthOkhUpqqjDta78mDUrKQBHB+k62UyaQ6QGp28gDa72IPYq0jpyhNBnWRJkiMRwx4c7qUSJvS+Zk3ObpVcl2VfFkbt0jNjDcxPGayqk7q10epEcrIZkIVbkqZmmEaXymHyh2HohqUIhgTE5M3cRVamrGaPL1BpKNEiI0EzaycaKKBwIaascpNPo23JddZqjDTzjzzdmKiES2QVF5uRlKnSFqrq7iWNZdkr3Q5OkMGWEslA2CmZKMSZhjDipIayFISxACSQU+P4Qlh4RMEaocHEQGuS9YQvlLpyVSNeacYeccaY7+V9YpE4czqa1bBenF2HirQzAljUKCzVbehkzUgRU1KrYKQNjY2okhZJRYSVEsVgzOGDjFBh5qw1gnHQ/MPP9xrVb64t1m269GnK4f4TEOM0ti3LUnsmn8kTICyhkKRBwZexmLdmCJTV2pmzkrzAaL2mb2tE3txgtDI50g5hpxxig48mNdE8Y3bIkLZAedaXh622PVfoJwojgmzbAkjguXTq0B7wGtDMU6LIJLJSEPOJzmKJS9/KRFjXgNUqM1hACSEEcgnP3l73rOHE3DDjlq1yg2rUdj2Kv69gMBrKqfc27nOk6wVqFSp8taRpi1CQxUEABgEboYix7RJ4MisQ9q2J3EQmT6Lw484zAExqkfLOmhdVdBSw0wxQNVI3hnc3IDE1tqdv4vrX3Nlbnz3Ely1RKbMkGJTQEkaTSEbdGVz8XokpWeop4bfdJzYjQmJW58PTYeYkQFuB5Ee5WpTgz//xAAbAQACAwEBAQAAAAAAAAAAAAACAwABBAUGB//aAAgBAhAAAADrc9gIJ+kHCN5SfcrJRuJzpCuS7hFy8ct2bSlVnooZKyt11Czgw1wnOKyu7u75uWRyNCk0bBlllRrdLYkWMRVkIveZkV3z80jUakLErCSYB6NkbVgxuSigLHoMYRGWLPUbn1LSs5Uk5J9OS9IA28sWMhFtsjI8gBGJ1IBLaqScTV0ZJppTgTMYYn9IdD7smZhsWI1IBDqqSee6HRkj8yT0BB8qE9O6iNjWIA6PNszrW4aknl+j15IGHTZNled5516HZcvQ0QuHm1oUvTVBJ47sdmSY/MadZd2RPK6PA6nRmZT+pVSzybUIrRG5mJ8P7DcmQfMZ8Xd792IKA789jv1nRkq9EuUNrtD8vitnodAy/Nc7N0/R7ABMEE+XPt942SQWanhjMBl83xPd6nVz1aPJYH+y12tYOR5QG+tZNMkOytcKC9OfxvrWvzUC+DwvRb9LbZ5Z+DNfoupU1VIyrsVNLL0smTy3rK0ZxFVY8CunWfIkhX3utRtlyNklKNqwvL89+km4MqrFeHDelYhz+l19pS7uSNkgrLQ5Fh8y+ryTk5WCMqanuDmbVldwpcjqgxRaBtLeT1xqc5LhqQO5Q48qG7ZdwpcZUlBWmxEFSyVnUajKi3eHQ/Tt62mS5d3Dq5Sq05RZJCmRbQgVS03w8nY9Gcl3Lu4soQjNChuVLBStK03Yirf5/D2evcl3LKW1vPHo4o17jDDg4OV2m3s6FRLm8PHu7dy5cjLvMeQ9eQtKsm9zSzzNzB5HJ9PgxjpLubsD9JyoUbc5nn9ne6TOfWTjc3G/Rt39PJysPQXq5Kcavb7eT4kG69WrVt6WgsuzVpa+cxnKyCDKx8niM17yemu52bDN5/nZOQu7Psaj9g6NHPz8JGwTK0tzYqbxu729xHS7anxvW7+PDhBSOPOi3D1elKG5bNQiHDd29xXIdAeLP1nS5MwV8qXTNnrN/SXhz1bLw8/P3O26glnKHJz+2R3Z2tNfKsw07v7eyPEf068rw69B7XZJVHCg8DX0zqSSE/B5HzOUGbG+g87zet6vT4TJ1/o7l0c0Z1FfI8/7aOKSqkPnf//EABsBAAIDAQEBAAAAAAAAAAAAAAMEAAECBQYH/9oACAEDEAAAAOScBHaXVmJKuqqts7yoQWdXiVKrFV13Kpe1jOQC+qrVqypIwTAXIMA5KlVVD6zUimljuWPOpkUATeaXMUQ35BNa5y0kqC6jEiulWWrxZZIqp0bxhTW8B6moYpd+aDUkromkV0sZsg96kiIOrJSGSYx1I6SXlbg5zJOgaRa1jtFES5Jz66MkQwyC39dhl1fiXzUB5qugaRa1jtFES5JzCvSRN3oh5bEY9u0LyCdgXXWy8WRfIjNEHu5JyzPSQvYAEC0v3xg35flyXz12SXAZCZvQpoknKabkj3sgKD8rI92OP6Lj8m3ir8M2pa9AM5YqVfA1yCssyX7V5vzvk6opYQWvUv58XydTVoyWSEtZpPo9/wC3/FPObgPddacvzXP1suDU567HC86uOTV5CC2bpex+t19M8l816Fg6Pt21/ELVomgH9kXPiRjUu7xMUTVVxqdfyOl+pey+n73muYrilfZLdFknmePKR1NCu7rcxfHZ6C5dKOEOwV/rXwo04zNm87xqoa9XoGpqXoaxVuklvASPMwhOz0wJE2Tr8fhoSpQxasV1c1sXHbHoikk7roSy9FWXVN3uViVJWQ6rF1LsglsupNJ3Wu2wtu4UnEsvTJ1uRzZUmQ1UkkvYwTVkuZM6xgoxFoHvnApcrhqVJUBmTUuXYi1UzUp1gWrJD6dD6hzh+QHJUmQ5hZdStC3M1dWzsipD1LMxyvQ9Dh8mSSpatTIX9qmgxAwRn0no+jzUxKg5GrONbutpcWSSSlZLO3hZilGG0RCtrb3Vvp9Xxrrm0w8rn9FcGLlSlpOt31OBzMP6c7XQKJdZHns9NxWA6R2r8mj1vU2JZdcCPPXw+koAK2X8dvoDwC3Or3MhSxgs4/KXI72nWOnqVnnBrxq8zGStTIyDzuxuP2Ls8fkKzIq3s/qeTx2m2tGP175oX+PxZU3UwO977YuQviSVmX0i8rEqVvU+o7glfIoc+nWM1szjhePyxy7qSred4lSpVau/p+7oPEX5uOtjkC9R2r43lFZJJV1ruJ86SrklQ3su1d4WzzOo3zvMJ+zbQ8HVyQJbk6voPFSTNXWryb//xAAlEAAABgICAwEBAQEBAAAAAAAAAQIDBAUGEQcSEBMgFDAVFhf/2gAIAQEAAQIBJXaxE9NiitGUFPJwqUcUFJGoCnz3swstF42Y0QU9rTs7r1661p8sPD4xBMAcojjVPIBCiQbCmdfGta1/TWhN8SBr511SLAWCZxVoywp4UKIcUHdDVcHiHfsOuvmbJF/b8Qw/nT5YgmUMQKvHJw4yLPw6VED8G2pk0a8a0Y1rWvnXxqWQkeDGvB+OnSwTYCcVcMtE4LFKXF6MpRqpS+nr6/V1HXr16mjrXva5rusWrPqQMPEwYiVeOTC4xLPQ6VIR/BoU2bevBlrWta0NfcrxJL+NiJxzhXjMBPCxSp4vTk6tUwkF10DGhvfgi45e6WSjLXjXh8YeU4sTTAHJo4xLOyeFN9mnXxrWtDXjWviWQk/wLxZCaJYrxmDc4llVFxQ/JmGmlEjxsH40ZaBCdI4RTd2HDMLxrzqSWIFNGLCAOUBxgM5EgqgvOteFKyPmJ/niBz1j/JQ0NaGteTLxLBiT41rxrXiwVNVJcryywpocFCMFDSFiiEgi8a1r41nUnhZrl2fwdX/cksPKwGKlATyeXGSc4KSVT9293nvJyl9iNC8J5Nr7DWtDQ1oaGpniV/KyTLRJRXDJGJoWKZEQlNLFCH/58tSOI2ec53GlfrWtaGpIw8rAYuVeOTi40LMymLq/rM+RclyhS9mSAgRkYJmDD/jQ1rWhoTvEr+VmJxPHAK5VNBiqCxkMdQoDeBfy5qe41a5akwo33KGHiwGMlVjk0uNRmQmlWF8cnZvKkGoJIyJLBNk0vi3LfOvjWtTvEr+G92YnCWuCVwucDKrEgs7QssdDxF/Lmx3BkVxfwlliAtDxkVhcmDjMsvKcVd8bzy8Wg0aCWmWFsm10J3DMjGvOvOrDxL/lZCaJxQRlD83xWHHHIQWMcDv8+Z3Kp3hqJ/CWMOFqWNiqPkdHGZZYU5MAvOVTUt/kOKqGiAuojtLj9VrjvcX5V40Za+Zxamfw1q0E8WBQBnYmgxSjHk8jBZY2HP58vryl/guENa18SxiBWwx0UieRhxqWVlYCJ8ckmGA422mHBfxSxqokuc6zNKT7OKM41518SyITfB/wtBYiemAM7KcFlALCy5HCjxoOfz5VXyk7w5E+9TBiIthSOV+VZde8cnlAsCi/GbsLegoOlo2qGpZFpXZjj8l11TbhOVttgOfDfxoSgQm/B/VqLAWIrxnBTycFeXHo5JJwYyHP58inzS9gcT+EwYmWVWOR5QkMtRRXZnPVG+JDCuC7TjKTWQMWip7qRyBUOKCwS+8SfU5jhFUmd+1t7zKCRN/laCwE8oIzY7A1iEni0ckksYwF/wA8zHNS4bH8JCMwzm6vQ0muS9NlyMEyNjyfm1BHLU0EBKZjWTU6k+OmLYnjtC4SR6OsaT4khInfytRPFgIQzkWAcEMcWs8lksYyFnve9/V6M4I/4vFk01KipGaWBFtZK1V0vHbT5mRrO1cZZbSkTJGVVdhTKYg0eJ8J11VLcipcUThmsRJQkEkTv5WwmlYiAM5OwNZQT40RySFDGQ4e+29ke9+XSrS/io5eOMcYrwdTs21lTFBpXBlp4P45jkVjsBpDtxeR8xW8/TtY1Hg6eWbjKTGtOCO6hT4SU0/k/i4E07Mq4ZyLEOiuLjkclEo8aDx7UveyPsR7BCIvjNO+3Yj3vfh9MSCgNpJORYdkeFkVHxlwxF8H8c+VOMX1TJvs2sJXHWI3NEqHigInlTVstLebQTZt/nW1WOvhImfyuDmCzEAZyLIOnCLjwuSiUMZO2RCFkdcsb2Su2+yneH2TPt22St73sZQhL9jmNVlSH7mtrOO1pwiP4P4uKu4gHb4zkuLYngbK2ZFdxtDEhVq6t6Mn/bVeuXsvJ5L7AfCRO/ldHNFmmAM6FkCFefH45KG8ZD6et1Io/GzWTmyNRuL4mbVLQ8DBHvzvmCvwiyyPFcZxaNGW62JE/iLJDzY8+c5GXye5zC9zc5zxyPkalYRhqEYo2k88bkVVWufYsSrSwYWy2s31rFfVuOpeQJ/8rsTBZHAGdCeTYrhgauRh+nHrlq8cFgyDU8brsSR4kvuDj6K4CMGCBGXjb702/pIDiScU8olJmw2GZbQ7d+/s9hiQxhdUk4FujLJ5kmuKxTWqkCGyQdOQdTHkPIRCdSJ38F8mr5Qm8mO271//ANVIyJeVKyBV6eR/767Z6TRTXlVyrVxg0M3CYK0qWu6deVU8jVnI7b7TijLxve7lVtPosrbsZsCK6yiY6bGSGw6pk0GkzNz9EdyOziVj0lY7IqplfVWLrk03nYMZpJm4b4qI5BwyDK5v8DhehyM3Edd/zvwfh/CUA6mQGVrJpaHaxu1FSbEm6Q23GC15NyutxbinuO7WEH3EqL4ujvHqe+hWbERiG6604sSI8/C3WVtONuJeChFdyJESVOdSTrKI8FFyH5aEMNEFG4phjSicBqq3J38FZsWZ/wDZ9VVbEkzGorNtce2MtwNnVPQGrkqI4Z2wOXBPl64nQpcW7xUnKo4g024kwfi8GRBw+OJ9ZXuKdkMNqFnH49rOScQcUs3Q8Fhs8raqTq7aXKILTMjykrdjsoSoKDqqKLtQI3Cq1zgf2pIbOsEpTKCBBhFtcqUZxHXwyrDX2ZVtJr7WDItQgoYv8cv+NYbNqi6wHHcIgh6Uy618Xp5Q5UYvjXFjcyDl0FpkLVW1xhbNnh+W8arDhLIZVDxdcmPcP1EuSFqtpEVtogpTi2WCSZmahuKcz+BxDrTrYqP3fvTY/wClcy1LMyNKpAZHGkiWxexYbVQLQ2kxTSdsqMrGEmHRBW6IIZG+27ByNijsSvrZtXX0TLqJUaJFjoQ+54y3jHI8McSoXg4kq5SKVmkkybB63cJtgiM1qfdq4OzCjMjDKjlfe9je/LyTGuiyfJocRPym75lxEJdq40qJJSdiH3WUpNw2VvOwVMgyNMxxlHSUVe5IKdWtRYkQ3zl/panrX1aaU3yzx6+WI1tRT5nWxoZx2oSWPWYWtapEiohGojBnq1tLDk5s/szGyPYLwYYR6+roUElww82WStoEcWyY5ICBaLgIWaUuEaXThqQpK3HHlRyQUlUKwZvJIrmLIfqlzP3fmINOGFr5FhxeFsegfkRWphEx0CzdPUmZDQiUqeu4Nm6vLnlWwtyPjq58n8H5SNl8Qg42sPhPjhV0hfGmTFeu3qhiZDbGVzYTZJS0lL6EpZPtuSJBRDQHGrKzn5xP5IXyxg15mlnn1L+mgy9LrsJuCdf+Xqp39Jvm+bxvnKKQqW441TW+dSOYlcrv8wTc1M9+OHrz7JzwfnfhoS0mmSTISfCPh9lVGVHLS0EPqPM5jBOzStE3keXMkouEXH+uUl1FSsOouuKm+LouCNw7CpsaC6qqydOxylyFuR+g3TcNa3fd7vYpxcqXb2/I73K1LzLlucoRsaIa8EeJ5BXWf1vx1+XRLBnKKL444nf6lW9XLIWYgVNEubItTiNvoU+44m+dt23YtovJKSzSK5wbM9k4oyWbxJmYgUGZj0OtVXfgsJMzlzHcsU52tr4smescqikCSlBAjBGO3bx2HCtiD+NmNgvqaTies0oYMYc8uVEuafI/+0OZNuo95Iy5uSRTnXG0sNlo4SaxMHHI4dUl7spSlEpx/wByF9u63X3P1fsenXb+Qo4zyOLkNha5ZdxoMbF//PeQ8d6Dtokes0mQ0SQQ4wsz8H4PwoH4IbL4sQpZiYmCFCpP8CUkv0nUlUoitIdJ0dVvPXa8sVma8y/6YpmIMLGQuNjqsGbyi8aCQ+byIcU6lilVQIxdps3pUZmF7DNSuerUjSpRdepI87G+8aVQWnyfkgXzZEkGLAQgoQlst+o3XLhzJHcjXe/7DU6RPVj/APwSeP2cKaxpmjRVppiaMZo1TWBqJYkhaegI9TnK2bjstSARa1169OvQ2co45z/iP09u299u/btvwkcU2/0YUC+rEML1OTFUZRDo0KoE4w3jp0dnRwokWuTXKqmq1NebBvLt15G7lX+6iYc2+Xgc9t5QJyRMIE316uFKOiYM356gUf8AMsplo/krvIkrk4uSrXNP+6tc/lSPX6+u96G/GunCU/52fhIIF8TygrUqWGjMRRiMRMBMFMH83Tt73FG9+9aw4TiVoM6eNHbMprONuJV7FSVrQv3+xCli1cxOZUSIwzO7VyRIy920U8YUFzZNmu/cvn21o0ThPk97NjQ2CHCK/naS8GXxKEYiTJJPiKOO5BuqHsKzN38xViKtLSjWajeU4pZrchtEShKPPG8ZulRyefCpEKelCkKGXWFfVWyKSfyXkarRy0xtmrw7PccyTC3DzXCgYiz0cmyXuvXp06evp6+uiHEBA/ghvwlPw4KdZhxJEQbPjxMRnpJB/Sgs1m8pxUtzDsTbcU4qU5M5eybFMsq8hVHfrf8APnUrVkxPfflN2ZuSai4my1GscdLyhGa0WKSchwXNwSjPRt67ezv3Ne978b4r8H4PwR7BAgXwYpjUpalBJkfEjqvEkH53sws1G4bynDrqxBEpBvO5ZaznWA3VYxamPU4iW2mJbZCxkX7pip8X/jcwSbmBWr66pbDNxbZNmifrXXWvOgQ4aYB+dnYR3zLxv4rULTqQTQUOH39EJJmN+VGs1m6p1SURmSR0WZHyu+6vfEmT3DEGTKYeEk5hliFLgzzMutegqay+ygt4wCuarKzyWY42E+d9t+D+yPhGJ5PxYt1UC7gvqKzbtW5e97jrU+JRMG4fEkklpUtBgk9PWszWs1qdV6a6KxObe04bzvIlkZjjWDJborFDpj0712kochv13K9yK5zHT3slENI+da/gQ4caPxs/BFLeburV10aSK5xTuzHsN+ScQ3S4pdJrqqSc9qxSs1STZCpb1mp+JaWd3GvSeg5Sq6mpyXArXAWq6FS444UAkdTS34Uhxvq4nnabtp7G19uxGS56Eed734MvvixowZg/EWqdSqtlR5KDBFFNt905ShpIhm4eBTLmH/x9u2/kzFgVzFygs1ZyI5T1ln2R4LWWtPgUhyzroyExLNa+pEhr81Y/rSiNPsJT0mZaw8j5dgd4sqgd3sldsgaQC/spWJQ/J+H8ZkHMnSJO+htoUuW8ZKNAIRDcDao2K/8ASt8lf9ahbNa1S/6TlgufKrZtBJfYRQR6VNlPqqe0yqyzPGb5h+M46/YWyFuvPXZ2s65VkRTJUNhOYVz+AOYzSxGi/wARVWpORNt+NjWgQ39V8M0/NpeS7FphnGlY7IiK8IT2UG3VJMRgsQjspdljcjEHqkm0NlNiZPH5ELPf9v8AMrG5WN2JWl9NbgvyayPHkPw85r8oRBm01jcUEBnHG45sJielbCWpTJtobrTXDXSuYvKwq447d4zdwl+hWjv4198V1o3vfiws4NYy2RpCDslRKWXip8byq71IC1xyMo6o1UuhOh/w1Ur+HPcfSOO3sKdp1LKamZGtYmbo5Jh8mXE9m2U+44zBwvErLOWIbNKzk7OTszjIKCjM3/CBABeNOpmtqR10pt6lewt7jp7jJ7jR7AHsWdhmojHDNR52NwKvWiS9Y49Ts8Q1VOtT+Xy8xn4xY8e2WIOtIZp6CBVHBVAXXLrlRfyHGVEOE5Tv4i/x5I40f4/XXElqJFwmJx7CxNiEVMtS7lLhk3VwcViRzCgo1BxJttsRCT47OHLBq7EY0Y146es0vV72KP8AH1bYtW37f0dz8Kvf99FkuW6mOh21pMgt3YlFawlqVV9nYaFrJKCcS4aPz/k/G3QJxpikTC/I5TPYlJwywxtmgTVor0xER0NSBb0zlLjFBNqaqMhiVa2eTYzLM0sOVn+WmPCQ7CXFCzfUprp06dfPbv23sbI2AQMtb9kdmcpCyc0lKAh5uRWZhA5Pi5KSYq0sKgf5sirhQp9JWSidYYbQSSbSyOw1pQmUEqqJj0pRrSkuRGqj0WeTWGV49SQ8YYgNwEoMKI0xPG+7pSa44ZwjgnX/AIP85xnqaST16G312k2FEfg/C0HISpUBBkCINssVDGLQKGjqZlC9aR7VuUmcm3RY6mQIakBDrLviwnypkC7iSWFLS4ldQVR/l/5xwVV78FBTm5WN1uPsUqWVeNjf6IL+xrrZud+w6vzHHCZYZKT6VVyqddMqqVD9TSST1B+CWkqAnmH6dOLN43FoY2PN0vtx5DYXJu6Sz4oPEWr9jlKPybGyx24x9aQkRwpXa2oTxODQsGlezEm3N/1fnOLZIjNqJMJiEg1k4hYSFLM1oNUOWTfq6atjIEkmpCeseGpZpUwbZDZL9/vS80tURVeqsVXfhaxGPSvUKa6O5V2Ccim3ix/ltxKGPve+npXDk49M46mcRYnxaaG5KVsqBIclKuTyP/oyukStqhLrlhJNw2mJMuM1skqaNBthbpHpSfzHBiu+zbrqiS0TL8xDSGOwNBsqYUx6x277YLwYPx09ZISELjuJluPqdN1k2kb2R7IyUSiPSCCmDgMMNlmOUWEg4yoxwzg/kROxLJYlyTUibCq/T2Qvfq9ZAx09f5zjLh/iQEuKMz9akSpxqbhmrbjpva10Jvr1MuptRGAY0NdPX1IESB20YcXjLfbI7LEuSWcobuUPF42SiMj3sgZ5VdKdNzv232g45XR2XijJhJBrIJGyWa/abxPE/wBtEOy3VRkueszeEhVIPzGwuvXUKqFQDZNXt/Qcj3m+b7bzR+Na6ggSfX0IJPUpOFhK+V7HEYxOE4iS1eqztvl+v5FYnkZAjaGY2Z5H/rNHFx+Dg0XBoVMgR62LXhajjHDOMUdCfX4JXYzT4S77fd7TX2ddcU3XtKKQTvfWhvqcRdWqmOlXQqoE1aiSrs9Jc5K6dSSSSIgQIiS7Hq5UbIOQJtTTHVqrnoVg5aSJSFsx58Dkfja9IMDk5MTComKMVSGSbSSU10AgR9t60SUA3SXr1mz+f0G0qOTKQajUTcmxNUOJ6jaOKbZOfo93uS97B1Mh2MzStK1yS5FyFc2HkLb2iGiIiSEkkJassWiUTIT4fOVCk0MnFZOKTqGQjjuCGRlbiW0oJJIJtiBCSSnEOJQXs9mtGO/bZeO3t9nQm+vR116WkMxVJ9KXzlpUY6m36vQoicJ8nic9hOGtS5KLp/K7YRV0rsNRfBBIIMeJAaCQ4FBXhQdLLlqRRkGxehISEghXE0EBwkg/CghL4bMhpwnzgmYIGFeE+FiSYgkgGFAxog8aVNn4cNJ6UkxtJhQkDKxkcXX/xABcEAABAwIDAgcJCwgFCwMDBQABAAIDBBEFEiExQQYTIlFhcYEQFCMyQlJykbEgJCUwM2JzgqGywRUmQ1NjdJLRB0RUg6IWJzRAZGV1k8Lh8EWEsxfD8TVVo9Ly/9oACAEBAAM/Ae57xm6vxXvap+nl+8r4BwVd/vGUfah30PRK/Pbgn6b/AGrwPqWv1ShxuKfvh9i8Djv/ABGP7i4rhhX/APFg31zlcly94Un0EfsV4T2e33A9zfuxMkjjcbOkzZRz5Rcodz8uf0h02HxG9NgcD6ifpqJBkb/Df3fgn9S+EcU9L8Vamn+jf7FyXdQV/wCkfFf+Hwe1Zu+z0MV6Kb94cvDYIP8AeQ+6ssEQ/ZtTThFHf9UgnD/Wfek3odzwfb8QQij3lL1D2r3pVfTy/eX5u8Fv+JSr32z0V+enBL6R/tXgT2Lb1FcvFTfbWu9iHe+Nn/eLP/jWThlVDnx2L/5SuRJ2q+HUf0EfsXgz2e1W7pPxGfhrhFJ+rw6snP1i1g7kWAYNW4jJr3vES0ec7Y0etSnCsRxeoOafEKs3cd4Z/wBz7vwT+pWxDE+se1e9J/on+xeD7Av85eLf8Ph9q8HVdTF7wk/eHL31gf8AxP8A6VyGfRtXwRR/RDug9wj/AFb3tL6Pc8H2/EDuWopur8VajqvppfvIf5OcFz/vWVe+2eircMuCP0z/AGrwR6wtT2r/APUf3wrJBjf/ABFn/wAYRi4dVA/3zTn1uJWkg61fC6I/7Oz2LwTuz2+7Hue/P6U8X5qLCIoh1vdfucXT0GERnV5NTMOhujAvyPwcwui3xUrM/pO5TvtPu/Av6ke/sT9Ie1e8qj6F/sVovUv85eLfuEXtXg6j6i+Dn/vD17+wL/iX/QuQz0Gr4KpPoh7kFdH+q+9pfR7ng+34r3jN1fir0NUf2833l+bvBX/ikq99t9FfnhwRP7d/tXgj1hanqKt+UL/2sle9cY1vfEG//GEJOF1XJz49AB9VcqTrPtXwTQ/u7F4F3Z7fiR3fylwu4Y1/nTtY3qzH+Xc/yx/pUbD40Rr2RD6Kn2+xa+78C/qXv3Euse1XoKn6F/sVo/Urf0mYt+4xe1eCqepi+DHfvD18IYAP94n7i8FH6LfYvguj+ib7u/8AqnvaTqWq8GOv4r3hP6P4o/k+p2fLzfeX5t8Ff+Kyr3030V+cnBOXza4s9YurxFWJ6kT39Yf1hZMFxeabTi60l/1IwpavG2TvaQ2sxJtTH0Nz5bFcuT0j7V8D0P0DV4F3Z7Vp8V3pQ1VR+qgkf/C26LqDHKh22Ssj+7f8U3CcGr64/wBXppHjrA0Tq3hbU1j9e9aR7r/PlOX4jwMnUh35iWvlD2le8aj6F/sXgkP/AKlYv+5x+1eAqfRYvgj/ANxJ7V8I8H/+IP8AuLwTPQavguj+hb8QGgkmw3krg5gsr4KJj8SnZoeLOWIH0964Qud4HDKCNvTneq7OBXYPTvbvMMpaf8S4KcIC2NlT3rO7ZFUci56HbD8f73k6u5yB1/Fe8pvR/FB2H1J3cdN95B3B/gs0bsVluvfQ9FZsa4PfNronfbl/FeCcFqrd+n/bHFPdwP4UGEcp1RUZbm22NQtlwkNBuxtNmPpPzaLwsnpu9q+BqH6ELwLuz2/F958EMXk397Fg+ucqycFqqT9ZXv8A8LQu8+Bk0QNnVc8UXZ4x9i4rB8TriNZ6psTT0RD/AL/EeAk9Fe+cS9Me1fB9T9C/2LwXqX+cjGb/ANjj9q8FU9TParYP/wC4k9qviXB/9/k+4vBD0Wr4Mo/oWez3eF4FSOq8QqWQRN2X2u6GjesS4Tvlo6LPS4be2XY+b0+joW7uZiiy1z2rEMEdHR12appNmQm7mD9mfwVFitHFWUczZoZBdrh7DzH421LL1dzkDr+K94z+j+KIw6qub3nm+8h+QeClt+KSkr339VcZX4JJ5lZb2O/BeDcruQYZ7f2gFCHgnwqA0vWuH+AK08Id+jfTj1ZV4aT03e1fA1F9F+K8C7s9q0+K4jgfK39bURN9Wq4vgTTnz6md322XLweh5myzu+6F3hwJwppFnSxund/em/xHveX0V74xH02r4PqfoX+xeCPYr/0kY2f9kZ7V4OfqZ7V8DN/eJPavhjg6P9tl+4gBb5rV8G0n0DPZ7rBOCQ73kk42tc3kwN1y82fmWJ4/XyVNZMZHE6czRzBu4Iu1XPsWXuG1j2oWAcDkPbl6jzKp4K1+SYufRSHw7Ob9o3pChqYWTRPD45GhzXDYQdh+M95zej3OQOv4r3hN1fivg2e362X7yH5F4MD/AHrN+C99fVTTWULfNqoH9jiYz7V4E9i5SPvi/wDaAgzg3jrfPxD/AOyF3rjUsVvENN9xq8NJ6R9q+BaP6P8AFeDPZ7fi8uCUEPnzvd/C3/uuK4EYV85r3et5RxPhxLTMueKZBTt6z/8A6TaKipaZuyCBkY+qLfEe9pfRXhsR9Nq+Dav6B/sXgyOpN/8AqFjfP3q32rwc3osXwLGf20ntV8c4Oj/ap/uLV3otXwdSfQM9nuf8kMIEdM4flCsu2D5jd8n8lNUTSyTSOkkebve43c4neVm2rVX7lu1F7iB4w3c6yO5B8b/yxQcwHY5v2hFk4wOqfyH3NKT5LtpZ27vjLUcx+b3OQ30viveE/Uvg2b6WX7yAw3gkwts7v+dwPRde+/qq/CXC4P11LMR1xPY8exeCPYuUrMnP7e6+CqyMeXVh3rjaEGcKcQHNJD91q8LJ6RQ/IlF6B9q8Gez2/F3dhUP7OR3rIC4vgjgzf9lb9uq/yj/pTY48psuLukPoxG/4fE+9pfRXLxH6RqthlWf2D/YiYb8+Vf5xMd/dG+1Xhl9Bi+AofpZPvL84OD37xUfcXjdTV8H0n0DPZ7gDan8IuFGIVmbNE2UwwdEcegWpPPZW7ljdOkaC07dE46FPgka/p2rjhmAsdvanMs4Xv+G9SRNjqoJC18Ja5rxt6+xR8KcBp67Tjh4OobzSN2+vb8X7zm6vx7nIb6XxXvCbqHtQOGTEbpZdPrK1HwO6aqf7y99fVXe/DLgi/nmmYfriy8D2BcsK8E55pbJ9TUGnto6ei/x2v7FbhRiHpw+wK0z/AEivgSk6nfeXIPZ8XmxegZ5tIPtchh/Ammm2cRhIf6o7o1fDCWqP9XpJH9shy/E+9pfRK5WIem1fBtX9A/2K0buxX/pHxr91/FeBffzGL834PpH/AHlfhBwe+mqfuLxvRb7FagpfoWez3D8O4NYtVMNnR0j8vWdEfB33kp2zfr9icdQNDsT8mYDZ4wV3NG51jdPon8s3geN20KPjSc92kadDv+6iccrhu+xNpWBt7XF2O/BMmizN0N9nMU3i3gC9r2HtCHB3HhSzyWoq6zHHzXeQ/wDA/F+9Jez2rRchvX8VbD5uoe1AYZNb9ZL95e9eBvTVTe1e+vqq3Cfgh+9/9QXgz2LloiiqS79c4rjsWp7a3npD/DFmX50V39190LwzutfAlL9f2rkHs+LvwlY3zaaL8ShQf0YTOGh/JcMY+uA1Di8ZrPnQwt7OUfife0vola1/0gXwXV/QOXgj2L/ORjX7p+KJgd9G1WwCn9N/3l+cXB/6Sp+6vG6mr3nS/QM9nuHDgTimXexn3kONjZ80n7E1z728c5mnpUEDXNd4r9W9CDjlDdXaAkaE8xVRV1LqcMIc2/I2EFcJo38W+ll4txDc9rs7bbOtYtgUpZWU8kVz2EdB6EKiEG/K3dY/mhJHa+u1vQU+InmdoR0JzJOMZvOqBfyd+rP5JvCDDhhlZJ7+pGckn9LGPxG9X+J97ydi0C5DOv4r3hN1BD8nSfSSfeXvTgZf+1z+1e+vqr86eCP73/1BeDctqHetRbZxn4LNiVMbXszN/DCB+K/Oqv8Ao4vuo53X0K+Bafrf7VyfV8Xn4XzDmbEP8K73/o9ghv8AKvpI/UL/AILvfgfxttaismf2N5P4fE+9ZfQK/wBP+kC+C6r6Fyhp6d8ksjY427XvNgO0rg7QcOMVrZ6+MU8sGRkgDnAm6wfG43d4VbJuSBze1Nbg1NDmbxgc67b6+MvzlwDrqvurlP6mr3pTfQs9nuDU8EcXjAv73J9SLO95fKjNnLvtskbNX3zxqrpaQ4hVgh9jlZbW4VbjFfPBHGY+KjM0rpHBrAB1/YmSOjramnAqANCdvavB6qjr+TNGHdi/IOJOFOPAz+Ei9LeFna1zVmdfn7mmV2z2dKrMIrYqynkdFLE7MyVqouGNFlcWR18Q8NCD43z2dCur2t7v3u9ckdS5DOv4r4Pm6gvg6T05PvL3pwK/eZ/avfbvQX50cEf3sfeC5EnZ3Pek/pn2ISTF3mUo+3KF+dVf9BD91eFd2exfAsPpye1cn4vjeGdUP2rR/hC4rBMEpvOnLz9Rn/dd5cD8GitY96h5/vOV+PxPvSb0FYVv0gVFhWA1c9ZOyGMxloLj4xO5vOVV4zUZQZGU7Pk4/wAetFxJJKsQ4ZuzRVUMbKmKaSJzR4zBqLb7hVFXimG/lmYPFMXhtTbW0gty/wCaDy4tILS1hBGoK97U/wBEz2e4jqaeWGQXZKwtd1FTy1M5dioZFrkys2817rhlwXxKjkjLKujbIDx8emQX1DwVS1eHtidHfTQqmw+uMrwXuHiX2BAWXIsEXo12COkaPCU5ztXKduDvavJOicCraG6sLA9h2KbD6iOelkkgmYbtfG8ghf0pcKXiioZp64sZymhjWtF98r9Fw3wOKb8rYjRTNls5zeXM9hHM7QKQi+QFRjxgQopvEdfo9x4B/Z7VyQuS3r+Kvh83UF8Gu9N/3l4HgSP2s/tVqkn5i/Obgh++D7y+UWqtTVA/an2BZ6Suk83imfZdW4T137vF91cs9TfYvgaL6ST2rk/F8bw4qR/t1vtARmxXCKNvk07v8brfgu9aOmgH6KFjP4Rb4n3vJfmUPAhs1LRMikxCcB1j4kLdznDnO4LEsfqzVYlVz1EpPjPdcD0RsHYr9ITc2tvYmh92NNxu2/Ynwx3jIbpzaKR0hcRYnXTYUZoRhc77/qCfurwEH0TPZ7pv5OqSQDaM7VaBm3YslWWOFju6fcMlicxwu1wsU7DMRqYgDkDzl6kH7dHBEbU1X3G3SsY4VV7aTDoL2PhpyPBwt53H8FhfBLBYsMo2aA3kmPjSvO17lxjhGDyGavP4J79nJbz86H/5TGG+rTzrjHcW+2bn5+74B/Z7VyQuSzr+K+D5upXw8+k77yOXgSPnVH3l4c+gF+dHBH97b95EZulaq9NUdM7vYFkwCqk/WVrv8LQF+ctb+6xexeE7G+wL4Hj+lk9q5J+LM/DmTpxE/wDyI4r/AEo0NJtDZaSP7cx+K8G7fYKWvxbEKup1kmqXutzcyINnDRGWMPjdbMLg7iq6xzRHTeFJT8sAEf8AnqURBaLNPNz9SLDpqE+kqIp43WLHg35io8ZwTD66PZPTsPUdhHr902spZYCS3jG2uNy72x2owWomnimpY4peMhfkDmSbHW6N674LXPcXkDRyI7lkyNhJKFd4UN5Vk9pzRjRTtdZ0b/UsTxB4ZS0FRK8+bGfxVTV5KjHZO94v7PGfCO63blhmBULKHDqWOmgb5DBt6TzlG4jHjucgXWb4g2/OKyNuewJ5RRbqNCEKhoB8a3r7l4Xdi5IXJZ1/FfBs/Uj+Tb87j95Wl4Fj6b7yvU/VCtwp4J/vY+8rX61r60O9ZrbBM9ZeC7emqmK/OGr/AHSL2LlfVZ90L4Ib9NJ7VyT8Xx/DlnzsS/8Aur8qf0wSv2iKrnf/AMpth8UGtJO5S49idZVQx5YpKmUsH1jopZGXdLkO4LH6OENpiJC3UfyKxagNqvDaiJzdr4m3aexRiQvhkLX+UHC32KOpNzdhO46jsKcDfdzosdm3bCu/eCs1EXa0NS4D0JOUPdv4O8L8LxeJv+lUDoH9jljEjntnreL2ZGxOzdpJXe0WXj5JXXuXPN1pqqDB6OSqq5mxRM2uPsHOVheOz+96trsvk2I9qjmHjAqlncS0DXaP5KkOK4VHY+GrYwR0XuVSU4tDAxg6llC8Y32Ap5BcNXudlb0F2z1BNiYABs0aOdAuu7lO+wI910UlxzoPYHDevBOXJCuxvpfFfBs/Ur4WPSP3kOO4FejP7V75PohfnXwU/e2/eVmv6Le1ATFvMveU308itwYi/eJvavh6pP8AscfsK8X6OP7oR/JQ+mkThbrUltyDG3ee4fdj/LOF52Ctzep9135w0xSrPkQzO/5j/inSQSsbtcxwHaFBh8YgjZky7t4N9Qeoq6sEH7U3F7uYyHN0hYlhWZ7gLcwTg8MAOYmwWFR4QZcUjqqmrLM8vEyZBAPmjyzzpuD4/wAJsLE2fieJsfOG53qPu31vBSmr2Nv3hVDjPQl5JVU3EIKKtqHcRIMsbybFh8nXmUcbBG93KbzrBsHilBlbNNG2/e8bhn7eYLhDw9xezvEZ4sY+Tgb/ADVDLilVQzNuGUl+s5tqk4OObK/NLQudbjdphPz/AJvSuMZmhfqFNXcL6KOX+qtkk9Qt+PcytQbSSuPR7U4ywt3gFx9J38gg48XHsGhKsnHcncxUkh0QZccbHnadWZhm9SzB8fNqF4Fy5C8Gz0j8V8GzdS+Cx6X/AFIcZwK9GoXvk+iF+dXBX96b99eDeszmv3lgVqKb6eT2q3BeD6ab2r4bm/c2fitGfRR/cC+Cv756fLRvYzxnFvtRFHCDtyr3q/XUBGWAE/EWxd7/AJ51+sgZccqemJntd8VoqenxQOYAHSx5njnPOmWzbk2Fz46RjXCPSSeQ5Ym/zKw6VvhcaY7nfxNmg9ailF43h4OocN6GJUzo3NvdQDGopZA8sZIHWcOZYkKghksjeLcOToYjGdq/zo4/PG4+ByMe35rmN93TY1hdZh1SLxVUD4ndGYbexVmE4hUYdWNtLSSOhd9U6LG3w8R39OWDS2f8Vg9LgowyswCDEa25bRPe0Wzyu8veo8FZVUo5boYYXSud5T3DUrJwpxN1rDvTL251FMx7JYw+N7SHtO8HbdTYHPNTcY60TiIyfKZ5KM2IYpiJ18SBv3nK3cPEshG2WoaPVquIpp6jfo0dZUFLCHzva0bySqJmkUb39NrD7VIdlOB1uVRb5KP1lYpHbveKl6Q/MjVYhT18jI2SOmYxxaLXDuSjDURP1sTlcvBOXJC8Gxvzj8V8GTo/kxnX/wBS8JwK9CoXvl3UF+dfBX96Z95eCkWaNh6XBXoJj+3l+8vzXpvpJfavhuT9yZ+KuyL6GL7gXwX/AHz0HixQijDW7lLx7W5zbKpG0xz7zp1dwJjfGICY7YQe7yHn5rvYrVrz0/imw8Hqyc/pq4/4Gha+KU907QTod3xNWJoMUoqaplfHFapfG4tETG+KftWIV9TTQ1Ie6Kqhle3P4zOLPlde5RVlGYYhlDbkBo3qmwiGWARySCaQPka/xSRs0TYWbAOgBAEod8BxVJQQVNXP4rGZiN7g3WwVM3hjjdXic0dP+UYeOzPdZodmvlv1Lggz/wBboP8AntXAtv8A67Qf80LgI3bj1F/HdcAIxrjkB9Frz+C4Bx/16Z/owPXAxg5DK+Q/Qge0rBR4mEVjut7GrDeFmMR4rSUD6N74slQHPDs7m7Hab7LimErCG4lglVfj5xA2okto2F1tL87jdNFVixHmwD/Ci3Ha6zLtbTgX6S5ZgSCeZRiigkLgwieONz+ZkhtfsQbRPpqRvFNhb4PJpsHQqk0jPfcxOXaTm9qxijdymwys87Lb1p2I1cTZWBpY10gy8+xN/I50s9tQ2458p2Kor5BNUPJO4bm9S0WUdy7lxsAmmj0aQYw4bT53YuKc9njE/YuNpS4+NscOlclclnWfivgyfqXwTEen/qXhOBI/Z1C98O6gvzr4K/vTPvrwUiuyRvmvB9eiLsPkJ2mWX7ya3gvS3c3x5d/zkH4w4tIPvNo0PWqZkUWaaMHiYvK+YFhEGGO42upmeGd40gCwaumENNX000nmskBK5K4/EIY7Xva6bHIxg0AFrIBMkGRruUDqqZmXONugKHfmQDfbusiikB28U+3qXhnp1PwVo3/rXyvt1uRzdqf+UI8vmnu6+5EEMknmtJUFVwerXTtDOMkZT2OushsE2nxSQ5s2WIWvuuVEdd/Mmcc5txcbkMgGizFWF0yaGqktd72PF+i25d6crTMW2t1JgtKxoDZL6ead4Q9yUHRXcLhr2m3Or4gadjMokmDYxzB50UOH01VxYsITHC3oDG/zKeZa/kkAmM5ucBuxSYNjE0zo80Urcr27CbbMvSsGdCH+Ga07uL1WGcI6RvEytl4p7XTREcrIDe+XoTWts3ZuK4qprKc7GyZm9TleE9SDMTlef1JCNXXSW8QPvbpQbZBo7s0lTxzKfjhEdhdlFysTlPFnKxu8R7fWU+JnkM/xFZRLGT4+oPSFp61yGdZ+IA2d4D67ipB+loPU8qWqidE+opcp82JywiSMRExZBsHFH+aoJDTZqi/erSIPA+IDtso2kltbPfoYAqaokZLLLPK+PxHOAu3qRf41TVn66iN+VUfxqn/VyH66DeSGvA5uNKLuVxIPSXEqoqs3FmONoaSdNTZCb5VznBfk3GqSemuzi6qO3UTqFoCOtPfishdrZqkbXnK4jRXp4768lWJcLapzTGrVLHdd0HtDhsKIKeGO+if7Fle47tFPhVFh9B3iziqdjQ85r523ubcxWC1JjjrXvhe50l5cvIbyuQDbnCpaiaSSmqYpjELO4t4dl67LjIw4b1a3uiMLqiP1ZVSOBMz85z/lZhH9224VNO1tSBdtVk1GuR42sPap6t1mkRNPMzV3aoJGCXOIXsGj82X1p0ugkbK3c9p2+pc6EbLL3q5m8tP2qbCsaqoHW5MoNvmnmTJgY/JkF2n5wTgU5FEK3cE3GR87UH4xglRkuXv0bzui1Cpq6imEV2utd7DtDlLBiMcDqh7oalsnJdsa4a6KjmfnDtepBlzbkWtboVVhU0dZTOLSw3aQocQwyKZuhtym8xQgxsHdKyxQMJXEOkLdrhlHasgCsO7oV3tRM3Pfyz2pr9rAXc/Ovm27brKQ4HUISQteN+q5EfWfiHgXMci+Y/1oObYZm686Ed82Z1+dyo4fHcwfWU1geIbqLjlKf9VH/EpfMi/iU3NF61MdnFetPJLnCL/8Lirshe27PJGwpsrGOb5V79BWiySh3Nkd6k19HTvv40LD6wow6dzXZrkaoivdfmCvRNJ53J8lS9nkjYs0LJAdi4mCOe+11rLwDOpRRRukle1jGi7nuNg0dKZWv4jCqHkslex80x5L2eLdtkeMc5r9mmT8UZHAPc0ajVTsqjBNTviIF2ybiOlTUeOwwCpZFDU3bLGdkhtpb5y97MTQWi+t1f3NsKq/oz7Fbgfk8/EZD6mhT4JWiVt3REjjY+cDeOkKsxVkc0dTFDRPAc2VjCcw5hbf1qgqmXpc7jvqJeU/Q7WDY1QwNayFgY1ugAQiaSUaucvPijYuSoJZHRyiEh+wSRMkDv4lR4n39+T6RlPX0sPHjvf5OdoPKaWHY7msmEZ9CHbwmoIDulkwPSPanUWF4dNHdj4MQfZ3McocE+mdTY1TjR7B31E3eD5QVLPR4fWwuL/DMc09DtEbpsjSOdMnhMEgvbRSYRVSR/oyb26FaWGZu4oGnvfybo1M3GW08lZVbumrq44Rscbu6htRt0D7EW6ppGbITz2TNzSFmgczzHe1clnWfiJ3foz9iqB5B+xVTvI9ikrgZ+OsHEcknnUQgfM55dk9qdMwFxN7K/dEERe/QuHqCzXihOm8pwIcm8ZZp8Yh3rXKcOkrT+7QqOD2Gy7c1FEf8KbHTZxe79qtUNdaxcEXUBB8l5Xv5/aj3mE803FE6Nfor0sXopzqemwGNh98ZZpX7srTo1Mp+S2zgMmX16qz4p2NGYSEE2vdQYbwOwrFo2TvmrZPCuc3IIwRyWZennUrGhoebetVIr6Y0+fjePYQGi5BzbRZe92c+9Zn5t6AVx7j4Jq/onexFvBGmvvxGb7oWpRqKmTBJXEtmvNT67JG+MPrBSUjQ0OAHmhRwRlziBZHEJLNd4IH1pjBYK64+BwGjhsVZDT1eKVYs6rc1lPuJij8v6xX5HnkxqiZ7zqX++ohsild5Y+a72pm1pDlf3FpG+kpvyLxLm3M5gq4u1hjPsTYsIpA8WgnpIJoJR+j4xmoPzbhClhdhFQLO74bJTncQTqB7UaZzH7uMs4dBQPSuLnvuchK0P8AKanOpXM3s1HUnzsbCPFHjdPQrD3FgskLqk+NJo30f+/cHV0rKddh2pwJv7Vaoe3zmX9S0Z1n4gA9xoOqE8LcvNt5tVxtomfJs2dJ51l7g51HfjJLBjNv8k6cmOM2Yrq6yVLeoLwjutat6iu+eBWDP2+9AP4dFCKNrc4BAXhmgWdyedR0NG7MwkXubc5TZalsg0zXTu826IEO6yrU0XohUXCOnhjqHvYYXl7C2221tehU/wCTI5IakcbG8ySuLfGaB4rQgaqiBHjVTPvBDGuF9PTO5VNg0XfErdzp5eTGD1DVcHcXq4KkwCBzZs0wj045vmnmWF8GOMqYnySVTmPZxhOgY47AOpe9I1Z/J59U2QX3rwY9xbCKs/snexF3A+ke4/8AqM/3Qsf4QS2w+hllbvlPIjHW86IUFZBU1mLSNqqd4kYKdtmjL8521TZy5gcWj2Kr4T19VTOi73hj+SF7l2tjmUkfJtohZNsjitTZ/wDo7D4Q+d80fig2zWiwAsANwCbNG5j2hzSLEEXBXBzFoDHV4bBfdJGOLkHUWrGMC4yoow+uohrmaPCx+m0besIEaa93wg60Y+CfBDE7XbLRmB/W05x+KZX/ANHGFYhByjQCSnqI+dgf7W3uFarpJnNyxsma89nME3vNklwQdQRvuhUUrDfYECFZiZT0vHeWbBvSSrBW9w6vqmwjxdrzzNTWMDQLADQdyy5vUmOZrtbv6OlOZWxdJt2FcmM9J+IoD/VmjtWG/wBm/wASwv8As5/jVLSaRw8i3ikqIbKVij/s7VF/Z2qH+zNUskd2DKweSET3cszCvCuXKb1lcbwDoR+qdKz1OTWRQkyfKC9u1Clq4omPOrAdU5wnpnyDltDhf5qkixGEF3JXvVqPhOte949fJXJCHeDm87HexNhr6FzvFZUMcT0NddPkw+fEZdJcTq5Kk+he0Y9Xc8E7qUvFhu6yIkd1q91yB7iOrppafbnBBKwWCCODvZkzYpHSM45ofZ7tp1VmgbhsG4dibVGaciwN2D8U2CgqraO4mT2LvOVs0QtZqeBmT3PEbQSTsCq62oEOVzBtkeR4o/moqOnbHG3KANAt5RY3ki5vZHeE2+hssG4RZ6mC1DXO141g5Eh/aM/ELhHwZ5VdSHiSbNqYuXGe3d2q6tI1Pn/ofwOO2Z3GMnjPM1gdnC7z4GU75rmHEy90jN3m37QsVbiVXhlRWzZqad0fjbR5J9SndTjC5ql8hyOfBc+SNrexPpJHRu3bVRsju6ZtzuGpTpRaGL6zv5BTVmTjTcM8UWsAsqt3LBEaDUk2A5yu8aflW41+sh/DsQsgj3C03CDKqIXsM2Zn8kajQi2X/UA+J7ehFHp9SdzH1LK5vWuUDztCs76yz8EZ4v1dZJ9uqdLT0reLc4FpuQNiikxPl3GWFiDn52X5CjFTTON811aKMc4Q5fWoeKAffZortB6FF3k/jA4tET/F1OxUckVK20gldOWP2+Kdiiho4Y4dY2Ma1nU0WW9XjPUiwsbuLCrucrXXJGvccQnMjDN7/Yrsb0oaLk2UZbJE0WERsTzlMfG5pGa4I9a7wexj7HOLtIUIZm3FZa6OUNIZrYneo+MdGRq2xFlUsda9+h2pVWeW52UKGV1nGzvJO4rQlHf43sQfd1ur+aL5D5mXKfndiwzD6B2PYVE2nyyDvunb4hD/AC2Dd0hWeFBi/wDRnh0RZx00tNPSR+awOfq49W1UGAYZFhtM4up2AANO47yOa+1THhRT1dGDOJ6YNnItyXRmwv1hVQqKKobyJKWTMN+a4sWnoKmnldK/Qu3DRMb5KCt3A1WQYCSUcwq5hZ36NvmjnPSjbTYiUVYcpMd4rgsOwekfU107aeJm0u39XOsRxLHqFlBIaKgbVx3dpxjwXalx3BMNRMWEFvkkai3R/qFwVne5t9hPlWQ3n1yJgHkesoX7VmZEfmKxPYg7CMYh82oY71tQEbNPICbxTX2150OLdY7kc8R6kO9o3czQg8P1tqVkjjvbYvBt6lkw+c/sH+xZ8bwpnPUx/wA1naTznuXYQi3iwdDlOi5TtVouQtE4bynFxd5ouF4CL0QtqyFzzsYPtVXSyTOFnF7rua7YVFJbNTuZbbY3VBjEHFxyjjBqy+hupHQXMQziV0eV3klqqmPu5wz6MituzbUykqqajccr5Y8+b7LLDcHpJKmpeGRRtu953JmMiJ9I4Op3sa8PG8FRGEMcG7FPTbjKwbNdWplSMrDqfG5wrDLsCjjF3Pa3rKl4R8HpMNw+VhmkmiOvi5Wm51U8xDq3ErfNhj/FyquDmCw4VRyu4mIuIc4Xfyjc6qeb5SV7usprfJTQo2lRhMTUSrDMU1t3HZuWaQTztDrasj5uk9KkJ0aVFTay1DWnm2qR597wE38p/JasVqos3fAi58guuCmA3/KmIF8/6rOZH/wtUbs0eD4VHDzTznM7saNFiWKy8bW1U1Q8bM7rhvUNy15Wipsa4O0xi5L6WJlPLHe5aWCwP1v9Ry17hr452AE/ante4eEFjzNUlv0nrCtdeAjKuT1Lw2MQ88UL/tsuS3qWWheeZcl4sm2h03LJSR6bQEKypdEXWG0qKmaziy49G1eBZu5Kmw7A5Zo42PNstnG3jKWnxvDHgtOSoj2+pP4pua2qfZZHC9k6WqOU6BqjEpzRlyp27IU07GWQ2ALKAbX6FZsn0RWaniPzG+xWJWZpzDbdcGcKDjW4nSQHzS+7vUNVwfpI4HwU9XVNni4yKRjQ1jhe21yYReDCooT50tRf7GrhPP4KnNJfZ4OnMrisWruEEzcYmqy7i7RNnbka0l2tgoaDGsBe5r8sjpIy8DkjeLlU+P0OFE1b6dnfjOMLT47D5JWG8HMH427Y4Yo7MaOjYAoMZnyMuzLtz6EqORoCa92ZujucI/pHOf1lU29QM2KJu5Nb5K5k5OT0QiudRc6kmIjjF3O2BSMfxsjru57XA6lwNwAlk1c2ecfoqfwjvs2LCpfkqeqY30Bf1qnjJMGDyOPnPm/kuEhPvWnpYWW2PHHe1cKK8SNmxOoaxztY4ncWz1NWpN7k7T3Cu1VVLjjsK4uMwVzXPc7ymuibpb4jjHy2YWta6zb83daxrXZhyidOb3TW4oc2S2fyr2+xMEzvk9vmuQI0Df8Allcp38rK9K3rXK7CjJwhq4gfHoL+pysqecZJrFp3XWEO5MUMea2y6pmiwYBZUs7WRyvaAy2l+ZYVTPzRzRMsNxTJ5mhk3GO5xzLK27js2qGXAJWMfmOYLNjGH/TxKBjjxhcbXFu1RjY56iYblrnLv2TwbCOdR07xrbnUfkguQcCcp0XzHaqV55Ubg2xN1mF9t2K9JGN4bb1adx8lwDH9Zmb8VhOP4vNidfWT8bMG5mwMaxnJFt91wU73pYJe/JoqVrmxsM2XxjmN8o1XA6hN4sGpSeeQGQ/4lR05jMFPFDk2CNgb7Fh+LPJqYgZWnkTDR7D1qpxij70flL4SC2Z4tqN3WsdwtrY53OMTSMoPKZp0qgxXCRSTuZn05LucI0mSuoNHtOwbCoK2MRv8HMzxmHT1KMtvnUXnBM51GmJqjBTOdN3FWQbzKlpwXPcGgdiwClzNZO6V/mxcpVIJ71o/+Y/+S7yee/cHdLmFi6OfldmYLFuFNfK9j56Oh8WOkbKbW5322uKA2AIo9xut0O50o+SFNwYxumxJsTZRHdr4zvY/R1uY8yoMYooq2gnbPTyi7Xj2HmI3j4phsSL22e6kbiPgyQ42sQpON5TnnQXPGhdPrmQznZs3OzK9L2rlNVVQ8JKN9O8te+ORnZZY68OzTvVTJcTvLnX3o/5QyMvoYXLVB2JTXcflSnVMj3nkRC+Z5/BMp5pQw31sCpe9JfRXGYfUR32MBVsVw76aNMjM0hGYl7tO1VBd/o4t1qHKPCwx893hYfQxNayvpg/fZ4KoKtxL6kvv5jHH2BQNBMUVU4fRke1Q07XZ8PmJPnOYz2lP2CnpW+lUD/pU2IVb2PMJDYr2ZmP2lcVdh1j3c7VxVVPB5sht0h+o7nKVwiroBWcSuZNeCHgOB2g6hYRVHjIG97SXvdni+pYjE0QE08rLdITe+xIKlkTxroCVUzwB/f5yXNrM5kdjq4/wpzNldJ/CqrDaaWczcYGMLstrE2VDSaPpao9WVP4UU01RSROibE8NPGka3F9yxP8AZ/xLFL+I0/3gWI4RUQRPpx4ZhcHcZfYbblXS8zVXzD5YhTTYXUOc5zi1ubbzIDRBDnXSuldI9XdAJuF0J53WQG1y5gSnHafUqqn4QVWHZz3vVUr5Mm7jIvK9X+oDvtt7WLVDli4vVuTkniUbbH9kQRzi+bZvAHsXgXBbOsJ1Pj2HPbt4+3rUzQ/M9resgLD6ZznVFbTtsNLyBYLFipqqjEoLFjm2YHO29QWAfo3VU3oUsh/BRzVsk5wzEXsc8utlaz2lYvPZkWFthgbsD6ljSqyjN3fkyP06u/sCqJYjEa7C2X8xkkpTqyKrBquO8DupnQjbznajFW00nmPafUpJqyaQwTPzPJv35xbNeZm0Jsmr6Wl08+eWT1qFvi09Az0aUu+8VOPFc8ehTxsVW7yqo9c2X7oUkm2IH05Xu/FHzIG/Uv7U4fpbei0BGKrkfxrn3jtqVZzlxGOt3CWA+tpTS0cpvrWvdO1Wdl39wW7miDQVxspZtz3zejvUMYDSDlA0FrWUJ2BSj5NoCLMMqnvdme+MtHahGdo2rCsGwaoiqzLmlqAW5InSbG28lYbXG1PHWSf+0m//AKrD8GjhkxJxomTEiMzNLMxG1YNi78POH1cNQ6My58hvYFYlUFvEUVTL6MTlwgka34OmHpWasQxGN0NW6OmjeLPObM6x5gFwO4KcA8QfQYNRRTytipoZsl5LvO3Md9k8bLHrVtsfqUV/KCjPlFDziguj7UQfFCf0BOO0lN5u63DeGFBnHJqM9KT5vGjQ/wCoeFiPWmPgiOVrNNnHFR7zF/G4pt2luT6oP4rxx1rQoCsgOUOHHt5JNrojUYbhTPSzSfgqlmx2HxfR0v8ANVp/9SlHoQsb/NVEvj1mIP6pMvsCifq5lS/6SoefxVKzXvKD63K9t1HELiGnb1Rt/knkfLFvVohHE88Y51221K5cRUF7lgJO02VPFtyN6yAsMi+Uq6dv1wsDZ/XGu9FpKw3yGVD+piJ+Sw+Z3pOAWOzfI4fGOsly4b1PitZF1RfzWJ0tFAcRl42eaSQk6aN2AaKzr86FLXYdUOda1QAep2hQ2Jlv+3dkzCw0Gpd1J3KJ3o3TupTDY7/Cpt5v9X/unhh1ARhmNSw5spAI6AFTV8OcAOsdVAD8kPUqN7bvZv3LBZPlKRknpargxG7M3BsPzc5gafaoKZuWGKKIczGBnsUlvHd61SVzQyqp4p2g3AkaHWPaqCm+Qo6aL0IWt9gT+cooo2wXCGvt8pVSD/A1PZtsQg5NKAKBRA8Yp/OnpycndCf0Jw8lTUtRHPDdskb2vYeZzTcFOxzA8OxJzMj6unbI9vM7f8QR7u4iPzlUuo4i7jOizW7FL+19bQnZWkl23e8O9i8M4Lku6lZwdzOYVC6KN2UasBTB5H2KGMcp7G9bgFhcHj1tOPrg+xYQ3+s5vRaSqP8ARxVD/qW9quzkUkrrrFLeDoB9YrGp+PMzYI2Rx5rc/auKipC/i2ufmPjXuFi2IHje+pAyTlACSwsU6XWaf1uJVBoHPvb5v81hUe0SO+xYbHspx2klU7PFp2fwp25gH2Jzt4HrKEUMFv0Zsr6J7sPLgNW6hCvw6lqd8kYzdDthTlrZap2QBu1z2hbR0ogp3MEbqwJUmSzU6oiqz5tU9vsXe+JNiPydQMnU7cgrfEvPkqp4X8O5a7FK2lpsLa2KGNjJgaiVrB5I8m5XBfAeB1bXYTDUd80ro5DJJKX+DvZ3Qt7XJ7dHIFD3RJ2JyKA1QxPgmyDJldh8nEE+cDy2n4jZ7vwAPM4KF9C0eCu068lxPrTL6BnZE4o8Tex2/qsv2q0/qW1C31QsVmoonzVD5WuY3KAcgaFxr3l8lU+50BndYKG+sY9qij1ZTMv1BPYc/erT071idezi20pjj25rgf8Al1XxwRxvw/PkaG5+Mveye8DPSmM+tMhilGRpEjbHRUkvFZoIyW6AluwcyZlHJHqTB5IVK3xnsHasNi8sdjbqhi8WOV3ZZNb4lKB6TlUjRpgZ1DMsVrZGxRTzOc82AY3KmsiZC1r53NaASN5605o8NA9jfO2pn5Nkc0Zzl5N9gT3wV9K91zFPxjep+37QmuFlm2bVfrXERh+W9nahCUZhv7gstQuQVZpCAgxDprpD9gUgcHMNi03HYnPpopov0gadBfxlkZdx2DUo84Q51xR5fEM9ORYXGzl4tDCQdeKs49Wt1wYi8fFK6bqeR7LLBaazIInyNA0LzqmOjc2CARP3P8a3YsVjeDxgeOYsAH2KbFJmyzxi7RYBpIHqWIwtywNhj0tdsbQfWsfmpZe+Kx5hyctp1BHMQu/6mSoc/lyG+mif5907nTwnDaO5dBBdCK6e5H3liuH+U2WOcdRGU/H+9Xq9ERyh08aG36guc+udMMDtY7/SFxVph2LVbugoz8GsMlt40AQQQ5lG3bZU42uCgZ5x7ENcsPrKlkbymsYN+0qmj21LL9YTSOS+V/otKkcb8TJ9Y2U3msHbdSW1eB1NXO957VH5oQGywQipRLblzezcEGN6Vpbcm97VEW4DTtXePCaaJwsKiJze1vKCa3VOvdhR2vbYjfzqKcZD5YsFHGwNJ15kLaIlcpaFCGJzidgKM9JUOsTepmNudYbi0Ejo4nMngdlnhl8eM9IRIpo26AS2t0bUMIwiV4I4xw5A6TsXCd0YaJWNs3aGi64RVHytfN1Xsq2Y3fPI7tUzz4zinX1Tm6nRQtcAZWDtUNORcl1+YFR+TG49Zsqo+IyNv2rFq6lErHGoZbwjItTH0PaFEDryT06J48V2ilG0XXQmcyj503z03zgh0Io92FmLYtGflX0bMnU1/K908A5LXIsiGgO27+7kIBI2X09zeCQfNQLGGwv9FmUu4SdkLQpDC+/G7N+UBWkaeharwn1imP4H0HJJLM7NvMUTsFu26kfzKKBk8j3OIZq7Qk9gUjx4DDa6TmLmCIf4ysdk8SipYemWcvPqYFjcnylfBEOaKC/2vKLvla6sf0B4YD/CFh7P0Aced7i/2qKPxI2N6mge7ab2ujkiAaS2KNo057J/NbuRh/K2OFincH8uN0/6Nwyh29271qj4XYUKygf4hyzRHx43cxVUzcVOzRzbqOdmWzoyw3Y4bQp2uzNGfo9qgqBtsU0qy0UpY+CHXcTzpuDYdDHpmbGL9LtpVZRzNxegF6qmHLZ+vi8ph6eZUmJR0tbTPzQTtErD2bOsJ9TizKSIjLT8p+/lHZ6gqsDR7R9UKt/XFYxwgxLvKks94Zmc555LQquHhPJQY5W8UzihJFxTyGya227rLC8AxPDpKapn7zqZhHNd5dYje09S4Nng26fCIXtqoYuNDs5JIbtDutZZIwNC9zWgnpKp8K4NRYhDWvqJIpGNmYWgCzt4TVYE2uqqhqGVFPLJBMw3bIw2IXCosDZpaGptvno4nn12UlXUSzyZc8ry92UBoueYDZ7kcybzJnmpnMutP85P5wg/hlFm2tpagtt1fE3Tb5t/ubscOhM4t7XBh08px09Si/Y/43JhYbNZs3QlZXt6yvF6laX6wWNVOAZaeubT08dS8ECIOfrrtKfGSJJZJeTtebpum1AXt0LX4stheRrbmQyuxKsmMonPgIi4kZPOd/JBgy5Xddv5KX9Gy/TeylcCy2V43FVTJbPYx1/FdZSYhibMKjkvDS8qa2+U7vqrEuB+LMr6N12mzaiE+LKzmP4FYTi8bTE/I8gExP0IvqoZBqxBhuNibmDmkIl5fASx6xLDXhlVE7Lsz7lBWRZo3bNoQaL3UVRi9BD5L6hrndTeUVt2osdsK/yXxCspLs7zqvfNOxx1jLtJQ3oTq2qmqZDypZHOParqNu0qdmJ1j4OaPXYqscIOD9RJfJI2pie/pNiEzGeCNS2JxM1N4eLpLNo7QpKrAqHvg8p1MwPB6rLHongtjj4ozlsTg7UtvtUjOAVWZLX4mLt5QWZXHUgUE4IhdHuG22IIdwrrVUeGtD3vubLx1/1WXlfH3Uoke1jnDU5rWH2lSAnWT/mtC53euo/kuX9dchvUrSn6qzYTiEXm1QPrarTG36v8VovGPUtfi5MSlO1sLfHd+A6UyJjWNZZrRYKQbGpxPTzc6ZDI0TizCfBy8x808xUeBYZV1jTd0bfB+mdifNUPkkcXPe4uc47yd6Y6pga/RplYHHounsfmbz6FVAqH0k73OZ5GbcrhRu26FAFMkjcxzbghd7O46N+XnG4hRR3a3lH2KsbjcLoRmkkPFsZbMeXzBVgaIqhjJXtbyy0Ftj2qWVzn0tpOeM6PCgxh8XHOkhlgLgLDWzxYjVYYGX42d59IBDDMSjpabNGBCHPF73JKkew8oqqgcS+VzwHZRmPOpceoeKcMkkZD4z85qdI2SOVjvNfzKfCnd7xZjGPEPQqSnwxjJ5Y2mJpNztsFX8IR3q17mULHXZHvfbe7+X+o5+E9TLuhoZP8ZA91xby18ezbZMqtGXvzItNnaEfEMdWua4Dxjbkly5brN37qdS7hL/ymBOE0t7+NdcgK0n1VysUi6I3LwpPzPx7nIf8AVWvxRnnZE3a9wCjgiZDCyzWDT+alRbztWmpB5+dR1DDDOLh4tfnUlNSSU2fkxhn271d3c/L9C/C6vWqomDK/9ZFsHaFJhVWypjGgITKujZM3Y5qll4rizaxN9Vq1ZWvcSqyu8DTMcGeVJY2U74yWxk337PaqTCn9+OBfWuZlzE6R38zp6VUBjmPibIwbxtTYjx8LHFo25NHDs3qnxSnEkbmukYfGGjupwKraTTa1R4nj9TNGQ5jMsQcPKyb0JI5b8ytDUD56xRkIhbVyhnNdYlhZfYMmz6eEWMOlkk48Av3BosOpSTQ1DnuLnOifqTc7FoO6UfjbU+M1vO+KAdnK91G+uc2HO4uLRYjW5U9LVFkzcr+bmVdHXwvYx72P+1TQ1TIizKHN061UMNntabKE+O1w+1U8viyBad3iq556fPyqN7iXcU3++cfYof2Pqe5Wmkt7LLwQXKZ2ri8Zqmbc9J7Ci51wNydbn7E+W9yLFFO6EU9ZNuiadhv3Bz9yWU6DtK70qTNPbRvIA26qlcDd2S2pzaKnk8WWM9qI0PqW52w7CmU8buMcGtY0vJ5gE+voKmpdpxk7bDmaNgVz3KnCcGGNMHLnqvXHFybetU+NYY1zQLSNuF+S6mSgqTlZIfBuO53N2oG29OemO8ZuYbSD0K7QiznLUx+wrJKHc+1Nfc86y3mh5E8Rt0PHM5T4fglG2ma6N1cZGPOwsy7R3ALg8yAFX6a17ufQ7wR61le5vM4j1LT3Z+I0WTgg9/62vmP8Nh7qlOIU81RmjZJUZr7eSz+awuoqOPimabi5sbfYqOY2zNvbQFYfFRyVT42Oe1tmabyszyefuNB5QKpYjDLJIXXeQ6PbpbaoJGniHHMHG4PMjvVqvrHNdX28dcdDWo22y9swCDpndI87N9qvGhyPSQHCeFnn00gQWXXQDnuqSM2dURN9J4CoWC5qYrHYcw1VBOXCOqicWeMM2zrTXAFpDgdhBvddC0XLVNA0cZK0faqDIcmR7t126Js5Jdktfk5W5U3EcQqKalkBZBpI5p8o+TdUWDyiKSYcY5t7X1KgrrAOBGy/SjaztQQsQwTFoKGRstdRVTiGsHKlp+kHezoKpn5mtje4c50X5RbMySR+WTQtbzdaqMYoDBSVYYc4cONbf2LhXhBPG0D5WfrIPCN+xV81VHSsp5OOleGNaWkalQYbgVFhrCD3vTsjPSfKPrVRhsIpKmIiG5yy5w4i/wA0blSZ+MMMbnecRdALoQdoVycp2tVk062QdoU6Lb4qZLW5RqMwUQqsKoG2zgTVDxzZ+S32dwxnRXbU+kO5r3LFcViFSz9qft17uvx3FcBsP/aSTv8AW/3Ti4S1O0DkR30YFDfLlBPMhOc02lvFDUyOK0sz+KB8TMo2yu4sODL6B20dw9C4qXjnQOIzcnJuVG901i5sj3CzHDXTpVpLLwunMr7+5quQVdvaF3jwioJc7WcpzS47NQpOENKyE19RG1rw8GmeG6jnssZpJXy0mPVIe4a8cwSW6lwgwDDbVtbh8kEkrQ+QxuMj79CgqZcPZRVdCeIDT4YvhZb0ip5qmofRTMqOOuJ5QxphjtzF7rqpFS3DsK4Q97iLa/iGk9QK4UQ1lTJHwkhqpGtyd7SsJuPOY1oPKXCuWWEQ4TWyjy2yxtaPWuEdJC6bEMFji5XIaKmNtxznMd6wipo2VEtTUtdnzvpYp2ua0X1GewJCwqrrGQyVpoozK1whEZJkYNrbnRo+1d605psMlAa9xzva8Z2N5rbe1OwbgpFiEtsk5fJ848yfiks9fUl/Gv16huATp6eeiyjPE/kv6P5qOMcQNXbOklQwZpJOVM8co/gOhUzYjLI5rG21J0sFTYpUO/JrDJTs0dUkWjvzM85SwmxtY9Cc5ucJt9fWsx5kW6E6K473ftaLs6QN3Z3dLrN0HnRGjx2oEJgOnrUUQJNyqOSZ0kbZJMtxo2wv1qf8pU+MH+tN4qXoczxfsRTogbaJzn1NztIPc17vF4hxm6VgPaNO7r8blaTzBfk7gtg9MRYsooy7rfyvx91wOdQOr6qkbBTQ5i5wcW3VE6qndSxlsZecmbU5dyipxba/mT5X5nnVOlcd6h6LoblLD4j7a3snSkl7W3dfUdKlZLYOOwK5s7b3CFqto61yCg2B5O65XDdlFT1tPSzmOeMSMySjPY7LtvcLhjg78klTWwkeTJf/AKrrHHMyVTKaqb+0iH4LgzW27+wCG/nRHKv6Oqp5detpswNx4w1WBxVIlwbhMynGng38n7SuFkXHGlrqGsL2m0oc18nrcuG1BRmA0s98/KcyR7iRbdrosSips1HhlSyXLmlqKjwr22O8kLC56EHvlvHmQcYeJcTc/OeTZU08kBjrGyl+o8JxhBHXlCq5ariXv4xxGgbZ3V4t9OtcIJ8GhwuSufDBTtF2vtF4v2kLHpYjxdbM2Numr+SfWsXoiaqi4yYPIzSZcsfa59lgWHYjPiOM4lHUTNgc/iWvzNB32I0usAwHBYsQmnzDLnBBzZmkaAdKq+HUDcYxGqy4UbugoBdue367n6lh+EMmp6F1PNLThreI41kQaTsaLrhZU1wfWRvjga75KIaAdabj9K98JZFBAMpaTynO7UJC4M2XXJuedBp0KOFRR13k00zXv9DY/wCxMla18bg5jwHNI3g6gqGAeEkazrNlhkLbmpit0vATZvkJ4Ou91iNK244qb5rNqx+bTvJrR86UfgsWlHL4iPqzO9tlPVG8tVI5vmbG/YslvBaDmUPCDA6uhawtnJa+Au2B7OcrhXD/AFNsnoSNK4SUwOfC6odTM3sVbTvm42lnjuPKjcEZ5mQs1kebNbexJWLnZSPPaFibByqWX1XUkZ5TXN6xZZ6WGTzJLfxf6g/EcRo6Nu2oqIo/4nWQZZjfFYA0dQ091imIUEVFPVOdBC7MG7B286Dbsg273KprH5Yhm53FNOs0/Y0KWHWnlDuvQqti+WpyRz2umeQMnQn7LXTYRnk8bcOZXJN9quraOWYXGoRuvCO61yXdSbnZmaHNztJadhHMqqrYOKkdG0t0y7lX1jiTWSO9JYxGTxbg5Y5TePA5YhvpyfsU41lyRD50gVPAeRO4u84aBY1S/IYlMP7w/iuE0O2pZMP2kbXKOYWrMFoJ77bNyLgLWsDKnBpoNf0T77fUuA1Vk73xesonBoaMzObYsIquMnw/HqWSrfs41wygHbYFcLpIYbx0taIybsa5uR3NsssfvT9+4JNxDGcuGG7eVvF9dFSVLoaCqjqKOgja0iMMzg3G/wAW1lHWVYw/DKOeSOnDrxtOQnquSLLF6DjcJp5pqKG3KZxgz8oX8rxexYPUYlkbUCNpNg6R2fM7nv0qGlM5jqpi1ls7G3F9ba89lPHPTupMQq2CZvLLiAG3PMFjNDRSNjEbpGOa1vGeW3edFJjD6eKmxGkhkez/AEfV0mbeq9/ytdN9WzUyqpzHLNUEHnfcHsOhVBhsEdLXcJaqzGhjKSldZ9hutGqHG5qh/wCR6qCFgaWSVUpe6QnbpfRYfFq2jhB9EFMgHIjaOoJzzcpqCCDXbkM+zuEFODdp9aopyDJS07yNQTG0kdtlhEm2ih6wLexYNJ+ikZ6MhWHPactTUDoNnBSSwzQxV7LOPJzxnT1LhDD8nLSS/XLfaFwoiH+g5/Qe1yxyn+Vw2rb/AHZPsUsWkkcjPSaR7U07x8X+UOGVLIRyKKOSod1jkt+0+7kqjlbyY/apKrlPuyP2qOFgYxoACCKKwVrT3zFG93M3xvsTqqVz44+94TsLzdYw3lR5JxuynVcMxStqPyWSHNvlD25vUqugfkq6eancPPYQs2wgqSLa0+pMdqAi1wvvOqvdWHYFis9JBI/FHNDo2kCOFo3c5ROslfWv/vMvsCo/KdUv65XKgGykv6Vz7VCW2FOxo6gqOoPLiiHUFh7hycwPQU8fJTntCxiDxCHdtlj1N40D/VdVsXykJHWFbawhRHaT2qWnN4KlzD815C4SUujK+Rw5nHP7VirgG1dNS1DfnRrD4RysLay+3izb2rg46rZiFPSVPfMp43O6bTN0jVGkbIymjYxr9t2g+26c4kho1271LvNvUFX1h8DDNL6IJWIU2N0lfXRup4aV3G8o8pxA0ClD3x0dOI7G2d+p9S4SY5UMmlfMxocDxjzlt6IWC4W+SeRsTZZXFz3Ws5xO3pKwHDbtllkjadwiNlweqI+MZXsDPOcC0fasPqdYKunkHzZAfc67VfX3AQ7nJKOY9zXYjudZZxyxn6xdYTUfK0NM/pMTVwZm24dGz0CWrg+/5PvqLqkv7VTfosQmb6UYPsWJD5Kup3+k1zVwli8WKCX0ZR+K4R0/j4bP1tAd7FWwfK007PSjcFbbp3XU2D12KSN5VbMI4j+zh/m73Y+VnF+Zi06u5vVPDoOU7oWJcLah8TauCigaBmc54ub7mjesKijY6KqmdPbV77OaexUHB6ldHWUkcpceXOcpHVldsC4CccKiOtoIJWO2iZrSD1LA4NmLQVNtzGucf8IXBw0MklTQ1b4meMX0hLfW5YBjzeNpeC9XE5+okZKyD7LrHsKYJWPYONkDIYC8Pe9x3Cy4VUIPHYZf58Yv7Eaa3GxSRvza5mkKrewyspKlzBtcIzYLHsYk4ukw+YnznDK0X6ShSYfS07+U6KFrCekBR8wTeZBWRG66PmnuDmUZ8kKlk8aNp7LrCp/Gp2eqywuTxQ5nUV+qqCOsLGYb8W5ru1V+G1IgrWPjz7HbuxV3GPY1ufI6xsFjmICNrKW2RthdYxUW46eOEdAuVQi3fE08/wBbKPsWD0luKo4gectzH7Uxgs0AdidJJeWUll75MosuDeEzyOY0TVBNzblG/WdAq2ocQwxwM5mm7v4k1pzu1POTe/au+rl9rdKgqW5WU5mG+3JZ6ymPAJjYxm/JyG9rztTaWnZC03azRvQO33AvsQPcedQCexED3GiNz3AmlaILm7gQ50EFcbVRTjwlLA/0owVwen8fDYPqjL7Fwcmvljli9CU/ihhlBS0UNM3iaaJsbADub+KZJthcFD0jsUJ8sJh8od2gj2Z39QRLbtgt1lVM+e8oj5HIDVM/a+V56SpJjyieq6ltGBUcXl2ZBYrGqbLHHi1YYneMwyuFj2LgsGsbiuCh7t8xe+W/8RK4I1/BqsZhVPQ8fMxsUbWxNDwZDlv2KPDaaOKhfxLGMDQ0AFuirq2Wg74YyWnpqjjpGN/SFo5Oh5lRzuOnFu53DKUHVUNXxhfLExzY82oaHbT1p7dHM06NVQVDmudTRvdtDsoKczkhjC3fybKNwF22PV/JOZ4sp7dQn21bfpamO3jq2FXQKbzIvNmsuppfGysCoh4zi4+pYZEPkGuPOVQt2U0f8KpT/VYv4VQzeNRxepUMmxmTqKnbrE8O6CuQYqyjD2fOZcLDqZuWGmjjaTrYbetRN2CyaNiciFZVTGgwsz67N6xB1VUzR4rxck0jns76js0dBsCuFbmf6Ng9aP2VSGuPZcKoxTFDQ4jhlbTiKnbK7iZOc2y6puFziCmpH1Eu1vfT7sYOkN2noWIx05FS6OSYvvxjm2DB5rGDS3WjmzPe6R/O4+xUFE1xmnYMvkg3d6gsQfA59NxWHwf2qq2/VYp6ikl4yapqAHXFRMLZ7+aNw7j5To1TeS4dqk8uRx6ALKSHxLt6UyajidKxpeW6myafEACqm6ZLhTgnPTyN7LoZTcFtucKKXxSEf/ArIlFFWRTgjzJqauldPc6O4PcOGwlSecVTgsL36ZxmttAUQnkEHGCHNyM+0jddMab6konY1SdAT/PVnXPK61l2aKxDhoedpsVjmHWEVa5zR5EmqBs2upMvzmLAsUaOLnFzuKjJvA+3UfwUGa08wB3DYqR/ilhUThsTbaOIVRxL+9pWiW3JzbFwjghc2rbBM8b49AfWsbqasVFZBI3I68RhkILPVoVaG1VNygPLblKpzYca31pj9TsTGCzW2V+5zqNqduCfzp/nqTzlcWewEKgq7mLwT+j+SqKN+WRum525dCAVu6cx0VJN8rTRO62BYcX8mDJ6JLfYm0/JjaB0qmwccW6OSWQ3tYWb61jGIu4uN/FNdsjh2ntWLtnlqC7vYvhe0F/Kcc3OFh7J+PmElbUX+UmOa3UNymtbJlCjb42pQbsHdC8A3t7hRTJo3Rv1a4WKbC+xbcbjzqLzUznKbz/Yhfanc6k/8CyHcekIc/dCHccite5p7kxDZYJ1tmi4x4Y2MknZlVdC3M6Iq5tv7oU0h5DHO6gsUm2Q2HzlWP1fO1nVqqeikZKS+R7dm4fYpXRNqa0cs6sb5qppR3yIs0jNQL7Vg9M8tqYpKdwWFy/6Piluguuq3bHUwyhVzfHps3S0qEaSRyM6wqKTZK3tVNMP0blS5C/vdhsOZZ4mua4gcyeNjypR0oFp01RO1ALvfLG1wzO6VWd7SvbIQQwkLGqg6y3WKutmAKqDbjGppQe0slAexDjTlfyNygHjOJVGFRbv/LqkO77VAdjyFJEM3jN5wtVWmrY4Na+IA8i+242nqXflPSQTzv4iBz3CMHyn7dVBTgClpms+dbX1prdXuVPAy0UWvOnZTmZ6ioRuc13TqjlGWUG52lThx0DhuT7EuHYFGCAWvbfoUckGjvFNkPcNBZHv2nuDm7sMJyi8j/Napph4Z2VnmNUk/it5I9Shp7OcwzO6fFHYqZ+jom9oVC/yGhUT/wD8qI+JIQpd0jSqpvkg9RU7NsTkQdWuCt7qa1tbItNy0HrKoYyJNOMI2qCUX0UFSbZR1pv6x3rVO21yT2pgPg6Uk+hdYgRpBkHTouJF56iNiwSn0dUcYRzf9lR4pVkx07hFDqXOG0pt9di5mqPFwQ7k36FWSPL6auc37Fw7ws3gqHyAczlw8wrSeCYgdF1iMGlVS+sWWB1H+kUwb2LglVeLUmI9dlRGFxpsWaeTsJRkwuBztSRt7vJKHcixap46aIkt8Ug2spMxMc1S3S1uMNlPQuFmFwRYBeMhCy17k3GvZTxCwNszlicu2e3QAqlxu6ol9ad+ul3eWpAbsqJmn0lwjzxihmE7iQCxwy6c9wnQ0rWynMcuq5bg0aX0U8pB4u3SVBGBm1sm+TsTXjeiQA15aOhPy8i3apb8q1kzJyouxQ5gQXXttCdbkyuHMqoRuOZjnW5IRhd4RoDTtsmOAI2HVBdKKd33bTRgT+ZPO5OKaxpa52ttgTI9GtuT6yrnPU3A3RoWytZlaNgWbauZPbuTk8b08JwRKZvYCqeQ5eKaCqZ3kBU55wm+TIVKNjgVU9Cw9njmR3asLp7Zadh9LVUFQczW8U/zmaLFKN/JyVEfqcsh8JQv7Fg0ZHG4a49bLrCoofe+F/8A8QCxaokPFQRQjp1KxWo+Vq3joZyVC7WS7z843VPF4rR6k2loG6WMnKPdCYdyaUx+0ArDqr5Wkjd9VcHKn+qZD83RYc//AEeokjRo8a4+uqOPp4fFj53dKjoHmEMdk3EDRQO2P9abzheCv3AFTQ/KSsZ1mywlnjVsA+uFgLf/AFGm/wCYFgP/AO40v/MCweTxa+mP94FSSeJPE7qcECNypT+jChPilwTmOLeYp8jsrRcqWTe23XdQUjL7+dV9W/i6Wne6Py5djR1LiBbiD171CfGuOtNPilpCBFi1WFhopdBxnanA8q3YnB1shPSEHC5Dmpl9v2JnlPbbrTXbHNPanpzY2xvbbKLApvOhzpsTC4p8sjnkauKJ3IBuYmwG9Ma0ti0+epZtRo3zjvUcBzNbc+cm71EVEedN89D9YUdzypW71KNyfzLoV1eVuu/3IQKCsiFzhRXGYKlbH4l+tBzrgBHuGaWOIeW4BcWxrRsAHxLTuCazYgVA/wAaNvqVMdgLepCDy3O61pdN4P0Vo7OqZdGDm6VWYlO6aqqJJHuO93sUe9Q+aFCR4oUPMizxJJG9TyFjVL8hilYzqlK4ZT4oyN+JzzwMBc9rgD9qrZh4Rlk95zO5I3uKfT5RBGXRhw4x29wTmYnVV1JKWw1Ubc0euXODfMm7X+EPTsW4nsV9601AcVC7xmC/QmjxXvCk85rutHfGOxRHcQoudN7mbxm3TY9jbdiMTg4P2HnUUg0LdelRg2sUL+KonauCgYfO6FDTeNynbmBVdfrsG4bkYtZLvP2Ib0xBuxhco72Nwer3J7t0EBy76+4HcBRRHuNEedPVtq74xdjtoiaXHufkjA62sabPjj5HpbljmK1MsNbTQ2jb47d6pnfKMLVh8myUDrUT/Fe09vxN0I2E7gE7FsaqZs12Ndkj6gh7gLMbMBc7mGq4Gy00LpY6ySUsGfaNd6wvD2cXRYbJG3nJ1Ky7IQD0m6qqrlPByoAXQYLZXKM77JtuS5ADXVabwU7Zcp43hO3WTt7UOkKLeonIHZcIjY5Sjc0oXs+muOdYdN40JYecCyg4oRtcCALaqJpN5ET1KKCMue7IPtKhY9z2ggHn2qrncQKfwV/GcbepAbkw83aoH+T6lH5LyFN5MgKrm7gVUt2xJ42scFbehzhM85R86j85NTnGzTZSEcvTuH3QQ7oQKIaml9W7ygQEVxWD01G061Elz1NXFUkk1tZHaHqRGidvU0Z5L3DtVfTnSc9qlo23ma14HYsAY7LUxSxdNrrgfiJAixOEHmccvtVFUi8NRG8dDrq+zu3eF+SODWJVe+OneR12WJyaiJuqxuTYAOxcI6jxT9i4W1VuXbsWOyW4+vDOpqpGAcfUSzfYsPohaGBrenemsA5Nyqqo1txbOcqGn1PLdznuMdySy91SsIsXA8wKl1yzX5ri6mvazHfYnjyXDtTn+eOsKUeUFJvb6iughBo0RQ81RHbdq82RPG0Ap3T3OcBQprtje1RwNu52qfM+7ruO5Ne4OkF7bk2IW2AIG2V4Tt4uojtamnY4p3O0p3meooczh1hRu809ap3bYmFUR2wKg8whUe4lQHY5c0gQh5ipmbG37VMR4hCkTaeJ8kjrNaLlUjXubxZ0J+IB7mdpCn4PYi6SRjnU8os8jW3SsIqPEqo7ncTYp2OcIo6ak8KIY8jbc52oUuHwQ25TWa9aZzJw1Ce1Op2mwuVUTE7VM47E7eFiFEb09VNCR5ryFwzw+3F4m945pBmWK8IuDcdfiOTjJJHAZdNB3OUU+q4OvpGbZ3hvYm6ZmqjhteMKjh8WIJrdgVkUXGzQSehYi2XjCxjB87Up1tbK+5BAocyuNqLBySnt2m6cDo1NO0KI7kOdXXQgNxXM/wBal3BruoqTzSFkHJbfpKlKOy2Yre/1JreRCLu59wUkj973lOYzl2zH7E3pCCZ5qlB5L3BVDRuKFhomIc5WnjIHmUZ3Ju5xCk3SetTjzSn74h2Jp2sKi5lC0HaFEQctRZSPl/0nRQ4Zhvesb7vtd5/BVL3ucZDqbqhqrWe1Qy+K4e75kU2TQhYfXyNe6PK5u9uio8Nn42OO77WzHatB3BZNI2Jk17hRSX5Ka7YE/c1PpwS7QIxuIbqvydwNwmE7eIa4/W17mhTZZoovN1Q7nQgUVPKQS2zVTRizIC0jeQmnehIRfYnWAjdlWVvOrvy5O1Q5sodqulHoRG49wIBX3p3OnBc6YoXIbnFObsKleeYKGkbyj/NTVRtq1nMomvawuy33qOEcka86JBClazI1/wBbep234xuzZbeomsaZGlrjsATSNqb5qiOtiojvIQ3OR3EKVu1ObtBQ5yvnBO5winWXQhzKBwN2BUuGwPqNltinxateSSWg6nuPZKMriOoqUtF3u9acRtK2e409zy1yB7nXuCx0T++bZja6ZxHij1IDB6G39nZ7O5yVevd7kF2xa9we5bc6BeDRvGuWtELI2RIPuNO7qtOxE1Trm60QdWnML671oPcAk9w5DqncRHqUSHa71oFZ6POjkQTeYdw86Pd5KzVNO06tzbNypmcHpy2GMHnDQhzL/8QAKBABAAIBAwQCAgMBAQEAAAAAAQARITFBURBhcZGBsaHwIMHR4fEw/9oACAEBAAE/EA6FdOtfpHGB/wC+fvLqw1g1++VvMinvCC4Pogw8pA/i/BN9gtSQQT3Zen7VlB5fqhphnEVkjtqhQloOIUjBKQU5ISyi/YATvxMc0cw4qHyP4henT5MKpyPvDa4+7LE5ianJ1Ng3ShB3H34G11Klmz7mtFJsNxZqSulRL6aRyjFVNZUplSolypURlRIpZaVCP7tZUH4JolW9KiRL6OQwQKPP0JbvP96BTVY4b8z7mXN5h7pNIf0IrywgjvsPoQVVj+estRxAUfsGe++qWqYxlUg6zLkphZMsJsxgHAHhgKIdQGXd/JRF7L9dqXwtLhLqVElInYvOX/7ZS97/ALwLO0V+lpAvuQeKLDkfeYWZQpCViOOIBpZBbSNIDA6gI4/wVMqUdK6KlRI56LD5UpJpePQRiowSulWeMH6QDwvb72jN9RDXmfcP6HRDi4kB2sQFswP0yn1qnqAC2+dAQ7y/tKmdYqmefojAGYMMJWgwt1gIm0WDK3d9pgPo0XUO8NchTPccCpUqU9CC/LhCP6uEHB/74QlbQQiGfHBzMx7/ALTK/vUqn/2hDfp2hn/UTgZclSr6mkYsRjCVcYpiSokqJ0MFXKB49Uj0SVDMohB+36Socn9iWpLqJq7b6sfmfQhoaf8ApBg5ggsqweIBkagxO7b4gmDoX/3QX2vonuPqmQeCDJUOJSGIggKlQBAUvmZzsfdlcCUA1YmQ33gn+8tSGLipTK6KlPEv8uUXfsoga1+/LXzCe6CMWhd35+QfjUNNNjKT76B/AIdUTLKrMYE1xRHoAxxlSoiMVUoiSpRs/wDZEyg9Ho9EldUBCZD2fSEuuSYwGcItvRBi4X3O70/iE+ckzDlHwGoPNRc8VThDNtaHuDDmVFz96EjA+T6oaPBCCnooY9acdBLNfmiP8UEShhaPPr95kTP8hRRKJSUTEQ6KPNlwL6uIKn4+zNZrVJZ2vqTD9TMNPlkN99vSiW/pUqD9anECBLRXS6hbAFQABqrGnMqHBquGrwJtB/4ldgxbrgEdhKoGMVGHoeokQ6FPQX4/2QZg9WVKRimVG0tKjZMb2fSFinCTzmQBP5EP5v3KhuH8twroaJD7iR51tu9yskMJVp1WZD3Eaage0OB4PylH9rLLP0aIKDwQL2/g3EvtMsDt01G0/IlH/mOBH83mt5sH3Ki79/w1KiSp7tM9+toP1t8pRUMbg+hLx5kqWBhhb/biGfP9cNQjt/FCHSBy5jYZUcGyRafLNOzBoBVFAaBLXrcoG2xww90m2v3FtxB3aJ959/Ixg5DcehhUrtHoQxUqKiRhXOD9kTno9dEiY6VKgVEzLqtafSZsKygALQ7ipzY3P7zlH8CuGnmPuBOUR1R5W2B8aI88ITbpdVWKqn6KMf2YUP6OENHg/lX8GFafOiZKRMYFqoeamTz54r0qdD0k6SHkRT9zLKfq6pXsUxf9zp0nfJQ4punpxxx+vpxXWo4joL7krmXGxOtuG300EFK2ZzB2Ltb7kvJxAruO1RvQVoSz5JcwahHLshr9I9OoPPAD3ckY3n9j2Dw9EjCTJ6HoVGahUf1TEsuCo6erGJ0AgXBXh/SUaA/tTLub/UAYfu4o9fmyvxgcmv8AuFmheA1eMTa9/uoR4D7qwrZMb2UenHZvP0RWGNj/AOTf5WUfcVWPFvp3xnT3BlB4kP8AiTpc/Mymb9Fhgs+2/pAAz9CkY7v3kN8yUeSvUJgD/nKsUFdSWLSLqkZjtpCpNvsyUdViLlsyqi4ktAcUVL+RSMjGy1MBlANnC8EJdrFF1NPjZmaNIv8AE64qV0YqJEolRhXRX9JmV9ENwVEiRIhGVCkIV+L9kH4H2l4WJaII33jUluGrz99Iff8AaIHG1wvOD6S7iud5ewoJ8FEaP1YG2+X4cA8n0QAHg/8Ak/Cxo79YZ+TD3byMv1m7z/KrlED5M1kO30xRS6L2Q3Fn+pAs5+8lsIz2ZAs/vUB5P4BAqoDLwR+1E7OaPOWI8wvEsIm+GCQszM18m8MpbpbqmzGK0LavUrX1D1YFXZmFkaFdv9DMNKi/LRHLtTgDYXlzARgG3/DQlX/BUYovpURUtWa1PwjoeCDoHSU9WV0uMnGcJTeFKd+epWHGpN5o8alJYe78MQy97hlOF9KjRCiHlKgb/cmXgt3ufKPtTIu/2hoeP/kZJdx8vLdh+RnKO7d7SD/4G5QE5BXzAKRoVPgjhByfpGT0Mspz+VxRd6DJAl/hM3JrgawQrNx+Y5qZJ8JuVDW23eAV52n5ldIAHYxSUW5FvJGhq2aWRzuCIGpfva/zftF7yF/IG7xE+EKbN8ezszIQu7F0/wDUz8p2llN1GNiYRJUqJmVBQ7fRC58Ew/XpKiRInR6EECGIUYcHyhDuf6OCgEJTveN/n+0FhdmFu1mxQDy4sWD7ODfPYhDj+2fkfb/5qS6H3B1fZ04bzKx2DKiUlJSBHrjKtaJr9XE/S7S5f3jALx9sGya/alocJJp7frhov1p/DfkB8UWJZliTbORLBi7V7Efamu1U7TOBlRQ+zmU3uCaZjQQUn2tV7YwN3bzywstZI3kWu+Np8coswW5kP9lnihlYVqH9Rkh9h3fO8alUJq/MWPKuqwx/HFIquiXERK61cuLt9iarsSuKUgiRI5lSoQir9zJFb/W0QzKKfFYHDiSRdvtMB20+4g0dCDp1b+THqQHjSDBoe7+MaxERS7pSZdo33mr5+0P41Arq6QtnnzGp6a7AXNWOaD/8DAqIW0GsRYu/1x22lT51BKXCLjLsQCDzh1jCIalyi05JZLufhC/X0mg/enSpVS+HrnvFhyLbsiOzU/LDYzWJ0UjzF5I8uGrdDdh5/BABsg3l27brWCgculDVlYuaSDRwIk0K2ax4iJSqlhskakRp+5VYQIiGr9Zg8Xq6JzyMbAXoA9l4MJCQ3o7PRIkToqH0n2QHwIa8mMSMFn8LZbFo/okdn8XmRfCwUo4l2dVeX7yyLeIhemPqwf8AWs/0TI8fTn6HZFfZh1fJ9w0P5HV0Y6jdeucIV0j2r+5Z3AXul/8AwUwZeFPKI05AEOiLVOCWBlS4A5HKMwV1W7fK3LXi3fT3KEvanaClIlFfqvQJxCXlmoJYjuMG9dF6EplMZRvncjTHt1ZvStiCc/jFskWTatWNGLLnVsRsxvW1zQm8oQZwlr+8b0aypksq4wPf5jVuq3dLhUct2OPMdC00viVTV66jyS4Ld78iTDMNx6CV9sd2BlJNLJmMoXhSOszem4Q0I10J8dElMP7OEF+Ahrz/AKldEjK/hcfc/wB0QxXijygrKmILx9OXBopo/NzWNswOEOVHNQfwoRfL+pCtz9GPDxHr/H3BKJZLP4HV0iea/DBvuMO5mCFUHnAf8VSpXQmgNuyBRfjajuUtJqEyHsND2EosjwI3Kii7K/kQI2VqK133EN6S219i/wAzWL0q+8VZByMLdkrL/men1ADpRKhRhCy3AwCpoUA4hCzFrYO5G4oSUd4VbDBcIg6Iw4R9cPJAEFSK9WTOSW9YYazOwlEAShzPra2cIJR7quxsRtnKPpf3Ka5mlM9wQssz5RIsHQWJETAFh0PjmJUYb/Zohrwz93t1SMSOOh0XrfcGrmPkKEmfUCDEbfRg5WJ+Yr8rxBiBklMLlzTPEr5D7MR4TqBRl6vuGJ1wwb/iaxbn/WEFbc58Jav4V/Gh0yVCWm75dVD4AI18qA/UCZir0PDKygqojZDq1wjz3HMb2hW471sxje1YuDk8Thmp1E4T0h1SzorDFi75aieGKWlOY0CosBhighCAl5JTUGmYXooZqDbu9yPUNMG48M7OKx5mL7JpPLQl+OpRYcVEiULc+qcsFx57XvM2F23vfEoEBrBj3Nxm7ycMwL22JBDoZN4pECfAftMvFF8X9YldGMY9DpinbCPdGbV3Q8cqgtPJB7H6cAPeofLCvxDcECK7UQO87zYQU/P25Wm9v7kqLiIfV9wxOqdKQRd9DWarun8znGmHs8hxTLl9L/jphBXwRU3F6OYnEa5rkI1LLcCkFmPKPcOibs0KXy9HuR6iWcq8wWqL0OfcoIFiO1P9Q2ZvnYf4h6WMRfQs2NJpYkc6IfEUym7bV2OCPkUgnfzd3QNU2CJW3WNB5AQGODmAVDvTzypcBF8TH+hONp1lAYyX2U1XEunHt7H7EtlhCbiNSG9/RBsuI4SqMLrS7skBTAuVPGfc9Gyw934OqTb+A6JPEl0SiQn7GcVTrfoaY1jlS9R2qLKvP/kg8uOnkRLVcpJIh/8AQgAxVLjoHRiDgELko6J0ImDFkm38N7PB8sHuBGUdApCCKcQWDGIp67oEoJEt90O8BGApcU6gzQfJiJCeZj7CKUPCmgABqrRUNSVAd2TRMxgTa03K1dx/gettezw7oz8a6Kat60mz0keWWjXVychYVfRVtiXV5fOrHmP3TKgYHoo9wUHc22w1aFoMThdvRvS6B0jG4G/iH8V2yiLG0f6JKJjPLAsr4IQzdxGqEaDAUA3ol7lp6r3aw38P3KA+ZdWsVfjo9EjElEuulT9ka2mrzzAXO5PlhuJhZt9KZO9h8XAr6Gc8FQjn7ieQy/N/XCLouNF8CiLaNDNkuqXe8rbCyRQlsJbBEK9AUJKC8CxL6kU3ZUoDtffURB0h0yCFWCeYM5trHuECcMMxsoFvLsJb4onxiBdqWgAd7DxSU1gleEDAyshkRdNJblWP9ysXKJgcIw0/gC2UwDG7GlAO6ySndQe9YTZGyA6eo2NcUyktF2t3ZIupdiRC7X/IjI4od6FgNAIsNQDYiQJvvxuX8GIHKKhs3sljwUs06Ufsr/USwoj+N2ENANEF5o7zWJ4XvmP0sPEIk+aFvvDgKx4pJxHD8qZiHU7DnE/DPuGy7v3Hlt0346PRjHodKMlYIHxfeFMGlkAeDigVi8fcfZvRbPuI5i1+IFR4qg1+iUM2gPL99IRuM6kW41oVtgQDnTZjAKZb79CCCxmlKsIxXjIPoqcRvSmY9ufaY0X1QSqhCu8CwRUS+EDiXBZRJavn+jP5YerklHIdBi8O7MDZW6TNjH0OuLEGocKKBAQIOBRhAUXQ6HusQdKBEa7xOITY62Xh4AekofqvH1M6T2Myjt8D7m21PMwcE9IMxCtBhJFpaXnAQYo9AGKEYZJvU0fhMsyZa4nFVUqdrMMV5JsZEnhQYAl2peb8ndlNFDCOnAj3iJePYMUiuKxpsfMDXQFlWzI7kOInPocCYvYgNDruz8+G/FfhF2cbIuXwMPLDFIUA4UYfJ+5bR/8ALo9HrRBlkdVcIEFizFfJX7iX93Esfo3DaXaNww/HmEGphntSHouYQTFtoHMgAYSIw5CECunsZyzWZrE3gl0eSZ+gghteYY3oXCoWKxVaA9hKy+8S4PSg30Go5lNPg5r+WWs1XyD+olA4U1EYR4icIhFLhDlLxHNEL6EOBNBbqn4Gn+Jc4a6h9UenQkUStG0S+O/ZFU0lq1YUHQmbobRcaL9QK8Ig0Wl/qCNiUNGY9OMWidYC2i8kNw3dxTXxcKhH+2WPlLYr9p7R07jEyZR0la7DAG5RSpy41I1DL6Aqs1KTChZg3GcFBcZEO1wLeS00cD8wZk3N4r4WGAKCLAmFiWsbAOsAaS3d1F6brUuzn81LOGzi1fLLKkAxu0/smHz+0d/rY6MYx66g8st9Q+Jd6qSDlswaze8KN5sD/aRHatWArkrVobp0nzL3FkWvWNhc5pAaSdqZ7DGuAukaqjVHM4XKcUJeKit7a7pTidkYl7hD2IqjVpTTMZKIaMJ5shWUsGMuuqy47S79LUZ4QsisCKQafncB3AcpYBEtzWOwYsyxsYeqN02yrvTsTmrGoRagS8eWEGEIOi6FIo/ExtqTzCZ/VUWDRdSlM2tYT4lpHnSFJUbJc+GaSS0CU4cGJk+DOSEo8y5n9kJd3QU1YWU8MtePZMKp8kfCNiw0CadIfZEDMiVVXkjTb4jgx4bhsK0PuoK18MX5pU5OYLPTXenEYOooO0FKI1o/2JhIKVZD9fjiIrVbVJAfemPq6+WDtZvKymoJQVKyIsYz1bZpVOn8QNRgaDRCroBxUStIMJNFAvVBj0Yy2Ogw1uiVMPyRWTEbMsQ9tKVtVEs1YUzVhAWDRiaiHkS/UjwhxfxjIBtbWhfS0rAukLGgG0ETOE8GSEP8xQ91hyhjJQejWMmtlbNaQ7ExQHloXwMR6UuiOUBcrzHfxYw7fmEY4FGbpwBHmF3FTQpA4aXDTeypHEsCgOYX5iXQit2KtMxLy2GthYNcJgbwwnjWS4bmR0upbLxxNVJhJvU/bZ8G1L7izE1bOm5BZ79PLc2TYbpcOi6HR6iYVUDhtaSh8JkD2uWiNLE2gApjpKXYivKgCdEssHcSAx2EhOIcwUMxcTWXT+gxNKnsJZbkCWQw9CDIcOsOQBnQG/3COtBjHCikzuFlcW+QPX1rDUcX32Y0rBsvYjCwu0eOYAKKgpGGYGW/ZLxnUQRQ7AaPGpEqOFKTNMefripwxNWnect4fGcxP9ai10YuItx6Ft+iNG9UVgF6hZ201hTPxACCKKwvQzGlqC+xMotDzAu1uIq0hRFgr6JozzOWFiZG4Qlqk4QjGiGF1DVi4n9KShCG4vFZH4miMM8OZQDuCYILIatKTxLY60gEN946Ghti2ji6ZSzMwLA4x3l8jdk/TrV7ookHRwB1oYuLKrNGDYxuYMFppe8QzNesxS5hCkHqN/uZQU1GOskQ6i3Kl+PL5IBY1eT2wwAMrE9TUGvj2hUwDeBHb7h6pSjaYZOs5ENmGM/ElKaW27xBFAYRG08x7o7XVRYZod8wzXtYZjzZMnBUhB+bko3ktB4SLnYjki/XSyOlyHq5VEaJdxRrQU+SG7G5yRlinQ6p/wAiZttM9IpgNoIERGhD1IgN33Y/6jdORhSrlx0gsOsSPwy4Q+Axf+mHP3nHU9XoyBGuIGYLQQCIxR4CBOPj/IUqlxkKCxENQrXLtFQGa8xmqxcCMJcP4GOq+c151KIi6n5TDDonM1CXBhLB3VpDAy0PJLKMKLhq7QrEEYELJOly3tiSpR4X4BwiTiTmuZD4bpC0HfPFXn21YprrYUkTUxUWKckoLLWdmAQhpAj4VyJXosZX20ABaStwSDXzj0+aMFFdsvJvOIF5Si1q0YZwRn10q0zhAgdselMwbxiWoOWPifnM3vtFUoQlAKAIksQwDsjEnXdK3k5EBqVg3pCV2oHMvGVoMuloKCQgOk/Nu9x3j8D+v0bx9mqCuw05SV9U9GGQky8IDEFriGPhlRoF8NxzEy7+YIGDUxSglk5o2xr70IUIIDYNCWMcLdXHZWjdRq7G3IVlY2jsQN51hoxjHoyyH1Q7IiXPoyaN+NRbcgJRvKrAQB8LM+n8wGvulZAEFmYM2xblE8yx2nMv3Q/cLePZcQvluMDCYFo6rYVpc+WwjtYhE71GopbqJaGiS4BejCE2cRzlo/5RJw4PFX1C9eSIvhQxi4Y67kWElUkK6OqY3UVFGko7xwh3sG+gJTXMZWNmMAQVaEqRQqDAdhgJS+HeA5fMSzmh5bxrtjtrDILItk6AtYF8UgPHeq2gAaXgJY1RHnAC61iYtTcshV3hY/tcfwD/AGils1H54y+0FR2dGVZS0koHTz3+Spirc60A5OJomnClvBssYYHtXWv3rO0u/pgHtNNII34SKG0aLUK4bGnIb+YI0g6SUy8gNKcLKmAJvk4TZ2PaGC7fMvUzeNLuozVWS+wAnasysFFvz0Yx6PSkTUXeGjovMGGajG3cA1NbplzgXhQc24DLiwzYn8R0+H5jaxX8BIOLyQQXvC22lQNgDC7wdMC60yRteAzN+TKArFGAGcMOwmCF1OaJla3DOq4VhtjS2+pMDQ1xEcoWDFQgyEoibsIlaKiHaQKZhFawltNlHZ/sshpavcE/CCVFLglGc3UDl4mZck7CkmF6uhUNJ5IYAMNlCdQtwgkDyHTtZvD2DV4vxy0uL8HjSFzUOxZr8xhl1oBquwQFNT9SQthXN17ozUIQpYHNWwkPcDwmKN5HQmq6tm+nQiV4oghaEzKGfLcjZlxwmTfLN1mYrMVBRHA5iqW8jWuMSwQXDN1BVlQNMzBXLxeIXbBColMu5+R+Iy7savERQgHLAuYHfEuW582Qp85+p0DqnYIJ2VABTPXm0VBaUiLRRqRejHWLFZbEKlxtCZZg26TRLlRVqMuCko3/AGH9RcyvKwAdjVQAm4uN2V5ugHJk4CsEWfG5BtG10P6TWW6jG9hKVCEpjNCTV75jGi4cd3CbBMKGTznmKKMKpQEbOhcsVFUMLZUpQKWJa1gJVCxdK02Mtwf9i5chNMzWFaDd4QkmIirBBrAF6jFUV0MPhTs7wLjJK2aVNIxq4gVTJ1yBNQaV8udSK5Hpohp06Dmh/pFPCfKXxQzV9LmHFuhqi3ElCAAVTiiU1RuJEf8Adaa6zQb7xgBJpmcxDkXeooeiLND1MzQM8YJpZhxDXLZGGYmM5jiatQq14AmXg2c325eBMG184IiNGKFntRdwSBf6ovNQxkNBLwLcO4Ysb5sQcCLsvKukA30u1jbSHiE2VETS9YJS6XrMBkjGOvQvW9WMtTMvXopFeZcuasIrXGXhfQhGLi8UIZXpwW1qPNx1rsJAf3cMp4+HhQKDgzEdsGjYN3EmOg5lHvuyMotUDMJU2zGzG2JamGIAabBQRbNyJ7UgHNwXWFqNGGOsVTpGSwMq4p2zU3m+IK3lxGWCoEjRreNLyuRFb5vTBA/YRFveppObEXBfMF6zTp7oLYu5zNsERMkPGvOemHD2me/VthHaXt3FwRqZmx4ogEB9m2qJkLDwst0P8IKHus0lUrRdaBwRmYUj64tyn2RNvfHZrgQyNnaGmLNDM7BDHRkaFj4ZuXcUfMFMwg8UYWCwafy9iYkg1AG/YS16Ikk8OiC64gfBUzt1Cl80YuNxsvdY6hTjB4og2UtUbVd1jxJn2ghuwT0phua2bMYx16PXTh0zflvKmSWsiY303fMAehBLBiNCQa3Bxqg7WfC9E/DGv3KT0sa/zismz/OYgO5SngyvilUB2I00EXS5YCJLcw7SlARZehQzwivEUaN92FKEaAQh8fA2qoEoHl5q9L2QeOZHQbQoKBplnyUENcw34QAx1qghanVEg2CqukqoOFekTFnjNoIkAw1uJLZyJfJhLyTwhMX1QhTxJmxKIOXoNWnVkpYuOjHuNsucQofstTSW58txmEP0xy9JTGLReXJiJC4be6EEAEoGl3LzNcf3suxLhRVmsb21hKqtu8xVm8xPKK0SZ0yDabpC9s5ipgl7WPW6IJqFVQHlYiAFP7DpACk2f6IvIpDgSsiAL5G9TmwkBQGwKiWmG8IxF30fBE0uAIZeZxlF56Hw6pddmujufhJGMf4KQOJzFi0pO+xniD0sjlljkTUw85guvdQ7a5lA3+jtMCVepPyisOAwxj3T2RgeVN1Av0hVARZOWFbTPDQIinmK1O38wM03Idpej83Ca0AVWJUaTNd2Xh2ZfkmvJIhTQN3pMjt3U0HCEP6MsDmrSCDCWxc8YRh0kTPmFZoeC2nwGKuuFTKGRBIGsRXlb/UHktwyghcaPvCSIuIGDh0lRCtymKtK5hO+MQ7jEqTqyW7uGhlL2+4jEDawdDu4lgpUKBtV2sWWONjf3LWfwUZsIilG0I292yLxTgVpMpwV4J9kW/DQrj+7RDCd5trWUCs7GIytNFN2uEiwK6R1Yvv0DAxh3J2nqDjg3juPqC3rcCwW1UeKzxKz+5uVyUA6hy4lFdBuQnlepsMJ5KRmYpbNmLmWiqMj0DmDfR6cfmqL9RrVcFnIYlOAJwvuFclaH40hPZf0ytPiSkUEfikBCAM/cMtxqA+fAsSeGIzZqxB8Z90iExaZUXdmoNFdkVgNEQnmzmHxIlMcn4NUzjosE1gmu7QBag75qzJ79XtWKx7MoaO9Fx4AEcFh1Xfcrc4EP+mUgRvY+ghV9+Rgme2oP3aP6cf/AAyM4WDca3iN+VDB2YbJ+mZ0tOH3ENivuVO5cS2CIxhvU9iNHLHjvOxgiGbVSG/ewa0/tpKhQi8AaBUA45dAHLmPjBzxi0R9eSkI3Rz6uWlqAkjoH2MH3H/FsFmwwDKwvlKuBrS4ufmosN0/csvxuUHieZGmcY485jUQEFDgQNaDyoMMxNJ7ph17Yhor8sSghgF3dlmq+6bhfiE0GsI8xfJvLdR7wiRRj0MXUqYLGQhnSzoEyzA1r8zWHhazG7XML/BY7odwH2wq+MHzBTcIxVFZwLJhTQYVd1lnikPHSnhZMMdt66ElWG1P8cefCT9Ui3shX5whWbJoCp2JBFMsmWd4zzIP3VBcUukcFA9D5T6l1SySWz41nxdUMXU1erX4mlU/tcvFX4GwvhLiNCSiWbnz3uHXac4GFTT5EdaOksaghYLICuHe4MCWrg22XMOH4AzBl04v0kex9cbYjiYsI/UqpAr+j5ZGb/V2hFSSoCioch9JWKoErGjc9WGBvQ96JFUU8qV2PFeQA1NXblL3BqDEbDH7sAb4lWCjI2UR2IIYFmaDEocTFAmOPzJjuMacTLDasHcwQL5VHd39PogkEsTOiYr2Uix16PRXGJqEdxdBgsudlGacwoay48NmHMR1TP8AtIzSNg1v8kNh3T/w5ah+bPa6kNCK7Ulg/ZDLHqd4uerQDBHH9NEKJ9qfaI5ZpQNXBKlvRnTyZVTJrBukj0BAtGwRKIlvLDTIAXBuT2j493bXihsuFPqTUIyUhN7/AHCleHD6TuK1hxu1irLq5izOSOyvs7ko5XcgYX5jARqpZY5Z5R1lfjK2stdBtctJZDJGgqreZbES91XV4JaS7/wEWeyEbaj/AKmVpLpo7xem87iZOCaqbjLbQhGEFmXBiaFN1PU7LMub0fJq01C67CFfQ5jvIrCuktxKDEmpEXozSIKbhKaWnyS5oER8y/ZfaRhjGOkuWz7R2OODcKhLOjWBWE3yNfIlqf1prLAEGop+YwPdS5KcwD83qL/uwQ6FGWGKFy/sAViMugqqf9XKV3mXHuWU/jmDxtoXUNEMtpSs4YTGqVBR464BECQG2EG6SHM+DLwDgIMQCsYShUJ+Vac0unNaY5k4A/MLfIf5y8T8WIYVyBHKtFBuwMhy2Bqq7wF3coHmoIUzU77lSv8APZx16QOKAF6UWglBRg0quI1h3Ix3gtJ2OkA1c9FtrRn/AJQUqahucqyJU3Sg8O+gMAlYBTbliwutMMMFt8EsLCKX+bFQeAFVoQ4Jw4FfjJx7ylXm7WI6hmQfIZjFOvPvxGUN8eXeQ0wXI7C/GNwrJspe3FxquSsAbBWmCKYsO5GqO0YYCHjMLGCXpgljWPLCl1XEbLuqlQEtrmMwznNmlixYvUosQMRUyl1L6WPV1TEpWsbj7IEXT+/E1YU2+ENIzjj7RTrez9MRDrLrjEGllTijBhcmtHyzmp4tlTTtEZLSBhMt8EGFuVBg2palBVDg/rIjV+12/wABFIS9z+yxAuv5S/ARq+EP7jN1r4/gl3Ld3P3MSPAVFbbNe+dD23YeKtFrswFIFakE4yk9oQ0EjuQBpjkmgI8MKi8zcQkQM487Q1AAuFMEcSmIC9FJn4JgRoYr2kCh2gLHo0x21oap2TEpcGVYCFHphd2qF7oFdAbYHYxEKl5nK2vdioqj3uHK8soRc0umT+Ika8P91S6/Cv0uYEfmlnuWVHfi57DvkjAZdkyfmB7vWm5NhEU1QxN3qlpikXMVgU1PmOwxYxZOzLZlFhBYLre6FIsZcb2DRZZmJYNTDpdSxRZCrM/5BYuIMUg84lykUXZ+5qv1jrDnXer51RATulYeBmBNg4Ip1HAywQQPNqRFgB3LjhmhKANu1+JYv+odCXoZwR+neHms+7Mwg+/wUKaPyAvzYWB/H1BEbtYXToGYJr5IeFYNz6hKcg0XAvxA4qJYLY5GUw38y1FKsbPh4kDali1F1/Tox62TqM3okRnvL69tyY4gW014PDuQIA3B2YLYkBGNyZwYqDA3OAINRGvmy+2ZQ+HoeeWDL2Zdm0tQsK4XDGjgyAE8IeoIEzqz8Af1Lc4Qr4OVlv8ATzVsFZ7UCJupGCo5UfxZ2Xt5RpGphMcCRb2zDDgieFr0BiDgjCQjQ5ZYzw93klGRYa/NEUFZtS24QOAiKmbptNbGFdk7ODYpMG2V2R4ZQ4XyR/8ANFlvkwSzV0Xo2NotQYBgcavtBIMuMJyjjLKYOgdoKcvhcBPUtTtyxvBo7F2S6eG9ksWFpEEbP8vnnOpVEELWwarMGKE0eY1Vt5/gxcRwCUXKrmtCzULrJTeu9S3uTTg4R52gDj1oxizYE3fkISNdd45OSCiFtS2bQeCQNItfhK4R3X7T5Ndphp6WFP7Exmt3IFO7XmGcHRrfskN+V0xcWCvSlt87RmSx3xj5aDW6h6B8o/8ARKhruoikqTOYtYIpRx5S9ISz1znncLAuuYrEdN2CUi1Z8tGI2exKOvesSppV4WJ9pLuDOct3Cwz6ySK5TbET0mXkQ1GkQDFK4gyROhF+s40OUBvcZXusBKSvBP8AYmjrcy6+5XeUb3gysRjcpawBMi4iKRSoszlxRD0YlwhAnIwqWUFcQIE6XX6ssEDIjcwXIqjbGVrZPii/PS0cPfEmjwQqaVVPZNTzL6mGdA5Tcuvpgyolev8AuhXiDNAIRQIZG9Ja8W3Y7qee0xXbHFn6mjANOhdxx8E1ZKKPtXlYVgXjAIjHb2Cq42plA+Ga1HDwwjjokDlFODWChb8msH8zdynxut8Blja8IaLzFlOmzMCCR+O4pTgkGITS/wCFhiyKBkGbGWRrMWF9EShvDS2c9ojqpm7YNRguhB/D28G4XzDCrHfVlPiP20ZLHYM2xhVhbRrDw/pcmn+uNMw9WUSjK31qgRDiIZSGGAQj1mTXuDFOi4uJaLUyqS3jyLGqMxUTqBwzSDNEGXB7xi9lNkbOxHLCxfoxDR+PswEoQq6vXesSj21jA7r2Ms55/SRt2AhYI52/7EbbzCF6L/AXEzXi6NNTa3LMcRR9rlYt0H1Lw5FqzS4wnsTHlB+WhaHv3iSSGd2DM3EIjowHmW2XUH1scYffR9ygpw7kwyKKUR2ZZ7FXBEAbysEQzClXs7xyAFyLrF7gFjWsBm2825kytTnOZqq5d5d8fixGIxBDxCMVceZ0IfMtCU0jssGIvPaEhwloMvessSRlZT5NYTQSNAvF0SXg+XIr3Zh4iIqWssXKtY8oRZCuf4N4y2XcwGJbxnx0LjF6KXBVKlNFVUR+ZIZfaOPEWi6brFYrW3qRvpU4plJ3VMIB6K0wLbXoc4gBjJML5ZRBLdMpVs5qjQEHDPwMR8kEbkEFkaSBzJsguko9Ec0gRFTgMlNkCccsVq9msdtIhse5St0mi3g3GNRPiZUO3F68PARrsuMu2plVdVDCHnSUli6UY2CxupV8g/TC1X9rqpenS96ElsjNZUQMZ3wKYxYXmHiYaw2omfCB0PwIGaNM92J0xDp9rBUEEwUY0IhdNpWUhjZOGISFCoaG6OU0fMuuHL6C93ZmiJf4J87cNoGTWKg4zOHI8xogOl6hUu1/sKiuLg3cAYgsZUQpM1mCRc9LxLhMhP8AW0pFRjQuK5c0oKW8wpwooBglhra0JgKDAQ8MscAkLBcmljSZmhvUrF6oo4kvp9uFxiFdaqzEul4ND+LObdxU1ZdgZbL1HQvdv88Ovx9QHh9ibTPXxmhYGYgCuaIVFVKFQMPKkLCZLLLitY9vAoi3SjVQUM2AByJqSgzBK1+4KPmUhPF2fguLpUrVXkrGOOgPDvUpvTcA0e4BbKYyB2s7RREJRkeF9ohiUDv7Jm91YxyrT1dCVbe9Ivi4AqK0pBtFpDUga2uUkU6n+0liibdejUmK772y/lTL6OmvDAhZ3XHGuRCvsi9CNwCEIBYw8DE9zZjamXFYP2IITK3qJaUVe4ZjdLfMWey70G1OTaWXdWFGZmuTot4EZx8KeP8Ap0XZDEYGvSBllzEDXrtL6EWtJbHD7+Iugsw6DkVALB0Hlgw1TAhNwrKqvLC9CXk7FzuHAA4YS8TDci94Wj2swtyU/qVeDMWfAImbcAalpyvceUFeQkr8UYXhpjcXyAGM3HqvVUsKvYcQTUwq0lZLiPrXd65r8sTB0CTaMBlhAIBUbLY/FQRJG/kNOJexMVGYqJiw2ETGLGOuFJpHmemzokJUBWbxbn7Fxh0Gj7xStBbUyG66p3S1BGRyLsFUWl3sIg8erQvSCPJ+5VZAOMzxBALmaHlYMNoDy0t9rlzAbvI0DztBFxViHVnL7mIUTTrCYii52ikjO273MYWuSDCxTMpkzV+V5QGBJpm0KiKOkC8Lg6MQN2EWKKMRgU9wjWg0onC0TetibZ4PMm5LsirUqIrK0xzYIHkmSFmMHI/yI5RCJUIRhrLgj/C+pO8Ixasadq8XGMXQDGLAzFYcnBK6n9U3wWMA3aGL7uw2PE3Q3XFrNgQNkp9PRHhQnYKTW9hgp4R/ohrbgl9hhSqiLkqFpKPMI3xgxiKCjBzlpKdowiQ5z/o9COWcn2qxDgXJHiyXlBGcevmKK2ZKTcaI0B0xW+JZQnFWk5axlAw6rexCw3eZSNit12jUKm5rZ4WmOHm4spO2xHQXouYZecbxLupKcGSgFyN1PER1QIW8qBW7C8g7O4pxAlo3CoWQjW2CEpYHtrFhscS5RfSJrIfBD3CVZ3ZgolqMa1dQEawRruzErdRmSkwvv8rJo8LjyAmsPQ8jLQV2ocD0clnthOaVhAfcaO22o90SzHZ17CwG5htU+aJXHOJ6tUvgFdA6xKh+2WVnAJZNE/R2UlKZyo+0wdA3ovklULFEEXRaZmrIs0/1l2Eb1/UyhVvP9olLln4H+kWamTM7phIiKgJEjAS+o46gPZ07EvxKsh4Bafg6XFiy4+pqKR57UNVYxsEfJS3pDyykWvYB8sa7SPvEZxbV1+yFqKuCp6Zr1S4CYQGpCzu8xTNla9Ja5BhnIAHmPw3JZDmPPpaItdmGRIStCbS9UbBSMnBgpLqyG+GNa5LEeRM/QLYsC/IfjdjpRmgJ+Ew8GBSWMamddE/coVLImB1IXCFJfNVY2SFPrTDorKtjfT3wLF0ViUFpUYUUSxaBhIbi3VHUNaqmoBvyzpr8EEXFZRzrdCvVmHJgXC+EZDbmDrEhAlVMFm0vRxwzaW4qpm7vpssDHSA0cjMJYtoU4JlkrrU/pmGiteKJUdmMSnoo6CZDg/muammWwU3Wfm5TWQwGRlbcEWxCIdSYqQZIdeY5kXTiYKpxK+oPNiJWOzhyKwy+SP6eEHH4S+7iiTX/AFwQuy0wWGzUzIi7Ut+JOrdd/wC8JlwD9mUXUhr/AE6GFm8wzHoqujGZohLj3r2aKKt5iowxfRYrtcxV1l+fGbPdidxLDDA4VztrcHGI4j8oo8a7x4Jop3Y+jFEaEIjzePWxSGe5bLx7UT4ZuqSDazcOZvCRgKORJ2NG9SuLeTHuGPWbfEPwU0/Lj4ZSvlfajhY5qjZU5pZRu7QvXwZ2yf2SmoEOIYnyyQNWdmUlR5FwDsMfQpBAX0GPxZvEYCMTjtLD9EOQClPaQTKYI53jSk0qGCqWgpVtczSr5MAIg4D8/EogoNsBNhXKxk0r1c4GILkM8VbGAdE6EdrF3MNqQ0Gm8GOmz3lFQV5KhU7E2lxWDzK7EFos9pVaMLdISgVFTRhbUxKpqTFdpuPeI1d+omy0oqGi0AwQdg+8YS+/WCGtwu//AIZbqTbHPgsy3jj78S1azaKw0HP1KS4vhsH8mL1zv+JAVK3DiAmpEwx0Yht9ZOyRei1GFlJhKWv+0AAKAUGmISiNECIqBXhY+WM7EEDosWofcXKryQ4vzfYaDQouwjPnDy+LJQp+Fhh8oCorgaAzDQspFndahCXRzZIUpqstiJqCVHCilgHAzUNuv5qgBiKVl3NMLg5K6TGXXriiqx+zCzPSTG18JqMFuJvBJrMzUsOTG9N1r+iOqxDd4gul4q+KQb3g2VzaPVtGtqpXFA1qrmNWVtMuj25b0Qahm29l4LOBoUD1E7f60q2MRKoB4PqSFvBo6nlRS2fNiuYVCWt2B3WV5ByFPnF6gke4H7x8ITEmC62AyehmegVuMFqqmbC3MYTktNfnchoLmS8MaQ2hxFE1zT114iFtOZi1BKjCQchlty/ipQ5upQEdXOZQlIneOhu28RyLJxrFwy7QgLsu6r2iHZGOfoWhZaWRqoXlasfrxaMF3s5g/u2IaBPDFZ01Sp2/uaFjpmfkI6syQF3MeYcrUzQE2FA9RzcExT+WEaTwvJVcQPvD5IIaHrADU0YvBgJE7VQN5VmCxGkvgpt1Apt5V3dmBIAD2wcLS4mKALVUYeqtigx1r4Kg/wCMqRLANAvhgWgF6m/5gMOurCfmLIXGqaI6pwsV5gJYIXSXYjgrcXlhJi+2EEjwhcFop4QPhvjL3UGtSHuc6MrcTN11dQpubQAeXK3lNV4lShtDaBHizM3EYIoFLVDiLvEWTNm2jFjG4LanzcSGLeWawrYNbzAiuVrA5e/CAFMKpRhg5swRJNLFeBgPEcs6vsQwj8EsB3zasICGdNecYjtcBFqHMQ7thHVcsFRVMrR7ABKjMNAt+5QrwxFRq5R0fAR1DJwgxMCzV4GHSuK3kcV196+5noKfL6i93xSRBTvpEteZUtND3lUoZqbV/EUtCa+yO4TeLhZacVrAuiCeZqwoC0mWG43KAQGIhpF8z/349SwUdyhu8EyyLYFXVTF8wlcjYkwavlqC7t7lPL+CpYmdja4YpJryRnDyiWw/V4mapu8BE5K8yqUOXFcJRs8h8w+7TcSZAvcrp+Ti7hZw1e9QWENMs8aIzrQM3e3sIWCCDLVu7RkVaug3TK/NbBCAB46LJgYjVqgYQtguKTvERknSUbHyQLLWpVyt9++le8Zuk0GVHqkd8QFqkLAzDHSJefiNRCT3QofqYPoLv8MpeQ/A+1ga3421Yc8NOtRWxlAfm1M2K4KnXiIbJLCW/UIi4bIYiKggWAkEBdJb0lw+Gni4qSrJZDlSG/dgxCfI14EddUxtNmMXnNeRMkVY7eMWKse8Xw1hqXHep3gm5fCQbqsxrWri0Q0dIi6MRUXxUQF5n0RXZLjXMpzMS5qbR1j2N91lR1MqF1XsQiOPGp5IgUcqreb7hMAbwx22xlZZ3IQw5SXltK63N4gFKFMqnL3jkCrVKG03MqWhNQ3sT7SocFJTASrkNjVhNcpGqFe2ExWHwMKZ+JpeID9zA0JrxzWQMdBEUedHENohMKV05V2IBQ6HIQ9avEPQDUx4Lmq67JrHtjFpBHVc5MItbzAlPDqsbKy13dmI3imRx2S2FK5GMyB67UMSVAUd6JaKDe1kV3W+l1K+Zz9AZPKyyiecgSpxWLOfMTFKSCoCoYo0lB6075wRytJocj3iAEo5GOZIUr25g2hveNNwZZVIyoSDiGrY/USvEWhp2hfMXH8iPLtHzkOIfLvNXje0EuVu1D+0zGhzTHNe+qHNFLuR+WamC/MEuE76FXe71cCXygkPVCSPcQEgWV2Y4O40z0hwS9z2ywXY4I5xDuRZaeOGYqGd4FCJrk3Aht5F9oA+Yk459Zpv7imzsHf4isHleNgLgJvQqCCEkf4Y1IVfagQ20A2sS3Dq4eGvNsFFw6sOiz4cEr7Udi1WF8s6zSdOHmIAlR3gUPU6h2qETLZaZwjK0hywoqyOtoRKq5ieYm5nYos4CNWB4Z9xCXR7dq2gdapqVtKWI4Rp6Zk9rE5dkQX9MbotZgCBtwNoXkDsRyA1aulwo1KcShLTsweB1N0JILWariupVpWrGJWhdGYgIVCzRhW1hg01DgPoKLN1jQqmmdPMq4QAkoqlAlgwoGajKECMM1GDVGUjFkZxfV5PLLthWAykr+OGr5igPwAj7XqC6olhJBzqGh/OZy/THPzDXFyzfn5ciGmBGbBeJa18bP7QOid2PeVD5+hMww3F/wAxC/aTXDFuE4j6YA+fgxJTgqa3WK/LYrbf/pEvKE7AIKF5jqFYIcAiyWXQkiagxiOigFD8IQJLLVRCVQm8LNf9zBI72tt8ITuW7gDtNNB4wjjDeGUIbsM92eYjdPJtw/XlpuUh5iO0XTx/g/8AeFXccqKVKPMdVNdUl1e15JYzaxBCU2IqUNqUP0iR0b2wwncJLyWyviBUz53y17PwUg9cKGWsB2hvtKMQSzM5prMxANiKVMbnRDFsKnekWoEbtIb7OJUYH4YmpjsUIaNYMYD8xChosOZmDhZonkukSCFatJf6NFGfiYCz51YYWHVZYWqW+ZZZbhmEeGbdHmbGTHKlzCYuoobrhTDGfWV3g6LExEqAGhBZa0iKppAJC2MrgEw4GO08KGkRi32rB0cx8EKMICHR0TDBm+hbhC4UZBmuiM653JLSgbCupf8Ahhas1bjk+xFHIVSi9hsS3u+biH+iXR9US4pNQvz9UzI8P/TWXPTnIaGEcAd1RUlmcGEcEWRzASBsgYaOjWIGiPxj8Q0oo0GAjiwDB3sXMc4HwSotTtdwWA7UUw6XeREOe0qXFAIrokd0iiqXiLru+UoZsFGn3Ay7gCKrbyGJo2LvMVBXK1MQFOgg4M8PPMq7FmfBg1HBNM2v4cVG7t5l7QHGrA1CV7RGsVsxbKDBmIbRPFQSJ1ZlriOKSGg6CPKJFOIaaFHMNgwu8qY0hbK/cLrFGNckEqIJXhBehKMYVTU6WxDxB1ZLMLBJlZkNsIbd7xCWCkGQIPRJJzByx46tfiOxphsY4jpuWsq7wu1gboNDV6I7Vs8XAI8YnJTyLLF7ScbwEqVBmnB8EKIHsoJHf1b7wahyiC3Ormcqcyr9hLAGd1mowLMFGavwwiL4kmiQveDND4ZrvYicLXhIAzKrxcph6ptwj1DjICmYkiX6lyDfKOzSf+UY7t53K5tl18N4ly/jJAEbnAmrE7qJthlzOd0s18+I20HcjwfnEYy9keH8zgInRmU9XOI1xW1dG2Kl+ktvAZllJw4JkggxAmsDrEjUQpDYTyKoFxFRCfr62MNoPbWrhjsoTceZYhHhYlEQGmsjfuItO83mmKke3UF2bRKAyQxtRJaCgf6ZCNIirb3WC/USkDZ4lFFbuyJSHWhY6OfBgt+a1tfLKyP2WxvH8z8EpgQloS020tTgLyYFS2bp0hjT1lLVy+vx7y6DG1PVC9kpgaDfkjVU+WSoL3cpORm/EGVJzCzRTvFwA7awLJSXdZQxJqgLmpPyI+OIx38ERstaOpMncsg3gaNp4nMw6bMalpbtKFm85nH5eI11TBRz8gm88QqY3xMsaDxGEy/sQi/xym8DuTWFubleVTiLczGCxPMPu2i8QfOGHww7IEIBqTgxE+IMXBWvRMNEuzDUGzUnR0pGKr2lhieDZuZRIGlj7RBzOJhDXCS216CFdb4xhagVgBZGP65jYH6EM0DktC9DGyikW7KjkNstQ5BEhBRTtMYQR++C2YU4qFsPVHtwQEjbWdpXxY7dC1hIuu6NwsRwfK5itb7B1jcEKFjtQx2UQhVj5I32Q1NfZhux2M/oMR66eGu9JmGoh5qCaFNoBpHZt5iqAsMFxdvOIub0utxxcwgWHzECWJGZpnhl70UDgiwXLhrSYBXec4jOMCuY2q83VomrXuMzeuYI1PC48R+KlP5yhGHHmVnkjMC8kswfDiZQG+gwA9Hf9QMSugeW4dqXDDLd8wsQdOXMVwJADTiWOWZQl1D06aokd5oH/pCndAwFTS53SrFCPAylmBe8XW6WUT3ACwuYCGBqRwFLzMCGmeSPIstNPDU7BOWNMxNtmMGFzykoKwuHvzviZ60lG3MqJQtt1m6QHLiIR2kXVhZo6r4ZupU7/JFdyOoxKaLTDSL2gzHAihkiHjwyhifMrXabjFYU548QH2owGVowvmMlB1FzvWu9l5EUSyA6x3WYMN2gjIN2IiOsqBkLjRmHI+Ih/CmmfEjoj8wVynsx7dACYt2EBSmJTw0AM5itFRdxF2IiR5lSuQi05Y+6fAsRtmnDmoS+3pq4MtsGrMuomlHhibJv8EDMB1kkCZ3CXpELQnE0c0dohLhLagrgqltICgAwnj0++NsZwfw4JbEPkhKYI0xeLlxwdOgnMWSAbNzUaLMYFsSsWIVggFggHSksNXPTkwnE1fiUYjYKiYfJL9KaW3DGMpwrhS+mJCF9CgQUmJD0Iw7w9PulpJihvNmOGsTHyjMlggWDiZn1Qg4xC9ynIYLyxtsbeb9jt5WOIOfZAmDlo9T/xABEEQABAwIDBQUFBQYFAgcAAAABAAIDBBESITEFMkFRcRATImGBBiCRobEUM0JSchUjgpLB0TA0NUNiJERTVHN0k6Lx/9oACAECAQE/ACxvAqcWlHQfVAXHqU7Q9U/Q9F+FvRUwFzdOALR1K7t10DhFiUbFAC6JUEj5HTX0EhDeg1+ae7E8MB0zKxFYisSebxSfpH1UY06qTR36lAPA7oswwWQme3U3TZ2lBwOhV1dXV1dXV1dXQK4KtN5v4R7mJ3NSkl/wTP6lO3E/Qonwt6KLJYyGjqf6LvXc1dXKuu8Nls+QGmD/AM5c7+ZxKpnYi+Q/icfhoFcLEsQRP7qXoPqox4f4lJkCoPuz0R+7HaHObxTZisSDrLGg4K6urq6ur5Kr+99B7sos8eib/Uo7p6qTj0COg6BDJX8A6n+iuFcc1crEqybuqWZ41bG4j0ChPc7PBHBlgoBaNvRX7LoH9xL0b9VGfCf1J+hUG4eifuN6olXQKBARkeTqu/kbobqOcSeRWJB6xLEgezgFU/eenuXUh8fwTR9Sn8f1KTT0XH0CsSjcNHU+7tN+Cik8yxv8zgE9xFHTt/Ng+QxJt2tAVyg5XQzgl/hTN3+JP49VBuHopco2IlXTTmi5FyLrlBxaQQo5BI36q6DkHIOQKBy9VUfenoEe0hPuXgeYQyt1KlUm76Jrhquivl7u2s6No5zRfJ104EmkZyaCfl7lyo86eb+FM3f4k/j1UQ8BHkpzeNnaX4c1jurkoBYVG7A8ctD2XQKBQKB8PqpDd56D3LlO3x6IaqTQKTcX4QuHvbddhp4f/W+jSUwXq2D8kY92P7iXq1MyYP1J5B4hRZsPRS7jO2c4I76KOTHlH4rcb8U+vkc4gAWt81S1zZnhjhZ3PscLqKS/hPbdAq/gHVSbyHaUfvB6Ia2UuVuqk3F+ELh73tC60EI/5u+TCqYh9ZUkaMs33WECF/UJ8mVgiSmuczQ2KMgexvMajtkwFjsTQRYoyGJmEGwIAVyAm3YQ4cDdQyiVgcD2EBB5HErG4Jkl9ez/AGx+op2vaQiUbd8P4VxB8ypDdpJ5qTdR3Wo7q4e77Su/yjPzOk+lv6rZDu9ZPL+eZ/1v7skgYy5NgvttMXWxprmPFwQUUze7SAQQVX0fdvu2wZbiUxOx4HODXFo1IBIVLXSU58lSVH2phda1jZaBNCOaCY5A+H1RCKGiPY7774IDMeqePAnm7EdGq+SJ8lYFt/c9q5e6ko364WzOt0LVsBrm7KgxakZ+mXu1/wDlX52sQU3xOs1HvYA1wu30sodqxhh70eIaWGq2XUzz1Dw95ILSbEaEHh7k8LJmFrhrx5KDZTA797Ll5IsaxrGtADQ4ABbWo2Q2mjGTnWc3zK2SzDRt/wCRJTlitqU6upGmxlB6Zo7TpB+M/AqjrW1Tn4RYNIseYK/D6q90UO1/33wQ4J+6nbiINm5IAkI6Ikd01vEhNFzmmxhzSRfIdntdnPRN5xTfVi2NC0bNpRziafiLqZoBbbl7j8JYQ5SCPve8YAw8gn1DpS4veXW5oE4gtlOsJHW8VwPRNOIXWfY82ammxtzUrgC0nTEpmxVMToyQcQQaA9mX/FYSHbx6LadU6SfuWk4GZO8ysrKnidPM1g4lU9MyFoa0aIbo7R2Yrf7Tfmsdj92weibJId1jf5VeX8v/ANURKRm35I422umkkKXQKCxmGWjE4HG+3AlRv/dAcyQjha430C9oXxV9bs8R3wMimxOtbXCVSVdbTsb3cl2sbYNNiABYKlqzW05kc0Nc12EgKxw37f7hVdBLHIQzNpJI6clPCGhpu0u8kCG6hbMmayYtd+MfMKB453B09FhCITt0oktIKwtcTyyKfC11hpyKc53d56tI+Sr6oU8WIbzt0IAnMoBbIgzfKR5BBNcMVr+6ZH31UZxHMqEkE9j3hgvxKc4uNym6KTdUFxLdOaccptkVALs9VXwyTRPYw6n4i62x3tLVUrS2xcyT1zC/Z0smN9O4YGuDMJyuW5E/FbOpZKaneZD4nublysnAkZhEWPYP6qaMSRuYcsQT4ZIg44CADqSmkuccSjjklkYyPeJ+CYwMjDeXFMcHDXNEIhW8RaeVwmauF9NFk5t1IG4XE6WU8rqiXETkMh07I4y9wa0ZlQxNhiawcAim710fcuUCQu8f+YrvpOaJJNyrhM0Kk3VG7xD1UgyJ5hQfdlSanqvaWF0tVQubhuyOXI8buZZU7BBSxsvfCMzzPEoO8NvNFxLU7VAApzuSIBW2IpMLHs3ACHeqige4gX6+SoRBSMJOIl2rtfRMq4pHYb4eV0LoSka5o+Sa0F7HeSmmfR7QILy5gceN8in7XtdsMRPm7JS1NVPk99mngMgrABNWz6PugJXjxEZDl2FNBJR194dh7I+KfulR77eqkaQxRG0RWZcVt2qhbtChYXeJjhiHIPcLfRHdPVZ2sET4U4lB2XY9rnEBqdTSysLHMDmkWIKno/stGRdoa0jFnmUH4HkXaBYEHWwQlaw3fm53yTZpIgLeNqNbJwh+JT6yrdoWMHlmnySSCz5nnyurRN4ErGwaNTngjJNhdI8WVFs0RkPkzdqBwCseSwPOpQjb1QyCF/8AAPYzeRBcLDUr7I8Zl1rKV0kZDcZOQK7gWu6S2SltHLgzOQXtISzbbB/7b5kpkL5BdrHEc7r7O63haQfMpkDsAx620C7hxJ/dlTtLH4SLZIEELE4DIrG46kpzWvBDgCORUuzoHjw3aeHEJ2z5gC1wDhoC1QwVrDgIBHMlGCoI3W/FfZap2kfwIX7Oq3Z918wpdn1EMZke0Bo1zBRczmqPZ0VQzG97jnoMlFR08VsLBcIe5f8AwimbwTd4IyxWIuU8QSOucenBExghxY6wWKCSS/c3dzK9s7jbMT7AeGD5OKD34RbDbgu8I/E0LEPzuKLw3Uu+KqnB0pITTrnoUEe26jYZHkAgFCkk/MEync1pGt0yJzRZVFIZ4nxggYk32bp/xyvPSwTKKCiiIjvnzKAVvdPuFX9w6pm8EN5F9nEBvFB7+Q+aqHTNiJY0PdwbeyjZkCdSNLraOxdn11QJahri8NaAA6wsDdd1Nh3mD0uhHIL4pmjpksMPGV59UPs4/ASpHsJyKBtJ1CuiciVr2aJju7wvTCLA80Z4h+JGojbvGw5lS7Z2dAM6mO/IG/0TvaPZrdJC7oxyf7Q0DzbBM4Dk0C/xKpa1tZcsjcGt/E6wz9O0duqGnaST7rtSgVxT6hrCd0/FGsAywI1jycmhOqJydbIl7zckkqxOpKDQrBSPbIwYTkdUbJ2rbc042OhT3m2Q9E14PYdCqd7I3tEgBv8AJV0xp6V7mWxYbM6lGPakuTql3/yOX7Ofcl7wbEczqm7ObisZgP8A9so9nwF4YS4kg+WijoKYEAR3J5qKN0DcDGMw+Xh7M1n2hA69t1ftJTk1BTAYzzThcdlu0JwBBBRGFoaNEU1nhLinkgiwWThosDTlZWc063Cc7gpANbKofNUNYwuu1qjonkC7wq4OgqZI3E2e3IA5ZttdCe8MQtk15+apxNW1URt4IrEu0zKDGDMD3Ln3Bx7cZzsgTYEiyxArF2EoaoaBStu/JOY4qxVirgG1x2EtaLkgLvoXWAe25GQun5nJamyOTQ1ECyAwnqgPF1RagwBFuIJrC5+E5FNAAA5LaDGuqnXAOmoRiZphGt1so+GUeY/wQMuyyFJZt3Gx5J+MssWjNPBCBXcMOzRMB4g+xQsVYBDcCbEZDe4shTM/O74IUkJyxuv6I0ZJ8DwQpdm1Dn3bhd62U76+C7DE1uV7m5DW8yVN9ok/eyF5A3BzH5k65bia8kghQzyseHYjlvX0KfVzkCzrZ/hQ2jUMOdnAc0dp3blGL9VS1RleWPtc6dhWqL2A2uo2h3iQCrm/9SegRC2ccMrhzb7oCHaN0duyqOTZlJFDLLJK5jAC57y8uccy67uZVTtCCI2keL/lGZX2ynqLYXjocigmVBjgfCWghxv0RaRm1Nl55FMN2hT1VJShpne9pdu28k3bNMT4Kj+bJR1zpRdha8eWaFQQM229bIVbTkHuCdM2TIuB+SkpoZsJIuQcjiBR2dQEHEz5aqr2bA5x7lsbWHQcRz1R2YbgHE0DS2d0/ZjyPBb4WVZTVNG0SSRktzzb4gLc7aJm0f3jSxr94EEBTbSfHCZfssoGVg/C0m/IE3X7YqZP+3wjzcpKvaErz3bsDfUpj65jsXfuv0C/aW0IGB2NrurR/RQ7dqiQHRxn4hNpxWFkzzYG12jVfszZZH3lS3+UqppqfZ8kUrJy9pOd2YSAmV9E/IVEfq631TXxv3Xtd0IKsVn7tu2t2g4XbEQXcXcuiLHvPEkqn2ZUVDgA2wPEqDZFTEcDXYct8nFbo1UuzHMxfaJg8/hwjCn7MadyT0KmoJ4wHujPXVRXw+qroBOGDK4J1aCfmCpKGrG5UPb0NvopqKv1dK9/VxKx7RiNmvfkm7V2lHvEO6hR+0Eo34R6ZJntDTnea9vzUW3KV1rT2/VcJlY4x94HNLPzcFUbXpozd0rR0zU3tJTs3GOefgnbdnqcTHxFsZaQcObjfgFTyyxsAoKGOluM5pPG9RbIia/vZZZJZOLiUdmsdo4jrZPaGOcAQQDqE5ymJcCFTteZAACSqN1omrGAtqEOgKkd4igQm1NRHbBNI3o4hN2rtFmlQ89bH6pm39oN1LHdW/2TPaScb8DD0JH90z2kgO/A9vQg/wBlFt7Zzzm57Orf7XUdfRyi7Zmn5fVNkjdo4H1VwhSwMtaLF1UUIA0DfIKNrWaDVMe8cb+RTZwDmLJj78jfkVPUR08eN5yX7Wpic4nWRraGRpufQtVXLQ4GOYx4c46AOIHWwKDA9ocBcFGOPQ2Rgpv/AAgeqGBos1jR0CIDtQD6KXZ1HNvQt6gWPyVRsN4aRTzODdcDjkjs+dri14ITKNrUwd2QRwQ21O0nHC12eZunbbfg/dUvi0Bcbj4KLZu19osElTJ3MVr+I4GgdApZdl0khZC41bxbMDK/lwUWza6ueZO6ELXHioNgUUYBlBldxuSB8AoKamphaKFjOgRiaSjTRn8IU2y4Khha5zmqb2QxG8dV6OapPZKvbuSRO9SE/wBm9rM0iaejwpoZqaQxyZOGouD9FdwOaxLEmnRUp8KuhJINHuHQ9jQcAIIKE1tUJ2o1TWp20AHWa5uLkqiWSoOJ7iTw8k0vBIcsZGhQlfzQqHt4IG5ugVdZcym97fN5QVU+kjAdOQBfJCuoAfDAT54E7aNJ/wCWd/K1Rw0O0I3GOMxkZXtbNRx1GO0IcXtdkWi6Zsisqms+3VUj2jPu7qloaWlt3UTR58firjgEChYp2FqLxwKYbu7Jp4oIy97gGgZlP2jtba8pipWGOO9iRy83f0CoPZukpnNkmHfSDPxboPROp4X5OY0jkRdSbI2bLvUsXo0D6KT2Z2U/SNzf0uKf7J02sc0g62KGwJYbhsrT1BCdsqsboGu6FOoatpsYXKSuqpMnzPP8RUG0aqm+7ldbkcwo9u1jiAREfiqjaVSQML429G/3XezTmz55HeV7fRUNLHFeQDM5XQKyKwtPAIxtQjsdSsFuKAN01pcQEKY8SjAAN5d0NMSmp2BhLsLgOYVQIpC1sLPFfOwVLRRSk3OMg5gJlKMGHdHIKNkLGBjYw0DQBd2w6EhYCNCFgcgHIEpxvqFZnmm4RoqqvigeIhd8p0jbr68gvsMtY5r6s5DSNugUMMcLAxjA1vIK1l6+5Jm7tcibIPshJdU7/EAoxhY0eQVNE2R3i0X7NhkHhc5vzR2NNqx7XdclNR1EB8bbeoKLXDW/YEyRsZJJCfXRDekaPVSbZoI9Zgemal9oYGg9zE97uF8gm7Q2hWuwRxNfzABNlTbPneAamQAfkZkPUqGGmibha3CPILu4+D13J4PBRikGhBVpG6tKDzxugfNFcNCOhTWtF7XzVZWNo2+EY5DusVDRTCc1UxwvfwAV3eSD3ISFd4UJAUHBXCe1zjkEWvbqCmsLuSljkZqCiroOTH2VPthrGNbMwkaYm6qHa0c7rRXaG89SotpSs/Ep/ad0DbDMqXb4lN3EqDa0byAHZnksRc25QW2K2VtUY2OsGtHzTnyv3nEoRknS62fsCrqbOfeJnmMyqKiZQw9225F75oYeIWBvAoMPArC5XcEJTzXeXQc3kiQdFYC5JyVTtnG8Q0QDiTbvDp6c1R0AhcZZXGSU6uOaAasJ4FYSsKwqx5lZq/kg5Bxw5XCnfcgMOi2vqFJ/RO7G6hM3QqD75yP3foqrUorYX+eCh3pf1oLan+oT9R9E3RbG/wBRj6IJu6E5BDQL8TV+Eo6o6odm3f8ASp+gWyvv4f1dgTdEEUdB2DsCP3buhR3Qv//EAD4RAAEDAgQDBgIIBQQCAwAAAAEAAgMEEQUSITFBUXEQEyAiMmEUgQYjM0JScpGxFTShwdEHYoKSMEMWNUT/2gAIAQMBAT8Aa9yYbtKv5vmo/UOqj9QTvtXKfRo6q5ufki7wbK/7rS/9VorDkrJn2sXV37KY6/IqD1MVSbSDqgLynou6uUYnBWVllRCsrBWWUFFqLVY9lEPqB+Y9llZZQhsU7QpnqHVR+pOB7wlSi4QYXEgcghDfQp0VkGNA1KEbDsV8OCo2F7A4a5nOP9U3Vrj7qysrJv2sXV37Kb1D8v8AdQfdVT9oOqiF5vkms1KDSnRtdwsnxlqsrKyssqLUQQrItKt5iqQWh+Z8LToU9R65SovUE/1rmho89AspRB5ItB4IwjgiDHG93JpKjcI6IG2zDbqoGfUtXdD2XdhZEdJ4urv2Uw84/KoPuqo9f/JQfbHp22CMTpiA1MpYWgXbmKNFC/by9FUU0lOfNqDsR4bK1lY5iqX7EdT4LIbFFRatYovUn6H5ppAJQIMh/KO2w7K02pZOllPZtFCBu7L+10yNrWNFtgAsgTmgcFlCkFqiHq5TesfkUOmVVHq/5Kluah/RNaQgF3Vxc7JrCNAg1NanRtkYWOFwVPA6CQsPyPMeK2vyVOPqh1PhHpK5qAfuo/Uni5CaASUGgHw4gbUx9yFM0udRs5AH9vBYclOLVUPzU3r/AOKh1LVPq75qkb9e8+3b3WY25BCAoMsiQAFntcBVkXxENwPM3UKyI8HH5KD7IdT4dmu7Kfgm7o8Ez1FcfDiX8uPzhWvWxj8EQ8NR/Nw9CpvtP+IUMcl2eR3DgptHfNUn2knQdjfUOqpmh8tuJGikiMdnTeQO4WubI4fDGBuTffZVmFiOIyxEkAXLTuOxrsqqqcNJkb6TuOSLUWqysuPyUOkQ+fhvoeyD0pu6KZ6lx8OJ6xRjm/8AsoBmq5ncgB4ZmOdUREC9gVFT2dndqUxmmt1LFFNcOHQ8Qo4HQTSA8hY8+2GSXvWBriCXAJkZlddwv5iRdSxtbYrPcZTtsfmqqAwTOYeeisb6FanknhpFsoARhjPBS0+XVuysra/JQ/Zjw8+iIUIGnVMFz2DRxXHw4lqYRzcVRa98/wDFIfDBCZZLAXJCZhtU1ubID7X1Uomi0cxzeoQCmGgPLta4scHA2IIIKwSaXEqdwL2CRh10tpzUhN98yzRh4a57Q47AkAlVFJDVMs8WKr4WUMrWXL8zbovc9wvtfYJ6aNFYEKWLKfYots5R/Zjqh279h0UPBM0eUdwm6uQHmcLndZ7SZPbwYkSHQ+2ZUTbU7fck+HDXZatml7ghONmJgjlc5vlcOtwp8FcXh0JaGfeudljVHTwUTHRtaDnAuDckEX18FHUy07g6NxBBFwDuORVRjTi0d1CQeZOyMjnGR5cS/Q3WDVslVeGQjMxt2nmFjsgfiD2j/wBbWt/um+oIAkiwvwATcPrngEQOHXT903Cq8n0NH/IKvoTStju65e259inev5JgAaPCU5Q8Oq1DkSEDYo+pxUTT3zn30DrfqnvyDhfkjUZZA05bE27MQIdLEAdrrvXxRMA0sP3VK8va69/V4GSGOZliLnTbmo3yCPu3Oc8bEkKKmbGG5I2tvy0UjWtaRzCxdhkDWZjltcdRZPa5ji07jtiBzgJ50KYC4PAGtgqaSaknZMARkcg8uY65uQQVoWEhoBssKomQQd85ozu29ghclVsrKanLzwCqKqSocXP+Q4AJ4tIeibt2Ds7pxH2zl3AO8rz80YYm2zOf8ygyD8R/VDuhs7+qaI3HQqwabAIqRpbTDXd90zLkjLuIFlK0id54AgpuZ7G2JBNtlhH0Krq76UYbh9a0xiqLnuDS0va1gucwJFrrHPoP9AWPioZ6SWjllcGNkiLjZ9ja5OYW1X0r+jH/AMRxv4FlR38UkDZWSFuUm5sbhB4Ly3l2yfaNWGV8VU0B4DXgWOhJJ5gC97pksjLtdnAHBwsf0Trym6xKIvhDm/cKq49jpcAXt79g1TTlcCnAPuOJ2Qe5tiDuLKOdzd9RsQgwCQgHRwNj1WG0hqpbH0NN3f4TyNhwQ3WO1GYxwg/7irKdtgHJp8o7QhFFyTmNZqApWg9UAg0uNhsmtDRYI3un+kqokaImtHsSmzMcIADqDqql9pSP9oX0XraHD8WoqqsYXxRHMbcCBobDkV9IK7D4sahrsKqppJC8zOfJcObIH3FtBpposE/1Yw+QOgx+jlmf3szxVtOYjMSQzLpoNhZf6g/SvDvpPi1L8BE4QUkL2tlcMrpM5HA7AWUb7PuDqgbgHsfrK3oqSd9LMyVhIcwg6ITsnLT3hc4gX0RLWt0T5Y4InSS6NA/VPmc+V8lrZuHtyTmi+iCKJBbG73ITmtc9vuhobKLMXMaNSTYKmhZTRBoFidT7nskkbDG6Rx0aLlTSvqJXyO3ceyUXjcgLDtCt7lW9yu6ZyXcx32QAboFYI6WTvSeiljcTJa48zLfobqMOD2HUWIVULzHoFB6fkE6jqqiYviie8AalovZZSHgbECysBINTsmAZwmekdETbgmxtuHHdDTULAaiMmSN/2jiC2/GykmijaTbXh7lYj8RWSM9LWgCzP7qSimibm0cONuCsCEWI7KWS0RZexz3UMMddhjXMjDHlgIIFvMFHgl7OnlA5tZr/AFUVJSU5BZEC4bOOpTc7jqgFitcJ3dzGfI06nmUTZAp/pIPEJlyy55nw2KHZxQThoEdQVUi9O/omnOd+SqPPUZRqdFC3LcHcWU4Hf7cAg059uATmWseN7KNt37poF7Zk1lnb3V1GWBpzJlRDFI18bixzTdpGtiqWt+Lry5zXOc4OyAC9ivh++ZESx4LiQ4DS5TqWWSN3dDLHF+pToY5HEH6t99+BQoo7gOn/AEao6Ojbu17z7qOFkesdOxvvYXV6g8QEIZDu9Ngc3UkouLIy53lDRclV+K980xxXDNieJV28UXMGwTpHnsIDdBtfsHYFcodoR1YibAko4hE+zQwm/NQMhnYXGNrfMRp7I1ZMhywtJvvfkoCZafvdBz+SLi+S5TpmMNjI0HoviGH1PuPZGZokIYdOBRnaBcyC6pMr48w1uSnAglNA4hFo4BMkkhIfG9zHDiDYqDH6uIgPa17OI2PUFMxql3jeWXddzHqpmwuUZ85b7BpKElCNpHf9Su+o27zgdQV/E8Oj8pqB/wBXf4UGJUVRK2KKXM917DKRt1TIZNyFiOJzUTxHHG3UXzO1U2IVlS0tklJad2gABHtKKeNPA1FbIHsCPoKcLtPRNgkuCGqJ1RAzK0NtcnVCN9iMzdTcoGeOPIJrN5BQNIbvf3RiF/vFd0Pw/wBV3YHAIM5ZVRMLIWgqQDyHmP27G6rS9lZNjBIUkjY2gkEj2RrIuDSVLOJHXsQnsa9xJuqOUUlTHMATkvpzuLI/SeqHoijHW5VRXT1sgdLbQaACyB7QezRO1CHaBbsIVuwL7p7Mu2qys5lYbBQ1FWyOqqPhoT65chfYezRuVMGiR7WOzNDjZ1rXHAqlqxFEWfDwP/3ODs39CEXxlxNnDXmrtI0jJQbIdowF3Uh4gKGN7WjTgEW54Pdrv3Vkxji4AarbTsZupGiUvZYIixssruSipppnBrWOPRpP7IYDiT7d3Tyu6syD9XIfRnGSQDAG+5kZ/lM+ihZC1z6kd6QczALBp/MM1/0Vfhpw5rA+dr5HH0tBsBzufHxKPjCHZ8OXE+oIUt/vf0QpQN3lCniHAn5oMjbswBaDYAK5sjsqduV5J3G3VC/EpjhZwOxCa0EauCYwZhfQcwnxFlzv2R6uCmbJM1zoyRb+qw2FlRWRiS+RpzP6BMqcKh1ZStB5iJqbiLXtAax9rE25AcbBPxB2toXa7H5ZhfknYjUvic9jWgN9iVJilYQSZcoHIAKeZtS7vHvkz+/mR8BRTvUfAArKxVk0douQE3ftPaNSE1vE6poHJSSahvtqog1wIceiOaM7oSuB3V2vF7WKZGcu/BQl3puqcU9OHEMs5255qXEY2kgRkkLCpW1FFFIywyPs4kWd6s1uiMH18pz+YsF/cA3VVJBh9JMS60k12hm5NrgdBqnSyPFi7Tw2Vux/q7Y2hwF9DZaHYhAdoHYdym7IaK4RKsbbFBNa+RwDGlxPABCkqmtLjE8AGxNk0EbgjsBzOc7mU3dE5224hOcS0A202Qfl4XWd7uOiY/UGwTpGtZmFy1OcXEk8Vhsj2UoyuLbk3sbL4qc/+x2gt72WK3Jid1/8Ltz2moGezRf3TWx59HFAghAHkjI8VZjOxZcKxAQTvUVJMI9wSbI1TuDAviZN8jf1KFXGAczSDyUWI02Qgkt/r+yo6PBqpjJRUSSXcG5bNBc82s1rdSqZlJAe4jYxhPr9nfh04pmTNkkiFrGxHsp6SNzHMcwFjvSOSiw+jaXfVmwH3zcJ+C0rx5czCdrG4TcDDX+aY26WVdQMhYJIrkD1Df59g7GRvdsFMSwFvPsw8j4YdSgViIzQtP4Xf+Ao9tTJFVSOdG0N14C1gqXD55ReNlx+I6BfB1FPfNGjbkQnRB0okBtYWQcDo8fNPhtqNQn+pU9NWVIIgYx1j5syOE1Ib9ZSgnm1SUUMZs5rmFOpmu2kv1F0aBwIOSNybHNDsxw/QqKvnps+XyhwsRlIUeM4jG5pZLtfW5NuiocYlZGPiHSukAJuXDK7kDbqv4vGI87HCQutcO8oHRRYzFtNfKfwkEjoqapo65/dsnAtaweLE9Lp1HA2M53BwtqDxCiwuJ0mQ1TDzLQXAdTshhNLHtMXH8qjp6GFvnY17udgP2TxSPbl7poHsv4dQyutkLejipMFprEtkkH6FOmfRtfCwBxF8pO11/EsWafs6Zw9swVNPU18MsclOGOyjKQ8OBTqKsZvA/5C6cx7PU1w6jxPNmnwUWHCwdKLDcN/yg5rQNgAFUYnT0zSS65HJTYzRzDO9hJB0jAy36uVVijXZfh4XM55jmUeJut547DmCoMQgeSwSNHsdFNYEW5LD5+5LzrZ1tnEbdFHWUx9cLHH31/dQ1VH92JjegARbQyDzRsKOG4dJs3L0Nk/BIvuTOHWxT8GqLeSSN39FLhNUN4A7pYp1LHnyGMtdy4qHCZpBpG49VFgUrvW5rUzCYYHNe193tIIzaDRTZZHXrKt89jcRM8rFLiUpZkjjYxn4QEK8jQtCYS5oJFiRsowmgAqQsDDc26qsYDK4jmshKwxpbImtNgrFOggf64mO6tCdh1C7eBg6XCfg1C7YPb0d/lOwKA+mZ46gH/CdgMg9E7D1BCmwauAGVrHdHf5sn0FYz1Qu+Wv7J0UjPUxw6jsNVUO0MuX2ClldfQlx5uT3Pfu5PY0jUEe4TqcOGjrp8RA1GyhgfPJkYLlHC5wNHt6JtLVxuGXTXcFUbKwyubJNE5jRvmaCemYtWYscQHbcijVPB0uSjV1jt5SOmiJe7Vz3O6lAuadHEfNRYhWQ2yzONuB1/dU+NtzAzxDNtnaE2tY9gcxwcCjUkovDwQdiEcJgcBklLfkmYS0P+sqSRbYf5KfWYbROyU7O8ft5BnJ+ajZX1DM8jRTtPAnUBPxGipG5e8MzhyU+NVkhPdkRN4AAE/qVNPPOfrJXP6lZ3DYkIVMw+8qfE5oXXytKi+kpAAkg/RyZ9JKM+psrfldNx3DX7yEdWlRyRzMD2ag7aEKzSsoKyItsph5+wxsO7GnqFkIUhs70lZLoxOuhTPfwTcOlyEua7KoomQ6MACzNKsw8l3I5I07T7IgDREBWTXMG7bolvABFUbax5c2nvtrtYfqjQ159VQAfzlDD6s//q1/M5STVtBIwPlbICL2vfROfAG3mLQ0t1DipcWp4Hu+Ep2A7ZyFUVlTUn6yQkcuHaUQbrXkm9kFNLO8MY0klCiw3CoxJO4SScAefsFWY3VVALI7RMPAbnqU2WRmocQfY2TMTrmbVEv/AGv+6jx7EGbyNd1aE36SVGz4oz0uEcbjebuicOhBTcVpHblw6hNrqR20rVHR0zCC2FgPRTYdTVH2kbSeY0KkwKjaCQJP1UGG0jHawud1Ka2OL0RMZ0CxOpfJljJ037LLVAkLO/mUJEXNI21RKzhA3VioS/OAxxBPI2VOJYLmd4LbeW5uqusniAytDA4aEjVNqckmctEjr/f1Ce98ji57i4niVr4CEQspQaqWhklYZXkRxDd5/snYgylaWUjbc5DuVJI+V5c9xcTuT26KysU3QdougUQCE5llK3y7KqOaZ3topHEC4Tq18Z1aCm4rF95rgoJ46geQ3+SseyyZEZL2TKJ52YT8lHhNU7aOw5lMwR7iO8kDW8balOw6gpLvkmezq4C6mrYGOPw7CT+N+p+Sklllddzi4+6v7LMrhaeDValUVGal13Etjb6nKur4nQilibmY0AZjzHLssFZWWUqxXmQvbtaSRfhz3QK0KITgqnChK4ujeGu5FfwowsvIQ4u5KbDIX6ZVF9GopCCdAo8CEQsyylw2aMEkEDmpQBoP17MGp4xSCR7blzigWN2aAs3MqsxmmpyWx2kf7HQKrq31kvePy3tbTwXHbbtAJIAFyVTYUGM72quABcMH91WV5nAjjaGRDYDwE+D5ooJoFlgPpTvW7qmoo7lP9ZVR9m3qvvBQ+lN3WLfyjlVemL8qKw3+Qg6H9+zFP5B/YNz4Ruh4MF/+xi+ar/5efp4T2HfsHYN0N1//2Q==" data-filename="ADMINISTRATIVE-ASSISTANT.jpg" style="width: 650px;"><br></p><p><span style="background-color: rgb(206, 0, 0);"><font face="Arial Black">This is just for testing only !</font></span></p>', N'Hi Sweta !Very good Noon !This is just for testing only !', 28, CAST(N'2018-12-05T15:11:01.830' AS DateTime))
GO
INSERT [dbo].[GroupMessage] ([GroupMessageId], [SenderId], [MessageTitle], [MessageText], [SimpleText], [WeekNumber], [CreatedDate]) VALUES (2, 2, N'hi', N'Hello you&nbsp; got notification !', N'Hello you  got notification !', 28, CAST(N'2018-12-05T15:47:21.350' AS DateTime))
GO
INSERT [dbo].[GroupMessage] ([GroupMessageId], [SenderId], [MessageTitle], [MessageText], [SimpleText], [WeekNumber], [CreatedDate]) VALUES (3, 2, N'Group Meeting !', N'Message for Group Meeting Alert !', N'Message for Group Meeting Alert !', 28, CAST(N'2018-12-05T15:51:32.230' AS DateTime))
GO
INSERT [dbo].[GroupMessage] ([GroupMessageId], [SenderId], [MessageTitle], [MessageText], [SimpleText], [WeekNumber], [CreatedDate]) VALUES (4, 2, N'test', N'<p>test</p><p><br></p><p><br></p><p>test</p><p><br></p><p><br></p><p>test</p><p>
                                                </p>', N'testtesttest
                                                ', 28, CAST(N'2018-12-06T02:55:58.880' AS DateTime))
GO
INSERT [dbo].[GroupMessage] ([GroupMessageId], [SenderId], [MessageTitle], [MessageText], [SimpleText], [WeekNumber], [CreatedDate]) VALUES (5, 2, N'تجربة  البرنامج', N'<p>السلام عليكم&nbsp;</p><p><br></p><p><br></p><p><br></p><p>مرحبا</p><p><br></p><p><br></p><p>وعليكم السلام&nbsp;</p>', N'السلام عليكم مرحباوعليكم السلام ', 29, CAST(N'2018-12-09T14:18:50.343' AS DateTime))
GO
INSERT [dbo].[GroupMessage] ([GroupMessageId], [SenderId], [MessageTitle], [MessageText], [SimpleText], [WeekNumber], [CreatedDate]) VALUES (6, 2, N'This is test', N'
                                                testing point<br>', N'
                                                testing point', 29, CAST(N'2018-12-09T15:01:27.620' AS DateTime))
GO
INSERT [dbo].[GroupMessage] ([GroupMessageId], [SenderId], [MessageTitle], [MessageText], [SimpleText], [WeekNumber], [CreatedDate]) VALUES (7, 2, N'This is test message', N'
                                                Hi trhis is message<br>', N'
                                                Hi trhis is message', 29, CAST(N'2018-12-09T15:04:37.440' AS DateTime))
GO
INSERT [dbo].[GroupMessage] ([GroupMessageId], [SenderId], [MessageTitle], [MessageText], [SimpleText], [WeekNumber], [CreatedDate]) VALUES (8, 2, N'Test msg', N'This is for test', N'This is for test', 29, CAST(N'2018-12-10T15:04:20.063' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[GroupMessage] OFF
GO
SET IDENTITY_INSERT [dbo].[GroupMessageDetail] ON 

GO
INSERT [dbo].[GroupMessageDetail] ([Id], [GroupMessageId], [GroupId], [UserId], [SenderId], [IsRead]) VALUES (1, 1, 0, 4, 2, 1)
GO
INSERT [dbo].[GroupMessageDetail] ([Id], [GroupMessageId], [GroupId], [UserId], [SenderId], [IsRead]) VALUES (2, 2, 0, 4, 2, 1)
GO
INSERT [dbo].[GroupMessageDetail] ([Id], [GroupMessageId], [GroupId], [UserId], [SenderId], [IsRead]) VALUES (3, 3, 0, 3, 2, 1)
GO
INSERT [dbo].[GroupMessageDetail] ([Id], [GroupMessageId], [GroupId], [UserId], [SenderId], [IsRead]) VALUES (4, 4, 0, 3, 2, 1)
GO
INSERT [dbo].[GroupMessageDetail] ([Id], [GroupMessageId], [GroupId], [UserId], [SenderId], [IsRead]) VALUES (5, 5, 0, 45, 2, 1)
GO
INSERT [dbo].[GroupMessageDetail] ([Id], [GroupMessageId], [GroupId], [UserId], [SenderId], [IsRead]) VALUES (6, 6, 0, 7, 2, 1)
GO
INSERT [dbo].[GroupMessageDetail] ([Id], [GroupMessageId], [GroupId], [UserId], [SenderId], [IsRead]) VALUES (7, 7, 0, 7, 2, 1)
GO
INSERT [dbo].[GroupMessageDetail] ([Id], [GroupMessageId], [GroupId], [UserId], [SenderId], [IsRead]) VALUES (8, 8, 0, 3, 2, 1)
GO
SET IDENTITY_INSERT [dbo].[GroupMessageDetail] OFF
GO
SET IDENTITY_INSERT [dbo].[GuardianMaster] ON 

GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (1, 3, N'Al', N'M', N'9652202649', N'6101 Wilson Lane,Bethesda, Maryland', N'6101 Wilson Lane,Bethesda, Maryland', 3, N'Maryland ', N'20817', N'1', 0, N'HN001', N'Only dummy entry here', 0, 1, N'2', CAST(N'2018-12-05T13:04:09.460' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (2, 5, N'C', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, N'2', CAST(N'2018-12-05T18:42:26.947' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (3, 6, N'Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T11:57:01.857' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (4, 7, N'Jehad', N'F', NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T11:57:52.970' AS DateTime), N'2', CAST(N'2018-12-09T20:10:39.397' AS DateTime))
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (5, 8, N'Faisla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T12:00:10.973' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (6, 9, N'Mahmoud', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T12:01:02.997' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (7, 10, N'Middile Name', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T12:04:03.710' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (8, 17, N'Saleh', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T12:46:02.007' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (9, 18, N'Mortada', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T12:51:57.290' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (10, 19, N'Muneer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T12:56:19.107' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (11, 20, N'Mohammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T12:58:09.373' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (12, 21, N'Mohammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:01:12.430' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (13, 22, N'Ali', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:02:30.920' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (14, 23, N'Khaled', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:06:44.297' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (15, 24, N'Sameeh', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:08:13.493' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (16, 25, N'Abdulraheem', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:12:07.190' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (17, 26, N'Abdulqader', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:14:19.200' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (18, 27, N'Ismaeel', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:22:53.147' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (19, 28, N'Mohedeen', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:24:14.613' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (20, 29, N'Saadi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:28:05.477' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (21, 30, N'Mohammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:29:04.607' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (22, 31, N'Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:31:48.277' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (23, 32, N'Mohammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:32:48.250' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (24, 33, N'Bahaa Aldeen', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:36:53.483' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (25, 34, N'Eyad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:39:03.253' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (26, 35, N'Yaseen', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:41:50.373' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (27, 36, N'Abdelqader', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:43:21.097' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (28, 37, N'Fawzi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:45:29.720' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (29, 38, N'Mohammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:47:47.663' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (30, 39, N'Matouq ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:51:09.727' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (31, 40, N'Matouq ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:52:43.390' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (32, 41, N'Sameer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:54:09.983' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (33, 42, N'Mohammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:57:44.227' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (34, 43, N'Zakaria', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:59:09.743' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (35, 47, N'Abdelghani', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-13T19:48:51.873' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (36, 48, N'Hasan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-13T19:50:31.390' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (37, 49, N'Marwan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-13T19:55:36.147' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (38, 50, N'Issa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-13T19:56:50.460' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (39, 51, N'Mohammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-13T20:00:13.483' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (40, 52, N'Abdulah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-13T20:01:19.280' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (41, 53, N'abdelmuti', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-13T20:09:12.600' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (42, 54, N'Ryad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-13T20:10:30.887' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (43, 55, N'Yousef', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-13T20:18:53.550' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (44, 56, N'Mohammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-13T20:21:04.813' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (45, 57, N'Mousa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-13T20:25:02.450' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[GuardianMaster] ([Id], [UserId], [MiddleName], [Gender], [Landline], [CurrentAddress], [PermanentAddress], [Country], [City], [ZipCode], [EducationLevel], [Religion], [IdNumber], [Notes], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (46, 58, N'Ayoub', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-13T20:26:07.150' AS DateTime), NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[GuardianMaster] OFF
GO
SET IDENTITY_INSERT [dbo].[GuardianStudentDetail] ON 

GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (10, 2, 8, N'2')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (11, 2, 9, N'1')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (12, 2, 10, N'3')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (14, 3, 17, N'2')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (15, 3, 18, N'1')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (16, 4, 19, N'2')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (17, 4, 20, N'1')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (20, 6, 23, N'1')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (21, 6, 24, N'2')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (22, 7, 25, N'1')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (23, 7, 26, N'2')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (24, 8, 27, N'1')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (25, 8, 28, N'2')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (26, 9, 29, N'1')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (27, 9, 30, N'2')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (28, 10, 31, N'1')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (29, 10, 32, N'2')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (30, 11, 33, N'1')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (31, 11, 34, N'2')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (32, 12, 35, N'2')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (33, 12, 36, N'1')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (34, 13, 37, N'2')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (35, 13, 38, N'1')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (36, 14, 39, N'2')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (37, 14, 40, N'3')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (38, 14, 41, N'1')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (39, 15, 42, N'1')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (40, 15, 43, N'2')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (46, 1, 6, N'2')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (47, 1, 7, N'1')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (48, 1, 3, N'3')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (49, 5, 21, N'1')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (50, 5, 22, N'2')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (51, 16, 47, N'2')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (52, 16, 48, N'1')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (53, 17, 49, N'2')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (54, 17, 50, N'1')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (55, 18, 51, N'2')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (56, 18, 52, N'1')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (57, 19, 53, N'2')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (58, 19, 54, N'1')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (59, 20, 8, N'2')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (60, 20, 9, N'1')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (62, 21, 55, N'2')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (63, 21, 56, N'1')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (64, 22, 57, N'2')
GO
INSERT [dbo].[GuardianStudentDetail] ([Id], [StudentId], [ParentId], [Relationship]) VALUES (65, 22, 58, N'1')
GO
SET IDENTITY_INSERT [dbo].[GuardianStudentDetail] OFF
GO
SET IDENTITY_INSERT [dbo].[HouseDetail] ON 

GO
INSERT [dbo].[HouseDetail] ([Id], [HouseId], [StudentId], [AccademidYearID]) VALUES (1, 1, 1, 3)
GO
INSERT [dbo].[HouseDetail] ([Id], [HouseId], [StudentId], [AccademidYearID]) VALUES (2, 3, 2, 3)
GO
INSERT [dbo].[HouseDetail] ([Id], [HouseId], [StudentId], [AccademidYearID]) VALUES (3, 2, 3, 3)
GO
INSERT [dbo].[HouseDetail] ([Id], [HouseId], [StudentId], [AccademidYearID]) VALUES (4, 3, 4, 3)
GO
INSERT [dbo].[HouseDetail] ([Id], [HouseId], [StudentId], [AccademidYearID]) VALUES (5, 2, 5, 3)
GO
INSERT [dbo].[HouseDetail] ([Id], [HouseId], [StudentId], [AccademidYearID]) VALUES (6, 1, 6, 3)
GO
INSERT [dbo].[HouseDetail] ([Id], [HouseId], [StudentId], [AccademidYearID]) VALUES (7, 2, 7, 3)
GO
INSERT [dbo].[HouseDetail] ([Id], [HouseId], [StudentId], [AccademidYearID]) VALUES (8, 4, 8, 3)
GO
INSERT [dbo].[HouseDetail] ([Id], [HouseId], [StudentId], [AccademidYearID]) VALUES (9, 4, 9, 3)
GO
INSERT [dbo].[HouseDetail] ([Id], [HouseId], [StudentId], [AccademidYearID]) VALUES (10, 3, 10, 3)
GO
INSERT [dbo].[HouseDetail] ([Id], [HouseId], [StudentId], [AccademidYearID]) VALUES (11, 2, 11, 3)
GO
INSERT [dbo].[HouseDetail] ([Id], [HouseId], [StudentId], [AccademidYearID]) VALUES (12, 2, 12, 3)
GO
INSERT [dbo].[HouseDetail] ([Id], [HouseId], [StudentId], [AccademidYearID]) VALUES (13, 1, 13, 3)
GO
INSERT [dbo].[HouseDetail] ([Id], [HouseId], [StudentId], [AccademidYearID]) VALUES (14, 4, 14, 3)
GO
INSERT [dbo].[HouseDetail] ([Id], [HouseId], [StudentId], [AccademidYearID]) VALUES (15, 3, 15, 3)
GO
INSERT [dbo].[HouseDetail] ([Id], [HouseId], [StudentId], [AccademidYearID]) VALUES (16, 4, 16, 3)
GO
INSERT [dbo].[HouseDetail] ([Id], [HouseId], [StudentId], [AccademidYearID]) VALUES (17, 2, 17, 3)
GO
INSERT [dbo].[HouseDetail] ([Id], [HouseId], [StudentId], [AccademidYearID]) VALUES (18, 2, 18, 3)
GO
INSERT [dbo].[HouseDetail] ([Id], [HouseId], [StudentId], [AccademidYearID]) VALUES (19, 3, 19, 3)
GO
INSERT [dbo].[HouseDetail] ([Id], [HouseId], [StudentId], [AccademidYearID]) VALUES (20, 3, 20, 3)
GO
INSERT [dbo].[HouseDetail] ([Id], [HouseId], [StudentId], [AccademidYearID]) VALUES (21, 4, 21, 3)
GO
INSERT [dbo].[HouseDetail] ([Id], [HouseId], [StudentId], [AccademidYearID]) VALUES (22, 1, 22, 3)
GO
SET IDENTITY_INSERT [dbo].[HouseDetail] OFF
GO
SET IDENTITY_INSERT [dbo].[HouseMaster] ON 

GO
INSERT [dbo].[HouseMaster] ([Id], [Name], [SchoolId], [AcademicYearId], [Note], [Image], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (1, N'Interactive', 1, 3, N'Just for dummy entry', N'1-01.png', 0, 1, N'2', CAST(N'2018-12-05T14:39:18.430' AS DateTime), N'2', CAST(N'2018-12-13T19:06:26.017' AS DateTime))
GO
INSERT [dbo].[HouseMaster] ([Id], [Name], [SchoolId], [AcademicYearId], [Note], [Image], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (2, N'Appreciative', 1, 3, N'Just for Dummy test ', N'1-04.png', 0, 1, N'2', CAST(N'2018-12-05T18:38:17.463' AS DateTime), N'2', CAST(N'2018-12-13T19:06:01.810' AS DateTime))
GO
INSERT [dbo].[HouseMaster] ([Id], [Name], [SchoolId], [AcademicYearId], [Note], [Image], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (3, N'Wondering', 1, 3, NULL, N'1-02.png', 0, 1, N'2', CAST(N'2018-12-06T11:54:05.687' AS DateTime), N'2', CAST(N'2018-12-13T19:05:46.120' AS DateTime))
GO
INSERT [dbo].[HouseMaster] ([Id], [Name], [SchoolId], [AcademicYearId], [Note], [Image], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (4, N'Initiative', 1, 3, NULL, N'1-03.png', 0, 1, N'2', CAST(N'2018-12-06T11:54:26.347' AS DateTime), N'2', CAST(N'2018-12-13T19:05:25.263' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[HouseMaster] OFF
GO
SET IDENTITY_INSERT [dbo].[Link] ON 

GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (2, 1, N'Dashboard', N'Dashboard', N'Home', N'Dashboard', N'قائمة طعام', N'AdminMenu', 1, 1, 1, 1, 1, 0, 1, 1, N'1', CAST(N'2018-09-17T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-09-17T16:03:54.313' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (3, 10, N'Houses', N'Houses', N'Master', N'HomeList', N'طعام', N'Menu', 1, 1, 1, 1, 1, 0, 1, 1, N'1', CAST(N'2018-09-17T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-09-17T17:26:15.577' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (5, 5, N'Roles', N'Roles', N'RoleManagement', N'RoleList', N'طعام', N'Menu', 1, 1, 1, 1, 1, 0, 1, 1, N'1', CAST(N'2018-09-17T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-09-17T18:44:04.863' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (6, 11, N'Classes', N'Classes', N'Master', N'ClassList', NULL, N'menu', 2, 1, 1, 1, 1, 0, 1, 1, N'1', CAST(N'2018-09-19T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-09-19T19:29:04.850' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (8, 14, N'Values', N'Values', N'Master', N'ValuePointList', NULL, N'menu', 5, 1, 1, 1, 1, 0, 1, 1, N'1', CAST(N'2018-09-20T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-09-20T13:14:01.283' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (9, 6, N'School List', N'School List', N'SchoolMaster', N'SchoolList', NULL, N'Menu', 1, 1, 0, 1, 1, 0, 1, 1, N'1', CAST(N'2018-09-17T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-09-20T13:14:43.293' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (10, 6, N'Add School', N'Add School', N'SchoolMaster', N'AddSchool', NULL, N'Menu', 2, 1, 0, 1, 1, 0, 1, 1, N'1', CAST(N'2018-09-17T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-09-20T13:33:46.790' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (11, 13, N'Subjects', N'Subjects', N'Master', N'SubjectList', NULL, N'Menu', 4, 1, 1, 1, 1, 0, 1, 1, N'1', CAST(N'2018-09-17T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-09-20T14:55:53.667' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (12, 12, N'Sections', N'Sections', N'Master', N'SectionList', NULL, N'Menu', 3, 1, 1, 1, 1, 0, 1, 1, N'1', CAST(N'2019-09-17T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-09-20T19:16:01.680' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (13, 7, N'Admin List', N'Admin List', N'SchoolAdmin', N'SchoolAdminList', NULL, N'Menu', 1, 1, 0, 1, 1, 0, 1, 1, N'1', CAST(N'2018-09-17T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-09-24T10:31:36.490' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (14, 7, N'Add Admin', N'Add Admin', N'SchoolAdmin', N'AddSchoolAdmin', NULL, N'Menu', 2, 1, 0, 1, 1, 0, 1, 1, N'1', CAST(N'2018-09-17T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-09-24T10:32:30.217' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (15, 8, N'Teacher List', N'Teacher List', N'TeacherMaster', N'TeacherList', NULL, N'Menu', 1, 1, 0, 1, 1, 0, 1, 1, N'1', CAST(N'2018-09-17T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-09-24T10:31:36.490' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (16, 8, N'Add Teacher', N'Add Teacher', N'TeacherMaster', N'AddTeacher', NULL, N'Menu', 2, 1, 0, 1, 1, 0, 1, 1, N'1', CAST(N'2018-09-17T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-09-24T10:32:30.217' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (17, 9, N'Student List', N'Student List', N'Student', N'StudentList', NULL, N'Menu', 1, 1, 0, 1, 1, 0, 1, 1, N'1', CAST(N'2018-09-25T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-09-25T15:38:12.957' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (18, 9, N'Add Student', N'Add Student', N'Student', N'AddStudent', NULL, N'Menu', 2, 1, 0, 1, 1, 0, 1, 1, N'1', CAST(N'2018-09-25T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-09-25T15:38:56.673' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (20, 8, N'Assign to Classes', N'Assign to Classes', N'TeacherMaster', N'AssignTeacherClasses', NULL, N'Menu', 3, 1, 0, 1, 1, 0, 1, 1, N'1', CAST(N'2018-09-25T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-18T12:17:46.883' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (21, 19, N'Parent List', N'Parent List', N'GuardianMaster', N'GuardianList', NULL, N'Menu', 1, 1, 0, 1, 1, 0, 1, 1, N'1', CAST(N'2018-10-23T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-23T12:40:48.557' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (23, 19, N'Add Parent', N'Add Parent', N'GuardianMaster', N'AddGuardian', NULL, N'Menu', 2, 1, 0, 1, 1, 0, 1, 1, N'1', CAST(N'2018-10-23T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-23T12:42:07.693' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (24, 10, N'Add House', N'Add House', N'Master', N'AddHome', NULL, N'Menu', 2, 1, 0, 1, 1, 0, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-09-24T10:32:30.217' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (25, 11, N'Add Class', N'Add Class', N'Master', N'AddClass', NULL, N'Menu', 2, 1, 0, 1, 1, 0, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-24T18:09:55.237' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (26, 14, N'Add Value', N'Add Value', N'Master', N'AddValuePoint', NULL, N'Menu', 2, 1, 0, 1, 1, 0, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-24T18:11:40.907' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (27, 13, N'Add Subject', N'Add Subject', N'Master', N'AddSubject', NULL, N'Menu', 2, 1, 0, 1, 1, 0, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-24T18:12:30.030' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (28, 12, N'Add Section', N'Add Section', N'Master', N'AddSection', NULL, N'Menu', 2, 1, 0, 1, 1, 0, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-24T18:13:06.823' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (30, 9, N'SaveStudentDetail', N'SaveStudentDetail', N'Student', N'SaveStudentDetail', NULL, N'Menu', 2, 1, 0, 1, 1, 0, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-24T18:14:33.200' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (33, 11, N'DeleteClass', N'DeleteClass', N'Master', N'DeleteClass', NULL, N'Action', 0, 0, 0, 0, 1, 6, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-26T14:26:13.093' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (34, 11, N'ActiveDeactiveClass', N'ActiveDeactiveClass', N'Master', N'ActiveDeactiveClass', NULL, N'Action', 0, 0, 0, 0, 1, 6, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-26T14:28:22.077' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (35, 11, N'IsClassExists', N'IsClassExists', N'Master', N'IsClassExists', NULL, N'Action', 0, 0, 0, 0, 1, 25, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-26T14:29:16.053' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (36, 14, N'DeleteValuePoint', N'DeleteValuePoint', N'Master', N'DeleteValuePoint', NULL, N'Action', 0, 0, 0, 0, 1, 8, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-26T14:26:13.093' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (37, 14, N'ActiveDeactiveValuePoint', N'ActiveDeactiveValuePoint', N'Master', N'ActiveDeactiveValuePoint', NULL, N'Action', 0, 0, 0, 0, 1, 8, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-26T14:28:22.077' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (38, 14, N'IsValuePointExists', N'IsValuePointExists', N'Master', N'IsValuePointExists', NULL, N'Action', 0, 0, 0, 0, 1, 26, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-26T14:29:16.053' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (39, 13, N'DeleteSubject', N'DeleteSubject', N'Master', N'DeleteSubject', NULL, N'Action', 0, 0, 0, 0, 1, 11, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-26T14:26:13.093' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (40, 13, N'ActiveDeactiveSubject', N'ActiveDeactiveSubject', N'Master', N'ActiveDeactiveSubject', NULL, N'Action', 0, 0, 0, 0, 1, 11, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-26T14:28:22.077' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (41, 13, N'IsSubjectExists', N'IsSubjectExists', N'Master', N'IsSubjectExists', NULL, N'Action', 0, 0, 0, 0, 1, 27, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-26T14:29:16.053' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (42, 12, N'DeleteSection', N'DeleteSection', N'Master', N'DeleteSection', NULL, N'Action', 0, 0, 0, 0, 1, 12, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-26T14:26:13.093' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (43, 12, N'ActiveDeactiveSection', N'ActiveDeactiveSection', N'Master', N'ActiveDeactiveSection', NULL, N'Action', 0, 0, 0, 0, 1, 12, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-26T14:28:22.077' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (44, 12, N'IsSectionExists', N'IsSectionExists', N'Master', N'IsSectionExists', NULL, N'Action', 0, 0, 0, 0, 1, 28, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-26T14:29:16.053' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (45, 10, N'DeleteHome', N'DeleteHome', N'Master', N'DeleteHome', NULL, N'Action', 0, 0, 0, 0, 1, 3, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-26T14:26:13.093' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (46, 10, N'ActiveDeactiveHome', N'ActiveDeactiveHome', N'Master', N'ActiveDeactiveHome', NULL, N'Action', 0, 0, 0, 0, 1, 3, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-26T14:28:22.077' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (47, 10, N'IsHomeExists', N'IsHomeExists', N'Master', N'IsHomeExists', NULL, N'Action', 0, 0, 0, 0, 1, 24, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-26T14:29:16.053' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (48, 7, N'IsLoginEmailExist', N'IsLoginEmailExist', N'SchoolAdmin', N'IsLoginEmailExist', NULL, N'Action', 0, 0, 0, 0, 1, 14, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-26T14:26:13.093' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (49, 7, N'IsLoginMobileExist', N'IsLoginMobileExist', N'SchoolAdmin', N'IsLoginMobileExist', NULL, N'Action', 0, 0, 0, 0, 1, 14, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-26T14:28:22.077' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (50, 7, N'ActiveDeactiveSchoolAdmin', N'ActiveDeactiveSchoolAdmin', N'SchoolAdmin', N'ActiveDeactiveSchoolAdmin', NULL, N'Action', 0, 0, 0, 0, 1, 13, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-26T14:29:16.053' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (51, 7, N'DeleteSchoolAdmin', N'DeleteSchoolAdmin', N'SchoolAdmin', N'DeleteSchoolAdmin', NULL, N'Action', 0, 0, 0, 0, 1, 13, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-26T14:44:44.887' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (52, 6, N'DeleteSchool', N'DeleteSchool', N'SchoolMaster', N'DeleteSchool', NULL, N'Action', 0, 0, 0, 0, 1, 9, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-26T14:26:13.093' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (53, 6, N'ActiveDeactiveSchool', N'ActiveDeactiveSchool', N'SchoolMaster', N'ActiveDeactiveSchool', NULL, N'Action', 0, 0, 0, 0, 1, 9, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-26T14:28:22.077' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (54, 6, N'IsEmailExist', N'IsEmailExist', N'SchoolMaster', N'IsEmailExist', NULL, N'Action', 0, 0, 0, 0, 1, 10, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-26T14:29:16.053' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (55, 8, N'IsTeacherEmailExist', N'IsTeacherEmailExist', N'TeacherMaster', N'IsTeacherEmailExist', NULL, N'Action', 0, 0, 0, 0, 1, 16, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-26T14:26:13.093' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (56, 8, N'IsTeacherMobileExist', N'IsTeacherMobileExist', N'TeacherMaster', N'IsTeacherMobileExist', NULL, N'Action', 0, 0, 0, 0, 1, 16, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-26T14:28:22.077' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (57, 8, N'ActiveDeactiveSchoolTeacher', N'ActiveDeactiveSchoolTeacher', N'TeacherMaster', N'ActiveDeactiveSchoolTeacher', NULL, N'Action', 0, 0, 0, 0, 1, 15, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-26T14:29:16.053' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (58, 8, N'DeleteSchoolTeacher', N'DeleteSchoolTeacher', N'TeacherMaster', N'DeleteSchoolTeacher', NULL, N'Action', 0, 0, 0, 0, 1, 15, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-26T14:55:44.030' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (60, 8, N'Assign Teacher Detail', N'Assign Teacher Detail', N'TeacherMaster', N'AssignTeacherDetail', NULL, N'Action', 0, 0, 0, 0, 1, 20, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-26T14:59:08.283' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (61, 8, N'AssignClassesList', N'AssignClassesList', N'TeacherMaster', N'AssignClassesList', NULL, N'Action', 0, 0, 0, 0, 1, 20, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-26T15:00:41.833' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (62, 8, N'GetSectionListByClass', N'GetSectionListByClass', N'TeacherMaster', N'GetSectionListByClass', NULL, N'Action', 0, 0, 0, 0, 1, 20, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-26T15:01:38.293' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (63, 8, N'ChangeHomeTeacher', N'ChangeHomeTeacher', N'TeacherMaster', N'ChangeHomeTeacher', NULL, N'Action', 0, 0, 0, 0, 1, 20, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-26T15:02:14.163' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (64, 8, N'DeleteAssignedClass', N'DeleteAssignedClass', N'TeacherMaster', N'DeleteAssignedClass', NULL, N'Action', 0, 0, 0, 0, 1, 20, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-26T15:02:44.427' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (65, 19, N'DeleteParent', N'DeleteParent', N'GuardianMaster', N'DeleteParent', NULL, N'Action', 0, 0, 0, 0, 1, 21, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-26T14:26:13.093' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (66, 19, N'ActiveDeactiveGuardian', N'ActiveDeactiveGuardian', N'GuardianMaster', N'ActiveDeactiveGuardian', NULL, N'Action', 0, 0, 0, 0, 1, 21, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-26T14:28:22.077' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (67, 19, N'IsGuardianEmailExists', N'IsGuardianEmailExists', N'GuardianMaster', N'IsGuardianEmailExists', NULL, N'Action', 0, 0, 0, 0, 1, 23, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-26T14:29:16.053' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (68, 19, N'IsParentMobileExist', N'IsParentMobileExist', N'GuardianMaster', N'IsParentMobileExist', NULL, N'Action', 0, 0, 0, 0, 1, 23, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-26T15:18:53.540' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (69, 9, N'DeleteStudent', N'DeleteStudent', N'Student', N'DeleteStudent', NULL, N'Action', 0, 0, 0, 0, 1, 17, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-26T14:26:13.093' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (70, 9, N'ActiveDeactiveStudent', N'ActiveDeactiveStudent', N'Student', N'ActiveDeactiveStudent', NULL, N'Action', 0, 0, 0, 0, 1, 17, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-26T14:28:22.077' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (71, 9, N'IsRollNoExists', N'IsRollNoExists', N'Student', N'IsRollNoExists', NULL, N'Action', 0, 0, 0, 0, 1, 18, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-26T14:29:16.053' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (72, 9, N'IsStudentCodeExists', N'IsStudentCodeExists', N'Student', N'IsStudentCodeExists', NULL, N'Action', 0, 0, 0, 0, 1, 18, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-26T15:18:53.540' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (73, 9, N'EditStudentDetails', N'EditStudentDetails', N'Student', N'EditStudentDetails', NULL, N'Action', 0, 0, 0, 0, 1, 18, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-26T14:26:13.093' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (74, 9, N'AddNewGuardian', N'AddNewGuardian', N'Student', N'AddNewGuardian', NULL, N'Action', 0, 0, 0, 0, 1, 18, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-26T14:28:22.077' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (75, 9, N'IsParentEmailExist', N'IsParentEmailExist', N'Student', N'IsParentEmailExist', NULL, N'Action', 0, 0, 0, 0, 1, 18, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-26T14:29:16.053' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (77, 9, N'GetSectionListByClassId', N'GetSectionListByClassId', N'Student', N'GetSectionListByClassId', NULL, N'Action', 0, 0, 0, 0, 1, 18, 0, 1, N'1', CAST(N'2018-10-24T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-26T16:01:13.727' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (78, 9, N'Add Value Point', N'Add Value Point', N'Student', N'AddStudentValuePoint', NULL, N'Menu', 1, 1, 0, 1, 1, 0, 1, 1, N'1', CAST(N'2018-10-30T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-30T15:12:31.930' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (79, 9, N'GetSectionListByUser', N'GetSectionListByUser', N'Student', N'GetSectionListByUser', NULL, N'Action', 0, 0, 0, 0, 1, 78, 0, 1, N'1', CAST(N'2018-10-30T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-30T16:32:38.867' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (80, 9, N'_StudentValuePointList', N'_StudentValuePointList', N'Student', N'_StudentValuePointList', NULL, N'Action', 0, 0, 0, 0, 1, 78, 0, 1, N'1', CAST(N'2018-10-30T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-10-31T12:04:40.837' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (81, 9, N'All Value Points', N'All Value Points', N'Student', N'AllValuePoints', NULL, N'Menu', 3, 1, 0, 1, 1, 0, 1, 1, N'1', CAST(N'2018-11-01T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-11-01T11:38:58.777' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (82, 20, N'All Groups', N'All Groups', N'Master', N'GroupList', NULL, N'Menu', 1, 1, 0, 1, 1, 0, 1, 1, N'1', CAST(N'2018-11-01T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-11-01T14:55:25.030' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (83, 20, N'Create Group', N'Create Group', N'Master', N'AddGroup', NULL, N'Menu', 2, 1, 0, 1, 1, 0, 1, 1, N'1', CAST(N'2018-11-01T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-11-01T15:02:00.023' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (84, 20, N'DeleteGroup', N'DeleteGroup', N'Master', N'DeleteGroup', NULL, N'Action', 0, 0, 0, 0, 1, 82, 0, 1, N'1', CAST(N'2018-11-01T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-11-01T15:08:55.893' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (85, 20, N'ActiveDeactiveGroup', N'ActiveDeactiveGroup', N'Master', N'ActiveDeactiveGroup', NULL, N'Action', 0, 0, 0, 0, 1, 82, 0, 1, N'1', CAST(N'2018-11-01T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-11-01T15:09:37.837' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (86, 20, N'IsGroupExists', N'IsGroupExists', N'Master', N'IsGroupExists', NULL, N'Action', 0, 0, 0, 0, 1, 83, 0, 1, N'1', CAST(N'2018-11-01T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-11-01T15:11:24.730' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (88, 9, N'DeleteAllValuePoint', N'DeleteAllValuePoint', N'Student', N'DeleteAllValuePoint', NULL, N'Action', 0, 0, 0, 0, 1, 81, 0, 1, N'1', CAST(N'2018-11-01T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-11-01T15:22:13.180' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (89, 5, N'Assign Rights', N'Assign Rights', N'Rolemanagement', N'RoleRights', NULL, N'Menu', 1, 1, 1, 1, 1, 0, 1, 1, N'1', CAST(N'2018-11-01T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-11-01T17:27:31.250' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (90, 9, N'GetHomeListByYearId', N'GetHomeListByYearId', N'Student', N'GetHomeListByYearId', NULL, N'Action', 0, 0, 0, 0, 1, 18, 0, 1, N'1', CAST(N'2018-11-06T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-11-06T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (91, 21, N'Send Message', N'Send Message', N'Notification', N'SendMessage', NULL, N'Menu', 3, 1, 0, 1, 1, 0, 1, 1, N'1', CAST(N'2018-11-16T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-11-16T18:23:59.250' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (10091, 21, N'All Notifications', N'All Notifications', N'Notification', N'AllNotification', NULL, N'Menu', 1, 1, 0, 1, 1, 0, 1, 1, N'1', CAST(N'2018-11-22T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-11-22T15:26:37.800' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (10093, 21, N'All Message', N'All Message', N'Notification', N'AllMessage', NULL, N'Menu', 2, 1, 0, 1, 1, 0, 1, 1, N'1', CAST(N'2018-11-22T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-11-22T15:28:05.150' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (10094, 21, N'ViewMessage', N'ViewMessage', N'Notification', N'ViewMessage', NULL, N'Action', 0, 0, 0, 0, 1, 10093, 0, 1, N'1', CAST(N'2018-11-22T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-11-22T17:43:57.227' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (10095, 1, N'MaxValuePointGraph', N'MaxValuePointGraph', N'Home', N'_ValuePointGraph', NULL, N'Action', 0, 0, 0, 0, 1, 2, 0, 1, N'1', CAST(N'2018-11-29T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-11-29T17:05:53.387' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (10096, 1, N'ValuePointMinGraph', N'ValuePointMinGraph', N'Home', N'_ValuePointMinGraph', NULL, N'Action', 0, 0, 0, 0, 1, 2, 0, 1, N'1', CAST(N'2018-11-29T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-11-29T17:08:47.453' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (10097, 1, N'HouseGraph', N'HouseGraph', N'Home', N'_HouseGraph', NULL, N'Action', 0, 0, 0, 0, 1, 2, 0, 1, N'1', CAST(N'2018-11-29T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-11-29T17:08:47.453' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (10098, 1, N'TopTeacherList', N'TopTeacherList', N'Home', N'_TopTeacherList', NULL, N'Action', 0, 0, 0, 0, 1, 2, 0, 1, N'1', CAST(N'2018-11-29T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-11-29T17:08:47.453' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (10099, 1, N'LeastTeacherList', N'LeastTeacherList', N'Home', N'_LeastTeacherList', NULL, N'Action', 0, 0, 0, 0, 1, 2, 0, 1, N'1', CAST(N'2018-11-29T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-11-29T17:08:47.453' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (10100, 1, N'TopStudentList', N'TopStudentList', N'Home', N'_TopStudentList', NULL, N'Action', 0, 0, 0, 0, 1, 2, 0, 1, N'1', CAST(N'2018-11-29T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-11-29T17:08:47.453' AS DateTime))
GO
INSERT [dbo].[Link] ([LinkId], [ModuleId], [LinkNameArabic], [LinkName], [Controller], [Action], [TypeArabic], [Type], [ViewIndex], [IsDefault], [IsSingle], [IsPage], [LinkGroupId], [ParentId], [IsVisible], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (10101, 1, N'LeastStudentList', N'LeastStudentList', N'Home', N'_LeastStudentList', NULL, N'Action', 0, 0, 0, 0, 1, 2, 0, 1, N'1', CAST(N'2018-11-29T00:00:00.000' AS DateTime), N'sa', CAST(N'2018-11-29T17:08:47.453' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Link] OFF
GO
SET IDENTITY_INSERT [dbo].[LoginCredentials] ON 

GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (1, 1, 0, N'webmyne.isc@gmail.com', N'isc12345', N'Super', N'Admin', N'+962_8160699043', N'Webmyne', NULL, 1, 0, CAST(N'2018-12-05T11:51:20.000' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (2, 2, 1, N'isc.web.18@gmail.com', N'User@123', N'School', N'Admin', N'+962_8980399337', N'702, Ivory Terrace Opp. Circuit House, RC Dutt Road', N'schooladmin.jpg', 1, 0, CAST(N'2018-12-05T12:09:47.207' AS DateTime), CAST(N'2018-12-05T21:52:10.217' AS DateTime))
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (3, 4, 1, N'hasan.nimr@aura-techs.com', N'12345678', N'Hassan', N'Nimr', N'+962_791482205', N'6101 Wilson Lane,Bethesda, Maryland', N'a0683d5c-9e3d-4fa8-afa3-9542cd2e2ae4.jpg', 1, 0, CAST(N'2018-12-05T13:04:09.447' AS DateTime), CAST(N'2018-12-13T18:57:42.650' AS DateTime))
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (4, 3, 1, N't.mustafa@ivsjo.com', N'12345678', N'Tamara', N'Mustafa', N'+962_780622500', NULL, NULL, 1, 0, CAST(N'2018-12-05T14:49:28.857' AS DateTime), CAST(N'2018-12-10T15:20:29.663' AS DateTime))
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (5, 4, 1, N'lalu@webmyne.com', N'12345', N'Ram', N'Lalu', N'+962_7600028733', NULL, NULL, 0, 1, CAST(N'2018-12-05T18:42:26.913' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (6, 4, 1, N'yazana@sharekacademy.com', N'12345', N'Yazan', N'Abdo', N'+962_796676227', NULL, NULL, 1, 0, CAST(N'2018-12-06T11:57:01.833' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (7, 4, 1, N'dinae@sharekacademy.com', N'12345', N'Dina', N'Ishqaidef', N'+962_799955119', NULL, N'0438ece0-eb9b-4a9b-87e8-c4d98c47f03f.jpg', 1, 0, CAST(N'2018-12-06T11:57:52.943' AS DateTime), CAST(N'2018-12-09T20:10:38.933' AS DateTime))
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (8, 4, 1, N'yalazzam12@gmail.com', N'12345', N'Yazan', N'Alazzam', N'+962_777247635', NULL, NULL, 1, 0, CAST(N'2018-12-06T12:00:10.960' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (9, 4, 1, N'sshalabieh@hotmail.com', N'12345', N'Suha', N'Shalabiah', N'+962_779999188', NULL, NULL, 1, 0, CAST(N'2018-12-06T12:01:02.967' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (10, 4, 1, N'email@email.com', N'12345', N'Faisal', N'Alazzam', N'+962_775226467', NULL, NULL, 1, 0, CAST(N'2018-12-06T12:04:03.683' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (11, 3, 1, N'n.abusaif@ivsjo.com', N'nadia@1245', N'Nadia', N'Abuseif', N'+962_786912622', NULL, NULL, 1, 0, CAST(N'2018-12-06T12:13:31.893' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (12, 3, 1, N'w.tameme@ivsjo.com', N'wafa@1245', N'Wafa', N'Shabaneh', N'+962_785557765', NULL, NULL, 1, 0, CAST(N'2018-12-06T12:22:29.157' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (13, 3, 1, N'o.daraghmeh@ivsjo.com', N'osama@1245', N'Osama', N'Daraghmeh', N'+962_796664514', NULL, NULL, 1, 0, CAST(N'2018-12-06T12:26:47.387' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (14, 3, 1, N's.hijjawi@ivsjo.com', N'sally@1245', N'Sally', N'AlSabbah', N'+962_798341880', NULL, NULL, 1, 0, CAST(N'2018-12-06T12:29:08.593' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (15, 3, 1, N'o.almasri@ivsjo.com', N'ola@1245', N'Ola', N'Almasri', N'+962_797400335', NULL, NULL, 1, 0, CAST(N'2018-12-06T12:30:49.030' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (16, 3, 1, N'n.mohammad@ivsjo.com', N'noura@1245', N'Noura', N'Mohammad', N'+962_799519816', NULL, NULL, 1, 0, CAST(N'2018-12-06T12:33:31.930' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (17, 4, 1, N'Muh1982@gmail.com', N'12345', N'Mohammad', N'Sharabati', N'+962_799900090', NULL, NULL, 1, 0, CAST(N'2018-12-06T12:46:01.937' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (18, 4, 1, N'samar.dwaik@me.com', N'12345', N'Samar', N'Dwaik', N'+962_796467700', NULL, NULL, 1, 0, CAST(N'2018-12-06T12:51:57.270' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (19, 4, 1, N'thameen2010@gmail.com', N'12345', N'Thamin', N'Alqasqas', N'+962_79999639', NULL, NULL, 1, 0, CAST(N'2018-12-06T12:56:19.090' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (20, 4, 1, N'email1@email1.com', N'12345', N'Rana', N'Aabdeen', N'+962_796950051', NULL, NULL, 1, 0, CAST(N'2018-12-06T12:58:09.367' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (21, 4, 1, N'Etomamal@gmail.com', N'12345', N'Amal', N'Etom', N'+962_798701060', NULL, NULL, 1, 0, CAST(N'2018-12-06T13:01:12.413' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (22, 4, 1, N'hussien_73@hotmail.com', N'12345', N'Husien', N'Aljedy', N'+962_776314331', NULL, NULL, 1, 0, CAST(N'2018-12-06T13:02:30.903' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (23, 4, 1, N'Dina.teen@gmail.com', N'12345', N'Dina', N'Abu teen', N'+962_799809090', NULL, NULL, 1, 0, CAST(N'2018-12-06T13:06:44.280' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (24, 4, 1, N'awas@aura-techs.com', N'12345', N'Awas', N'Hatab', N'+962_786288864', NULL, NULL, 1, 0, CAST(N'2018-12-06T13:08:13.490' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (25, 4, 1, N'aishaalsabri2@gmail.com', N'12345', N'Aisha', N'Alsabri', N'+962_7824948447', NULL, NULL, 1, 0, CAST(N'2018-12-06T13:12:07.187' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (26, 4, 1, N'Abdulaha@aura-techs.com', N'12345', N'Abdulbasit', N'Albakri', N'+962_798280008', NULL, NULL, 1, 0, CAST(N'2018-12-06T13:14:19.193' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (27, 4, 1, N'Lamq29@icloud.com', N'12345', N'Lama', N'Haqi', N'+962_796076296', NULL, NULL, 1, 0, CAST(N'2018-12-06T13:22:53.143' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (28, 4, 1, N'fares@aura-techs.com', N'12345', N'Fares', N'Albokari', N'+962_777447676', NULL, NULL, 1, 0, CAST(N'2018-12-06T13:24:14.610' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (29, 4, 1, N'sherin@aura-techs.com', N'12345', N'Sherin', N'Abu halawah', N'+962_796958258', NULL, NULL, 1, 0, CAST(N'2018-12-06T13:28:05.473' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (30, 4, 1, N'yasin_tax@yahoo.com', N'12345', N'Yasin', N'Abu odeh', N'+962_799047595', NULL, NULL, 1, 0, CAST(N'2018-12-06T13:29:04.600' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (31, 4, 1, N'amaniahmad982@yahoo.com', N'12345', N'Amani', N'Hazaemih', N'+962_779173791', NULL, NULL, 1, 0, CAST(N'2018-12-06T13:31:48.257' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (32, 4, 1, N'jalal@aura-techs.com', N'12345', N'Jalal', N'Alkateeb', N'+962_770403100', NULL, NULL, 1, 0, CAST(N'2018-12-06T13:32:48.247' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (33, 4, 1, N'weld.ali_salma@hotmail.com', N'12345', N'Salma', N'Weld ali', N'+962_797334847', NULL, NULL, 1, 0, CAST(N'2018-12-06T13:36:53.480' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (34, 4, 1, N'mohammad@aura-techs.com', N'12345', N'Mohammad ', N'Alshari', N'+962_795681186', NULL, NULL, 1, 0, CAST(N'2018-12-06T13:39:03.250' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (35, 4, 1, N'mansour@aura-techs.com', N'12345', N'Mansour', N'Taqem', N'+962_798951622', NULL, NULL, 1, 0, CAST(N'2018-12-06T13:41:50.370' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (36, 4, 1, N'Daisybloom500@gmail.com', N'12345', N'Layla   ', N'Samha', N'+962_798951567', NULL, NULL, 1, 0, CAST(N'2018-12-06T13:43:21.093' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (37, 4, 1, N'tareqfawzi70@hotmail.com', N'12345', N'Tareq', N'Aateh', N'+962_799996352', NULL, NULL, 1, 0, CAST(N'2018-12-06T13:45:29.717' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (38, 4, 1, N'sherein@aura-techs.com', N'12345', N'Sherin', N'Aaboshi', N'+962_798807447', NULL, NULL, 1, 0, CAST(N'2018-12-06T13:47:47.660' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (39, 4, 1, N'mohammad.m@aura-techs.com', N'12345', N'Mohammad', N'Alhuneidi', N'+962_795715030', NULL, NULL, 1, 0, CAST(N'2018-12-06T13:51:09.707' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (40, 4, 1, N'ibrahim@aura-techs.com', N'12345', N'Ibrahim', N'Alhuneidi', N'+962_777420179', NULL, NULL, 1, 0, CAST(N'2018-12-06T13:52:43.387' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (41, 4, 1, N'noor.mellahuneidi@yahoo.com', N'12345', N'Noor', N'Almalla', N'+962_790224942', NULL, NULL, 1, 0, CAST(N'2018-12-06T13:54:09.920' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (42, 4, 1, N'naljaabari77@gmail.com', N'12345', N'Narmeen', N'Aljaabri', N'+962_777755422', NULL, NULL, 1, 0, CAST(N'2018-12-06T13:57:44.210' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (43, 4, 1, N'mohammad.z@aura-techs.com', N'12345', N'Mohammad', N'Altarawneh', N'+962_797282499', NULL, NULL, 1, 0, CAST(N'2018-12-06T13:59:09.737' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (44, 3, 1, N'hswe@ajkb.dk', N'A123453456', N'ahmed', N'ghareeb', N'+962_0795387536', NULL, NULL, 0, 1, CAST(N'2018-12-08T15:36:12.777' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (45, 3, 1, N'kjg@kjgh.hg', N'12345345', N'Ahmed', N'Ghareeb', N'+962_795387536', NULL, NULL, 1, 0, CAST(N'2018-12-08T15:39:06.413' AS DateTime), CAST(N'2018-12-09T14:14:21.007' AS DateTime))
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (46, 3, 1, N'aaa@aa.a', N'12345345', N'Hanaa', N'alzoubi', N'+962_786947590', NULL, NULL, 1, 0, CAST(N'2018-12-11T12:49:09.920' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (47, 4, 1, N'motasem@aura-techs.com', N'12345', N'Motasem', N'Aldiab', N'+962_799333039', NULL, NULL, 1, 0, CAST(N'2018-12-13T19:48:51.833' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (48, 4, 1, N'suad@aura-techs.com', N'12345', N'Suad', N'Kabbaha', N'+962_799206148', NULL, NULL, 1, 0, CAST(N'2018-12-13T19:50:31.387' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (49, 4, 1, N'jamal.albitar@aylalogistics.com', N'12345', N'Jamaleddin', N'Albitar', N'+962_777777876', NULL, NULL, 1, 0, CAST(N'2018-12-13T19:55:35.570' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (50, 4, 1, N'razankousayeh12@gmail.com', N'12345', N'Razan', N'Kousayeh', N'+962_797960607', NULL, NULL, 1, 0, CAST(N'2018-12-13T19:56:50.457' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (51, 4, 1, N'Marwan@aura-techs.com', N'12345', N'Marwan', N'Alhusban', N'+962_795580580', NULL, NULL, 1, 0, CAST(N'2018-12-13T20:00:13.463' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (52, 4, 1, N'husban_abeer@yahoo.com', N'12345', N'Abeer', N'Alhusban', N'+962_796084208', NULL, NULL, 1, 0, CAST(N'2018-12-13T20:01:19.270' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (53, 4, 1, N'n.zater@veilkini.com', N'12345', N'Mamoun', N'Zater', N'+962_795663111', NULL, NULL, 1, 0, CAST(N'2018-12-13T20:09:12.110' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (54, 4, 1, N'rania@aura-techs.com', N'12345', N'Rania', N'Darwazeh', N'+962_796371117', NULL, NULL, 1, 0, CAST(N'2018-12-13T20:10:30.860' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (55, 4, 1, N'abselrahem1@yahoo.com', N'12345', N'Abdelraheem', N'Hasan', N'+962_795027178', NULL, NULL, 1, 0, CAST(N'2018-12-13T20:18:53.547' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (56, 4, 1, N'enaszuaiter@gmail.com', N'12345', N'Enas', N'Zueter', N'+962_7909752922', NULL, NULL, 1, 0, CAST(N'2018-12-13T20:21:04.807' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (57, 4, 1, N'othman_ahmad1@yahoo.com', N'12345', N'Ahmad', N'Othman ', N'+962_799772546', NULL, NULL, 1, 0, CAST(N'2018-12-13T20:25:02.447' AS DateTime), NULL)
GO
INSERT [dbo].[LoginCredentials] ([UserId], [RoleId], [SchoolId], [EmailId], [Password], [FirstName], [LastName], [Mobile], [Address], [ProfilePic], [IsActive], [IsDelete], [CreatedDate], [UpdatedDate]) VALUES (58, 4, 1, N'alaa@aura-techs.com', N'12345', N'Alaa', N'Badawi', N'+962_796620084', NULL, NULL, 1, 0, CAST(N'2018-12-13T20:26:07.127' AS DateTime), NULL)
GO
SET IDENTITY_INSERT [dbo].[LoginCredentials] OFF
GO
SET IDENTITY_INSERT [dbo].[Module] ON 

GO
INSERT [dbo].[Module] ([ModuleId], [ModuleNameArabic], [ModuleName], [CssClass], [ViewIndex], [CreatedBy], [CreatedDate]) VALUES (1, N'Dashboard', N'Dashboard', N'd1.png', 1, N'1', CAST(N'2018-09-17T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Module] ([ModuleId], [ModuleNameArabic], [ModuleName], [CssClass], [ViewIndex], [CreatedBy], [CreatedDate]) VALUES (5, N'Roles', N'Roles', N'd13.png', 11, N'1', CAST(N'2018-09-17T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Module] ([ModuleId], [ModuleNameArabic], [ModuleName], [CssClass], [ViewIndex], [CreatedBy], [CreatedDate]) VALUES (6, N'Schools', N'Schools', N'add_school_icon_white.png', 2, N'1', CAST(N'2018-09-17T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Module] ([ModuleId], [ModuleNameArabic], [ModuleName], [CssClass], [ViewIndex], [CreatedBy], [CreatedDate]) VALUES (7, N'School Admin', N'School Admin', N'd12.png', 3, N'1', CAST(N'2018-09-17T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Module] ([ModuleId], [ModuleNameArabic], [ModuleName], [CssClass], [ViewIndex], [CreatedBy], [CreatedDate]) VALUES (8, N'Teachers', N'Teachers', N'd3.png', 4, N'1', CAST(N'2018-09-17T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Module] ([ModuleId], [ModuleNameArabic], [ModuleName], [CssClass], [ViewIndex], [CreatedBy], [CreatedDate]) VALUES (9, N'Students', N'Students', N'd2.png', 7, N'1', CAST(N'2018-09-25T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Module] ([ModuleId], [ModuleNameArabic], [ModuleName], [CssClass], [ViewIndex], [CreatedBy], [CreatedDate]) VALUES (10, N'Houses', N'Houses', N'd14.png', 6, N'1', CAST(N'2018-10-05T15:43:19.827' AS DateTime))
GO
INSERT [dbo].[Module] ([ModuleId], [ModuleNameArabic], [ModuleName], [CssClass], [ViewIndex], [CreatedBy], [CreatedDate]) VALUES (11, N'Classes', N'Classes', N'd5.png', 2, N'1', CAST(N'2018-10-05T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Module] ([ModuleId], [ModuleNameArabic], [ModuleName], [CssClass], [ViewIndex], [CreatedBy], [CreatedDate]) VALUES (12, N'Sections', N'Sections', N'd15.png', 3, N'1', CAST(N'2018-10-05T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Module] ([ModuleId], [ModuleNameArabic], [ModuleName], [CssClass], [ViewIndex], [CreatedBy], [CreatedDate]) VALUES (13, N'Subjects', N'Subjects', N'd16.png', 5, N'1', CAST(N'2018-10-05T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Module] ([ModuleId], [ModuleNameArabic], [ModuleName], [CssClass], [ViewIndex], [CreatedBy], [CreatedDate]) VALUES (14, N'Value Point', N'Value Point', N'd17.png', 9, N'1', CAST(N'2018-10-05T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Module] ([ModuleId], [ModuleNameArabic], [ModuleName], [CssClass], [ViewIndex], [CreatedBy], [CreatedDate]) VALUES (19, N'Parents', N'Parents', N'd4.png', 8, N'1', CAST(N'2018-10-23T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Module] ([ModuleId], [ModuleNameArabic], [ModuleName], [CssClass], [ViewIndex], [CreatedBy], [CreatedDate]) VALUES (20, N'Groups', N'Groups', N'd13.png', 10, N'1', CAST(N'2018-11-01T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Module] ([ModuleId], [ModuleNameArabic], [ModuleName], [CssClass], [ViewIndex], [CreatedBy], [CreatedDate]) VALUES (21, N'Notif. & Msgs', N'Notif. & Msgs', N'd13.png', 12, N'1', CAST(N'2018-11-16T00:00:00.000' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Module] OFF
GO
SET IDENTITY_INSERT [dbo].[Notification] ON 

GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (1, 1, 3, 2, N'Jeeya Gupta Got New Point for Accuracy By sweta Patel.', N'Jeeya Gupta Got New Point for Accuracy By sweta Patel.', 1, 0, 28, N'4', CAST(N'2018-12-05T15:01:54.580' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (2, 1, 3, 1, N'Amman Khan Got New Point for Good Answer By sweta Patel.', N'Amman Khan Got New Point for Good Answer By sweta Patel.', 1, 0, 28, N'4', CAST(N'2018-12-05T15:02:10.783' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (3, 1, 4, 1, N'Amman Khan Got New Point for Attendance By Hassan Nimr.', N'Amman Khan Got New Point for Attendance By Hassan Nimr.', 0, 0, 28, N'3', CAST(N'2018-12-05T15:34:22.807' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (4, 1, 3, 1, N'Amman Khan Got New Point for Accuracy By sweta Patel.', N'Amman Khan Got New Point for Accuracy By sweta Patel.', 1, 0, 28, N'4', CAST(N'2018-12-05T15:49:14.717' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (5, 1, 3, 2, N'Jeeya Gupta Got New Point for Dance By sweta Patel.', N'Jeeya Gupta Got New Point for Dance By sweta Patel.', 1, 0, 28, N'4', CAST(N'2018-12-05T15:55:56.733' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (6, 1, 5, 3, N'Taimur Khan Got New Point for Accuracy By sweta Patel.', N'Taimur Khan Got New Point for Accuracy By sweta Patel.', 0, 0, 28, N'4', CAST(N'2018-12-05T18:46:13.713' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (7, 1, 4, 1, N'Osama Abdo Got New Point for Love By Dina Ishqaidef.', N'Osama Abdo Got New Point for Love By Dina Ishqaidef.', 0, 0, 29, N'7', CAST(N'2018-12-09T14:57:41.923' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (8, 1, 6, 1, N'Osama Abdo Got New Point for Love By Dina Ishqaidef.', N'Osama Abdo Got New Point for Love By Dina Ishqaidef.', 0, 0, 29, N'7', CAST(N'2018-12-09T14:57:41.923' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (9, 1, 11, 1, N'Osama Abdo Got New Point for Love By Dina Ishqaidef.', N'Osama Abdo Got New Point for Love By Dina Ishqaidef.', 0, 0, 29, N'7', CAST(N'2018-12-09T14:57:41.923' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (10, 1, 12, 1, N'Osama Abdo Got New Point for Love By Dina Ishqaidef.', N'Osama Abdo Got New Point for Love By Dina Ishqaidef.', 0, 0, 29, N'7', CAST(N'2018-12-09T14:57:41.923' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (11, 1, 13, 1, N'Osama Abdo Got New Point for Love By Dina Ishqaidef.', N'Osama Abdo Got New Point for Love By Dina Ishqaidef.', 0, 0, 29, N'7', CAST(N'2018-12-09T14:57:41.923' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (12, 1, 14, 1, N'Osama Abdo Got New Point for Love By Dina Ishqaidef.', N'Osama Abdo Got New Point for Love By Dina Ishqaidef.', 0, 0, 29, N'7', CAST(N'2018-12-09T14:57:41.923' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (13, 1, 15, 1, N'Osama Abdo Got New Point for Love By Dina Ishqaidef.', N'Osama Abdo Got New Point for Love By Dina Ishqaidef.', 0, 0, 29, N'7', CAST(N'2018-12-09T14:57:41.923' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (14, 1, 16, 1, N'Osama Abdo Got New Point for Love By Dina Ishqaidef.', N'Osama Abdo Got New Point for Love By Dina Ishqaidef.', 0, 0, 29, N'7', CAST(N'2018-12-09T14:57:41.923' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (15, 1, 45, 1, N'Osama Abdo Got New Point for Love By Dina Ishqaidef.', N'Osama Abdo Got New Point for Love By Dina Ishqaidef.', 1, 0, 29, N'7', CAST(N'2018-12-09T14:57:41.923' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (16, 1, 6, 1, N'Osama Abdo Got New Point for Freedom By Ahmed Ghareeb.', N'Osama Abdo Got New Point for Freedom By Ahmed Ghareeb.', 0, 0, 29, N'45', CAST(N'2018-12-09T15:08:03.783' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (17, 1, 7, 1, N'Osama Abdo Got New Point for Freedom By Ahmed Ghareeb.', N'Osama Abdo Got New Point for Freedom By Ahmed Ghareeb.', 1, 0, 29, N'45', CAST(N'2018-12-09T15:08:03.783' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (18, 1, 8, 2, N'Sarah Alazzam Got New Point for Love By Ahmed Ghareeb.', N'Sarah Alazzam Got New Point for Love By Ahmed Ghareeb.', 0, 0, 29, N'45', CAST(N'2018-12-09T18:13:20.027' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (19, 1, 9, 2, N'Sarah Alazzam Got New Point for Love By Ahmed Ghareeb.', N'Sarah Alazzam Got New Point for Love By Ahmed Ghareeb.', 0, 0, 29, N'45', CAST(N'2018-12-09T18:13:20.027' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (20, 1, 10, 2, N'Sarah Alazzam Got New Point for Love By Ahmed Ghareeb.', N'Sarah Alazzam Got New Point for Love By Ahmed Ghareeb.', 0, 0, 29, N'45', CAST(N'2018-12-09T18:13:20.027' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (21, 1, 4, 1, N'Osama Abdo Got New Point for Happiness By Hassan Nimr.', N'Osama Abdo Got New Point for Happiness By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-09T20:35:23.563' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (22, 1, 6, 1, N'Osama Abdo Got New Point for Happiness By Hassan Nimr.', N'Osama Abdo Got New Point for Happiness By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-09T20:35:23.563' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (23, 1, 7, 1, N'Osama Abdo Got New Point for Happiness By Hassan Nimr.', N'Osama Abdo Got New Point for Happiness By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-09T20:35:23.563' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (24, 1, 11, 1, N'Osama Abdo Got New Point for Happiness By Hassan Nimr.', N'Osama Abdo Got New Point for Happiness By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-09T20:35:23.563' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (25, 1, 12, 1, N'Osama Abdo Got New Point for Happiness By Hassan Nimr.', N'Osama Abdo Got New Point for Happiness By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-09T20:35:23.563' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (26, 1, 13, 1, N'Osama Abdo Got New Point for Happiness By Hassan Nimr.', N'Osama Abdo Got New Point for Happiness By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-09T20:35:23.563' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (27, 1, 14, 1, N'Osama Abdo Got New Point for Happiness By Hassan Nimr.', N'Osama Abdo Got New Point for Happiness By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-09T20:35:23.563' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (28, 1, 15, 1, N'Osama Abdo Got New Point for Happiness By Hassan Nimr.', N'Osama Abdo Got New Point for Happiness By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-09T20:35:23.563' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (29, 1, 16, 1, N'Osama Abdo Got New Point for Happiness By Hassan Nimr.', N'Osama Abdo Got New Point for Happiness By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-09T20:35:23.563' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (30, 1, 45, 1, N'Osama Abdo Got New Point for Happiness By Hassan Nimr.', N'Osama Abdo Got New Point for Happiness By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-09T20:35:23.563' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (31, 1, 4, 1, N'Osama Abdo Got New Point for Union By Hassan Nimr.', N'Osama Abdo Got New Point for Union By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-10T11:54:26.270' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (32, 1, 6, 1, N'Osama Abdo Got New Point for Union By Hassan Nimr.', N'Osama Abdo Got New Point for Union By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-10T11:54:26.270' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (33, 1, 7, 1, N'Osama Abdo Got New Point for Union By Hassan Nimr.', N'Osama Abdo Got New Point for Union By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-10T11:54:26.270' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (34, 1, 11, 1, N'Osama Abdo Got New Point for Union By Hassan Nimr.', N'Osama Abdo Got New Point for Union By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-10T11:54:26.270' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (35, 1, 12, 1, N'Osama Abdo Got New Point for Union By Hassan Nimr.', N'Osama Abdo Got New Point for Union By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-10T11:54:26.270' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (36, 1, 13, 1, N'Osama Abdo Got New Point for Union By Hassan Nimr.', N'Osama Abdo Got New Point for Union By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-10T11:54:26.270' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (37, 1, 14, 1, N'Osama Abdo Got New Point for Union By Hassan Nimr.', N'Osama Abdo Got New Point for Union By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-10T11:54:26.270' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (38, 1, 15, 1, N'Osama Abdo Got New Point for Union By Hassan Nimr.', N'Osama Abdo Got New Point for Union By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-10T11:54:26.270' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (39, 1, 16, 1, N'Osama Abdo Got New Point for Union By Hassan Nimr.', N'Osama Abdo Got New Point for Union By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-10T11:54:26.270' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (40, 1, 45, 1, N'Osama Abdo Got New Point for Union By Hassan Nimr.', N'Osama Abdo Got New Point for Union By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-10T11:54:26.270' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (41, 1, 4, 1, N'Osama Abdo Got New Point for Freedom By Hassan Nimr.', N'Osama Abdo Got New Point for Freedom By Hassan Nimr.', 1, 0, 29, N'3', CAST(N'2018-12-10T12:48:56.907' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (42, 1, 6, 1, N'Osama Abdo Got New Point for Freedom By Hassan Nimr.', N'Osama Abdo Got New Point for Freedom By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-10T12:48:56.907' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (43, 1, 7, 1, N'Osama Abdo Got New Point for Freedom By Hassan Nimr.', N'Osama Abdo Got New Point for Freedom By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-10T12:48:56.907' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (44, 1, 11, 1, N'Osama Abdo Got New Point for Freedom By Hassan Nimr.', N'Osama Abdo Got New Point for Freedom By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-10T12:48:56.907' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (45, 1, 12, 1, N'Osama Abdo Got New Point for Freedom By Hassan Nimr.', N'Osama Abdo Got New Point for Freedom By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-10T12:48:56.907' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (46, 1, 13, 1, N'Osama Abdo Got New Point for Freedom By Hassan Nimr.', N'Osama Abdo Got New Point for Freedom By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-10T12:48:56.907' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (47, 1, 14, 1, N'Osama Abdo Got New Point for Freedom By Hassan Nimr.', N'Osama Abdo Got New Point for Freedom By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-10T12:48:56.907' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (48, 1, 15, 1, N'Osama Abdo Got New Point for Freedom By Hassan Nimr.', N'Osama Abdo Got New Point for Freedom By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-10T12:48:56.907' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (49, 1, 16, 1, N'Osama Abdo Got New Point for Freedom By Hassan Nimr.', N'Osama Abdo Got New Point for Freedom By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-10T12:48:56.907' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (50, 1, 45, 1, N'Osama Abdo Got New Point for Freedom By Hassan Nimr.', N'Osama Abdo Got New Point for Freedom By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-10T12:48:56.907' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (51, 1, 3, 1, N'Osama Abdo Got New Point for Testing By Tamara Mustafa.', N'Osama Abdo Got New Point for Testing By Tamara Mustafa.', 1, 0, 29, N'4', CAST(N'2018-12-10T15:05:30.997' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (52, 1, 6, 1, N'Osama Abdo Got New Point for Testing By Tamara Mustafa.', N'Osama Abdo Got New Point for Testing By Tamara Mustafa.', 0, 0, 29, N'4', CAST(N'2018-12-10T15:05:30.997' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (53, 1, 7, 1, N'Osama Abdo Got New Point for Testing By Tamara Mustafa.', N'Osama Abdo Got New Point for Testing By Tamara Mustafa.', 0, 0, 29, N'4', CAST(N'2018-12-10T15:05:30.997' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (54, 1, 17, 3, N'Zaid Sharabati Got New Point for Freedom By Ahmed Ghareeb.', N'Zaid Sharabati Got New Point for Freedom By Ahmed Ghareeb.', 0, 0, 29, N'45', CAST(N'2018-12-10T15:07:09.917' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (55, 1, 18, 3, N'Zaid Sharabati Got New Point for Freedom By Ahmed Ghareeb.', N'Zaid Sharabati Got New Point for Freedom By Ahmed Ghareeb.', 0, 0, 29, N'45', CAST(N'2018-12-10T15:07:09.917' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (56, 1, 3, 1, N'Osama Abdo Got New Point for Freedom By Dina Ishqaidef.', N'Osama Abdo Got New Point for Freedom By Dina Ishqaidef.', 1, 0, 29, N'7', CAST(N'2018-12-10T17:05:20.757' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (57, 1, 4, 1, N'Osama Abdo Got New Point for Freedom By Dina Ishqaidef.', N'Osama Abdo Got New Point for Freedom By Dina Ishqaidef.', 0, 0, 29, N'7', CAST(N'2018-12-10T17:05:20.757' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (58, 1, 6, 1, N'Osama Abdo Got New Point for Freedom By Dina Ishqaidef.', N'Osama Abdo Got New Point for Freedom By Dina Ishqaidef.', 0, 0, 29, N'7', CAST(N'2018-12-10T17:05:20.757' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (59, 1, 11, 1, N'Osama Abdo Got New Point for Freedom By Dina Ishqaidef.', N'Osama Abdo Got New Point for Freedom By Dina Ishqaidef.', 0, 0, 29, N'7', CAST(N'2018-12-10T17:05:20.757' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (60, 1, 12, 1, N'Osama Abdo Got New Point for Freedom By Dina Ishqaidef.', N'Osama Abdo Got New Point for Freedom By Dina Ishqaidef.', 0, 0, 29, N'7', CAST(N'2018-12-10T17:05:20.757' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (61, 1, 13, 1, N'Osama Abdo Got New Point for Freedom By Dina Ishqaidef.', N'Osama Abdo Got New Point for Freedom By Dina Ishqaidef.', 0, 0, 29, N'7', CAST(N'2018-12-10T17:05:20.757' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (62, 1, 14, 1, N'Osama Abdo Got New Point for Freedom By Dina Ishqaidef.', N'Osama Abdo Got New Point for Freedom By Dina Ishqaidef.', 0, 0, 29, N'7', CAST(N'2018-12-10T17:05:20.757' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (63, 1, 15, 1, N'Osama Abdo Got New Point for Freedom By Dina Ishqaidef.', N'Osama Abdo Got New Point for Freedom By Dina Ishqaidef.', 0, 0, 29, N'7', CAST(N'2018-12-10T17:05:20.757' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (64, 1, 16, 1, N'Osama Abdo Got New Point for Freedom By Dina Ishqaidef.', N'Osama Abdo Got New Point for Freedom By Dina Ishqaidef.', 0, 0, 29, N'7', CAST(N'2018-12-10T17:05:20.757' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (65, 1, 45, 1, N'Osama Abdo Got New Point for Freedom By Dina Ishqaidef.', N'Osama Abdo Got New Point for Freedom By Dina Ishqaidef.', 0, 0, 29, N'7', CAST(N'2018-12-10T17:05:20.757' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (66, 1, 3, 1, N'Osama Abdo Got New Point for Peace By Dina Ishqaidef.', N'Osama Abdo Got New Point for Peace By Dina Ishqaidef.', 1, 0, 29, N'7', CAST(N'2018-12-11T13:28:54.357' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (67, 1, 4, 1, N'Osama Abdo Got New Point for Peace By Dina Ishqaidef.', N'Osama Abdo Got New Point for Peace By Dina Ishqaidef.', 0, 0, 29, N'7', CAST(N'2018-12-11T13:28:54.357' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (68, 1, 6, 1, N'Osama Abdo Got New Point for Peace By Dina Ishqaidef.', N'Osama Abdo Got New Point for Peace By Dina Ishqaidef.', 0, 0, 29, N'7', CAST(N'2018-12-11T13:28:54.357' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (69, 1, 11, 1, N'Osama Abdo Got New Point for Peace By Dina Ishqaidef.', N'Osama Abdo Got New Point for Peace By Dina Ishqaidef.', 0, 0, 29, N'7', CAST(N'2018-12-11T13:28:54.357' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (70, 1, 12, 1, N'Osama Abdo Got New Point for Peace By Dina Ishqaidef.', N'Osama Abdo Got New Point for Peace By Dina Ishqaidef.', 0, 0, 29, N'7', CAST(N'2018-12-11T13:28:54.357' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (71, 1, 13, 1, N'Osama Abdo Got New Point for Peace By Dina Ishqaidef.', N'Osama Abdo Got New Point for Peace By Dina Ishqaidef.', 0, 0, 29, N'7', CAST(N'2018-12-11T13:28:54.357' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (72, 1, 14, 1, N'Osama Abdo Got New Point for Peace By Dina Ishqaidef.', N'Osama Abdo Got New Point for Peace By Dina Ishqaidef.', 0, 0, 29, N'7', CAST(N'2018-12-11T13:28:54.357' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (73, 1, 15, 1, N'Osama Abdo Got New Point for Peace By Dina Ishqaidef.', N'Osama Abdo Got New Point for Peace By Dina Ishqaidef.', 0, 0, 29, N'7', CAST(N'2018-12-11T13:28:54.357' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (74, 1, 16, 1, N'Osama Abdo Got New Point for Peace By Dina Ishqaidef.', N'Osama Abdo Got New Point for Peace By Dina Ishqaidef.', 0, 0, 29, N'7', CAST(N'2018-12-11T13:28:54.357' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (75, 1, 45, 1, N'Osama Abdo Got New Point for Peace By Dina Ishqaidef.', N'Osama Abdo Got New Point for Peace By Dina Ishqaidef.', 0, 0, 29, N'7', CAST(N'2018-12-11T13:28:54.357' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (76, 1, 46, 1, N'Osama Abdo Got New Point for Peace By Dina Ishqaidef.', N'Osama Abdo Got New Point for Peace By Dina Ishqaidef.', 0, 0, 29, N'7', CAST(N'2018-12-11T13:28:54.357' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (77, 1, 21, 5, N'Aram Aljedy Got New Point for Love By Ahmed Ghareeb.', N'Aram Aljedy Got New Point for Love By Ahmed Ghareeb.', 0, 0, 29, N'45', CAST(N'2018-12-12T19:17:49.200' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (78, 1, 22, 5, N'Aram Aljedy Got New Point for Love By Ahmed Ghareeb.', N'Aram Aljedy Got New Point for Love By Ahmed Ghareeb.', 0, 0, 29, N'45', CAST(N'2018-12-12T19:17:49.200' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (79, 1, 21, 5, N'Aram Aljedy Got New Point for Peace By Ahmed Ghareeb.', N'Aram Aljedy Got New Point for Peace By Ahmed Ghareeb.', 0, 0, 29, N'45', CAST(N'2018-12-13T15:24:32.883' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (80, 1, 22, 5, N'Aram Aljedy Got New Point for Peace By Ahmed Ghareeb.', N'Aram Aljedy Got New Point for Peace By Ahmed Ghareeb.', 0, 0, 29, N'45', CAST(N'2018-12-13T15:24:32.883' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (81, 1, 8, 2, N'Sarah Alazzam Got New Point for الصدف
الصدق By Ahmed Ghareeb.', N'Sarah Alazzam Got New Point for الصدف
الصدق By Ahmed Ghareeb.', 0, 0, 29, N'45', CAST(N'2018-12-13T17:14:05.783' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (82, 1, 9, 2, N'Sarah Alazzam Got New Point for الصدف
الصدق By Ahmed Ghareeb.', N'Sarah Alazzam Got New Point for الصدف
الصدق By Ahmed Ghareeb.', 0, 0, 29, N'45', CAST(N'2018-12-13T17:14:05.783' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (83, 1, 10, 2, N'Sarah Alazzam Got New Point for الصدف
الصدق By Ahmed Ghareeb.', N'Sarah Alazzam Got New Point for الصدف
الصدق By Ahmed Ghareeb.', 0, 0, 29, N'45', CAST(N'2018-12-13T17:14:05.783' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (84, 1, 4, 1, N'Osama Abdo Got New Point for Honesty By Hassan Nimr.', N'Osama Abdo Got New Point for Honesty By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-13T18:54:44.637' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (85, 1, 6, 1, N'Osama Abdo Got New Point for Honesty By Hassan Nimr.', N'Osama Abdo Got New Point for Honesty By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-13T18:54:44.637' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (86, 1, 7, 1, N'Osama Abdo Got New Point for Honesty By Hassan Nimr.', N'Osama Abdo Got New Point for Honesty By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-13T18:54:44.637' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (87, 1, 11, 1, N'Osama Abdo Got New Point for Honesty By Hassan Nimr.', N'Osama Abdo Got New Point for Honesty By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-13T18:54:44.637' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (88, 1, 12, 1, N'Osama Abdo Got New Point for Honesty By Hassan Nimr.', N'Osama Abdo Got New Point for Honesty By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-13T18:54:44.637' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (89, 1, 13, 1, N'Osama Abdo Got New Point for Honesty By Hassan Nimr.', N'Osama Abdo Got New Point for Honesty By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-13T18:54:44.637' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (90, 1, 14, 1, N'Osama Abdo Got New Point for Honesty By Hassan Nimr.', N'Osama Abdo Got New Point for Honesty By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-13T18:54:44.637' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (91, 1, 15, 1, N'Osama Abdo Got New Point for Honesty By Hassan Nimr.', N'Osama Abdo Got New Point for Honesty By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-13T18:54:44.637' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (92, 1, 16, 1, N'Osama Abdo Got New Point for Honesty By Hassan Nimr.', N'Osama Abdo Got New Point for Honesty By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-13T18:54:44.637' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (93, 1, 45, 1, N'Osama Abdo Got New Point for Honesty By Hassan Nimr.', N'Osama Abdo Got New Point for Honesty By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-13T18:54:44.637' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (94, 1, 46, 1, N'Osama Abdo Got New Point for Honesty By Hassan Nimr.', N'Osama Abdo Got New Point for Honesty By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-13T18:54:44.637' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (95, 1, 4, 1, N'Osama Abdo Got New Point for Modesty By Hassan Nimr.', N'Osama Abdo Got New Point for Modesty By Hassan Nimr.', 1, 0, 29, N'3', CAST(N'2018-12-14T04:06:15.223' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (96, 1, 6, 1, N'Osama Abdo Got New Point for Modesty By Hassan Nimr.', N'Osama Abdo Got New Point for Modesty By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-14T04:06:15.223' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (97, 1, 7, 1, N'Osama Abdo Got New Point for Modesty By Hassan Nimr.', N'Osama Abdo Got New Point for Modesty By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-14T04:06:15.223' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (98, 1, 11, 1, N'Osama Abdo Got New Point for Modesty By Hassan Nimr.', N'Osama Abdo Got New Point for Modesty By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-14T04:06:15.223' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (99, 1, 12, 1, N'Osama Abdo Got New Point for Modesty By Hassan Nimr.', N'Osama Abdo Got New Point for Modesty By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-14T04:06:15.223' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (100, 1, 13, 1, N'Osama Abdo Got New Point for Modesty By Hassan Nimr.', N'Osama Abdo Got New Point for Modesty By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-14T04:06:15.223' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (101, 1, 14, 1, N'Osama Abdo Got New Point for Modesty By Hassan Nimr.', N'Osama Abdo Got New Point for Modesty By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-14T04:06:15.223' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (102, 1, 15, 1, N'Osama Abdo Got New Point for Modesty By Hassan Nimr.', N'Osama Abdo Got New Point for Modesty By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-14T04:06:15.223' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (103, 1, 16, 1, N'Osama Abdo Got New Point for Modesty By Hassan Nimr.', N'Osama Abdo Got New Point for Modesty By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-14T04:06:15.223' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (104, 1, 45, 1, N'Osama Abdo Got New Point for Modesty By Hassan Nimr.', N'Osama Abdo Got New Point for Modesty By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-14T04:06:15.223' AS DateTime))
GO
INSERT [dbo].[Notification] ([NotificationId], [NotificationTypeId], [UserId], [StudentId], [MessageEn], [MessageAr], [IsRead], [IsDelete], [WeekNumber], [CreatedBy], [CreatedDate]) VALUES (105, 1, 46, 1, N'Osama Abdo Got New Point for Modesty By Hassan Nimr.', N'Osama Abdo Got New Point for Modesty By Hassan Nimr.', 0, 0, 29, N'3', CAST(N'2018-12-14T04:06:15.223' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Notification] OFF
GO
SET IDENTITY_INSERT [dbo].[NotificationType] ON 

GO
INSERT [dbo].[NotificationType] ([NotificationTypeId], [NotificationType], [MessageEn], [MessageAr]) VALUES (1, N'NewPoint', N'##Student_Name## Got New Point for ##Vale_Point## By ##User_Name##.', N'##Student_Name## Got New Point for ##Vale_Point## By ##User_Name##.')
GO
INSERT [dbo].[NotificationType] ([NotificationTypeId], [NotificationType], [MessageEn], [MessageAr]) VALUES (2, N'MonthlyEvalution', N'Monthly Evaluation For ##Student_Name## is been sent to you.', N'Monthly Evaluation For ##Student_Name## is been sent to you.')
GO
INSERT [dbo].[NotificationType] ([NotificationTypeId], [NotificationType], [MessageEn], [MessageAr]) VALUES (3, N'VacationDay', N'Tomorrow will be a Vacation Day for ##School_Name##.', N'Tomorrow will be a Vacation Day for ##School_Name##.')
GO
INSERT [dbo].[NotificationType] ([NotificationTypeId], [NotificationType], [MessageEn], [MessageAr]) VALUES (4, N'GeneralMessage', N'    ', N'  ')
GO
SET IDENTITY_INSERT [dbo].[NotificationType] OFF
GO
SET IDENTITY_INSERT [dbo].[Religion] ON 

GO
INSERT [dbo].[Religion] ([Id], [Name]) VALUES (1, N'Hindu')
GO
INSERT [dbo].[Religion] ([Id], [Name]) VALUES (2, N'Islam')
GO
INSERT [dbo].[Religion] ([Id], [Name]) VALUES (3, N'Christianity')
GO
SET IDENTITY_INSERT [dbo].[Religion] OFF
GO
SET IDENTITY_INSERT [dbo].[RoleMaster] ON 

GO
INSERT [dbo].[RoleMaster] ([RoleID], [RoleName], [IsActive]) VALUES (1, N'SuperAdmin', 1)
GO
INSERT [dbo].[RoleMaster] ([RoleID], [RoleName], [IsActive]) VALUES (2, N'Admin', 1)
GO
INSERT [dbo].[RoleMaster] ([RoleID], [RoleName], [IsActive]) VALUES (3, N'Teacher', 1)
GO
INSERT [dbo].[RoleMaster] ([RoleID], [RoleName], [IsActive]) VALUES (4, N'Guardian', 1)
GO
SET IDENTITY_INSERT [dbo].[RoleMaster] OFF
GO
SET IDENTITY_INSERT [dbo].[RoleRight] ON 

GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (1, 1, 2)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (2, 1, 9)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (3, 1, 10)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (4, 1, 13)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (5, 1, 14)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (6, 1, 48)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (7, 1, 49)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (8, 1, 50)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (9, 1, 51)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (10, 1, 52)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (11, 1, 53)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (12, 1, 54)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (13, 1, 89)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (14, 2, 10095)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (15, 2, 10096)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (16, 2, 10097)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (17, 2, 10098)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (18, 2, 10099)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (19, 2, 10100)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (20, 2, 10101)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (21, 2, 2)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (22, 2, 57)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (23, 2, 58)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (24, 2, 15)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (25, 2, 55)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (26, 2, 56)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (27, 2, 16)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (28, 2, 60)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (29, 2, 61)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (30, 2, 62)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (31, 2, 63)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (32, 2, 64)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (33, 2, 20)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (34, 2, 45)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (35, 2, 46)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (36, 2, 3)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (37, 2, 47)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (38, 2, 24)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (39, 2, 69)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (40, 2, 70)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (41, 2, 17)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (42, 2, 71)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (43, 2, 72)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (44, 2, 73)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (45, 2, 74)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (46, 2, 75)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (47, 2, 77)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (48, 2, 90)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (49, 2, 18)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (50, 2, 30)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (52, 2, 81)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (53, 2, 65)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (54, 2, 66)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (55, 2, 21)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (56, 2, 67)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (57, 2, 68)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (58, 2, 23)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (59, 2, 84)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (60, 2, 85)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (61, 2, 82)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (62, 2, 86)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (63, 2, 83)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (64, 2, 89)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (65, 2, 10091)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (66, 2, 10094)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (67, 2, 10093)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (68, 2, 91)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (69, 2, 33)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (70, 2, 34)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (71, 2, 6)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (72, 2, 35)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (73, 2, 25)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (74, 2, 44)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (75, 2, 28)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (76, 2, 42)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (77, 2, 43)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (78, 2, 12)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (79, 2, 41)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (80, 2, 27)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (81, 2, 39)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (82, 2, 40)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (83, 2, 11)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (84, 2, 38)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (85, 2, 26)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (86, 2, 36)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (87, 2, 37)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (88, 2, 8)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (89, 3, 10095)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (90, 3, 10096)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (91, 3, 10097)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (92, 3, 10098)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (93, 3, 10099)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (94, 3, 10100)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (95, 3, 10101)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (96, 3, 2)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (97, 3, 79)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (98, 3, 80)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (99, 3, 78)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (100, 3, 88)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (101, 3, 81)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (102, 3, 38)
GO
INSERT [dbo].[RoleRight] ([RoleRightId], [RoleId], [LinkId]) VALUES (103, 3, 26)
GO
SET IDENTITY_INSERT [dbo].[RoleRight] OFF
GO
SET IDENTITY_INSERT [dbo].[School] ON 

GO
INSERT [dbo].[School] ([Id], [Name], [Logo], [EmailId], [Website], [ContactNumber1], [ContactNumber2], [Board], [Address], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (1, N'Interactive Value School', NULL, N'isc.web.18@gmail.com', N'http://www.iscgate.com', N'9626552558', NULL, N'CBSC', N'Rana Complex 263 ,Al-Madenah Al-Monawarah St.', N'Do not update if not necessary !', 0, 1, N'1', CAST(N'2018-12-05T12:05:28.537' AS DateTime), N'1', CAST(N'2018-12-05T21:51:47.460' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[School] OFF
GO
SET IDENTITY_INSERT [dbo].[SectionMaster] ON 

GO
INSERT [dbo].[SectionMaster] ([Id], [Name], [ClassId], [Image], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (1, N'Section A', 1, NULL, N'Only for Dummy insert', 1, 0, N'2', CAST(N'2018-12-05T12:50:50.493' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[SectionMaster] ([Id], [Name], [ClassId], [Image], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (2, N'Section 1', 1, NULL, N'Notes', 0, 1, N'2', CAST(N'2018-12-05T12:51:02.597' AS DateTime), N'2', CAST(N'2018-12-10T15:20:20.847' AS DateTime))
GO
INSERT [dbo].[SectionMaster] ([Id], [Name], [ClassId], [Image], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (3, N'Explorers', 2, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-13T19:42:02.213' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[SectionMaster] ([Id], [Name], [ClassId], [Image], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (4, N'Rainbow', 2, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-13T19:42:45.063' AS DateTime), NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[SectionMaster] OFF
GO
SET IDENTITY_INSERT [dbo].[StudentMaster] ON 

GO
INSERT [dbo].[StudentMaster] ([Id], [SchoolId], [StudentCode], [FirstName], [LastName], [MiddleName], [Gender], [BirthDate], [RegisterDate], [ProfilePic], [BloodGroup], [Address], [Country], [City], [ZipCode], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (1, 1, N'eST2018001', N'Osama', N'Abdo', N'Yazan', N'M', CAST(N'2000-01-31T00:00:00.000' AS DateTime), CAST(N'2005-06-06T00:00:00.000' AS DateTime), NULL, N'O+', NULL, 0, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-09T20:50:19.260' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[StudentMaster] ([Id], [SchoolId], [StudentCode], [FirstName], [LastName], [MiddleName], [Gender], [BirthDate], [RegisterDate], [ProfilePic], [BloodGroup], [Address], [Country], [City], [ZipCode], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (2, 1, N'ST2018002', N'Sarah', N'Alazzam', N'Yazan', N'F', CAST(N'2000-01-30T00:00:00.000' AS DateTime), CAST(N'2006-06-01T00:00:00.000' AS DateTime), NULL, N'B+', NULL, 0, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T12:04:08.860' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[StudentMaster] ([Id], [SchoolId], [StudentCode], [FirstName], [LastName], [MiddleName], [Gender], [BirthDate], [RegisterDate], [ProfilePic], [BloodGroup], [Address], [Country], [City], [ZipCode], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (3, 1, NULL, N'Zaid', N'Sharabati', N'Mohammad', N'M', CAST(N'2010-02-15T00:00:00.000' AS DateTime), CAST(N'2018-02-11T00:00:00.000' AS DateTime), N'jal.jpg', NULL, NULL, 2, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T12:52:04.977' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[StudentMaster] ([Id], [SchoolId], [StudentCode], [FirstName], [LastName], [MiddleName], [Gender], [BirthDate], [RegisterDate], [ProfilePic], [BloodGroup], [Address], [Country], [City], [ZipCode], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (4, 1, NULL, N'Kais', N'Alqasqas', N'Thamin', N'M', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T12:58:19.910' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[StudentMaster] ([Id], [SchoolId], [StudentCode], [FirstName], [LastName], [MiddleName], [Gender], [BirthDate], [RegisterDate], [ProfilePic], [BloodGroup], [Address], [Country], [City], [ZipCode], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (5, 1, NULL, N'Aram', N'Aljedy', N'Husien', N'M', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-10T15:37:57.070' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[StudentMaster] ([Id], [SchoolId], [StudentCode], [FirstName], [LastName], [MiddleName], [Gender], [BirthDate], [RegisterDate], [ProfilePic], [BloodGroup], [Address], [Country], [City], [ZipCode], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (6, 1, NULL, N'Noor', N'Hatab', N'Awas', N'F', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:10:00.813' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[StudentMaster] ([Id], [SchoolId], [StudentCode], [FirstName], [LastName], [MiddleName], [Gender], [BirthDate], [RegisterDate], [ProfilePic], [BloodGroup], [Address], [Country], [City], [ZipCode], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (7, 1, NULL, N'Abdulaha', N'Albakri', N'Abdulbasit', N'M', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:20:37.747' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[StudentMaster] ([Id], [SchoolId], [StudentCode], [FirstName], [LastName], [MiddleName], [Gender], [BirthDate], [RegisterDate], [ProfilePic], [BloodGroup], [Address], [Country], [City], [ZipCode], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (8, 1, NULL, N'Toleen', N'Albokari', N'Fares', N'F', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:25:49.033' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[StudentMaster] ([Id], [SchoolId], [StudentCode], [FirstName], [LastName], [MiddleName], [Gender], [BirthDate], [RegisterDate], [ProfilePic], [BloodGroup], [Address], [Country], [City], [ZipCode], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (9, 1, NULL, N'Jana', N'Abu odeh', N'Yasin', N'F', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:29:13.407' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[StudentMaster] ([Id], [SchoolId], [StudentCode], [FirstName], [LastName], [MiddleName], [Gender], [BirthDate], [RegisterDate], [ProfilePic], [BloodGroup], [Address], [Country], [City], [ZipCode], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (10, 1, NULL, N'Awas', N'Alkateeb', N'Jalal', N'M', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:32:55.277' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[StudentMaster] ([Id], [SchoolId], [StudentCode], [FirstName], [LastName], [MiddleName], [Gender], [BirthDate], [RegisterDate], [ProfilePic], [BloodGroup], [Address], [Country], [City], [ZipCode], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (11, 1, NULL, N'Adam', N'Alshari', N'Mohammad', N'F', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:39:18.160' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[StudentMaster] ([Id], [SchoolId], [StudentCode], [FirstName], [LastName], [MiddleName], [Gender], [BirthDate], [RegisterDate], [ProfilePic], [BloodGroup], [Address], [Country], [City], [ZipCode], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (12, 1, NULL, N'Amir', N'Taqem', N'Mansour', N'M', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:43:30.023' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[StudentMaster] ([Id], [SchoolId], [StudentCode], [FirstName], [LastName], [MiddleName], [Gender], [BirthDate], [RegisterDate], [ProfilePic], [BloodGroup], [Address], [Country], [City], [ZipCode], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (13, 1, NULL, N'Zaid', N'Aateh', N'Tareq', N'M', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:47:58.290' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[StudentMaster] ([Id], [SchoolId], [StudentCode], [FirstName], [LastName], [MiddleName], [Gender], [BirthDate], [RegisterDate], [ProfilePic], [BloodGroup], [Address], [Country], [City], [ZipCode], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (14, 1, NULL, N'Samar', N'Alhuneidi', N'Mohammad', N'F', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:54:18.043' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[StudentMaster] ([Id], [SchoolId], [StudentCode], [FirstName], [LastName], [MiddleName], [Gender], [BirthDate], [RegisterDate], [ProfilePic], [BloodGroup], [Address], [Country], [City], [ZipCode], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (15, 1, NULL, N'Masa', N'Altarawneh', N'Mohammad', N'F', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:59:25.627' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[StudentMaster] ([Id], [SchoolId], [StudentCode], [FirstName], [LastName], [MiddleName], [Gender], [BirthDate], [RegisterDate], [ProfilePic], [BloodGroup], [Address], [Country], [City], [ZipCode], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (16, 1, NULL, N'Mohammad', N'Aldiab', N'Motasem', N'M', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-13T19:50:38.263' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[StudentMaster] ([Id], [SchoolId], [StudentCode], [FirstName], [LastName], [MiddleName], [Gender], [BirthDate], [RegisterDate], [ProfilePic], [BloodGroup], [Address], [Country], [City], [ZipCode], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (17, 1, NULL, N'Sarah', N'Albitar', N'Jamaleddin', N'F', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-13T19:57:02.723' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[StudentMaster] ([Id], [SchoolId], [StudentCode], [FirstName], [LastName], [MiddleName], [Gender], [BirthDate], [RegisterDate], [ProfilePic], [BloodGroup], [Address], [Country], [City], [ZipCode], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (18, 1, NULL, N'Lara', N'Alhusban', N'Marwan', N'F', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-13T20:06:18.193' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[StudentMaster] ([Id], [SchoolId], [StudentCode], [FirstName], [LastName], [MiddleName], [Gender], [BirthDate], [RegisterDate], [ProfilePic], [BloodGroup], [Address], [Country], [City], [ZipCode], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (19, 1, NULL, N'Joury', N'Zater', N'Mamoun', N'M', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-13T20:10:37.310' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[StudentMaster] ([Id], [SchoolId], [StudentCode], [FirstName], [LastName], [MiddleName], [Gender], [BirthDate], [RegisterDate], [ProfilePic], [BloodGroup], [Address], [Country], [City], [ZipCode], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (20, 1, NULL, N'Farah', N'Alazzam', N'Yazan', N'F', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-13T20:15:25.327' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[StudentMaster] ([Id], [SchoolId], [StudentCode], [FirstName], [LastName], [MiddleName], [Gender], [BirthDate], [RegisterDate], [ProfilePic], [BloodGroup], [Address], [Country], [City], [ZipCode], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (21, 1, NULL, N'Adam', N'Hasan', N'Abdelraheem', N'M', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-13T20:21:13.110' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[StudentMaster] ([Id], [SchoolId], [StudentCode], [FirstName], [LastName], [MiddleName], [Gender], [BirthDate], [RegisterDate], [ProfilePic], [BloodGroup], [Address], [Country], [City], [ZipCode], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (22, 1, NULL, N'Qais', N'Othman', N'Ahmad', N'M', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-13T20:26:16.880' AS DateTime), NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[StudentMaster] OFF
GO
SET IDENTITY_INSERT [dbo].[StudentSchoolDetail] ON 

GO
INSERT [dbo].[StudentSchoolDetail] ([Id], [StudentId], [JoiningDate], [AcademicYearId], [ClassRollNo], [ClassId], [SectionId], [IsCurrent], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (1, 1, NULL, 3, N'ST2018001', 1, 2, 1, N'Notes', 0, 1, N'2', CAST(N'2018-12-05T14:41:25.387' AS DateTime), N'2', CAST(N'2018-12-09T20:50:19.337' AS DateTime))
GO
INSERT [dbo].[StudentSchoolDetail] ([Id], [StudentId], [JoiningDate], [AcademicYearId], [ClassRollNo], [ClassId], [SectionId], [IsCurrent], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (2, 2, NULL, 3, N'ST2018002', 1, 2, 1, N'Notes', 0, 1, N'2', CAST(N'2018-12-05T14:43:38.203' AS DateTime), N'2', CAST(N'2018-12-06T12:04:08.873' AS DateTime))
GO
INSERT [dbo].[StudentSchoolDetail] ([Id], [StudentId], [JoiningDate], [AcademicYearId], [ClassRollNo], [ClassId], [SectionId], [IsCurrent], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (3, 3, NULL, 3, NULL, 1, 2, 1, NULL, 0, 1, N'2', CAST(N'2018-12-05T18:42:34.857' AS DateTime), N'2', CAST(N'2018-12-06T12:52:04.997' AS DateTime))
GO
INSERT [dbo].[StudentSchoolDetail] ([Id], [StudentId], [JoiningDate], [AcademicYearId], [ClassRollNo], [ClassId], [SectionId], [IsCurrent], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (4, 4, NULL, 3, NULL, 1, 2, 1, NULL, 0, 1, N'2', CAST(N'2018-12-06T12:58:19.923' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[StudentSchoolDetail] ([Id], [StudentId], [JoiningDate], [AcademicYearId], [ClassRollNo], [ClassId], [SectionId], [IsCurrent], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (5, 5, NULL, 3, NULL, 1, 2, 1, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:02:52.813' AS DateTime), N'2', CAST(N'2018-12-10T15:37:57.137' AS DateTime))
GO
INSERT [dbo].[StudentSchoolDetail] ([Id], [StudentId], [JoiningDate], [AcademicYearId], [ClassRollNo], [ClassId], [SectionId], [IsCurrent], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (6, 6, NULL, 3, NULL, 1, 2, 1, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:10:00.820' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[StudentSchoolDetail] ([Id], [StudentId], [JoiningDate], [AcademicYearId], [ClassRollNo], [ClassId], [SectionId], [IsCurrent], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (7, 7, NULL, 3, NULL, 1, 2, 1, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:20:37.767' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[StudentSchoolDetail] ([Id], [StudentId], [JoiningDate], [AcademicYearId], [ClassRollNo], [ClassId], [SectionId], [IsCurrent], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (8, 8, NULL, 3, NULL, 1, 2, 1, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:25:49.083' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[StudentSchoolDetail] ([Id], [StudentId], [JoiningDate], [AcademicYearId], [ClassRollNo], [ClassId], [SectionId], [IsCurrent], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (9, 9, NULL, 3, NULL, 1, 2, 1, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:29:13.413' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[StudentSchoolDetail] ([Id], [StudentId], [JoiningDate], [AcademicYearId], [ClassRollNo], [ClassId], [SectionId], [IsCurrent], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (10, 10, NULL, 3, NULL, 1, 2, 1, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:32:55.283' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[StudentSchoolDetail] ([Id], [StudentId], [JoiningDate], [AcademicYearId], [ClassRollNo], [ClassId], [SectionId], [IsCurrent], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (11, 11, NULL, 3, NULL, 1, 2, 1, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:39:18.180' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[StudentSchoolDetail] ([Id], [StudentId], [JoiningDate], [AcademicYearId], [ClassRollNo], [ClassId], [SectionId], [IsCurrent], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (12, 12, NULL, 3, NULL, 1, 2, 1, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:43:30.037' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[StudentSchoolDetail] ([Id], [StudentId], [JoiningDate], [AcademicYearId], [ClassRollNo], [ClassId], [SectionId], [IsCurrent], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (13, 13, NULL, 3, NULL, 1, 2, 1, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:47:58.297' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[StudentSchoolDetail] ([Id], [StudentId], [JoiningDate], [AcademicYearId], [ClassRollNo], [ClassId], [SectionId], [IsCurrent], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (14, 14, NULL, 3, NULL, 1, 2, 1, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:54:18.053' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[StudentSchoolDetail] ([Id], [StudentId], [JoiningDate], [AcademicYearId], [ClassRollNo], [ClassId], [SectionId], [IsCurrent], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (15, 15, NULL, 3, NULL, 1, 2, 1, NULL, 0, 1, N'2', CAST(N'2018-12-06T13:59:25.637' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[StudentSchoolDetail] ([Id], [StudentId], [JoiningDate], [AcademicYearId], [ClassRollNo], [ClassId], [SectionId], [IsCurrent], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (16, 16, NULL, 3, NULL, 2, 3, 1, NULL, 0, 1, N'2', CAST(N'2018-12-13T19:50:38.340' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[StudentSchoolDetail] ([Id], [StudentId], [JoiningDate], [AcademicYearId], [ClassRollNo], [ClassId], [SectionId], [IsCurrent], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (17, 17, NULL, 3, NULL, 2, 3, 1, NULL, 0, 1, N'2', CAST(N'2018-12-13T19:57:02.783' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[StudentSchoolDetail] ([Id], [StudentId], [JoiningDate], [AcademicYearId], [ClassRollNo], [ClassId], [SectionId], [IsCurrent], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (18, 18, NULL, 3, NULL, 2, 3, 1, NULL, 0, 1, N'2', CAST(N'2018-12-13T20:06:18.203' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[StudentSchoolDetail] ([Id], [StudentId], [JoiningDate], [AcademicYearId], [ClassRollNo], [ClassId], [SectionId], [IsCurrent], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (19, 19, NULL, 3, NULL, 2, 3, 1, NULL, 0, 1, N'2', CAST(N'2018-12-13T20:10:37.393' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[StudentSchoolDetail] ([Id], [StudentId], [JoiningDate], [AcademicYearId], [ClassRollNo], [ClassId], [SectionId], [IsCurrent], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (20, 20, NULL, 3, NULL, 2, 3, 1, NULL, 0, 1, N'2', CAST(N'2018-12-13T20:15:25.353' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[StudentSchoolDetail] ([Id], [StudentId], [JoiningDate], [AcademicYearId], [ClassRollNo], [ClassId], [SectionId], [IsCurrent], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (21, 21, NULL, 3, NULL, 2, 3, 1, NULL, 0, 1, N'2', CAST(N'2018-12-13T20:19:05.590' AS DateTime), N'2', CAST(N'2018-12-13T20:21:13.190' AS DateTime))
GO
INSERT [dbo].[StudentSchoolDetail] ([Id], [StudentId], [JoiningDate], [AcademicYearId], [ClassRollNo], [ClassId], [SectionId], [IsCurrent], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (22, 22, NULL, 3, NULL, 2, 3, 1, NULL, 0, 1, N'2', CAST(N'2018-12-13T20:26:16.887' AS DateTime), NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[StudentSchoolDetail] OFF
GO
SET IDENTITY_INSERT [dbo].[SubjectMaster] ON 

GO
INSERT [dbo].[SubjectMaster] ([Id], [Name], [SchoolId], [Types], [Note], [Image], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (1, N'HRT (English ,Math,Science,social)', 1, 1, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-05T14:50:19.080' AS DateTime), N'2', CAST(N'2018-12-06T12:34:24.850' AS DateTime))
GO
INSERT [dbo].[SubjectMaster] ([Id], [Name], [SchoolId], [Types], [Note], [Image], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (2, N'Arabic ', 1, 1, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T12:34:36.437' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[SubjectMaster] ([Id], [Name], [SchoolId], [Types], [Note], [Image], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (3, N'Religion  ', 1, 1, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T12:34:46.560' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[SubjectMaster] ([Id], [Name], [SchoolId], [Types], [Note], [Image], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (4, N'ICT', 1, 1, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T12:34:56.497' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[SubjectMaster] ([Id], [Name], [SchoolId], [Types], [Note], [Image], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (5, N'P.E', 1, 1, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T12:35:08.060' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[SubjectMaster] ([Id], [Name], [SchoolId], [Types], [Note], [Image], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (6, N'Art', 1, 1, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T12:40:17.557' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[SubjectMaster] ([Id], [Name], [SchoolId], [Types], [Note], [Image], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (7, N'test ', 1, 1, NULL, NULL, 1, 0, N'2', CAST(N'2018-12-07T12:16:36.080' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[SubjectMaster] ([Id], [Name], [SchoolId], [Types], [Note], [Image], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (8, N'kk', 1, 1, NULL, NULL, 1, 0, N'2', CAST(N'2018-12-07T12:16:41.310' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[SubjectMaster] ([Id], [Name], [SchoolId], [Types], [Note], [Image], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (9, N'Mang', 1, 1, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-08T16:46:10.787' AS DateTime), N'2', CAST(N'2018-12-13T11:30:10.217' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[SubjectMaster] OFF
GO
SET IDENTITY_INSERT [dbo].[TeacherDetail] ON 

GO
INSERT [dbo].[TeacherDetail] ([Id], [TeacherId], [ClassId], [SectionId], [SubjectId], [IsHomeTeacher], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (1, 4, 1, 1, 1, 0, 0, 1, N'2', CAST(N'2018-12-05T14:53:03.330' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[TeacherDetail] ([Id], [TeacherId], [ClassId], [SectionId], [SubjectId], [IsHomeTeacher], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (2, 4, 1, 2, 1, 1, 0, 1, N'2', CAST(N'2018-12-05T14:53:08.563' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[TeacherDetail] ([Id], [TeacherId], [ClassId], [SectionId], [SubjectId], [IsHomeTeacher], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (3, 11, 1, 2, 2, 0, 0, 1, N'2', CAST(N'2018-12-06T12:37:27.023' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[TeacherDetail] ([Id], [TeacherId], [ClassId], [SectionId], [SubjectId], [IsHomeTeacher], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (4, 12, 1, 2, 3, 0, 0, 1, N'2', CAST(N'2018-12-06T12:37:46.927' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[TeacherDetail] ([Id], [TeacherId], [ClassId], [SectionId], [SubjectId], [IsHomeTeacher], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (5, 13, 1, 2, 4, 0, 0, 1, N'2', CAST(N'2018-12-06T12:38:21.673' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[TeacherDetail] ([Id], [TeacherId], [ClassId], [SectionId], [SubjectId], [IsHomeTeacher], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (6, 14, 1, 2, 5, 0, 0, 1, N'2', CAST(N'2018-12-06T12:38:56.143' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[TeacherDetail] ([Id], [TeacherId], [ClassId], [SectionId], [SubjectId], [IsHomeTeacher], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (7, 16, 1, 2, 1, 0, 0, 1, N'2', CAST(N'2018-12-06T12:39:49.297' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[TeacherDetail] ([Id], [TeacherId], [ClassId], [SectionId], [SubjectId], [IsHomeTeacher], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (8, 15, 1, 2, 6, 0, 0, 1, N'2', CAST(N'2018-12-06T12:40:41.587' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[TeacherDetail] ([Id], [TeacherId], [ClassId], [SectionId], [SubjectId], [IsHomeTeacher], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (9, 45, 1, 0, 2, 0, 0, 1, N'2', CAST(N'2018-12-08T16:44:59.603' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[TeacherDetail] ([Id], [TeacherId], [ClassId], [SectionId], [SubjectId], [IsHomeTeacher], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (10, 46, 1, 0, 6, 0, 0, 1, N'2', CAST(N'2018-12-11T12:53:11.527' AS DateTime), NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[TeacherDetail] OFF
GO
SET IDENTITY_INSERT [dbo].[TeacherMaster] ON 

GO
INSERT [dbo].[TeacherMaster] ([Id], [UserId], [TeacherCode], [MiddleName], [Landline], [Gender], [BirthDate], [HireDate], [BloodGroup], [IdType], [IdNumber], [PermanentAddress], [CurrentAddress], [Country], [City], [ZipCode], [University], [HouseId], [HomeTeacher], [Religion], [Speciality], [HasAccount]) VALUES (1, 4, NULL, N'Ahmad', NULL, 0, CAST(N'1990-12-24' AS Date), CAST(N'2018-12-06' AS Date), NULL, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 1, 0, 1, 0, 0)
GO
INSERT [dbo].[TeacherMaster] ([Id], [UserId], [TeacherCode], [MiddleName], [Landline], [Gender], [BirthDate], [HireDate], [BloodGroup], [IdType], [IdNumber], [PermanentAddress], [CurrentAddress], [Country], [City], [ZipCode], [University], [HouseId], [HomeTeacher], [Religion], [Speciality], [HasAccount]) VALUES (2, 11, NULL, N'Husni', NULL, 0, NULL, NULL, NULL, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 4, 0, 1, 0, 0)
GO
INSERT [dbo].[TeacherMaster] ([Id], [UserId], [TeacherCode], [MiddleName], [Landline], [Gender], [BirthDate], [HireDate], [BloodGroup], [IdType], [IdNumber], [PermanentAddress], [CurrentAddress], [Country], [City], [ZipCode], [University], [HouseId], [HomeTeacher], [Religion], [Speciality], [HasAccount]) VALUES (3, 12, NULL, N'Ishaq', NULL, 0, NULL, NULL, NULL, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 2, 0, 1, 0, 0)
GO
INSERT [dbo].[TeacherMaster] ([Id], [UserId], [TeacherCode], [MiddleName], [Landline], [Gender], [BirthDate], [HireDate], [BloodGroup], [IdType], [IdNumber], [PermanentAddress], [CurrentAddress], [Country], [City], [ZipCode], [University], [HouseId], [HomeTeacher], [Religion], [Speciality], [HasAccount]) VALUES (4, 13, NULL, N'Samir', NULL, 1, NULL, NULL, NULL, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 4, 0, 1, 0, 0)
GO
INSERT [dbo].[TeacherMaster] ([Id], [UserId], [TeacherCode], [MiddleName], [Landline], [Gender], [BirthDate], [HireDate], [BloodGroup], [IdType], [IdNumber], [PermanentAddress], [CurrentAddress], [Country], [City], [ZipCode], [University], [HouseId], [HomeTeacher], [Religion], [Speciality], [HasAccount]) VALUES (5, 14, NULL, N'Nabil', NULL, 0, NULL, NULL, NULL, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 2, 0, 1, 0, 0)
GO
INSERT [dbo].[TeacherMaster] ([Id], [UserId], [TeacherCode], [MiddleName], [Landline], [Gender], [BirthDate], [HireDate], [BloodGroup], [IdType], [IdNumber], [PermanentAddress], [CurrentAddress], [Country], [City], [ZipCode], [University], [HouseId], [HomeTeacher], [Religion], [Speciality], [HasAccount]) VALUES (6, 15, NULL, N'Tawfiq', NULL, 0, NULL, NULL, NULL, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 2, 0, 1, 0, 0)
GO
INSERT [dbo].[TeacherMaster] ([Id], [UserId], [TeacherCode], [MiddleName], [Landline], [Gender], [BirthDate], [HireDate], [BloodGroup], [IdType], [IdNumber], [PermanentAddress], [CurrentAddress], [Country], [City], [ZipCode], [University], [HouseId], [HomeTeacher], [Religion], [Speciality], [HasAccount]) VALUES (7, 16, NULL, N'Nawaff', NULL, 0, NULL, NULL, NULL, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 2, 0, 1, 0, 0)
GO
INSERT [dbo].[TeacherMaster] ([Id], [UserId], [TeacherCode], [MiddleName], [Landline], [Gender], [BirthDate], [HireDate], [BloodGroup], [IdType], [IdNumber], [PermanentAddress], [CurrentAddress], [Country], [City], [ZipCode], [University], [HouseId], [HomeTeacher], [Religion], [Speciality], [HasAccount]) VALUES (8, 44, NULL, N'fawzi', NULL, 0, NULL, NULL, NULL, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 2, 0, 1, 0, 0)
GO
INSERT [dbo].[TeacherMaster] ([Id], [UserId], [TeacherCode], [MiddleName], [Landline], [Gender], [BirthDate], [HireDate], [BloodGroup], [IdType], [IdNumber], [PermanentAddress], [CurrentAddress], [Country], [City], [ZipCode], [University], [HouseId], [HomeTeacher], [Religion], [Speciality], [HasAccount]) VALUES (9, 45, NULL, N'Fawzi', NULL, 0, NULL, NULL, NULL, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 4, 0, 1, 0, 0)
GO
INSERT [dbo].[TeacherMaster] ([Id], [UserId], [TeacherCode], [MiddleName], [Landline], [Gender], [BirthDate], [HireDate], [BloodGroup], [IdType], [IdNumber], [PermanentAddress], [CurrentAddress], [Country], [City], [ZipCode], [University], [HouseId], [HomeTeacher], [Religion], [Speciality], [HasAccount]) VALUES (10, 46, NULL, N'mohammed', NULL, 0, NULL, NULL, NULL, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 4, 0, 1, 0, 0)
GO
SET IDENTITY_INSERT [dbo].[TeacherMaster] OFF
GO
SET IDENTITY_INSERT [dbo].[ValuePointDetail] ON 

GO
INSERT [dbo].[ValuePointDetail] ([Id], [ValuePointId], [AddedBy], [WeekNumber], [AccademidYearID], [StudentId], [Note], [CreatedDate]) VALUES (1, 1, 4, 28, 3, 2, N'', CAST(N'2018-12-05T15:01:54.290' AS DateTime))
GO
INSERT [dbo].[ValuePointDetail] ([Id], [ValuePointId], [AddedBy], [WeekNumber], [AccademidYearID], [StudentId], [Note], [CreatedDate]) VALUES (2, 0, 4, 28, 3, 1, N'Good Answer', CAST(N'2018-12-05T15:02:10.783' AS DateTime))
GO
INSERT [dbo].[ValuePointDetail] ([Id], [ValuePointId], [AddedBy], [WeekNumber], [AccademidYearID], [StudentId], [Note], [CreatedDate]) VALUES (3, 2, 3, 28, 3, 1, N'', CAST(N'2018-12-05T15:34:22.793' AS DateTime))
GO
INSERT [dbo].[ValuePointDetail] ([Id], [ValuePointId], [AddedBy], [WeekNumber], [AccademidYearID], [StudentId], [Note], [CreatedDate]) VALUES (4, 1, 4, 28, 3, 1, N'', CAST(N'2018-12-05T15:49:14.717' AS DateTime))
GO
INSERT [dbo].[ValuePointDetail] ([Id], [ValuePointId], [AddedBy], [WeekNumber], [AccademidYearID], [StudentId], [Note], [CreatedDate]) VALUES (5, 0, 4, 28, 3, 2, N'Dance', CAST(N'2018-12-05T15:55:56.733' AS DateTime))
GO
INSERT [dbo].[ValuePointDetail] ([Id], [ValuePointId], [AddedBy], [WeekNumber], [AccademidYearID], [StudentId], [Note], [CreatedDate]) VALUES (6, 1, 4, 28, 3, 3, N'', CAST(N'2018-12-05T18:46:13.703' AS DateTime))
GO
INSERT [dbo].[ValuePointDetail] ([Id], [ValuePointId], [AddedBy], [WeekNumber], [AccademidYearID], [StudentId], [Note], [CreatedDate]) VALUES (7, 3, 7, 29, 3, 1, N'', CAST(N'2018-12-09T14:57:41.910' AS DateTime))
GO
INSERT [dbo].[ValuePointDetail] ([Id], [ValuePointId], [AddedBy], [WeekNumber], [AccademidYearID], [StudentId], [Note], [CreatedDate]) VALUES (8, 12, 45, 29, 3, 1, N'', CAST(N'2018-12-09T15:08:03.780' AS DateTime))
GO
INSERT [dbo].[ValuePointDetail] ([Id], [ValuePointId], [AddedBy], [WeekNumber], [AccademidYearID], [StudentId], [Note], [CreatedDate]) VALUES (9, 3, 45, 29, 3, 2, N'', CAST(N'2018-12-09T18:13:19.583' AS DateTime))
GO
INSERT [dbo].[ValuePointDetail] ([Id], [ValuePointId], [AddedBy], [WeekNumber], [AccademidYearID], [StudentId], [Note], [CreatedDate]) VALUES (10, 4, 3, 29, 3, 1, N'', CAST(N'2018-12-09T20:35:23.477' AS DateTime))
GO
INSERT [dbo].[ValuePointDetail] ([Id], [ValuePointId], [AddedBy], [WeekNumber], [AccademidYearID], [StudentId], [Note], [CreatedDate]) VALUES (12, 12, 3, 29, 3, 1, N'', CAST(N'2018-12-10T12:48:56.820' AS DateTime))
GO
INSERT [dbo].[ValuePointDetail] ([Id], [ValuePointId], [AddedBy], [WeekNumber], [AccademidYearID], [StudentId], [Note], [CreatedDate]) VALUES (13, 0, 4, 29, 3, 1, N'Testing', CAST(N'2018-12-10T15:05:30.907' AS DateTime))
GO
INSERT [dbo].[ValuePointDetail] ([Id], [ValuePointId], [AddedBy], [WeekNumber], [AccademidYearID], [StudentId], [Note], [CreatedDate]) VALUES (14, 12, 45, 29, 3, 3, N'', CAST(N'2018-12-10T15:07:09.813' AS DateTime))
GO
INSERT [dbo].[ValuePointDetail] ([Id], [ValuePointId], [AddedBy], [WeekNumber], [AccademidYearID], [StudentId], [Note], [CreatedDate]) VALUES (15, 12, 7, 29, 3, 1, N'', CAST(N'2018-12-10T17:05:20.323' AS DateTime))
GO
INSERT [dbo].[ValuePointDetail] ([Id], [ValuePointId], [AddedBy], [WeekNumber], [AccademidYearID], [StudentId], [Note], [CreatedDate]) VALUES (16, 2, 7, 29, 3, 1, N'', CAST(N'2018-12-11T13:28:54.267' AS DateTime))
GO
INSERT [dbo].[ValuePointDetail] ([Id], [ValuePointId], [AddedBy], [WeekNumber], [AccademidYearID], [StudentId], [Note], [CreatedDate]) VALUES (17, 3, 45, 29, 3, 5, N'', CAST(N'2018-12-12T19:17:48.707' AS DateTime))
GO
INSERT [dbo].[ValuePointDetail] ([Id], [ValuePointId], [AddedBy], [WeekNumber], [AccademidYearID], [StudentId], [Note], [CreatedDate]) VALUES (18, 2, 45, 29, 3, 5, N'', CAST(N'2018-12-13T15:24:32.400' AS DateTime))
GO
INSERT [dbo].[ValuePointDetail] ([Id], [ValuePointId], [AddedBy], [WeekNumber], [AccademidYearID], [StudentId], [Note], [CreatedDate]) VALUES (19, 0, 45, 29, 3, 2, N'الصدف
الصدق', CAST(N'2018-12-13T17:14:05.293' AS DateTime))
GO
INSERT [dbo].[ValuePointDetail] ([Id], [ValuePointId], [AddedBy], [WeekNumber], [AccademidYearID], [StudentId], [Note], [CreatedDate]) VALUES (20, 5, 3, 29, 3, 1, N'', CAST(N'2018-12-13T18:54:44.537' AS DateTime))
GO
INSERT [dbo].[ValuePointDetail] ([Id], [ValuePointId], [AddedBy], [WeekNumber], [AccademidYearID], [StudentId], [Note], [CreatedDate]) VALUES (21, 7, 3, 29, 3, 1, N'', CAST(N'2018-12-14T04:06:14.747' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[ValuePointDetail] OFF
GO
SET IDENTITY_INSERT [dbo].[ValuePointMaster] ON 

GO
INSERT [dbo].[ValuePointMaster] ([Id], [Name], [SchoolId], [Image], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (1, N'Respect', 1, NULL, N'Note', 0, 1, N'2', CAST(N'2018-12-05T14:50:45.817' AS DateTime), N'2', CAST(N'2018-12-06T12:07:16.883' AS DateTime))
GO
INSERT [dbo].[ValuePointMaster] ([Id], [Name], [SchoolId], [Image], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (2, N'Peace', 1, NULL, N'Note', 0, 1, N'2', CAST(N'2018-12-05T14:52:03.367' AS DateTime), N'2', CAST(N'2018-12-06T12:06:00.663' AS DateTime))
GO
INSERT [dbo].[ValuePointMaster] ([Id], [Name], [SchoolId], [Image], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (3, N'Love', 1, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T12:07:28.523' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[ValuePointMaster] ([Id], [Name], [SchoolId], [Image], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (4, N'Happiness', 1, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T12:07:48.330' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[ValuePointMaster] ([Id], [Name], [SchoolId], [Image], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (5, N'Honesty', 1, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T12:08:36.637' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[ValuePointMaster] ([Id], [Name], [SchoolId], [Image], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (6, N'Ram test', 1, NULL, N'just test', 1, 0, N'2', CAST(N'2018-12-06T12:08:44.140' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[ValuePointMaster] ([Id], [Name], [SchoolId], [Image], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (7, N'Modesty', 1, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T12:08:51.770' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[ValuePointMaster] ([Id], [Name], [SchoolId], [Image], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (8, N'Responsibility', 1, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T12:09:17.083' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[ValuePointMaster] ([Id], [Name], [SchoolId], [Image], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (9, N'Simplicity', 1, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T12:09:32.683' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[ValuePointMaster] ([Id], [Name], [SchoolId], [Image], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (10, N'Forgiveness', 1, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T12:10:15.523' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[ValuePointMaster] ([Id], [Name], [SchoolId], [Image], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (11, N'Coopration', 1, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T12:11:08.613' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[ValuePointMaster] ([Id], [Name], [SchoolId], [Image], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (12, N'Freedom', 1, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T12:11:24.260' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[ValuePointMaster] ([Id], [Name], [SchoolId], [Image], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (13, N'Union', 1, NULL, NULL, 0, 1, N'2', CAST(N'2018-12-06T12:12:19.630' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[ValuePointMaster] ([Id], [Name], [SchoolId], [Image], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (14, N'Ram test', 1, NULL, NULL, 1, 0, N'2', CAST(N'2018-12-06T12:13:19.863' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[ValuePointMaster] ([Id], [Name], [SchoolId], [Image], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (15, N'New Value', 1, NULL, NULL, 1, 0, N'2', CAST(N'2018-12-06T12:14:43.120' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[ValuePointMaster] ([Id], [Name], [SchoolId], [Image], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (16, N'New Dada', 1, NULL, NULL, 1, 0, N'2', CAST(N'2018-12-06T12:19:00.757' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[ValuePointMaster] ([Id], [Name], [SchoolId], [Image], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (17, N'rtgert', 1, NULL, NULL, 1, 0, N'2', CAST(N'2018-12-06T12:19:05.957' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[ValuePointMaster] ([Id], [Name], [SchoolId], [Image], [Note], [IsDelete], [IsActive], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (18, N'Sweetness 1', 1, NULL, NULL, 1, 0, N'2', CAST(N'2018-12-06T12:25:01.130' AS DateTime), NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[ValuePointMaster] OFF
GO
ALTER TABLE [dbo].[AboutUsDetail] ADD  CONSTRAINT [DF_AboutUsDetail_IsActive]  DEFAULT ((0)) FOR [IsActive]
GO
ALTER TABLE [dbo].[AccademicYear] ADD  CONSTRAINT [DF_AccademicYear_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[AccademicYear] ADD  CONSTRAINT [DF_AccademicYear_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[AccademicYear] ADD  CONSTRAINT [DF_AccademicYear_IsCurrent]  DEFAULT ((0)) FOR [IsCurrent]
GO
ALTER TABLE [dbo].[AccademicYear] ADD  CONSTRAINT [DF_AccademicYear_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[AccademicYear] ADD  CONSTRAINT [DF_AccademicYear_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[AccademicYear] ADD  CONSTRAINT [DF_AccademicYear_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[AccademicYear] ADD  CONSTRAINT [DF_AccademicYear_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[ClassMaster] ADD  CONSTRAINT [DF_ClassMaster_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[ClassMaster] ADD  CONSTRAINT [DF_ClassMaster_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[ClassMaster] ADD  CONSTRAINT [DF_ClassMaster_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[ClassMaster] ADD  CONSTRAINT [DF_ClassMaster_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[ClassMaster] ADD  CONSTRAINT [DF_ClassMaster_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[ClassMaster] ADD  CONSTRAINT [DF_ClassMaster_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[DeviceInfo] ADD  CONSTRAINT [DF_DeviceInfo_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[DeviceInfo] ADD  CONSTRAINT [DF_DeviceInfo_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[EmailTemplate] ADD  CONSTRAINT [DF_EmailTemplate_IsEdit]  DEFAULT ((1)) FOR [IsEdit]
GO
ALTER TABLE [dbo].[EmailTemplate] ADD  CONSTRAINT [DF_EmailTemplate_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[EmailTemplate] ADD  CONSTRAINT [DF_EmailTemplate_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[EmailTemplate] ADD  CONSTRAINT [DF_EmailTemplate_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[EmailTemplate] ADD  CONSTRAINT [DF_EmailTemplate_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[EmailTemplate] ADD  CONSTRAINT [DF_EmailTemplate_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[EmailTemplate] ADD  CONSTRAINT [DF_EmailTemplate_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[EmailTemplateTag] ADD  CONSTRAINT [DF_EmailTemplateTag_IsActive_1]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[GroupMaster] ADD  CONSTRAINT [DF_GroupMaster_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[GroupMaster] ADD  CONSTRAINT [DF_GroupMaster_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[GroupMaster] ADD  CONSTRAINT [DF_GroupMaster_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[GroupMaster] ADD  CONSTRAINT [DF_GroupMaster_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[GroupMaster] ADD  CONSTRAINT [DF_GroupMaster_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[GroupMaster] ADD  CONSTRAINT [DF_GroupMaster_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[GroupMessage] ADD  CONSTRAINT [DF_GroupMessage_WeekNumber]  DEFAULT ((1)) FOR [WeekNumber]
GO
ALTER TABLE [dbo].[GroupMessage] ADD  CONSTRAINT [DF_GroupMessage_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[GroupMessageDetail] ADD  CONSTRAINT [DF_GroupMessageDetail_GroupMessageId]  DEFAULT ((0)) FOR [GroupMessageId]
GO
ALTER TABLE [dbo].[GuardianMaster] ADD  CONSTRAINT [DF_GuardianMaster_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[GuardianMaster] ADD  CONSTRAINT [DF_GuardianMaster_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[GuardianMaster] ADD  CONSTRAINT [DF_GuardianMaster_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[GuardianMaster] ADD  CONSTRAINT [DF_GuardianMaster_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[GuardianMaster] ADD  CONSTRAINT [DF_GuardianMaster_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[GuardianMaster] ADD  CONSTRAINT [DF_GuardianMaster_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[HouseMaster] ADD  CONSTRAINT [DF_HouseMaster_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[HouseMaster] ADD  CONSTRAINT [DF_HouseMaster_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[HouseMaster] ADD  CONSTRAINT [DF_HouseMaster_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[HouseMaster] ADD  CONSTRAINT [DF_HouseMaster_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[HouseMaster] ADD  CONSTRAINT [DF_HouseMaster_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[HouseMaster] ADD  CONSTRAINT [DF_HouseMaster_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[Link] ADD  CONSTRAINT [DF_Link_IsDefault]  DEFAULT ((1)) FOR [IsDefault]
GO
ALTER TABLE [dbo].[Link] ADD  CONSTRAINT [DF_Link_IsSingle]  DEFAULT ((0)) FOR [IsSingle]
GO
ALTER TABLE [dbo].[Link] ADD  CONSTRAINT [DF_Link_IsPage]  DEFAULT ((1)) FOR [IsPage]
GO
ALTER TABLE [dbo].[Link] ADD  CONSTRAINT [DF_Link_ParentId]  DEFAULT ((0)) FOR [ParentId]
GO
ALTER TABLE [dbo].[Link] ADD  CONSTRAINT [DF_Link_IsVisible]  DEFAULT ((0)) FOR [IsVisible]
GO
ALTER TABLE [dbo].[Link] ADD  CONSTRAINT [DF_Link_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[Link] ADD  CONSTRAINT [DF_Link_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Link] ADD  CONSTRAINT [DF_Link_UpdatedBy_1]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[Link] ADD  CONSTRAINT [DF_Link_UpdatedDate_1]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[LoginCredentials] ADD  CONSTRAINT [DF_LoginCredentials_SchoolId]  DEFAULT ((0)) FOR [SchoolId]
GO
ALTER TABLE [dbo].[LoginCredentials] ADD  CONSTRAINT [DF_LoginCredentials_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[LoginCredentials] ADD  CONSTRAINT [DF_LoginCredentials_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[Module] ADD  CONSTRAINT [DF_Module_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[Module] ADD  CONSTRAINT [DF_Module_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Notification] ADD  CONSTRAINT [DF_Table_1_IsActive]  DEFAULT ((1)) FOR [IsRead]
GO
ALTER TABLE [dbo].[Notification] ADD  CONSTRAINT [DF_NotificationList_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[Notification] ADD  CONSTRAINT [DF_Notification_WeekNumber]  DEFAULT ((1)) FOR [WeekNumber]
GO
ALTER TABLE [dbo].[Notification] ADD  CONSTRAINT [DF_NotificationList_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[Notification] ADD  CONSTRAINT [DF_NotificationList_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[RoleRight] ADD  CONSTRAINT [DF_Table_1_UserId]  DEFAULT ((0)) FOR [RoleId]
GO
ALTER TABLE [dbo].[School] ADD  CONSTRAINT [DF_School_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[School] ADD  CONSTRAINT [DF_School_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[School] ADD  CONSTRAINT [DF_School_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[School] ADD  CONSTRAINT [DF_School_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[School] ADD  CONSTRAINT [DF_School_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[School] ADD  CONSTRAINT [DF_School_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[SectionMaster] ADD  CONSTRAINT [DF_SectionMaster_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[SectionMaster] ADD  CONSTRAINT [DF_SectionMaster_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[SectionMaster] ADD  CONSTRAINT [DF_SectionMaster_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[SectionMaster] ADD  CONSTRAINT [DF_SectionMaster_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[SectionMaster] ADD  CONSTRAINT [DF_SectionMaster_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[SectionMaster] ADD  CONSTRAINT [DF_SectionMaster_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[StudentMaster] ADD  CONSTRAINT [DF_StudentMaster_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[StudentMaster] ADD  CONSTRAINT [DF_StudentMaster_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[StudentMaster] ADD  CONSTRAINT [DF_StudentMaster_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[StudentMaster] ADD  CONSTRAINT [DF_StudentMaster_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[StudentMaster] ADD  CONSTRAINT [DF_StudentMaster_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[StudentMaster] ADD  CONSTRAINT [DF_StudentMaster_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[StudentSchoolDetail] ADD  CONSTRAINT [DF_StudentSchoolDetail_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[StudentSchoolDetail] ADD  CONSTRAINT [DF_StudentSchoolDetail_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[StudentSchoolDetail] ADD  CONSTRAINT [DF_StudentSchoolDetail_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[StudentSchoolDetail] ADD  CONSTRAINT [DF_StudentSchoolDetail_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[StudentSchoolDetail] ADD  CONSTRAINT [DF_StudentSchoolDetail_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[StudentSchoolDetail] ADD  CONSTRAINT [DF_StudentSchoolDetail_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[SubjectMaster] ADD  CONSTRAINT [DF_SubjectMaster_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[SubjectMaster] ADD  CONSTRAINT [DF_SubjectMaster_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[SubjectMaster] ADD  CONSTRAINT [DF_SubjectMaster_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[SubjectMaster] ADD  CONSTRAINT [DF_SubjectMaster_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[SubjectMaster] ADD  CONSTRAINT [DF_SubjectMaster_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[SubjectMaster] ADD  CONSTRAINT [DF_SubjectMaster_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[TeacherDetail] ADD  CONSTRAINT [DF_TeacherDetail_SectionId]  DEFAULT ((0)) FOR [SectionId]
GO
ALTER TABLE [dbo].[TeacherDetail] ADD  CONSTRAINT [DF_TeacherDetail_IsTeacher]  DEFAULT ((0)) FOR [IsHomeTeacher]
GO
ALTER TABLE [dbo].[TeacherDetail] ADD  CONSTRAINT [DF_TeacherDetail_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[TeacherDetail] ADD  CONSTRAINT [DF_TeacherDetail_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[TeacherDetail] ADD  CONSTRAINT [DF_TeacherDetail_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[TeacherDetail] ADD  CONSTRAINT [DF_TeacherDetail_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[TeacherDetail] ADD  CONSTRAINT [DF_TeacherDetail_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[TeacherDetail] ADD  CONSTRAINT [DF_TeacherDetail_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[ValuePointDetail] ADD  CONSTRAINT [DF_ValuePointDetail_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[ValuePointMaster] ADD  CONSTRAINT [DF_ValuePointMaster_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[ValuePointMaster] ADD  CONSTRAINT [DF_ValuePointMaster_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[ValuePointMaster] ADD  CONSTRAINT [DF_ValuePointMaster_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[ValuePointMaster] ADD  CONSTRAINT [DF_ValuePointMaster_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[ValuePointMaster] ADD  CONSTRAINT [DF_ValuePointMaster_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[ValuePointMaster] ADD  CONSTRAINT [DF_ValuePointMaster_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[RoleRight]  WITH CHECK ADD  CONSTRAINT [FK_RoleRight_Link] FOREIGN KEY([LinkId])
REFERENCES [dbo].[Link] ([LinkId])
GO
ALTER TABLE [dbo].[RoleRight] CHECK CONSTRAINT [FK_RoleRight_Link]
GO
/****** Object:  StoredProcedure [dbo].[uspAddNotificationAfterAddValue]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Dhrumil Patel>
-- Create date: <19/11/2018>
-- Description:	<Save Notification after add value point>
-- =============================================

CREATE PROCEDURE [dbo].[uspAddNotificationAfterAddValue]
	@StudentId BIGINT=0,
	@ValuPointDetail NVARCHAR(MAX)=null,
	@WeekNo AS BIGINT=0,
	@SenderId BIGINT=0
AS
BEGIN
	DECLARE @ClassId BIGINT=0;
	DECLARE @SectionId BIGINT=0;
	DECLARE @RoleId BIGINT=0;
	DECLARE @StudentName NVARCHAR(MAX)=null;
	DECLARE @SenderName NVARCHAR(MAX)=null

	SELECT @StudentName=( FirstName +' ' +LastName) FROM StudentMaster WHERE IsActive=1 AND IsDelete=0 AND Id=@StudentId
	SELECT @RoleId=RoleId, @SenderName=( FirstName +' ' +LastName) FROM LoginCredentials WHERE IsActive=1 AND IsDelete=0 AND UserId=@SenderId
	
	SELECT @ClassId=ClassId,@SectionId=SectionId FROM  StudentSchoolDetail  WHERE StudentId=@StudentId AND IsCurrent=1
	
	DECLARE  @Receiver TABLE ( Id BIGINT )
	
	IF(@RoleId=3)
	BEGIN
		INSERT INTO @Receiver
	-- Get All Parent List
		SELECT Distinct SSD.ParentId AS Id FROM GuardianStudentDetail SSD 
		INNER JOIN LoginCredentials LC ON LC.UserId=SSD.ParentId
		WHERE SSD.StudentId=@StudentId
			AND LC.IsActive=1 AND Lc.IsDelete=0 AND LC.UserId!=@SenderId
	END
	ELSE
	BEGIN
		INSERT INTO @Receiver
	-- Get All Parent List
		SELECT Distinct SSD.ParentId AS Id FROM GuardianStudentDetail SSD 
		INNER JOIN LoginCredentials LC ON LC.UserId=SSD.ParentId
		WHERE SSD.StudentId=@StudentId
			AND LC.IsActive=1 AND Lc.IsDelete=0 AND LC.UserId!=@SenderId
		UNION
	-- Get All teacher List
		SELECT DISTINCT LC.UserId AS Id FROM TeacherDetail TD  
		INNER JOIN LoginCredentials LC ON LC.UserId=TD.TeacherId
		WHERE TD.ClassId=@ClassId AND TD.SectionId in (0,@SectionId)
			AND LC.IsActive=1 AND Lc.IsDelete=0 AND LC.UserId!=@SenderId
	END
	--;WITH Receiver  AS 
	--( 
	-- -- Get All Parent List
	--	SELECT Distinct SSD.ParentId AS Id FROM GuardianStudentDetail SSD 
	--	INNER JOIN LoginCredentials LC ON LC.UserId=SSD.ParentId
	--	WHERE SSD.StudentId=@StudentId
	--		AND LC.IsActive=1 AND Lc.IsDelete=0 AND LC.UserId!=@SenderId
	--UNION
	---- Get All teacher List
	--	SELECT DISTINCT LC.UserId AS Id FROM TeacherDetail TD  
	--	INNER JOIN LoginCredentials LC ON LC.UserId=TD.TeacherId
	--	WHERE TD.ClassId=@ClassId AND TD.SectionId in (0,@SectionId)
	--		AND LC.IsActive=1 AND Lc.IsDelete=0 AND LC.UserId!=@SenderId
	--)


	INSERT INTO [dbo].[Notification]
	           ([NotificationTypeId]
	           ,[UserId]
			   ,[StudentId]
	           ,[MessageEn]
	           ,[MessageAr]
			   ,[WeekNumber]
	           ,[IsRead]
	           ,[IsDelete]
	           ,[CreatedBy]
	           ,[CreatedDate])
	SELECT 1 --Notifaction Type for Assign Value
		,Id
		,@StudentId
		,ISNULL(@StudentName,'') + ' Got New Point for '+ ISNULL(@ValuPointDetail,'') +' By '+ ISNULL(@SenderName,'') +'.'
		,ISNULL(@StudentName,'') + ' Got New Point for '+ ISNULL(@ValuPointDetail,'') +' By '+ ISNULL(@SenderName,'') +'.'
		,@WeekNo
		,CAST(0 AS BIT)
		,CAST(0 AS BIT)
		,@SenderId
		,GETDATE()
	FROM @Receiver
		
	--;WITH Receiver  AS 
	--( 
	---- Get All Parent List
	--	SELECT Distinct SSD.ParentId AS Id FROM GuardianStudentDetail SSD 
	--	INNER JOIN LoginCredentials LC ON LC.UserId=SSD.ParentId
	--	WHERE SSD.StudentId=@StudentId
	--		AND LC.IsActive=1 AND Lc.IsDelete=0 AND LC.UserId!=@SenderId
	--UNION
	---- Get All teacher List
	--	SELECT DISTINCT LC.UserId AS Id FROM TeacherDetail TD  
	--	INNER JOIN LoginCredentials LC ON LC.UserId=TD.TeacherId
	--	WHERE TD.ClassId=@ClassId AND TD.SectionId in (0,@SectionId)
	--		AND LC.IsActive=1 AND Lc.IsDelete=0 AND LC.UserId!=@SenderId
	--)

	SELECT DI.DeviceToken
		,(SELECT NotificationType FROM NotificationType WHERE NotificationTypeId = 1) AS [Title]
		,ISNULL(@StudentName,'') + ' Got New Point for '+ ISNULL(@ValuPointDetail,'') +' By '+ ISNULL(@SenderName,'') +'.' AS [Description]
		,CASE WHEN LC.RoleId = 3 THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS IsParent
		,DI.DeviceType
	FROM DeviceInfo DI
	INNER JOIN LoginCredentials LC ON DI.Mobile = LC.Mobile
	INNER JOIN @Receiver R ON R.Id = LC.UserId
END



GO
/****** Object:  StoredProcedure [dbo].[uspBindDashboard]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Sweta Patel>
-- Create date: <26/11/2018>
-- Description:	<Load Dashboard Data>
-- =============================================
-- uspBindDashboard 2
CREATE PROCEDURE [dbo].[uspBindDashboard]
		@SchoolId BIGINT = 0,
		@UserId BIGINT = NULL
AS
BEGIN
	

	--DECLARE @SchoolId BIGINT=2;
--STRAT :// Get Total Count OF Student & Teacher //----------------------------------------------------------------
DECLARE @CurrentYearId BIGINT=0;
DECLARE @CurrentWeekNumber BIGINT=0;

SELECT TOP 1 @CurrentYearId=Id FROM AccademicYear AY Where Ay.IsCurrent=1;
SELECT TOP 1 @CurrentWeekNumber=WeekNumber FROM AccademicYearWeekDetail WHERE AccademicYearId=@CurrentYearId AND  CAST (CONVERT(VARCHAR(8), GETDATE(), 112) AS BIGINT) BETWEEN StartDateInt AND EndDateInt
PRINT 'Current Year Id :' + CAST(@CurrentYearId As Nvarchar(10)) + ' ,  Week Number : ' + CAST(@CurrentWeekNumber As Nvarchar(10))  

DECLARE @TotalStudent BIGINT=0;
DECLARE @TotalGirls BIGINT=0;
DECLARE @TotalBoys BIGINT=0;
DECLARE @TotalTeacher BIGINT=0;

DECLARE  @StudentData TABLE (
StudentId BIGINT,
FullName Nvarchar(MAX),
ProfilePic Nvarchar(MAX),
Gender Nvarchar(10))

INSERT INTO @StudentData
SELECT SM.Id,
ISNULL(SM.FirstName,'') + ' ' + ISNULL(SM.LastName,'') AS FullName,ISNULL(SM.ProfilePic,'') AS ProfilePic,Gender FROM StudentMaster SM INNER JOIN StudentSchoolDetail SSD ON SM.Id=SSD.StudentId
					WHERE SM.IsActive=1 AND SM.IsDelete=0 AND SSD.IsCurrent=1 
							AND SSD.AcademicYearId=@CurrentYearId AND SM.SchoolId=@SchoolId

SELECT @TotalStudent=Count(*) From @StudentData
SELECT @TotalGirls=Count(*) From @StudentData WHERE Gender='F'
SELECT @TotalBoys=Count(*) From @StudentData WHERE Gender='M'

DECLARE  @TeacherParentData TABLE (
UserId BIGINT,
FullName Nvarchar(MAX),
RoleId BIGINT
)

INSERT INTO @TeacherParentData
SELECT LC.UserId,ISNULL(LC.FirstName,'') + ' ' + ISNULL(LC.LastName,'') AS FullName,LC.RoleId FROM LoginCredentials LC Where LC.IsActive=1 AND LC.IsDelete=0 AND SchoolId=@SchoolId  --('3' is for teacher Role)
SELECT @TotalTeacher=Count(*) FROM @TeacherParentData WHERE RoleId=3
---------------------------------------------------------------------------------------------------------------------
SELECT @TotalStudent As TotalStudents , @TotalBoys As TotalMaleStudents 
	,@TotalGirls As TotalFemaleStudents  ,@TotalTeacher As TotalTeachers
	,@CurrentYearId As AcademicYearId, @CurrentWeekNumber As CurrentWeekNumber
	,(@CurrentWeekNumber - 1) AS PreviousWeekNumber

--STRAT :// Get Top 4 House by Point Count //------------------------------------------------------------------------------------------------------------------------

DECLARE  @HouseData TABLE (
HouseId BIGINT,
HouseName Nvarchar(MAX)
)
INSERT INTO @HouseData
SELECT HM.Id,HM.Name FROM HouseMaster HM WHERE HM.IsActive=1 AND HM.IsDelete=0 AND HM.SchoolId=@SchoolId AND HM.AcademicYearId=@CurrentYearId

DECLARE  @HouseGraphicalCount TABLE (
HouseId BIGINT,
HouseName Nvarchar(MAX),
HousePoint BIGINT
)
INSERT INTO @HouseGraphicalCount
SELECT TOP 4  HM.HouseId,HM.HouseName,Count(*) AS HousePoint FROM @HouseData HM INNER JOIN HouseDetail HD ON HM.HouseId=HD.HouseId
INNER JOIN ValuePointDetail VPD ON HD.StudentId=VPD.StudentId 
INNER JOIN StudentMaster SM ON SM.Id=HD.StudentId 
WHERE VPD.AccademidYearID=@CurrentYearId  AND SM.IsActive=1 AND SM.IsDelete=0
GROUP BY HM.HouseId,HM.HouseName
ORDER BY HousePoint DESC

SELECT * FROM @HouseGraphicalCount ORDER BY HousePoint DESC

DECLARE @ScaleUnit BIGINT=0;
DECLARE @ScaleEnd BIGINT=0;
DECLARE	@lastdigit AS BIGINT=0;
SELECT TOP 1 @ScaleEnd=HousePoint FROM @HouseGraphicalCount ORDER BY HousePoint DESC

SELECT @lastdigit=RIGHT(ISNULL(@ScaleEnd,0), 1)
		IF  @ScaleEnd>0 
			BEGIN
				SELECT @ScaleEnd=ISNULL(@ScaleEnd,0) + (10-ISNULL(@lastdigit,0))
		END
		IF  @ScaleEnd=0 
			BEGIN
				SELECT @ScaleEnd=0
		END
SELECT @ScaleUnit=ISNULL((ISNULL(@ScaleEnd,0)/10),0)
--SELECT @ScaleUnit As ScaleUnit,@ScaleEnd AS MaxScale
-------------------------------------------------------------------------------------------------------------
--STRAT :// Get Value Point (Top & Least)  //-------------------------------------------------------
DECLARE  @ValuePointCounts TABLE (
ValuePointId BIGINT,
PointName Nvarchar(MAX),
TotalPoint BIGINT
)

INSERT INTO @ValuePointCounts
SELECT VM.Id AS ValuePointId,VM.Name AS PointName, 0 AS TotalPoint FROM ValuePointMaster VM  
		WHERE VM.IsActive=1 AND VM.IsDelete=0 AND VM.SchoolId=@SchoolId
		AND VM.Id NOT IN (SELECT VPD.ValuePointId FROM ValuePointDetail VPD WHERE VPD.AccademidYearID=@CurrentYearId)
		UNION
		SELECT VPD.ValuePointId,ISNULL(VM.Name,'Other') AS PointName,Count(VPD.ValuePointId) AS TotalPoint FROM ValuePointDetail VPD 
				LEFT JOIN ValuePointMaster VM ON  VPD.ValuePointId= COALESCE(VM.Id,0)
				LEFT JOIN StudentMaster SM ON SM.ID =VPD.StudentId 
		WHERE VPD.AccademidYearID=@CurrentYearId AND SM.SchoolId=@SchoolId 
			AND SM.IsActive=1 AND SM.IsDelete=0 
			AND ISNULL(VM.IsActive,1)=1 AND ISNULL(VM.IsDelete,0)=0 
		GROUP BY  VPD.ValuePointId,ISNULL(VM.Name,'Other')
		ORDER BY TotalPoint DESC


SELECT SUM(TotalPoint) AS MinPointTotal 
	FROM  @ValuePointCounts WHERE ValuePointId IN 
	(SELECT TOP 5 ValuePointId FROM  @ValuePointCounts ORDER BY TotalPoint ASC)
	 SELECT TOP 5 * FROM  @ValuePointCounts  ORDER BY TotalPoint ASC

SELECT SUM(TotalPoint) AS MaxPointTotal 
	FROM @ValuePointCounts WHERE ValuePointId IN 
	(SELECT TOP 5 ValuePointId FROM  @ValuePointCounts WHERE TotalPoint>0  ORDER BY TotalPoint DESC)
	 SELECT TOP 5 * FROM  @ValuePointCounts WHERE TotalPoint>0 ORDER BY TotalPoint DESC

----------------------------------------------------------------------------------------------------------------------------

--SELECT TOP 5 SM.StudentId,SM.FullName,CM.Name AS ClassName, S.Name AS SectionName
--,ISNULL((SELECT TOP 1 HouseName FROM @HouseData WHERE HouseId=HD.HouseId),'house') AS House
--, Count(VPD.ValuePointId) AS TotalPoint
--FROM @StudentData SM
--LEFT JOIN StudentSchoolDetail SSD ON SM.StudentId=SSD.StudentId AND SSD.IsCurrent=1
--LEFT JOIN SectionMaster S ON SSD.SectionId=S.Id
--LEFT JOIN ClassMaster CM ON SSD.ClassId=CM.Id
--LEFT JOIN HouseDetail HD ON HD.StudentId=SM.StudentId
--LEFT JOIN ValuePointDetail VPD ON VPD.StudentId=SM.StudentId
--WHERE VPD.AccademidYearID=@CurrentYearId
--GROUP BY SM.StudentId,SM.FullName,CM.Name,S.Name,HD.HouseId
--ORDER BY TotalPoint DESC

----------------------------------------------------------------------------------------------------------------------------
DECLARE  @StudentPointList TABLE (
StudentId BIGINT,
StudentName Nvarchar(MAX),
ProfilePic Nvarchar(MAX),
ClassName Nvarchar(MAX),
SectionName Nvarchar(MAX),
HouseName Nvarchar(MAX),
TotalPoint BIGINT
)
------------------------------------------------------------------------------------------------------------------
INSERT INTO @StudentPointList
SELECT SM.StudentId,SM.FullName,SM.ProfilePic AS ProfilePic, CM.Name AS ClassName, S.Name AS SectionName
,ISNULL((SELECT TOP 1 Name FROM HouseMaster WHERE Id=HD.HouseId),'house') AS House
, Count(VPD.ValuePointId) AS TotalPoint
FROM @StudentData SM
LEFT JOIN StudentSchoolDetail SSD ON SM.StudentId=SSD.StudentId AND SSD.IsCurrent=1
LEFT JOIN SectionMaster S ON SSD.SectionId=S.Id
LEFT JOIN ClassMaster CM ON SSD.ClassId=CM.Id
LEFT JOIN HouseDetail HD ON HD.StudentId=SM.StudentId
LEFT JOIN ValuePointDetail VPD ON VPD.StudentId=SM.StudentId
WHERE VPD.AccademidYearID=@CurrentYearId AND HD.AccademidYearID=@CurrentYearId
GROUP BY SM.StudentId,SM.FullName,SM.ProfilePic,CM.Name,S.Name,HD.HouseId
UNION 
SELECT DISTINCT SM.StudentId,SM.FullName,SM.ProfilePic AS ProfilePic,CM.Name AS ClassName, S.Name AS SectionName
,ISNULL((SELECT TOP 1 Name FROM HouseMaster WHERE Id=HD.HouseId AND HD.StudentId=SM.StudentId AND HD.AccademidYearID=@CurrentYearId),'house') AS House
, 0 AS TotalPoint
FROM @StudentData SM
LEFT JOIN StudentSchoolDetail SSD ON SM.StudentId=SSD.StudentId AND SSD.IsCurrent=1
LEFT JOIN SectionMaster S ON SSD.SectionId=S.Id
LEFT JOIN ClassMaster CM ON SSD.ClassId=CM.Id
LEFT JOIN HouseDetail HD ON HD.StudentId=SM.StudentId
WHERE SM.StudentId NOT IN (Select VPD.StudentId FROM ValuePointDetail VPD WHERE VPD.AccademidYearID=@CurrentYearId)
		AND  HD.AccademidYearID=@CurrentYearId
ORDER BY TotalPoint ASC

SELECT TOP 5 * FROM @StudentPointList WHERE TotalPoint>0 ORDER BY TotalPoint DESC
SELECT TOP 5 * FROM @StudentPointList  ORDER BY TotalPoint ASC

-------------------------------------------------------------------------------------------------------------------
DECLARE  @TeacherPointList TABLE (
TeacherId BIGINT,
TeacherName Nvarchar(MAX),
ProfilePic Nvarchar(MAX),
TotalPoint BIGINT
)

INSERT INTO @TeacherPointList
SELECT 
LC.UserId,ISNULL(LC.FirstName,'') + ' ' + ISNULL(LC.LastName,'') AS FullName,
LC.ProfilePic AS ProfilePic
 ,0 AS TotalPoint
FROM LoginCredentials LC  
WHERE LC.IsActive=1 AND LC.IsDelete=0 AND LC.RoleId=3 AND LC.SchoolId=@SchoolId
AND LC.UserId NOT IN (SELECT VPD.AddedBy FROM ValuePointDetail VPD WHERE VPD.AccademidYearID=@CurrentYearId)
UNION 
SELECT 
LC.UserId
,ISNULL(LC.FirstName,'') + ' ' + ISNULL(LC.LastName,'') AS FullName
,LC.ProfilePic AS ProfilePic
, Count(VPD.AddedBy) AS TotalPoint
FROM LoginCredentials LC LEFT JOIN ValuePointDetail VPD ON VPD.AddedBy=LC.UserId
WHERE LC.IsActive=1 AND LC.IsDelete=0 AND LC.RoleId=3 
		AND LC.SchoolId=@SchoolId AND VPD.AccademidYearID=@CurrentYearId
GROUP BY LC.UserId,LC.FirstName,LC.LastName,LC.ProfilePic
ORDER BY TotalPoint DESC


SELECT TOP 5 * FROM @TeacherPointList WHERE TotalPoint>0 ORDER BY TotalPoint DESC
SELECT TOP 5 * FROM @TeacherPointList  ORDER BY TotalPoint ASC
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------

--SELECT DISTINCT 
--GMD.MessageTitle AS Title
--,'Message' As Type
--,LC.FirstName + ' ' + LC.LastName + (CASE WHEN GMD.GroupId >0  THEN ' ( ' +ISNULL(GM.Name,'') + ' )' ELSE ' ' END) AS Receiver
--,LC1.FirstName + ' ' + LC1.LastName AS Sender
--,GMD.CreatedDate AS CreatedDate
--FROM GroupMessageDetail GMD 
--LEFT JOIN GroupMaster GM ON GM.Id=(CASE WHEN GMD.GroupId >0  THEN GMD.GroupId ELSE GM.Id END)
--LEFT JOIN LoginCredentials LC ON  GMD.UserId=LC.UserId
--LEFT JOIN LoginCredentials LC1 ON  GMD.SenderId=LC1.UserId
--WHERE GMD.CreatedDateInt=CAST (CONVERT(VARCHAR(8), GETDATE(), 112) AS BIGINT)

SELECT DISTINCT 
GME.MessageTitle AS Title
,'Message' As Type
,LC.FirstName + ' ' + LC.LastName + (CASE WHEN GMD.GroupId >0  THEN ' ( ' +ISNULL(GM.Name,'') + ' )' ELSE ' ' END) AS Receiver
,LC1.FirstName + ' ' + LC1.LastName AS Sender
,GME.CreatedDate AS CreatedDate
FROM GroupMessage GME 
LEFT JOIN GroupMessageDetail GMD ON GME.GroupMessageId=GMD.GroupMessageId
LEFT JOIN GroupMaster GM ON GM.Id=(CASE WHEN GMD.GroupId >0  THEN GMD.GroupId ELSE GM.Id END)
LEFT JOIN LoginCredentials LC ON  GMD.UserId=LC.UserId
LEFT JOIN LoginCredentials LC1 ON  GMD.SenderId=LC1.UserId
WHERE GME.CreatedDateInt=CAST (CONVERT(VARCHAR(8), GETDATE(), 112) AS BIGINT)

 AND LC.SchoolId=@SchoolId AND LC1.SchoolId=@SchoolId AND GM.SchoolId=@SchoolId 
UNION ALL
SELECT 
(SELECT TOP 1 NT.NotificationType FROM NotificationType NT Where NT.NotificationTypeId=N.NotificationTypeId) As Title,
 'Notification' As Type
 ,LC.FirstName + ' ' + LC.LastName AS Receiver
 ,LC1.FirstName + ' ' + LC1.LastName AS Sender
 ,N.CreatedDate AS CreatedDate
 FROM Notification N
 LEFT JOIN LoginCredentials LC ON  N.UserId=LC.UserId
 LEFT JOIN LoginCredentials LC1 ON  N.CreatedBy=LC1.UserId
 WHERE N.CreatedDateInt=CAST (CONVERT(VARCHAR(8), GETDATE(), 112) AS BIGINT)
 AND LC.SchoolId=@SchoolId AND LC1.SchoolId=@SchoolId AND N.IsDelete=0
 ORDER BY  CreatedDate DESC



END



GO
/****** Object:  StoredProcedure [dbo].[uspGetAddValueStudentList]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			<Dhrumil Patel> 
-- Created date:	<30-10-2018>
-- Description:		<Get School Teacher List>
-- =============================================
-- uspGetAddValueStudentList null,null,47,''
CREATE PROCEDURE [dbo].[uspGetAddValueStudentList]
	@ClassId AS BIGINT=null,
	@SectionId AS BIGINT=null,
	@TeacherId AS BIGINT=0,
	@SearchCharacter AS NVARCHAR(1)=''
AS
BEGIN
	SELECT DISTINCT
		SM.Id
		,SM.FirstName + ' ' + SM.LastName AS [Name]
		,SM.ProfilePic
		,(SELECT Count(*) FROM ValuePointDetail VPD WHERE VPD.AccademidYearID=SSD.AcademicYearId AND VPD.StudentId=SM.Id) AS TotalPoints
	FROM StudentMaster SM 
	INNER JOIN StudentSchoolDetail  SSD ON SM.Id=SSD.StudentId
	INNER JOIN TeacherDetail TD ON TD.ClassId=SSD.ClassId
	WHERE TD.TeacherId=@TeacherId AND SSD.IsCurrent = 1
	AND SSD.SectionId=COAlESCE((CASE WHEN TD.SectionId !=0 THEN TD.SectionId ELSE SSD.SectionId END),SSD.SectionId)
	AND SSD.ClassId=COAlESCE((CASE WHEN @ClassId != 0 THEN @ClassId ELSE SSD.ClassId END),SSD.ClassId)
	AND SSD.SectionId=COAlESCE((CASE WHEN @SectionId != 0 THEN @SectionId ELSE SSD.SectionId END),SSD.SectionId)
	AND SM.FirstName LIKE COAlESCE(@SearchCharacter+'%',SM.FirstName)
END




GO
/****** Object:  StoredProcedure [dbo].[uspGetAllMessageList]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Sweat PAtel>
-- Create date: <22/11/2018>
-- Description:	<Get All Message >
-- uspGetAllMessageList 2
CREATE PROCEDURE [dbo].[uspGetAllMessageList]
	@SchoolId BIGINT = 0,
	@UserId BIGINT =NUll -- Pass only when only received notification required to show
AS
BEGIN
	DECLARE @TotalCount BIGINT = 0, @WeekCount BIGINT = 0, @MonthCount BIGINT = 0
	DECLARE @CurrDateInt BIGINT = YEAR(GETDATE()) * 10000 + MONTH(GETDATE()) * 100 + DAY(GETDATE())
	DECLARE @CurrWeek BIGINT = 0
	
	SET @CurrWeek = (SELECT WeekNumber
					FROM AccademicYearWeekDetail
					WHERE StartDateInt <= @CurrDateInt
						AND EndDateInt >= @CurrDateInt)
	
	SET @TotalCount = (SELECT COUNT(*) FROM GroupMessage GM
					  INNER JOIN LoginCredentials LC ON GM.SenderId = LC.UserId	
					  WHERE LC.SchoolId=@SchoolId)

	SET @WeekCount = (SELECT COUNT(*) FROM GroupMessage GM
					  INNER JOIN LoginCredentials LC ON GM.SenderId = LC.UserId	
					  WHERE  GM.WeekNumber = @CurrWeek AND LC.SchoolId = @SchoolId)

	SET @MonthCount = (SELECT COUNT(*) FROM GroupMessage GM
					  INNER JOIN LoginCredentials LC ON GM.SenderId = LC.UserId	
					   WHERE GM.SenderId >0 
						AND DATEPART(m, GM.CreatedDate) = DATEPART(m, DATEADD(m, -1, getdate()))
						AND DATEPART(yyyy, GM.CreatedDate) = DATEPART(yyyy, DATEADD(m, -1, getdate()))
						AND LC.SchoolId = @SchoolId)

	SELECT @TotalCount AS TotalCount, @WeekCount As WeekCount ,@MonthCount AS MonthCount

	SELECT DISTINCT GMD.Id AS MessageId
		,GME.MessageTitle
		,(ISNULL(LC1.FirstName,'sender') + ' ' + LC1.LastName + ' (' + GM.[Name] + ')') AS SendTo
		,ISNULL(LC.FirstName,'sender') + ' ' + LC.LastName AS SentBy
		,CASE WHEN convert(NVARCHAR,GME.CreatedDate,103) = CONVERT(NVARCHAR,GETDATE(),103) THEN CONVERT(varchar(15),CAST(GME.CreatedDate AS TIME),100) ELSE CONCAT(CONVERT(VARCHAR, GME.CreatedDate, 103) ,' at ' , CONVERT(varchar(15),CAST(GME.CreatedDate AS TIME),100)) END AS DisplayDate
		,GMD.UserId
		,GME.CreatedDate
	FROM GroupMessageDetail GMD 
	INNER JOIN GroupMessage GME ON GME.GroupMessageId=GMD.GroupMessageId
	LEFT JOIN LoginCredentials LC ON LC.UserId=GME.SenderId
	LEFT JOIN GroupMaster GM ON GMD.GroupId=GM.ID
	LEFT JOIN LoginCredentials LC1 ON LC1.UserId=GMD.UserId
	WHERE GM.IsActive=1 AND GM.IsDelete=0 AND GMD.GroupId>0 AND GME.SenderId>0
		AND LC.SchoolId=2 AND GM.SchoolId=2
		AND GMD.UserId=COALESCE(NULL,GMD.UserId)
	
	UNION 
	
	SELECT GMD.Id AS MessageId
		,GME.MessageTitle
		,ISNULL(LC1.FirstName,'sender') + ' ' + LC1.LastName AS SentTo
		,ISNULL(LC.FirstName,'sender') + ' ' + LC.LastName AS SentBy
		,CASE WHEN convert(NVARCHAR,GME.CreatedDate,103) = CONVERT(NVARCHAR,GETDATE(),103) THEN CONVERT(varchar(15),CAST(GME.CreatedDate AS TIME),100) ELSE CONCAT(CONVERT(VARCHAR, GME.CreatedDate, 103) ,' at ' , CONVERT(varchar(15),CAST(GME.CreatedDate AS TIME),100)) END AS DisplayDate
		,GMD.UserId
		,GME.CreatedDate
	FROM GroupMessageDetail GMD 
	INNER JOIN GroupMessage GME ON GME.GroupMessageId=GMD.GroupMessageId
	LEFT JOIN LoginCredentials LC ON LC.UserId=GMD.SenderId
	LEFT JOIN LoginCredentials LC1 ON LC1.UserId=GMD.UserId
	WHERE GMD.GroupId=0 AND GMD.SenderId>0 AND GMD.UserId>0
		AND LC.SchoolId=@SchoolId AND LC1.SchoolId=@SchoolId
		AND GMD.UserId=COALESCE(@UserId,GMD.UserId)
	ORDER BY GME.CreatedDate DESC

END






GO
/****** Object:  StoredProcedure [dbo].[uspGetAllNotificationList]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<SWeta>
-- Create date: <22/11/2018>
-- Description:	<Get All Notification>
-- uspGetAllNotificationList 2
CREATE PROCEDURE [dbo].[uspGetAllNotificationList]
	@SchoolId BIGINT = 0,
	@UserId BIGINT =NUll -- PAss only when only received notification required to show
AS
BEGIN	
---Note : Please Correct the logic for count and SP its not 100% Accurate based on All school ,date,code 
	DECLARE @TotalCount BIGINT = 0, @WeekCount BIGINT = 0, @MonthCount BIGINT = 0
	DECLARE @CurrDateInt BIGINT = YEAR(GETDATE()) * 10000 + MONTH(GETDATE()) * 100 + DAY(GETDATE())
	DECLARE @CurrWeek BIGINT = 0	
	SET @CurrWeek = (SELECT WeekNumber
					FROM AccademicYearWeekDetail
					WHERE StartDateInt <= @CurrDateInt
						AND EndDateInt >= @CurrDateInt)
	
	SELECT @TotalCount=Count(*) FROM Notification N INNER JOIN LoginCredentials LC ON N.CreatedBy=LC.UserId
	WHERE N.CreatedBy >0 AND N.UserId>0 AND LC.SchoolId=@SchoolId

	SELECT @WeekCount=Count(*) FROM Notification N INNER JOIN LoginCredentials LC ON N.CreatedBy=LC.UserId
	WHERE N.CreatedBy >0 AND N.UserId>0 AND LC.SchoolId=@SchoolId
		AND WeekNumber=@CurrWeek

	SELECT @MonthCount=Count(*) FROM Notification N INNER JOIN LoginCredentials LC ON N.CreatedBy=LC.UserId
	WHERE N.CreatedBy >0 AND N.UserId>0 AND LC.SchoolId=@SchoolId
		AND DATEPART(m, N.CreatedDate) = DATEPART(m, DATEADD(m, -1, getdate()))
		AND DATEPART(yyyy, N.CreatedDate) = DATEPART(yyyy, DATEADD(m, -1, getdate()))

SELECT @TotalCount AS TotalCount, @WeekCount AS WeekCount , @MonthCount AS MonthCount

SELECT
NotificationId,
NT.NotificationType AS NotificationTitle,
ISNULL(LC.FirstName,'sender') + ' ' +LC.LastName AS SentBy,
LC1.FirstName + ' ' +LC1.LastName AS SentTo,
CASE WHEN convert(NVARCHAR,N.CreatedDate,103) = CONVERT(NVARCHAR,GETDATE(),103) THEN CONVERT(varchar(15),CAST(N.CreatedDate AS TIME),100) ELSE CONCAT(CONVERT(VARCHAR, N.CreatedDate, 103) ,' at ' , CONVERT(varchar(15),CAST(N.CreatedDate AS TIME),100)) END AS DisplayDate
FROM Notification N
INNER JOIN NotificationType NT ON N.NotificationTypeId=NT.NotificationTypeId
LEFT JOIN LoginCredentials LC ON LC.UserId=N.CreatedBy
LEFT JOIN LoginCredentials LC1 ON LC1.UserId=N.UserId
WHERE N.CreatedBy >0 AND N.UserId>0 AND LC.SchoolId=@SchoolId AND LC1.SchoolId=@SchoolId
AND N.UserId=COALESCE(@UserId,N.UserId)
ORDER BY N.CreatedDate DESC

END






GO
/****** Object:  StoredProcedure [dbo].[uspGetAllValuePointList]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			<Krupali Prajapati> 
-- Created date:	<1-10-2018>
-- Description:		<Get Student List who have assigned valuepoint by teacher>
-- =============================================
-- uspGetAllValuePointList 2,45
CREATE PROCEDURE [dbo].[uspGetAllValuePointList]
	@SchoolId BIGINT = 0,
	@UserId BIGINT = 0
AS
BEGIN

SELECT
	Count(*	) AS TotalCount
	FROM ValuePointDetail VPD 
	INNER JOIN LoginCredentials LC ON LC.UserId = VPD.AddedBy
	INNER JOIN StudentSchoolDetail SSD ON SSD.StudentId = VPD.StudentId
	JOIN StudentMaster SM ON SM.Id = VPD.StudentId
	LEFT JOIN ValuePointMaster VM ON VM.Id = VPD.ValuePointId
	JOIN ClassMaster CM on cm.Id = SSD.ClassId
	JOIN SectionMaster SecM ON SecM.Id = SSD.SectionId 
	WHERE VPD.AddedBy = @UserId AND LC.SchoolId = @SchoolId AND SSD.IsCurrent = 1 
-- for List value Point
	SELECT
	SM.ProfilePic
	,CONCAT(SM.FirstName ,' ', ISNULL(SM.MiddleName,'') ,' ', SM.LastName) AS FullName
	,CM.Name AS Class
	,SecM.Name AS Grade
	,ISNULL((SELECT  VM.Name FROM ValuePointMaster VM WHERE VM.ID=VPD.ValuePointId),'Other - ' + ISNULL(VPD.Note,'no comments')) AS [Value]
	,CASE WHEN VPD.CreatedDate = GETDATE() THEN CONVERT(varchar(15),CAST(VPD.CreatedDate AS TIME),100) ELSE CONCAT(CONVERT(VARCHAR, VPD.CreatedDate, 103) ,' at ' , CONVERT(varchar(15),CAST(VPD.CreatedDate AS TIME),100)) END AS [Date]
	,VPD.WeekNumber AS [Week]
	,VPD.Id
	
	FROM ValuePointDetail VPD 
	INNER JOIN LoginCredentials LC ON LC.UserId = VPD.AddedBy
	INNER JOIN StudentSchoolDetail SSD ON SSD.StudentId = VPD.StudentId
	JOIN StudentMaster SM ON SM.Id = VPD.StudentId
	LEFT JOIN ValuePointMaster VM ON VM.Id = VPD.ValuePointId
	JOIN ClassMaster CM on cm.Id = SSD.ClassId
	JOIN SectionMaster SecM ON SecM.Id = SSD.SectionId 
	WHERE VPD.AddedBy = @UserId AND LC.SchoolId = @SchoolId AND SSD.IsCurrent = 1 
	
	---for total count
	SELECT
	-- TOP(4)
	COUNT(*) AS Total
	,VPD.ValuePointId
	,CASE WHEN VPD.ValuePointId = 0 THEN 'Other' ELSE VM.Name END AS [Value]
	FROM ValuePointDetail VPD 
	INNER JOIN LoginCredentials LC ON LC.UserId = VPD.AddedBy
	INNER JOIN StudentSchoolDetail SSD ON SSD.StudentId = VPD.StudentId
	JOIN StudentMaster SM ON SM.Id = VPD.StudentId
	LEFT JOIN ValuePointMaster VM ON VM.Id = VPD.ValuePointId
	JOIN ClassMaster CM on cm.Id = SSD.ClassId
	JOIN SectionMaster SecM ON SecM.Id = SSD.SectionId 
	WHERE VPD.AddedBy = @UserId AND LC.SchoolId = @SchoolId AND SSD.IsCurrent = 1 
	GROUP BY VPD.ValuePointId, VM.Name 
	ORDER BY count(*) DESC

	


END




GO
/****** Object:  StoredProcedure [dbo].[uspGetAllValuePointListByTeacher]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			<Krupali Prajapati> 
-- Created date:	<31-10-2018>
-- Description:		<Get Student List who have assigned valuepoint by teacher>
-- =============================================
-- uspGetAllValuePointListByTeacher 48,NULL,2
CREATE PROCEDURE [dbo].[uspGetAllValuePointListByTeacher]
	@UserId BIGINT = 0,
	@RoleId BIGINT = NULL,
	@SchoolId BIGINT = NULL
	--@ClassId AS BIGINT=null,
	--@SectionId AS BIGINT=null,
	--@SearchCharacter AS NVARCHAR(1)
AS
BEGIN
-- For TotalCount
DECLARE @StudentList TABLE(
    StudentId BIGINT,	  
	StudentName NVARCHAR(MAX),
	ProfilePic NVARCHAR(MAX),
	 ClassId BIGINT,
	   SectionId BIGINT
	)             
INSERT INTO @StudentList 
	SELECT DISTINCT
		SM.Id
		,CONCAT(SM.FirstName ,' ', ISNULL(SM.MiddleName,'') ,' ', SM.LastName) AS FullName
		,SM.ProfilePic
		,SSD.ClassId
		,SSD.SectionId
	FROM StudentMaster SM 
	INNER JOIN StudentSchoolDetail  SSD ON SM.Id=SSD.StudentId AND SSD.IsCurrent=1
	INNER JOIN TeacherDetail TD ON TD.ClassId=SSD.ClassId
	
	WHERE TD.TeacherId=CASE WHEN ISNULL(@RoleId,0) = 0 THEN  @UserId ELSE TD.TeacherId END
	AND SM.SchoolId = COAlESCE(@SchoolId,SM.SchoolId)
	AND SSD.SectionId=COAlESCE((CASE WHEN TD.SectionId !=0 THEN TD.SectionId ELSE SSD.SectionId END),SSD.SectionId)
	--AND SSD.ClassId=COAlESCE((CASE WHEN @ClassId != 0 THEN @ClassId ELSE SSD.ClassId END),SSD.ClassId)
	--AND SSD.SectionId=COAlESCE((CASE WHEN @SectionId != 0 THEN @SectionId ELSE SSD.SectionId END),SSD.SectionId)
	--AND SM.FirstName LIKE COAlESCE(@SearchCharacter+'%',SM.FirstName)
	
--total count
SELECT
	COUNT(*) as TotalCount
	FROM ValuePointDetail VPD 
	INNER JOIN @StudentList ST ON VPD.StudentId=ST.StudentId
	INNER JOIN ClassMaster CM ON CM.Id=ST.ClassId
	INNER JOIN SectionMaster SM ON SM.Id=ST.SectionId
	LEFT JOIN ValuePointMaster VP ON VP.Id=VPD.ValuePointId
	INNER JOIN LoginCredentials LC ON LC.UserId=VPD.AddedBy

-- for List value Point
	SELECT DISTINCT
	ST.ProfilePic,
	ST.StudentName,
	CM.Name As Class,
	SM.Name As Section
	--ISNULL(VP.Name,'Other') As [PointName]
	,ISNULL((SELECT  VM.Name FROM ValuePointMaster VM WHERE VM.ID=VPD.ValuePointId),'Other - ' + ISNULL(VPD.Note,'no comments')) AS [PointName]
	,CONCAT(LC.FirstName ,' ', LC.LastName) AS AddedByName,
	CASE WHEN convert(NVARCHAR,VPD.CreatedDate,103) = CONVERT(NVARCHAR,GETDATE(),103) THEN CONVERT(varchar(15),CAST(VPD.CreatedDate AS TIME),100) ELSE CONCAT(CONVERT(VARCHAR, VPD.CreatedDate, 103) ,' at ' , CONVERT(varchar(15),CAST(VPD.CreatedDate AS TIME),100)) END AS DisplayDate
	,VPD.WeekNumber
	,VPD.Id
	,St.StudentId
	,VPD.CreatedDate
	FROM ValuePointDetail VPD 
	INNER JOIN @StudentList ST ON VPD.StudentId=ST.StudentId
	INNER JOIN ClassMaster CM ON CM.Id=ST.ClassId
	INNER JOIN SectionMaster SM ON SM.Id=ST.SectionId
	LEFT JOIN ValuePointMaster VP ON VP.Id=VPD.ValuePointId
	INNER JOIN LoginCredentials LC ON LC.UserId=VPD.AddedBy
	ORDER BY VPD.CreatedDate DESC

-- total value count group by
SELECT 
  --TOP(4)
	COUNT(*) AS Total
	,VPD.ValuePointId
	,CASE WHEN VPD.ValuePointId = 0 THEN 'Other' ELSE vp.Name END AS PointName
	FROM ValuePointDetail VPD 
	INNER JOIN @StudentList ST ON VPD.StudentId=ST.StudentId
	INNER JOIN ClassMaster CM ON CM.Id=ST.ClassId
	INNER JOIN SectionMaster SM ON SM.Id=ST.SectionId
	LEFT JOIN ValuePointMaster VP ON VP.Id=VPD.ValuePointId
	INNER JOIN LoginCredentials LC ON LC.UserId=VPD.AddedBy
	GROUP BY VPD.ValuePointId, VP.Name 
	ORDER BY count(*) DESC
END




GO
/****** Object:  StoredProcedure [dbo].[uspGetChildrenDetail]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Dhrumil & Ankit>
-- Create date: <22/10/2018>
-- Description:	<Get Student and Value Point detail by Id>
-- =============================================
--uspGetChildrenDetail 1
CREATE PROCEDURE [dbo].[uspGetChildrenDetail]
	@UserId BIGINT = 0
AS
BEGIN
	SELECT 
		SM.Id AS ChildId
		,SM.FirstName + ' ' + SM.MiddleName + ' ' + SM.LastName AS ChildName
		,(Case when (SM.ProfilePic  != null or SM.ProfilePic != '') then  'http://ws-srv-net.in.webmyne.com/Applications/SchoolManagement/Uploads/ProfilePic/StudentLogo/'+ CAST(SM.Id AS Nvarchar(MAX)) +'/'+SM.ProfilePic else '' end) AS ChildImage
		,S.Name AS SchoolName
		,S.Id AS SchoolId
		,(CM.Name + ' - ' + SECM.Name) AS [Standard]
		,HM.Name AS HomeName
		,SM.IsActive 
		,(SELECT Count(*) FROM ValuePointDetail VPD LEFT JOIN AccademicYear AY ON VPD.AccademidYearID=AY.Id WHERE AY.IsCurrent=1 AND VPD.StudentId=SM.ID) AS AllPoints
		,(SELECT Count(*) FROM ValuePointDetail VPD LEFT JOIN AccademicYear AY ON VPD.AccademidYearID=AY.Id WHERE AY.IsCurrent=1 AND VPD.StudentId=SM.ID AND VPD.WeekNumber= 
   (SELECT WeekNumber FROM AccademicYearWeekDetail WHERE  CAST(CONVERT(VARCHAR(10),GETDATE(), 112) AS BIGINT ) BETWEEN StartDateInt AND EndDateInt ) 
   ) AS WeekPoints
	FROM StudentMaster SM 
	LEFT JOIN School S On S.Id = SM.SchoolId
	LEFT JOIN StudentSchoolDetail SSD ON SSD.StudentId = SM.Id
	LEFT JOIN ClassMaster CM ON CM.Id = SSD.ClassId
	LEFT JOIN SectionMaster SECM ON SECM.Id = SSD.Id
	LEFT JOIN HouseDetail HD ON HD.StudentId = SSD.StudentId
	LEFT JOIN HouseMaster HM ON HM.Id = HD.HouseId
	where SM.Id = @UserId 

	SELECT
	ISNULL((SELECT  VM.Name FROM ValuePointMaster VM WHERE VM.ID=VPD.ValuePointId),'Other - ' + ISNULL(VPD.Note,'no comments')) AS PointName
	,CONVERT(VARCHAR, vpd.CreatedDate ,103) as [Date]
	,CAST(FORMAT(vpd.CreatedDate, 'yyyyMMdd') as int) as DateInt,
	 Format(vpd.CreatedDate,'hh:mm tt') AS [Time],
	(lc.FirstName + ' ' + lc.LastName + ISNULL(' - ' +
		(SELECT sm.[Name] 
		FROM TeacherDetail td
		INNER JOIN SubjectMaster sm On sm.Id=td.SubjectId
		where TeacherId = VPD.AddedBy and IsHomeTeacher = 1)
	,'')) AS FromWho
	FROM StudentMaster SM
	JOIN ValuePointDetail VPD ON VPD.StudentId = SM.ID
    LEFT JOIN AccademicYear AY ON VPD.AccademidYearID=AY.Id
	JOIN LoginCredentials LC ON LC.UserId = VPD.AddedBy
    JOIN StudentSchoolDetail SSD ON SM.Id = SSD.StudentId AND SSD.AcademicYearId = VPD.AccademidYearID
	WHERE SM.Id = @UserId AND AY.IsCurrent=1
	ORDER BY  VPD.CreatedDate DESC
END




GO
/****** Object:  StoredProcedure [dbo].[uspGetDashboardHouseList]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			<Krupali Prajapati> 
-- Created date:	<26-11-2018>
-- Description:		<Get Top 4 House List by Max Value Point>
-- =============================================
--uspGetDashboardHouseList 3,2
CREATE PROCEDURE [dbo].[uspGetDashboardHouseList]
@AcademicYearId	AS BIGINT = 0,
@SchoolId AS BIGINT = 0,
@WeekNumber AS BIGINT = Null
AS
BEGIN
SELECT TOP 1 @AcademicYearId=Id FROM AccademicYear AY Where Ay.IsCurrent=1;

SELECT TOP 4 
hd.HouseId ,
hm.Name AS HouseName, 
Count(ValuePointId) AS HousePoint 
FROM ValuePointDetail vpd
JOIN HouseDetail HD ON vpd.StudentId = HD.StudentId AND vpd.AccademidYearID=HD.AccademidYearID
JOIN HouseMaster HM ON HD.HouseId = HM.Id
JOIN StudentMaster SM ON vpd.StudentId = SM.Id
WHERE vpd.AccademidYearID = @AcademicYearId 
AND HM.SchoolId = @SchoolId
AND VPD.WeekNumber = COAlESCE(@WeekNumber,VPD.WeekNumber)
AND HM.IsActive = 1 AND hm.IsDelete =0 
AND SM.IsActive =1 AND SM.IsDelete = 0
GROUP BY HD.HouseId,hm.Name
ORDER BY Count(ValuePointId) DESC
END




GO
/****** Object:  StoredProcedure [dbo].[uspGetDashboardLeastActiveTeachers]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			<Krupali Prajapati> 
-- Created date:	<26-11-2018>
-- Description:		<Get Top 5 Value Points>
-- =============================================
-- uspGetDashboardLeastActiveTeachers 3,10005
CREATE PROCEDURE [dbo].[uspGetDashboardLeastActiveTeachers]
@AcademicYearId	AS BIGINT = 0,
@SchoolId AS BIGINT = 0,
@WeekNumber AS BIGINT = Null
AS
BEGIN
SELECT TOP 1 @AcademicYearId=Id FROM AccademicYear AY Where Ay.IsCurrent=1;

-------------------------------------------------------------------------------------------------------------------
DECLARE  @TeacherPointList TABLE (
TeacherId BIGINT,
TeacherName Nvarchar(MAX),
ProfilePic Nvarchar(MAX),
TotalPoint BIGINT
)

INSERT INTO @TeacherPointList
SELECT 
LC.UserId,ISNULL(LC.FirstName,'') + ' ' + ISNULL(LC.LastName,'') AS FullName,
LC.ProfilePic AS ProfilePic
 ,0 AS TotalPoint
FROM LoginCredentials LC  
WHERE LC.IsActive=1 AND LC.IsDelete=0 AND LC.RoleId=3 AND LC.SchoolId=@SchoolId
AND LC.UserId NOT IN (SELECT VPD.AddedBy FROM ValuePointDetail VPD WHERE VPD.AccademidYearID=@AcademicYearId AND  VPD.WeekNumber = COAlESCE(@WeekNumber,VPD.WeekNumber))
UNION 
SELECT 
LC.UserId
,ISNULL(LC.FirstName,'') + ' ' + ISNULL(LC.LastName,'') AS FullName
,LC.ProfilePic AS ProfilePic
, Count(VPD.AddedBy) AS TotalPoint
FROM LoginCredentials LC LEFT JOIN ValuePointDetail VPD ON VPD.AddedBy=LC.UserId
WHERE LC.IsActive=1 AND LC.IsDelete=0 AND LC.RoleId=3 
		AND LC.SchoolId=@SchoolId AND VPD.AccademidYearID=@AcademicYearId
		AND VPD.WeekNumber = COAlESCE(@WeekNumber,VPD.WeekNumber)
GROUP BY LC.UserId,LC.FirstName,LC.LastName,LC.ProfilePic
ORDER BY TotalPoint DESC

SELECT TOP 5 * FROM @TeacherPointList  ORDER BY TotalPoint ASC


END




GO
/****** Object:  StoredProcedure [dbo].[uspGetDashboardLeastStudentList]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			<Krupali Prajapati> 
-- Created date:	<26-11-2018>
-- Description:		<Get Top 5 Student List>
-- =============================================
--uspGetDashboardLeastStudentList 3,2,23
CREATE PROCEDURE [dbo].[uspGetDashboardLeastStudentList]
@AcademicYearId	AS BIGINT = 0,
@SchoolId AS BIGINT = 0,
@WeekNumber AS BIGINT = Null,
@classId AS BIGINT = Null,
@HouseId AS BIGINT = Null
AS
BEGIN
SELECT TOP 1 @AcademicYearId=Id FROM AccademicYear AY Where Ay.IsCurrent=1;

-----------------------------------------------------------------------------------------------------------
DECLARE  @StudentData TABLE (
StudentId BIGINT,
FullName Nvarchar(MAX),
ProfilePic Nvarchar(MAX),
Gender Nvarchar(10))

INSERT INTO @StudentData
SELECT SM.Id,
ISNULL(SM.FirstName,'') + ' ' + ISNULL(SM.LastName,'') AS FullName,ISNULL(SM.ProfilePic,'') AS ProfilePic,Gender FROM StudentMaster SM INNER JOIN StudentSchoolDetail SSD ON SM.Id=SSD.StudentId
					WHERE SM.IsActive=1 AND SM.IsDelete=0 AND SSD.IsCurrent=1 
							AND SSD.AcademicYearId=@AcademicYearId AND SM.SchoolId=@SchoolId
--------------------------------------------------------------------------------------------------------------
DECLARE  @HouseData TABLE (
HouseId BIGINT,
HouseName Nvarchar(MAX)
)
INSERT INTO @HouseData
SELECT HM.Id,HM.Name FROM HouseMaster HM WHERE HM.IsActive=1 AND HM.IsDelete=0 AND HM.SchoolId=@SchoolId AND HM.AcademicYearId=@AcademicYearId
----------------------------------------------------------------------------------------------------------------------------
DECLARE  @StudentPointList TABLE (
StudentId BIGINT,
StudentName Nvarchar(MAX),
ProfilePic Nvarchar(MAX),
ClassName Nvarchar(MAX),
SectionName Nvarchar(MAX),
HouseName Nvarchar(MAX),
TotalPoint BIGINT
)
------------------------------------------------------------------------------------------------------------------
INSERT INTO @StudentPointList
SELECT SM.StudentId,SM.FullName,SM.ProfilePic AS ProfilePic, CM.Name AS ClassName, S.Name AS SectionName
,ISNULL((SELECT TOP 1 HouseName FROM @HouseData WHERE HouseId=HD.HouseId),'house') AS House
, Count(VPD.ValuePointId) AS TotalPoint
FROM @StudentData SM
LEFT JOIN StudentSchoolDetail SSD ON SM.StudentId=SSD.StudentId AND SSD.IsCurrent=1
LEFT JOIN SectionMaster S ON SSD.SectionId=S.Id
LEFT JOIN ClassMaster CM ON SSD.ClassId=CM.Id
LEFT JOIN HouseDetail HD ON HD.StudentId=SM.StudentId
LEFT JOIN ValuePointDetail VPD ON VPD.StudentId=SM.StudentId
WHERE VPD.AccademidYearID=@AcademicYearId AND HD.AccademidYearID=@AcademicYearId
AND SSD.ClassId = COAlESCE(@classId,SSD.ClassId)
AND HD.HouseId = COAlESCE(@HouseId,HD.HouseId)
AND VPD.WeekNumber = COAlESCE(@WeekNumber,VPD.WeekNumber)
GROUP BY SM.StudentId,SM.FullName,SM.ProfilePic,CM.Name,S.Name,HD.HouseId
UNION 
SELECT DISTINCT SM.StudentId,SM.FullName,SM.ProfilePic AS ProfilePic,CM.Name AS ClassName, S.Name AS SectionName
,ISNULL((SELECT TOP 1 HouseName FROM @HouseData WHERE HouseId=HD.HouseId),'house') AS House
, 0 AS TotalPoint
FROM @StudentData SM
LEFT JOIN StudentSchoolDetail SSD ON SM.StudentId=SSD.StudentId AND SSD.IsCurrent=1
LEFT JOIN SectionMaster S ON SSD.SectionId=S.Id
LEFT JOIN ClassMaster CM ON SSD.ClassId=CM.Id
LEFT JOIN HouseDetail HD ON HD.StudentId=SM.StudentId
WHERE SM.StudentId NOT IN (Select VPD.StudentId FROM ValuePointDetail VPD WHERE VPD.AccademidYearID=@AcademicYearId AND VPD.WeekNumber = COAlESCE(@WeekNumber,VPD.WeekNumber))
AND  HD.AccademidYearID=@AcademicYearId
AND SSD.ClassId = COAlESCE(@classId,SSD.ClassId)
AND HD.HouseId = COAlESCE(@HouseId,HD.HouseId)

ORDER BY TotalPoint ASC

--SELECT TOP 5 * FROM @StudentPointList WHERE TotalPoint>0 ORDER BY TotalPoint DESC
SELECT TOP 5 * FROM @StudentPointList  ORDER BY TotalPoint ASC

END




GO
/****** Object:  StoredProcedure [dbo].[uspGetDashboardLeastValues]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			<Krupali Prajapati> 
-- Created date:	<26-11-2018>
-- Description:		<Get Least 5 Value Points>
-- =============================================
-- uspGetDashboardLeastValues 3,2,
CREATE PROCEDURE [dbo].[uspGetDashboardLeastValues]
@AcademicYearId	AS BIGINT = 0,
@SchoolId AS BIGINT = 0,
@WeekNumber AS BIGINT = Null
AS
BEGIN
SELECT TOP 1 @AcademicYearId=Id FROM AccademicYear AY Where Ay.IsCurrent=1;

DECLARE  @ValuePointCounts TABLE (
ValuePointId BIGINT,
PointName Nvarchar(MAX),
TotalPoint BIGINT
)

INSERT INTO @ValuePointCounts
SELECT VM.Id AS ValuePointId,VM.Name AS PointName, 0 AS TotalPoint FROM ValuePointMaster VM  
		WHERE VM.IsActive=1 AND VM.IsDelete=0 AND VM.SchoolId=@SchoolId
		AND VM.Id NOT IN (SELECT VPD.ValuePointId FROM ValuePointDetail VPD WHERE VPD.AccademidYearID=@AcademicYearId)
UNION 
SELECT 
vpd.ValuePointId AS ValuePointId,
IIF (vm.Name IS NULL, 'Other', vm.Name) AS PointName,
COUNT(vpd.ValuePointId) AS TotalPoint
FROM ValuePointDetail vpd
FULL OUTER JOIN ValuePointMaster vm ON vpd.ValuePointId = vm.Id
JOIN StudentMaster sm ON vpd.StudentId = sm.Id
WHERE vpd.AccademidYearID=@AcademicYearId 
AND COALESCE(IIF(vm.SchoolId IS NULL,sm.SchoolId,vm.SchoolId),0) = COALESCE(@SchoolId,0)
AND VPD.WeekNumber = COAlESCE(@WeekNumber,VPD.WeekNumber)
AND IIF(vm.IsActive IS NULL,1,vm.IsActive) = 1 AND IIF(vm.IsDelete IS NULL,0,vm.IsDelete)=0
AND sm.IsActive =1 AND sm.IsDelete =0 
GROUP BY vpd.ValuePointId, vm.Name



SELECT SUM(TotalPoint) AS MinPointTotal 
FROM  @ValuePointCounts WHERE ValuePointId IN (SELECT TOP 5 ValuePointId FROM  @ValuePointCounts ORDER BY TotalPoint ASC)

SELECT TOP 5 * FROM  @ValuePointCounts  ORDER BY TotalPoint ASC


END




GO
/****** Object:  StoredProcedure [dbo].[uspGetDashboardNotificationMSGList]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			<Krupali Prajapati> 
-- Created date:	<26-11-2018>
-- Description:		<Get Top 4 House List by Max Value Point>
-- =============================================
--uspGetDashboardNotificationMSGList 2
CREATE PROCEDURE [dbo].[uspGetDashboardNotificationMSGList]
@SchoolId AS BIGINT = 0
AS
BEGIN

SELECT GME.MessageTitle AS Title,'Message' AS Type,
IIF(GMD.GroupId =0, (LcR.FirstName+''+LcR.LastName),(LcR.FirstName+''+LcR.LastName + '('+ GM.Name +')')) AS Receiver, 
LcS.FirstName+''+LcS.LastName AS Sender
,GME.CreatedDate AS CreatedDate
--,CONVERT(VARCHAR(MAX),GMD.CreatedDate,20) AS CreatedDate
FROM GroupMessageDetail GMD
INNER JOIN GroupMessage GME ON GME.GroupMessageId=GMD.GroupMessageId
JOIN LoginCredentials LcR ON GMD.UserId = LcR.UserId
JOIN LoginCredentials LcS ON GME.SenderId = LcS.UserId
--JOIN GroupMaster GM ON COALESCE(IIF(GM.Id=0,0,GM.Id),0) = COALESCE(GMD.GroupId,0)
FULL OUTER JOIN GroupMaster GM ON GM.Id = gmd.GroupId
WHERE CONVERT(VARCHAR(MAX),GME.CreatedDate,103) = CONVERT(VARCHAR(MAX),GETDATE(),103) 
AND LcR.SchoolId = @SchoolId
AND LcS.SchoolId = @SchoolId
UNION ALL
SELECT NT.NotificationType AS Title,'Notification' AS Type,LcR.FirstName+''+LcR.LastName AS Reciever, LcS.FirstName+''+LcS.LastName AS Sender
,N.CreatedDate AS CreatedDate
--,CONVERT(VARCHAR(MAX),N.CreatedDate,20) AS CreatedDate
FROM Notification N
JOIN LoginCredentials LcR ON N.UserId = LcR.UserId
JOIN LoginCredentials LcS ON N.CreatedBy = LcS.UserId
JOIN NotificationType NT ON n.NotificationTypeId = NT.NotificationTypeId
WHERE LcR.SchoolId = @SchoolId
AND LcS.SchoolId = @SchoolId
AND CONVERT(VARCHAR(MAX),N.CreatedDate,103) = CONVERT(VARCHAR(MAX),GETDATE(),103)  
ORDER BY CreatedDate  DESC

END




GO
/****** Object:  StoredProcedure [dbo].[uspGetDashboardTopActiveTeachers]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			<Krupali Prajapati> 
-- Created date:	<26-11-2018>
-- Description:		<Get Top 5 Value Points>
-- =============================================
-- uspGetDashboardTopActiveTeachers 3,2,26
CREATE PROCEDURE [dbo].[uspGetDashboardTopActiveTeachers]
@AcademicYearId	AS BIGINT = 0,
@SchoolId AS BIGINT = 0,
@WeekNumber AS BIGINT = Null
AS
BEGIN
SELECT TOP 1 @AcademicYearId=Id FROM AccademicYear AY Where Ay.IsCurrent=1;

SELECT TOP 5 
LC.UserId AS TeacherId,
LC.FirstName+''+ LC.LastName AS TeacherName,
LC.ProfilePic AS ProfilePic,
COUNT(VPD.ValuePointId) AS TotalPoint

FROM ValuePointDetail VPD 
JOIN LoginCredentials LC on VPD.AddedBy = LC.UserId
WHERE VPD.AccademidYearID = @AcademicYearId AND LC.SchoolId = @SchoolId AND LC.RoleId = 3
AND LC.IsActive =1 AND LC.IsDelete = 0
AND VPD.WeekNumber = COAlESCE(@WeekNumber,VPD.WeekNumber)
GROUP BY LC.UserId,lc.FirstName, lc.LastName, lc.ProfilePic
ORDER BY COUNT(VPD.ValuePointId) DESC

END




GO
/****** Object:  StoredProcedure [dbo].[uspGetDashboardTopStudentList]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			<Krupali Prajapati> 
-- Created date:	<26-11-2018>
-- Description:		<Get Top 5 Student List>
-- =============================================
--uspGetDashboardTopStudentList 3,2,Null,2
CREATE PROCEDURE [dbo].[uspGetDashboardTopStudentList]
@AcademicYearId	AS BIGINT = 0,
@SchoolId AS BIGINT = 0,
@WeekNumber AS BIGINT = Null,
@classId AS BIGINT = Null,
@HouseId AS BIGINT = Null
AS
BEGIN
SELECT TOP 1 @AcademicYearId=Id FROM AccademicYear AY Where Ay.IsCurrent=1;

SELECT TOP 5
sm.Id As StudentId,
sm.FirstName + ' ' + sm.LastName AS StudentName
,sm.ProfilePic AS ProfilePic
,cm.Name AS ClassName
,SecM.Name AS SectionName
,HM.Name AS HouseName
,COUNT(VPD.ValuePointId) AS TotalPoint

FROM StudentMaster SM
JOIN StudentSchoolDetail SSD ON SM.Id = SSD.StudentId
JOIN ValuePointDetail VPD ON SM.Id = VPD.StudentId
JOIN HouseDetail HD on SM.Id = HD.StudentId
JOIN HouseMaster HM ON HD.HouseId = HM.Id
JOIN ClassMaster CM on SSD.ClassId = CM.Id
JOIN SectionMaster SecM ON SSD.SectionId = SecM.Id

WHERE SM.SchoolId = @SchoolId AND HD.AccademidYearID = @AcademicYearId AND SM.IsActive = 1 AND SM.IsDelete =0
AND SSD.ClassId = COAlESCE(@classId,SSD.ClassId)
AND HD.HouseId = COAlESCE(@HouseId,HD.HouseId)
AND VPD.WeekNumber = COAlESCE(@WeekNumber,VPD.WeekNumber)
AND VPD.AccademidYearID = @AcademicYearId


GROUP BY sm.Id,SM.FirstName,SM.LastName,cm.Name,SecM.Name,HM.Name,sm.ProfilePic
ORDER BY COUNT(VPD.ValuePointId) DESC

END




GO
/****** Object:  StoredProcedure [dbo].[uspGetDashboardTopValues]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			<Krupali Prajapati> 
-- Created date:	<26-11-2018>
-- Description:		<Get Top 5 Value Points>
-- =============================================
-- uspGetDashboardTopValues 2,3,22
CREATE PROCEDURE [dbo].[uspGetDashboardTopValues]
@SchoolId AS BIGINT = 0,
@AcademicYearId	AS BIGINT = null,
@WeekNumber AS BIGINT = Null
AS
BEGIN

SELECT TOP 1 @AcademicYearId=Id FROM AccademicYear AY Where Ay.IsCurrent=1;

--Total ValuePointCount
DECLARE @TotalValuePoint TABLE(
	VALUEPOINT BIGINT 
	) 
INSERT INTO @TotalValuePoint
SELECT TOP 5
COUNT(vpd.ValuePointId) AS TotalValuePoint
FROM ValuePointDetail vpd
FULL OUTER JOIN ValuePointMaster vm ON vpd.ValuePointId = vm.Id
JOIN StudentMaster sm ON vpd.StudentId = sm.Id
WHERE vpd.AccademidYearID=@AcademicYearId 
AND COALESCE(IIF(vm.SchoolId IS NULL,sm.SchoolId,vm.SchoolId),0) = COALESCE(@SchoolId,0)
AND VPD.WeekNumber = COAlESCE(@WeekNumber,VPD.WeekNumber)
AND IIF(vm.IsActive IS NULL,1,vm.IsActive) = 1 AND IIF(vm.IsDelete IS NULL,0,vm.IsDelete)=0
AND sm.IsActive =1 AND sm.IsDelete =0
GROUP BY vpd.ValuePointId
ORDER BY COUNT(VPD.ValuePointId) DESC

SELECT ISNULL(SUM(VALUEPOINT),0) AS MaxPointTotal FROM @TotalValuePoint

SELECT TOP 5
vpd.ValuePointId,
IIF (vm.Name IS NULL, 'Other', vm.Name) AS PointName,
COUNT(vpd.ValuePointId) AS TotalPoint
FROM ValuePointDetail vpd
FULL OUTER JOIN ValuePointMaster vm ON vpd.ValuePointId = vm.Id
JOIN StudentMaster sm ON vpd.StudentId = sm.Id
WHERE vpd.AccademidYearID=@AcademicYearId 
AND COALESCE(IIF(vm.SchoolId IS NULL,sm.SchoolId,vm.SchoolId),0) = COALESCE(@SchoolId,0)
AND VPD.WeekNumber = COAlESCE(@WeekNumber,VPD.WeekNumber) 
AND IIF(vm.IsActive IS NULL,1,vm.IsActive) = 1 AND IIF(vm.IsDelete IS NULL,0,vm.IsDelete)=0
AND sm.IsActive =1 AND sm.IsDelete =0
GROUP BY vpd.ValuePointId, vm.Name
ORDER BY COUNT(VPD.ValuePointId) DESC

END




GO
/****** Object:  StoredProcedure [dbo].[uspGetDashboardTotalCount]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			<Krupali Prajapati> 
-- Created date:	<26-11-2018>
-- Description:		<Get Total count of Dashboard>
-- =============================================
-- uspGetDashboardTotalCount 3,2
CREATE PROCEDURE [dbo].[uspGetDashboardTotalCount]
@AcademicYearId	AS BIGINT = 0,
@SchoolId AS BIGINT = 0
AS
BEGIN
DECLARE @MaleCount BIGINT = 0, @FemaleCount BIGINT = 0, @TotalTeachers BIGINT = 0

SET @MaleCount = (SELECT COUNT(*) FROM StudentMaster s JOIN StudentSchoolDetail sd ON sd.StudentId = s.Id  WHERE Gender = 'M' AND s.SchoolId = @schoolId AND sd.AcademicYearId = @AcademicYearId AND s.IsDelete = 0 AND s.IsActive = 1)
SET @FemaleCount = (SELECT COUNT(*) FROM StudentMaster s JOIN StudentSchoolDetail sd ON sd.StudentId = s.Id  WHERE Gender = 'F' AND s.SchoolId = @schoolId AND sd.AcademicYearId = @AcademicYearId AND s.IsDelete = 0 AND s.IsActive = 1 )
SET @TotalTeachers = (SELECT COUNT(*) FROM LoginCredentials WHERE RoleId=3 AND SchoolId=2 AND IsActive=1)


Select count(*) AS TotalStudents, @MaleCount AS TotalMaleStudents, @FemaleCount AS TotalFemaleStudents, @TotalTeachers AS TotalTeachers

FROM StudentSchoolDetail SSD 
JOIN StudentMaster SM ON SM.Id = SSD.StudentId
WHERE SSD.AcademicYearId = @AcademicYearId  AND SM.SchoolId = @SchoolId AND sm.IsDelete = 0 AND SM.IsActive =1
END




GO
/****** Object:  StoredProcedure [dbo].[uspGetInboxList]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
-- uspGetInboxList 44
CREATE PROCEDURE [dbo].[uspGetInboxList]
	@UserId BIGINT = 0,
	@MessageId BIGINT = NULL
AS
BEGIN
	SELECT GMDS.Id AS InboxId,
	   GM.MessageTitle AS Title,
	   GM.MessageText AS [Description],
	   GM.SimpleText AS [DescriptionText],
	   GMDS.IsRead AS IsRead,
	   CONVERT(VARCHAR,  GM.CreatedDate ,103) AS [Date],
	   Format(GM.CreatedDate,'hh:mm tt') AS [Time],
	   LG.FirstName + ' ' + LG.LastName AS FromWho,
	   (STUFF((SELECT ',' + Reciver.[Name] 
		FROM 
		(SELECT (FirstName + ' ' + LastName) AS [Name] FROM GroupMessageDetail gmd 
		INNER JOIN [LoginCredentials] lc on lc.UserId=gmd.UserId
		where MessageTitle=GM.MessageTitle and gmd.GroupMessageId=GM.GroupMessageId and gmd.GroupId = 0
		UNION
		SELECT [Name]
		FROM [GroupMessageDetail] gmd 
		INNER JOIN GroupMaster lc on lc.Id=gmd.GroupId
		where MessageTitle=GM.MessageTitle and gmd.GroupMessageId=GM.GroupMessageId and gmd.GroupId != 0
		GROUP BY [Name]
		) Reciver
		FOR XML PATH('')),1,1,'')) AS SentTo
	   FROM [GroupMessageDetail] GMDS 
		   INNER JOIN GroupMessage GM ON GM.GroupMessageId=GMDS.GroupMessageId
		   INNER JOIN [LoginCredentials] LG ON GM.SenderId = LG.UserId 
	   Where GMDS.UserID = @UserId AND GMDS.Id = COALESCE(@MessageId,GMDS.Id)
	   Order by GM.[CreatedDate] desc
END




GO
/****** Object:  StoredProcedure [dbo].[uspGetMobileUserInfo]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			<Dhrumil Patel> 
-- Created date:	<15-10-2018>
-- Description:		<Get Mobile User Info>
-- =============================================
-- uspGetMobileUserInfo 47,3  --teacher
-- uspGetMobileUserInfo 50,4  --Parents
CREATE PROCEDURE [dbo].[uspGetMobileUserInfo]
	@UserId BIGINT = 0,
	@RoleId BIGINT = 0
AS
BEGIN
	IF(@RoleId = 3)
	BEGIN
		SELECT
			LC.UserId
			,LC.SchoolId
			,(LC.FirstName + ' ' + LC.LastName) AS [Name]
			,(Case when (LC.ProfilePic  != null or LC.ProfilePic != '') then  'http://ws-srv-net.in.webmyne.com/Applications/SchoolManagement/Uploads/ProfilePic/UserLogo/'+ CAST(LC.UserId AS Nvarchar(MAX)) +'/'+LC.ProfilePic else '' end) AS ProfileImg
			,LC.EmailId
			,LC.Mobile
			,TM.PermanentAddress As PermenantAddress
			,TM.CurrentAddress
			,TM.Landline As LandLine
			,CONVERT(BIT, 0) AS IsParent
			,LC.IsActive	
			,(select HM.Name from HouseMaster HM where HM.Id = TM.HouseId) AS HouseName
			,(select SM.Name from SubjectMaster SM where SM.Id = (select TD.SubjectId from TeacherDetail TD where TD.TeacherId = TM.UserId and TD.IsHomeTeacher = 1)) AS SubjectName
		FROM 
		LoginCredentials LC
		INNER JOIN TeacherMaster TM ON TM.UserId = LC.UserId
		WHERE LC.UserId = @UserId
	END
	ELSE
	BEGIN
		SELECT
			LC.UserId
			,0 AS SchoolId
			,(LC.FirstName + ' ' + LC.LastName) AS [Name]
			,(Case when (LC.ProfilePic  != null or LC.ProfilePic != '') then  'http://ws-srv-net.in.webmyne.com/Applications/SchoolManagement/Uploads/ProfilePic/UserLogo/'+ CAST(LC.UserId AS Nvarchar(MAX)) +'/'+LC.ProfilePic else '' end) AS ProfileImg
			,LC.EmailId
			,LC.Mobile
			,GM.PermanentAddress
			,GM.CurrentAddress
			,GM.Landline
			,CONVERT(BIT, 1) AS IsParent
			,LC.IsActive
		FROM 
		LoginCredentials LC
		INNER JOIN GuardianMaster GM ON GM.UserId = LC.UserId
		WHERE LC.UserId = @UserId

			--List of Own Children
	SELECT 
	  SM.Id AS ChildId,
	  (Case when (GSD.Relationship = 1) then 'Mother' else (Case when (GSD.Relationship = 2) then 'Father' else 'Guardian' end) end) AS Relation,
	  (SM.FirstName + ' ' + SM.LastName) AS ChildName,
	  (Case when (SM.ProfilePic  != null or SM.ProfilePic != '') then  'http://ws-srv-net.in.webmyne.com/Applications/SchoolManagement/Uploads/ProfilePic/StudentLogo/'+ CAST(SM.Id AS Nvarchar(MAX)) +'/'+SM.ProfilePic else '' end) AS ChildImage
	  FROM StudentMaster SM 
	  LEFT JOIN GuardianStudentDetail GSD  ON GSD.StudentId=SM.Id 
    WHERE  GSD.ParentId=@UserId AND GSD.Relationship != 3

				

	--List of Guardian Children
	SELECT 
	  SM.Id AS ChildId,
	  (Case when (GSD.Relationship = 1) then 'Mother' else (Case when (GSD.Relationship = 2) then 'Father' else 'Guardian' end) end) AS Relation,
	  (SM.FirstName + ' ' + SM.LastName) AS ChildName,
	  (Case when (SM.ProfilePic  != null or SM.ProfilePic != '') then  'http://ws-srv-net.in.webmyne.com/Applications/SchoolManagement/Uploads/ProfilePic/StudentLogo/'+ CAST(SM.Id AS Nvarchar(MAX)) +'/'+SM.ProfilePic else '' end) AS ChildImage
	  FROM StudentMaster SM 
	  LEFT JOIN GuardianStudentDetail GSD  ON GSD.StudentId=SM.Id 
    WHERE  GSD.ParentId=@UserId AND GSD.Relationship = 3
	END
END




GO
/****** Object:  StoredProcedure [dbo].[uspGetNotificationList]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- uspGetNotificationList 35
CREATE PROCEDURE [dbo].[uspGetNotificationList]
	@UserId BIGINT = 0
AS
BEGIN
	SELECT NF.NotificationId AS NotificationId,
	   NF.MessageEn AS Title,
	   NF.IsRead AS IsRead,
	   CONVERT(VARCHAR,  NF.CreatedDate ,103) AS [Date],
	   Format(NF.CreatedDate,'hh:mm tt') AS [Time],
	   LG.FirstName + ' ' + LG.LastName AS FromWho,
	   NF.StudentId
	FROM [Notification] NF 
	INNER JOIN [LoginCredentials] LG
		ON NF.CreatedBy = LG.UserId Where NF.UserID = @UserId
	Order by NF.[CreatedDate] desc
END






GO
/****** Object:  StoredProcedure [dbo].[uspGetParentDashboard]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			<Krupali Prajapati> 
-- Created date:	<1-10-2018>
-- Description:		<Get Student List>
-- I need to update weepoints logic
-- =============================================
-- uspGetParentDashboard 44
CREATE PROCEDURE [dbo].[uspGetParentDashboard]
	@USERID BIGINT = 0
	--, @SCHOOLID BIGINT= 0
AS
BEGIN
SELECT
	GSD.StudentId AS ChildId
   ,SM.FirstName + ' ' + SM.MiddleName + ' ' + SM.LastName AS ChildName
   ,(Case when (SM.ProfilePic  != null or SM.ProfilePic != '') then  'http://ws-srv-net.in.webmyne.com/Applications/SchoolManagement/Uploads/ProfilePic/StudentLogo/'+ CAST(SM.Id AS Nvarchar(MAX)) +'/'+SM.ProfilePic else '' end) AS ChildImage
   ,S.Name AS SchoolName
   ,S.Id AS SchoolId
   ,CM.Name + ' - ' + SECM.Name + ' - ' + HM.Name AS [Standard]
   ,LC.IsActive
   ,HM.Name As HomeName
   ,(SELECT Count(*) FROM ValuePointDetail VPD LEFT JOIN AccademicYear AY ON VPD.AccademidYearID=AY.Id WHERE AY.IsCurrent=1 AND VPD.StudentId=SM.ID) AS AllPoints
   ,(SELECT Count(*) FROM ValuePointDetail VPD LEFT JOIN AccademicYear AY ON VPD.AccademidYearID=AY.Id WHERE AY.IsCurrent=1 AND VPD.StudentId=SM.ID AND VPD.WeekNumber= 
   (SELECT WeekNumber FROM AccademicYearWeekDetail WHERE  CAST(CONVERT(VARCHAR(10),GETDATE(), 112) AS BIGINT ) BETWEEN StartDateInt AND EndDateInt ) 
   ) AS WeekPoints
FROM LoginCredentials LC
JOIN GuardianMaster GM
	ON GM.UserId = lc.UserId
 JOIN GuardianStudentDetail GSD
	ON GSD.ParentId = LC.UserId
 JOIN StudentMaster SM
	ON SM.Id = GSD.StudentId
 JOIN School S
	ON S.Id = LC.SchoolId
 JOIN StudentSchoolDetail SSD
	ON SSD.StudentId = SM.Id
 JOIN ClassMaster CM
	ON CM.Id = SSD.ClassId
 JOIN SectionMaster SECM
	ON SECM.Id = SSD.SectionId
 JOIN HouseDetail HD
	ON HD.StudentId = SSD.StudentId
 JOIN HouseMaster HM
	ON HM.Id = HD.HouseId
WHERE GM.IsActive = 1
AND GM.IsDelete = 0
AND lc.UserId = @USERID

SELECT  StudentId INTO #ControlTable 
FROM GuardianStudentDetail WHERE ParentId = @USERID

DECLARE @StudentId INT
 
DECLARE @ChildValuePoints TABLE (
		ChildId BIGINT,
		PointName NVARCHAR(MAX) , 
		Date NVARCHAR(MAX),
		DateInt BIGINT ,
		Time NVARCHAR(MAX),FromWho NVARCHAR(MAX)
)

WHILE exists (SELECT * FROM #ControlTable)
BEGIN

    SELECT TOP 1 @StudentId = StudentId
    FROM #ControlTable
    ORDER BY StudentId ASC

	INSERT INTO @ChildValuePoints
    SELECT TOP 5
	VPD.StudentId AS ChildId
    ,ISNULL((SELECT  VM.Name FROM ValuePointMaster VM WHERE VM.ID=VPD.ValuePointId),'Other - ' + ISNULL(VPD.Note,'no comments')) AS PointName
	,CONVERT(VARCHAR, VPD.CreatedDate ,103) as [Date]
	,VPD.CreatedDateInt as [DateInt]
	,Format(VPD.CreatedDate,'hh:mm tt') AS [Time]
	,lc.FirstName + + lc.LastName AS FromWho
	 FROM ValuePointDetail VPD LEFT JOIN  LoginCredentials LC ON VPD.AddedBy=LC.UserId
	 LEFT JOIN AccademicYear AY ON VPD.AccademidYearID=AY.Id WHERE AY.IsCurrent=1 AND
	 VPD.StudentId=@StudentId
	 ORDER BY VPD.CreatedDate DESC

    DELETE #ControlTable
    WHERE StudentId = @StudentId

END

DROP TABLE #ControlTable

SELECT * FROM @ChildValuePoints

END



GO
/****** Object:  StoredProcedure [dbo].[uspGetParentList]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			<krupali Prajapati>
-- Created date:	<22-10-2018>
-- Description:		<Get Parent List>
-- =============================================
-- uspGetParentList 4,2
CREATE PROCEDURE [dbo].[uspGetParentList]
	@RoleId BIGINT = 0,
	@SchoolId BIGINT = 0
AS
BEGIN
	SELECT
		LC.UserId AS Id
		,LC.RoleId
		,LC.SchoolId
		,LC.[Password]
		,S.[Name] AS SchoolName
		,(LC.FirstName + ' ' + GM.MiddleName + ' ' + LC.LastName) AS ParentName
		,LC.EmailId
		,LC.Mobile
		,GM.Landline
		,GM.CurrentAddress
		,GM.PermanentAddress
		,GM.EducationLevel
		,GM.Religion
		,GM.Notes
		,GM.Country
		,GM.City
		,GM.Zipcode
		,GM.IdNumber
		,LC.[Address]
		,LC.ProfilePic
		,LC.SchoolId
		,LC.IsActive
		,LC.IsDelete
		,c.Name as CountryName
		,r.Name as ReligionName
		
	FROM  LoginCredentials LC
	JOIN GuardianMaster GM ON GM.UserId = LC.UserId
	JOIN School S ON S.Id = LC.SchoolId
	LEFT JOIN Country c ON c.Id = GM.Country
	 left JOIN Religion r on r.Id = gm.Religion
	WHERE LC.IsDelete = 0 AND LC.RoleId = @RoleId AND LC.SchoolId = @SchoolId
	ORDER BY LC.FirstName + ' ' + LC.LastName DESC
END




GO
/****** Object:  StoredProcedure [dbo].[uspGetReceipentList]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
-- uspGetReceipentList 2
CREATE PROCEDURE [dbo].[uspGetReceipentList]
@SchoolId BIGINT = 0
AS
BEGIN
	SELECT 
	 LC.UserId AS Id
	,LC.FirstName + ' ' + LC.LastName + ' - ' + (CASE WHEN  LC.RoleId=3 THEN 'Teacher' ELSE CASE WHEN LC.RoleId=4 THEN 'Parent' END END) AS [RecipientName] 
	FROM LoginCredentials LC
	WHERE LC.IsActive=1 and LC.IsDelete=0 AND RoleId IN (3,4) AND SchoolId=@SchoolId
UNION All
	SELECT 
		Id,Name + ' - Group' AS DisplayText
	FROM GroupMaster Where SchoolId=@SchoolId
END




GO
/****** Object:  StoredProcedure [dbo].[uspGetSchoolAdminList]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			<Dhrumil Patel> 
-- Created date:	<21-09-2018>
-- Description:		<Get School Admin List>
-- =============================================
-- uspGetSchoolAdminList 2
CREATE PROCEDURE [dbo].[uspGetSchoolAdminList]
	@RoleId BIGINT = 0
AS
BEGIN
	SELECT
		LC.UserId AS Id
		,LC.RoleId
		,LC.SchoolId
		,LC.[Password]
		,S.[Name] AS SchoolName
		,(LC.FirstName + ' ' + LC.LastName) AS AdminName
		,LC.EmailId
		,LC.Mobile
		,LC.[Address]
		,LC.ProfilePic
		,LC.IsActive
		,Lc.IsDelete
	FROM LoginCredentials LC
	INNER JOIN School S ON S.Id = LC.SchoolId
	WHERE LC.IsDelete = 0 AND LC.RoleId = @RoleId
END




GO
/****** Object:  StoredProcedure [dbo].[uspGetSchoolHouses]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			<Sweta Patel> 
-- Created date:	<12-11-2018>
-- Description:		<Get School Houses Based On school Id>
-- =============================================
-- uspGetSchoolHouses 
CREATE PROCEDURE [dbo].[uspGetSchoolHouses]
	@UserId BIGINT = NUll,
	@SchoolId AS BIGINT=null
AS
BEGIN
  SELECT 
 HM.AcademicYearId
,HM.SchoolId
,HM.Id
,HM.Name AS HouseName
,Y.Id AS CurrentYear
,(Case when (HM.Image  != null or HM.Image != '') then  'http://ws-srv-net.in.webmyne.com/Applications/SchoolManagement/Uploads/ProfilePic/HomeLogo/'+ CAST(HM.Id AS Nvarchar(MAX)) +'/'+HM.Image else '' end) AS HouseIcon
,Count(HD.StudentId) AS TotalStudent
,ISNULL((SELECT Count(*) FROM TeacherMaster TM Where TM.HouseId=HM.Id ),0) AS TotalTeacher
,ISNULL((SELECT Count(*) FROM ValuePointDetail VPD Where VPD.StudentId IN (SELECT H.StudentId FROM HouseDetail H WHERE H.HouseId=HM.Id) AND VPD.AccademidYearID=Y.Id),0) AS TotalPoint
FROM HouseMaster HM
INNER JOIN AccademicYear Y ON HM.AcademicYearId =Y.Id AND Y.IsCurrent=1
INNER JOIN HouseDetail HD ON HM.Id=HD.HouseId
GROUP BY HM.Id,HM.Name,HM.SchoolId,HM.Image,Y.Id, HM.AcademicYearId
HAVING HM.SchoolId=COALESCE(@SchoolId,HM.SchoolId) 

ORDER BY TotalPoint DESC



END





GO
/****** Object:  StoredProcedure [dbo].[uspGetSchoolHousesGraphical]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			<Sweta Patel> 
-- Created date:	<13-11-2018>
-- Description:		<Get School Houses Based On school Id with Graph Data>
-- =============================================
-- uspGetSchoolHousesGraphical 2
-- uspGetSchoolHousesGraphical 3
CREATE PROCEDURE [dbo].[uspGetSchoolHousesGraphical]
	@SchoolId AS BIGINT=null
AS
BEGIN
DECLARE	@TotalHouse AS BIGINT=0;
DECLARE @ScaleUnit AS BIGINT=0;
DECLARE	@ScaleMax AS BIGINT=0;
DECLARE	@MaxPointTotal AS BIGINT=0;
DECLARE	@RoundPoint AS BIGINT=0;
DECLARE	@lastdigit AS BIGINT=0;

DECLARE @HousePoint TABLE(
			Id BIGINT,
			HouseName NVARCHAR(MAX),
			HouseIcon NVARCHAR(MAX),
			TotalPoint BIGINT 
		)  

INSERT INTO @HousePoint
SELECT 
HM.Id
,HM.Name AS  HouseName
,(Case when (HM.Image  != null or HM.Image != '') then  'http://ws-srv-net.in.webmyne.com/Applications/SchoolManagement/Uploads/ProfilePic/HomeLogo/'+ CAST(HM.Id AS Nvarchar(MAX)) +'/'+HM.Image else '' end) AS HouseIcon
,Count(VPD.StudentId) AS TotalPoint
	FROM HouseMaster HM
		LEFT JOIN HouseDetail HD ON HM.Id=HD.HouseId
		INNER JOIN AccademicYear AY ON HM.AcademicYearId=AY.Id AND AY.ISCurrent=1
		LEFT JOIN ValuePointDetail VPD ON VPD.StudentId=HD.StudentId AND VPD.AccademidYearID=AY.Id
	WHERE AY.IsCurrent=1 AND HM.SchoolId=COALESCE(@SchoolId,HM.SchoolId)
	AND HM.IsActive=1 AND HM.IsDelete=0
GROUP BY HM.Id,HM.Name,HM.Image

SELECT @TotalHouse=Count(*) FROM @HousePoint
SELECT @MaxPointTotal=ISNULL(MAX(TotalPoint),0) FROM @HousePoint
SELECT @lastdigit=RIGHT(ISNULL(@MaxPointTotal,0), 1)
		IF  @MaxPointTotal>0 
			BEGIN
				SELECT @ScaleMax=ISNULL(@MaxPointTotal,0) + (10-ISNULL(@lastdigit,0))
		END
		IF  @MaxPointTotal=0 
			BEGIN
				SELECT @ScaleMax=0
		END
SELECT @ScaleUnit=ISNULL((ISNULL(@ScaleMax,0)/5),0)

SELECT ISNULL(@SchoolId,0)  AS SchoolId,@TotalHouse AS TotalHouse, @ScaleMax AS ScaleMax , @ScaleUnit AS ScaleUnit ,@MaxPointTotal AS MaxPoint

--SELECT Id,HouseIcon,TotalPoint FROM @HousePoint

SELECT 
HP.Id
,HP.HouseName
,HP.HouseIcon
,ISNULL((SELECT Count(*) FROM TeacherMaster TM INNER JOIN LoginCredentials LC ON TM.UserId=LC.UserId AND LC.IsActive=1 AND LC.IsDelete=0 AND LC.RoleId=3 Where TM.HouseId=HP.Id  ),0) AS TotalTeacher
,ISNULL((SELECT Count(*) FROM HouseDetail HD INNER JOIN StudentMaster SM ON HD.StudentId=SM.Id AND SM.IsActive=1 AND SM.IsDelete=0  Where HD.HouseId=HP.Id),0) AS TotalStudent
,HP.TotalPoint
 FROM @HousePoint HP 
 ORDER BY TotalPoint DESC

END





GO
/****** Object:  StoredProcedure [dbo].[uspGetSchoolTeacherList]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			<Dhrumil Patel> 
-- Created date:	<24-02-2018>
-- Description:		<Get School Teacher List>
-- =============================================
-- uspGetSchoolTeacherList 3,2
CREATE PROCEDURE [dbo].[uspGetSchoolTeacherList]
	@RoleId BIGINT = 0,
	@SchoolId BIGINT = 0
AS
BEGIN
	SELECT
	
		LC.UserId AS Id
		,LC.RoleId
		,LC.SchoolId
		,LC.[Password]
		,S.[Name] AS SchoolName
		,(LC.FirstName + ' ' + LC.LastName) AS TeacherName
		,LC.EmailId
		,LC.Mobile
		,LC.[Address]
		,LC.ProfilePic
		,LC.SchoolId
		,LC.IsActive
		,LC.IsDelete
		,(CASE WHEN TM.Gender = 1 THEN 'fa-male fa-2x' ELSE 'fa-female fa-2x' END) AS Gender
		,ISNULL((SELECT [Name] FROM Religion WHERE Id = TM.Religion),'-NA-') AS Religion
		,ISNULL(TM.BloodGroup,'-NA-') AS BloodGroup
		,TM.BirthDate
		,TM.HireDate
		--,ISNULL(TM.BirthDate,GETDATE()) as BirthDate
		--,ISNULL(TM.HireDate,GETDATE()) as HireDate
		,CONVERT(VARCHAR(11), BirthDate, 106) AS BirthDateD
		,CONVERT(VARCHAR(11), HireDate, 106) AS HireDateD
		,HM.[Name] AS HouseName
		,(Select (SM.[Name] + ' (' + (SELECT [Name] FROM ClassMaster WHERE Id = CM.Id) + ')') AS [Name]
			FROM SectionMaster SM
			INNER JOIN ClassMaster CM ON CM.Id = SM.ClassId
			WHERE SM.Id = TM.HomeTeacher AND CM.IsActive = 1
				AND CM.IsDelete = 0) AS HomeTeacher
	FROM LoginCredentials LC
	INNER JOIN TeacherMaster TM ON TM.UserId = LC.UserId
	INNER JOIN School S ON S.Id = LC.SchoolId
	INNER JOIN HouseMaster HM ON HM.Id = TM.HouseId
	WHERE LC.IsDelete = 0 AND LC.RoleId = @RoleId AND LC.SchoolId = @SchoolId
	ORDER BY LC.FirstName + ' ' + LC.LastName DESC
END



--SELECT * from teacherMaster



GO
/****** Object:  StoredProcedure [dbo].[uspGetSectionWithClassBySchool]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			<Dhrumil Patel> 
-- Created date:	<08-10-2018>
-- Description:		<Get Section List With Class By School>
-- =============================================
-- uspGetSectionWithClassBySchool 2
CREATE PROCEDURE [dbo].[uspGetSectionWithClassBySchool]
	@SchoolId BIGINT = 0
AS
BEGIN
	Select 
		SM.Id
		,(SM.[Name] + ' (' + (SELECT [Name] FROM ClassMaster WHERE Id = CM.Id) + ')') AS [Name]
	FROM SectionMaster SM
	INNER JOIN ClassMaster CM ON CM.Id = SM.ClassId
	WHERE CM.SchoolId = @SchoolId AND CM.IsActive = 1
		AND CM.IsDelete = 0
	ORDER BY CM.Id
END




GO
/****** Object:  StoredProcedure [dbo].[uspGetStudentList]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			<Krupali Prajapati> 
-- Created date:	<1-10-2018>
-- Description:		<Get Student List>
-- =============================================
-- uspGetStudentList 2
CREATE PROCEDURE [dbo].[uspGetStudentList]
	@SchoolId BIGINT = 0
AS
BEGIN
	SELECT
		SM.Id,
		SM.StudentCode,
		SM.FirstName ,
		SM.LastName,
		SM.MiddleName,
		sm.Gender,
		cast(SM.BirthDate as NVARCHAR) AS BirthDate,
		cast(SM.RegisterDate AS NVARCHAR) AS RegisterDate,
		SM.ProfilePic,
		SM.BloodGroup,
		SM.Address,
		SM.Country,
		SM.City,
		SM.ZipCode,
		SM.IsDelete,
		SM.IsActive,
		SSD.ClassRollNo,
		SSD.ClassId,
		CM.Name AS ClassName,
		S.Name AS SectionName,
		SSD.SectionId,
		SSD.ClassRollNo,
		HM.Name as House,
		CAST(SSD.JoiningDate as DATE) AS JoiningDt,
		CAST(SM.RegisterDate as DATE) AS RegDate,
		CAST(SM.BirthDate as DATE) AS BDate,
		ssd.AcademicYearId,
		AY.Name AS AcademicYear
		,(SELECT Count(Gender) FROM studentmaster WHERE Gender = 'F') AS TotalFemale
		,(SELECT Count(Gender) FROM studentmaster WHERE Gender = 'M') AS TotalMale
	FROM StudentMaster SM 
	INNER JOIN StudentSchoolDetail SSD ON SM.Id = SSD.StudentId
	JOIN ClassMaster CM ON CM.Id = SSD.ClassId
	LEFT JOIN SectionMaster S ON S.Id = SSD.SectionId
	JOIN AccademicYear AY ON AY.Id = SSD.AcademicYearId
	LEFT JOIN HouseDetail HD ON SM.Id = HD.StudentId
	LEFT JOIN HouseMaster HM ON HD.HouseId = HM.Id
	WHERE SM.IsDelete = 0 AND SM.SchoolId = @SchoolId

	--SELECT count(*) AS TotalStudent from StudentMaster WHERE IsDelete = 0
	--SELECT count(Gender) AS FemaleCount from StudentMaster where Gender = 'F'
	--SELECT count(Gender) AS maleCount from StudentMaster where Gender = 'M'



END




GO
/****** Object:  StoredProcedure [dbo].[uspGetTeacherDetailList]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			<Dhrumil Patel> 
-- Created date:	<26-02-2018>
-- Description:		<Get School Teacher Assign Detail List>
-- =============================================
-- uspGetTeacherDetailList 48
CREATE PROCEDURE [dbo].[uspGetTeacherDetailList]
	--@RoleId BIGINT = 0,
	--@SchoolId BIGINT = 0,
	@TeacherId BIGINT = 0
AS
BEGIN
	DECLARE @ClassIds NVARCHAR(MAX) = '', @SectionIds NVARCHAR(MAX) = ''
	SELECT @ClassIds = STUFF((SELECT ',' + CAST(D1.ClassId  AS VARCHAR(MAX)) 
	FROM (
		SELECT TD.ClassId FROM TeacherDetail TD INNER JOIN ClassMaster CM ON TD.ClassId=CM.Id WHERE TeacherId = @TeacherId AND CM.IsActive=1 AND CM.IsDelete=0
	) D1 FOR XML PATH('')),1,1,'')
	
	SELECT @SectionIds = STUFF((SELECT ',' + CAST(D2.SectionId  AS VARCHAR(MAX)) 
	FROM (
		SELECT SectionId FROM TeacherDetail TD INNER JOIN SectionMaster SM ON TD.SectionId=SM.Id WHERE TeacherId = @TeacherId AND SM.IsActive=1 AND SM.IsDelete=0
	) D2 FOR XML PATH('')),1,1,'')	
	
	SELECT
		LC.UserId AS TeacherId
		,(LC.FirstName + ' ' + LC.LastName) AS [Name]
		,LC.SchoolId
		,S.[Name] AS SchoolName
		,HM.[Name] AS HouseName
		,LC.ProfilePic
		,(SELECT COUNT(*) FROM StudentSchoolDetail S 
			WHERE CAST(S.ClassId AS VARCHAR(MAX)) in (SELECT LTRIM(VALUE) 
				FROM SchoolManagement_Local.dbo.split(@ClassIds,',')) 
				AND CAST(S.SectionId AS VARCHAR(MAX)) in 
					(SELECT LTRIM(VALUE) 
					FROM SchoolManagement_Local.dbo.split(@SectionIds,','))) AS TotalStudent
		,(SELECT COUNT(DISTINCT TD.ClassId) FROM TeacherDetail TD 
			INNER JOIN ClassMaster CM ON TD.ClassId=CM.Id 
			INNER JOIN SectionMaster SM ON TD.SectionId=SM.Id
			WHERE TeacherId = @TeacherId AND CM.IsActive=1 AND CM.IsDelete=0 AND SM.IsActive=1 AND SM.IsDelete=0) AS TotalClass
	FROM 
	LoginCredentials LC
	INNER JOIN TeacherMaster TM ON TM.UserId = LC.UserId
	INNER JOIN School S ON S.Id = LC.SchoolId
	LEFT JOIN HouseMaster HM ON HM.Id = TM.HouseId
	WHERE LC.UserId = @TeacherId 

	SELECT
		--LC.UserId AS TeacherId
		--,(LC.FirstName + ' ' + LC.LastName) AS [Name]
		--,LC.SchoolId
		--,S.[Name] AS SchoolName,
		TD.Id
		,CM.[Name] AS ClassName
		,SeM.[Name] AS SectionName
		,SM.[Name] AS SubjectName
		,TD.IsHomeTeacher
		,(SELECT COUNT(*) FROM StudentSchoolDetail S WHERE S.ClassId = TD.ClassId AND S.SectionId = COALESCE(SeM.Id,S.SectionId)) AS StudentCount
	FROM 
	--LoginCredentials LC
	--INNER JOIN SchoolManagement.dbo.School S ON S.Id = LC.SchoolId
	--INNER JOIN 
	TeacherDetail TD 
	--ON TD.TeacherId = LC.UserId
	INNER JOIN ClassMaster CM ON CM.Id = TD.ClassId
	LEFT JOIN SectionMaster SeM ON SeM.Id = TD.SectionId
	INNER JOIN SubjectMaster SM ON SM.Id = TD.SubjectId
	WHERE TD.TeacherId = @TeacherId 
		AND CM.IsActive = 1 AND cm.IsDelete = 0 
		AND ISNULL(SeM.IsDelete,0) = 0 AND ISNULL(SeM.IsActive,1) = 1
		--AND LC.IsDelete = 0 AND LC.RoleId = @RoleId AND LC.SchoolId = @SchoolId
	ORDER BY TD.Id DESC	
END



GO
/****** Object:  StoredProcedure [dbo].[uspGetWeeknumberList]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Sweta & Dhrumil>
-- Create date: <20/11/2018>
-- Description:	<Logic for Calculating WeekNumber>
-- =============================================
-- uspGetWeeknumberList
CREATE  PROCEDURE [dbo].[uspGetWeeknumberList]
AS
BEGIN
	DECLARE @FinalEndDateINT BIGINT=0;
DECLARE @FinalStartDate DateTime=0;
DECLARE @DBFinalEndDate DateTime=0;
DECLARE @FinalEndDate DateTime=0;
DECLARE @YearId BIGINT=0;

DECLARE @WeekDayNum  BIGINT=0;
DECLARE @StartDayNum  BIGINT=7; -- It Is Saturday
DECLARE @EndDayNum  BIGINT=6; -- It is Friday
DECLARE @AdditionCount BIGINT=0;

DECLARE @LoopEndDateINT BIGINT=0;
DECLARE @Count BIGINT=1;

DECLARE @WeekNumberTable TABLE (
AccademicYearId BIGINT,	
WeekNumber BIGINT,	
StartDate NVARCHAR(MAX),
StartDateINT NVARCHAR(MAX),
EndDate NVARCHAR(MAX),
EndDateINT NVARCHAR(MAX)
)

SELECT  @YearId=Id, @FinalStartDate=StartDate, @DBFinalEndDate=EndDate,
  @WeekDayNum=DATEPART(dw,StartDate),@FinalEndDateINT=CONVERT(VARCHAR(10),EndDate, 112) FROM AccademicYear Where IsCurrent=1

WHILE  CAST(@LoopEndDateINT AS BIGINT) <= CAST(@FinalEndDateINT AS BIGINT) AND  @Count<66
BEGIN
	
	IF @WeekDayNum=@EndDayNum --It is Friday
	BEGIN
		PRINT'It Is FRIDAY'
		SET @LoopEndDateINT=CONVERT(VARCHAR(10),DATEADD(DD,0,@FinalStartDate), 112)	

		INSERT INTO @WeekNumberTable
			SELECT @YearId As AccademicYearId, @Count As WeekNumber, 
				  @FinalStartDate,CONVERT(VARCHAR(10),@FinalStartDate, 112) AS StartDateINT,
				  @FinalStartDate AS EndDate,CONVERT(VARCHAR(10),@FinalStartDate, 112) AS EndDateINT

	   SET @FinalStartDate=DATEADD(DD,1,@FinalStartDate)
	END
	
	IF @WeekDayNum=@StartDayNum --It is Saturday
	BEGIN
		PRINT'It Is Saturday'
		IF (CAST(@FinalEndDateINT AS BIGINT)<= CAST (CONVERT(VARCHAR(10),DATEADD(DD,@EndDayNum,@FinalStartDate), 112) AS BIGINT))
		BEGIN
			SET @LoopEndDateINT = @FinalEndDateINT
			SET @FinalEndDate = @DBFinalEndDate
		END
		ELSE
		BEGIN
			SET @LoopEndDateINT= CONVERT(VARCHAR(10),DATEADD(DD,@EndDayNum,@FinalStartDate), 112)
			SET @FinalEndDate = DATEADD(DD,@EndDayNum,@FinalStartDate)
		END
			
		INSERT INTO @WeekNumberTable
			SELECT @YearId As AccademicYearId, @Count As WeekNumber, 
				   @FinalStartDate,CONVERT(VARCHAR(10),@FinalStartDate, 112) AS StartDateINT,
				   @FinalEndDate AS EndDate,
				   @LoopEndDateINT AS EndDateINT

	   SET @FinalStartDate=DATEADD(DD,@EndDayNum+1,@FinalStartDate)
	END

	IF (@WeekDayNum!=@EndDayNum  AND @WeekDayNum!=@StartDayNum)
	BEGIN
		print 'Not Friday or Saturday'
		IF @WeekDayNum< @EndDayNum		 -- If it is less than Friday 
		BEGIN
			SET @AdditionCount=(@EndDayNum-@WeekDayNum)
		END
		 
		SET @LoopEndDateINT=CONVERT(VARCHAR(10),DATEADD(DD,@AdditionCount,@FinalStartDate), 112)
		INSERT INTO @WeekNumberTable	
			SELECT @YearId As AccademicYearId, @Count As WeekNumber, Convert(varchar,@FinalStartDate,103) AS StartDate,
				CONVERT(VARCHAR(10),@FinalStartDate, 112) AS StartDateINT, 
				CONVERT(varchar,DATEADD(DD,@AdditionCount,@FinalStartDate),103) AS EndDate,
				CONVERT(VARCHAR(10),DATEADD(DD,@AdditionCount,@FinalStartDate), 112) AS EndDateINT 
	
		SET @FinalStartDate=DATEADD(DD,@AdditionCount+1,@FinalStartDate)
	END

	--In Loop counter 
    SET @LoopEndDateINT= CONVERT(VARCHAR(10),@FinalStartDate, 112)
    SELECT @WeekDayNum= DATEPART(dw,@FinalStartDate)
    SET @Count = @Count + 1;
END;

--SELECT * FROM  @WeekNumberTable

TRUNCATE TABLE AccademicYearWeekDetail

INSERT INTO [dbo].[AccademicYearWeekDetail]
           ([AccademicYearId]
           ,[WeekNumber]
           ,[StartDateInt]
           ,[EndDateInt])
SELECT AccademicYearId,WeekNumber,CAST(StartDateINT AS BIGINT),CAST(EndDAteINT AS BIGINT) FROM  @WeekNumberTable


SELECT * FROM AccademicYearWeekDetail WHERE  CAST(CONVERT(VARCHAR(10),GETDATE(), 112) AS BIGINT ) BETWEEN StartDateInt AND EndDateInt 
END




GO
/****** Object:  StoredProcedure [dbo].[uspSaveGroupInvidualMessage]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Sweta Patel>
-- Create date: <15/11/2018>
-- Description:	<Save Group Message Detail school id need to take care later>
-- =============================================

CREATE PROCEDURE [dbo].[uspSaveGroupInvidualMessage]
 @UserIds AS NVARCHAR(MAX)=null,
 @GroupIds AS NVARCHAR(MAX)=null,
 @SchoolId AS BIGINT=0,
 @Title AS NVARCHAR(MAX) ='Group Meeting Alert!',
 @Description AS NVARCHAR(MAX) ='detail',
 @DescriptionText AS NVARCHAR(MAX) =NULL,
 @WeekNo AS BIGINT=0,
 --@MessageCode AS Nvarchar(50)='',
 @SenderId AS BIGINT=0
AS
BEGIN
--DECLARE @UserIds NVARCHAR(MAX) ='49,50'
--DECLARE @GroupIds NVARCHAR(MAX) ='4,8'
--DECLARE @SchoolId BIGINT=2
--DECLARE @Title NVARCHAR(MAX) ='Group Meeting Alert!'
--DECLARE @Description NVARCHAR(MAX) ='Hello all members! Tomorrow  we have arranged meeting for Annual Report. Please be present at 11:30 AM in morning !'
--DECLARE @SenderId BIGINT=1
DECLARE @CreatedDate datetime=GETDATE();

INSERT INTO [dbo].[GroupMessage]
           ([SenderId]
           ,[MessageTitle]
           ,[MessageText]
           ,[SimpleText]
           ,[WeekNumber]
           ,[CreatedDate])
SELECT  @SenderId AS SenderId, @Title AS MessageTitle , @Description AS MessageText,@DescriptionText AS SimpleText
		   ,@WeekNo AS WeekNumbe ,@CreatedDate AS CreatedDate 

DECLARE @GroupMessageId BIGINT=0
SELECT TOP 1 @GroupMessageId=GroupMessageId FROM GroupMessage WHERE CreatedDate=@CreatedDate AND WeekNumber=@WeekNo AND MessageTitle=@Title

	INSERT INTO [dbo].[GroupMessageDetail]
           ([GroupMessageId]
           ,[GroupId]
           ,[UserId]
           ,[SenderId]
           ,[IsRead])

	(	SELECT @GroupMessageId,0 As GroupId,LC.UserId,@SenderId AS SenderId,
				CAST(0 as bit) AS IsRead 
		FROM LoginCredentials LC 
		WHERE LC.IsActive=1 AND LC.IsDelete=0 AND LC.RoleId IN (3,4)
			AND LC.UserId in (SELECT LTRIM(value) FROM dbo.Split(@UserIds,','))
		UNION 
		SELECT @GroupMessageId, GD.GroupId As GroupId,LC.UserId,@SenderId AS SenderId,
				CAST(0 as bit) AS IsRead 
		FROM GroupMaster GM 
		LEFT JOIN GroupDetail GD ON GM.Id=GD.GroupId
		INNER JOIN LoginCredentials LC ON LC.UserId=GD.UserId AND LC.IsActive=1 
			AND LC.IsDelete=0 AND LC.RoleId IN (3,4)
		WHERE GM.IsActive=1 AND GM.IsDelete=0 AND GM.SchoolId =@SchoolId
		AND GM.Id in (SELECT LTRIM(value) FROM dbo.Split(@GroupIds,','))
	)

	;WITH Receiver  AS 
	( 
		SELECT LC.UserId
		FROM LoginCredentials LC 
		WHERE LC.IsActive=1 AND LC.IsDelete=0 AND LC.RoleId IN (3,4)
			AND LC.UserId in (SELECT LTRIM(value) FROM dbo.Split(@UserIds,','))
		UNION 
		SELECT LC.UserId
		FROM GroupMaster GM 
		LEFT JOIN GroupDetail GD ON GM.Id=GD.GroupId
		INNER JOIN LoginCredentials LC ON LC.UserId=GD.UserId AND LC.IsActive=1 
			AND LC.IsDelete=0 AND LC.RoleId IN (3,4)
		WHERE GM.IsActive=1 AND GM.IsDelete=0 AND GM.SchoolId =@SchoolId
		AND GM.Id in (SELECT LTRIM(value) FROM dbo.Split(@GroupIds,','))
	)

	SELECT DI.DeviceToken
		,@Title AS [Title]
		,@Description AS [Description]
		,@DescriptionText AS SimpleText
		,CASE WHEN LC.RoleId = 3 THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS IsParent
		,DI.DeviceType
	FROM DeviceInfo DI
	INNER JOIN LoginCredentials LC ON DI.Mobile = LC.Mobile
	INNER JOIN Receiver R ON R.UserId = LC.UserId
END




GO
/****** Object:  StoredProcedure [dbo].[uspTeacherDashboard]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ankit 
-- Create date: 31/10/2018
-- Description:	<Description,,>
-- =============================================
-- uspTeacherDashboard 48
CREATE PROCEDURE [dbo].[uspTeacherDashboard]
	@teacherId BIGINT = 0
AS
BEGIN
SELECT 
CM.Id AS ClassRoomId,
SM.Id As SectionId,
CM.Name As ClassName,
SM.Name As Section,

(Case when (SM.Image  != null or SM.Image != '') then  'http://ws-srv-net.in.webmyne.com/Applications/SchoolManagement/Uploads/ProfilePic/SectionLogo/'+ CAST(SM.Id AS Nvarchar(MAX)) +'/'+SM.Image else '' end) AS SectionImage
,(SELECT Count(*) FROM StudentSchoolDetail SSD WHERE SSD.ClassId=TD.ClassId AND SSD.SectionId=SM.Id AND SSD.IsCurrent=1 AND SSD.IsActive=1) AS TotalStudent
-- Here Total Student is section wise count 
,(SELECT TOP 1 S.Name FROM SubjectMaster S WHERE S.Id=TD.SubjectId) AS SubjectName
FROM TeacherDetail  TD 
LEFT JOIN SectionMaster SM ON SM.ClassId=TD.ClassId
LEFT JOIN ClassMaster CM ON CM.Id=TD.ClassId
WHERE Td.TeacherId=@teacherId
AND TD.SectionId=(CASE WHEN TD.SectionId!=0 THEN SM.Id  ELSE 0 END)
END




GO
/****** Object:  StoredProcedure [dbo].[uspTeacherStudentClass]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ankit 
-- Create date: 31/10/2018
-- Description:	<Description,,>
-- =============================================
-- uspTeacherStudentClass 48,1
CREATE PROCEDURE [dbo].[uspTeacherStudentClass]
	@teacherId BIGINT = 0,
	@stucentId BIGINT=0
AS
BEGIN
SELECT 
TOP 1
CM.Id AS ClassRoomId,
SM.Id As SectionId,
CM.Name As ClassName,
SM.Name As Section,
(Case when (SM.Image  != null or SM.Image != '') then  'http://ws-srv-net.in.webmyne.com/Applications/SchoolManagement/Uploads/ProfilePic/SectionLogo/'+ CAST(SM.Id AS Nvarchar(MAX)) +'/'+SM.Image else '' end) AS SectionImage
,(SELECT Count(*) FROM StudentSchoolDetail SSD WHERE SSD.ClassId=TD.ClassId AND SSD.SectionId=SM.Id AND SSD.IsCurrent=1 AND SSD.IsActive=1) AS TotalStudent
-- Here Total Student is section wise count 
,(SELECT TOP 1 S.Name FROM SubjectMaster S WHERE S.Id=TD.SubjectId) AS SubjectName
FROM TeacherDetail  TD 
LEFT JOIN SectionMaster SM ON SM.ClassId=TD.ClassId
LEFT JOIN ClassMaster CM ON CM.Id=TD.ClassId
LEFT JOIN StudentSchoolDetail SSD ON SSD.ClassId=TD.ClassId AND SSD.SectionId=SM.Id
WHERE Td.TeacherId=@teacherId
AND  SSD.StudentId=@stucentId
END




GO
/****** Object:  StoredProcedure [dbo].[uspUpdateCurrentStatus]    Script Date: 14-12-2018 14:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:			<Krupali> 
-- Created date:	<27-09-2018>
-- Description:		<Set all Current status as 0>
-- =============================================
-- uspUpdateCurrentStatus 2
CREATE PROCEDURE [dbo].[uspUpdateCurrentStatus]
@StudentId BIGINT = 0
AS
BEGIN
	UPDATE StudentSchoolDetail SET IsCurrent = 0 WHERE StudentId = @StudentId
	END




GO
