USE [DocWare]
GO
/****** Object:  UserDefinedFunction [dbo].[GetCabinetFolderForDocument]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetCabinetFolderForDocument](@DocumentId BIGINT) 
RETURNS
 @CabinetFolderDetail TABLE (
		[CabinetId] BIGINT,
		[FolderId] BIGINT,
		[IsPublic] BIT,
		[FolderIsPublic] BIT
	) 
AS  
BEGIN


	INSERT INTO @CabinetFolderDetail
	SELECT FD.CabinetId AS CabinetId,
	DD.FolderId AS FolderId,
    (SELECT TOP 1 IsPublic FROM DocumentVersionDetail WHERE DocumentId=@DocumentId  AND IsCurrent=1) AS IsPublic
	,FD.IsPublic AS FolderIsPublic
	FROM DocumentDetail  DD
	INNER JOIN FolderDetail FD ON DD.FolderId=FD.Id
	WHERE DD.DocumentId=@DocumentId

RETURN
END


--SELECT * FROM dbo.GetCabinetForDocument(10153)

	--SELECT FD.CabinetId AS CabinetId
	--FROM DocumentDetail  DD
	--INNER JOIN FolderDetail FD ON DD.FolderId=FD.Id
	--WHERE DD.DocumentId=10153


GO
/****** Object:  UserDefinedFunction [dbo].[GetCabinetForDocument]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetCabinetForDocument](@DocumentId BIGINT) 
RETURNS
 @CabinetDetail TABLE (
		[CabinetId] BIGINT
	) 
AS  
BEGIN


	INSERT INTO @CabinetDetail
	SELECT FD.CabinetId AS CabinetId
	FROM DocumentDetail  DD
	INNER JOIN FolderDetail FD ON DD.FolderId=FD.Id
	WHERE DD.DocumentId=@DocumentId

RETURN
END


--SELECT * FROM dbo.GetCabinetForDocument(10153)

	--SELECT FD.CabinetId AS CabinetId
	--FROM DocumentDetail  DD
	--INNER JOIN FolderDetail FD ON DD.FolderId=FD.Id
	--WHERE DD.DocumentId=10153

GO
/****** Object:  UserDefinedFunction [dbo].[GetFavoriteFolderItemsCount]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetFavoriteFolderItemsCount](@SelectedFolder NVARCHAR(MAX),@CreadtedBy BIGINT,@SelectedFiles NVARCHAR(MAX),@CabinetId BIGINT) 
RETURNS
	@CountItems TABLE (
		[TotalFolders] BIGINT,
		[TotalFiles] BIGINT,
		[TotalItems] BIGINT,
		[DisplaySize] NVARCHAR(MAX),
		[TotalSize] DECIMAL(18,2)		
	) 
AS  
BEGIN

	DECLARE @ChildFolderCount INT = 0;
	DECLARE @ChildFileCount INT = 0;  
	DECLARE @ChildCount INT = 0;
	DECLARE @SelectedFileCount INT = 0;  
	DECLARE @SelectedFileSize DECIMAL(18,2) = 0;  
	DECLARE @FileSize DECIMAL(18,2) = 0;    
	DECLARE @FileSizeUnit VARCHAR(MAX)='';

	IF(@SelectedFiles='' AND  @SelectedFolder='')
	BEGIN
		SELECT @ChildFolderCount=Count(*) FROM Favorite FV
		INNER JOIN FolderDetail FD On FD.Id=FV.FolderId
		WHERE UserId=@CreadtedBy AND FolderId>0 AND FV.CabinetId=@CabinetId AND FD.IsActive=1 AND Fd.IsDelete=0

		SELECT @ChildFileCount=Count(*) FROM Favorite FV
		INNER JOIN DocumentDetail DD On DD.DocumentId=FV.DocumentId
		WHERE UserId=@CreadtedBy AND FV.DocumentId>0 AND FV.CabinetId=@CabinetId AND DD.IsActive=1 AND DD.IsDelete=0

		SET @ChildCount = @ChildFolderCount + @ChildFileCount

		SELECT @FileSize= SUM(DocumentSize) 
		FROM DocumentVersionDetail 
		WHERE  IsActive = 1 AND IsDelete = 0
			AND  ID IN (SELECT MAX(Id) AS DocumentVersionId FROM DocumentVersionDetail 
						GROUP BY DocumentId
						Having DocumentId 
						IN (SELECT FV.DocumentId 
							FROM Favorite FV
							INNER JOIN DocumentDetail DD On DD.DocumentId=FV.DocumentId
							WHERE UserId=@CreadtedBy AND FV.DocumentId>0 AND FV.CabinetId=@CabinetId 
								AND DD.IsActive=1 AND DD.IsDelete=0
							)
						)
	END
	ELSE
	BEGIN
		DECLARE @ParentFolders TABLE(
			FolderId BIGINT
		)             
	
		;WITH HierarchyOFFile(FolderId, Name, LevelNumber,ParentId)
		AS	
		(
		    SELECT Id, Name, 1, ParentFolderId
		    FROM FolderDetail AS FirtGeneration
		    WHERE Id in (SELECT LTRIM(value) FROM dbo.Split(@SelectedFolder,','))
		    
			UNION ALL
		    
			SELECT NextGeneration.Id, NextGeneration.Name, Parent.LevelNumber + 1, NextGeneration.ParentFolderId
		    FROM FolderDetail AS NextGeneration
		    INNER JOIN HierarchyOFFile AS Parent ON NextGeneration.ParentFolderId = Parent.FolderId 
		    WHERE NextGeneration.IsActive=1 AND NextGeneration.IsDelete=0       
		)
	
		INSERT INTO @ParentFolders
		SELECT FolderId
		FROM HierarchyOFFile 
	
		SELECT @ChildFolderCount=Count(*) FROM Favorite FV
		INNER JOIN FolderDetail FD On FD.Id=FV.FolderId
		WHERE UserId=@CreadtedBy AND FolderId>0 AND FV.CabinetId=@CabinetId AND FD.IsActive=1 AND Fd.IsDelete=0
			AND FD.ParentFolderId IN (SELECT * FROM @ParentFolders )
		
		SELECT @ChildFileCount=Count(*) FROM Favorite FV
		INNER JOIN DocumentDetail DD On DD.DocumentId=FV.DocumentId
		WHERE UserId=@CreadtedBy AND FV.DocumentId>0 AND FV.CabinetId=@CabinetId AND DD.IsActive=1 AND DD.IsDelete=0
			AND DD.FolderId IN (SELECT * FROM @ParentFolders)
	
		SET @ChildCount = @ChildFolderCount + @ChildFileCount

		SELECT @FileSize= SUM(DocumentSize) 
		FROM DocumentVersionDetail 
		WHERE  IsActive = 1 AND IsDelete = 0
			AND  ID IN (SELECT MAX(Id) AS DocumentVersionId FROM DocumentVersionDetail 
						GROUP BY DocumentId
						Having DocumentId 
						IN (SELECT FV.DocumentId 
							FROM Favorite FV
							INNER JOIN DocumentDetail DD On DD.DocumentId=FV.DocumentId
							WHERE UserId=@CreadtedBy AND FV.DocumentId>0 AND FV.CabinetId=@CabinetId 
								AND DD.IsActive=1 AND DD.IsDelete=0 AND DD.FolderId IN (SELECT * FROM @ParentFolders)
							)
						)
	
		SELECT @SelectedFileCount=Count(*) 
		FROM DocumentDetail 
		WHERE  IsActive = 1 AND IsDelete = 0 AND DocumentId IN (SELECT LTRIM(value) FROM dbo.Split(@SelectedFiles,',')) 
	
		IF @SelectedFileCount>0 
		BEGIN
			SELECT @SelectedFileSize= SUM(DocumentSize)
			FROM DocumentVersionDetail 
			WHERE IsActive = 1 AND IsDelete = 0
				AND ID IN (SELECT MAX(Id) AS DocumentVersionId 
								FROM DocumentVersionDetail 
								GROUP BY DocumentId
								Having DocumentId IN (SELECT DocumentId FROM DocumentDetail 
															WHERE  IsActive = 1 AND IsDelete = 0 
																AND DocumentId IN (SELECT LTRIM(value) 
																						FROM dbo.Split(@SelectedFiles,',')
																				  )
													 )
						  )
		END
	 END

	INSERT INTO  @CountItems SELECT @ChildFolderCount  AS 'TotalFolders', 
	@ChildFileCount+@SelectedFileCount  AS 'TotalFiles',
	@ChildCount+@SelectedFileCount  AS 'TotalItems',	
	dbo.GetSizeWithUnit( ISNULL(@FileSize,0)+ISNULL(@SelectedFileSize,0)) as [DisplaySize],
	ISNULL(@FileSize,0)+ISNULL(@SelectedFileSize,0) AS 'TotalSize'

	RETURN
END




	

	--select * FROM GetFavoriteFolderItemsCount('',3,'',1)
	--select * FROM GetFavoriteFolderItemsCount('10125',3,'',1)
	--select * FROM GetFavoriteFolderItemsCount('10125',3,'10253',1)










GO
/****** Object:  UserDefinedFunction [dbo].[GetFolderItemsCount]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetFolderItemsCount](@SelectedFolder NVARCHAR(MAX),@CreadtedBy BIGINT,@SelectedFiles NVARCHAR(MAX)) 
RETURNS
 @CountItems TABLE (
		[TotalFolders] BIGINT,
		[TotalFiles] BIGINT,
		[TotalItems] BIGINT,
		[DisplaySize] NVARCHAR(MAX),
		[TotalSize] DECIMAL(18,2)
		
	) 
AS  
BEGIN

DECLARE @ChildFolderCount INT = 0;
DECLARE @ChildFileCount INT = 0;  
DECLARE @ChildCount INT = 0;
DECLARE @SelectedFileCount INT = 0;  
DECLARE @SelectedFileSize DECIMAL(18,2) = 0;  
DECLARE @FileSize DECIMAL(18,2) = 0;    
DECLARE @FileSizeUnit VARCHAR(MAX)='';


DECLARE @ParentFolders TABLE(
    FolderId BIGINT
)             

;WITH HierarchyOFFile(FolderId, Name, LevelNumber,ParentId)
AS

(
    SELECT Id, Name, 1, ParentFolderId
        FROM FolderDetail AS FirtGeneration
        WHERE Id in (SELECT LTRIM(value) FROM dbo.Split(@SelectedFolder,','))
    UNION ALL
    SELECT NextGeneration.Id, NextGeneration.Name, Parent.LevelNumber + 1, NextGeneration.ParentFolderId
        FROM FolderDetail AS NextGeneration
        INNER JOIN HierarchyOFFile AS Parent ON NextGeneration.ParentFolderId = Parent.FolderId 
               WHERE NextGeneration.IsActive=1 AND NextGeneration.IsDelete=0
			   -- AND NextGeneration.CreatedBy=@CreadtedBy 
       
)
INSERT INTO @ParentFolders
SELECT FolderId
FROM HierarchyOFFile
--SELECT *  FROM @ParentFolders   

    SELECT @ChildFolderCount =Count(*) FROM FolderDetail WHERE ParentFolderId IN (SELECT *
    FROM @ParentFolders )  AND IsActive = 1 AND IsDelete = 0 --AND CreatedBy = @CreadtedBy
    SELECT @ChildFileCount =Count(*)  FROM DocumentDetail WHERE FolderId IN (SELECT *
    FROM @ParentFolders)AND IsActive = 1 AND IsDelete = 0 --AND CreatedBy = @CreadtedBy 
       SET @ChildCount = @ChildFolderCount +@ChildFileCount ;
       

SELECT @FileSize= SUM(DocumentSize) FROM DocumentVersionDetail WHERE  IsActive = 1 AND IsDelete = 0 --AND CreatedBy = @CreadtedBy 
AND  ID IN (SELECT MAX(Id) AS DocumentVersionId FROM DocumentVersionDetail 
			GROUP BY DocumentId
			Having DocumentId 
IN (SELECT DocumentId FROM DocumentDetail WHERE  IsActive = 1 AND IsDelete = 0
--AND CreatedBy = @CreadtedBy  
AND FolderId 
IN (SELECT * FROM @ParentFolders )))



-- // If (Multiple/single) files or Folders are selected than 
 
SELECT @SelectedFileCount=Count(*) 
FROM DocumentDetail 
WHERE  IsActive = 1 AND IsDelete = 0 AND DocumentId 
IN (SELECT LTRIM(value) FROM dbo.Split(@SelectedFiles,',')) 

IF @SelectedFileCount>0 
BEGIN
SELECT @SelectedFileSize= SUM(DocumentSize)
   FROM DocumentVersionDetail WHERE IsActive = 1 AND IsDelete = 0
									AND ID IN (SELECT MAX(Id) AS DocumentVersionId 
												FROM DocumentVersionDetail 
												GROUP BY DocumentId
												Having DocumentId 
											IN (SELECT DocumentId 
												FROM DocumentDetail 
												WHERE  IsActive = 1 AND IsDelete = 0 AND DocumentId 
											IN (SELECT LTRIM(value) FROM dbo.Split(@SelectedFiles,','))))
END

--// -----------------------------------------------------


 -- SELECT @ChildFolderCount  AS 'F_TotalFolders',@ChildFileCount  AS 'F_TotalFiles',@ChildCount  AS 'F_TotalItems', @FileSize AS 'F_FileSize', dbo.GetSizeWithUnit(@FileSize) as [F_TotalSizeWithUnit]
 -- SELECT @SelectedFileCount AS 'SelectedFileCount', @SelectedFileSize AS 'SelectedFileSize',  dbo.GetSizeWithUnit(@SelectedFileSize) as [SelectedFileSizeUnit]
 -- SELECT @ChildFolderCount AS 'Folders',   @ChildFileCount+@SelectedFileCount AS 'Files',   ISNULL(@FileSize,0)+ISNULL(@SelectedFileSize,0) AS 'TotalSize',   dbo.GetSizeWithUnit( ISNULL(@FileSize,0)+ISNULL(@SelectedFileSize,0)) as [DisplaySize]     
 -------------------
	
	INSERT INTO  @CountItems SELECT @ChildFolderCount  AS 'TotalFolders', 
	@ChildFileCount+@SelectedFileCount  AS 'TotalFiles',
	@ChildCount  AS 'TotalItems',	
	dbo.GetSizeWithUnit( ISNULL(@FileSize,0)+ISNULL(@SelectedFileSize,0)) as [DisplaySize],
	ISNULL(@FileSize,0)+ISNULL(@SelectedFileSize,0) AS 'TotalSize'
	RETURN
	END


--SELECT * FROM dbo.GetFolderItemsCount('10099,10100',10048,'')
--SELECT * FROM dbo.GetFolderItemsCount('43',3,'')
--SELECT * FROM dbo.GetFolderItemsCount('57',10048,'')
--SELECT * FROM dbo.GetFolderItemsCount('10099,10100',10048,'10129,10130')
--SELECT * FROM dbo.GetFolderItemsCount('',10048,'10128')
--SELECT * FROM dbo.GetFolderItemsCount('52',7,'')
GO
/****** Object:  UserDefinedFunction [dbo].[GetFullPath]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetFullPath](@ItemId BIGINT,@Type VARCHAR(10),@CabinetId BIGINT,@ShareWith BIGINT=0)  
RETURNS VARCHAR(MAX)   
AS   

BEGIN 
DECLARE @FolderId BIGINT=0;
DECLARE @fullPath VARCHAR(MAX)='' ; 
DECLARE @folderPath  VARCHAR(MAX)='' ; 
DECLARE @ReversefolderPath  VARCHAR(MAX)='' ; 
DECLARE @Cname AS NVARCHAR(MAX)='';
SELECT  @Cname= CM.Name FROM CabinetMaster CM WHERE CM.Id=@CabinetId 

SET @FolderId=@ItemId;
IF(@Type='D')
BEGIN
SELECT @FolderId=FolderId  FROM DocumentDetail WHERE DocumentId=@ItemId
END

 SELECT @folderPath= dbo.GetParentName(@FolderId)

 -------------------------Reverse Path------------------------------------
DECLARE @input VARCHAR(MAX)
DECLARE @output VARCHAR(MAX)
DECLARE @lenght INT

SET @input = @folderPath
SET @output = ''

WHILE LEN(@input) > 0
BEGIN
    IF CHARINDEX('/', @input) > 0
    BEGIN
        SET @output = SUBSTRING(@input,0,CHARINDEX('/', @input)) + '/' + @output
        SET @input = LTRIM(RTRIM(SUBSTRING(@input,CHARINDEX('/', @input)+1,LEN(@input))))
    END
    ELSE
    BEGIN
        SET @output = @input + '/' + @output
        SET @input = ''
    END
END

SELECT @ReversefolderPath= substring(@output, 1, (len(@output) - 2))
 -------------------------Reverse Path End--------------------------------

 SET @fullPath=@Cname+'/'+@ReversefolderPath; 

 IF (@ShareWith>0)
 BEGIN
 SET @fullPath=@Cname+'/Share With Me/'+@ReversefolderPath;  
 END
  
  RETURN @fullPath;  
END; 


--SELECT [dbo].[GetFullPath](20166,'F',10)
GO
/****** Object:  UserDefinedFunction [dbo].[GetParentDocumentId]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetParentDocumentId](@DocumentId BIGINT) 
RETURNS
 @CabinetFolderDetail TABLE (
		[DocumentId] BIGINT
	) 
AS  
BEGIN

INSERT INTO @CabinetFolderDetail
SELECT LD.ParentId FROM LinkDocument LD   WHERE ChildId=@DocumentId

RETURN
END


--SELECT * FROM dbo.GetParentDocumentId(40514)




GO
/****** Object:  UserDefinedFunction [dbo].[GetParentName]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetParentName](@FolderId BIGINT)  
RETURNS VARCHAR(MAX)   
AS   
-- Returns the stock level for the product.  
BEGIN 


DECLARE @ret VARCHAR(MAX)='' ; 
DECLARE @name VARCHAR(MAX) ; 
DECLARE @Parent BIGINT=0;

SELECT  @Parent=FD.ParentFolderId , @name= FD.Name  FROM  FolderDetail FD WHERE FD.Id= @FolderId
SET @ret= @ret +'/'+ @name;

IF(@Parent!=0)
BEGIN

SELECT @ret= @ret + [dbo].[GetParentName](@Parent)

SELECT  @Parent=FD.ParentFolderId , @name= FD.Name FROM  FolderDetail FD WHERE FD.Id=@Parent
END
  
--IF(@Parent=0)
--BEGIN
----DECLARE @Cname AS NVARCHAR(MAX)='';
----SELECT @Cname=+'/'+ CM.Name FROM CabinetMaster CM WHERE CM.Id=@CabinetId 
--RETURN @ret;  
--END

    RETURN @ret;  
END; 



 --SELECT [dbo].[GetParentName](20166)
GO
/****** Object:  UserDefinedFunction [dbo].[GetPublicFolderItemsCount]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetPublicFolderItemsCount](@SelectedFolder NVARCHAR(MAX),@CreadtedBy BIGINT,@SelectedFiles NVARCHAR(MAX),@CabinetId BIGINT)  
RETURNS
 @CountItems TABLE (
		[TotalFolders] BIGINT,
		[TotalFiles] BIGINT,
		[TotalItems] BIGINT,
		[DisplaySize] NVARCHAR(MAX),
		[TotalSize] DECIMAL(18,2)
		
	) 
AS  
BEGIN

DECLARE @ChildFolderCount INT = 0;
DECLARE @ChildFileCount INT = 0;  
DECLARE @ChildCount INT = 0;
DECLARE @SelectedFileCount INT = 0;  
DECLARE @SelectedFileSize DECIMAL(18,2) = 0;  
DECLARE @FileSize DECIMAL(18,2) = 0;    
DECLARE @FileSizeUnit VARCHAR(MAX)='';

	IF(@SelectedFiles='' AND  @SelectedFolder='')
	BEGIN

	SELECT @ChildFolderCount=Count(*) FROM FolderDetail FD WHERE FD.CabinetId=@CabinetId AND FD.IsActive=1 AND FD.IsDelete=0  AND FD.IsPublic=1
	SELECT @SelectedFileCount= Count(*) FROM DocumentDetail DD WHERE   @CabinetId=(Select CabinetId From dbo.GetCabinetFolderForDocument(DD.DocumentId))
		AND (Select IsPublic From dbo.GetCabinetFolderForDocument(DD.DocumentId))=1

   SELECT @FileSize= SUM(DocumentSize)
   FROM DocumentDetail DD 
	   INNER JOIN FolderDetail FD ON FD.Id=DD.FolderId
	   INNER JOIN (SELECT RANK() OVER (PARTITION BY DocumentId ORDER BY Id DESC) r, *
   FROM DocumentVersionDetail) DV ON DD.DocumentId = DV.DocumentId
   WHERE DV.r = 1  AND DV.IsActive = 1 AND DV.IsDelete = 0
		AND FD.IsActive=1 AND FD.IsDelete=0 AND FD.CabinetId=@CabinetId
		AND DV.IsPublic=1 AND DD.IsActive = 1 AND DD.IsDelete = 0

		 SET @ChildCount = @ChildFolderCount +@SelectedFileCount ; -- // Not sure For this case
END
ELSE
BEGIN


DECLARE @ParentFolders TABLE(
    FolderId BIGINT
)             

;WITH HierarchyOFFile(FolderId, Name, LevelNumber,ParentId)
AS

(
    SELECT Id, Name, 1, ParentFolderId
        FROM FolderDetail AS FirtGeneration
        WHERE Id in (SELECT LTRIM(value) FROM dbo.Split(@SelectedFolder,','))
    UNION ALL
    SELECT NextGeneration.Id, NextGeneration.Name, Parent.LevelNumber + 1, NextGeneration.ParentFolderId
        FROM FolderDetail AS NextGeneration
        INNER JOIN HierarchyOFFile AS Parent ON NextGeneration.ParentFolderId = Parent.FolderId 
               WHERE NextGeneration.IsActive=1 AND NextGeneration.IsDelete=0 
			   -- AND NextGeneration.CreatedBy=@CreadtedBy 
       
)
INSERT INTO @ParentFolders
SELECT FolderId
FROM HierarchyOFFile
--SELECT *  FROM @ParentFolders   

    SELECT @ChildFolderCount =Count(*) FROM FolderDetail WHERE ParentFolderId IN (SELECT *
    FROM @ParentFolders )  AND IsActive = 1 AND IsDelete = 0 AND IsPublic = 1
    SELECT @ChildFileCount =Count(*)  FROM DocumentDetail DD WHERE DD.FolderId IN (SELECT *
    FROM @ParentFolders)AND DD.IsActive = 1 AND DD.IsDelete = 0  AND 1 =(SELECT TOP 1 IsPublic FROM DocumentVersionDetail DVD  WHERE DVD.DocumentId= DD.DocumentId ORDER BY Id DESC)
       SET @ChildCount = @ChildFolderCount +@ChildFileCount ;
   

SELECT @FileSize= SUM(DocumentSize) FROM DocumentVersionDetail WHERE  IsActive = 1 AND IsDelete = 0 AND  IsPublic =1 
AND  ID IN (SELECT MAX(Id) AS DocumentVersionId FROM DocumentVersionDetail 
			GROUP BY DocumentId
			Having
			
			  DocumentId 
IN (SELECT DocumentId FROM DocumentDetail WHERE  IsActive = 1 AND IsDelete = 0
AND FolderId 
IN (SELECT * FROM @ParentFolders )))



-- // If (Multiple/single) files or Folders are selected than 
 
SELECT @SelectedFileCount=Count(*) 
FROM DocumentDetail 
WHERE  IsActive = 1 AND IsDelete = 0 AND DocumentId 
IN (SELECT LTRIM(value) FROM dbo.Split(@SelectedFiles,',')) 

IF @SelectedFileCount>0 
BEGIN
SELECT @SelectedFileSize= SUM(DocumentSize)
   FROM DocumentVersionDetail WHERE IsActive = 1 AND IsDelete = 0 	AND IsPublic=1 
									AND ID IN (SELECT MAX(Id) AS DocumentVersionId 
												FROM DocumentVersionDetail 
												GROUP BY DocumentId
												Having
											
												 DocumentId 
											IN (SELECT DocumentId 
												FROM DocumentDetail 
												WHERE  IsActive = 1 AND IsDelete = 0 AND DocumentId 
											IN (SELECT LTRIM(value) FROM dbo.Split(@SelectedFiles,','))))
END

--// -----------------------------------------------------

END --// End of Else Part 	

	
	INSERT INTO  @CountItems SELECT @ChildFolderCount  AS 'TotalFolders', 
	@ChildFileCount+@SelectedFileCount  AS 'TotalFiles',
	@ChildCount  AS 'TotalItems',	
	dbo.GetSizeWithUnit( ISNULL(@FileSize,0)+ISNULL(@SelectedFileSize,0)) as [DisplaySize],
	ISNULL(@FileSize,0)+ISNULL(@SelectedFileSize,0) AS 'TotalSize'
	RETURN
	END



--SELECT * FROM dbo.GetPublicFolderItemsCount('',10048,'',20)

GO
/****** Object:  UserDefinedFunction [dbo].[GetSharedFolderItemsCount]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetSharedFolderItemsCount](@SelectedFolder NVARCHAR(MAX),@CreadtedBy BIGINT,@SelectedFiles NVARCHAR(MAX),@CabinetId BIGINT) 
RETURNS
	@CountItems TABLE (
		[TotalFolders] BIGINT,
		[TotalFiles] BIGINT,
		[TotalItems] BIGINT,
		[DisplaySize] NVARCHAR(MAX),
		[TotalSize] DECIMAL(18,2)		
	) 
AS  
BEGIN

	DECLARE @ChildFolderCount INT = 0;
	DECLARE @ChildFileCount INT = 0;  
	DECLARE @ChildCount INT = 0;
	DECLARE @SelectedFileCount INT = 0;  
	DECLARE @SelectedFileSize DECIMAL(18,2) = 0;  
	DECLARE @FileSize DECIMAL(18,2) = 0;    
	DECLARE @FileSizeUnit VARCHAR(MAX)='';

	IF(@SelectedFiles='' AND  @SelectedFolder='')
	BEGIN
		SELECT @ChildFolderCount=Count(*) FROM FolderDetail FD
		INNER JOIN UserDocShareDetail UDSD ON UDSD.FolderId = FD.Id
		LEFT JOIN GroupDetail GD on UDSD.DocShareGroupId = GD.Id AND GD.IsActive = 1 AND GD.IsDelete = 0
		LEFT JOIN GroupUserDetail GUD on GD.Id = GUD.GroupId
		WHERE (UDSD.DocShareUserId = @CreadtedBy OR GUD.UserId = @CreadtedBy) 
			AND UDSD.FolderId>0 AND UDSD.CabinetId=@CabinetId AND FD.IsActive=1 AND Fd.IsDelete=0

		SELECT @ChildFileCount=Count(*) FROM DocumentDetail DD 
		INNER JOIN UserDocShareDetail UDSD ON UDSD.DocumentId = DD.DocumentId
		LEFT JOIN GroupDetail GD on UDSD.DocShareGroupId = GD.Id AND GD.IsActive = 1 AND GD.IsDelete = 0
		LEFT JOIN GroupUserDetail GUD on GD.Id = GUD.GroupId
		WHERE (UDSD.DocShareUserId = @CreadtedBy OR GUD.UserId = @CreadtedBy) 
			AND UDSD.DocumentId>0 AND UDSD.CabinetId=@CabinetId AND DD.IsActive=1 AND DD.IsDelete=0

		SET @ChildCount = @ChildFolderCount + @ChildFileCount

		SELECT @FileSize= SUM(DocumentSize) 
		FROM DocumentVersionDetail 
		WHERE  IsActive = 1 AND IsDelete = 0
			AND  ID IN (SELECT MAX(Id) AS DocumentVersionId FROM DocumentVersionDetail 
						GROUP BY DocumentId
						Having DocumentId 
						IN (SELECT UDSD.DocumentId 
							FROM DocumentDetail DD 
							INNER JOIN UserDocShareDetail UDSD ON UDSD.DocumentId = DD.DocumentId
							LEFT JOIN GroupDetail GD on UDSD.DocShareGroupId = GD.Id AND GD.IsActive = 1 AND GD.IsDelete = 0
							LEFT JOIN GroupUserDetail GUD on GD.Id = GUD.GroupId
							WHERE (UDSD.DocShareUserId = @CreadtedBy OR GUD.UserId = @CreadtedBy) 
								AND UDSD.DocumentId>0 AND UDSD.CabinetId=@CabinetId 
								AND DD.IsActive=1 AND DD.IsDelete=0
							)
						)
	END
	ELSE
	BEGIN
		DECLARE @ParentFolders TABLE(
			FolderId BIGINT
		)             
	
		;WITH HierarchyOFFile(FolderId, Name, LevelNumber,ParentId)
		AS	
		(
		    SELECT Id, Name, 1, ParentFolderId
		    FROM FolderDetail AS FirtGeneration
		    WHERE Id in (SELECT LTRIM(value) FROM dbo.Split(@SelectedFolder,','))
		    
			UNION ALL
		    
			SELECT NextGeneration.Id, NextGeneration.Name, Parent.LevelNumber + 1, NextGeneration.ParentFolderId
		    FROM FolderDetail AS NextGeneration
		    INNER JOIN HierarchyOFFile AS Parent ON NextGeneration.ParentFolderId = Parent.FolderId 
		    WHERE NextGeneration.IsActive=1 AND NextGeneration.IsDelete=0       
		)
	
		INSERT INTO @ParentFolders
		SELECT FolderId
		FROM HierarchyOFFile 

		SELECT @ChildFolderCount=Count(*) FROM FolderDetail FD
		INNER JOIN UserDocShareDetail UDSD ON UDSD.FolderId = FD.Id
		LEFT JOIN GroupDetail GD on UDSD.DocShareGroupId = GD.Id AND GD.IsActive = 1 AND GD.IsDelete = 0
		LEFT JOIN GroupUserDetail GUD on GD.Id = GUD.GroupId
		WHERE (UDSD.DocShareUserId = @CreadtedBy OR GUD.UserId = @CreadtedBy) 
			AND UDSD.FolderId>0 AND UDSD.CabinetId=@CabinetId AND FD.IsActive=1 AND Fd.IsDelete=0
			AND FD.ParentFolderId IN (SELECT * FROM @ParentFolders )

		SELECT @ChildFileCount=Count(*) FROM DocumentDetail DD 
		INNER JOIN UserDocShareDetail UDSD ON UDSD.DocumentId = DD.DocumentId
		LEFT JOIN GroupDetail GD on UDSD.DocShareGroupId = GD.Id AND GD.IsActive = 1 AND GD.IsDelete = 0
		LEFT JOIN GroupUserDetail GUD on GD.Id = GUD.GroupId
		WHERE (UDSD.DocShareUserId = @CreadtedBy OR GUD.UserId = @CreadtedBy) 
			AND UDSD.DocumentId>0 AND UDSD.CabinetId=@CabinetId AND DD.IsActive=1 AND DD.IsDelete=0
			AND DD.FolderId IN (SELECT * FROM @ParentFolders)
	
		SET @ChildCount = @ChildFolderCount + @ChildFileCount

		SELECT @FileSize= SUM(DocumentSize) 
		FROM DocumentVersionDetail 
		WHERE  IsActive = 1 AND IsDelete = 0
			AND  ID IN (SELECT MAX(Id) AS DocumentVersionId FROM DocumentVersionDetail 
						GROUP BY DocumentId
						Having DocumentId 
						IN (SELECT UDSD.DocumentId 
							FROM DocumentDetail DD 
							INNER JOIN UserDocShareDetail UDSD ON UDSD.DocumentId = DD.DocumentId
							LEFT JOIN GroupDetail GD on UDSD.DocShareGroupId = GD.Id AND GD.IsActive = 1 AND GD.IsDelete = 0
							LEFT JOIN GroupUserDetail GUD on GD.Id = GUD.GroupId
							WHERE (UDSD.DocShareUserId = @CreadtedBy OR GUD.UserId = @CreadtedBy) 
								AND UDSD.DocumentId>0 AND UDSD.CabinetId=@CabinetId 
								AND DD.IsActive=1 AND DD.IsDelete=0
								AND DD.FolderId IN (SELECT * FROM @ParentFolders)
							)
						)
	
		SELECT @SelectedFileCount=Count(*) 
		FROM DocumentDetail 
		WHERE  IsActive = 1 AND IsDelete = 0 AND DocumentId IN (SELECT LTRIM(value) FROM dbo.Split(@SelectedFiles,',')) 
	
		IF @SelectedFileCount>0 
		BEGIN
			SELECT @SelectedFileSize= SUM(DocumentSize)
			FROM DocumentVersionDetail 
			WHERE IsActive = 1 AND IsDelete = 0
				AND ID IN (SELECT MAX(Id) AS DocumentVersionId 
								FROM DocumentVersionDetail 
								GROUP BY DocumentId
								Having DocumentId IN (SELECT DocumentId FROM DocumentDetail 
															WHERE  IsActive = 1 AND IsDelete = 0 
																AND DocumentId IN (SELECT LTRIM(value) 
																						FROM dbo.Split(@SelectedFiles,',')
																				  )
													 )
						  )
		END
	 END

	INSERT INTO  @CountItems SELECT @ChildFolderCount  AS 'TotalFolders', 
	@ChildFileCount+@SelectedFileCount  AS 'TotalFiles',
	@ChildCount+@SelectedFileCount  AS 'TotalItems',	
	dbo.GetSizeWithUnit( ISNULL(@FileSize,0)+ISNULL(@SelectedFileSize,0)) as [DisplaySize],
	ISNULL(@FileSize,0)+ISNULL(@SelectedFileSize,0) AS 'TotalSize'

	RETURN
END




	

	--select * FROM GetSharedFolderItemsCount('',3,'',1)
	--select * FROM GetSharedFolderItemsCount('10125',3,'',1)
	--select * FROM GetSharedFolderItemsCount('10125',3,'10253',1)










GO
/****** Object:  UserDefinedFunction [dbo].[GetSizeWithUnit]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetSizeWithUnit](@DocumentSize DECIMAL(18,2))  
RETURNS VARCHAR(MAX)   
AS   
-- Returns the stock level for the product.  
BEGIN 

DECLARE @ret VARCHAR(MAX) ;  


SELECT @ret=(SELECT CASE
 WHEN (ROUND(@DocumentSize, 2)  > 1024) THEN  CONVERT(VARCHAR(MAX) , FORMAT((@DocumentSize/ 1024), 'N2')) + ' MB'
 WHEN (ROUND((@DocumentSize /1024), 2) >   1024) THEN  CONVERT(VARCHAR(MAX) ,FORMAT(((@DocumentSize/ 1024) /1024), 'N2')) + ' GB'
 WHEN (ROUND((@DocumentSize/ 1024)/1024, 2) > 1024) THEN CONVERT(VARCHAR(MAX) ,FORMAT(((@DocumentSize / 1024) /1024)/1024, 'N2')) + ' TB'
ELSE CONVERT(VARCHAR(MAX) , FORMAT((@DocumentSize), 'N2')) + ' KB'

 END) 
  
    RETURN @ret;  
END; 
GO
/****** Object:  UserDefinedFunction [dbo].[Split]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[Split] 
(
@String nvarchar (4000),
 @Delimiter nvarchar (10)
 )
 

returns @ValueTable table ([ID] int,[Value] nvarchar(4000))
begin
 declare @NextString nvarchar(4000)
 declare @Pos int
 declare @NextPos int
 declare @CommaCheck nvarchar(1)
 
 --Initialize
 set @NextString = ''
 set @CommaCheck = right(@String,1) 
 
 --Check for trailing Comma, if not exists, INSERT
 --if (@CommaCheck <> @Delimiter )
 set @String = @String + @Delimiter
 
 --Get position of first Comma
 set @Pos = charindex(@Delimiter,@String)
 set @NextPos = 1
 
 --Loop while there is still a comma in the String of levels
Declare @sr as int
set @sr =0
 while (@pos <>  0)  
 begin
  set @NextString = substring(@String,1,@Pos - 1)
  set @sr = @sr + 1
  insert into @ValueTable ([id], [Value]) Values (@sr , @NextString)
 
  set @String = substring(@String,@pos +1,len(@String))
  
  set @NextPos = @Pos
  set @pos  = charindex(@Delimiter,@String)
 end
 
 return
end



GO
/****** Object:  Table [dbo].[ApplicationMaster]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicationMaster](
	[ApplicationId] [tinyint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
 CONSTRAINT [PK_ApplicationMaster] PRIMARY KEY CLUSTERED 
(
	[ApplicationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AssignedFormControlProperty]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AssignedFormControlProperty](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[AssignedFormTemplateId] [bigint] NOT NULL,
	[FieldId] [nvarchar](50) NOT NULL,
	[FieldType] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_AssignedFormControlProperty] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AssignFormTemplate]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AssignFormTemplate](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[FormTitle] [nvarchar](150) NOT NULL,
	[FormContant] [nvarchar](max) NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_AssignFormTemplateDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AuditrialLog]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AuditrialLog](
	[AuidtrailLogId] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[LogTypeId] [bigint] NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[IPAddress] [nvarchar](50) NOT NULL,
	[Browser] [nvarchar](150) NOT NULL,
	[Version] [nvarchar](50) NOT NULL,
	[ApplicationId] [tinyint] NOT NULL,
	[IsMobile] [bit] NOT NULL,
	[AuditDate] [datetime] NOT NULL,
	[AuditDateInt]  AS ((datepart(year,[AuditDate])*(10000)+datepart(month,[AuditDate])*(100))+datepart(day,[AuditDate])),
 CONSTRAINT [PK_AuidtrailLog] PRIMARY KEY CLUSTERED 
(
	[AuidtrailLogId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CabinetMaster]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CabinetMaster](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UDID] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[Notes] [nvarchar](max) NOT NULL,
	[Keywords] [nvarchar](max) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_CabinetMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CategoryMaster]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CategoryMaster](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_CategoryMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DeletedData]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DeletedData](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[FolderId] [bigint] NOT NULL,
	[DocumentId] [bigint] NOT NULL,
	[CabinetId] [bigint] NOT NULL,
	[DisplayPath] [nvarchar](max) NULL,
	[IsRecycleBinDeleted] [bit] NOT NULL,
	[DeletedBy] [nvarchar](max) NOT NULL,
	[DeletedDate] [datetime] NOT NULL,
	[DeletedDateInt]  AS ((datepart(year,[DeletedDate])*(10000)+datepart(month,[DeletedDate])*(100))+datepart(day,[DeletedDate])),
	[OwnerId] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_DeletedData] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DepartmentMaster]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DepartmentMaster](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[DepartmentManagerId] [bigint] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_DepartmentMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DocumentDetail]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DocumentDetail](
	[DocumentId] [bigint] IDENTITY(1,1) NOT NULL,
	[UDID] [nvarchar](50) NOT NULL,
	[FolderId] [bigint] NOT NULL,
	[AssignFormTemplateId] [bigint] NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[CategoryId] [bigint] NOT NULL,
	[SubCategoryId] [bigint] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_DocumentDetail] PRIMARY KEY CLUSTERED 
(
	[DocumentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DocumentFormTemplateValue]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DocumentFormTemplateValue](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[AssignFormTemplateId] [bigint] NOT NULL,
	[AssignedFormControlId] [bigint] NOT NULL,
	[Value] [nvarchar](max) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[FilledDate] [datetime] NOT NULL,
	[FilledDateInt]  AS ((datepart(year,[FilledDate])*(10000)+datepart(month,[FilledDate])*(100))+datepart(day,[FilledDate])),
 CONSTRAINT [PK_DocumentFormTemplateValue] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DocumentSubscriberDetail]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DocumentSubscriberDetail](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[DocumentId] [bigint] NOT NULL,
	[IsSubscribe] [bit] NOT NULL,
 CONSTRAINT [PK_DocumentSubscriberDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DocumentVersionDetail]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DocumentVersionDetail](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[DocumentId] [bigint] NOT NULL,
	[DocumentName] [nvarchar](500) NOT NULL,
	[Notes] [nvarchar](max) NOT NULL,
	[Keywords] [nvarchar](max) NOT NULL,
	[DocumentType] [nvarchar](10) NOT NULL,
	[DocumentSize] [decimal](18, 2) NOT NULL,
	[DocumentDisplaySize] [nvarchar](20) NOT NULL,
	[DocumentVersion] [float] NOT NULL,
	[PhysicalLocation] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[Language] [int] NOT NULL,
	[IsPublic] [bit] NOT NULL,
	[IsLocked] [bit] NOT NULL,
	[IsCurrent] [bit] NOT NULL,
	[DocumentPath] [nvarchar](max) NOT NULL,
	[CheckedOutBy] [bigint] NULL,
	[Confidentiality] [tinyint] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_DocumentVersionDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DynamicFormResponse]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DynamicFormResponse](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ResponseCode] [nvarchar](max) NOT NULL,
	[FormTemplateId] [bigint] NOT NULL,
	[AttachedType] [nchar](10) NOT NULL,
	[AttachedId] [bigint] NOT NULL,
	[FiledId] [nvarchar](50) NOT NULL,
	[Type] [nvarchar](50) NOT NULL,
	[Value] [nvarchar](max) NULL,
	[IsSelected] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_DynamicFormResponse] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EmailTemplate]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailTemplate](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[Subject] [nvarchar](200) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[Body] [nvarchar](max) NOT NULL,
	[IsEdit] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_EmailTemplate] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EmailTemplateTag]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailTemplateTag](
	[EmailTemplateTagID] [bigint] IDENTITY(1,1) NOT NULL,
	[EmailTemplateTag] [varchar](50) NOT NULL,
	[Description] [varchar](500) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[TagType] [bigint] NOT NULL,
	[DisplayIndex] [int] NOT NULL,
 CONSTRAINT [PK_MessgaeTagMaster] PRIMARY KEY CLUSTERED 
(
	[EmailTemplateTagID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EmailTemplateTagType]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailTemplateTagType](
	[TagTypeId] [bigint] IDENTITY(1,1) NOT NULL,
	[TypeName] [nvarchar](50) NOT NULL,
	[ViewIndex] [int] NOT NULL,
 CONSTRAINT [PK_EmailTemplateTagType] PRIMARY KEY CLUSTERED 
(
	[TagTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ExceptionLog]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExceptionLog](
	[ExceptionLogID] [bigint] IDENTITY(1,1) NOT NULL,
	[LogDate] [datetime] NOT NULL,
	[LogDateInt]  AS ((datepart(year,[LogDate])*(10000)+datepart(month,[LogDate])*(100))+datepart(day,[LogDate])),
	[IPAddress] [nvarchar](50) NULL,
	[Browser] [nvarchar](150) NULL,
	[Version] [nvarchar](50) NULL,
	[IsMobile] [bit] NOT NULL,
	[Controller] [nvarchar](50) NOT NULL,
	[Action] [nvarchar](50) NOT NULL,
	[Exception] [nvarchar](max) NOT NULL,
	[DeveloperNote] [nvarchar](max) NOT NULL,
	[UserId] [bigint] NULL,
	[ApplicationId] [tinyint] NULL,
 CONSTRAINT [PK_ExceptionLog] PRIMARY KEY CLUSTERED 
(
	[ExceptionLogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Favorite]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Favorite](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[FolderId] [bigint] NOT NULL,
	[DocumentId] [bigint] NOT NULL,
	[ParentId] [bigint] NOT NULL,
	[CabinetId] [bigint] NOT NULL,
 CONSTRAINT [PK_Favorite] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FileIcon]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FileIcon](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[DocType] [nvarchar](70) NOT NULL,
	[CssClass] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_FileIcon] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FolderDetail]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FolderDetail](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ParentFolderId] [bigint] NOT NULL,
	[UDID] [nvarchar](50) NOT NULL,
	[CabinetId] [bigint] NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[Notes] [nvarchar](max) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[Keywords] [nvarchar](max) NOT NULL,
	[PhysicalLocation] [nvarchar](max) NULL,
	[AssignFormTemplateId] [bigint] NOT NULL,
	[IsPublic] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_FolderDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FolderFileComments]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FolderFileComments](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CommentBy] [bigint] NOT NULL,
	[FolderId] [bigint] NOT NULL,
	[DocumentId] [bigint] NOT NULL,
	[Comment] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
 CONSTRAINT [PK_FolderFileComments] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FolderFormTemplateValue]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FolderFormTemplateValue](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[AssignFormTemplateId] [bigint] NOT NULL,
	[AssignedFormControlId] [bigint] NOT NULL,
	[Value] [nvarchar](max) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[FilledDate] [datetime] NOT NULL,
	[FilledDateInt]  AS ((datepart(year,[FilledDate])*(10000)+datepart(month,[FilledDate])*(100))+datepart(day,[FilledDate])),
 CONSTRAINT [PK_FolderFormTemplateValue] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FolderTree]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FolderTree](
	[FolderTreeId] [bigint] IDENTITY(1,1) NOT NULL,
	[CabinetId] [bigint] NOT NULL,
	[MenuId] [bigint] NOT NULL,
	[MenuName] [nvarchar](max) NOT NULL,
	[CssClass] [nvarchar](max) NOT NULL,
	[FileType] [nvarchar](max) NOT NULL,
	[ParentId] [bigint] NOT NULL,
	[ViewIndex] [bigint] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_FolderTree] PRIMARY KEY CLUSTERED 
(
	[FolderTreeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FontFamily]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FontFamily](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[FontType] [nvarchar](max) NOT NULL,
	[FontFamilyCSS] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_FontFamily] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FormCabinetDetail]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FormCabinetDetail](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[FormId] [bigint] NOT NULL,
	[CabinetId] [bigint] NOT NULL,
 CONSTRAINT [PK_FormCabinetDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FormControlProperty]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FormControlProperty](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[FormTemplateId] [bigint] NOT NULL,
	[FieldId] [nvarchar](50) NOT NULL,
	[FieldType] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_FormControlProperty] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FormTemplate]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FormTemplate](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[FormTitle] [nvarchar](150) NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[HtmlContent] [nvarchar](max) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
	[CreatedBy] [nvarchar](max) NOT NULL,
	[UpdatedBy] [nvarchar](max) NULL,
 CONSTRAINT [PK_FormTemplate] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FormTemplateDetail]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FormTemplateDetail](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[FormTitle] [nvarchar](150) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
	[FormContant] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[CabinetID] [bigint] NOT NULL,
 CONSTRAINT [PK_FormTemplateDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GroupDepartmentDetail]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GroupDepartmentDetail](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[GroupId] [bigint] NOT NULL,
	[DepartmentId] [bigint] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
 CONSTRAINT [PK_GroupDepartmentDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GroupDetail]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GroupDetail](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[CabinetId] [bigint] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_GroupDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GroupUserDetail]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GroupUserDetail](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[GroupId] [bigint] NOT NULL,
	[UserId] [bigint] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
 CONSTRAINT [PK_GroupUserDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Link]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Link](
	[LinkId] [bigint] IDENTITY(1,1) NOT NULL,
	[ModuleId] [bigint] NOT NULL,
	[ApplicationId] [tinyint] NOT NULL,
	[LinkNameArabic] [nvarchar](500) NULL,
	[LinkName] [varchar](100) NOT NULL,
	[Controller] [varchar](50) NOT NULL,
	[Action] [varchar](50) NOT NULL,
	[TypeArabic] [nvarchar](50) NULL,
	[Type] [nvarchar](50) NOT NULL,
	[ViewIndex] [int] NOT NULL,
	[IsDefault] [bit] NOT NULL,
	[IsSingle] [bit] NOT NULL,
	[IsPage] [bit] NOT NULL,
	[LinkGroupId] [int] NOT NULL,
	[ParentId] [bigint] NOT NULL,
	[IsVisible] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_Link] PRIMARY KEY CLUSTERED 
(
	[LinkId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LinkDocument]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LinkDocument](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ParentId] [bigint] NOT NULL,
	[ChildId] [bigint] NOT NULL,
	[LinkBy] [bigint] NOT NULL,
	[CabinetId] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
 CONSTRAINT [PK_LinkDocument] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LoginCredential]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoginCredential](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[MiddleName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[Gender] [char](1) NOT NULL,
	[EmailId] [nvarchar](100) NOT NULL,
	[DepartmentId] [bigint] NOT NULL,
	[SectionId] [bigint] NOT NULL,
	[PositionId] [bigint] NULL,
	[MobileNumber] [nvarchar](20) NOT NULL,
	[ManagerId] [bigint] NULL,
	[PhoneNumber] [nvarchar](20) NULL,
	[DOB] [datetime] NOT NULL,
	[ProfileImage] [nvarchar](150) NULL,
	[SentNotification] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_LoginCredential] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LogType]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LogType](
	[LogTypeId] [bigint] IDENTITY(1,1) NOT NULL,
	[TypeName] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_LogType] PRIMARY KEY CLUSTERED 
(
	[LogTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Module]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Module](
	[ModuleId] [bigint] IDENTITY(1,1) NOT NULL,
	[ModuleNameArabic] [nvarchar](500) NULL,
	[ModuleName] [varchar](50) NOT NULL,
	[CssClass] [varchar](100) NOT NULL,
	[ViewIndex] [int] NULL,
	[CreatedById] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
 CONSTRAINT [PK_Module] PRIMARY KEY CLUSTERED 
(
	[ModuleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Notification]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Notification](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[SenderId] [bigint] NOT NULL,
	[ReceiverId] [bigint] NOT NULL,
	[Message] [nvarchar](max) NOT NULL,
	[DocumentId] [bigint] NOT NULL,
	[IsRead] [bit] NOT NULL,
	[ReadDate] [datetime] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
 CONSTRAINT [PK_Notification] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PasswordHistory]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PasswordHistory](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[ApplicationId] [tinyint] NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[ChangeDate] [datetime] NOT NULL,
	[ChangeDateInt]  AS ((datepart(year,[ChangeDate])*(10000)+datepart(month,[ChangeDate])*(100))+datepart(day,[ChangeDate])),
 CONSTRAINT [PK_PasswordHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PositionMaster]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PositionMaster](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[SectionId] [bigint] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_PositionMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RoleMaster]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoleMaster](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_RoleMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RoleRight]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoleRight](
	[RoleRightId] [bigint] IDENTITY(1,1) NOT NULL,
	[CabinetId] [bigint] NOT NULL,
	[RoleId] [bigint] NOT NULL,
	[LinkId] [bigint] NOT NULL,
 CONSTRAINT [PK_RoleRight] PRIMARY KEY CLUSTERED 
(
	[RoleRightId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SectionMaster]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SectionMaster](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[DepartmentId] [bigint] NOT NULL,
	[SectionManagerId] [bigint] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_DesignationMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SubCategoryMaster]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubCategoryMaster](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[CategoryId] [bigint] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_SubCategoryMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TrashBasketDetail]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TrashBasketDetail](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[DocumentId] [bigint] NOT NULL,
	[IsUserDelete] [bit] NOT NULL,
	[IsAdminDelete] [bit] NOT NULL,
 CONSTRAINT [PK_TrashBasketDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserCabinetDetail]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserCabinetDetail](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[CabinetId] [bigint] NOT NULL,
 CONSTRAINT [PK_UserCabinet] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserDocShareDetail]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserDocShareDetail](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[DocShareUserId] [bigint] NOT NULL,
	[DocShareGroupId] [bigint] NOT NULL,
	[FolderId] [bigint] NOT NULL,
	[DocumentId] [bigint] NOT NULL,
	[ParentId] [bigint] NOT NULL,
	[CabinetId] [bigint] NOT NULL,
	[IsDownload] [bit] NOT NULL,
	[IsUpload] [bit] NOT NULL,
	[IsSeeVersion] [bit] NOT NULL,
	[IsReadComment] [bit] NOT NULL,
	[IsAddComment] [bit] NOT NULL,
	[IsShare] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
 CONSTRAINT [PK_UserDocShareDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserDocSharePermission]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserDocSharePermission](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CabinetId] [bigint] NOT NULL,
	[UserId] [bigint] NOT NULL,
	[DocShareUserId] [bigint] NOT NULL,
	[DocShareGroupId] [bigint] NOT NULL,
 CONSTRAINT [PK_UserDocSharePermission] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserLoginDetail]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserLoginDetail](
	[LoginId] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[UserName] [nvarchar](64) NOT NULL,
	[ApplicationId] [tinyint] NOT NULL,
	[LastLogin] [datetime] NOT NULL,
 CONSTRAINT [PK_UserLoginDetail] PRIMARY KEY CLUSTERED 
(
	[LoginId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserRight]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRight](
	[UserRightId] [bigint] IDENTITY(1,1) NOT NULL,
	[CabinetId] [bigint] NOT NULL,
	[UserId] [bigint] NOT NULL,
	[LinkId] [bigint] NOT NULL,
 CONSTRAINT [PK_UserRight] PRIMARY KEY CLUSTERED 
(
	[UserRightId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserRole]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRole](
	[UserRoleId] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[RoleId] [bigint] NOT NULL,
 CONSTRAINT [PK_UserRole] PRIMARY KEY CLUSTERED 
(
	[UserRoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[WF_Actions]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_Actions](
	[ActionId] [int] IDENTITY(1,1) NOT NULL,
	[ActionName] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_WF_Actions_1] PRIMARY KEY CLUSTERED 
(
	[ActionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[WF_Close]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_Close](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[WFId] [bigint] NOT NULL,
	[UserId] [bigint] NOT NULL,
	[GroupId] [bigint] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
 CONSTRAINT [PK_WF_Close_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[WF_Details]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_Details](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[WFId] [bigint] NOT NULL,
	[WFStep] [int] NOT NULL,
	[WFStepName] [nvarchar](100) NOT NULL,
	[WFStepNotes] [nvarchar](100) NOT NULL,
	[AssignedUserID] [bigint] NOT NULL,
	[WFActions] [nvarchar](20) NOT NULL,
	[SubsidiaryId] [bigint] NOT NULL,
	[IsSubsidiaryActive] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
 CONSTRAINT [PK_WF_Details_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[WF_FormTemplateValue]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_FormTemplateValue](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[WFTransactionId] [bigint] NOT NULL,
	[AssignFormTemplateId] [bigint] NOT NULL,
	[AssignedFormControlId] [bigint] NOT NULL,
	[Value] [nvarchar](max) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[FilledDate] [datetime] NOT NULL,
	[Notes] [nvarchar](100) NULL,
	[FilledDateInt]  AS ((datepart(year,[FilledDate])*(10000)+datepart(month,[FilledDate])*(100))+datepart(day,[FilledDate])),
 CONSTRAINT [PK_WF_FormTemplateInstance] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[WF_Initaitors]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_Initaitors](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[WFId] [bigint] NOT NULL,
	[UserId] [bigint] NOT NULL,
	[GroupId] [bigint] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
 CONSTRAINT [PK_WF_Initaitors] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[WF_Instance]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_Instance](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CurrentStep] [int] NOT NULL,
	[NextStep] [int] NOT NULL,
	[WFId] [bigint] NOT NULL,
	[InitiatorUser] [bigint] NOT NULL,
	[CloseUser] [bigint] NOT NULL,
	[IsOpen] [bit] NOT NULL,
	[StartDateTime] [datetime] NOT NULL,
	[EndDateTime] [datetime] NOT NULL,
	[StartDateTimeInt]  AS ((datepart(year,[StartDateTime])*(10000)+datepart(month,[StartDateTime])*(100))+datepart(day,[StartDateTime])),
	[EndDateTimeInt]  AS ((datepart(year,[EndDateTime])*(10000)+datepart(month,[EndDateTime])*(100))+datepart(day,[EndDateTime])),
 CONSTRAINT [PK_WF_Instance_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[WF_Master]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_Master](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CabinetId] [bigint] NOT NULL,
	[Descritpion] [nvarchar](max) NOT NULL,
	[Notes] [nvarchar](max) NOT NULL,
	[WFName] [nvarchar](100) NOT NULL,
	[IsManual] [bit] NOT NULL,
	[WFTotalSteps] [int] NOT NULL,
	[AssignFormTemplateId] [bigint] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedDateInt]  AS ((datepart(year,[UpdatedDate])*(10000)+datepart(month,[UpdatedDate])*(100))+datepart(day,[UpdatedDate])),
 CONSTRAINT [PK_WF_Master] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[WF_ProcessAdmin]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_ProcessAdmin](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[WFId] [bigint] NOT NULL,
	[UserId] [bigint] NOT NULL,
	[GroupId] [bigint] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
 CONSTRAINT [PK_WF_ProcessAdmin_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[WF_TransactionDocuments]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_TransactionDocuments](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[WFTransactionId] [bigint] NOT NULL,
	[DocumentId] [bigint] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedDateInt]  AS ((datepart(year,[CreatedDate])*(10000)+datepart(month,[CreatedDate])*(100))+datepart(day,[CreatedDate])),
 CONSTRAINT [PK_WF_TransactionDocuments] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[WF_Transactions]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_Transactions](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[WFInstanceId] [bigint] NOT NULL,
	[FromUser] [bigint] NOT NULL,
	[WFStep] [int] NOT NULL,
	[ToUser] [bigint] NOT NULL,
	[Notes] [nvarchar](100) NOT NULL,
	[ActionId] [int] NOT NULL,
	[ActionDate] [datetime] NOT NULL,
	[ActionDateInt]  AS ((datepart(year,[ActionDate])*(10000)+datepart(month,[ActionDate])*(100))+datepart(day,[ActionDate])),
 CONSTRAINT [PK_WF_Transactions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[WF_TransactionsNotificatoins]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_TransactionsNotificatoins](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[WFTransactionId] [bigint] NOT NULL,
	[FromUser] [bigint] NOT NULL,
	[ToUser] [bigint] NOT NULL,
	[Message] [nvarchar](max) NOT NULL,
	[IsRead] [bit] NOT NULL,
 CONSTRAINT [PK_WF_TransactionsNotificatoins] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[AssignFormTemplate] ADD  CONSTRAINT [DF_AssignFormTemplateDetail_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[AssignFormTemplate] ADD  CONSTRAINT [DF_AssignFormTemplateDetail_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[AssignFormTemplate] ADD  CONSTRAINT [DF_AssignFormTemplateDetail_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[AssignFormTemplate] ADD  CONSTRAINT [DF_AssignFormTemplateDetail_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[AuditrialLog] ADD  CONSTRAINT [DF_AuidtrailLog_IsMobile]  DEFAULT ((0)) FOR [IsMobile]
GO
ALTER TABLE [dbo].[AuditrialLog] ADD  CONSTRAINT [DF_AuidtrailLog_AuditDate]  DEFAULT (getdate()) FOR [AuditDate]
GO
ALTER TABLE [dbo].[CabinetMaster] ADD  CONSTRAINT [DF_CabinetMaster_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[CabinetMaster] ADD  CONSTRAINT [DF_CabinetMaster_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[CabinetMaster] ADD  CONSTRAINT [DF_CabinetMaster_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[CabinetMaster] ADD  CONSTRAINT [DF_CabinetMaster_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[CabinetMaster] ADD  CONSTRAINT [DF_CabinetMaster_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[CabinetMaster] ADD  CONSTRAINT [DF_CabinetMaster_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[CategoryMaster] ADD  CONSTRAINT [DF_CategoryMaster_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[CategoryMaster] ADD  CONSTRAINT [DF_CategoryMaster_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[CategoryMaster] ADD  CONSTRAINT [DF_CategoryMaster_CreatedBy1]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[CategoryMaster] ADD  CONSTRAINT [DF_CategoryMaster_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[CategoryMaster] ADD  CONSTRAINT [DF_CategoryMaster_UpdatedBy1]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[CategoryMaster] ADD  CONSTRAINT [DF_CategoryMaster_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[DeletedData] ADD  CONSTRAINT [DF_Table_1_IsDelete]  DEFAULT ((0)) FOR [IsRecycleBinDeleted]
GO
ALTER TABLE [dbo].[DeletedData] ADD  CONSTRAINT [DF_Table_1_CreatedBy]  DEFAULT (suser_name()) FOR [DeletedBy]
GO
ALTER TABLE [dbo].[DeletedData] ADD  CONSTRAINT [DF_Table_1_CreatedDate]  DEFAULT (getdate()) FOR [DeletedDate]
GO
ALTER TABLE [dbo].[DepartmentMaster] ADD  CONSTRAINT [DF_DepartmentMaster_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[DepartmentMaster] ADD  CONSTRAINT [DF_DepartmentMaster_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[DepartmentMaster] ADD  CONSTRAINT [DF_DepartmentMaster_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[DepartmentMaster] ADD  CONSTRAINT [DF_DepartmentMaster_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[DepartmentMaster] ADD  CONSTRAINT [DF_DepartmentMaster_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[DepartmentMaster] ADD  CONSTRAINT [DF_DepartmentMaster_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[DocumentDetail] ADD  CONSTRAINT [DF_DocumentDetail_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[DocumentDetail] ADD  CONSTRAINT [DF_DocumentDetail_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[DocumentDetail] ADD  CONSTRAINT [DF_DocumentDetail_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[DocumentDetail] ADD  CONSTRAINT [DF_DocumentDetail_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[DocumentDetail] ADD  CONSTRAINT [DF_DocumentDetail_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[DocumentDetail] ADD  CONSTRAINT [DF_DocumentDetail_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[DocumentSubscriberDetail] ADD  CONSTRAINT [DF_DocumentSubscriberDetail_IsSubscribe]  DEFAULT ((0)) FOR [IsSubscribe]
GO
ALTER TABLE [dbo].[DocumentVersionDetail] ADD  CONSTRAINT [DF_DocumentVersionDetail_Confidentiality]  DEFAULT ((0)) FOR [IsPublic]
GO
ALTER TABLE [dbo].[DocumentVersionDetail] ADD  CONSTRAINT [DF_DocumentVersionDetail_IsLocked]  DEFAULT ((0)) FOR [IsLocked]
GO
ALTER TABLE [dbo].[DocumentVersionDetail] ADD  CONSTRAINT [DF_DocumentVersionDetail_IsCurrent]  DEFAULT ((0)) FOR [IsCurrent]
GO
ALTER TABLE [dbo].[DocumentVersionDetail] ADD  CONSTRAINT [DF_DocumentVersionDetail_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[DocumentVersionDetail] ADD  CONSTRAINT [DF_DocumentVersionDetail_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[DocumentVersionDetail] ADD  CONSTRAINT [DF_DocumentVersionDetail_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[DocumentVersionDetail] ADD  CONSTRAINT [DF_DocumentVersionDetail_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[DocumentVersionDetail] ADD  CONSTRAINT [DF_DocumentVersionDetail_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[DocumentVersionDetail] ADD  CONSTRAINT [DF_DocumentVersionDetail_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[DynamicFormResponse] ADD  CONSTRAINT [DF_DynamicFormResponse_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[DynamicFormResponse] ADD  CONSTRAINT [DF_DynamicFormResponse_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[DynamicFormResponse] ADD  CONSTRAINT [DF_DynamicFormResponse_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[DynamicFormResponse] ADD  CONSTRAINT [DF_DynamicFormResponse_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[EmailTemplate] ADD  CONSTRAINT [DF_EmailTemplate_IsEdit]  DEFAULT ((1)) FOR [IsEdit]
GO
ALTER TABLE [dbo].[EmailTemplate] ADD  CONSTRAINT [DF_EmailTemplate_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[EmailTemplate] ADD  CONSTRAINT [DF_EmailTemplate_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[EmailTemplate] ADD  CONSTRAINT [DF_EmailTemplate_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[EmailTemplate] ADD  CONSTRAINT [DF_EmailTemplate_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[EmailTemplate] ADD  CONSTRAINT [DF_EmailTemplate_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[EmailTemplate] ADD  CONSTRAINT [DF_EmailTemplate_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[EmailTemplateTag] ADD  CONSTRAINT [DF_EmailTemplateTag_IsActive_1]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[ExceptionLog] ADD  CONSTRAINT [DF_ExceptionLog_LogDate]  DEFAULT (getdate()) FOR [LogDate]
GO
ALTER TABLE [dbo].[ExceptionLog] ADD  CONSTRAINT [DF_ExceptionLog_IsMobile]  DEFAULT ((0)) FOR [IsMobile]
GO
ALTER TABLE [dbo].[FolderDetail] ADD  CONSTRAINT [DF_FolderDetail_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[FolderDetail] ADD  CONSTRAINT [DF_FolderDetail_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[FolderDetail] ADD  CONSTRAINT [DF_FolderDetail_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[FolderDetail] ADD  CONSTRAINT [DF_FolderDetail_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[FolderDetail] ADD  CONSTRAINT [DF_FolderDetail_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[FolderDetail] ADD  CONSTRAINT [DF_FolderDetail_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[FolderFileComments] ADD  CONSTRAINT [DF_FolderFileComments_CreatedDate1]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[FormTemplate] ADD  CONSTRAINT [DF_FormTemplate_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[FormTemplate] ADD  CONSTRAINT [DF_FormTemplate_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[FormTemplate] ADD  CONSTRAINT [DF_FormTemplate_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[FormTemplate] ADD  CONSTRAINT [DF_FormTemplate_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[FormTemplate] ADD  CONSTRAINT [DF_FormTemplate_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[FormTemplate] ADD  CONSTRAINT [DF_FormTemplate_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[FormTemplateDetail] ADD  CONSTRAINT [DF_FormTemplateDetail_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[FormTemplateDetail] ADD  CONSTRAINT [DF_FormTemplateDetail_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[FormTemplateDetail] ADD  CONSTRAINT [DF_FormTemplateDetail_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[FormTemplateDetail] ADD  CONSTRAINT [DF_FormTemplateDetail_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[FormTemplateDetail] ADD  CONSTRAINT [DF_FormTemplateDetail_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[FormTemplateDetail] ADD  CONSTRAINT [DF_FormTemplateDetail_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[GroupDepartmentDetail] ADD  CONSTRAINT [DF_GroupDepartmentDetail_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[GroupDepartmentDetail] ADD  CONSTRAINT [DF_GroupDepartmentDetail_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[GroupDetail] ADD  CONSTRAINT [DF_GroupDetail_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[GroupDetail] ADD  CONSTRAINT [DF_GroupDetail_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[GroupDetail] ADD  CONSTRAINT [DF_GroupDetail_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[GroupDetail] ADD  CONSTRAINT [DF_GroupDetail_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[GroupDetail] ADD  CONSTRAINT [DF_GroupDetail_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[GroupDetail] ADD  CONSTRAINT [DF_GroupDetail_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[GroupUserDetail] ADD  CONSTRAINT [DF_GroupUserDetail_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[GroupUserDetail] ADD  CONSTRAINT [DF_GroupUserDetail_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Link] ADD  CONSTRAINT [DF_Link_IsDefault]  DEFAULT ((1)) FOR [IsDefault]
GO
ALTER TABLE [dbo].[Link] ADD  CONSTRAINT [DF_Link_IsSingle]  DEFAULT ((0)) FOR [IsSingle]
GO
ALTER TABLE [dbo].[Link] ADD  CONSTRAINT [DF_Link_IsPage]  DEFAULT ((1)) FOR [IsPage]
GO
ALTER TABLE [dbo].[Link] ADD  CONSTRAINT [DF_Link_ParentId]  DEFAULT ((0)) FOR [ParentId]
GO
ALTER TABLE [dbo].[Link] ADD  CONSTRAINT [DF_Link_IsVisible]  DEFAULT ((0)) FOR [IsVisible]
GO
ALTER TABLE [dbo].[Link] ADD  CONSTRAINT [DF_Link_CreatedByID]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[Link] ADD  CONSTRAINT [DF_Link_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Link] ADD  CONSTRAINT [DF_Link_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[Link] ADD  CONSTRAINT [DF_Link_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[LinkDocument] ADD  CONSTRAINT [DF_LinkDocument_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[LoginCredential] ADD  CONSTRAINT [DF_LoginCredential_SentNotification]  DEFAULT ((0)) FOR [SentNotification]
GO
ALTER TABLE [dbo].[LoginCredential] ADD  CONSTRAINT [DF_LoginCredential_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[LoginCredential] ADD  CONSTRAINT [DF_LoginCredential_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[LoginCredential] ADD  CONSTRAINT [DF_LoginCredential_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[LoginCredential] ADD  CONSTRAINT [DF_LoginCredential_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[LoginCredential] ADD  CONSTRAINT [DF_LoginCredential_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[LoginCredential] ADD  CONSTRAINT [DF_LoginCredential_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[Module] ADD  CONSTRAINT [DF_Module_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Notification] ADD  CONSTRAINT [DF_Notification_IsRead]  DEFAULT ((0)) FOR [IsRead]
GO
ALTER TABLE [dbo].[Notification] ADD  CONSTRAINT [DF_Notification_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[PositionMaster] ADD  CONSTRAINT [DF_PositionMaster_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[PositionMaster] ADD  CONSTRAINT [DF_PositionMaster_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[PositionMaster] ADD  CONSTRAINT [DF_PositionMaster_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[PositionMaster] ADD  CONSTRAINT [DF_PositionMaster_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[PositionMaster] ADD  CONSTRAINT [DF_PositionMaster_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[PositionMaster] ADD  CONSTRAINT [DF_PositionMaster_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[RoleMaster] ADD  CONSTRAINT [DF_RoleMaster_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[RoleMaster] ADD  CONSTRAINT [DF_RoleMaster_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[RoleMaster] ADD  CONSTRAINT [DF_RoleMaster_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[RoleMaster] ADD  CONSTRAINT [DF_RoleMaster_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[RoleMaster] ADD  CONSTRAINT [DF_RoleMaster_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[RoleMaster] ADD  CONSTRAINT [DF_RoleMaster_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[RoleRight] ADD  CONSTRAINT [DF_Table_1_UserId]  DEFAULT ((0)) FOR [RoleId]
GO
ALTER TABLE [dbo].[SectionMaster] ADD  CONSTRAINT [DF_DesignationMaster_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[SectionMaster] ADD  CONSTRAINT [DF_DesignationMaster_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[SectionMaster] ADD  CONSTRAINT [DF_SectionMaster_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[SectionMaster] ADD  CONSTRAINT [DF_SectionMaster_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[SectionMaster] ADD  CONSTRAINT [DF_SectionMaster_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[SectionMaster] ADD  CONSTRAINT [DF_SectionMaster_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[SubCategoryMaster] ADD  CONSTRAINT [DF_SubCategoryMaster_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[SubCategoryMaster] ADD  CONSTRAINT [DF_SubCategoryMaster_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[SubCategoryMaster] ADD  CONSTRAINT [DF_SubCategoryMaster_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[SubCategoryMaster] ADD  CONSTRAINT [DF_SubCategoryMaster_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[SubCategoryMaster] ADD  CONSTRAINT [DF_SubCategoryMaster_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[SubCategoryMaster] ADD  CONSTRAINT [DF_SubCategoryMaster_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[TrashBasketDetail] ADD  CONSTRAINT [DF_TrashBasketDetail_IsUserDelete]  DEFAULT ((0)) FOR [IsUserDelete]
GO
ALTER TABLE [dbo].[TrashBasketDetail] ADD  CONSTRAINT [DF_TrashBasketDetail_IsAdminDelete]  DEFAULT ((0)) FOR [IsAdminDelete]
GO
ALTER TABLE [dbo].[UserDocShareDetail] ADD  CONSTRAINT [DF_UserDocShareDetail_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[UserDocShareDetail] ADD  CONSTRAINT [DF_UserDocShareDetail_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[UserRight] ADD  CONSTRAINT [DF_UserRight_UserID]  DEFAULT ((0)) FOR [UserId]
GO
ALTER TABLE [dbo].[UserRole] ADD  CONSTRAINT [DF_UserRole_RoleId]  DEFAULT ((0)) FOR [RoleId]
GO
ALTER TABLE [dbo].[WF_Close] ADD  CONSTRAINT [DF_WF_Close_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[WF_Close] ADD  CONSTRAINT [DF_WF_Close_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[WF_Details] ADD  CONSTRAINT [DF_WF_Details_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[WF_Details] ADD  CONSTRAINT [DF_WF_Details_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[WF_Initaitors] ADD  CONSTRAINT [DF_WF_Initaitors_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[WF_Initaitors] ADD  CONSTRAINT [DF_WF_Initaitors_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[WF_Master] ADD  CONSTRAINT [DF_WF_Master_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[WF_Master] ADD  CONSTRAINT [DF_WF_Master_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[WF_Master] ADD  CONSTRAINT [DF_WF_Master_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[WF_Master] ADD  CONSTRAINT [DF_WF_Master_UpdatedBy]  DEFAULT (suser_name()) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[WF_Master] ADD  CONSTRAINT [DF_WF_Master_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[WF_ProcessAdmin] ADD  CONSTRAINT [DF_WF_ProcessAdmin_CreatedBy_1]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[WF_ProcessAdmin] ADD  CONSTRAINT [DF_WF_ProcessAdmin_CreatedDate_1]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[WF_TransactionDocuments] ADD  CONSTRAINT [DF_WF_TransactionDocuments_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[WF_TransactionDocuments] ADD  CONSTRAINT [DF_WF_TransactionDocuments_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Link]  WITH CHECK ADD  CONSTRAINT [FK_Link_Module] FOREIGN KEY([ModuleId])
REFERENCES [dbo].[Module] ([ModuleId])
GO
ALTER TABLE [dbo].[Link] CHECK CONSTRAINT [FK_Link_Module]
GO
ALTER TABLE [dbo].[RoleRight]  WITH CHECK ADD  CONSTRAINT [FK_RoleRight_Link] FOREIGN KEY([LinkId])
REFERENCES [dbo].[Link] ([LinkId])
GO
ALTER TABLE [dbo].[RoleRight] CHECK CONSTRAINT [FK_RoleRight_Link]
GO
ALTER TABLE [dbo].[UserRight]  WITH CHECK ADD  CONSTRAINT [FK_UserRight_Link] FOREIGN KEY([LinkId])
REFERENCES [dbo].[Link] ([LinkId])
GO
ALTER TABLE [dbo].[UserRight] CHECK CONSTRAINT [FK_UserRight_Link]
GO
ALTER TABLE [dbo].[WF_Details]  WITH CHECK ADD  CONSTRAINT [FK_WF_Details_WF_Master] FOREIGN KEY([WFId])
REFERENCES [dbo].[WF_Master] ([Id])
GO
ALTER TABLE [dbo].[WF_Details] CHECK CONSTRAINT [FK_WF_Details_WF_Master]
GO
ALTER TABLE [dbo].[WF_FormTemplateValue]  WITH CHECK ADD  CONSTRAINT [FK_WF_FormTemplateInstance_WF_Transactions] FOREIGN KEY([WFTransactionId])
REFERENCES [dbo].[WF_Transactions] ([Id])
GO
ALTER TABLE [dbo].[WF_FormTemplateValue] CHECK CONSTRAINT [FK_WF_FormTemplateInstance_WF_Transactions]
GO
ALTER TABLE [dbo].[WF_Initaitors]  WITH CHECK ADD  CONSTRAINT [FK_WF_Initaitors_WF_Master] FOREIGN KEY([WFId])
REFERENCES [dbo].[WF_Master] ([Id])
GO
ALTER TABLE [dbo].[WF_Initaitors] CHECK CONSTRAINT [FK_WF_Initaitors_WF_Master]
GO
ALTER TABLE [dbo].[WF_TransactionDocuments]  WITH CHECK ADD  CONSTRAINT [FK_WFTransaction_Documetns_WF_Transactions] FOREIGN KEY([WFTransactionId])
REFERENCES [dbo].[WF_Transactions] ([Id])
GO
ALTER TABLE [dbo].[WF_TransactionDocuments] CHECK CONSTRAINT [FK_WFTransaction_Documetns_WF_Transactions]
GO
ALTER TABLE [dbo].[WF_TransactionsNotificatoins]  WITH CHECK ADD  CONSTRAINT [FK_WF_TransactionsNotificatoins_WF_Transactions] FOREIGN KEY([WFTransactionId])
REFERENCES [dbo].[WF_Transactions] ([Id])
GO
ALTER TABLE [dbo].[WF_TransactionsNotificatoins] CHECK CONSTRAINT [FK_WF_TransactionsNotificatoins_WF_Transactions]
GO
/****** Object:  StoredProcedure [dbo].[Test29052018]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
-- Test29052018 1,'10048'

CREATE PROCEDURE [dbo].[Test29052018]
	-- Add the parameters for the stored procedure here
@cabinetId AS BIGINT = NULL,
	@userId AS NVARCHAR(20) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT 
	FD.Id AS Id
	,FD.Name AS Name
	,'folder' AS DocumentType
	,0 AS DocumentVersion
	,'-' AS DocumentDisplaySize
	,LC.FirstName + ' ' +LC.LastName AS ModifiedBy
	,CASE WHEN FD.UpdatedDate IS NOT NULL THEN FORMAT(FD.UpdatedDate, 'dd, MMM yyyy hh:mm tt') END AS ModifiedOn
	FROM UserDocShareDetail UDSD 
	LEFT JOIN FolderDetail FD on  UDSD.FolderId = FD.Id
	LEFT JOIN GroupDetail GD on UDSD.DocShareGroupId = GD.Id   AND GD.IsActive = 1 AND GD.IsDelete = 0
	LEFT JOIN GroupUserDetail GUD on GD.Id = GUD.GroupId
	LEFT JOIN LoginCredential LC ON FD.UpdatedBy = CAST(LC.Id AS NVARCHAR(50))	
	WHERE (UDSD.DocShareUserId = @userId OR GUD.UserId = @userId) 			
			AND ((FD.CabinetId = @cabinetId AND FD.IsActive = 1 AND FD.IsDelete = 0)  			
			AND (GD.IsActive = 1 AND GD.IsDelete = 0))
			--OR (GD.CabinetId = @cabinetId AND GD.IsActive = 1 AND GD.IsDelete = 0))	
	
	UNION ALL

	SELECT 
	DD.DocumentId AS Id
	,DD.Name AS Name
	,DVD.DocumentType AS DocumentType
	,dvd.DocumentVersion AS DocumentVersion
	,dvd.DocumentDisplaySize AS DocumentDisplaySiz	
	,LC.FirstName + ' '+ LC.LastName AS ModifiedBy
	,CASE WHEN DD.UpdatedDate IS NOT NULL THEN FORMAT(DD.UpdatedDate, 'dd, MMM yyyy hh:mm tt') END AS ModifiedOn	 
	FROM UserDocShareDetail UDSD 	
	INNER JOIN DocumentDetail DD on UDSD.DocumentId = DD.DocumentId
	LEFT JOIN GroupDetail GD on UDSD.DocShareGroupId = GD.Id AND GD.IsActive = 1 AND GD.IsDelete = 0
	LEFT JOIN GroupUserDetail GUD on GD.Id = GUD.GroupId	
	INNER JOIN (SELECT RANK() OVER (PARTITION BY DocumentId ORDER BY Id DESC) r
		,*
		FROM DocumentVersionDetail)DVD ON DD.DocumentId = DVD.DocumentId
	LEFT JOIN LoginCredential LC ON DVD.UpdatedBy =CAST(LC.Id AS NVARCHAR(50))	
	WHERE DVD.r = 1 
		AND  UDSD.CabinetId = @cabinetId 		 
		AND (UDSD.DocShareUserId = @userId OR GUD.UserId = @userId)
		AND ((DVD.IsActive = 1 AND DVD.IsDelete = 0))
		--AND (GD.IsActive = 1 AND GD.IsDelete = 0))
		--AND DVD.IsActive = 1 AND DVD.IsDelete = 0
		--AND GD.IsActive = 1 AND GD.IsDelete = 0
END

GO
/****** Object:  StoredProcedure [dbo].[uspDynamicResponseList]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sweta Patel
-- Create date: 24-04-2018
-- Description:	DynamicResponseList
-- =============================================
--uspDynamicResponseList
CREATE PROCEDURE [dbo].[uspDynamicResponseList]	
AS
BEGIN
	
SELECT
DISTINCT DR.ResponseCode,
DR.FormTemplateId,
FT.FormTitle,
LC.FirstName + ' ' + LC.LastName AS Respondent,
CAST(DR.CreatedDAte AS NVARCHAR(MAX)) AS ResponseTime
FROM DynamicFormResponse DR LEFT JOIN FormTemplate FT ON DR.FormTemplateId=FT.Id
LEFT JOIN LoginCredential LC on DR.CreatedBy = LC.Id

END



GO
/****** Object:  StoredProcedure [dbo].[uspGetChildLinkDocument]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sweta Patel
-- ALTER date: 28-06-2018
-- Description:	Get All Child Link For selected Document
-- =============================================
--uspGetChildLinkDocument 7,30496,1
--uspGetChildLinkDocument 7,30497,1
--uspGetChildLinkDocument 7,30500,1

CREATE  PROCEDURE [dbo].[uspGetChildLinkDocument]	
	@UserId NVARCHAR(20) = 0,
	@DocumentId BIGINT = 0,			  --DocumentDetail table id
    @CabinetId BIGINT = 0    
AS
BEGIN

SELECT
LD.ParentId,
LD.ChildId,
DD.DocumentId,
DVD.Id AS DocumentVersionId,
CASE WHEN DD.CreatedBy = @UserId THEN 'Me' ELSE (LC1.FirstName + ' ' + LC1.LastName) END AS DocumentOwner,
CASE WHEN DVD.CreatedBy = @UserId THEN 'Me' ELSE (LC2.FirstName + ' ' + LC2.LastName) END AS DocumentVersionOwner
, DVD.DocumentType
, DVD.IsCurrent
, DVD.IsPublic
, (SELECT [dbo].[GetFullPath](DD.DocumentId,'D',1,default))  AS OriginalLocation
 
FROM LinkDocument LD
INNER JOIN DocumentDetail DD ON DD.DocumentId=LD.ChildId
INNER JOIN DocumentVersionDetail DVD ON DD.DocumentId=DVD.DocumentId 
LEFT JOIN LoginCredential LC1 ON LC1.Id=DD.CreatedBy
LEFT JOIN LoginCredential LC2 ON LC2.Id=DVD.CreatedBy
WHERE LD.ParentId=@DocumentId AND LD.CabinetId=@CabinetId
AND DD.IsActive=1 AND DD.IsDelete=0
AND DVD.IsActive=1 AND DVD.IsDelete=0 AND DVD.IsCurrent=1

UNION ALL

SELECT
LD.ParentId,
LD.ChildId,
DD.DocumentId,
DVD.Id AS DocumentVersionId,
CASE WHEN DD.CreatedBy = @UserId THEN 'Me' ELSE (LC1.FirstName + ' ' + LC1.LastName) END AS DocumentOwner,
CASE WHEN DVD.CreatedBy = @UserId THEN 'Me' ELSE (LC2.FirstName + ' ' + LC2.LastName) END AS DocumentVersionOwner
, DVD.DocumentType
, DVD.IsCurrent
, DVD.IsPublic
, (SELECT [dbo].[GetFullPath](DD.DocumentId,'D',1,default))  AS OriginalLocation
 
FROM LinkDocument LD
INNER JOIN DocumentDetail DD ON DD.DocumentId=LD.ChildId
INNER JOIN DocumentVersionDetail DVD ON DD.DocumentId=DVD.DocumentId 
LEFT JOIN LoginCredential LC1 ON LC1.Id=DD.CreatedBy
LEFT JOIN LoginCredential LC2 ON LC2.Id=DVD.CreatedBy
WHERE LD.ParentId IN (SELECT DISTINCT ChildId FROM LinkDocument WHERE ParentId=@DocumentId) 
AND LD.CabinetId=@CabinetId
AND DD.IsActive=1 AND DD.IsDelete=0
AND DVD.IsActive=1 AND DVD.IsDelete=0 AND DVD.IsCurrent=1
ORDER BY LD.ParentId

END




GO
/****** Object:  StoredProcedure [dbo].[uspGetDeletedUserList]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dhrumil Patel
-- Create date: 16-03-2018
-- Description:	Get deleted user list
-- =============================================
--uspGetDeletedUserList
CREATE PROCEDURE [dbo].[uspGetDeletedUserList]	
	@CabinetId BIGINT = 0
AS
BEGIN
	SELECT LC.Id AS UserId
		,ISNULL(LC.FirstName,'') AS FirstName
		,ISNULL(LC.MiddleName,'')  AS MiddleName
		,ISNULL(LC.LastName,'')  AS LastName
		,ISNULL(Lc.FirstName,'') + '  ' + ISNULL(Lc.LastName,'') AS [Name]
		,ISNULL(ULD.UserName,'') AS [UserName]
		,(SELECT FirstName + ' ' + LastName FROM LoginCredential WHERE Id = LC.ManagerId) AS ManagerName
		,ISNULL(STUFF(
			(SELECT ','+ SM.Name 
				FROM UserRole ESD
				LEFT JOIN RoleMaster SM ON SM.Id = ESD.RoleId
				WHERE ESD.UserId = LC.Id
			    FOR XML PATH(''),TYPE
			).value('.','NVARCHAR(MAX)') ,1,1,''), 'All') AS RoleName	
		,ISNULL(STUFF(
		(SELECT ','+ CM.Name 
			FROM UserCabinetDetail UCD
			LEFT JOIN CabinetMaster CM ON CM.Id = UCD.CabinetId
			WHERE UCD.UserId = LC.Id
		    FOR XML PATH(''),TYPE
		).value('.','NVARCHAR(MAX)') ,1,1,''), 'All') AS CabinetName		
		,LC.DepartmentId
		,DM.[Name] AS DepartmentName
		,LC.EmailId
		,LC.IsActive
		,LC.IsDelete
    FROM LoginCredential LC
	LEFT JOIN DepartmentMaster DM ON LC.DepartmentId=DM.Id	
	LEFT JOIN UserCabinetDetail UCD ON LC.Id = UCD.UserId
	INNER JOIN UserLoginDetail ULD ON LC.Id = ULD.UserId
	WHERE ISNULL(LC.IsDelete, 0) = 1 AND ULD.ApplicationId != 1
		AND UCD.CabinetId = @CabinetId
	GROUP BY LC.Id,LC.FirstName,LC.MiddleName,LC.LastName,LC.DepartmentId,DM.[Name],LC.ManagerId,LC.IsActive
		,LC.IsDelete,ULD.UserName,LC.EmailId
	ORDER BY LC.Id DESC
END



GO
/****** Object:  StoredProcedure [dbo].[uspGetDeleteFolderDetails]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Sweta Patel> 
-- Create date: <08-06-2018>
-- Description:	<Get Folder list with cabinet In Deleted Menu>
-- =============================================
-- exec uspGetPublicFolderDetails 20,'10048',0,''
-- exec uspGetPublicFolderDetails 20,'10048',0,'about'
-- exec uspGetPublicFolderDetails 20,'10048',0,'Te'
CREATE PROCEDURE [dbo].[uspGetDeleteFolderDetails] 
	@cabinetId AS BIGINT = null,
	@userId AS NVARCHAR(20) = null,
	@folderId AS BIGINT = null,
	@searchword AS NVARCHAR(MAX) = null
AS
BEGIN	
	SET NOCOUNT ON;

If(@FolderId=0)
BEGIN

DECLARE @PublicFolderId TABLE (FolderId BIGINT)
DECLARE @PublicFiles TABLE (DocumentVersionId BIGINT)

SELECT *
INTO   #PublicFolders
FROM FolderDetail WHERE CabinetId=@CabinetId AND IsDelete=1 AND IsActive=0
SELECT *
INTO   #Temp
FROM FolderDetail WHERE CabinetId=@CabinetId AND IsDelete=1 AND IsActive=0

Declare @Id BIGINT
Declare @Parent BIGINT

While (Select Count(*) From #Temp) > 0
BEGIN
    SELECT Top 1 @Id = Id From #Temp
    SELECT Top 1 @Parent = ParentFolderId FROM #Temp

   IF((SELECT count(*) FROM #PublicFolders Where Id=@Parent)=0)
   BEGIN
              INSERT  INTO @PublicFolderId VALUES  (@Id)
   END

   DELETE #Temp WHERE Id = @Id -- delete 1 by 1 row

END

--------------------------------------------------------
INSERT INTO @PublicFiles ( DocumentVersionId )
(SELECT MAX(DVD.Id)
              FROM  DocumentVersionDetail DVD
              WHERE    @cabinetId=(SELECT TOP 1 CabinetId FROM dbo.GetCabinetFolderForDocument(DVD.DocumentId))
                        AND 0= (SELECT TOP 1 IsPublic FROM dbo.GetCabinetFolderForDocument(DVD.DocumentId))
                        AND DVD.IsActive=0 AND DVD.IsDelete=1
              GROUP BY DVD.DocumentId)

  SELECT 
              F.FolderId AS Id,
              FD.ParentFolderId,
              0 AS DocumentVersionId,
              0 As DocumentId,
              (SELECT CASE WHEN (SELECT Count(*) FROM Favorite FV WHERE FV.UserId=COALESCE(@userId,FV.UserId) AND FV.FolderId=COALESCE(F.FolderId,FV.FolderId)) >0 THEN Convert(Bit,1) ELSE Convert(Bit,0) END) AS IsFavorite,
              FD.Name,
              NULL AS FileName,
              '-' AS Version,
                (SELECT count(*) FROM FolderDetail
                                            WHERE ParentFolderId = FD.Id AND IsActive = 0 AND IsDelete = 1 )
              + (SELECT count(*) FROM DocumentDetail D 
                                            WHERE D.FolderId = FD.Id AND D.IsActive = 0 AND D.IsDelete = 1 )
              AS ChildCount,

              CAST(0 AS BIT) AS IsDocument,'folder' AS DocType,'-' AS DocSize,
              CASE WHEN DS.Id IS NOT NULL THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS Shared,
              CASE WHEN FD.UpdatedBy = @userId THEN 'Me' ELSE (LC.FirstName + ' ' + LC.LastName) END AS ModifiedBy,
              CASE WHEN FD.UpdatedDate IS NOT NULL THEN FORMAT(FD.UpdatedDate, 'dd, MMM yyyy hh:mm tt')END AS ModifiedOn

              FROM @PublicFolderId F
              INNER JOIN FolderDetail FD ON F.FolderId=FD.Id
              LEFT JOIN LoginCredential LC ON FD.UpdatedBy = LC.Id
              LEFT JOIN UserDocShareDetail DS ON FD.Id = DS.FolderId
			  WHERE
			   (UPPER(FD.Name) LIKE UPPER('%'+COALESCE(@searchword,FD.Name)+'%') OR 
	UPPER(LC.FirstName) LIKE UPPER('%'+COALESCE(@searchword,LC.FirstName)+'%') OR 
	UPPER(LC.LastName) LIKE UPPER('%'+COALESCE(@searchword,LC.LastName)+'%') OR
	UPPER(CASE WHEN FD.UpdatedBy = @userId THEN 'Me' ELSE (LC.FirstName + ' ' + LC.LastName) END) LIKE UPPER('%'+COALESCE(@searchword,'Me')+'%') OR
	UPPER(FORMAT(FD.UpdatedDate, 'dd, MMM yyyy hh:mm tt')) LIKE UPPER('%'+COALESCE(@searchword,FORMAT(FD.UpdatedDate, 'dd, MMM yyyy hh:mm tt'))+'%'))
	
  --------------------End --------------------------------
  UNION ALL
  --------------------Get Full Data For File--------------
       SELECT 
              DD.DocumentId AS Id,
              DD.FolderId AS ParentFolderId,
              DVD.Id AS DocumentVersionId,
              DD.DocumentId,
              (SELECT CASE WHEN (SELECT Count(*) FROM Favorite FV WHERE FV.UserId=COALESCE(@userId,FV.UserId) AND FV.DocumentId=COALESCE(DD.DocumentId,FV.DocumentId)) >0 THEN Convert(Bit,1) ELSE Convert(Bit,0) END) AS IsFavorite,
              Name,
              DVD.DocumentName AS FileName,
              'v'+CAST(Format(DVD.DocumentVersion,'0.0') AS NVARCHAR(MAX)) AS Version,
              0 AS ChildCount,
              CAST(1 AS BIT) AS IsDocument
              ,DVD.DocumentType AS DocType,
              DVD.DocumentDisplaySize AS DocSize,
              CASE WHEN DS.Id IS NOT NULL THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS Shared,
              CASE WHEN DD.UpdatedBy = @userId THEN 'Me' ELSE (LC.FirstName + ' ' + LC.LastName) END AS ModifiedBy,
              CASE WHEN DD.UpdatedDate IS NOT NULL THEN FORMAT(DD.UpdatedDate, 'dd, MMM yyyy hh:mm tt')END AS ModifiedOn

              FROM DocumentVersionDetail DVD
              INNER JOIN DocumentDetail DD ON DD.DocumentId=DVD.DocumentId
              LEFT JOIN LoginCredential LC ON DD.UpdatedBy = LC.Id
              LEFT JOIN UserDocShareDetail DS ON DD.DocumentId = DS.DocumentId
              WHERE DVD.Id IN (SELECT DocumentVersionId  FROM @PublicFiles)
              AND DD.IsActive=0 AND DD.IsDelete=1
-- Copy From Jasmin search code 

	AND (UPPER(DD.Name) LIKE UPPER('%'+COALESCE(@searchword,DD.Name)+'%') OR 
	UPPER('v'+CAST(Format(DVD.DocumentVersion,'0.0') AS NVARCHAR(MAX))) LIKE UPPER('%'+COALESCE(@searchword,CAST(DVD.DocumentVersion AS NVARCHAR(MAX)))+'%') OR
	UPPER(DVD.DocumentDisplaySize) LIKE UPPER('%'+COALESCE(@searchword,DVD.DocumentDisplaySize)+'%') OR
	UPPER(LC.FirstName) LIKE UPPER('%'+COALESCE(@searchword,LC.FirstName)+'%') OR 
	UPPER(LC.LastName) LIKE UPPER('%'+COALESCE(@searchword,LC.LastName)+'%') OR
	UPPER(CASE WHEN DD.UpdatedBy = @userId THEN 'Me' ELSE (LC.FirstName + ' ' + LC.LastName) END) LIKE UPPER('%'+COALESCE(@searchword,'Me')+'%') OR
	UPPER(FORMAT(DD.UpdatedDate, 'dd, MMM yyyy hh:mm tt')) LIKE UPPER('%'+COALESCE(@searchword,FORMAT(DD.UpdatedDate, 'dd, MMM yyyy hh:mm tt'))+'%'))
  --------------------End --------------------------------
  
  DROP Table #Temp
  DROP Table #PublicFolders

END
---------------------------------------------------    
If(@FolderId>0)
BEGIN

DECLARE @PublicFolderId2 TABLE (FolderId BIGINT)
DECLARE @PublicFiles2 TABLE (DocumentVersionId BIGINT)
SELECT *
INTO   #PublicFolders2
FROM FolderDetail WHERE CabinetId=@CabinetId AND IsDelete=1 AND IsActive=0
AND ParentFolderId=@FolderId
AND ParentFolderId=@FolderId
SELECT *
INTO   #Temp2
FROM FolderDetail WHERE CabinetId=@CabinetId AND IsDelete=1 AND IsActive=0
AND ParentFolderId=@FolderId

Declare @Id2 BIGINT
Declare @Parent2 BIGINT

While (Select Count(*) From #Temp2) > 0
BEGIN
    SELECT Top 1 @Id2 = Id From #Temp2
       SELECT Top 1 @Parent2 = ParentFolderId FROM #Temp2

   if((SELECT count(*) FROM #PublicFolders2 Where Id=@Parent2)=0)
   BEGIN
    INSERT  INTO @PublicFolderId2
    VALUES  (@Id2)
   END

   DELETE #Temp2 WHERE Id = @Id2

END

-------------------------------------------------------------
INSERT INTO @PublicFiles2 ( DocumentVersionId )
(SELECT 
       MAX(DVD.Id)
FROM  DocumentVersionDetail DVD
WHERE 
         @CabinetId=(SELECT TOP 1 CabinetId FROM dbo.GetCabinetFolderForDocument(DVD.DocumentId))
          AND DVD.IsActive=0 AND DVD.IsDelete=1
       AND @FolderId= (SELECT TOP 1 FolderId FROM dbo.GetCabinetFolderForDocument(DVD.DocumentId))
GROUP BY DVD.DocumentId)
--------------------------------------------------------------
--------------Get Data Inside Any Folder------------------------
              SELECT 
              F.FolderId AS Id,
              Fd.ParentFolderId,
              0 AS DocumentVersionId,
              0 As DocumentId,
              (SELECT CASE WHEN (SELECT Count(*) FROM Favorite FV WHERE FV.UserId=COALESCE(@userId,FV.UserId) AND FV.FolderId=COALESCE(F.FolderId,FV.FolderId)) >0 THEN Convert(Bit,1) ELSE Convert(Bit,0) END) AS IsFavorite,
              FD.Name,
              NULL AS FileName,
              '-' AS Version,
                (SELECT count(*) FROM FolderDetail
                                            WHERE ParentFolderId = FD.Id AND IsActive = 1 AND IsDelete = 0 AND IsPublic=1)
              + (SELECT count(*) FROM DocumentDetail D 
                                            WHERE D.FolderId = FD.Id AND D.IsActive = 1 AND D.IsDelete = 0 AND 
                                            (SELECT TOP 1 IsPublic FROM DocumentVersionDetail WHERE DocumentId=D.DocumentId ORDER BY Id DESC)=1)
              AS ChildCount,

              CAST(0 AS BIT) AS IsDocument,'folder' AS DocType,'-' AS DocSize,
              CASE WHEN DS.Id IS NOT NULL THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS Shared,
              CASE WHEN FD.UpdatedBy = @userId THEN 'Me' ELSE (LC.FirstName + ' ' + LC.LastName) END AS ModifiedBy,
              CASE WHEN FD.UpdatedDate IS NOT NULL THEN FORMAT(FD.UpdatedDate, 'dd, MMM yyyy hh:mm tt')END AS ModifiedOn

              FROM @PublicFolderId2 F
              INNER JOIN FolderDetail FD ON F.FolderId=FD.Id
              LEFT JOIN LoginCredential LC ON FD.UpdatedBy = LC.Id
              LEFT JOIN UserDocShareDetail DS ON FD.Id = DS.FolderId

			    WHERE
			   (UPPER(FD.Name) LIKE UPPER('%'+COALESCE(@searchword,FD.Name)+'%') OR 
	UPPER(LC.FirstName) LIKE UPPER('%'+COALESCE(@searchword,LC.FirstName)+'%') OR 
	UPPER(LC.LastName) LIKE UPPER('%'+COALESCE(@searchword,LC.LastName)+'%') OR
	UPPER(CASE WHEN FD.UpdatedBy = @userId THEN 'Me' ELSE (LC.FirstName + ' ' + LC.LastName) END) LIKE UPPER('%'+COALESCE(@searchword,'Me')+'%') OR
	UPPER(FORMAT(FD.UpdatedDate, 'dd, MMM yyyy hh:mm tt')) LIKE UPPER('%'+COALESCE(@searchword,FORMAT(FD.UpdatedDate, 'dd, MMM yyyy hh:mm tt'))+'%'))
	

UNION All
              SELECT 
              DD.DocumentId AS Id,
              DD.FolderId AS ParentFolderId,
              DVD.Id AS DocumentVersionId,
              DD.DocumentId,
              (SELECT CASE WHEN (SELECT Count(*) FROM Favorite FV WHERE FV.UserId=COALESCE(@userId,FV.UserId) AND FV.DocumentId=COALESCE(DD.DocumentId,FV.DocumentId)) >0 THEN Convert(Bit,1) ELSE Convert(Bit,0) END) AS IsFavorite,
              Name,
              DVD.DocumentName AS FileName,
              'v'+CAST(Format(DVD.DocumentVersion,'0.0') AS NVARCHAR(MAX)) AS Version,
              0 AS ChildCount,
              CAST(1 AS BIT) AS IsDocument
              ,DVD.DocumentType AS DocType,
              DVD.DocumentDisplaySize AS DocSize,
              CASE WHEN DS.Id IS NOT NULL THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS Shared,
              CASE WHEN DD.UpdatedBy = @userId THEN 'Me' ELSE (LC.FirstName + ' ' + LC.LastName) END AS ModifiedBy,
              CASE WHEN DD.UpdatedDate IS NOT NULL THEN FORMAT(DD.UpdatedDate, 'dd, MMM yyyy hh:mm tt')END AS ModifiedOn

              FROM DocumentVersionDetail DVD
              INNER JOIN DocumentDetail DD ON DD.DocumentId=DVD.DocumentId
              LEFT JOIN LoginCredential LC ON DD.UpdatedBy = LC.Id
              LEFT JOIN UserDocShareDetail DS ON DD.DocumentId = DS.DocumentId
              WHERE DVD.Id IN (SELECT DocumentVersionId  FROM @PublicFiles2)
              AND DD.IsActive=0 AND DD.IsDelete=1

			  -- Copy From Jasmin search code 

	AND (UPPER(DD.Name) LIKE UPPER('%'+COALESCE(@searchword,DD.Name)+'%') OR 
	UPPER('v'+CAST(Format(DVD.DocumentVersion,'0.0') AS NVARCHAR(MAX))) LIKE UPPER('%'+COALESCE(@searchword,CAST(DVD.DocumentVersion AS NVARCHAR(MAX)))+'%') OR
	UPPER(DVD.DocumentDisplaySize) LIKE UPPER('%'+COALESCE(@searchword,DVD.DocumentDisplaySize)+'%') OR
	UPPER(LC.FirstName) LIKE UPPER('%'+COALESCE(@searchword,LC.FirstName)+'%') OR 
	UPPER(LC.LastName) LIKE UPPER('%'+COALESCE(@searchword,LC.LastName)+'%') OR
	UPPER(CASE WHEN DD.UpdatedBy = @userId THEN 'Me' ELSE (LC.FirstName + ' ' + LC.LastName) END) LIKE UPPER('%'+COALESCE(@searchword,'Me')+'%') OR
	UPPER(FORMAT(DD.UpdatedDate, 'dd, MMM yyyy hh:mm tt')) LIKE UPPER('%'+COALESCE(@searchword,FORMAT(DD.UpdatedDate, 'dd, MMM yyyy hh:mm tt'))+'%'))

-----------------------------------------------------------------


  DROP Table #Temp2
  DROP Table #PublicFolders2
END


END



GO
/****** Object:  StoredProcedure [dbo].[uspGetDocSharedListByCabinet]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dhrumil Patel
-- Create date: 23-05-2018
-- Description:	Get shared user list by cabinet
-- =============================================
--uspGetDocSharedListByCabinet 3,1,'User'
CREATE PROCEDURE [dbo].[uspGetDocSharedListByCabinet]
	@userId AS BIGINT = 0,
	@cabinetId AS BIGINT = 0,
	@mode AS NVARCHAR(10) = ''
AS
BEGIN
	IF(@mode = 'User')
	BEGIN
		SELECT LC.Id AS UserId
			,ISNULL(Lc.FirstName,'') + ' ' + ISNULL(Lc.LastName,'') AS [Name]
		FROM UserDocSharePermission UDSP
		INNER JOIN LoginCredential LC ON LC.Id = UDSP.DocShareUserId
		WHERE ISNULL(LC.IsActive,0) = 1 AND ISNULL(LC.IsDelete, 0) = 0 
			AND UDSP.CabinetId = @cabinetId AND UDSP.UserId = @userId 
		ORDER BY [Name]
	END
	ELSE
	BEGIN
		SELECT GD.Id AS GroupId
			,GD.[Name]
		FROM UserDocSharePermission UDSP
		INNER JOIN GroupDetail GD ON GD.Id = UDSP.DocShareGroupId
		WHERE ISNULL(GD.IsActive,0) = 1 AND ISNULL(GD.IsDelete, 0) = 0 
			AND UDSP.CabinetId = @cabinetId AND UDSP.UserId = @userId 
		ORDER BY [Name]
	END
END



GO
/****** Object:  StoredProcedure [dbo].[uspGetDocShareUserListByUserId]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Jasmin Vohra>
-- Create date: <12032018>
-- Description:	<document share user list by userid and cabinetid>
-- =============================================
CREATE PROCEDURE [dbo].[uspGetDocShareUserListByUserId]
	@UserId AS BIGINT = NULL,
	@CabinetId AS BIGINT = 1
AS
BEGIN
	SELECT LC.Id AS UserId
		,ISNULL(Lc.FirstName,'') + '  ' + ISNULL(Lc.LastName,'') AS FullName
		,ISNULL(STUFF(
			(SELECT ','+ SM.Name 
				FROM UserRole ESD
				LEFT JOIN RoleMaster SM ON SM.Id = ESD.RoleId
				WHERE ESD.UserId = LC.Id
			    FOR XML PATH(''),TYPE
			).value('.','NVARCHAR(MAX)') ,1,1,''), 'All') AS RoleName	
    FROM LoginCredential LC
	LEFT JOIN UserDocSharePermission UCP ON LC.Id = UCP.DocShareUserId
	WHERE ISNULL(LC.IsActive,0) = 1 AND ISNULL(LC.IsDelete, 0) = 0 
	AND UCP.UserId = ISNULL(@UserId,UCP.UserId) AND UCP.CabinetId = ISNULL(@CabinetId,UCP.CabinetId)
END



GO
/****** Object:  StoredProcedure [dbo].[uspGetDocumentListForLink]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Sweta Patel> 
-- Create date: <20-06-2018>
-- Description:	<Get Document List For Link>
-- =============================================
-- exec uspGetDocumentListForLink 20,'10048',''
CREATE PROCEDURE [dbo].[uspGetDocumentListForLink] 
	@cabinetId AS BIGINT = null,
	@userId AS NVARCHAR(20) = null,
	@searchword AS NVARCHAR(MAX) = null
AS
BEGIN	
	SET NOCOUNT ON;
 --- Get Document Id From Created By  me
SELECT DD.DocumentId,
DD.Name,
DVD.DocumentType,
'( '+ (SELECT dbo.GetFullPath(DD.DocumentId,'D',20,default)) +' )' AS FullPath,
DD.Name + '  ' +'( '+ (SELECT dbo.GetFullPath(DD.DocumentId,'D',20,default)) +' )' AS DisplayText
 ,'<i class="fa fa-user"></i> '+ DD.Name + '  ' +'( '+ (SELECT dbo.GetFullPath(DD.DocumentId,'D',20,default)) +' )' AS HTMLText

 FROM DocumentDetail DD 
 LEFT JOIN  DocumentVersionDetail DVD ON DD.DocumentId=DVD.DocumentId AND DVD.IsCurrent=1
 LEFT JOIN FolderDetail FD ON FD.Id=DD.FolderId
 WHERE FD.IsActive=1 AND FD.IsDelete=0 AND FD.CabinetId=@cabinetId
 AND DD.IsActive=1 AND DD.IsDelete=0 AND DD.CreatedBy=@userId
 --AND (UPPER(DD.Name) LIKE UPPER('%'+COALESCE(@searchword,DD.Name)+'%')
 UNION ALL
 --- Get Documents From Share With Me Data
SELECT DD.DocumentId,
 DD.Name,
 DVD.DocumentType,
'( '+ (SELECT dbo.GetFullPath(DD.DocumentId,'D',20,1)) +' )' AS FullPath,
 DD.Name + '  ' +'( '+ (SELECT dbo.GetFullPath(DD.DocumentId,'D',20,default)) +' )' AS DisplayText
 ,'<i class="fa fa-user"></i> '+ DD.Name + '  ' +'( '+ (SELECT dbo.GetFullPath(DD.DocumentId,'D',20,default)) +' )' AS HTMLText
 FROM DocumentDetail DD 
 LEFT JOIN  DocumentVersionDetail DVD ON DD.DocumentId=DVD.DocumentId AND DVD.IsCurrent=1
 LEFT JOIN FolderDetail FD ON FD.Id=DD.FolderId
 WHERE FD.IsActive=1 AND FD.IsDelete=0 AND FD.CabinetId=@cabinetId
 AND DD.IsActive=1 AND DD.IsDelete=0 AND DD.DocumentId IN

 --- Get Document Id From Share With Me Data
 (
 SELECT DocumentId FROM UserDocShareDetail WHERE DocumentId>0 AND CabinetId=@cabinetId AND DocShareGroupId=0 AND DocShareUserId=@userId
 UNION ALL
 SELECT DocumentId FROM UserDocShareDetail WHERE DocumentId>0 AND CabinetId=@cabinetId 
 AND DocShareUserId=0 AND DocShareGroupId IN  
 (SELECT DISTINCT GroupId FROM GroupUserDetail WHERE UserId=@userId 
  AND GroupId IN (Select Id From GroupDetail WHERE CabinetId=@cabinetId AND IsActive=1 AND IsDelete=0))
 )
 --------Search Filter
  
	

---------------------------------------------------    

END



GO
/****** Object:  StoredProcedure [dbo].[uspGetEmployesCabinetFolderDetails]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Sweta Patel> 
-- Create date: <12-06-2018>
-- Description:	<Get Employees Cabinet Screen Folder List>
-- =============================================
-- exec uspGetEmployesCabinetFolderDetails 10,'10050',''
CREATE PROCEDURE [dbo].[uspGetEmployesCabinetFolderDetails] 
	@cabinetId AS BIGINT = null,
	@userId AS NVARCHAR(20) = null,
	@searchword AS NVARCHAR(MAX) = null
AS
BEGIN	
	SET NOCOUNT ON;

   SELECT
'FV' AS FolderType,
0 AS Id, 0 AS ParentFolderId, 0 AS DocumentVersionId, 0 AS	DocumentId,	CAST(0 AS BIT) AS IsFavorite	,
'Favorite' AS Name	, NULL AS FileName,	'-'  AS Version, 
 (SELECT CONVERT(INT,(TotalFolders+TotalFiles)) FROM dbo.GetFavoriteFolderItemsCount('',@UserId,'',@cabinetId)) AS ChildCount, 
 CAST(0 AS BIT) AS IsDocument, 'cus-user-favourite cabinet_grid' AS DocType,'-' AS DocSize,	
 CAST(0 AS BIT) AS Shared,'-' AS	ModifiedBy,'-' AS	ModifiedOn
 UNION ALL

 SELECT
 'SH' AS FolderType,
0 AS Id, 0 AS ParentFolderId, 0 AS DocumentVersionId, 0 AS	DocumentId,	CAST(0 AS BIT) AS IsFavorite	,
'Share With Me' AS Name	, NULL AS FileName,	'-'  AS Version, 
(SELECT CONVERT(INT,(TotalFolders+TotalFiles)) FROM dbo.GetSharedFolderItemsCount('',@UserId,'',@cabinetId)) AS ChildCount,
 CAST(0 AS BIT) AS IsDocument, 'cus-user-share cabinet_grid' AS DocType,'-' AS DocSize,	
 --folder cus
 CAST(0 AS BIT) AS Shared,'-' AS	ModifiedBy,'-' AS	ModifiedOn
 UNION ALL

 SELECT
 'PU' AS FolderType,
0 AS Id, 0 AS ParentFolderId, 0 AS DocumentVersionId, 0 AS	DocumentId,	CAST(0 AS BIT) AS IsFavorite	,
'Public' AS Name	, NULL AS FileName,	'-'  AS Version, 
(SELECT CONVERT(INT,(TotalFolders+TotalFiles)) FROM dbo.GetPublicFolderItemsCount('',@UserId,'',@cabinetId)) AS ChildCount,
 CAST(0 AS BIT) AS IsDocument, 'cus-user-public cabinet_grid' AS DocType,'-' AS DocSize,	
 CAST(0 AS BIT) AS Shared,'-' AS	ModifiedBy,'-' AS	ModifiedOn
 UNION ALL

SELECT 
'DF' AS FolderType,
FD.Id,
FD.ParentFolderId,
 0 AS DocumentVersionId,
              0 As DocumentId,
              (SELECT CASE WHEN (SELECT Count(*) FROM Favorite FV WHERE FV.UserId=COALESCE(@userId,FV.UserId) AND FV.FolderId=COALESCE(FD.Id,FV.FolderId)) >0 THEN Convert(Bit,1) ELSE Convert(Bit,0) END) AS IsFavorite,
              FD.Name,
              NULL AS FileName,
              '-' AS Version,
              --  (SELECT count(*) FROM FolderDetail
              --                              WHERE ParentFolderId = FD.Id AND IsActive = 1 AND IsDelete = 0)
              --+ (SELECT count(*) FROM DocumentDetail D 
              --                              WHERE D.FolderId = FD.Id AND D.IsActive = 1 AND D.IsDelete = 0)
              (SELECT CONVERT(INT,(TotalFolders+TotalFiles)) FROM dbo.GetFolderItemsCount(Convert(nvarchar(Max),FD.Id),@UserId,''))
			  AS ChildCount,  -- // This si immediate Child count

              CAST(0 AS BIT) AS IsDocument,'folder' AS DocType,'-' AS DocSize,
              CASE WHEN DS.Id IS NOT NULL THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS Shared,
              CASE WHEN FD.UpdatedBy = @userId THEN 'Me' ELSE (LC.FirstName + ' ' + LC.LastName) END AS ModifiedBy,
              CASE WHEN FD.UpdatedDate IS NOT NULL THEN FORMAT(FD.UpdatedDate, 'dd, MMM yyyy hh:mm tt')END AS ModifiedOn

              FROM  FolderDetail FD 
              LEFT JOIN LoginCredential LC ON FD.UpdatedBy = LC.Id
              LEFT JOIN UserDocShareDetail DS ON FD.Id = DS.FolderId
			  WHERE
			  
			    (UPPER(FD.Name) LIKE UPPER('%'+COALESCE(@searchword,FD.Name)+'%') OR 
	UPPER(LC.FirstName) LIKE UPPER('%'+COALESCE(@searchword,LC.FirstName)+'%') OR 
	UPPER(LC.LastName) LIKE UPPER('%'+COALESCE(@searchword,LC.LastName)+'%') OR
	UPPER(CASE WHEN FD.UpdatedBy = @userId THEN 'Me' ELSE (LC.FirstName + ' ' + LC.LastName) END) LIKE UPPER('%'+COALESCE(@searchword,'Me')+'%') OR
	UPPER(FORMAT(FD.UpdatedDate, 'dd, MMM yyyy hh:mm tt')) LIKE UPPER('%'+COALESCE(@searchword,FORMAT(FD.UpdatedDate, 'dd, MMM yyyy hh:mm tt'))+'%'))
	AND  FD.CabinetId =@cabinetId AND FD.ParentFolderId=0  AND FD.CreatedBy=@userId AND FD.IsDelete=0 AND FD.IsActive=1
 UNION ALL
	SELECT
	'RB' AS FolderType,
0 AS Id, 0 AS ParentFolderId, 0 AS DocumentVersionId, 0 AS	DocumentId,	CAST(0 AS BIT) AS IsFavorite	,
'Recycle Bin' AS Name	, NULL AS FileName,	'-'  AS Version,
(SELECT Count(*) FROM DeletedData WHERE IsRecycleBinDeleted = 0 AND DeletedBy = CONVERT(nvarchar(max), @userId) AND CabinetId = @cabinetId) AS	ChildCount, 
 CAST(0 AS BIT) AS IsDocument, 'cus-user-recycle cabinet_grid' AS DocType,'-' AS DocSize,	
 CAST(0 AS BIT) AS Shared,'-' AS	ModifiedBy,'-' AS	ModifiedOn
END




GO
/****** Object:  StoredProcedure [dbo].[uspGetFavoriteCountByUserCabinet]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sweta Patel
-- Create date: 24-05-2018
-- Description:	Get Total Count For Favorite Folder/Files cabinet & User Wise
-- =============================================
--uspGetFavoriteCountByUserCabinet 10048,1
CREATE PROCEDURE [dbo].[uspGetFavoriteCountByUserCabinet]
	@userId AS BIGINT = 0,
	@cabinetId AS BIGINT = 0
	--@RequestStatus INTEGER	OUTPUT
AS
BEGIN
DECLARE @RequestStatus INTEGER=0;

SELECT  @RequestStatus= ISNULL(Count(DISTINCT(FV.Id)) ,0) FROM Favorite FV
INNER JOIN FolderDetail FD ON FD.Id=FV.FolderId
WHERE FD.IsActive=1 AND FD.IsDelete=0 AND FD.CabinetId=@cabinetId AND FV.UserId=@userId

SELECT  @RequestStatus= @RequestStatus+ ISNULL(Count(DISTINCT(FV.Id)) ,0) FROM Favorite FV
INNER JOIN DocumentDetail DD ON DD.DocumentId=FV.DocumentId
INNER JOIN FolderDetail FD ON FD.Id=DD.FolderId
WHERE DD.IsActive=1 AND DD.IsDelete=0 AND FV.UserId=@userId
AND FD.CabinetId=@cabinetId

SELECT  @RequestStatus

--RETURN @RequestStatus	
END



GO
/****** Object:  StoredProcedure [dbo].[uspGetFavoriteFilesByCabinet]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dhrumil Patel
-- Create date: 05-06-2018
-- Description:	Get Favorite Files By User and Cabinet
-- =============================================
-- uspGetFavoriteFilesByCabinet 1,3,38
CREATE PROCEDURE [dbo].[uspGetFavoriteFilesByCabinet]
	@cabinetId AS BIGINT = 0,
	@userId AS NVARCHAR(20) = NULL,
	@folderId AS BIGINT = 0,
	@searchword AS NVARCHAR(MAX) = null
AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT FV.Id AS FavoriteId, FD.Id AS Id,FD.ParentFolderId,0 AS DocumentVersionId,
	0 AS DocumentId,
	Convert(Bit,1) AS IsFavorite,
	FD.Name,NULL AS FileName,'-' AS Version,
	(SELECT count(*) FROM FolderDetail WHERE ParentFolderId = FD.Id AND IsActive = 1 AND IsDelete = 0) + (SELECT count(*) FROM DocumentDetail WHERE FolderId = FD.Id AND IsActive = 1 AND IsDelete = 0) AS ChildCount,
	CAST(0 AS BIT) AS IsDocument,'folder' AS DocType,'-' AS DocSize--, CAST(0 AS BIT) AS Shared,
	,CASE WHEN DS.Id IS NOT NULL THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS Shared,
	CASE WHEN FD.UpdatedBy = @userId THEN 'Me' ELSE (LC.FirstName + ' ' + LC.LastName) END AS ModifiedBy,
	CASE
		WHEN FD.UpdatedDate IS NOT NULL THEN FORMAT(FD.UpdatedDate, 'dd, MMM yyyy hh:mm tt')
	END AS ModifiedOn
	FROM FolderDetail FD 
	INNER JOIN Favorite FV ON FV.FolderId = FD.Id
	LEFT JOIN LoginCredential LC ON FD.UpdatedBy = CAST(LC.Id AS NVARCHAR(50))
	LEFT JOIN UserDocShareDetail DS ON FD.Id = DS.FolderId AND DS.DocShareUserId = @userId
	LEFT JOIN GroupDetail GD on DS.DocShareGroupId = GD.Id AND GD.IsActive = 1 AND GD.IsDelete = 0
	LEFT JOIN GroupUserDetail GUD on GD.Id = GUD.GroupId AND GUD.UserId = @userId
	WHERE FD.IsActive = 1 AND FD.IsDelete = 0 AND FV.CabinetId = @cabinetId 
	AND FV.UserId = @userId
	AND FV.ParentId = @folderId
	AND (UPPER(FD.Name) LIKE UPPER('%'+COALESCE(@searchword,FD.Name)+'%') OR 
	UPPER(LC.FirstName) LIKE UPPER('%'+COALESCE(@searchword,LC.FirstName)+'%') OR 
	UPPER(LC.LastName) LIKE UPPER('%'+COALESCE(@searchword,LC.LastName)+'%') OR
	UPPER(CASE WHEN FD.UpdatedBy = @userId THEN 'Me' ELSE (LC.FirstName + ' ' + LC.LastName) END) LIKE UPPER('%'+COALESCE(@searchword,'Me')+'%') OR
	UPPER(FORMAT(FD.UpdatedDate, 'dd, MMM yyyy hh:mm tt')) LIKE UPPER('%'+COALESCE(@searchword,FORMAT(FD.UpdatedDate, 'dd, MMM yyyy hh:mm tt'))+'%'))
	
	UNION ALL
	
	SELECT DISTINCT FV.Id AS FavoriteId, DD.DocumentId AS Id,DD.FolderId AS ParentFolderId,DV.Id AS DocumentVersionId,
	DV.DocumentId AS DocumentId,
	Convert(Bit,1) AS IsFavorite,
	DD.Name,DV.DocumentName AS FileName,'v'+CAST(Format(DV.DocumentVersion,'0.0') AS NVARCHAR(MAX)) AS Version,0 AS ChildCount,CAST(1 AS BIT) AS IsDocument
	,DV.DocumentType AS DocType,DV.DocumentDisplaySize AS DocSize,
	CASE WHEN DS.Id IS NOT NULL THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS Shared,
	CASE WHEN DD.UpdatedBy = @userId THEN 'Me' ELSE (LC.FirstName + ' ' + LC.LastName) END AS ModifiedBy,
	CASE
		WHEN DD.UpdatedDate IS NOT NULL THEN FORMAT(DD.UpdatedDate, 'dd, MMM yyyy hh:mm tt')
	END AS ModifiedOn
	FROM DocumentDetail DD 
	INNER JOIN Favorite FV ON FV.DocumentId = DD.DocumentId
	LEFT JOIN WF_TransactionDocuments WFT ON DD.DocumentId = WFT.DocumentId 
	INNER JOIN (SELECT RANK() OVER (PARTITION BY DocumentId ORDER BY Id DESC) r, *
	FROM DocumentVersionDetail) DV ON DD.DocumentId = DV.DocumentId
	LEFT JOIN LoginCredential LC ON DD.UpdatedBy = LC.Id
	LEFT JOIN UserDocShareDetail DS ON DD.DocumentId = DS.DocumentId AND DS.DocShareUserId = @userId
	LEFT JOIN GroupDetail GD on DS.DocShareGroupId = GD.Id AND GD.IsActive = 1 AND GD.IsDelete = 0
	LEFT JOIN GroupUserDetail GUD on GD.Id = GUD.GroupId AND GUD.UserId = @userId
	WHERE DV.r = 1 AND DD.IsActive = 1 AND DD.IsDelete = 0 AND DV.IsActive = 1 AND DV.IsDelete = 0
	AND FV.UserId = @userId AND FV.CabinetId = @cabinetId
	AND FV.ParentId = @folderId
	AND (UPPER(DD.Name) LIKE UPPER('%'+COALESCE(@searchword,DD.Name)+'%') OR 
	UPPER('v'+CAST(Format(DV.DocumentVersion,'0.0') AS NVARCHAR(MAX))) LIKE UPPER('%'+COALESCE(@searchword,CAST(DV.DocumentVersion AS NVARCHAR(MAX)))+'%') OR
	UPPER(DV.DocumentDisplaySize) LIKE UPPER('%'+COALESCE(@searchword,DV.DocumentDisplaySize)+'%') OR
	UPPER(LC.FirstName) LIKE UPPER('%'+COALESCE(@searchword,LC.FirstName)+'%') OR 
	UPPER(LC.LastName) LIKE UPPER('%'+COALESCE(@searchword,LC.LastName)+'%') OR
	UPPER(CASE WHEN DD.UpdatedBy = @userId THEN 'Me' ELSE (LC.FirstName + ' ' + LC.LastName) END) LIKE UPPER('%'+COALESCE(@searchword,'Me')+'%') OR
	UPPER(FORMAT(DD.UpdatedDate, 'dd, MMM yyyy hh:mm tt')) LIKE UPPER('%'+COALESCE(@searchword,FORMAT(DD.UpdatedDate, 'dd, MMM yyyy hh:mm tt'))+'%'))

	--SELECT DISTINCT
	--	FD.Id AS Id
	--   ,FD.Name
	--   ,0 AS DocumentVersion
	--   ,'folder' AS DocumentType
	--   ,'-' AS DocumentDisplaySize
	--   ,ISNULL((LC.FirstName + ' ' + LC.LastName),'-') AS ModifiedBy
	--   ,CASE WHEN FD.UpdatedDate IS NOT NULL THEN FORMAT(FD.UpdatedDate, 'dd, MMM yyyy hh:mm tt') ELSE '-' END AS ModifiedOn
	--FROM FolderDetail FD
	--INNER JOIN Favorite FV ON FV.FolderId = FD.Id
	--LEFT JOIN LoginCredential LC ON FD.UpdatedBy = CAST(LC.Id AS NVARCHAR(50))
	--WHERE FD.IsActive = 1
	--	AND FD.IsDelete = 0
	--	AND CabinetId = ISNULL(@cabinetId,CabinetId) 
	--	AND FV.UserId = @userId

	--UNION ALL

	--SELECT DISTINCT DD.DocumentId AS DocumentId
	--	,DD.Name
	--	,DVD.DocumentVersion
	--	,DVD.DocumentType
	--	,DVD.DocumentDisplaySize
	--	,ISNULL((LC.FirstName + ' ' + LC.LastName),'-') AS ModifiedBy
	--	,CASE WHEN DVD.UpdatedDate IS NOT NULL THEN FORMAT(DVD.UpdatedDate, 'dd, MMM yyyy hh:mm tt') ELSE '-' END AS ModifiedOn
	--FROM FolderDetail FD
	--LEFT JOIN DocumentDetail DD ON DD.FolderId = FD.Id
	--INNER JOIN (SELECT RANK() OVER (PARTITION BY DocumentId ORDER BY Id DESC) r
	--	,*
	--	FROM DocumentVersionDetail) DVD ON DD.DocumentId = DVD.DocumentId
	--INNER JOIN Favorite FV ON FV.DocumentId = DVD.DocumentId
	--LEFT JOIN LoginCredential LC ON DVD.UpdatedBy = CAST(LC.Id AS NVARCHAR(50))
	--WHERE DVD.r = 1
	--	AND DD.IsActive = 1
	--	AND DD.IsDelete = 0
	--	AND CabinetId = ISNULL(@cabinetId,CabinetId) 
	--	AND FV.UserId = @userId
END
GO
/****** Object:  StoredProcedure [dbo].[uspGetFavoriteFolderFileInfo]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dhrumil Patel
-- Create date: 02-05-2018
-- Description:	Get folder and file information
-- MOdified By : Sweta Patel
-- Modified Date : 23-05-2018
-- Description : Total Folders,Total files & Size Up to all child code is updated
-- =============================================
--uspGetFavoriteFolderFileInfo 7,52
--uspGetFavoriteFolderFileInfo 3,0,10164
--uspGetFavoriteFolderFileInfo 3,0,0,'','10323,10324'
CREATE PROCEDURE [dbo].[uspGetFavoriteFolderFileInfo]	
	@UserId NVARCHAR(20) = 0,
	@FolderId BIGINT = 0,
	@DocumentId BIGINT = 0,			  --DocumentVersionDetail table id
	@FolderIds NVARCHAR(MAX) = '',
	@FileIds NVARCHAR(MAX) = '',       --DocumentDetail table ids
	@CabinetId BIGINT = 0
AS
BEGIN
	DECLARE @TodayDate BIGINT = YEAR(GETDATE())*10000+MONTH(GETDATE())*100+DAY(GETDATE())
	DECLARE @TotalFolders BIGINT = 0
	DECLARE @TotalSizeUnit NVARCHAR(MAX)
	IF(@FolderId != 0)
	BEGIN
		DECLARE @FileCount BIGINT = 0		
		DECLARE @FileSize  DECIMAL(18,2) = 0.00

		
		-----------------------Call function-------------------------------------
		--TotalFolders	TotalFiles	TotalItems	DisplaySize	TotalSize
		---------------------------------------------------------------------------
		
		SELECT @TotalFolders=TotalFolders,@FileCount=TotalFiles,@FileSize=TotalSize,@TotalSizeUnit=DisplaySize FROM dbo.GetFavoriteFolderItemsCount(@FolderId,@UserId,'',@CabinetId)
		------------------------------------------------------------------------------

		SELECT UDID
			,Id
			,[Name]
			,'Folder' AS InfoType
			,CASE WHEN CreatedBy = @UserId THEN 'me' Else (SELECT FirstName + ' ' + LastName FROM LoginCredential WHERE CONVERT(NVARCHAR(MAX),Id) = FD.CreatedBy) END AS [Owner]
			,CASE WHEN UpdatedBy = @UserId THEN 'me' Else (SELECT FirstName + ' ' + LastName FROM LoginCredential WHERE CONVERT(NVARCHAR(MAX),Id) = FD.UpdatedBy) END AS [ModifyBy]
			,(SELECT FORMAT(CreatedDate, 'dd, MMM yyyy HH:mm tt')) AS CreatedDate
			,(SELECT FORMAT(UpdatedDate, 'dd, MMM yyyy HH:mm tt')) AS ModifyDate
			,CONVERT(NVARCHAR(5), @TotalFolders) + ' folders' AS TotalFolders
			,CONVERT(NVARCHAR(5), @FileCount) + ' files' AS TotalFiles
			,@TotalSizeUnit AS FileSize
			--,CASE WHEN @FileSize>1024 THEN CONVERT(NVARCHAR(20),@FileSize/1024) + ' MB' ELSE CONVERT(NVARCHAR(20),@FileSize) + ' KB' END AS FileSize
			,[Description]
			,Keywords
			,PhysicalLocation
			 ,(SELECT [dbo].[GetFullPath](FD.Id,'F',@CabinetId,default))  AS OriginalLocation
			,Notes
			--,(SELECT count(*) FROM FolderDetail WHERE ParentFolderId = FD.Id AND IsActive = 1 AND IsDelete = 0 AND CreatedBy = @UserId) + (SELECT count(*) FROM DocumentDetail WHERE FolderId = FD.Id AND IsActive = 1 AND IsDelete = 0 AND CreatedBy = @UserId) AS TotalCount
			,(@TotalFolders + @FileCount) AS TotalCount
		FROM FolderDetail FD
		WHERE Id = @FolderId
	END
	ELSE IF(@DocumentId != 0)
	BEGIN		
		SELECT DD.UDID
			,DD.DocumentId AS Id
			,@DocumentId AS DocVersionId
			,[Name]
			,'File' AS InfoType
			,CASE WHEN DVD.CreatedBy = @UserId THEN 'me' Else (SELECT FirstName + ' ' + LastName FROM LoginCredential WHERE CONVERT(NVARCHAR(MAX),Id) = DVD.CreatedBy) END AS [Owner]
			,CASE WHEN DVD.UpdatedBy = @UserId THEN 'me' Else (SELECT FirstName + ' ' + LastName FROM LoginCredential WHERE CONVERT(NVARCHAR(MAX),Id) = DVD.UpdatedBy) END AS [ModifyBy]
			,(SELECT FORMAT(DVD.CreatedDate, 'dd, MMM yyyy HH:mm tt')) AS CreatedDate
			,(SELECT FORMAT(DVD.UpdatedDate, 'dd, MMM yyyy HH:mm tt')) AS ModifyDate
			,DVD.DocumentDisplaySize AS FileSize
			,[Description]
			,Keywords
			,PhysicalLocation
			 ,(SELECT [dbo].[GetFullPath](DD.DocumentId,'D',@CabinetId,default))  AS OriginalLocation
			,Notes
			,DocumentType AS [Type]
			,'v' + CAST(Format(DocumentVersion,'0.0') AS NVARCHAR(MAX)) AS [Version]
			,CASE WHEN Confidentiality = 1 THEN 'Low' ELSE CASE WHEN Confidentiality = 2 THEN 'Medium' ELSE 'High' END END AS Confidentiality
			,CASE WHEN [Language] = 1 THEN 'English' ELSE 'Arabic' END AS [Language]
			,(SELECT [Name] FROM CategoryMaster WHERE Id = DD.CategoryId) AS Category
			,(SELECT [Name] FROM SubCategoryMaster WHERE Id = DD.SubCategoryId) AS SubCategory
		FROM DocumentDetail DD
		LEFT JOIN DocumentVersionDetail DVD ON DVD.DocumentId = DD.DocumentId
		WHERE DVD.Id = @DocumentId
	END
	ELSE
	BEGIN


	-----------------------Call function-------------------------------------
		--TotalFolders	TotalFiles	TotalItems	DisplaySize	TotalSize
		---------------------------------------------------------------------------
		SELECT @TotalFolders=TotalFolders,@FileCount=TotalFiles,@FileSize=TotalSize ,@TotalSizeUnit=DisplaySize FROM dbo.GetFavoriteFolderItemsCount(@FolderIds,@UserId,@FileIds,@CabinetId)
		------------------------------------------------------------------------------


		SELECT 'Both' AS InfoType
			,CONVERT(NVARCHAR(5), @TotalFolders) + ' folders' AS TotalFolders
			,CONVERT(NVARCHAR(5), @FileCount) + ' files' AS TotalFiles
		     ,@TotalSizeUnit AS FileSize
			--,CASE WHEN @FileSize>1024 THEN CONVERT(NVARCHAR(20),@FileSize/1024) + ' MB' ELSE CONVERT(NVARCHAR(20),@FileSize) + ' KB' END AS FileSize
		
	END
END



GO
/****** Object:  StoredProcedure [dbo].[uspGetFolderDetails]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Jasmin Vohra> 
-- Create date: <12-02-2018>
-- Description:	<Get Folder list with cabinet and user id wise And also for some specific folder wise>
-- =============================================
-- exec uspGetFolderDetails 1,'10048',53,'pm'
CREATE PROCEDURE [dbo].[uspGetFolderDetails] 
	@cabinetId AS BIGINT = null,
	@userId AS NVARCHAR(20) = null,
	@folderId AS BIGINT = null,
	@searchword AS NVARCHAR(MAX) = null
AS
BEGIN	
	SET NOCOUNT ON;

    SELECT DISTINCT FD.Id AS Id,FD.ParentFolderId,0 AS DocumentVersionId,
	0 AS DocumentId,
	(SELECT CASE WHEN (SELECT Count(*) FROM Favorite FV WHERE FV.UserId=COALESCE(@userId,FV.UserId) AND FV.FolderId=COALESCE(FD.Id,FV.FolderId)) >0 THEN Convert(Bit,1) ELSE Convert(Bit,0) END) AS IsFavorite,
	FD.Name,NULL AS FileName,'-' AS Version,
	(SELECT count(*) FROM FolderDetail WHERE ParentFolderId = FD.Id AND IsActive = 1 AND IsDelete = 0) + (SELECT count(*) FROM DocumentDetail WHERE FolderId = FD.Id AND IsActive = 1 AND IsDelete = 0) AS ChildCount,
	CAST(0 AS BIT) AS IsDocument,'folder' AS DocType,'-' AS DocSize--, CAST(0 AS BIT) AS Shared,
	,CASE WHEN DS.Id IS NOT NULL THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS Shared,
	CASE WHEN FD.UpdatedBy = @userId THEN 'Me' ELSE (LC.FirstName + ' ' + LC.LastName) END AS ModifiedBy,
	CASE
		WHEN FD.UpdatedDate IS NOT NULL THEN FORMAT(FD.UpdatedDate, 'dd, MMM yyyy hh:mm tt')
	END AS ModifiedOn
	FROM FolderDetail FD LEFT JOIN DocumentDetail DD ON DD.FolderId = FD.Id
	LEFT JOIN LoginCredential LC ON FD.UpdatedBy = LC.Id
	LEFT JOIN UserDocShareDetail DS ON FD.Id = DS.FolderId
	WHERE FD.IsActive = 1 AND FD.IsDelete = 0 AND FD.CabinetId = ISNULL(@cabinetId,FD.CabinetId) 
	--AND FD.CreatedBy = ISNULL(@userId,FD.CreatedBy) 
	AND FD.ParentFolderId = ISNULL(@folderId,FD.ParentFolderId)
	AND (UPPER(FD.Name) LIKE UPPER('%'+COALESCE(@searchword,FD.Name)+'%') OR 
	UPPER(LC.FirstName) LIKE UPPER('%'+COALESCE(@searchword,LC.FirstName)+'%') OR 
	UPPER(LC.LastName) LIKE UPPER('%'+COALESCE(@searchword,LC.LastName)+'%') OR
	UPPER(CASE WHEN FD.UpdatedBy = @userId THEN 'Me' ELSE (LC.FirstName + ' ' + LC.LastName) END) LIKE UPPER('%'+COALESCE(@searchword,'Me')+'%') OR
	UPPER(FORMAT(FD.UpdatedDate, 'dd, MMM yyyy hh:mm tt')) LIKE UPPER('%'+COALESCE(@searchword,FORMAT(FD.UpdatedDate, 'dd, MMM yyyy hh:mm tt'))+'%'))
	
	UNION ALL
	
	SELECT DISTINCT DD.DocumentId AS Id,DD.FolderId AS ParentFolderId,DV.Id AS DocumentVersionId,
	DV.DocumentId AS DocumentId,
	(SELECT CASE WHEN (SELECT Count(*) FROM Favorite FV WHERE FV.UserId=COALESCE(@userId,FV.UserId) AND FV.DocumentId=COALESCE(DD.DocumentId,FV.DocumentId)) >0 THEN Convert(Bit,1) ELSE Convert(Bit,0) END) AS IsFavorite,
	Name,DV.DocumentName AS FileName,'v'+CAST(Format(DV.DocumentVersion,'0.0') AS NVARCHAR(MAX)) AS Version,0 AS ChildCount,CAST(1 AS BIT) AS IsDocument
	,DV.DocumentType AS DocType,DV.DocumentDisplaySize AS DocSize,
	CASE WHEN DS.Id IS NOT NULL THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS Shared,
	CASE WHEN DD.UpdatedBy = @userId THEN 'Me' ELSE (LC.FirstName + ' ' + LC.LastName) END AS ModifiedBy,
	CASE
		WHEN DD.UpdatedDate IS NOT NULL THEN FORMAT(DD.UpdatedDate, 'dd, MMM yyyy hh:mm tt')
	END AS ModifiedOn
	FROM DocumentDetail DD 
	LEFT JOIN WF_TransactionDocuments WFT ON DD.DocumentId = WFT.DocumentId 
	--INNER JOIN (SELECT RANK() OVER (PARTITION BY DocumentId ORDER BY Id DESC) r, *
	--FROM DocumentVersionDetail) DV ON DD.DocumentId = DV.DocumentId
	INNER JOIN DocumentVersionDetail DV ON DD.DocumentId = DV.DocumentId AND DV.IsCurrent = 1
	LEFT JOIN LoginCredential LC ON DD.UpdatedBy = LC.Id
	LEFT JOIN UserDocShareDetail DS ON DD.DocumentId = DS.DocumentId
	WHERE DD.IsActive = 1 AND DD.IsDelete = 0 AND DV.IsActive = 1 AND DV.IsDelete = 0 --AND DV.r = 1
	--AND DD.CreatedBy = ISNULL(@userId,DD.CreatedBy)	
	AND DD.FolderId = ISNULL(@folderId,DD.FolderId)
	AND (UPPER(DD.Name) LIKE UPPER('%'+COALESCE(@searchword,DD.Name)+'%') OR 
	UPPER('v'+CAST(Format(DV.DocumentVersion,'0.0') AS NVARCHAR(MAX))) LIKE UPPER('%'+COALESCE(@searchword,CAST(DV.DocumentVersion AS NVARCHAR(MAX)))+'%') OR
	UPPER(DV.DocumentDisplaySize) LIKE UPPER('%'+COALESCE(@searchword,DV.DocumentDisplaySize)+'%') OR
	UPPER(LC.FirstName) LIKE UPPER('%'+COALESCE(@searchword,LC.FirstName)+'%') OR 
	UPPER(LC.LastName) LIKE UPPER('%'+COALESCE(@searchword,LC.LastName)+'%') OR
	UPPER(CASE WHEN DD.UpdatedBy = @userId THEN 'Me' ELSE (LC.FirstName + ' ' + LC.LastName) END) LIKE UPPER('%'+COALESCE(@searchword,'Me')+'%') OR
	UPPER(FORMAT(DD.UpdatedDate, 'dd, MMM yyyy hh:mm tt')) LIKE UPPER('%'+COALESCE(@searchword,FORMAT(DD.UpdatedDate, 'dd, MMM yyyy hh:mm tt'))+'%'))
END



--SELECT DISTINCT FD.Id AS Id,FD.ParentFolderId,0 AS DocumentVersionId,FD.Name,CAST(0 AS BIT) AS IsDocument,
--	CASE WHEN DD.DocumentId IS NOT NULL THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS HasDocument,CAST(0 AS BIT) AS IsAssigned
--	,'fol' AS jsType
--	FROM FolderDetail FD LEFT JOIN DocumentDetail DD ON DD.FolderId = FD.Id
--	WHERE FD.IsActive = 1 AND FD.IsDelete = 0 AND CabinetId = ISNULL(@cabinetId,CabinetId) 
--	AND FD.CreatedBy = ISNULL(@userId,FD.CreatedBy) AND FD.ParentFolderId = ISNULL(@folderId,FD.ParentFolderId)
--	UNION ALL
--	SELECT DISTINCT DD.DocumentId AS Id,DD.FolderId AS ParentFolderId,DV.Id AS DocumentVersionId,
--	Name + '('+ CAST(DV.DocumentVersion AS NVARCHAR(MAX)) + ')' ,CAST(1 AS BIT) AS IsDocument, CAST(0 AS BIT),
--	CASE WHEN WFT.WFTransactionId IS NOT NULL  THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS IsAssigned
--	,DV.DocumentType AS jsType
--	FROM DocumentDetail DD 
--	LEFT JOIN WF_TransactionDocuments WFT ON DD.DocumentId = WFT.DocumentId 
--	INNER JOIN (SELECT RANK() OVER (PARTITION BY DocumentId ORDER BY Id DESC) r, *
--	FROM DocumentVersionDetail) DV ON DD.DocumentId = DV.DocumentId
--	WHERE DV.r = 1 AND DD.IsActive = 1 AND DD.IsDelete = 0 AND DV.IsActive = 1 AND DV.IsDelete = 0
--	AND DD.CreatedBy = ISNULL(@userId,DD.CreatedBy)	AND DD.FolderId = ISNULL(@folderId,DD.FolderId)
GO
/****** Object:  StoredProcedure [dbo].[uspGetFolderFileComments]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dhrumil Patel
-- Create date: 03-05-2018
-- Description:	Get folder and file comments
-- Update By: Sweta Patel
-- Update Date : 13/06/2018
-- Description :Update the Comment By  Name issue
-- =============================================
--uspGetFolderFileComments 0,29
CREATE PROCEDURE [dbo].[uspGetFolderFileComments]
	@FolderId BIGINT = 0,
	@DocumentId BIGINT = 0,
    @UserId NVARCHAR(20) = '0'
AS
BEGIN
	DECLARE @TodayDate BIGINT = YEAR(GETDATE())*10000+MONTH(GETDATE())*100+DAY(GETDATE())
	DECLARE @FileName NVARCHAR(100) = '', @FileType NVARCHAR(50) = ''

	IF(@FolderId != 0)
	BEGIN
		SELECT @FileName = Name
		FROM FolderDetail
		WHERE Id = @FolderId

		IF((SELECT COUNT(*) FROM FolderFileComments WHERE FolderId = @FolderId) > 0)
		BEGIN
			SELECT (CONVERT(NVARCHAR(20), LC.Id) + '/' + LC.ProfileImage) AS ImageName
				,@FileName AS [Name]
				,'' AS DocumentType
				,@FolderId As Id

				,CASE WHEN FFC.CreatedBy = @UserId THEN 'me' Else (SELECT FirstName + ' ' + LastName FROM LoginCredential WHERE CONVERT(NVARCHAR(MAX),Id) = FFC.CreatedBy) END AS FullName
				
				,Comment
				,CASE WHEN FFC.CreatedDateInt = @TodayDate
					THEN
						(SELECT FORMAT(FFC.CreatedDate, 'HH:mm tt'))
					ELSE
						(SELECT FORMAT(FFC.CreatedDate, 'dd, MMM yyyy HH:mm tt'))
					END AS CommentDateTime
			FROM FolderFileComments  FFC
			INNER JOIN LoginCredential LC ON LC.Id = FFC.CommentBy
			WHERE FFC.FolderId = @FolderId
			ORDER BY FFC.Id DESC
		END
		ELSE
		BEGIN
			SELECT [Name]
				,'' AS DocumentType
				,Id
			FROM FolderDetail
			WHERE Id = @FolderId
		END
	END
	ELSE IF(@DocumentId != 0)
	BEGIN	
		DECLARE @Id BIGINT = 0, @VersionId BIGINT = 0
		SELECT TOP 1 @FileName = DD.Name
			,@FileType = DVD.DocumentType
			,@Id = DD.DocumentId
			,@VersionId = DVD.Id
		FROM DocumentDetail DD
		LEFT JOIN DocumentVersionDetail DVD ON DD.DocumentId = DVD.DocumentId
		WHERE DVD.Id = @DocumentId
		ORDER BY 1 DESC
		
		IF((SELECT COUNT(*) FROM FolderFileComments WHERE DocumentId = @Id) > 0)
		BEGIN
			SELECT (CONVERT(NVARCHAR(20), LC.Id) + '/' + LC.ProfileImage) AS ImageName
				,@FileName AS [Name]
				,@FileType AS DocumentType
				,@Id AS Id
				,@VersionId AS DocVersionId
				,CASE WHEN FFC.CreatedBy = FFC.CommentBy
					THEN
						'Me'
					ELSE
						LC.FirstName + ' ' + LC.LastName 
					END AS FullName
				,Comment
				,CASE WHEN FFC.CreatedDateInt = @TodayDate
					THEN
						(SELECT FORMAT(FFC.CreatedDate, 'HH:mm tt'))
					ELSE
						(SELECT FORMAT(FFC.CreatedDate, 'dd, MMM yyyy HH:mm tt'))
					END AS CommentDateTime
			FROM FolderFileComments  FFC
			INNER JOIN LoginCredential LC ON LC.Id = FFC.CommentBy
			WHERE FFC.DocumentId = @Id
			ORDER BY FFC.Id DESC
		END
		ELSE
		BEGIN
			SELECT TOP 1 DD.[Name]
			,DVD.DocumentType
			,DVD.DocumentId AS Id
			,DVD.Id AS DocVersionId
			FROM DocumentDetail DD
			LEFT JOIN DocumentVersionDetail DVD ON DD.DocumentId = DVD.DocumentId
			WHERE DVD.Id = @DocumentId
			ORDER BY 1 DESC
		END
	END
END



GO
/****** Object:  StoredProcedure [dbo].[uspGetFolderFileInfo]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dhrumil Patel
-- Create date: 02-05-2018
-- Description:	Get folder and file information
-- MOdified By : Sweta Patel
-- Modified Date : 23-05-2018
-- Description : Total Folders,Total files & Size Up to all child code is updated
-- =============================================
--uspGetFolderFileInfo 7,52
--uspGetFolderFileInfo 3,0,10164
--uspGetFolderFileInfo 3,0,0,'','10323,10324'
CREATE PROCEDURE [dbo].[uspGetFolderFileInfo]	
	@UserId NVARCHAR(20) = 0,
	@FolderId BIGINT = 0,
	@DocumentId BIGINT = 0,			  --DocumentVersionDetail table id
	@FolderIds NVARCHAR(MAX) = '',
	@FileIds NVARCHAR(MAX) = ''   ,--DocumentDetail table ids
   @CabinetId BIGINT = 0    
AS
BEGIN
	DECLARE @TodayDate BIGINT = YEAR(GETDATE())*10000+MONTH(GETDATE())*100+DAY(GETDATE())
	DECLARE @TotalFolders BIGINT = 0
	DECLARE @TotalSizeUnit NVARCHAR(MAX)
	IF(@FolderId != 0)
	BEGIN
		DECLARE @FileCount BIGINT = 0		
		DECLARE @FileSize  DECIMAL(18,2) = 0.00

		
		-----------------------Call function-------------------------------------
		--TotalFolders	TotalFiles	TotalItems	DisplaySize	TotalSize
		---------------------------------------------------------------------------
		
		SELECT @TotalFolders=TotalFolders,@FileCount=TotalFiles,@FileSize=TotalSize,@TotalSizeUnit=DisplaySize FROM dbo.GetFolderItemsCount(@FolderId,@UserId,'')
		------------------------------------------------------------------------------

		SELECT UDID
			,Id
			,[Name]
			,'Folder' AS InfoType
			,CASE WHEN CreatedBy = @UserId THEN 'me' Else (SELECT FirstName + ' ' + LastName FROM LoginCredential WHERE CONVERT(NVARCHAR(MAX),Id) = FD.CreatedBy) END AS [Owner]
			,CASE WHEN UpdatedBy = @UserId THEN 'me' Else (SELECT FirstName + ' ' + LastName FROM LoginCredential WHERE CONVERT(NVARCHAR(MAX),Id) = FD.UpdatedBy) END AS [ModifyBy]
			,(SELECT FORMAT(CreatedDate, 'dd, MMM yyyy HH:mm tt')) AS CreatedDate
			,(SELECT FORMAT(UpdatedDate, 'dd, MMM yyyy HH:mm tt')) AS ModifyDate
			,CONVERT(NVARCHAR(5), @TotalFolders) + ' folders' AS TotalFolders
			,CONVERT(NVARCHAR(5), @FileCount) + ' files' AS TotalFiles
			,@TotalSizeUnit AS FileSize
			--,CASE WHEN @FileSize>1024 THEN CONVERT(NVARCHAR(20),@FileSize/1024) + ' MB' ELSE CONVERT(NVARCHAR(20),@FileSize) + ' KB' END AS FileSize
			,[Description]
			,Keywords
			,PhysicalLocation
			 ,(SELECT [dbo].[GetFullPath](FD.Id,'F',@CabinetId,default))  AS OriginalLocation
			,Notes
			--,(SELECT count(*) FROM FolderDetail WHERE ParentFolderId = FD.Id AND IsActive = 1 AND IsDelete = 0 AND CreatedBy = @UserId) + (SELECT count(*) FROM DocumentDetail WHERE FolderId = FD.Id AND IsActive = 1 AND IsDelete = 0 AND CreatedBy = @UserId) AS TotalCount
			,(@TotalFolders + @FileCount) AS TotalCount
		FROM FolderDetail FD
		WHERE Id = @FolderId
	END
	ELSE IF(@DocumentId != 0)
	BEGIN		
		SELECT DD.UDID
			,DD.DocumentId AS Id
			,@DocumentId AS DocVersionId
			,[Name]
			,'File' AS InfoType
			,CASE WHEN DVD.CreatedBy = @UserId THEN 'me' Else (SELECT FirstName + ' ' + LastName FROM LoginCredential WHERE CONVERT(NVARCHAR(MAX),Id) = DVD.CreatedBy) END AS [Owner]
			,CASE WHEN DVD.UpdatedBy = @UserId THEN 'me' Else (SELECT FirstName + ' ' + LastName FROM LoginCredential WHERE CONVERT(NVARCHAR(MAX),Id) = DVD.UpdatedBy) END AS [ModifyBy]
			,(SELECT FORMAT(DVD.CreatedDate, 'dd, MMM yyyy HH:mm tt')) AS CreatedDate
			,(SELECT FORMAT(DVD.UpdatedDate, 'dd, MMM yyyy HH:mm tt')) AS ModifyDate
			,DVD.DocumentDisplaySize AS FileSize
			,[Description]
			,Keywords
			,PhysicalLocation
			 ,(SELECT [dbo].[GetFullPath](DD.DocumentId,'D',@CabinetId,default))  AS OriginalLocation
			,Notes
			,DocumentType AS [Type]
			,'v' + CAST(Format(DocumentVersion,'0.0') AS NVARCHAR(MAX)) AS [Version]
			,CASE WHEN Confidentiality = 1 THEN 'Low' ELSE CASE WHEN Confidentiality = 2 THEN 'Medium' ELSE 'High' END END AS Confidentiality
			,CASE WHEN [Language] = 1 THEN 'English' ELSE 'Arabic' END AS [Language]
			,(SELECT [Name] FROM CategoryMaster WHERE Id = DD.CategoryId) AS Category
			,(SELECT [Name] FROM SubCategoryMaster WHERE Id = DD.SubCategoryId) AS SubCategory
		FROM DocumentDetail DD
		LEFT JOIN DocumentVersionDetail DVD ON DVD.DocumentId = DD.DocumentId
		WHERE DVD.Id = @DocumentId
	END
	ELSE
	BEGIN


	-----------------------Call function-------------------------------------
		--TotalFolders	TotalFiles	TotalItems	DisplaySize	TotalSize
		---------------------------------------------------------------------------
		SELECT @TotalFolders=TotalFolders,@FileCount=TotalFiles,@FileSize=TotalSize ,@TotalSizeUnit=DisplaySize FROM dbo.GetFolderItemsCount(@FolderIds,@UserId,@FileIds)
		------------------------------------------------------------------------------


		SELECT 'Both' AS InfoType
			,CONVERT(NVARCHAR(5), @TotalFolders) + ' folders' AS TotalFolders
			,CONVERT(NVARCHAR(5), @FileCount) + ' files' AS TotalFiles
		     ,@TotalSizeUnit AS FileSize
			--,CASE WHEN @FileSize>1024 THEN CONVERT(NVARCHAR(20),@FileSize/1024) + ' MB' ELSE CONVERT(NVARCHAR(20),@FileSize) + ' KB' END AS FileSize
		
	END
END



GO
/****** Object:  StoredProcedure [dbo].[uspGetFolderFileInfoOld]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dhrumil Patel
-- Create date: 02-05-2018
-- Description:	Get folder and file information
-- =============================================
--uspGetFolderFileInfoOld 7,52
--uspGetFolderFileInfoOld 1,0,8
--uspGetFolderFileInfoOld 1,0,0,'39,39','10,10'
CREATE PROCEDURE [dbo].[uspGetFolderFileInfoOld]	
	@UserId NVARCHAR(20) = 0,
	@FolderId BIGINT = 0,
	@DocumentId BIGINT = 0,
	@FolderIds NVARCHAR(MAX) = '',
	@FileIds NVARCHAR(MAX) = ''
AS
BEGIN
	DECLARE @TodayDate BIGINT = YEAR(GETDATE())*10000+MONTH(GETDATE())*100+DAY(GETDATE())

	IF(@FolderId != 0)
	BEGIN
		DECLARE @FileCount BIGINT = 0
		DECLARE @FileSize  DECIMAL(18,2) = 0.00
		SELECT @FileCount = COUNT(*)
			,@FileSize = SUM(DVD.DocumentSize)
		FROM DocumentDetail DD
		LEFT JOIN DocumentVersionDetail DVD ON DVD.DocumentId = DD.DocumentId
		WHERE FolderId = @FolderId AND DD.CreatedBy = @UserId
			AND DD.IsActive = 1 AND DD.IsDelete = 0
		
		SELECT UDID
			,Id
			,[Name]
			,'Folder' AS InfoType
			,CASE WHEN CreatedBy = @UserId THEN 'me' Else (SELECT FirstName + ' ' + LastName FROM LoginCredential WHERE Id = FD.CreatedBy) END AS [Owner]
			,CASE WHEN UpdatedBy = @UserId THEN 'me' Else (SELECT FirstName + ' ' + LastName FROM LoginCredential WHERE Id = FD.UpdatedBy) END AS [ModifyBy]
			,(SELECT FORMAT(CreatedDate, 'dd, MMM yyyy HH:mm tt')) AS CreatedDate
			,(SELECT FORMAT(UpdatedDate, 'dd, MMM yyyy HH:mm tt')) AS ModifyDate
			,CONVERT(NVARCHAR(5), (SELECT COUNT(*) FROM FolderDetail WHERE ParentFolderId = @FolderId)) + ' folders' AS TotalFolders
			,CONVERT(NVARCHAR(5), @FileCount) + ' files' AS TotalFiles
			,CASE WHEN @FileSize>1024 THEN CONVERT(NVARCHAR(20),@FileSize/1024) + ' MB' ELSE CONVERT(NVARCHAR(20),@FileSize) + ' KB' END AS FileSize
			,[Description]
			,Keywords
			,PhysicalLocation
			,Notes
			,(SELECT count(*) FROM FolderDetail WHERE ParentFolderId = FD.Id AND IsActive = 1 AND IsDelete = 0 AND CreatedBy = @UserId) + (SELECT count(*) FROM DocumentDetail WHERE FolderId = FD.Id AND IsActive = 1 AND IsDelete = 0 AND CreatedBy = @UserId) AS TotalCount
		FROM FolderDetail FD
		WHERE Id = @FolderId
	END
	ELSE IF(@DocumentId != 0)
	BEGIN		
		SELECT DD.UDID
			,DVD.Id
			,[Name]
			,'File' AS InfoType
			,CASE WHEN DVD.CreatedBy = @UserId THEN 'me' Else (SELECT FirstName + ' ' + LastName FROM LoginCredential WHERE Id = DVD.CreatedBy) END AS [Owner]
			,CASE WHEN DVD.UpdatedBy = @UserId THEN 'me' Else (SELECT FirstName + ' ' + LastName FROM LoginCredential WHERE Id = DVD.UpdatedBy) END AS [ModifyBy]
			,(SELECT FORMAT(DVD.CreatedDate, 'dd, MMM yyyy HH:mm tt')) AS CreatedDate
			,(SELECT FORMAT(DVD.UpdatedDate, 'dd, MMM yyyy HH:mm tt')) AS ModifyDate
			,DVD.DocumentDisplaySize AS FileSize
			,[Description]
			,Keywords
			,PhysicalLocation
			,Notes
			,DocumentType AS [Type]
			,'v' + CAST(Format(DocumentVersion,'0.0') AS NVARCHAR(MAX)) AS [Version]
			,CASE WHEN Confidentiality = 1 THEN 'Low' ELSE CASE WHEN Confidentiality = 2 THEN 'Medium' ELSE 'High' END END AS Confidentiality
			,CASE WHEN [Language] = 1 THEN 'English' ELSE 'Arabic' END AS [Language]
			,(SELECT [Name] FROM CategoryMaster WHERE Id = DD.CategoryId) AS Category
			,(SELECT [Name] FROM SubCategoryMaster WHERE Id = DD.SubCategoryId) AS SubCategory
		FROM DocumentDetail DD
		LEFT JOIN DocumentVersionDetail DVD ON DVD.DocumentId = DD.DocumentId
		WHERE DVD.Id = @DocumentId
	END
	ELSE
	BEGIN
		SELECT 'Both' AS InfoType
			,CASE WHEN @FolderIds != '' 
				THEN 
					CONVERT(NVARCHAR(5), (SELECT COUNT(*) FROM FolderDetail WHERE ParentFolderId in (SELECT LTRIM(value) FROM dbo.Split(@FolderIds,',')))) + ' folders'
				ELSE
					'0 folder'
				END AS TotalFolders
			,CONVERT(NVARCHAR(5), COUNT(*)) + ' files' AS TotalFiles
			,CASE WHEN SUM(DVD.DocumentSize)>1024 THEN CONVERT(NVARCHAR(20),CONVERT(DECIMAL(18,2), SUM(DVD.DocumentSize)/1024)) + ' MB' ELSE CONVERT(NVARCHAR(20),SUM(DVD.DocumentSize)) + ' KB' END AS FileSize
		FROM DocumentDetail DD
		LEFT JOIN DocumentVersionDetail DVD ON DVD.DocumentId = DD.DocumentId
		WHERE (DD.FolderId in (SELECT LTRIM(value) FROM dbo.Split(@FolderIds,','))
			OR DVD.Id in (SELECT LTRIM(value) FROM dbo.Split(@FileIds,',')))
			AND DD.IsActive = 1 AND DD.IsDelete = 0
	END
END



GO
/****** Object:  StoredProcedure [dbo].[uspGetFormTemplateListByCabinet]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		SWETA Patel
-- Create date: 10-04-2018
-- Description:	Get Form templat list by cabinet id
-- =============================================
--uspGetUserListByCabinet 1,2
CREATE PROCEDURE [dbo].[uspGetFormTemplateListByCabinet]
	@CabinetId AS BIGINT = 0,
	@Mode AS NVARCHAR(10) = ''
AS
BEGIN
	IF (@Mode='Delete')
	BEGIN
		SELECT FT.Id 
			,FT.FormTitle
			,FT.Description
			,FT.IsActive
		FROM FormTemplate FT
		LEFT JOIN FormCabinetDetail FCD ON FT.Id = FCD.FormId
		WHERE ISNULL(FT.IsDelete, 0) = 1 
			AND FCD.CabinetId = @cabinetId 
			ORDER BY FT.UpdatedDateInt DESC
	END
	ELSE
	BEGIN
		SELECT FT.Id 
			,FT.FormTitle
			,FT.Description
			,FT.IsActive
		FROM FormTemplate FT
		LEFT JOIN FormCabinetDetail FCD ON FT.Id = FCD.FormId
		WHERE ISNULL(FT.IsDelete, 0) = 0 
			AND FCD.CabinetId = @cabinetId 
			ORDER BY FT.ID DESC
	END
END



GO
/****** Object:  StoredProcedure [dbo].[uspGetParentLinkDocument]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sweta Patel
-- ALTER date: 27-06-2018
-- Description:	Get All Parent Link For selected Document
-- =============================================
--uspGetParentLinkDocument 7,30496,1
--uspGetParentLinkDocument 7,30497,1
--uspGetParentLinkDocument 7,30500,1

CREATE  PROCEDURE [dbo].[uspGetParentLinkDocument]	
	@UserId NVARCHAR(20) = 0,
	@DocumentId BIGINT = 0,			  --DocumentDetail table id
    @CabinetId BIGINT = 0    
AS
BEGIN

SELECT
DD.DocumentId,
DVD.Id AS DocumentVersionId,
CASE WHEN DD.CreatedBy = @UserId THEN 'Me' ELSE (LC1.FirstName + ' ' + LC1.LastName) END AS DocumentOwner,
CASE WHEN DVD.CreatedBy = @UserId THEN 'Me' ELSE (LC2.FirstName + ' ' + LC2.LastName) END AS DocumentVersionOwner
, DVD.DocumentType
, DVD.IsCurrent
, DVD.IsPublic
, (SELECT [dbo].[GetFullPath](DD.DocumentId,'D',1,default))  AS OriginalLocation
 
FROM LinkDocument LD
INNER JOIN DocumentDetail DD ON DD.DocumentId=LD.ParentId
INNER JOIN DocumentVersionDetail DVD ON DD.DocumentId=DVD.DocumentId 
LEFT JOIN LoginCredential LC1 ON LC1.Id=DD.CreatedBy
LEFT JOIN LoginCredential LC2 ON LC2.Id=DVD.CreatedBy
WHERE LD.ChildId=@DocumentId AND LD.CabinetId=@CabinetId
AND DD.IsActive=1 AND DD.IsDelete=0
AND DVD.IsActive=1 AND DVD.IsDelete=0 AND DVD.IsCurrent=1

END




GO
/****** Object:  StoredProcedure [dbo].[uspGetPublicFilesByCabinet]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<THis is required when Showing or updating left menu count>
-- =============================================
CREATE PROCEDURE [dbo].[uspGetPublicFilesByCabinet]
@cabinetId AS BIGINT = NULL,
@userId AS NVARCHAR(20) = NULL
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

SELECT DISTINCT
	FD.Id AS Id
   ,FD.Name
   ,0 AS DocumentVersion
   ,'folder' AS DocumentType
   ,'-' AS DocumentDisplaySize
   ,ISNULL((LC.FirstName + ' ' + LC.LastName),'-') AS ModifiedBy
   ,CASE WHEN FD.UpdatedDate IS NOT NULL THEN FORMAT(FD.UpdatedDate, 'dd, MMM yyyy hh:mm tt') ELSE '-' END AS ModifiedOn
FROM FolderDetail FD
LEFT JOIN LoginCredential LC
	ON FD.UpdatedBy = CAST(LC.Id AS NVARCHAR(50))
WHERE FD.IsActive = 1
AND FD.IsDelete = 0
AND FD.IsPublic = 1
AND CabinetId = ISNULL(@cabinetId,CabinetId) 
--AND FD.CreatedBy != @userId

UNION ALL

SELECT
DD.DocumentId AS DocumentId
   ,DD.Name
   ,DVD.DocumentVersion
   ,DVD.DocumentType
   ,DVD.DocumentDisplaySize
   ,ISNULL((LC.FirstName + ' ' + LC.LastName),'-') AS ModifiedBy
   ,CASE WHEN DVD.UpdatedDate IS NOT NULL THEN FORMAT(DVD.UpdatedDate, 'dd, MMM yyyy hh:mm tt') ELSE '-' END AS ModifiedOn
 FROM DocumentDetail DD 
LEFT JOIN DocumentVersionDetail DVD  on DD.DocumentId=DVD.DocumentId
LEFT JOIN FolderDetail FD ON FD.Id=DD.FolderId
LEFT JOIN LoginCredential LC ON LC.Id =DVD.CreatedBy
WHERE FD.IsActive=1 AND FD.IsDelete=0 AND FD.CabinetId=1
AND DD.IsActive=1 AND DD.IsDelete=0 
AND DVD.IsCurrent=1 AND DVD.IsPublic=1 
--AND DD.CreatedBy != @userId

END

GO
/****** Object:  StoredProcedure [dbo].[uspGetPublicFolderDetails]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Sweta Patel> 
-- Create date: <08-06-2018>
-- Description:	<Get Folder list with cabinet In Public Menu>
-- =============================================
-- exec uspGetPublicFolderDetails 1,'3',30207,''
-- exec uspGetPublicFolderDetails 20,'10048',0,'about'
-- exec uspGetPublicFolderDetails 20,'10048',0,'Te'
CREATE PROCEDURE [dbo].[uspGetPublicFolderDetails] 
	@cabinetId AS BIGINT = null,
	@userId AS NVARCHAR(20) = null,
	@folderId AS BIGINT = null,
	@searchword AS NVARCHAR(MAX) = null

AS
BEGIN	
	SET NOCOUNT ON;

If(@FolderId=0)
BEGIN

DECLARE @PublicFolderId TABLE (FolderId BIGINT)
DECLARE @PublicFiles TABLE (DocumentVersionId BIGINT)

SELECT *
INTO   #PublicFolders
FROM FolderDetail WHERE CabinetId=@CabinetId AND IsPublic=1
SELECT *
INTO   #Temp
FROM FolderDetail WHERE CabinetId=@CabinetId AND IsPublic=1

Declare @Id BIGINT
Declare @Parent BIGINT

While (Select Count(*) From #Temp) > 0
BEGIN
    SELECT Top 1 @Id = Id From #Temp
    SELECT Top 1 @Parent = ParentFolderId FROM #Temp

   IF((SELECT count(*) FROM #PublicFolders Where Id=@Parent)=0)
   BEGIN
              INSERT  INTO @PublicFolderId VALUES  (@Id)
   END

   DELETE #Temp WHERE Id = @Id -- delete 1 by 1 row

END

--------------------------------------------------------
INSERT INTO @PublicFiles ( DocumentVersionId )
(
			  SELECT  DVD.Id FROM  DocumentVersionDetail DVD
              WHERE DVD.IsPublic=1 
                           AND @cabinetId=(SELECT TOP 1 CabinetId FROM dbo.GetCabinetFolderForDocument(DVD.DocumentId))
                        AND 0= (SELECT TOP 1 FolderIsPublic  -- sweta just change
						 FROM dbo.GetCabinetFolderForDocument(DVD.DocumentId))
                        AND DVD.IsActive=1 AND DVD.IsDelete=0 AND  DVD.IsCurrent=1)

  SELECT 
              F.FolderId AS Id,
              FD.ParentFolderId,
              0 AS DocumentVersionId,
              0 As DocumentId,
              (SELECT CASE WHEN (SELECT Count(*) FROM Favorite FV WHERE FV.UserId=COALESCE(@userId,FV.UserId) AND FV.FolderId=COALESCE(F.FolderId,FV.FolderId)) >0 THEN Convert(Bit,1) ELSE Convert(Bit,0) END) AS IsFavorite,
              FD.Name,
              NULL AS FileName,
              '-' AS Version,
                (SELECT count(*) FROM FolderDetail
                                            WHERE ParentFolderId = FD.Id AND IsActive = 1 AND IsDelete = 0 AND IsPublic=1)
              + (SELECT count(*) FROM DocumentDetail D 
                                            WHERE D.FolderId = FD.Id AND D.IsActive = 1 AND D.IsDelete = 0 AND 
                                            (SELECT TOP 1 IsPublic FROM DocumentVersionDetail WHERE DocumentId=D.DocumentId ORDER BY Id DESC)=1)
              AS ChildCount,

              CAST(0 AS BIT) AS IsDocument,'folder' AS DocType,'-' AS DocSize,
              CASE WHEN DS.Id IS NOT NULL THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS Shared,
              CASE WHEN FD.UpdatedBy = @userId THEN 'Me' ELSE (LC.FirstName + ' ' + LC.LastName) END AS ModifiedBy,
              CASE WHEN FD.UpdatedDate IS NOT NULL THEN FORMAT(FD.UpdatedDate, 'dd, MMM yyyy hh:mm tt')END AS ModifiedOn

              FROM @PublicFolderId F
              INNER JOIN FolderDetail FD ON F.FolderId=FD.Id
              LEFT JOIN LoginCredential LC ON FD.UpdatedBy = LC.Id
              LEFT JOIN UserDocShareDetail DS ON FD.Id = DS.FolderId
			  WHERE
			   (UPPER(FD.Name) LIKE UPPER('%'+COALESCE(@searchword,FD.Name)+'%') OR 
	UPPER(LC.FirstName) LIKE UPPER('%'+COALESCE(@searchword,LC.FirstName)+'%') OR 
	UPPER(LC.LastName) LIKE UPPER('%'+COALESCE(@searchword,LC.LastName)+'%') OR
	UPPER(CASE WHEN FD.UpdatedBy = @userId THEN 'Me' ELSE (LC.FirstName + ' ' + LC.LastName) END) LIKE UPPER('%'+COALESCE(@searchword,'Me')+'%') OR
	UPPER(FORMAT(FD.UpdatedDate, 'dd, MMM yyyy hh:mm tt')) LIKE UPPER('%'+COALESCE(@searchword,FORMAT(FD.UpdatedDate, 'dd, MMM yyyy hh:mm tt'))+'%'))
	
  --------------------End --------------------------------
  UNION ALL
  --------------------Get Full Data For File--------------
       SELECT 
              DD.DocumentId AS Id,
              DD.FolderId AS ParentFolderId,
              DVD.Id AS DocumentVersionId,
              DD.DocumentId,
              (SELECT CASE WHEN (SELECT Count(*) FROM Favorite FV WHERE FV.UserId=COALESCE(@userId,FV.UserId) AND FV.DocumentId=COALESCE(DD.DocumentId,FV.DocumentId)) >0 THEN Convert(Bit,1) ELSE Convert(Bit,0) END) AS IsFavorite,
              Name,
              DVD.DocumentName AS FileName,
              'v'+CAST(Format(DVD.DocumentVersion,'0.0') AS NVARCHAR(MAX)) AS Version,
              0 AS ChildCount,
              CAST(1 AS BIT) AS IsDocument
              ,DVD.DocumentType AS DocType,
              DVD.DocumentDisplaySize AS DocSize,
              CASE WHEN DS.Id IS NOT NULL THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS Shared,
              CASE WHEN DD.UpdatedBy = @userId THEN 'Me' ELSE (LC.FirstName + ' ' + LC.LastName) END AS ModifiedBy,
              CASE WHEN DD.UpdatedDate IS NOT NULL THEN FORMAT(DD.UpdatedDate, 'dd, MMM yyyy hh:mm tt')END AS ModifiedOn

              FROM DocumentVersionDetail DVD
              INNER JOIN DocumentDetail DD ON DD.DocumentId=DVD.DocumentId
              LEFT JOIN LoginCredential LC ON DD.UpdatedBy = LC.Id
              LEFT JOIN UserDocShareDetail DS ON DD.DocumentId = DS.DocumentId
              WHERE DVD.Id IN (SELECT DocumentVersionId  FROM @PublicFiles)
              AND DD.IsActive=1 AND DD.IsDelete=0 AND DVD.IsPublic=1
-- Copy From Jasmin search code 

	AND (UPPER(DD.Name) LIKE UPPER('%'+COALESCE(@searchword,DD.Name)+'%') OR 
	UPPER('v'+CAST(Format(DVD.DocumentVersion,'0.0') AS NVARCHAR(MAX))) LIKE UPPER('%'+COALESCE(@searchword,CAST(DVD.DocumentVersion AS NVARCHAR(MAX)))+'%') OR
	UPPER(DVD.DocumentDisplaySize) LIKE UPPER('%'+COALESCE(@searchword,DVD.DocumentDisplaySize)+'%') OR
	UPPER(LC.FirstName) LIKE UPPER('%'+COALESCE(@searchword,LC.FirstName)+'%') OR 
	UPPER(LC.LastName) LIKE UPPER('%'+COALESCE(@searchword,LC.LastName)+'%') OR
	UPPER(CASE WHEN DD.UpdatedBy = @userId THEN 'Me' ELSE (LC.FirstName + ' ' + LC.LastName) END) LIKE UPPER('%'+COALESCE(@searchword,'Me')+'%') OR
	UPPER(FORMAT(DD.UpdatedDate, 'dd, MMM yyyy hh:mm tt')) LIKE UPPER('%'+COALESCE(@searchword,FORMAT(DD.UpdatedDate, 'dd, MMM yyyy hh:mm tt'))+'%'))
  --------------------End --------------------------------
  
  DROP Table #Temp
  DROP Table #PublicFolders

END
---------------------------------------------------    
If(@FolderId>0)
BEGIN

DECLARE @PublicFolderId2 TABLE (FolderId BIGINT)
DECLARE @PublicFiles2 TABLE (DocumentVersionId BIGINT)
SELECT *
INTO   #PublicFolders2
FROM FolderDetail WHERE CabinetId=@CabinetId AND IsPublic=1
AND ParentFolderId=@FolderId
SELECT *
INTO   #Temp2
FROM FolderDetail WHERE CabinetId=@CabinetId AND IsPublic=1
AND ParentFolderId=@FolderId

Declare @Id2 BIGINT
Declare @Parent2 BIGINT

While (Select Count(*) From #Temp2) > 0
BEGIN
    SELECT Top 1 @Id2 = Id From #Temp2
       SELECT Top 1 @Parent2 = ParentFolderId FROM #Temp2

   if((SELECT count(*) FROM #PublicFolders2 Where Id=@Parent2)=0)
   BEGIN
    INSERT  INTO @PublicFolderId2
    VALUES  (@Id2)
   END

   DELETE #Temp2 WHERE Id = @Id2

END

-------------------------------------------------------------
INSERT INTO @PublicFiles2 ( DocumentVersionId )
(SELECT 
       DVD.Id
FROM  DocumentVersionDetail DVD
WHERE DVD.IsPublic=1  AND DVD.IsCurrent=1
        AND @CabinetId=(SELECT TOP 1 CabinetId FROM dbo.GetCabinetFolderForDocument(DVD.DocumentId))
       AND 1= (SELECT TOP 1 FolderIsPublic -- Sweta Just Change
	    FROM dbo.GetCabinetFolderForDocument(DVD.DocumentId))
       AND DVD.IsActive=1 AND DVD.IsDelete=0
       AND @FolderId= (SELECT TOP 1 FolderId FROM dbo.GetCabinetFolderForDocument(DVD.DocumentId))
)
--------------------------------------------------------------
--------------Get Data Inside Any Folder------------------------
              SELECT 
              F.FolderId AS Id,
              Fd.ParentFolderId,
              0 AS DocumentVersionId,
              0 As DocumentId,
              (SELECT CASE WHEN (SELECT Count(*) FROM Favorite FV WHERE FV.UserId=COALESCE(@userId,FV.UserId) AND FV.FolderId=COALESCE(F.FolderId,FV.FolderId)) >0 THEN Convert(Bit,1) ELSE Convert(Bit,0) END) AS IsFavorite,
              FD.Name,
              NULL AS FileName,
              '-' AS Version,
                (SELECT count(*) FROM FolderDetail
                                            WHERE ParentFolderId = FD.Id AND IsActive = 1 AND IsDelete = 0 AND IsPublic=1)
              + (SELECT count(*) FROM DocumentDetail D 
                                            WHERE D.FolderId = FD.Id AND D.IsActive = 1 AND D.IsDelete = 0 AND 
                                            (SELECT TOP 1 IsPublic FROM DocumentVersionDetail WHERE DocumentId=D.DocumentId ORDER BY Id DESC)=1)
              AS ChildCount,

              CAST(0 AS BIT) AS IsDocument,'folder' AS DocType,'-' AS DocSize,
              CASE WHEN DS.Id IS NOT NULL THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS Shared,
              CASE WHEN FD.UpdatedBy = @userId THEN 'Me' ELSE (LC.FirstName + ' ' + LC.LastName) END AS ModifiedBy,
              CASE WHEN FD.UpdatedDate IS NOT NULL THEN FORMAT(FD.UpdatedDate, 'dd, MMM yyyy hh:mm tt')END AS ModifiedOn

              FROM @PublicFolderId2 F
              INNER JOIN FolderDetail FD ON F.FolderId=FD.Id
              LEFT JOIN LoginCredential LC ON FD.UpdatedBy = LC.Id
              LEFT JOIN UserDocShareDetail DS ON FD.Id = DS.FolderId
UNION All
              SELECT 
              DD.DocumentId AS Id,
              DD.FolderId AS ParentFolderId,
              DVD.Id AS DocumentVersionId,
              DD.DocumentId,
              (SELECT CASE WHEN (SELECT Count(*) FROM Favorite FV WHERE FV.UserId=COALESCE(@userId,FV.UserId) AND FV.DocumentId=COALESCE(DD.DocumentId,FV.DocumentId)) >0 THEN Convert(Bit,1) ELSE Convert(Bit,0) END) AS IsFavorite,
              Name,
              DVD.DocumentName AS FileName,
              'v'+CAST(Format(DVD.DocumentVersion,'0.0') AS NVARCHAR(MAX)) AS Version,
              0 AS ChildCount,
              CAST(1 AS BIT) AS IsDocument
              ,DVD.DocumentType AS DocType,
              DVD.DocumentDisplaySize AS DocSize,
              CASE WHEN DS.Id IS NOT NULL THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS Shared,
              CASE WHEN DD.UpdatedBy = @userId THEN 'Me' ELSE (LC.FirstName + ' ' + LC.LastName) END AS ModifiedBy,
              CASE WHEN DD.UpdatedDate IS NOT NULL THEN FORMAT(DD.UpdatedDate, 'dd, MMM yyyy hh:mm tt')END AS ModifiedOn

              FROM DocumentVersionDetail DVD
              INNER JOIN DocumentDetail DD ON DD.DocumentId=DVD.DocumentId
              LEFT JOIN LoginCredential LC ON DD.UpdatedBy = LC.Id
              LEFT JOIN UserDocShareDetail DS ON DD.DocumentId = DS.DocumentId
              WHERE DVD.Id IN (SELECT DocumentVersionId  FROM @PublicFiles2)
              AND DD.IsActive=1 AND DD.IsDelete=0 AND DVD.IsPublic=1
-----------------------------------------------------------------


  DROP Table #Temp2
  DROP Table #PublicFolders2
END


END




GO
/****** Object:  StoredProcedure [dbo].[uspGetPublicFolderFileInfo]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dhrumil Patel
-- Create date: 02-05-2018
-- Description:	Get folder and file information
-- MOdified By : Sweta Patel
-- Modified Date : 23-05-2018
-- Description : Total Folders,Total files & Size Up to all child code is updated
-- =============================================
--[uspGetPublicFolderFileInfo] 7,52
--[uspGetPublicFolderFileInfo] 3,0,10164
--[uspGetPublicFolderFileInfo] 3,0,0,'','10323,10324'
CREATE PROCEDURE [dbo].[uspGetPublicFolderFileInfo]	
	@UserId NVARCHAR(20) = 0,
	@FolderId BIGINT = 0,
	@DocumentId BIGINT = 0,			  --DocumentVersionDetail table id
	@FolderIds NVARCHAR(MAX) = '',
	@FileIds NVARCHAR(MAX) = '',       --DocumentDetail table ids
	@CabinetId BIGINT = 0
AS
BEGIN
	DECLARE @TodayDate BIGINT = YEAR(GETDATE())*10000+MONTH(GETDATE())*100+DAY(GETDATE())
	DECLARE @TotalFolders BIGINT = 0
	DECLARE @TotalSizeUnit NVARCHAR(MAX)
	IF(@FolderId != 0)
	BEGIN
		DECLARE @FileCount BIGINT = 0		
		DECLARE @FileSize  DECIMAL(18,2) = 0.00

		
		-----------------------Call function-------------------------------------
		--TotalFolders	TotalFiles	TotalItems	DisplaySize	TotalSize
		---------------------------------------------------------------------------
		
		SELECT @TotalFolders=TotalFolders,@FileCount=TotalFiles,@FileSize=TotalSize,@TotalSizeUnit=DisplaySize FROM dbo.GetPublicFolderItemsCount(@FolderId,@UserId,'',@CabinetId)
		------------------------------------------------------------------------------

		SELECT UDID
			,Id
			,[Name]
			,'Folder' AS InfoType
			,CASE WHEN CreatedBy = @UserId THEN 'me' Else (SELECT FirstName + ' ' + LastName FROM LoginCredential WHERE CONVERT(NVARCHAR(MAX),Id) = FD.CreatedBy) END AS [Owner]
			,CASE WHEN UpdatedBy = @UserId THEN 'me' Else (SELECT FirstName + ' ' + LastName FROM LoginCredential WHERE CONVERT(NVARCHAR(MAX),Id) = FD.UpdatedBy) END AS [ModifyBy]
			,(SELECT FORMAT(CreatedDate, 'dd, MMM yyyy HH:mm tt')) AS CreatedDate
			,(SELECT FORMAT(UpdatedDate, 'dd, MMM yyyy HH:mm tt')) AS ModifyDate
			,CONVERT(NVARCHAR(5), @TotalFolders) + ' folders' AS TotalFolders
			,CONVERT(NVARCHAR(5), @FileCount) + ' files' AS TotalFiles
			,@TotalSizeUnit AS FileSize
			--,CASE WHEN @FileSize>1024 THEN CONVERT(NVARCHAR(20),@FileSize/1024) + ' MB' ELSE CONVERT(NVARCHAR(20),@FileSize) + ' KB' END AS FileSize
			,[Description]
			,Keywords
			,PhysicalLocation
			 ,(SELECT [dbo].[GetFullPath](FD.Id,'F',@CabinetId,default))  AS OriginalLocation
			,Notes
			--,(SELECT count(*) FROM FolderDetail WHERE ParentFolderId = FD.Id AND IsActive = 1 AND IsDelete = 0 AND CreatedBy = @UserId) + (SELECT count(*) FROM DocumentDetail WHERE FolderId = FD.Id AND IsActive = 1 AND IsDelete = 0 AND CreatedBy = @UserId) AS TotalCount
			,(@TotalFolders + @FileCount) AS TotalCount
		FROM FolderDetail FD
		WHERE Id = @FolderId
		 AND FD.IsPublic=1
	END
	ELSE IF(@DocumentId != 0)
	BEGIN		
		SELECT DD.UDID
			,DD.DocumentId AS Id
			,@DocumentId AS DocVersionId
			,[Name]
			,'File' AS InfoType
			,CASE WHEN DVD.CreatedBy = @UserId THEN 'me' Else (SELECT FirstName + ' ' + LastName FROM LoginCredential WHERE CONVERT(NVARCHAR(MAX),Id) = DVD.CreatedBy) END AS [Owner]
			,CASE WHEN DVD.UpdatedBy = @UserId THEN 'me' Else (SELECT FirstName + ' ' + LastName FROM LoginCredential WHERE CONVERT(NVARCHAR(MAX),Id) = DVD.UpdatedBy) END AS [ModifyBy]
			,(SELECT FORMAT(DVD.CreatedDate, 'dd, MMM yyyy HH:mm tt')) AS CreatedDate
			,(SELECT FORMAT(DVD.UpdatedDate, 'dd, MMM yyyy HH:mm tt')) AS ModifyDate
			,DVD.DocumentDisplaySize AS FileSize
			,[Description]
			,Keywords
			,PhysicalLocation
			,(SELECT [dbo].[GetFullPath](DD.DocumentId,'D',@CabinetId,default))  AS OriginalLocation
			,Notes
			,DocumentType AS [Type]
			,'v' + CAST(Format(DocumentVersion,'0.0') AS NVARCHAR(MAX)) AS [Version]
			,CASE WHEN Confidentiality = 1 THEN 'Low' ELSE CASE WHEN Confidentiality = 2 THEN 'Medium' ELSE 'High' END END AS Confidentiality
			,CASE WHEN [Language] = 1 THEN 'English' ELSE 'Arabic' END AS [Language]
			,(SELECT [Name] FROM CategoryMaster WHERE Id = DD.CategoryId) AS Category
			,(SELECT [Name] FROM SubCategoryMaster WHERE Id = DD.SubCategoryId) AS SubCategory
		FROM DocumentDetail DD
		LEFT JOIN DocumentVersionDetail DVD ON DVD.DocumentId = DD.DocumentId
		WHERE DVD.Id = @DocumentId AND DVD.IsPublic=1
	END
	ELSE
	BEGIN


	-----------------------Call function-------------------------------------
		--TotalFolders	TotalFiles	TotalItems	DisplaySize	TotalSize
		---------------------------------------------------------------------------
		SELECT @TotalFolders=TotalFolders,@FileCount=TotalFiles,@FileSize=TotalSize ,@TotalSizeUnit=DisplaySize FROM dbo.GetPublicFolderItemsCount(@FolderIds,@UserId,@FileIds,@CabinetId)
		------------------------------------------------------------------------------


		SELECT 'Both' AS InfoType
			,CONVERT(NVARCHAR(5), @TotalFolders) + ' folders' AS TotalFolders
			,CONVERT(NVARCHAR(5), @FileCount) + ' files' AS TotalFiles
		     ,@TotalSizeUnit AS FileSize
			--,CASE WHEN @FileSize>1024 THEN CONVERT(NVARCHAR(20),@FileSize/1024) + ' MB' ELSE CONVERT(NVARCHAR(20),@FileSize) + ' KB' END AS FileSize
		
	END
END




GO
/****** Object:  StoredProcedure [dbo].[uspGetRecycleBinFilesByCabinet]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--uspGetRecycleBinFilesByCabinet 18, 10046
CREATE PROCEDURE [dbo].[uspGetRecycleBinFilesByCabinet]
	@cabinetId AS BIGINT = 0,
	@userId AS NVARCHAR(20) = NULL,
	--@folderId AS BIGINT = 0,
	@searchword AS NVARCHAR(MAX) = null
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
	D.ID AS DeletedDataID
	,CAST(1 AS BIT) AS IsDocument
	,DD.DocumentId AS Id	
	,DD.FolderId AS ParentFolderId	
	,DV.Id AS DocumentVersionId
	,DV.DocumentId AS DocumentId
	,DD.Name
	,DV.DocumentName AS FileName
	,'v'+CAST(Format(DV.DocumentVersion,'0.0') AS NVARCHAR(MAX)) AS Version
	,DV.DocumentType AS DocType
	,DV.DocumentDisplaySize AS DocSize
	,CASE WHEN D.DeletedDate IS NOT NULL THEN FORMAT(D.DeletedDate, 'dd, MMM yyyy hh:mm tt') END AS ModifiedOn
	,(SELECT [dbo].[GetFullPath](DD.DocumentId,'D',@cabinetId,default)) AS DataPath
	FROM DeletedData D
	INNER JOIN DocumentDetail DD ON D.DocumentId = DD.DocumentId
	INNER JOIN DocumentVersionDetail DV ON DD.DocumentId = DV.DocumentId AND DV.IsCurrent=1
	WHERE D.IsRecycleBinDeleted = 0 AND D.CabinetId = @cabinetId AND D.DeletedBy =@userId  
	AND (
		UPPER(DD.Name) LIKE UPPER('%'+COALESCE(@searchword,DD.Name)+'%')
	OR	UPPER(DV.DocumentName) LIKE UPPER('%'+COALESCE(@searchword,DV.DocumentName)+'%')
	OR	UPPER('v'+CAST(Format(DV.DocumentVersion,'0.0') AS NVARCHAR(MAX))) LIKE UPPER('%'+COALESCE(@searchword,'v'+CAST(Format(DV.DocumentVersion,'0.0') AS NVARCHAR(MAX)))+'%')
	OR	UPPER(DV.DocumentType) LIKE UPPER('%'+COALESCE(@searchword,DV.DocumentType)+'%')
	OR	UPPER(DV.DocumentDisplaySize) LIKE UPPER('%'+COALESCE(@searchword,DV.DocumentDisplaySize)+'%')
	OR	UPPER(FORMAT(D.DeletedDate, 'dd, MMM yyyy hh:mm tt')) LIKE UPPER('%'+COALESCE(@searchword,FORMAT(D.DeletedDate, 'dd, MMM yyyy hh:mm tt'))+'%')
	)

	UNION ALL

	SELECT
	D.ID AS DeletedDataID
	,CAST(0 AS BIT) AS IsDocument
	,FD.ID AS Id	
	,FD.ParentFolderId
	,0 AS DocumentVersionId
	,0 AS DocumentId
	,FD.Name
	,NULL AS FileName
	,'-' AS Version	
	,'folder' AS DocType
	,'-' AS DocSize	
	,CASE WHEN D.DeletedDate IS NOT NULL THEN FORMAT(D.DeletedDate, 'dd, MMM yyyy hh:mm tt') END AS ModifiedOn
	,(SELECT [dbo].[GetFullPath](FD.Id,'F',@cabinetId,default)) AS DataPath
	FROM DeletedData D
	INNER JOIN FolderDetail FD ON D.FolderId = FD.Id	
	WHERE D.IsRecycleBinDeleted = 0 AND D.CabinetId = @cabinetId AND D.DeletedBy =@userId 
	AND (
		UPPER(FD.Name) LIKE UPPER('%'+COALESCE(@searchword,FD.Name)+'%')
	OR	UPPER(FORMAT(D.DeletedDate, 'dd, MMM yyyy hh:mm tt')) LIKE UPPER('%'+COALESCE(@searchword,FORMAT(D.DeletedDate, 'dd, MMM yyyy hh:mm tt'))+'%')
	--OR	UPPER('folder') LIKE UPPER('%'+COALESCE(@searchword,'folder')+'%')	
	)


	
	
END

GO
/****** Object:  StoredProcedure [dbo].[uspGetRecycleBinFilesByUserAndCabinet]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--uspGetRecycleBinFilesByUserAndCabinet 1,3
CREATE PROCEDURE [dbo].[uspGetRecycleBinFilesByUserAndCabinet] 
	@cabinetId AS BIGINT = null,
	@userId AS NVARCHAR(20) = null
	--@folderId AS BIGINT = null,
	--@searchword AS NVARCHAR(MAX) = null
AS
BEGIN	
	SET NOCOUNT ON;
	
    SELECT DISTINCT FD.Id AS Id,FD.ParentFolderId,0 AS DocumentVersionId,
	0 AS DocumentId,
	(SELECT CASE WHEN (SELECT Count(*) FROM Favorite FV WHERE FV.UserId=COALESCE(@userId,FV.UserId) AND FV.FolderId=COALESCE(FD.Id,FV.FolderId)) >0 THEN Convert(Bit,1) ELSE Convert(Bit,0) END) AS IsFavorite,
	FD.Name,NULL AS FileName,'-' AS Version,
	--(SELECT count(*) FROM FolderDetail WHERE ParentFolderId = FD.Id AND IsActive = 1 AND IsDelete = 0) + (SELECT count(*) FROM DocumentDetail WHERE FolderId = FD.Id AND IsActive = 1 AND IsDelete = 0) AS ChildCount,
	(SELECT count(*) FROM FolderDetail WHERE ParentFolderId = FD.Id AND IsDelete = 1) + (SELECT count(*) FROM DocumentDetail WHERE FolderId = FD.Id AND IsDelete = 1) AS ChildCount,
	CAST(0 AS BIT) AS IsDocument,'folder' AS DocType,'-' AS DocumentDisplaySize--, CAST(0 AS BIT) AS Shared,
	,CASE WHEN DS.Id IS NOT NULL THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS Shared,
	CASE WHEN FD.UpdatedBy = @userId THEN 'Me' ELSE (LC.FirstName + ' ' + LC.LastName) END AS ModifiedBy,
	CASE
		WHEN FD.UpdatedDate IS NOT NULL THEN FORMAT(FD.UpdatedDate, 'dd, MMM yyyy hh:mm tt')
	END AS ModifiedOn

	FROM FolderDetail FD LEFT JOIN DocumentDetail DD ON DD.FolderId = FD.Id
	LEFT JOIN LoginCredential LC ON FD.UpdatedBy = LC.Id
	LEFT JOIN UserDocShareDetail DS ON FD.Id = DS.FolderId
	WHERE FD.CabinetId = ISNULL(@cabinetId,FD.CabinetId) 
	AND FD.Id IN (SELECT ID FROM FolderDetail WHERE CreatedBy = @userId AND IsDelete = 1)
	--AND FD.CreatedBy = ISNULL(@userId,FD.CreatedBy) 
	--AND FD.ParentFolderId = ISNULL(@folderId,FD.ParentFolderId)
	--AND (UPPER(FD.Name) LIKE UPPER('%'+COALESCE(@searchword,FD.Name)+'%') OR 
	--UPPER(LC.FirstName) LIKE UPPER('%'+COALESCE(@searchword,LC.FirstName)+'%') OR 
	--UPPER(LC.LastName) LIKE UPPER('%'+COALESCE(@searchword,LC.LastName)+'%') OR
	--UPPER(CASE WHEN FD.UpdatedBy = @userId THEN 'Me' ELSE (LC.FirstName + ' ' + LC.LastName) END) LIKE UPPER('%'+COALESCE(@searchword,'Me')+'%') OR
	--UPPER(FORMAT(FD.UpdatedDate, 'dd, MMM yyyy hh:mm tt')) LIKE UPPER('%'+COALESCE(@searchword,FORMAT(FD.UpdatedDate, 'dd, MMM yyyy hh:mm tt'))+'%'))
	
	UNION ALL
	
	SELECT DISTINCT DD.DocumentId AS Id,DD.FolderId AS ParentFolderId,DV.Id AS DocumentVersionId,
	DV.DocumentId AS DocumentId,
	(SELECT CASE WHEN (SELECT Count(*) FROM Favorite FV WHERE FV.UserId=COALESCE(@userId,FV.UserId) AND FV.DocumentId=COALESCE(DD.DocumentId,FV.DocumentId)) >0 THEN Convert(Bit,1) ELSE Convert(Bit,0) END) AS IsFavorite,
	Name,DV.DocumentName AS FileName,'v'+CAST(Format(DV.DocumentVersion,'0.0') AS NVARCHAR(MAX)) AS Version,0 AS ChildCount,CAST(1 AS BIT) AS IsDocument
	,DV.DocumentType AS DocType,DV.DocumentDisplaySize AS DocumentDisplaySize,
	CASE WHEN DS.Id IS NOT NULL THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS Shared,
	CASE WHEN DD.UpdatedBy = @userId THEN 'Me' ELSE (LC.FirstName + ' ' + LC.LastName) END AS ModifiedBy,
	CASE
		WHEN DD.UpdatedDate IS NOT NULL THEN FORMAT(DD.UpdatedDate, 'dd, MMM yyyy hh:mm tt')
	END AS ModifiedOn
	FROM DocumentDetail DD 
	LEFT JOIN WF_TransactionDocuments WFT ON DD.DocumentId = WFT.DocumentId 
	INNER JOIN (SELECT RANK() OVER (PARTITION BY DocumentId ORDER BY Id DESC) r, *
	FROM DocumentVersionDetail) DV ON DD.DocumentId = DV.DocumentId
	LEFT JOIN LoginCredential LC ON DD.UpdatedBy = LC.Id
	LEFT JOIN UserDocShareDetail DS ON DD.DocumentId = DS.DocumentId
	WHERE DV.r = 1 AND  DD.IsDelete = 1 --AND DV.IsDelete = 1
	AND DD.FolderId IN (SELECT ID FROM FolderDetail WHERE CreatedBy = @userId AND CabinetId = @cabinetId)
	--AND DD.CreatedBy = ISNULL(@userId,DD.CreatedBy)	
	--AND DD.FolderId = ISNULL(@folderId,DD.FolderId)
	--AND (UPPER(DD.Name) LIKE UPPER('%'+COALESCE(@searchword,DD.Name)+'%') OR 
	--UPPER('v'+CAST(Format(DV.DocumentVersion,'0.0') AS NVARCHAR(MAX))) LIKE UPPER('%'+COALESCE(@searchword,CAST(DV.DocumentVersion AS NVARCHAR(MAX)))+'%') OR
	--UPPER(DV.DocumentDisplaySize) LIKE UPPER('%'+COALESCE(@searchword,DV.DocumentDisplaySize)+'%') OR
	--UPPER(LC.FirstName) LIKE UPPER('%'+COALESCE(@searchword,LC.FirstName)+'%') OR 
	--UPPER(LC.LastName) LIKE UPPER('%'+COALESCE(@searchword,LC.LastName)+'%') OR
	--UPPER(CASE WHEN DD.UpdatedBy = @userId THEN 'Me' ELSE (LC.FirstName + ' ' + LC.LastName) END) LIKE UPPER('%'+COALESCE(@searchword,'Me')+'%') OR
	--UPPER(FORMAT(DD.UpdatedDate, 'dd, MMM yyyy hh:mm tt')) LIKE UPPER('%'+COALESCE(@searchword,FORMAT(DD.UpdatedDate, 'dd, MMM yyyy hh:mm tt'))+'%'))
END

GO
/****** Object:  StoredProcedure [dbo].[uspGetSharedFolderFileInfo]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dhrumil Patel
-- Create date: 02-05-2018
-- Description:	Get folder and file information
-- MOdified By : Sweta Patel
-- Modified Date : 23-05-2018
-- Description : Total Folders,Total files & Size Up to all child code is updated
-- =============================================
--uspGetSharedFolderFileInfo 7,52
--uspGetSharedFolderFileInfo 3,0,10164
--uspGetSharedFolderFileInfo 3,0,0,'','10323,10324'
CREATE PROCEDURE [dbo].[uspGetSharedFolderFileInfo]	
	@UserId NVARCHAR(20) = 0,
	@FolderId BIGINT = 0,
	@DocumentId BIGINT = 0,			  --DocumentVersionDetail table id
	@FolderIds NVARCHAR(MAX) = '',
	@FileIds NVARCHAR(MAX) = '',       --DocumentDetail table ids
	@CabinetId BIGINT = 0
AS
BEGIN
	DECLARE @TodayDate BIGINT = YEAR(GETDATE())*10000+MONTH(GETDATE())*100+DAY(GETDATE())
	DECLARE @TotalFolders BIGINT = 0
	DECLARE @TotalSizeUnit NVARCHAR(MAX)
	IF(@FolderId != 0)
	BEGIN
		DECLARE @FileCount BIGINT = 0		
		DECLARE @FileSize  DECIMAL(18,2) = 0.00		
		-----------------------Call function-------------------------------------
		--TotalFolders	TotalFiles	TotalItems	DisplaySize	TotalSize
		-------------------------------------------------------------------------	
		SELECT @TotalFolders=TotalFolders,@FileCount=TotalFiles,@FileSize=TotalSize,@TotalSizeUnit=DisplaySize FROM dbo.GetSharedFolderItemsCount(@FolderId,@UserId,'',@CabinetId)
		-------------------------------------------------------------------------

		SELECT UDID
			,Id
			,[Name]
			,'Folder' AS InfoType
			,CASE WHEN CreatedBy = @UserId THEN 'me' Else (SELECT FirstName + ' ' + LastName FROM LoginCredential WHERE CONVERT(NVARCHAR(MAX),Id) = FD.CreatedBy) END AS [Owner]
			,CASE WHEN UpdatedBy = @UserId THEN 'me' Else (SELECT FirstName + ' ' + LastName FROM LoginCredential WHERE CONVERT(NVARCHAR(MAX),Id) = FD.UpdatedBy) END AS [ModifyBy]
			,(SELECT FORMAT(CreatedDate, 'dd, MMM yyyy HH:mm tt')) AS CreatedDate
			,(SELECT FORMAT(UpdatedDate, 'dd, MMM yyyy HH:mm tt')) AS ModifyDate
			,CONVERT(NVARCHAR(5), @TotalFolders) + ' folders' AS TotalFolders
			,CONVERT(NVARCHAR(5), @FileCount) + ' files' AS TotalFiles
			,@TotalSizeUnit AS FileSize
			,[Description]
			,Keywords
			,PhysicalLocation
			 ,(SELECT [dbo].[GetFullPath](FD.Id,'F',@CabinetId,default))  AS OriginalLocation
			,Notes
			,(@TotalFolders + @FileCount) AS TotalCount
		FROM FolderDetail FD
		WHERE Id = @FolderId
	END
	ELSE IF(@DocumentId != 0)
	BEGIN		
		SELECT DD.UDID
			,DD.DocumentId AS Id
			,@DocumentId AS DocVersionId
			,[Name]
			,'File' AS InfoType
			,CASE WHEN DVD.CreatedBy = @UserId THEN 'me' Else (SELECT FirstName + ' ' + LastName FROM LoginCredential WHERE CONVERT(NVARCHAR(MAX),Id) = DVD.CreatedBy) END AS [Owner]
			,CASE WHEN DVD.UpdatedBy = @UserId THEN 'me' Else (SELECT FirstName + ' ' + LastName FROM LoginCredential WHERE CONVERT(NVARCHAR(MAX),Id) = DVD.UpdatedBy) END AS [ModifyBy]
			,(SELECT FORMAT(DVD.CreatedDate, 'dd, MMM yyyy HH:mm tt')) AS CreatedDate
			,(SELECT FORMAT(DVD.UpdatedDate, 'dd, MMM yyyy HH:mm tt')) AS ModifyDate
			,DVD.DocumentDisplaySize AS FileSize
			,[Description]
			,Keywords
			,PhysicalLocation
			 ,(SELECT [dbo].[GetFullPath](DD.DocumentId,'D',@CabinetId,default))  AS OriginalLocation
			,Notes
			,DocumentType AS [Type]
			,'v' + CAST(Format(DocumentVersion,'0.0') AS NVARCHAR(MAX)) AS [Version]
			,CASE WHEN Confidentiality = 1 THEN 'Low' ELSE CASE WHEN Confidentiality = 2 THEN 'Medium' ELSE 'High' END END AS Confidentiality
			,CASE WHEN [Language] = 1 THEN 'English' ELSE 'Arabic' END AS [Language]
			,(SELECT [Name] FROM CategoryMaster WHERE Id = DD.CategoryId) AS Category
			,(SELECT [Name] FROM SubCategoryMaster WHERE Id = DD.SubCategoryId) AS SubCategory
		FROM DocumentDetail DD
		LEFT JOIN DocumentVersionDetail DVD ON DVD.DocumentId = DD.DocumentId
		WHERE DVD.Id = @DocumentId
	END
	ELSE
	BEGIN
		-----------------------Call function---------------------------------------
		--TotalFolders	TotalFiles	TotalItems	DisplaySize	TotalSize
		---------------------------------------------------------------------------
		SELECT @TotalFolders=TotalFolders,@FileCount=TotalFiles,@FileSize=TotalSize ,@TotalSizeUnit=DisplaySize FROM dbo.GetSharedFolderItemsCount(@FolderIds,@UserId,@FileIds,@CabinetId)
		----------------------------------------------------------------------------
		
		SELECT 'Both' AS InfoType
			,CONVERT(NVARCHAR(5), @TotalFolders) + ' folders' AS TotalFolders
			,CONVERT(NVARCHAR(5), @FileCount) + ' files' AS TotalFiles
		    ,@TotalSizeUnit AS FileSize			
	END
END



GO
/****** Object:  StoredProcedure [dbo].[uspGetShareUserListForUser]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dhrumil Patel
-- Create date: 15-06-2018
-- Description:	Get User List Having Share Permission By userId AND Cabinet
-- =============================================
--uspGetShareUserListForUser 1,10048
CREATE PROCEDURE [dbo].[uspGetShareUserListForUser]	
	@CabinetId BIGINT = 0,
	@User BIGINT=0
AS
BEGIN
	
SELECT DocShareUserId AS UserId
,ISNULL(Lc.FirstName,'') + '  ' + ISNULL(Lc.LastName,'') AS [Name]
 FROM UserDocSharePermission  UDSP
LEFT JOIN LoginCredential LC ON LC.Id = UDSP.DocShareUserId
WHERE UserId = @User AND DocShareGroupId = 0 AND IsActive = 1 AND IsDelete = 0 AND CabinetId = @CabinetId

UNION

SELECT UserId 
,ISNULL(Lc.FirstName,'') + '  ' + ISNULL(Lc.LastName,'') AS [Name]
FROM GroupUserDetail GUD
LEFT JOIN GroupDetail GD ON GD.Id = GUD.GroupId
LEFT JOIN LoginCredential LC ON LC.Id = GUD.UserId
WHERE GroupId IN (SELECT DocShareGroupId FROM UserDocSharePermission WHERE UserId = @User AND DocShareGroupId != 0)
AND LC.IsActive = 1 AND LC.IsDelete = 0 AND GD.IsActive = 1 AND GD.IsDelete = 0 AND CabinetId = @CabinetId

END





GO
/****** Object:  StoredProcedure [dbo].[uspGetShareWithMeFilesByUserAndCabinet]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dhruvin Patel
-- Update:		Dhrumil Patel
-- Create date: 29052018
-- Update date: 08062018
-- Description:	Get Share with me files and folders
-- uspGetShareWithMeFilesByUserAndCabinet 1,'3',10106
-- uspGetShareWithMeFilesByUserAndCabinet 1,'10048',1,'','count'
-- =============================================
CREATE PROCEDURE [dbo].[uspGetShareWithMeFilesByUserAndCabinet]
	-- Add the parameters for the stored procedure here
	@cabinetId AS BIGINT = NULL,
	@userId AS NVARCHAR(20) = NULL,
	@folderId AS BIGINT = 0,
	@searchword AS NVARCHAR(MAX) = null,
	@mode AS NVARCHAR(MAX) = ''
AS
BEGIN
	SET NOCOUNT ON;

	--------------Sweta Code To Get Unique IDs----------------------------
	DECLARE @ShareData TABLE(
    ShareId BIGINT,	DocumentId BIGINT,	FolderId BIGINT
)  

DECLARE @FinalShareData TABLE (Id BIGINT)

INSERT INTO @ShareData
SELECT  U.Id As ShareId,U.DocumentId ,U.FolderId FROM UserDocShareDetail U WHERE U.CabinetId=@cabinetId AND  U.DocShareUserId=@userId  AND DocShareGroupId=0
UNION ALL
SELECT U.Id As ShareId,U.DocumentId ,U.FolderId  FROM UserDocShareDetail U WHERE U.CabinetId= @cabinetId AND  U.DocShareUserId=0  AND  DocShareGroupId IN (
SELECT GD.Id FROM GroupDetail GD
LEFT JOIN   GroupUserDetail   GUD ON GD.Id=GUD.GroupId
WHERE GUD.UserId=@userId AND GD.IsActive=1 AND GD.IsDelete=0 AND GD.CabinetId=@cabinetId)
--------------------------------------------------------------------------------
--SELECT *  FROM @ShareData ORDER BY ShareId

SELECT * INTO   #PublicFolders FROM @ShareData   ORDER BY DocumentId
SELECT * INTO   #Temp FROM @ShareData  ORDER BY DocumentId


Declare @Id BIGINT
Declare @Document BIGINT
Declare @Folder BIGINT

While (Select Count(*) From #Temp) > 0
BEGIN
    SELECT Top 1 @Id = ShareId From #Temp
    SELECT Top 1 @Document = DocumentId FROM #Temp
	SELECT Top 1 @Folder = FolderId FROM #Temp

	IF(@Document>0)
	BEGIN
	
	PRINT 'I m File'
	   IF((SELECT count(*) FROM #PublicFolders Where DocumentId=@Document)=1)
	   BEGIN
			INSERT  INTO @FinalShareData VALUES  (@Id)
			PRINT 'Insert single  File'
	   END

	    IF((SELECT count(*) FROM #PublicFolders Where DocumentId=@Document)>1)
	   BEGIN
			INSERT  INTO @FinalShareData VALUES  (@Id)
			PRINT 'Insert Unique  File'
			DELETE  FROM #PublicFolders WHERE DocumentId=@Document AND ShareId!=@Id
			DELETE  FROM #Temp WHERE DocumentId=@Document AND ShareId!=@Id
	   END

	END --// End Of Document ID >0 Block

	IF(@Folder>0)
	BEGIN
		PRINT 'I m Folder'
		   IF((SELECT count(*) FROM #PublicFolders Where FolderId=@Folder)=1)
	   BEGIN
			INSERT  INTO @FinalShareData VALUES  (@Id)
			PRINT 'Insert single  Folder'
	   END

	    IF((SELECT count(*) FROM #PublicFolders Where FolderId=@Folder)>1)
	   BEGIN
			INSERT  INTO @FinalShareData VALUES  (@Id)
					PRINT 'Insert Unique  Folder'
			DELETE  FROM #PublicFolders WHERE  FolderId=@Folder AND ShareId!=@Id
			DELETE  FROM #Temp WHERE FolderId=@Folder AND ShareId!=@Id
	   END

	END --// End Of Folder ID >0 Block

   DELETE #Temp WHERE ShareId = @Id -- delete 1 by 1 row

END

--SELECT * FROM @FinalShareData ORDER BY Id
DROP TABLE #PublicFolders
DROP TABLE #Temp

	--------------Sweta Code to get UniqueiDs-----------------------------

	IF (@mode = '')
	BEGIN
		SELECT DISTINCT FD.Id AS Id,UDSD.Id AS SharedId,FD.ParentFolderId,0 AS DocumentVersionId,
		0 AS DocumentId,
		CASE WHEN FV.Id IS NOT NULL THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS IsFavorite,
		FD.Name,NULL AS FileName,'-' AS Version,
		(SELECT count(*) FROM FolderDetail WHERE ParentFolderId = FD.Id AND IsActive = 1 AND IsDelete = 0) + (SELECT count(*) FROM DocumentDetail WHERE FolderId = FD.Id AND IsActive = 1 AND IsDelete = 0) AS ChildCount,
		CAST(0 AS BIT) AS IsDocument,'folder' AS DocType,'-' AS DocSize,
		CASE WHEN 
				(SELECT COUNT(*) FROM UserDocShareDetail WHERE Id != UDSD.Id AND FolderId = FD.Id AND UserId = @userId) > 0
			THEN 
				CAST(1 AS BIT) 	
			ELSE 
				CAST(0 AS BIT) 
			END AS Shared,
		CASE WHEN FD.UpdatedBy = @userId THEN 'Me' ELSE (LC.FirstName + ' ' + LC.LastName) END AS ModifiedBy,
		CASE
			WHEN FD.UpdatedDate IS NOT NULL THEN FORMAT(FD.UpdatedDate, 'dd, MMM yyyy hh:mm tt')
		END AS ModifiedOn
		FROM FolderDetail FD 
		INNER JOIN UserDocShareDetail UDSD ON UDSD.FolderId = FD.Id
		LEFT JOIN GroupDetail GD on UDSD.DocShareGroupId = GD.Id AND GD.IsActive = 1 AND GD.IsDelete = 0
		LEFT JOIN GroupUserDetail GUD on GD.Id = GUD.GroupId
		LEFT JOIN LoginCredential LC ON FD.UpdatedBy = CAST(LC.Id AS NVARCHAR(50))
		LEFT JOIN Favorite FV ON FD.Id = FV.FolderId AND FV.UserId = @userId
		WHERE FD.IsActive = 1 AND FD.IsDelete = 0 AND UDSD.CabinetId = @cabinetId 
		AND (UDSD.DocShareUserId = @userId OR GUD.UserId = @userId)
		AND UDSD.ParentId = @folderId
		--------------- Sweta Condition---------------
		AND  UDSD.Id  IN (SELECT Id FROM @FinalShareData)
		----------------End Sweta Condition-----------


		AND (UPPER(FD.Name) LIKE UPPER('%'+COALESCE(@searchword,FD.Name)+'%') OR 
		UPPER(LC.FirstName) LIKE UPPER('%'+COALESCE(@searchword,LC.FirstName)+'%') OR 
		UPPER(LC.LastName) LIKE UPPER('%'+COALESCE(@searchword,LC.LastName)+'%') OR
		UPPER(CASE WHEN FD.UpdatedBy = @userId THEN 'Me' ELSE (LC.FirstName + ' ' + LC.LastName) END) LIKE UPPER('%'+COALESCE(@searchword,'Me')+'%') OR
		UPPER(FORMAT(FD.UpdatedDate, 'dd, MMM yyyy hh:mm tt')) LIKE UPPER('%'+COALESCE(@searchword,FORMAT(FD.UpdatedDate, 'dd, MMM yyyy hh:mm tt'))+'%'))
		
		UNION ALL
		
		SELECT DISTINCT DD.DocumentId AS Id,UDSD.Id AS SharedId,DD.FolderId AS ParentFolderId,DV.Id AS DocumentVersionId,
		DV.DocumentId AS DocumentId,
		CASE WHEN FV.Id IS NOT NULL THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS IsFavorite,
		DD.Name,DV.DocumentName AS FileName,'v'+CAST(Format(DV.DocumentVersion,'0.0') AS NVARCHAR(MAX)) AS Version,0 AS ChildCount,CAST(1 AS BIT) AS IsDocument
		,DV.DocumentType AS DocType,DV.DocumentDisplaySize AS DocSize,
		CASE WHEN 
				(SELECT COUNT(*) FROM UserDocShareDetail WHERE Id != UDSD.Id AND DocumentId = DD.DocumentId AND UserId = @userId) > 0
			THEN 
				CAST(1 AS BIT) 	
			ELSE 
				CAST(0 AS BIT) 
			END AS Shared,
		CASE WHEN DD.UpdatedBy = @userId THEN 'Me' ELSE (LC.FirstName + ' ' + LC.LastName) END AS ModifiedBy,
		CASE
			WHEN DD.UpdatedDate IS NOT NULL THEN FORMAT(DD.UpdatedDate, 'dd, MMM yyyy hh:mm tt')
		END AS ModifiedOn
		FROM DocumentDetail DD 
		INNER JOIN UserDocShareDetail UDSD ON UDSD.DocumentId = DD.DocumentId
		LEFT JOIN GroupDetail GD on UDSD.DocShareGroupId = GD.Id AND GD.IsActive = 1 AND GD.IsDelete = 0
		LEFT JOIN GroupUserDetail GUD on GD.Id = GUD.GroupId
		LEFT JOIN WF_TransactionDocuments WFT ON DD.DocumentId = WFT.DocumentId 
		INNER JOIN (SELECT RANK() OVER (PARTITION BY DocumentId ORDER BY Id DESC) r, *
		FROM DocumentVersionDetail) DV ON DD.DocumentId = DV.DocumentId
		LEFT JOIN LoginCredential LC ON DD.UpdatedBy = LC.Id
		LEFT JOIN Favorite FV ON DD.DocumentId = FV.DocumentId AND FV.UserId = @userId
		WHERE DV.r = 1 AND DD.IsActive = 1 AND DD.IsDelete = 0 AND DV.IsActive = 1 AND DV.IsDelete = 0
		AND (UDSD.DocShareUserId = @userId OR GUD.UserId = @userId)
		AND UDSD.CabinetId = @cabinetId
		AND UDSD.ParentId = @folderId
		--------------- Sweta Condition---------------
		AND  UDSD.Id  IN (SELECT Id FROM @FinalShareData)
		----------------End Sweta Condition-----------
		AND (UPPER(DD.Name) LIKE UPPER('%'+COALESCE(@searchword,DD.Name)+'%') OR 
		UPPER('v'+CAST(Format(DV.DocumentVersion,'0.0') AS NVARCHAR(MAX))) LIKE UPPER('%'+COALESCE(@searchword,CAST(DV.DocumentVersion AS NVARCHAR(MAX)))+'%') OR
		UPPER(DV.DocumentDisplaySize) LIKE UPPER('%'+COALESCE(@searchword,DV.DocumentDisplaySize)+'%') OR
		UPPER(LC.FirstName) LIKE UPPER('%'+COALESCE(@searchword,LC.FirstName)+'%') OR 
		UPPER(LC.LastName) LIKE UPPER('%'+COALESCE(@searchword,LC.LastName)+'%') OR
		UPPER(CASE WHEN DD.UpdatedBy = @userId THEN 'Me' ELSE (LC.FirstName + ' ' + LC.LastName) END) LIKE UPPER('%'+COALESCE(@searchword,'Me')+'%') OR
		UPPER(FORMAT(DD.UpdatedDate, 'dd, MMM yyyy hh:mm tt')) LIKE UPPER('%'+COALESCE(@searchword,FORMAT(DD.UpdatedDate, 'dd, MMM yyyy hh:mm tt'))+'%'))
	END
	ELSE
	BEGIN
		SELECT DISTINCT UDSD.Id AS SharedId
		FROM FolderDetail FD 
		INNER JOIN UserDocShareDetail UDSD ON UDSD.FolderId = FD.Id
		LEFT JOIN GroupDetail GD on UDSD.DocShareGroupId = GD.Id AND GD.IsActive = 1 AND GD.IsDelete = 0
		LEFT JOIN GroupUserDetail GUD on GD.Id = GUD.GroupId
		LEFT JOIN LoginCredential LC ON FD.UpdatedBy = CAST(LC.Id AS NVARCHAR(50))
		LEFT JOIN Favorite FV ON FD.Id = FV.FolderId AND FV.UserId = @userId
		WHERE FD.IsActive = 1 AND FD.IsDelete = 0 AND UDSD.CabinetId = @cabinetId 
		AND (UDSD.DocShareUserId = @userId OR GUD.UserId = @userId)

		UNION ALL
		
		SELECT DISTINCT UDSD.Id AS SharedId
		FROM DocumentDetail DD 
		INNER JOIN UserDocShareDetail UDSD ON UDSD.DocumentId = DD.DocumentId
		LEFT JOIN GroupDetail GD on UDSD.DocShareGroupId = GD.Id AND GD.IsActive = 1 AND GD.IsDelete = 0
		LEFT JOIN GroupUserDetail GUD on GD.Id = GUD.GroupId
		LEFT JOIN WF_TransactionDocuments WFT ON DD.DocumentId = WFT.DocumentId 
		INNER JOIN (SELECT RANK() OVER (PARTITION BY DocumentId ORDER BY Id DESC) r, *
		FROM DocumentVersionDetail) DV ON DD.DocumentId = DV.DocumentId
		LEFT JOIN LoginCredential LC ON DD.UpdatedBy = LC.Id
		LEFT JOIN Favorite FV ON DD.DocumentId = FV.DocumentId AND FV.UserId = @userId
		WHERE DV.r = 1 AND DD.IsActive = 1 AND DD.IsDelete = 0 AND DV.IsActive = 1 AND DV.IsDelete = 0
		AND (UDSD.DocShareUserId = @userId OR GUD.UserId = @userId)
		AND UDSD.CabinetId = @cabinetId
	END


	---- SET NOCOUNT ON added to prevent extra result sets from
	---- interfering with SELECT statements.
	--SET NOCOUNT ON;
	
	--SELECT 
	--FD.Id AS Id
	--,FD.Name AS Name
	--,'folder' AS DocumentType
	--,0 AS DocumentVersion
	--,'-' AS DocumentDisplaySize
	--,ISNULL((LC.FirstName + ' ' +LC.LastName),'-') AS ModifiedBy
	--,CASE WHEN FD.UpdatedDate IS NOT NULL THEN FORMAT(FD.UpdatedDate, 'dd, MMM yyyy hh:mm tt') ELSE '-' END AS ModifiedOn
	--FROM UserDocShareDetail UDSD 
	--LEFT JOIN FolderDetail FD on  UDSD.FolderId = FD.Id
	--LEFT JOIN GroupDetail GD on UDSD.DocShareGroupId = GD.Id   AND GD.IsActive = 1 AND GD.IsDelete = 0
	--LEFT JOIN GroupUserDetail GUD on GD.Id = GUD.GroupId
	--LEFT JOIN LoginCredential LC ON FD.UpdatedBy = CAST(LC.Id AS NVARCHAR(50))	
	--WHERE (UDSD.DocShareUserId = @userId OR GUD.UserId = @userId) 			
	--		AND ((FD.CabinetId = @cabinetId AND FD.IsActive = 1 AND FD.IsDelete = 0))  			
	--		--AND (GD.IsActive = 1 AND GD.IsDelete = 0))
	--		--OR (GD.CabinetId = @cabinetId AND GD.IsActive = 1 AND GD.IsDelete = 0))	
	
	--UNION ALL

	--SELECT 
	--DD.DocumentId AS Id
	--,DD.Name AS Name
	--,DVD.DocumentType AS DocumentType
	--,dvd.DocumentVersion AS DocumentVersion
	--,dvd.DocumentDisplaySize AS DocumentDisplaySiz	
	--,ISNULL((LC.FirstName + ' ' +LC.LastName),'-') AS ModifiedBy
	--,CASE WHEN DD.UpdatedDate IS NOT NULL THEN FORMAT(DD.UpdatedDate, 'dd, MMM yyyy hh:mm tt') ELSE '-' END AS ModifiedOn	 
	--FROM UserDocShareDetail UDSD 	
	--INNER JOIN DocumentDetail DD on UDSD.DocumentId = DD.DocumentId
	--LEFT JOIN GroupDetail GD on UDSD.DocShareGroupId = GD.Id AND GD.IsActive = 1 AND GD.IsDelete = 0
	--LEFT JOIN GroupUserDetail GUD on GD.Id = GUD.GroupId	
	--INNER JOIN (SELECT RANK() OVER (PARTITION BY DocumentId ORDER BY Id DESC) r
	--	,*
	--	FROM DocumentVersionDetail)DVD ON DD.DocumentId = DVD.DocumentId
	--LEFT JOIN LoginCredential LC ON DVD.UpdatedBy =CAST(LC.Id AS NVARCHAR(50))	
	--WHERE DVD.r = 1 
	--	AND  UDSD.CabinetId = @cabinetId 		 
	--	AND (UDSD.DocShareUserId = @userId OR GUD.UserId = @userId)
	--	AND ((DVD.IsActive = 1 AND DVD.IsDelete = 0))
END

GO
/****** Object:  StoredProcedure [dbo].[uspGetUserDetail]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dhrumil Patel
-- Create date: 12-03-2018
-- Description:	Get user detail
-- =============================================
--uspGetUserDetail 1,1
CREATE PROCEDURE [dbo].[uspGetUserDetail]	
	@UserId BIGINT =0,
	@ApplicationId BIGINT = 0
AS
BEGIN
	SELECT LC.Id AS UserId
		,ULD.ApplicationId
		,ISNULL(LC.FirstName,'') AS FirstName
		,ISNULL(LC.MiddleName,'')  AS MiddleName
		,ISNULL(LC.LastName,'')  AS LastName
		,(SELECT FirstName + ' ' + LastName FROM LoginCredential WHERE Id = LC.ManagerId) AS ManagerName
		,ISNULL(STUFF(
			(SELECT ','+ SM.Name 
				FROM UserRole ESD
				LEFT JOIN RoleMaster SM ON SM.Id = ESD.RoleId
				WHERE ESD.UserId = LC.Id
			    FOR XML PATH(''),TYPE
			).value('.','NVARCHAR(MAX)') ,1,1,''), 'All') AS RoleName
		,ISNULL(STUFF(
			(SELECT ','+ CM.Name 
				FROM UserCabinetDetail UCD
				LEFT JOIN CabinetMaster CM ON CM.Id = UCD.CabinetId
				WHERE UCD.UserId = LC.Id
			    FOR XML PATH(''),TYPE
			).value('.','NVARCHAR(MAX)') ,1,1,''), 'All') AS CabinetName
		,ISNULL(STUFF(
			(SELECT ','+ AM.Name 
				FROM ApplicationMaster AM
				LEFT JOIN UserLoginDetail ULD ON ULD.ApplicationId = AM.ApplicationId
				WHERE ULD.UserId = LC.Id
			    FOR XML PATH(''),TYPE
			).value('.','NVARCHAR(MAX)') ,1,1,''), 'All') AS ApplicationName
		,DM.[Name] AS DepartmentName
		,SM.[Name] AS SectionName
		,PM.[Name] AS PositionName
		,LC.EmailId
		,LEFT(CONVERT(VARCHAR, LC.DOB, 101), 10) AS DOB
		,ULD.UserName
		,LC.MobileNumber
		,LC.PhoneNumber
		,LC.ProfileImage,
		ULD.Password,
		LC.Gender
    FROM LoginCredential LC
	LEFT JOIN DepartmentMaster DM ON LC.DepartmentId = DM.Id
	LEFT JOIN SectionMaster SM ON LC.SectionId = SM.Id
	LEFT JOIN PositionMaster PM ON LC.PositionId = PM.Id	
	INNER JOIN UserLoginDetail ULD ON LC.Id = ULD.UserId
	WHERE LC.Id = @UserId
		AND ISNULL(LC.IsActive, 0) = 1 AND ISNULL(LC.IsDelete, 0) = 0 
		AND ULD.ApplicationId = @ApplicationId
END



GO
/****** Object:  StoredProcedure [dbo].[uspGetUserList]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dhrumil Patel
-- Create date: 20-02-2018
-- Description:	Get user list
-- =============================================
--uspGetUserList
CREATE PROCEDURE [dbo].[uspGetUserList]	
	@CabinetId BIGINT = 0
AS
BEGIN
	SELECT LC.Id AS UserId
		,ISNULL(LC.FirstName,'') AS FirstName
		,ISNULL(LC.MiddleName,'')  AS MiddleName
		,ISNULL(LC.LastName,'')  AS LastName
		,ISNULL(Lc.FirstName,'') + '  ' + ISNULL(Lc.LastName,'') AS [Name]
		,ISNULL(ULD.UserName,'') AS [UserName]
		,(SELECT FirstName + ' ' + LastName FROM LoginCredential WHERE Id = LC.ManagerId) AS ManagerName
		,ISNULL(STUFF(
			(SELECT ','+ SM.Name 
				FROM UserRole ESD
				LEFT JOIN RoleMaster SM ON SM.Id = ESD.RoleId
				WHERE ESD.UserId = LC.Id
			    FOR XML PATH(''),TYPE
			).value('.','NVARCHAR(MAX)') ,1,1,''), 'All') AS RoleName	
		,ISNULL(STUFF(
			(SELECT ','+ CM.Name 
				FROM UserCabinetDetail UCD
				LEFT JOIN CabinetMaster CM ON CM.Id = UCD.CabinetId
				WHERE UCD.UserId = LC.Id
			    FOR XML PATH(''),TYPE
			).value('.','NVARCHAR(MAX)') ,1,1,''), 'All') AS CabinetName		
		,LC.DepartmentId
		,DM.[Name] AS DepartmentName
		,LC.EmailId
		,LC.IsActive
		,LC.IsDelete
    FROM LoginCredential LC
	LEFT JOIN DepartmentMaster DM ON LC.DepartmentId = DM.Id
	LEFT JOIN UserCabinetDetail UCD ON LC.Id = UCD.UserId
	INNER JOIN UserLoginDetail ULD ON LC.Id = ULD.UserId
	WHERE ISNULL(LC.IsDelete, 0) = 0 AND ULD.ApplicationId != 1
		AND UCD.CabinetId = @CabinetId
	GROUP BY LC.Id,LC.FirstName,LC.MiddleName,LC.LastName,LC.DepartmentId,DM.[Name],LC.ManagerId,LC.IsActive
		,LC.IsDelete,ULD.UserName,LC.EmailId
	ORDER BY LC.Id DESC
END



GO
/****** Object:  StoredProcedure [dbo].[uspGetUserListByCabinet]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dhrumil Patel
-- Create date: 16-02-2018
-- Description:	Get user list by cabinet id
-- =============================================
--uspGetUserListByCabinet 1,2
CREATE PROCEDURE [dbo].[uspGetUserListByCabinet]
	@userId AS BIGINT = 0,
	@cabinetId AS BIGINT = 0
AS
BEGIN
	SELECT LC.Id AS UserId
		,ISNULL(Lc.FirstName,'') + ' ' + ISNULL(Lc.LastName,'') AS FullName
		,ISNULL(STUFF(
			(SELECT ','+ SM.Name 
				FROM UserRole ESD
				LEFT JOIN RoleMaster SM ON SM.Id = ESD.RoleId
				WHERE ESD.UserId = LC.Id
			    FOR XML PATH(''),TYPE
			).value('.','NVARCHAR(MAX)') ,1,1,''), 'All') AS RoleName	
		,LC.EmailId
		,LC.ProfileImage
    FROM LoginCredential LC
	LEFT JOIN UserCabinetDetail UCD ON LC.Id = UCD.UserId
	WHERE ISNULL(LC.IsActive,0) = 1 AND ISNULL(LC.IsDelete, 0) = 0 
		AND UCD.CabinetId = @cabinetId AND LC.Id != @userId 
	ORDER BY FullName
END



GO
/****** Object:  StoredProcedure [dbo].[uspGetUserRoleListByDepartment]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sweta Patel
-- Create date: 2018-03-05
-- Description:	Get user list with role , application & BY department wise
-- Note : Bydefault Super Admin as user added 
-- =============================================
-- EXEC uspGetUserRoleListByDepartment
-- EXEC uspGetUserRoleListByDepartment 1
-- EXEC uspGetUserRoleListByDepartment 2
CREATE PROCEDURE [dbo].[uspGetUserRoleListByDepartment]
	@DepartmentId AS BIGINT = NULL
AS
BEGIN
	
 	SELECT LC.Id
		,ISNULL(LC.FirstName,'') AS FirstName
		,ISNULL(LC.MiddleName,'')  AS MiddleName	
		,ISNULL(LC.LastName,'')  AS LastName 
		,ISNULL(Lc.FirstName,'') + '  ' + ISNULL(Lc.LastName,'') AS FullName
		,'SuperAdmin' AS UserNameApp
		,'All' AS UserRolePermission
		,DepartmentId
		,DM.[Name] AS Department
	FROM LoginCredential LC 
	LEFT JOIN DepartmentMaster DM ON LC.DepartmentId=DM.Id
	WHERE LC.Id=(SELECT TOP 1  UserId FROM UserLoginDetail where ApplicationId=1)
		AND LC.IsActive = 1 AND ISNULL(LC.IsDelete, 0) = 0
	
	UNION 

	SELECT LC.Id
		,ISNULL(LC.FirstName,'') AS FirstName
		,ISNULL(LC.MiddleName,'')  AS MiddleName
		,ISNULL(LC.LastName,'')  AS LastName
		,ISNULL(Lc.FirstName,'') + '  ' + ISNULL(Lc.LastName,'') AS FullName
		,ISNULL(STUFF(
			(SELECT ','+ SM.Name 
				FROM UserLoginDetail ESD
				LEFT JOIN ApplicationMaster SM ON SM.ApplicationId = ESD.ApplicationId
				WHERE ESD.UserId = LC.Id
			    FOR XML PATH(''),TYPE
			).value('.','NVARCHAR(MAX)') ,1,1,''),'NA') AS  UserNameApp
		,ISNULL(STUFF(
			(SELECT ', '+ SM.Name 
				FROM UserRole ESD
				LEFT JOIN RoleMaster SM ON SM.Id = ESD.RoleId
				WHERE ESD.UserId = LC.Id
			    FOR XML PATH(''),TYPE
			).value('.','NVARCHAR(MAX)') ,1,1,''), 'All') AS  UserRolePermission			
		,LC.DepartmentId
		,DM.[Name] AS Department
    FROM LoginCredential LC
	LEFT JOIN DepartmentMaster DM ON LC.DepartmentId=DM.Id	
	WHERE LC.IsActive = 1 AND ISNULL(LC.IsDelete, 0) = 0
		AND COALESCE(@DepartmentId,LC.DepartmentId) = LC.DepartmentId
	ORDER BY Lc.Id 
END



GO
/****** Object:  StoredProcedure [dbo].[uspSaveDocSharedUserPermission]    Script Date: 29-06-2018 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dhrumil Patel	
-- Create date: 07/03/2018
-- Description:	Insert into UserDocSharePermission
-- =============================================
CREATE PROCEDURE [dbo].[uspSaveDocSharedUserPermission] 
	@UserId bigint = 0,	
	@CabinetId bigint = 0,
	@SharedUserList varchar(max)='',
	@SharedGroupList varchar(max)=''
AS
BEGIN
	IF(@SharedUserList != '' OR @SharedGroupList != '')
	BEGIN
		DELETE FROM UserDocSharePermission
		WHERE UserId = @UserId AND CabinetId = @CabinetId

		IF(@SharedUserList != '')
		BEGIN
			INSERT INTO UserDocSharePermission(UserId,CabinetId,DocShareGroupId,DocShareUserId)
			SELECT @UserId,@CabinetId,0,LTRIM(value) FROM dbo.Split(@SharedUserList,',')
		END

		IF(@SharedGroupList != '')
		BEGIN
			INSERT INTO UserDocSharePermission(UserId,CabinetId,DocShareUserId,DocShareGroupId)
			SELECT @UserId,@CabinetId,0,LTRIM(value) FROM dbo.Split(@SharedGroupList,',')		
		END
	END
END



GO
