DROP Table FolderData

/*create the tbl*/
CREATE TABLE FolderData(
    Id BIGINT NOT NULL,
    Name VARCHAR(100) NOT NULL,
    ParentFolderId BIGINT NULL    
)

INSERT [dbo].[FolderData] ([Id], [Name], [ParentFolderId]) VALUES (1, N'User_1', NULL)
INSERT [dbo].[FolderData] ([Id], [Name], [ParentFolderId]) VALUES (2, N'User_2', 1)
INSERT [dbo].[FolderData] ([Id], [Name], [ParentFolderId]) VALUES (3, N'User_3', 2)
INSERT [dbo].[FolderData] ([Id], [Name], [ParentFolderId]) VALUES (4, N'User_4', 3)
INSERT [dbo].[FolderData] ([Id], [Name], [ParentFolderId]) VALUES (5, N'User_5', 4)
INSERT [dbo].[FolderData] ([Id], [Name], [ParentFolderId]) VALUES (6, N'User_6', 5)
INSERT [dbo].[FolderData] ([Id], [Name], [ParentFolderId]) VALUES (7, N'User_7', 6)
INSERT [dbo].[FolderData] ([Id], [Name], [ParentFolderId]) VALUES (8, N'User_8', 7)
INSERT [dbo].[FolderData] ([Id], [Name], [ParentFolderId]) VALUES (9, N'User_9', 8)
INSERT [dbo].[FolderData] ([Id], [Name], [ParentFolderId]) VALUES (10, N'User_10', 9)


/*row posible childs in a column*/
WITH Hierarchy(MainFolderID, FolderName, ParentId, Childs)
AS
(
    SELECT Id, Name, ParentFolderId, CAST('' AS VARCHAR(MAX))
        FROM FolderData AS LastGeneration
        WHERE Id NOT IN (SELECT COALESCE(ParentFolderId, 0) FROM FolderData)     
    UNION ALL
    SELECT PrevGeneration.Id, PrevGeneration.Name, PrevGeneration.ParentFolderId,
    CAST(CASE WHEN Child.Childs = ''
        THEN(CAST(Child.MainFolderID AS VARCHAR(MAX)))
        ELSE(Child.Childs + '.' + CAST(Child.MainFolderID AS VARCHAR(MAX)))
    END AS VARCHAR(MAX))
        FROM FolderData AS PrevGeneration
        INNER JOIN Hierarchy AS Child ON PrevGeneration.Id = Child.ParentId    
)
SELECT  *
    FROM Hierarchy WHERE MainFolderID=3
OPTION(MAXRECURSION 32000) 

