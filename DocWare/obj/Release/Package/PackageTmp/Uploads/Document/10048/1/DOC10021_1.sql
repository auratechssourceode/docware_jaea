USE csistrykergtc_v01

DECLARE       @return_value int

EXEC   @return_value = [dbo].[GetSPS_Excel_Report]
              @SurveyId = 80195,
              @FromDate = N'01-01-2018',
              @ToDate = N'14-05-2018'

SELECT 'Return Value' = @return_value

GO
