/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */
var lang = DocWare.Cookies.getCookie("LangForMultiLanguage");

if (lang == "ar-bh") {
    CKEDITOR.editorConfig = function (config) {
        // Define changes to default configuration here. For example:
        //config.uiColor = '#1ab394';
        config.language = 'ar';
        config.contentsLangDirection = 'rtl';
    };
}

