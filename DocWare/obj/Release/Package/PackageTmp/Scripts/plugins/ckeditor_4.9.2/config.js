/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */
var lang = DocWare.Cookies.getCookie("LangForMultiLanguage");

if (lang == "ar-bh") {
    CKEDITOR.editorConfig = function (config) {
        // Define changes to default configuration here. For example:
        // config.language = 'fr';
        // config.uiColor = '#AADC6E';
        config.language = 'ar';
        config.contentsLangDirection = 'rtl';
    };
}
