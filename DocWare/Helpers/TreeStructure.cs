﻿using DocWare.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Utility.Helpers;
using Utility.Models;

namespace DocWare.Helpers
{
    public class TreeStructure
    {
        static DBEntities db = new DBEntities();
        static DAL objDAL = new DAL();
        static string treeStr = string.Empty;
        static List<long> parentFolderlst = new List<long>();
        public static string FolderTree(long id, long OpenFolderId)
        {
            if (id == 0)
            {
                treeStr = string.Empty;
            }
            List<long> openFolderLst = new List<long>();
            if (OpenFolderId != 0)
            {
                var lst = db.FolderDetails.FirstOrDefault(x => x.Id == OpenFolderId);
                if (lst != null && lst.ParentFolderId != 0)
                {
                    openFolderLst.Add(lst.ParentFolderId);
                    openFolderLst = GetAllParentIds(lst.ParentFolderId);
                }
                openFolderLst.Add(OpenFolderId);
            }

            List<FolderFileVM> folderDetailVMs = new List<FolderFileVM>();
            try
            {
                folderDetailVMs = getFolderDetails();

                folderDetailVMs = folderDetailVMs.Where(x => x.ParentFolderId == id).ToList();

                treeStr += "<ul>";
                if (folderDetailVMs != null)
                {
                    if (folderDetailVMs.Count > 0)
                    {
                        foreach (var submenu in folderDetailVMs)
                        {
                            if (openFolderLst.Contains(submenu.Id))
                            {
                                treeStr += "<li class=\"jstree-open\" data-jstree='{\"type\":\"" + submenu.DocType + "\"}'>";
                            }
                            else
                            {
                                treeStr += "<li data-jstree='{\"type\":\"" + submenu.DocType + "\"}'>";
                            }
                            if (submenu.IsDocument)
                            {
                                treeStr += "<a href='@Url.Action('openFile','FolderAndFile')' target='_blank'>" + submenu.Name + "</a>";
                            }
                            else
                            {
                                if (submenu.Id == OpenFolderId)
                                {
                                    treeStr += "<a href='javascript:void(0)' class=\"jstree-clicked\" style='word-break:break-all' onclick='folderDetails(" + submenu.Id + ")'>" + submenu.Name + "</a>";
                                }
                                else
                                {
                                    treeStr += "<a href='javascript:void(0)' style='word-break:break-all' onclick='folderDetails(" + submenu.Id + ")'>" + submenu.Name + "</a>";
                                }
                            }
                            if (!submenu.IsDocument)
                            {
                                if (HasChild(submenu.Id) > 0)
                                {
                                    FolderTree(submenu.Id, OpenFolderId);
                                }
                            }

                            treeStr = treeStr + "</li>";
                        }
                    }
                }

                treeStr = treeStr + "</ul>";
            }
            catch (Exception)
            {
                return treeStr;
            }
            return treeStr;
        }

        public static long HasChild(long id)
        {
            long menuCount = 0;
            try
            {
                List<FolderFileVM> folderDetailVMs = new List<FolderFileVM>();

                folderDetailVMs = getFolderDetails();

                menuCount = folderDetailVMs.Where(x => x.ParentFolderId == id).Count();
            }
            catch (Exception e)
            {
                return menuCount;
            }
            return menuCount;
        }

        public static List<FolderFileVM> getFolderDetails()
        {
            List<FolderFileVM> folderDetailVMs = new List<FolderFileVM>();
            SortedList sl = new SortedList();
            DataTable dt = new DataTable();
            try
            {
                long UserId = CommonFunctions.GetUserId();
                long CabinetId = CommonFunctions.GetCabinetId();

                if (CabinetId > 0)
                {
                    sl.Add("@cabinetId", CabinetId);
                }

                long ApplicationId = Convert.ToInt64(HttpContext.Current.Session[SessionClass.ApplicationID]);

                if (ApplicationId != (int)EnumList.Application.SuperAdmin)
                    sl.Add("@userId", UserId);

                dt = objDAL.GetDataTable("uspGetFolderDetails", sl);

                if (dt != null)
                    folderDetailVMs = objDAL.ConvertToList<FolderFileVM>(dt);
            }
            catch (Exception e)
            {
                return folderDetailVMs;
            }
            return folderDetailVMs;
        }

        public static List<long> GetAllParentIds(long id)
        {
            if (treeStr == string.Empty)
            {
                parentFolderlst = new List<long>();
            }
            var lst = db.FolderDetails.FirstOrDefault(x => x.Id == id);
            if (lst != null && lst.ParentFolderId != 0)
            {
                parentFolderlst.Add(lst.Id);
                GetAllParentIds(lst.ParentFolderId);
            }
            else
            {
                parentFolderlst.Add(lst.Id);
            }
            return parentFolderlst;
        }
    }
}