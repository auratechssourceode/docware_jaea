﻿using DocWare.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Utility.Helpers;
using Utility.Models;

namespace DocWare.Helpers
{
    public class RoleManagement
    {
        #region --> Get Link List For Role | Add | Jasmin Vohra | 06032018
        public static List<MenuVM> GetLinksForSelectedRole(long userRoleId)
        {
            List<MenuVM> lstMenu = new List<MenuVM>();
            try
            {
                using (DBEntities db = new DBEntities())
                {
                    MenuVM objMenu;
                    long cabinetId = CommonFunctions.GetCabinetId();

                    var lstResult = (from link in db.Links
                                     join module in db.Modules on link.ModuleId equals module.ModuleId
                                     where (link.IsActive == true)
                                     orderby module.ViewIndex
                                     select new
                                     {
                                         link.LinkId,
                                         link.LinkName,
                                         link.LinkNameArabic,
                                         link.Action,
                                         link.Controller,
                                         link.ModuleId,
                                         link.Module,
                                         link.IsDefault,
                                         link.IsSingle,
                                         link.Type,
                                         link.TypeArabic,
                                         module.CssClass,
                                         link.ViewIndex,
                                         link.ParentId,
                                         link.IsPage,
                                         link.LinkGroupId
                                     }).ToList();

                    foreach (var groupItem in lstResult.GroupBy(j => j.LinkGroupId))
                    {
                        foreach (var item in groupItem.OrderBy(j => j.ViewIndex).OrderBy(j => j.ParentId))
                        {
                            objMenu = new MenuVM();
                            objMenu.LinkID = item.LinkId;
                            objMenu.LinkName = item.LinkName;
                            objMenu.LinkType = item.Type;
                            objMenu.ArLinkName = item.LinkNameArabic;
                            objMenu.ArLinkType = item.TypeArabic;
                            objMenu.Action = item.Action;
                            objMenu.Controller = item.Controller;
                            objMenu.ModuleID = item.ModuleId;
                            objMenu.ModuleName = item.Module.ModuleName;
                            objMenu.ArModuleName = item.Module.ModuleNameArabic;
                            objMenu.IsDefault = item.IsDefault;
                            objMenu.IsSingle = item.IsSingle;
                            objMenu.CssClass = item.CssClass;
                            objMenu.ViewIndex = item.ViewIndex;
                            objMenu.IsPage = item.IsPage;

                            var rights = db.RoleRights.Where(x => x.LinkId == objMenu.LinkID && x.RoleId == userRoleId && x.CabinetId == cabinetId).FirstOrDefault();

                            if (rights != null)
                            {
                                objMenu.IsAssigned = true;
                                objMenu.UserRoleID = rights.RoleId;
                            }
                            else
                            {
                                objMenu.IsAssigned = false;
                            }
                            lstMenu.Add(objMenu);
                        }
                    }

                    return lstMenu;
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region --> Get Links List For User | Add | Jasmin Vohra | 06032018
        public static List<MenuVM> GetLinksForSelectedUser(long userId)
        {
            List<MenuVM> lstMenu = new List<MenuVM>();
            try
            {
                using (DBEntities db = new DBEntities())
                {
                    MenuVM objMenu;
                    long cabinetId = CommonFunctions.GetCabinetId();

                    var lstResult = (from link in db.Links
                                     join module in db.Modules on link.ModuleId equals module.ModuleId
                                     where (link.IsActive == true)
                                     orderby module.ViewIndex
                                     select new
                                     {
                                         link.LinkId,
                                         link.LinkName,
                                         link.LinkNameArabic,
                                         link.Action,
                                         link.Controller,
                                         link.ModuleId,
                                         link.Module,
                                         link.IsDefault,
                                         link.IsSingle,
                                         link.Type,
                                         link.TypeArabic,
                                         module.CssClass,
                                         link.ViewIndex,
                                         link.ParentId,
                                         link.IsPage,
                                         link.LinkGroupId
                                     }).ToList();

                    foreach (var groupItem in lstResult.GroupBy(j => j.LinkGroupId))
                    {
                        foreach (var item in groupItem.OrderBy(j => j.ViewIndex).OrderBy(j => j.ParentId))
                        {
                            objMenu = new MenuVM();
                            objMenu.LinkID = item.LinkId;
                            objMenu.LinkName = item.LinkName;
                            objMenu.LinkType = item.Type;
                            objMenu.ArLinkName = item.LinkNameArabic;
                            objMenu.ArLinkType = item.TypeArabic;
                            objMenu.Action = item.Action;
                            objMenu.Controller = item.Controller;
                            objMenu.ModuleID = item.ModuleId;
                            objMenu.ModuleName = item.Module.ModuleName;
                            objMenu.ArModuleName = item.Module.ModuleNameArabic;
                            objMenu.IsDefault = item.IsDefault;
                            objMenu.IsSingle = item.IsSingle;
                            objMenu.CssClass = item.CssClass;
                            objMenu.ViewIndex = item.ViewIndex;
                            objMenu.IsPage = item.IsPage;

                            var rights = db.UserRights.Where(x => x.LinkId == objMenu.LinkID && x.UserId == userId && x.CabinetId == cabinetId).FirstOrDefault();

                            if (rights != null)
                            {
                                objMenu.IsAssigned = true;
                                objMenu.UserID = rights.UserId;
                            }
                            else
                            {
                                objMenu.IsAssigned = false;
                            }
                            lstMenu.Add(objMenu);
                        }
                    }

                    return lstMenu;
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region --> Get Role Name | Add | Jasmin Vohra | 06032018
        public static string GetRoleNameById(long userRoleId)
        {
            try
            {
                var roleName = (object)null;
                using (DBEntities db = new DBEntities())
                {
                    roleName = (from a in db.RoleMasters
                                where a.Id == userRoleId
                                select a.Name).FirstOrDefault();
                }
                return Convert.ToString(roleName);
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region--> Get Menu | Add | Jasmin Vohra | 06032018
        public static string GetMenu()
        {
            string response = string.Empty;
            List<MenuVM> MenuItems = new List<MenuVM>();
            MenuVM objMenu;
            List<MenuVM> RoleMenuItems = new List<MenuVM>();
            try
            {
                using (DBEntities db = new DBEntities())
                {
                    long userId = CommonFunctions.GetUserId();
                    long cabinetId = CommonFunctions.GetCabinetId();
                    long applicationId = CommonFunctions.GetApplicationId();

                    List<long> roles = (List<long>)HttpContext.Current.Session["RolePermissionList"];

                    var lstResult = (from link in db.Links
                                     join module in db.Modules on link.ModuleId equals module.ModuleId
                                     where (link.IsActive == true && link.IsVisible == true && link.IsPage == true)
                                     orderby module.ViewIndex
                                     select new
                                     {
                                         link.LinkId,
                                         link.LinkName,
                                         link.LinkNameArabic,
                                         link.Action,
                                         link.Controller,
                                         link.ModuleId,
                                         link.Module,
                                         link.IsDefault,
                                         link.IsSingle,
                                         link.ApplicationId,
                                         module.CssClass,
                                         link.ViewIndex,
                                         link.IsPage,
                                         link.ParentId
                                     }).ToList();

                    if (Convert.ToInt32(HttpContext.Current.Session["ApplicationID"].ToString()) != (int)EnumList.Application.SuperAdmin)
                    {
                        var userList = db.UserRights.Where(x => x.CabinetId == cabinetId && x.UserId == userId).Select(x => x.LinkId).ToList();
                        var roleList = db.RoleRights.Where(x => x.CabinetId == cabinetId && roles.Contains(x.RoleId)).Select(x => x.LinkId).ToList();

                        List<long> rightsList = userList.Union(roleList).ToList();
                        lstResult = lstResult.Where(x => rightsList.Contains(x.LinkId) && x.ApplicationId == applicationId).ToList();
                    }

                    lstResult.ForEach(mn =>
                      {
                          objMenu = new MenuVM();

                          objMenu.LinkID = mn.LinkId;
                          objMenu.LinkName = mn.LinkName;
                          objMenu.ArLinkName = mn.LinkNameArabic;
                          objMenu.Action = mn.Action;
                          objMenu.Controller = mn.Controller;
                          objMenu.ModuleID = mn.ModuleId;
                          objMenu.ModuleName = mn.Module.ModuleName;
                          objMenu.ArModuleName = mn.Module.ModuleNameArabic;
                          objMenu.IsDefault = mn.IsDefault;
                          objMenu.IsSingle = mn.IsSingle;
                          objMenu.CssClass = mn.CssClass;
                          objMenu.ViewIndex = mn.ViewIndex;
                          objMenu.ParentId = mn.ParentId;
                          RoleMenuItems.Add(objMenu);
                      });

                    response = JsonConvert.SerializeObject(RoleMenuItems);
                }
            }
            catch
            {
                return response;
            }
            return response;
        }
        #endregion

        #region --> Get Permission For All Active Link From Link table | Add | Jasmin | 21032018
        public static string GetAllLinkPermission()
        {
            string AllPermission = string.Empty;
            long CabinetID = CommonFunctions.GetCabinetId();
            long UserID = CommonFunctions.GetUserId();
            long ApplicationId = CommonFunctions.GetApplicationId();
            List<UserPermissionVM> userPermissionVMs = new List<UserPermissionVM>();
            try
            {
                using (DBEntities db = new DBEntities())
                {
                    List<long> roles = (List<long>)HttpContext.Current.Session["RolePermissionList"];
                    List<long> rightsList = new List<long>();

                    if (ApplicationId == (int)EnumList.Application.SuperAdmin)
                    {
                        userPermissionVMs = db.Links.Where(j => j.IsActive).Select(x => new UserPermissionVM
                        {
                            ActionName = x.Action,
                            ControllerName = x.Controller,
                            Type = x.Type
                        }).ToList();
                    }
                    else
                    {
                        var userList = db.UserRights.Where(x => x.CabinetId == CabinetID && x.UserId == UserID).Select(x => x.LinkId).ToList();
                        var roleList = db.RoleRights.Where(x => x.CabinetId == CabinetID && roles.Contains(x.RoleId)).Select(x => x.LinkId).ToList();
                        rightsList = userList.Union(roleList).ToList();

                        userPermissionVMs = db.Links.Where(j => rightsList.Contains(j.LinkId) && j.IsActive && j.ApplicationId == ApplicationId).Select(x => new UserPermissionVM
                        {
                            ActionName = x.Action,
                            ControllerName = x.Controller,
                            Type = x.Type
                        }).ToList();
                    }

                    AllPermission = JsonConvert.SerializeObject(userPermissionVMs);
                }
            }
            catch
            {
                return AllPermission;
            }
            return AllPermission;
        }
        #endregion

        #region --> Get Parent Folder List And Counts | Add | Jasmin | 23042018
        public static List<FolderVM> GetParentFolderList()
        {
            List<FolderVM> folderList = new List<FolderVM>();
            try
            {
                using (DBEntities db = new DBEntities())
                {
                    long cabinetId = CommonFunctions.GetCabinetId();
                    long userId = CommonFunctions.GetUserId();

                    folderList = db.FolderDetails.Where(j => j.CreatedBy == userId.ToString() && j.CabinetId == cabinetId
                                                               && j.ParentFolderId == 0 && j.IsActive && !j.IsDelete).Select(x => new FolderVM
                                                               {
                                                                   Id = x.Id,
                                                                   Name = x.Name
                                                               }).ToList();
                    if (folderList != null)
                    {
                        foreach (var item in folderList)
                        {
                            if (HasChild(item.Id) > 0)
                            {
                                item.ChildCount = GetFolderChildCount(item.Id);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return folderList;
        }

        public static int GetFolderChildCount(long Id)
        {
            long userId = CommonFunctions.GetUserId();
            var childCount = 0;
            using (DBEntities db = new DBEntities())
            {
                var folderList = db.FolderDetails.Where(j => j.ParentFolderId == Id && j.IsActive && !j.IsDelete /*&& j.CreatedBy == userId.ToString()*/).ToList();

                var foldercount = folderList.Count;
                var filecount = db.DocumentDetails.Where(j => j.FolderId == Id && j.IsActive && !j.IsDelete /*&& j.CreatedBy == userId.ToString()*/).ToList().Count;

                childCount = foldercount + filecount;

                foreach (var item in folderList)
                {
                    if (HasChild(item.Id) > 0)
                    {
                        childCount += GetFolderChildCount(item.Id);
                    }
                }
            }
            return childCount;
        }

        public static int HasChild(long id)
        {
            var childCount = 0;
            try
            {
                using (DBEntities db = new DBEntities())
                {
                    childCount = db.FolderDetails.Where(x => x.ParentFolderId == id).Count();

                    if (childCount == 0)
                    {
                        childCount = db.DocumentDetails.Where(x => x.FolderId == id).Count();
                    }
                }
            }
            catch (Exception e)
            {
                return childCount;
            }
            return childCount;
        }

        public static MenuCountVM GetMenuCount()
        {
            MenuCountVM menuCountVM = new MenuCountVM();
            try
            {
                using (DBEntities db = new DBEntities())
                {
                    long userId = CommonFunctions.GetUserId();
                    long cabinetId = CommonFunctions.GetCabinetId();

                    var folderLst = db.FolderDetails.ToList();
                    var documentLst = db.DocumentDetails.ToList();
                    var favLst = db.Favorites.ToList();
                    var docDetails = (from fol in folderLst
                                      join doc in documentLst on fol.Id equals doc.FolderId
                                      join docv in db.DocumentVersionDetails on doc.DocumentId equals docv.DocumentId
                                      where (doc.IsActive && !doc.IsDelete && fol.CabinetId == cabinetId
                                      && docv.IsActive && !docv.IsDelete && docv.IsCurrent) // just added iscurrent for current verson Data
                                      select new
                                      {
                                          doc.DocumentId,
                                          docv.IsPublic,
                                          doc.CreatedBy
                                      }).Distinct().ToList();

                    var folderCount = folderLst.Where(j => j.CabinetId == cabinetId && /*j.CreatedBy != userId.ToString() &&*/ j.IsPublic && j.IsActive && !j.IsDelete).ToList().Count();
                    var docCount = docDetails.Count() > 0 ? docDetails.Where(x => x.IsPublic /*&& x.CreatedBy != userId.ToString()*/).Count() : 0;
                    menuCountVM.Public = folderCount + docCount;

                    var folderIds = folderLst.Where(j => j.CabinetId == cabinetId /*&& j.CreatedBy == userId.ToString()*/ && j.IsActive && !j.IsDelete).Select(x => x.Id).ToList();
                    List<long> docIds = docDetails.Select(x => x.DocumentId).ToList();
                    var fav = favLst.Where(x => x.UserId == userId).ToList();
                    var favFolder = fav.Where(x => folderIds.Contains(x.FolderId)).ToList();
                    var favDoc = fav.Where(x => docIds.Contains(x.DocumentId)).ToList();
                    menuCountVM.Favorite = favFolder.Count() + favDoc.Count();
                    List<FileListVM> fileListVMs = new List<FileListVM>();
                    DAL objDAL = new DAL();
                    System.Collections.SortedList sl = new System.Collections.SortedList();
                    System.Data.DataTable dt = new System.Data.DataTable();
                    sl.Add("@cabinetId", cabinetId);
                    sl.Add("@userId", userId);
                    sl.Add("@mode", "count");
                    dt = objDAL.GetDataTable("uspGetShareWithMeFilesByUserAndCabinet", sl);
                    fileListVMs = objDAL.ConvertToList<FileListVM>(dt);
                    menuCountVM.SharedWithMe = fileListVMs.Count();

                    menuCountVM.RecycleBin = GetDeleteFolderCount();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return menuCountVM;
        }
        #endregion

        public static int GetDeleteFolderCount()
        {
            long userId = CommonFunctions.GetUserId();
            long cabinetId = CommonFunctions.GetCabinetId();
            var childCount = 0;
            using (DBEntities db = new DBEntities())
            {
                childCount = db.DeletedDatas.Count(x => x.DeletedBy == userId.ToString() && x.CabinetId == cabinetId && !x.IsRecycleBinDeleted);
                //var fodlerLst = db.FolderDetails.ToList();
                //var userFolderIds = fodlerLst.Where(x => x.CreatedBy == userId.ToString() && x.CabinetId == cabinetId).Select(x => x.Id).ToList();
                //var folderlist = fodlerLst.Where(j => userFolderIds.Contains(j.ParentFolderId) && j.IsDelete).ToList();

                //var foldercount = folderlist.Count();
                //var filecount = db.DocumentDetails.Where(j => userFolderIds.Contains(j.FolderId) && j.IsDelete).ToList().Count();

                //childCount = foldercount + filecount;
            }
            return childCount;
        }
    }
}