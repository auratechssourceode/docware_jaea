﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DocWare.Helpers
{
    public static class HMTLHelperExtensions
    {
        public static string IsSelected(this HtmlHelper html, string controller = null, string action = null, string cssClass = null, long id = 0)
        {

            if (String.IsNullOrEmpty(cssClass))
                cssClass = "active";

            string currentAction = (string)html.ViewContext.RouteData.Values["action"];
            string currentController = (string)html.ViewContext.RouteData.Values["controller"];

            if (String.IsNullOrEmpty(controller))
                controller = currentController;

            if (String.IsNullOrEmpty(action))
                action = currentAction;

            if (id > 0 && html.ViewContext.RouteData.Values["id"] != null)
            {
                long currentId = Convert.ToInt64(html.ViewContext.RouteData.Values["id"].ToString());

                return controller == currentController && action == currentAction && id == currentId ? cssClass : String.Empty;
            }
            else
            {
                return controller == currentController && action == currentAction ? cssClass : String.Empty;
            }
        }

        public static string PageClass(this HtmlHelper html)
        {
            string currentAction = (string)html.ViewContext.RouteData.Values["action"];
            return currentAction;
        }

    }
}