﻿using DocWare.Models;
using DocWare.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Utility.Helpers;
using Utility.Models;

namespace DocWare.Helpers
{
    public class CheckAuthorization : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (HttpContext.Current.Session["UserID"] == null || HttpContext.Current.Session["CabinetID"] == null || HttpContext.Current.Session["LinkPermissionList"] == null || !HttpContext.Current.Request.IsAuthenticated)
            {
                if (filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    filterContext.HttpContext.Response.StatusCode = 302; //Found Redirection to another page. Here- login page. Check Layout ajaxError() script.
                    filterContext.HttpContext.Response.End();
                }
                else
                {
                    filterContext.Result = new RedirectResult(System.Web.Security.FormsAuthentication.LoginUrl + "?ReturnUrl=" +
                         filterContext.HttpContext.Server.UrlEncode(filterContext.HttpContext.Request.RawUrl));
                }
            }
            //do not delete the code below-for page level authorization
            else
            {
                using (DBEntities db = new DBEntities())
                {
                    long ApplicationID = Convert.ToInt64(HttpContext.Current.Session["ApplicationID"]);
                    long UserID = Convert.ToInt64(HttpContext.Current.Session["UserID"]);
                    long CabinetID = Convert.ToInt64(HttpContext.Current.Session["CabinetID"]);

                    if (ApplicationID != (int)EnumList.Application.SuperAdmin)
                    {
                        if (db.Links.Where(j => j.Controller.Trim() == filterContext.ActionDescriptor.ControllerDescriptor.ControllerName.Trim()
                                         && j.Action.Trim() == filterContext.ActionDescriptor.ActionName.Trim() && j.ApplicationId == ApplicationID && j.IsActive).ToList().Count > 0)
                        {
                            var count = 0;

                            List<long> roles = (List<long>)HttpContext.Current.Session["RolePermissionList"];

                            var userList = db.UserRights.Where(x => x.CabinetId == CabinetID && x.UserId == UserID).Select(x => x.LinkId).ToList();
                            var roleList = db.RoleRights.Where(x => x.CabinetId == CabinetID && roles.Contains(x.RoleId)).Select(x => x.LinkId).ToList();

                            List<long> rightsList = userList.Union(roleList).ToList();

                            count = db.Links.Where(j => rightsList.Contains(j.LinkId) && j.Action.Trim() == filterContext.ActionDescriptor.ActionName.Trim() &&
                                j.Controller.Trim() == filterContext.ActionDescriptor.ControllerDescriptor.ControllerName.Trim() && j.ApplicationId == ApplicationID && j.IsActive).Distinct().Count();

                            if (count < 1)
                            {
                                //HttpContext.Current.Session[SessionClass.NoPermission] = Messages.No_Permission;

                                if (filterContext.HttpContext.Request.IsAjaxRequest())
                                {
                                    filterContext.HttpContext.Response.StatusCode = 403; // This code is changed to avoide browser auth. Popup issue on ajax request
                                                                                         //filterContext.HttpContext.Response.StatusCode = 401; // UnAuthorizedAccess - Redirect to UnAuthorize Access page. Check Layout ajaxError() script.

                                    filterContext.HttpContext.Response.End();
                                    filterContext.Result = new JavaScriptResult() { Script = "window.location = '" + HttpContext.Current.Request.UrlReferrer.ToString() + "' " };
                                }
                                else
                                {
                                    if (HttpContext.Current.Request.UrlReferrer != null)
                                    {
                                        filterContext.Result = new RedirectResult(HttpContext.Current.Request.UrlReferrer.ToString());
                                    }
                                    else
                                    {
                                        filterContext.Result = new RedirectResult("~/Account/Error401");
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool authorize = true;
            if (!httpContext.Request.RawUrl.ToLower().Contains("logout"))
            {
                if (HttpContext.Current.Session["UserID"] == null)
                {
                    authorize = false;
                }
            }
            return authorize;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            //Returns HTTP 401 - see comment in HttpUnauthorizedResult.cs.
            filterContext.Result = new RedirectToRouteResult(
                                       new RouteValueDictionary
                                   {
                                       {"Area",""},
                                       { "action", "Login" },
                                       { "controller", "Account" },
                                   });
        }
    }
}