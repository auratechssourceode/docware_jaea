﻿using DocWare.Models;
using DocWare.Resources;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using Utility.Helpers;
using Utility.Models;
using static Utility.Helpers.EnumList;

namespace DocWare.Helpers
{
    public class CommonFunctions
    {
        #region --> Get UserId of Login User | Add | Jasmin | 07022018
        public static long GetUserId()
        {
            try
            {
                if (HttpContext.Current.Session["UserID"] != null)
                {
                    return Convert.ToInt64(HttpContext.Current.Session["UserID"].ToString());
                }
                return 0;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        #endregion

        #region --> Get FullName | Add | Jasmin |16022018
        public static string GetFullName(long UserID)
        {
            string FullName = string.Empty;
            try
            {
                if (UserID > 0)
                {
                    using (DBEntities db = new DBEntities())
                    {
                        var user = db.LoginCredentials.Where(j => j.Id == UserID).FirstOrDefault();

                        if (user != null)
                            FullName = user.FirstName + " " + user.LastName;
                    }
                }
                return FullName;
            }
            catch (Exception ex)
            {
                return FullName;
            }
        }
        #endregion

        #region --> Get CabinetId of Login User | Add | Jasmin | 07022018
        public static long GetCabinetId()
        {
            try
            {
                if (HttpContext.Current.Session["CabinetID"] != null)
                {
                    return Convert.ToInt64(HttpContext.Current.Session["CabinetID"].ToString());
                }
                return 0;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        #endregion

        #region --> Get ApplicationId of Login User | Add | Jasmin | 07022018
        public static long GetApplicationId()
        {
            try
            {
                if (HttpContext.Current.Session["ApplicationID"] != null)
                {
                    return Convert.ToInt64(HttpContext.Current.Session["ApplicationID"].ToString());
                }
                return 0;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        #endregion

        #region --> Get UDID for Cabinet | Add | Jasmin | 08022018
        public static string GetUDIDForCabinet()
        {
            string UDID = string.Empty;
            try
            {
                using (DBEntities db = new DBEntities())
                {
                    var cabinetLst = db.CabinetMasters.ToList();

                    if (cabinetLst.Count > 0)
                    {
                        var lastUDID = cabinetLst.OrderByDescending(j => j.Id).FirstOrDefault().UDID;

                        lastUDID = lastUDID.Substring(3);

                        long Id = Convert.ToInt32(lastUDID) + 1;

                        UDID = "CAB" + Id.ToString();
                    }
                    else
                    {
                        UDID = "CAB10001";
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return UDID;
        }
        #endregion

        #region --> Get UDID for Folder | Add | Jasmin | 08022018
        public static string GetUDIDForFolder()
        {
            string UDID = string.Empty;
            try
            {
                using (DBEntities db = new DBEntities())
                {
                    var folderLst = db.FolderDetails.ToList();

                    if (folderLst.Count > 0)
                    {
                        var lastUDID = folderLst.OrderByDescending(j => j.Id).FirstOrDefault().UDID;

                        lastUDID = lastUDID.Substring(3);

                        long Id = Convert.ToInt32(lastUDID) + 1;

                        UDID = "FOL" + Id.ToString();
                    }
                    else
                    {
                        UDID = "FOL10001";
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return UDID;
        }
        #endregion

        #region --> Get UDID for Document | Add | Jasmin | 08022018
        public static string GetUDIDForDocument()
        {
            string UDID = string.Empty;
            try
            {
                using (DBEntities db = new DBEntities())
                {
                    var documentLst = db.DocumentDetails.ToList();

                    if (documentLst.Count > 0)
                    {
                        var lastUDID = documentLst.OrderByDescending(j => j.DocumentId).FirstOrDefault().UDID;

                        lastUDID = lastUDID.Substring(3);

                        long Id = Convert.ToInt32(lastUDID) + 1;

                        UDID = "DOC" + Id.ToString();
                    }
                    else
                    {
                        UDID = "DOC10001";
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return UDID;
        }
        #endregion

        #region --> Get UDID for Folder WorkFlow | Add | kiiran | 09022021
        public static string GetUDIDForFolderWorkFlow()
        {
            string UDID = string.Empty;
            try
            {
                using (DBEntities db = new DBEntities())
                {
                    var folderLst = db.WF_FolderDetail.ToList();

                    if (folderLst.Count > 0)
                    {
                        var lastUDID = folderLst.OrderByDescending(j => j.WFFolderDetailId).FirstOrDefault().UDID;

                        lastUDID = lastUDID.Substring(3);

                        long Id = Convert.ToInt32(lastUDID) + 1;

                        UDID = "FOL" + Id.ToString();
                    }
                    else
                    {
                        UDID = "FOL10001";
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return UDID;
        }
        #endregion

        #region --> Get UDID for Document WorkFlow | Add | kiiran | 09022021
        public static string GetUDIDForDocumentWorkFlow()
        {
            string UDID = string.Empty;
            try
            {
                using (DBEntities db = new DBEntities())
                {
                    var documentLst = db.WF_DocumentDetail.ToList();

                    if (documentLst.Count > 0)
                    {
                        var lastUDID = documentLst.OrderByDescending(j => j.WFDocumentId).FirstOrDefault().UDID;

                        lastUDID = lastUDID.Substring(3);

                        long Id = Convert.ToInt32(lastUDID) + 1;

                        UDID = "DOC" + Id.ToString();
                    }
                    else
                    {
                        UDID = "DOC10001";
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return UDID;
        }
        #endregion

        #region --> Get User List | Add | Jasmin | 07022018 | SP CALL | Sweta Patel |05032018
        public static List<SelectListItem> GetUserList()
        {

            List<SelectListItem> lstUser = new List<SelectListItem>();
            List<UserNameWithRoleList> lstUserData = new List<UserNameWithRoleList>();

            try
            {
                DAL objDAL = new DAL();
                SortedList slProject = new SortedList();
                // slProject.Add("@WardId", wardId);
                lstUserData = objDAL.ConvertToList<UserNameWithRoleList>
                    (objDAL.GetDataTable("uspGetUserRoleListByDepartment", slProject)
                    ).ToList();

                lstUser.Add(new SelectListItem()
                {
                    Text = Resource.Select_User,
                    Value = ""
                });

                foreach (var data in lstUserData)
                {
                    SelectListItem item = new SelectListItem();
                    item.Text = data.FullName + " (" + data.UserNameApp + ") ";
                    item.Value = data.Id.ToString();
                    lstUser.Add(item);
                }
                return lstUser;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region --> Get User List by Department | Add | Dhrumil | 20022018
        public static List<SelectListItem> GetUserListByDepartment(long deptid, long userid = 0)
        {
            List<SelectListItem> lstUser = new List<SelectListItem>();
            List<UserNameWithRoleList> lstUserData = new List<UserNameWithRoleList>();

            try
            {
                lstUser.Add(new SelectListItem()
                {
                    Text = Resource.Select_User,
                    Value = ""
                });

                if (deptid > 0)
                {
                    DAL objDAL = new DAL();
                    SortedList slProject = new SortedList();
                    slProject.Add("@DepartmentId", deptid);
                    lstUserData = objDAL.ConvertToList<UserNameWithRoleList>
                        (objDAL.GetDataTable("uspGetUserRoleListByDepartment", slProject)
                        ).ToList();

                    foreach (var data in lstUserData)
                    {
                        SelectListItem item = new SelectListItem();
                        item.Text = data.FullName + " (" + data.UserNameApp + ") ";
                        item.Value = data.Id.ToString();
                        item.Selected = (data.Id == userid);
                        lstUser.Add(item);
                    }
                }
                return lstUser;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region --> Get User List by Cabinet | Add | Jasmin | 10042018
        public static List<SelectListItem> GetUserListByCabinet()
        {
            List<SelectListItem> lstUser = new List<SelectListItem>();
            List<UserRightVM> lstUserData = new List<UserRightVM>();
            long cabinetId = GetCabinetId();
            try
            {
                DAL objDAL = new DAL();
                SortedList slProject = new SortedList();
                slProject.Add("@cabinetId", cabinetId);
                lstUserData = objDAL.ConvertToList<UserRightVM>
                    (objDAL.GetDataTable("uspGetUserListByCabinet", slProject)
                    ).ToList();
                foreach (var data in lstUserData)
                {
                    SelectListItem item = new SelectListItem();
                    item.Text = data.FullName + " (" + data.RoleName + ") ";
                    item.Value = data.UserId.ToString();
                    lstUser.Add(item);
                }

                return lstUser;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region --> Get Department List | Add | Dhrumil | 07022018
        public static List<SelectListItem> GetDepartmentList()
        {
            List<SelectListItem> lstDepartment = new List<SelectListItem>();

            using (DBEntities db = new DBEntities())
            {
                lstDepartment.Add(new SelectListItem()
                {
                    Text = Resource.Select_Department,
                    Value = ""
                });

                var Departmentlist = db.DepartmentMasters.Where(x => x.IsActive && !x.IsDelete).OrderBy(s => s.Id).ToList();

                foreach (var data in Departmentlist)
                {
                    SelectListItem item = new SelectListItem();
                    item.Text = data.Name;
                    item.Value = data.Id.ToString();
                    lstDepartment.Add(item);
                }
            }
            return lstDepartment;
        }
        #endregion

        #region --> Get Section List By Department | Add | Dhrumil | 07022018
        public static List<SelectListItem> GetSectionListByDepartment(long DepartmentId, long SectionId = 0)
        {
            List<SelectListItem> lstSection = new List<SelectListItem>();

            using (DBEntities db = new DBEntities())
            {
                lstSection.Add(new SelectListItem()
                {
                    Text = Resource.Select_Section,
                    Value = ""
                });

                var Sectionlist = db.SectionMasters.Where(x => x.IsActive && !x.IsDelete).Where(j => j.DepartmentId == DepartmentId).OrderBy(s => s.Name).ToList();

                foreach (var data in Sectionlist)
                {
                    SelectListItem item = new SelectListItem();
                    item.Text = data.Name;
                    item.Value = data.Id.ToString();
                    item.Selected = (data.Id == SectionId);
                    lstSection.Add(item);
                }
            }
            return lstSection;
        }
        #endregion

        #region --> Get Category List | Add | Jasmin | 07022018
        public static List<SelectListItem> GetCategoryList()
        {
            List<SelectListItem> lstCategory = new List<SelectListItem>();

            using (DBEntities db = new DBEntities())
            {
                lstCategory.Add(new SelectListItem()
                {
                    Text = Resource.Select_Category,
                    Value = ""
                });

                var Categorylist = db.CategoryMasters.Where(x => x.IsActive && !x.IsDelete).OrderBy(s => s.Id).ToList();

                foreach (var data in Categorylist)
                {
                    SelectListItem item = new SelectListItem();
                    item.Text = data.Name;
                    item.Value = data.Id.ToString();
                    lstCategory.Add(item);
                }
            }
            return lstCategory;
        }
        #endregion

        #region --> Get File Type | Add | Krupali | 02082018
        public static List<SelectListItem> GetFileType()
        {
            List<SelectListItem> lstfile = new List<SelectListItem>();

            using (DBEntities db = new DBEntities())
            {
                lstfile.Add(new SelectListItem()
                {
                    Text = Resource.Select,
                    Value = ""
                });

                var FileTypeList = db.DocumentVersionDetails.Where(x => x.IsActive && !x.IsDelete && x.IsCurrent).OrderBy(s => s.DocumentType).Select(s => s.DocumentType).Distinct().ToList();

                foreach (var data in FileTypeList)
                {
                    SelectListItem item = new SelectListItem();
                    item.Text = data;
                    item.Value = data;
                    lstfile.Add(item);
                }
            }
            return lstfile;
        }
        #endregion

        #region --> Get Category List for Search | Add | krupali | 06082018
        public static List<SelectListItem> GetCategorySearchList()
        {
            List<SelectListItem> lstCategory = new List<SelectListItem>();

            using (DBEntities db = new DBEntities())
            {
                lstCategory.Add(new SelectListItem()
                {
                    Text = Resource.Select,
                    Value = ""
                });

                var Categorylist = db.CategoryMasters.Where(x => x.IsActive && !x.IsDelete).OrderBy(s => s.Id).ToList();

                foreach (var data in Categorylist)
                {
                    SelectListItem item = new SelectListItem();
                    item.Text = data.Name;
                    item.Value = data.Id.ToString();
                    lstCategory.Add(item);
                }
            }
            return lstCategory;
        }
        #endregion

        #region --> Get SubCategory List for search | Add | krupali | 06082018
        public static List<SelectListItem> GetSubCategorySearchList(long? CategoryId, long? SubCategoryId)
        {
            List<SelectListItem> lstSubCategory = new List<SelectListItem>();

            using (DBEntities db = new DBEntities())
            {
                lstSubCategory.Add(new SelectListItem()
                {
                    Text = Resource.Select,
                    Value = ""
                });

                var SubCategorylist = db.SubCategoryMasters.Where(x => x.IsActive && !x.IsDelete).Where(j => j.CategoryId == CategoryId).OrderBy(s => s.Name).ToList();

                foreach (var data in SubCategorylist)
                {
                    SelectListItem item = new SelectListItem();
                    item.Text = data.Name;
                    item.Value = data.Id.ToString();
                    item.Selected = (data.Id == SubCategoryId);
                    lstSubCategory.Add(item);
                }
            }
            return lstSubCategory;
        }
        #endregion

        #region --> Get SubCategory List | Add | Jasmin | 07022018
        public static List<SelectListItem> GetSubCategoryList(long? CategoryId, long? SubCategoryId)
        {
            List<SelectListItem> lstSubCategory = new List<SelectListItem>();

            using (DBEntities db = new DBEntities())
            {
                lstSubCategory.Add(new SelectListItem()
                {
                    Text = Resource.Select_SubCategory,
                    Value = ""
                });

                var SubCategorylist = db.SubCategoryMasters.Where(x => x.IsActive && !x.IsDelete).Where(j => j.CategoryId == CategoryId).OrderBy(s => s.Name).ToList();

                foreach (var data in SubCategorylist)
                {
                    SelectListItem item = new SelectListItem();
                    item.Text = data.Name;
                    item.Value = data.Id.ToString();
                    item.Selected = (data.Id == SubCategoryId);
                    lstSubCategory.Add(item);
                }
            }
            return lstSubCategory;
        }
        #endregion

        #region --> Get Section List | Add | Dhrumil | 05032018 
        public static List<SelectListItem> GetApplicationList(long RoleId = 0)
        {
            List<SelectListItem> lstApplication = new List<SelectListItem>();

            using (DBEntities db = new DBEntities())
            {
                lstApplication.Add(new SelectListItem()
                {
                    Text = Resource.Select_Application,
                    Value = ""
                });
                List<ApplicationMaster> ApplicationList = new List<ApplicationMaster>();
                //if (RoleId == 0)
                //{
                //    ApplicationList = db.ApplicationMasters.OrderBy(s => s.ApplicationId).ToList();
                //}
                //else
                //{
                //    ApplicationList = db.ApplicationMasters.Where(x => x.ApplicationId != (int)EnumList.Application.SuperAdmin).OrderBy(s => s.ApplicationId).ToList();
                //}

                ApplicationList = db.ApplicationMasters.Where(x => x.ApplicationId != (int)EnumList.Application.SuperAdmin).OrderBy(s => s.ApplicationId).ToList();

                foreach (var data in ApplicationList)
                {
                    SelectListItem item = new SelectListItem();
                    item.Text = data.Name;
                    item.Value = data.ApplicationId.ToString();
                    lstApplication.Add(item);
                }
            }
            return lstApplication;
        }
        #endregion

        #region --> All list of cabinet | GET | Add | Jaydeep shah | 08022018
        public static List<SelectListItem> GetCabinetList()
        {
            List<SelectListItem> lstWard = new List<SelectListItem>();
            try
            {
                using (DBEntities objdbContext = new DBEntities())
                {
                    List<CabinetMaster> lstTemp = objdbContext.CabinetMasters.Where(x => x.IsActive && !x.IsDelete).ToList();
                    if (lstTemp.Count() > 0)
                    {
                        lstTemp.ForEach(s =>
                        {
                            SelectListItem objSelectListItem = new SelectListItem();
                            objSelectListItem.Text = Convert.ToString(s.Name);
                            objSelectListItem.Value = Convert.ToString(s.Id);

                            lstWard.Add(objSelectListItem);
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return lstWard;
        }
        #endregion

        #region --> Get Position List By Section | Add | Dhrumil | 20022018
        public static List<SelectListItem> GetPositionListBySection(long SectiontId)
        {
            List<SelectListItem> lstPosition = new List<SelectListItem>();

            using (DBEntities db = new DBEntities())
            {
                lstPosition.Add(new SelectListItem()
                {
                    Text = Resource.Select_Position,
                    Value = ""
                });

                var Positionlist = db.PositionMasters.Where(x => x.IsActive && !x.IsDelete).Where(j => j.SectionId == SectiontId).OrderBy(s => s.Name).ToList();

                foreach (var data in Positionlist)
                {
                    SelectListItem item = new SelectListItem();
                    item.Text = data.Name;
                    item.Value = data.Id.ToString();
                    lstPosition.Add(item);
                }
            }
            return lstPosition;
        }
        #endregion

        #region Get Active Cabinet List | Dhrumil Patel | 08-12-2018
        public static List<SelectListItem> bindcabinet()
        {
            List<SelectListItem> Cabinet = new List<SelectListItem>();
            using (DBEntities db = new DBEntities())
            {
                var lstCabinet = db.CabinetMasters.Where(x => x.IsActive && !x.IsDelete).ToList();

                if (lstCabinet.Count() > 0)
                {
                    foreach (var item in lstCabinet)
                    {
                        Cabinet.Add(new SelectListItem()
                        {
                            Text = item.Name,
                            Value = item.Id.ToString(),
                            Selected = false
                        });
                    }
                }
            }
            return Cabinet;
        }
        #endregion

        #region Get Active Role List | Dhrumil Patel | 08-12-2018
        public static List<SelectListItem> bindRoles()
        {
            List<SelectListItem> Role = new List<SelectListItem>();
            using (DBEntities db = new DBEntities())
            {
                var lstRole = db.RoleMasters.Where(x => x.IsActive).ToList().OrderByDescending(x => x.Id);

                if (lstRole.Count() > 0)
                {
                    foreach (var item in lstRole)
                    {
                        Role.Add(new SelectListItem()
                        {
                            Text = item.Name,
                            Value = item.Id.ToString()
                        });
                    }
                }
            }
            return Role;
        }
        #endregion

        #region --> All list of cabinet by user | GET | Add | Dhrumil Patel | 07032018
        public static List<SelectListItem> GetCabinetListByUserId(long userId)
        {
            List<SelectListItem> lstWard = new List<SelectListItem>();
            try
            {
                using (DBEntities objdbContext = new DBEntities())
                {
                    var cabinetids = objdbContext.UserCabinetDetails.Where(x => x.UserId == userId).Select(x => x.CabinetId).ToList();
                    List<CabinetMaster> lstTemp = objdbContext.CabinetMasters.Where(x => cabinetids.Contains(x.Id) && x.IsActive && !x.IsDelete).ToList();
                    lstWard.Add(new SelectListItem()
                    {
                        Text = Resource.Cabinet_Select,
                        Value = ""
                    });
                    if (lstTemp.Count() > 0)
                    {
                        lstTemp.ForEach(s =>
                        {
                            SelectListItem objSelectListItem = new SelectListItem();
                            objSelectListItem.Text = Convert.ToString(s.Name);
                            objSelectListItem.Value = Convert.ToString(s.Id);

                            lstWard.Add(objSelectListItem);
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return lstWard;
        }
        #endregion

        #region -->  list of cabinet role wise | GET | Add | Jaydeep shah | 08022018
        public static List<SelectListItem> GetCabinetList_RoleWise(long id)
        {
            List<SelectListItem> lstWard = new List<SelectListItem>();
            long ApplicationId = GetApplicationId();
            List<CabinetVM> lstTemp = new List<CabinetVM>();
            try
            {
                using (DBEntities db = new DBEntities())
                {

                    if (ApplicationId == (int)EnumList.Application.SuperAdmin)
                    {
                        lstTemp = db.CabinetMasters.Where(x => x.IsActive && !x.IsDelete).Select(j => new CabinetVM
                        {
                            Id = j.Id,
                            Name = j.Name
                        }).ToList();
                    }
                    else
                    {
                        lstTemp = (from p in db.CabinetMasters
                                   join q in db.UserCabinetDetails on p.Id equals q.CabinetId
                                   join r in db.LoginCredentials on q.UserId equals r.Id
                                   where p.IsActive == true && r.Id == id
                                   select new CabinetVM
                                   {
                                       Id = p.Id,
                                       Name = p.Name
                                   }).ToList();
                    }

                    if (lstTemp.Count() > 0)
                    {
                        lstTemp.ForEach(s =>
                        {
                            SelectListItem objSelectListItem = new SelectListItem();
                            objSelectListItem.Text = Convert.ToString(s.Name);
                            objSelectListItem.Value = Convert.ToString(s.Id);
                            lstWard.Add(objSelectListItem);
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return lstWard;
        }
        #endregion

        #region --> Get Form Template List | Add | Jasmin | 09022018
        public static List<SelectListItem> GetFormTemplateList()
        {
            List<SelectListItem> lstFormTemplate = new List<SelectListItem>();
            var cabinetId = GetCabinetId();
            using (DBEntities db = new DBEntities())
            {
                lstFormTemplate.Add(new SelectListItem()
                {
                    Text = Resource.Select_FormTemplate,
                    Value = ""
                });

                var FormTemplatelist = (from form in db.FormTemplates
                                        join fcabinet in db.FormCabinetDetails on form.Id equals fcabinet.FormId
                                        where form.IsActive && !form.IsDelete && fcabinet.CabinetId == cabinetId
                                        select new
                                        {
                                            form.Id,
                                            form.FormTitle
                                        }).Distinct().ToList();

                var FormTemplatelist1 = db.FormTemplateDetails.Where(x => x.CabinetID == cabinetId && x.IsActive && !x.IsDelete).OrderBy(s => s.Id).ToList();

                foreach (var data in FormTemplatelist)
                {
                    SelectListItem item = new SelectListItem();
                    item.Text = data.FormTitle;
                    item.Value = data.Id.ToString();
                    lstFormTemplate.Add(item);
                }
            }
            return lstFormTemplate;
        }
        #endregion

        #region --> Get Doc share UserList | Add | Jasmin | 26022018
        public static List<SelectListItem> GetDocShareUserList()
        {
            List<SelectListItem> lstUser = new List<SelectListItem>();
            List<CabinetWiseUserList> lstUserData = new List<CabinetWiseUserList>();
            long UserId = GetUserId();
            long CabinetId = GetCabinetId();
            long ApplicationId = GetApplicationId();

            using (DBEntities db = new DBEntities())
            {
                DAL objDAL = new DAL();
                SortedList slProject = new SortedList();

                if (ApplicationId != (int)EnumList.Application.SuperAdmin)
                    slProject.Add("@UserId", UserId);

                slProject.Add("@CabinetId", CabinetId);

                lstUserData = objDAL.ConvertToList<CabinetWiseUserList>
                    (objDAL.GetDataTable("uspGetDocShareUserListByUserId", slProject)
                    ).ToList();

                lstUser.Add(new SelectListItem()
                {
                    Text = Resource.Select_User,
                    Value = ""
                });

                foreach (var data in lstUserData)
                {
                    SelectListItem item = new SelectListItem();
                    item.Text = data.FullName + " (" + data.RoleName + ") ";
                    item.Value = data.UserId.ToString();
                    lstUser.Add(item);
                }
            }
            return lstUser;
        }
        #endregion

        #region --> IsUserAssigned Function | Add | Dhrumil Patel | 14032018
        public static bool IsUserAssigned(string RequestIds, int RequestFor)
        {
            long userCount = 0;
            DBEntities db = new DBEntities();
            try
            {
                var RequestId = RequestIds.Split(',').ToList();
                if (RequestFor != 0 && RequestId.Count() > 0)
                {
                    if (RequestFor == (int)EnumMasterType.Position)
                    {
                        userCount = db.LoginCredentials.Where(x => RequestId.Contains(x.PositionId.ToString()) && !x.IsDelete && x.IsActive).ToList().Count();
                    }
                    else if (RequestFor == (int)EnumMasterType.Section)
                    {
                        userCount = db.LoginCredentials.Where(x => RequestId.Contains(x.SectionId.ToString()) && !x.IsDelete && x.IsActive).ToList().Count();
                    }
                    else if (RequestFor == (int)EnumMasterType.Department)
                    {
                        userCount = db.LoginCredentials.Where(x => RequestId.Contains(x.DepartmentId.ToString()) && !x.IsDelete && x.IsActive).ToList().Count();
                    }
                    else if (RequestFor == (int)EnumMasterType.Role)
                    {
                        userCount = (from role in db.UserRoles
                                     join user in db.LoginCredentials on role.UserId equals user.Id
                                     where RequestId.Contains(role.RoleId.ToString()) && user.IsActive && !user.IsDelete
                                     select new
                                     {
                                         user.Id
                                     }).ToList().Count();
                    }
                    else if (RequestFor == (int)EnumMasterType.Cabinet)
                    {
                        userCount = (from cabinet in db.UserCabinetDetails
                                     join user in db.LoginCredentials on cabinet.UserId equals user.Id
                                     where RequestId.Contains(cabinet.CabinetId.ToString()) && user.IsActive && !user.IsDelete
                                     select new
                                     {
                                         user.Id
                                     }).ToList().Count();
                    }
                    else
                    {
                        return false;
                    }

                    if (userCount > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region --> IsFileAssigned Function |Add | Jasmin Vohra | 15032018
        public static bool IsFileAssigned(string RequestIds, int RequestFor)
        {
            long fileCount = 0;
            DBEntities db = new DBEntities();
            try
            {
                var RequestId = RequestIds.Split(',').ToList();
                if (RequestFor != 0 && RequestId.Count() > 0)
                {
                    if (RequestFor == (int)EnumMasterType.Category)
                    {
                        fileCount = db.DocumentDetails.Where(x => RequestId.Contains(x.CategoryId.ToString()) && !x.IsDelete && x.IsActive).ToList().Count();
                    }
                    else if (RequestFor == (int)EnumMasterType.SubCategory)
                    {
                        fileCount = db.DocumentDetails.Where(x => RequestId.Contains(x.SubCategoryId.ToString()) && !x.IsDelete && x.IsActive).ToList().Count();
                    }
                    else
                    {
                        return false;
                    }

                    if (fileCount > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region --> Get limited font List | SWeta Patel | 11/04/2018
        public static List<SelectListItem> bindFonts()
        {
            List<SelectListItem> fonts = new List<SelectListItem>();
            using (DBEntities db = new DBEntities())
            {
                var lst = db.FontFamilies.ToList();

                if (lst.Count() > 0)
                {
                    foreach (var item in lst)
                    {
                        fonts.Add(new SelectListItem()
                        {
                            Text = item.FontFamilyCSS,
                            Value = item.FontFamilyCSS,
                            Selected = (item.FontFamilyCSS == "Segoe UI" ? true : false)
                        });
                    }
                }
            }
            return fonts;
        }
        #endregion

        #region --> Get WorkFlow Action List | Add | Dhrumil Patel | 01052018
        public static List<SelectListItem> GetActionList()
        {
            List<SelectListItem> lstAction = new List<SelectListItem>();
            long cabinetId = GetCabinetId();
            try
            {
                //DAL objDAL = new DAL();
                //SortedList slProject = new SortedList();
                //slProject.Add("@cabinetId", cabinetId);
                //lstUserData = objDAL.ConvertToList<WFUserListByCabinet>
                //    (objDAL.GetDataTable("uspGetUserListByCabinet", slProject)
                //    ).ToList();
                using (DBEntities db = new DBEntities())
                {
                    var actionLst = db.WF_Actions.Where(x => x.ActionId != (int)EnumActionType.Initiate).ToList();

                    foreach (var data in actionLst)
                    {
                        SelectListItem item = new SelectListItem();
                        item.Text = data.ActionName;
                        item.Value = data.ActionId.ToString();
                        item.Selected = true;
                        lstAction.Add(item);
                    }
                }
                return lstAction;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region --> Get Active Application List | Dhrumil Patel | 08-05-2018
        public static List<SelectListItem> bindApplications()
        {
            List<SelectListItem> Role = new List<SelectListItem>();
            using (DBEntities db = new DBEntities())
            {
                var lstApplication = db.ApplicationMasters.Where(x => x.ApplicationId != (long)Application.SuperAdmin).ToList().OrderByDescending(x => x.ApplicationId);

                if (lstApplication.Count() > 0)
                {
                    foreach (var item in lstApplication)
                    {
                        Role.Add(new SelectListItem()
                        {
                            Text = item.Name,
                            Value = item.ApplicationId.ToString()
                        });
                    }
                }
            }
            return Role;
        }
        #endregion

        #region --> Get User by Cabinet for for Dual List | Add | Dhrumil Patel | 10042018
        public static List<SelectListItem> GetUserByCabinetForDualList()
        {
            List<SelectListItem> lstUser = new List<SelectListItem>();
            List<WFUserListByCabinet> lstUserData = new List<WFUserListByCabinet>();
            long cabinetId = GetCabinetId();
            try
            {
                DAL objDAL = new DAL();
                SortedList slProject = new SortedList();
                slProject.Add("@cabinetId", cabinetId);
                lstUserData = objDAL.ConvertToList<WFUserListByCabinet>
                    (objDAL.GetDataTable("uspGetUserListByCabinet", slProject)
                    ).ToList();

                foreach (var data in lstUserData)
                {
                    var imgText = "";
                    if (data.ProfileImage != null)
                    {
                        imgText = "Uploads/ProfilePic/" + data.UserId.ToString() + "/" + data.ProfileImage;
                    }
                    else
                    {
                        imgText = "Images/User1.png";
                    }

                    SelectListItem item = new SelectListItem();
                    item.Text = data.FullName + " - " + data.EmailId + "(" + imgText;
                    item.Value = data.UserId.ToString();
                    lstUser.Add(item);
                }

                return lstUser;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region --> Get Department List by User Cabinet for Dual List | Add | Dhrumil | 07052018
        public static List<SelectListItem> GetDepartmentByUserCabinetForDualList()
        {
            List<SelectListItem> lstDepartment = new List<SelectListItem>();
            var cabinetId = GetCabinetId();
            using (DBEntities db = new DBEntities())
            {
                var Departmentlist = db.DepartmentMasters.Where(x => x.IsActive && !x.IsDelete).OrderBy(s => s.Name).ToList();
                foreach (var data in Departmentlist)
                {
                    var userIds = db.LoginCredentials.Where(j => j.DepartmentId == data.Id && j.IsActive && !j.IsDelete)
                                   .Select(x => x.Id).ToList();
                    var userList = db.UserCabinetDetails.Where(x => x.CabinetId == cabinetId && userIds.Contains(x.UserId)).ToList();
                    if (userList.Count > 0)
                    {
                        SelectListItem item = new SelectListItem();
                        var imgText = "Images/department.png";
                        item.Text = data.Name + " - " + userList.Count + " users(" + imgText;
                        item.Value = data.Id.ToString();
                        lstDepartment.Add(item);
                    }
                }
            }
            return lstDepartment;
        }
        #endregion

        #region --> Get Group List by Cabinet for Dual List | Add | Dhrumil | 09052018
        public static List<SelectListItem> GetGroupByCabinetForDualList()
        {
            List<SelectListItem> lstGroup = new List<SelectListItem>();
            var cabinetId = GetCabinetId();
            using (DBEntities db = new DBEntities())
            {
                var Grouplist = db.GroupDetails.Where(x => x.CabinetId == cabinetId && x.IsActive && !x.IsDelete).OrderBy(s => s.Name).ToList();

                foreach (var data in Grouplist)
                {
                    var userList = db.GroupUserDetails.Where(x => x.GroupId == data.Id).ToList();
                    SelectListItem item = new SelectListItem();
                    var imgText = "Images/group.png";
                    item.Text = data.Name + " - " + userList.Count + " users(" + imgText;
                    item.Value = data.Id.ToString();
                    lstGroup.Add(item);
                }
            }
            return lstGroup;
        }
        #endregion

        #region --> Get User Doc Shared List by Cabinet and User | Add | Jasmin | 10042018
        public static List<SelectListItem> GetDocSharedListByCabinet(string shareTo)
        {
            List<SelectListItem> lstUser = new List<SelectListItem>();
            List<DocSharedVM> lstDocSharedData = new List<DocSharedVM>();
            long userId = GetUserId();
            long cabinetId = GetCabinetId();
            try
            {
                DAL objDAL = new DAL();
                SortedList slProject = new SortedList();
                slProject.Add("@userId", userId);
                slProject.Add("@cabinetId", cabinetId);
                slProject.Add("@mode", shareTo);
                lstDocSharedData = objDAL.ConvertToList<DocSharedVM>
                    (objDAL.GetDataTable("uspGetDocSharedListByCabinet", slProject)
                    ).ToList();

                if (shareTo == "User")
                {
                    foreach (var data in lstDocSharedData)
                    {
                        SelectListItem item = new SelectListItem();
                        item.Text = data.Name;
                        item.Value = data.UserId.ToString();
                        lstUser.Add(item);
                    }
                }
                else
                {
                    foreach (var data in lstDocSharedData)
                    {
                        SelectListItem item = new SelectListItem();
                        item.Text = data.Name;
                        item.Value = data.GroupId.ToString();
                        lstUser.Add(item);
                    }
                }

                return lstUser;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region --> Check child present or not for the folder | Dhrumil Patel | 07-06-2018
        public static long HasChild(long id)
        {
            long childCount = 0;
            using (DBEntities db = new DBEntities())
            {
                try
                {
                    childCount = db.FolderDetails.Where(x => x.ParentFolderId == id).Count();

                    if (childCount == 0)
                    {
                        childCount = db.DocumentDetails.Where(x => x.FolderId == id).Count();
                    }
                }
                catch (Exception e)
                {
                    return childCount;
                }
            }
            return childCount;
        }        
        #endregion

        #region --> Check child present or not for the folder | Dhrumil Patel | 07-06-2018
        public static bool IsCreatedBy(long id, bool isDoc)
        {
            bool flag = false;
            try
            {
                using (var db = new DBEntities())
                {
                    var userId = GetUserId().ToString();
                    if (isDoc)
                    {
                        var docdetail = db.DocumentDetails.Find(id);
                        if (docdetail != null)
                        {
                            if (docdetail.CreatedBy == userId)
                            {
                                flag = true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }
                    else
                    {
                        var foldetail = db.FolderDetails.Find(id);
                        if (foldetail != null)
                        {
                            if (foldetail.CreatedBy == userId)
                            {
                                flag = true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }
                }
            }
            catch
            {
                return false;
            }
            return flag;
        }
        #endregion

        #region --> Get User Folder/File Shared Permission | Dhrumil Patel | 07-06-2018
        public static UserDocShareDetail GetUserPermission(bool isDoc, long id)
        {
            var data = new UserDocShareDetail();
            using (var db = new DBEntities())
            {
                var userId = GetUserId();
                var sharedList = db.UserDocShareDetails.ToList();
                if (isDoc)
                {
                    data = db.UserDocShareDetails.FirstOrDefault(x => x.DocumentId == id && x.DocShareUserId == userId);
                    var sharedgroupList = (from sl in sharedList
                                           join gd in db.GroupDetails on sl.DocShareGroupId equals gd.Id
                                           join gud in db.GroupUserDetails on gd.Id equals gud.GroupId
                                           where (gd.IsActive && !gd.IsDelete && gud.UserId == userId && sl.DocumentId == id)
                                           select new
                                           {
                                               sl
                                           }).FirstOrDefault();
                    if (sharedgroupList != null)
                    {
                        if (data == null)
                        {
                            data = new UserDocShareDetail();
                        }
                        data.IsUpload = sharedgroupList.sl.IsUpload ? sharedgroupList.sl.IsUpload : (data != null ? data.IsUpload : false);
                        data.IsDownload = sharedgroupList.sl.IsDownload ? sharedgroupList.sl.IsDownload : (data != null ? data.IsDownload : false);
                        data.IsReadComment = sharedgroupList.sl.IsReadComment ? sharedgroupList.sl.IsReadComment : (data != null ? data.IsReadComment : false);
                        data.IsAddComment = sharedgroupList.sl.IsAddComment ? sharedgroupList.sl.IsAddComment : (data != null ? data.IsAddComment : false);
                        data.IsShare = sharedgroupList.sl.IsShare ? sharedgroupList.sl.IsShare : (data != null ? data.IsShare : false);
                        data.IsSeeVersion = sharedgroupList.sl.IsSeeVersion ? sharedgroupList.sl.IsSeeVersion : (data != null ? data.IsSeeVersion : false);
                    }
                }
                else
                {
                    data = db.UserDocShareDetails.FirstOrDefault(x => x.FolderId == id && x.DocShareUserId == userId);
                    var sharedgroupList = (from sl in sharedList
                                           join gd in db.GroupDetails on sl.DocShareGroupId equals gd.Id
                                           join gud in db.GroupUserDetails on gd.Id equals gud.GroupId
                                           where (gd.IsActive && !gd.IsDelete && gud.UserId == userId && sl.FolderId == id)
                                           select new
                                           {
                                               sl
                                           }).FirstOrDefault();
                    if (sharedgroupList != null)
                    {
                        if (data == null)
                        {
                            data = new UserDocShareDetail();
                        }
                        data.IsUpload = sharedgroupList.sl.IsUpload ? sharedgroupList.sl.IsUpload : (data != null ? data.IsUpload : false);
                        data.IsDownload = sharedgroupList.sl.IsDownload ? sharedgroupList.sl.IsDownload : (data != null ? data.IsDownload : false);
                        data.IsReadComment = sharedgroupList.sl.IsReadComment ? sharedgroupList.sl.IsReadComment : (data != null ? data.IsReadComment : false);
                        data.IsAddComment = sharedgroupList.sl.IsAddComment ? sharedgroupList.sl.IsAddComment : (data != null ? data.IsAddComment : false);
                        data.IsShare = sharedgroupList.sl.IsShare ? sharedgroupList.sl.IsShare : (data != null ? data.IsShare : false);
                        data.IsSeeVersion = sharedgroupList.sl.IsSeeVersion ? sharedgroupList.sl.IsSeeVersion : (data != null ? data.IsSeeVersion : false);
                        data.DocShareUserId = sharedgroupList.sl.DocShareUserId;
                        data.DocShareGroupId = sharedgroupList.sl.DocShareGroupId;
                        data.UserId = sharedgroupList.sl.UserId;
                    }
                }
            }
            return data != null ? data : new UserDocShareDetail();
        }
        #endregion

        #region --> Get Share User List For User By Cabinet | Add | Dhrumil Patel | 15-06-2018
        public static List<SelectListItem> GetShareUserListForUserByCabinet()
        {
            List<SelectListItem> lstUser = new List<SelectListItem>();
            List<DocSharedUser> lstDocSharedData = new List<DocSharedUser>();
            long userId = GetUserId();
            long cabinetId = GetCabinetId();
            try
            {
                DAL objDAL = new DAL();
                SortedList slProject = new SortedList();
                slProject.Add("@User", userId);
                slProject.Add("@CabinetId", cabinetId);
                lstDocSharedData = objDAL.ConvertToList<DocSharedUser>
                    (objDAL.GetDataTable("uspGetShareUserListForUser", slProject)
                    ).ToList();

                foreach (var data in lstDocSharedData)
                {
                    SelectListItem item = new SelectListItem();
                    item.Text = data.Name;
                    item.Value = data.UserId.ToString();
                    lstUser.Add(item);
                }

                return lstUser;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region --> Get Link Doc List By Cabinet | Add | Sweta | 20062018
        public static List<SelectListItem> GetLinkDocListByCabinet(long DocumentId = 0)
        {
            List<SelectListItem> lst = new List<SelectListItem>();
            List<LinkDocumentList> lstLinkDoc = new List<LinkDocumentList>();
            long userId = GetUserId();
            long cabinetId = GetCabinetId();
            try
            {
                DAL objDAL = new DAL();
                SortedList slProject = new SortedList();
                slProject.Add("@userId", userId);
                slProject.Add("@cabinetId", cabinetId);
                slProject.Add("@DocumentId", DocumentId);
                lstLinkDoc = objDAL.ConvertToList<LinkDocumentList>
                    (objDAL.GetDataTable("uspGetDocumentListForLink", slProject)
                    ).ToList();

                foreach (var data in lstLinkDoc)
                {
                    SelectListItem item = new SelectListItem();
                    item.Text = data.DocumentType + ',' + data.DisplayText;
                    item.Value = data.DocumentId.ToString();
                    lst.Add(item);
                }

                return lst;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region --> Check Is File Permission (Public/Favorite/SharedWithMe/RecycleBin/Cabinet) | Add | Dhruvin Patel | 25062018
        public static long CheckIsFilePermission(string permissionBy, long documentId, long cabinetID, long userID)
        {
            try
            {
                using (DBEntities db = new DBEntities())
                {
                    var doc = db.DocumentVersionDetails.Where(x => x.DocumentId == documentId && x.IsActive && !x.IsDelete && x.IsCurrent).OrderByDescending(x => x.Id).FirstOrDefault();
                    if (permissionBy.ToLower() == "public")
                    {
                        if (doc != null && doc.IsPublic)
                        {
                            return doc.Id;
                        }
                    }
                    else if (permissionBy.ToLower() == "shared")
                    {
                        if (doc != null)
                        {
                            if (db.UserDocShareDetails.Any(x => x.DocumentId == doc.DocumentId && x.CabinetId == cabinetID && x.UserId == userID))
                                return doc.Id;
                        }
                    }
                    else if (permissionBy.ToLower() == "favorite")
                    {
                        if (doc != null)
                        {
                            if (db.Favorites.Any(x => x.DocumentId == doc.DocumentId && x.CabinetId == cabinetID && x.UserId == userID))
                                return doc.Id;
                        }
                    }
                    else
                    {
                        if (doc != null)
                        {
                            return doc.Id;
                        }
                    }
                    return 0;
                }
            }
            catch (Exception)
            {
                return 0;
            }
        }
        #endregion

        #region --> Share File Link On Email | Dhruvin Patel | 25062018
        public static bool SendShareEmailLink(long userId, List<string> docIds, string[] userIds)
        {
            try
            {
                string docLink = string.Empty;
                string hostURL = WebConfigurationManager.AppSettings["RedirectLink"].ToString();
                using (DBEntities db = new DBEntities())
                {
                    LoginCredential currentLoginModel = db.LoginCredentials.Find(userId);
                    if (docIds != null && docIds.Count > 0)
                    {
                        docLink = "<p> Document Link : <br/>";
                        foreach (string dvID in docIds)
                        {
                            DocumentVersionDetail dvModel = db.DocumentVersionDetails.Find(Convert.ToInt64(dvID));
                            if (dvModel != null)
                            {
                                //string url = hostURL + dvModel.DocumentPath.Replace("~", "");
                                string encDVID = UtilityCommonFunctions.Encrypt(dvID);
                                string url = hostURL + "/Account/DownloadFile?id=ToUserID&dvID=" + encDVID;
                                docLink = docLink + dvModel.DocumentName + " : " + "<a href='" + url + "'>Download</a>" + "<br/>";
                            }
                        }
                        docLink = docLink + "</p>";
                    }

                    foreach (string uID in userIds)
                    {
                        LoginCredential model = db.LoginCredentials.Find(Convert.ToInt64(uID));
                        if (model != null)
                        {
                            string encUserID = UtilityCommonFunctions.Encrypt(uID);
                            Dictionary<string, string> replacement = new Dictionary<string, string>();
                            //replacement.Add("#ToUserID#", encUserID);
                            replacement.Add("#SenderFirstName#", currentLoginModel.FirstName);
                            replacement.Add("#SenderLastName#", currentLoginModel.LastName);
                            replacement.Add("#FullName#", model.FirstName + " " + model.LastName);
                            //replacement.Add("#FolderLink#", folderLink);
                            docLink = Regex.Replace(docLink, @"\bToUserID\b", encUserID);
                            replacement.Add("#DocLink#", docLink);
                            UtilityCommonFunctions.SendEmailTemplete((int)EnumList.EmailTemplate.SendDocumentLink, replacement, model.EmailId, string.Empty);

                        }
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region --> Check File CheckOut By User | Dhrumil Patel | 25072018
        public static bool IsCheckOut(long docId)
        {
            try
            {
                using (DBEntities db = new DBEntities())
                {
                    var data = db.DocumentDetails.Find(docId);
                    if (data != null && data.IsCheckOut)
                    {
                        return true;
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion

        #region --> Check File CheckOut By User | Kiran | 09022021
        public static bool IsCheckOutWorkFlow(long docId)
        {
            try
            {
                using (DBEntities db = new DBEntities())
                {
                    var data = db.WF_DocumentDetail.Find(docId);
                    if (data != null && data.IsCheckOut)
                    {
                        return true;
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion

        #region --> Save Notification | Dhrumil Patel | SP By Sweta Patel | 09082018
        public static void SaveNotification(SaveNotificationVM detail)
        {
            try
            {
                DAL objDAL = new DAL();
                SortedList sl = new SortedList();
                sl.Add("@UserId", detail.UserId);
                sl.Add("@CabinetId", detail.CabinetId);
                sl.Add("@FolderId", detail.FolderId);
                sl.Add("@DocumentId", detail.DocumentId);
                sl.Add("@NotificationType", detail.NotificationType);
                sl.Add("@TypeFor", detail.TypeFor);
                sl.Add("@GroupName", detail.GroupName);
                sl.Add("@GroupId", detail.GroupId);
                sl.Add("@Comment", detail.Comment);

                objDAL.ExecuteScaler("uspSaveNotification", sl);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Save Notification of WorkFlow | Kiran Sawant |  02032021
        /// </summary>
        /// <param name="detail"></param>
        public static void SaveNotificationWorkFlow(SaveNotificationWorkFlowVM detail)
        {
            try
            {
                DAL objDAL = new DAL();
                SortedList sl = new SortedList();
                sl.Add("@UserId", detail.UserId);
                sl.Add("@CabinetId", detail.CabinetId);                
                sl.Add("@NotificationType", detail.NotificationType);
                sl.Add("@SendUserId", detail.SendUserId);
                sl.Add("@InstanceId", detail.InstanceId);                
                sl.Add("@Comment", detail.Comment);

                objDAL.ExecuteScaler("uspSaveNotificationWorkFlow", sl);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region --> Remove Linked Document | Dhrumil Patel | SP By Sweta Patel | 10082018
        public static void RemoveLinkedDocument()
        {
            try
            {
                DAL objDAL = new DAL();
                SortedList sl = new SortedList();
                var cabinetId = GetCabinetId();
                sl.Add("@CabinetId", cabinetId);

                objDAL.ExecuteScaler("uspRemoveExtraLink", sl);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region --> Remove Favourite data which are not linked | Sweta Patel | SP By Sweta Patel | 10082018
        public static void RemoveFavouriteDocument()
        {
            try
            {
                DAL objDAL = new DAL();
                SortedList sl = new SortedList();
                var cabinetId = GetCabinetId();
                sl.Add("@CabinetId", cabinetId);
                objDAL.ExecuteScaler("uspRemoveExtraFavouriteData", sl);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region --> Get User List by Cabinet With out Login User | Add | Jasmin | 10042018
        public static List<SelectListItem> GetUserListByCabinetExceptLoginUser()
        {
            List<SelectListItem> lstUser = new List<SelectListItem>();
            List<UserRightVM> lstUserData = new List<UserRightVM>();
            long cabinetId = GetCabinetId();
            long userId = GetUserId();
            try
            {
                DAL objDAL = new DAL();
                SortedList slProject = new SortedList();
                slProject.Add("@cabinetId", cabinetId);
                lstUserData = objDAL.ConvertToList<UserRightVM>
                    (objDAL.GetDataTable("uspGetUserListByCabinet", slProject)
                    ).ToList();
                lstUserData = lstUserData.Where(x => x.UserId != userId).ToList();
                foreach (var data in lstUserData)
                {
                    SelectListItem item = new SelectListItem();
                    item.Text = data.FullName + " (" + data.RoleName + ") ";
                    item.Value = data.UserId.ToString();
                    lstUser.Add(item);
                }

                return lstUser;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region --> Get User List by Cabinet With Static 2 entry for Work Flow | Add | Jasmin | 10042018
        public static List<SelectListItem> GetUserListByCabinetForWF()
        {
            List<SelectListItem> lstUser = new List<SelectListItem>();
            List<UserRightVM> lstUserData = new List<UserRightVM>();
            long cabinetId = GetCabinetId();
            try
            {
                DAL objDAL = new DAL();
                SortedList slProject = new SortedList();
                slProject.Add("@cabinetId", cabinetId);
                lstUserData = objDAL.ConvertToList<UserRightVM>
                    (objDAL.GetDataTable("uspGetUserListByCabinet", slProject)
                    ).ToList();
                foreach (var data in lstUserData)
                {
                    SelectListItem item = new SelectListItem();
                    item.Text = data.FullName + " (" + data.RoleName + ") ";
                    item.Value = data.UserId.ToString();
                    lstUser.Add(item);
                }

				// IMP Note : DHRUMIL : Jyre koe workflow initiate kre tyare jo e aa Select kre to  "-1" or "-2" made 
				//to je user Initiate krto hoy e user no Department levo and e dept no Manager Id  or User Manager Id (-2) mate
				//get kri ne to User Ma nakhvu as ToUser.  (WF : Automated Ma padse ) 

				//  Tow jgya e code correct krvo : 1. Jyre Initiate kre  or jyare next stpe par jay tyre   
				//  Table :WF_Transactions,ToUser ma Id padvo jyre WF_Details ma tane -1,-2 madse
				 

				lstUser.Add(new SelectListItem()
                {
                    Text = Resource.Department_Manager,
                    Value = "-1"
                });

                lstUser.Add(new SelectListItem()
                {
                    Text = Resource.User_Manager,
                    Value = "-2"
                });

                return lstUser;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region --> Get Work flow User List By UserId And Cabinet | Add | Dhrumil Patel | 09042020
        public static List<SelectListItem> GetWFUserListByUserIdAndCabinet()
        {
            List<SelectListItem> lstUser = new List<SelectListItem>();
            List<UserRightVM> lstUserData = new List<UserRightVM>();
            long cabinetId = GetCabinetId();
            long userId = GetUserId();
            try
            {
                DAL objDAL = new DAL();
                SortedList slProject = new SortedList();
                slProject.Add("@userId", userId);
                slProject.Add("@cabinetId", cabinetId);
                lstUserData = objDAL.ConvertToList<UserRightVM>
                    (objDAL.GetDataTable("uspGetWFUserListByUserIdAndCabinet", slProject)
                    ).ToList();
                lstUser.Add(new SelectListItem()
                {
                    Text = "Select Assign To",
                    Value = ""
                });
                foreach (var data in lstUserData)
                {
                    SelectListItem item = new SelectListItem();
                    item.Text = data.FullName + " (" + data.RoleName + ") ";
                    item.Value = data.UserId.ToString();
                    lstUser.Add(item);
                }

                return lstUser;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region --> Get Self Work flow  List By UserId And Cabinet | Add | Kiran | 11092020
        public static List<SelectListItem> GetSelfWFListByUserIdAndCabinet()
        {
            List<SelectListItem> lstUser = new List<SelectListItem>();
            //List<UserRightVM> lstUserData = new List<UserRightVM>();
            List<UserWorkflowListVM> lstUserWorkflow = new List<UserWorkflowListVM>();
            
            long cabinetId = GetCabinetId();
            long userId = GetUserId();
            try
            {
                DAL objDAL = new DAL();
                SortedList slProject = new SortedList();
                slProject.Add("@userId", userId);
                slProject.Add("@cabinetId", cabinetId);
                lstUserWorkflow = objDAL.ConvertToList<UserWorkflowListVM>
                    (objDAL.GetDataTable("uspGetSelfWFListByUserIdAndCabinet", slProject)
                    ).ToList();
                lstUser.Add(new SelectListItem()
                {
                    Text = "Select Work Flow",
                    Value = ""
                });
                foreach (var data in lstUserWorkflow)
                {
                    SelectListItem item = new SelectListItem();
                    item.Text = data.WFName;
                    item.Value = data.WFId.ToString();
                    lstUser.Add(item);
                }

                return lstUser;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region --> Get Priority List | Add | Kiran | 14092020
        public static List<SelectListItem> GetPriorityList()
        {
            List<SelectListItem> lstPriority = new List<SelectListItem>();
          
            try
            {

                lstPriority.Add(new SelectListItem()
                {
                    Text = "Select Priority",
                    Value = ""
                });
                lstPriority.Add(new SelectListItem()
                {
                    Text = "Low",
                    Value = "1"
                });
                lstPriority.Add(new SelectListItem()
                {
                    Text = "Medium",
                    Value = "2"
                });
                lstPriority.Add(new SelectListItem()
                {
                    Text = "High",
                    Value = "3"
                });                

                return lstPriority;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region --> Get Settings List | Add | Kiran Sawant | 04032021
        public static List<SelectListItem> GetSettingsList()
        {
            List<SelectListItem> lstSetting = new List<SelectListItem>();

            using (DBEntities db = new DBEntities())
            {
                lstSetting.Add(new SelectListItem()
                {
                    Text = "Select Setting",
                    Value = ""
                });

                var Settinglist = db.SettingsMasters.Where(x => x.IsActive && !x.IsDelete).OrderBy(s => s.SettingsId).ToList();

                foreach (var data in Settinglist)
                {
                    SelectListItem item = new SelectListItem();
                    item.Text = data.SettingName;
                    item.Value = data.SettingsId.ToString();
                    lstSetting.Add(item);
                }
            }
            return lstSetting;
        }
        #endregion

        #region --> Get UDID for Document | Add | Jasmin | 08022018
        public static bool GetStepOwner()
        {
            bool isvalue = false;
            try
            {
                using (DBEntities db = new DBEntities())
                {
                    var documentLst = db.SettingsMasterDetails.Where(x => x.DetailId == 8 && x.SettingsId == 2).FirstOrDefault();

                    if (documentLst != null)
                    {
                        var Value = documentLst.FieldValue;

                        if (Value == "True")
                        {
                            isvalue = true;
                        }                                                
                    }                    
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return isvalue;
        }
        #endregion
    }


    #region AuditLog attribute | Add |  Sweta Patel | Copy from Loews
    public class AuditLogAttribute : ActionFilterAttribute
    {
        private EnumLogType logType { get; set; }
        private long UserId { get; set; }
        private string Description { get; set; }
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            AuditrialLog auditLog = new AuditrialLog();
            auditLog.UserId = Convert.ToInt64(HttpContext.Current.Session[SessionClass.UserID]);
            auditLog.LogTypeId = (long)logType;
            auditLog.Description = Description + " Requested";
            UtilityCommonFunctions.SaveAuditTrailsLogs(auditLog);
        }
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            AuditrialLog auditLog = new AuditrialLog();
            auditLog.UserId = Convert.ToInt64(HttpContext.Current.Session[SessionClass.UserID]);
            auditLog.LogTypeId = (long)logType;
            auditLog.Description = Description + " Sucess";
            UtilityCommonFunctions.SaveAuditTrailsLogs(auditLog);
        }

        public AuditLogAttribute(EnumLogType logType, string Desc)
        {
            this.logType = logType;
            this.Description = Desc;

        }
    }
    #endregion
}