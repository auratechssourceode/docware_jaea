﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DocWare.Models
{
    public class UserVM
    {
        public long UserId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string EmailId { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public long DepartmentId { get; set; }
        public long SectionId { get; set; }
        public long PositionId { get; set; }
        public long ManagerId { get; set; }
        public string DepartmentName { get; set; }
        public string SectionName { get; set; }
        public string ManagerName { get; set; }
        public string MobileNumber { get; set; }
        public string PhoneNumber { get; set; }
        public string DOB { get; set; }
        public string ProfileImage { get; set; }
        public string RoleName { get; set; }
        public string CabinetName { get; set; }
        public bool SentNotification { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public bool IsEdit { get; set; }
        public string[] UserCabinet { get; set; }
        public string[] Roles { get; set; }
        public string[] Applications { get; set; }
    }

    public class UserAllVM
    {
        public long UserId { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string EmailId { get; set; }
        public string Password { get; set; }
        public string CabinetName { get; set; }
        public string DEPT_SEC_POS { get; set; }
        public string GroupName { get; set; }
        public string ApplicationName { get; set; }

    }

    public class UserNameWithRoleList
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string UserNameApp { get; set; }
        public string UserRolePermission { get; set; }
        public long DepartmentId { get; set; }
        public string Department { get; set; }
    }

    public class CabinetWiseUserList
    {
        public long UserId { get; set; }
        public string FullName { get; set; }
        public string RoleName { get; set; }
    }

    public class DocSharePermissionUserList
    {
        public long UserId { get; set; }
        public string UserName { get; set; }
        public string RoleName { get; set; }
        public long CabinetId { get; set; }
        public long ShareUserId { get; set; }
        public bool IsChecked { get; set; }
        public string SharedUserList { get; set; }
    }

    public class DocSharePermissionDetail
    {
        public long UserId { get; set; }
        public string FullName { get; set; }
        public string CabinetName { get; set; }
        public string[] SharedUsers { get; set; }
        public string[] SharedGroups { get; set; }
    }

    public class UserProfile
    {
        public long UserId { get; set; }
        public long ApplicationId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string EmailId { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string DepartmentName { get; set; }
        public string SectionName { get; set; }
        public string PositionName { get; set; }
        public string ManagerName { get; set; }
        public string MobileNumber { get; set; }
        public string PhoneNumber { get; set; }
        public string DOB { get; set; }
        public string ProfileImage { get; set; }
        public string CabinetName { get; set; }
        public string RoleName { get; set; }
        public string ApplicationName { get; set; }
        public long SubsidiaryUserId { get; set; }
        public bool AssignSubsidiary { get; set; }
    }

    public class WorkFlowPermissionDetail
    {
        public long UserId { get; set; }
        public string FullName { get; set; }
        public string CabinetName { get; set; }
        public string[] SharedUsers { get; set; }
    }
}