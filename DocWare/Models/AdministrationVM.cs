﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DocWare.Models
{
    public class DepartmentVM
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long DepartmentManagerId { get; set; }
        public string DepartmentManagerName { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public class SectionVM
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public long SectionManagerId { get; set; }
        public string SectionManagerName { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
    }

    public class PositionVM
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public long SectionId { get; set; }
        public string SectionName { get; set; }
        public bool IsActive { get; set; }
    }

    public class SubCategoryVM
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long CategoryId { get; set; }
        public string CategoryName { get; set; }
        public bool IsActive { get; set; }
    }

    public class CabinetVM
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}