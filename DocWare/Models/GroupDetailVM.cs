﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DocWare.Models
{
    public class GroupDetailVM
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string CabinetName { get; set; }
        public bool IsDepartment { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public string[] Users { get; set; }
        public string[] Departments { get; set; }
    }
}