﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Utility.Models;

namespace DocWare.Models
{
    public class WorkflowVM
    {
        public long WFId { get; set; }
        public string WFName { get; set; }
        public bool IsManual { get; set; }
        public string CreatedById { get; set; }
        public string CreatedByName { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool IsActive { get; set; }
    }

    public class WorkFlowVM
    {
        public WF_MasterVM WFList { get; set; }
        public WFUserList WFUserList { get; set; }
        public List<WFStepVM> WFStepList { get; set; }
        public WorkFlowVM()
        {
            WFList = new WF_MasterVM();
            WFUserList = new WFUserList();
            WFStepList = new List<WFStepVM>();
        }
    }

    public class WF_MasterVM
    {
        public long Id { get; set; }
        public long CabinetId { get; set; }
        public string WFName { get; set; }
        public string CabinetName { get; set; }
        public string WFType { get; set; }
        public string Notes { get; set; }
        public string Descritpion { get; set; }
        public long AssignFormTemplateId { get; set; }
        public string AssignFormTemplateName { get; set; }
        public long FormTemplateId { get; set; }
        public string[] WFInitaitors { get; set; }
        public string[] WFClosers { get; set; }
        public string[] WFProcessAdmins { get; set; }
        //public string[] WFGroupInitaitors { get; set; }
        //public string[] WFGroupClosers { get; set; }
        //public string[] WFGroupProcessAdmins { get; set; }
        public bool IsActive { get; set; }
        public bool IsManual { get; set; }
        public bool IsDelete { get; set; }
        public List<WFStepVM> WFStepList { get; set; }
    }

    public class WFUserList
    {
        public string[] WFInitaitors { get; set; }
        public string[] WFClosers { get; set; }
        public string[] WFProcessAdmins { get; set; }
        //public string[] WFGroupInitaitors { get; set; }
        //public string[] WFGroupClosers { get; set; }
        //public string[] WFGroupProcessAdmins { get; set; }
    }

    public partial class WFStepVM
    {
        public int WFStep { get; set; }
        public string WFStepName { get; set; }
        public long AssignedUserID { get; set; }
        public string[] WFActions { get; set; }
        public string WFStepNotes { get; set; }
    }

    public class WorkflowStepListVM
    {
        public long WFId { get; set; }
        public string WFName { get; set; }
        public List<WF_StepDetailVM> WFStepsList { get; set; }
    }

    public partial class WF_StepDetailVM
    {
        public long Id { get; set; }
        public long WFId { get; set; }
        public int WFStep { get; set; }
        public string WFStepName { get; set; }
        public string WFStepNotes { get; set; }
        public long AssignedUserID { get; set; }
        public string AssignedUserName { get; set; }
    }

    public class WFUserListByCabinet
    {
        public long UserId { get; set; }
        public string FullName { get; set; }
        public string RoleName { get; set; }
        public string EmailId { get; set; }
        public string ProfileImage { get; set; }
    }

    public class WFStep
    {
        public long StepNumber { get; set; }
    }

    public class UserWorkflowVM
    {
        public string CabinetName { get; set; }
        //Add By Kiran For check IsInitiator 
        public bool IsInitiator { get; set; }
        public List<UserWorkflowListVM> WorkFlowList { get; set; }
        public UserWorkflowVM()
        {
            WorkFlowList = new List<UserWorkflowListVM>();
        }
    }

    public class UserWorkflowListVM
    {
        public long WFId { get; set; }
        public string WFName { get; set; }
        public long TotalPendingWF { get; set; }
    }

    public class UserWorkflowAssignesVM
    {
        public string CabinetName { get; set; }
        public long WFId { get; set; }
        public string WFName { get; set; }
        public long WFInprocessCount { get; set; }
        public long WFInitiateCount { get; set; }
        public long WFHighPriorityCount { get; set; }
        public long WFMediumPriorityCount { get; set; }
        public long WFLowPriorityCount { get; set; }
        //Add By Kiran For check IsInitiator 
        public bool IsInitiator { get; set; }
        public long Priority { get; set; }
        public string WFInitiateName { get; set; }
    }

    public class UserWorkflowAssignesListVM
    {
        public long WFId { get; set; }
        public long WFInstanceId { get; set; }
        public string WFInitiateName { get; set; }
        public string Initiated { get; set; }
        public string InitiatorUser { get; set; }
        public string LastUpdate { get; set; }
        public string LastUpdatedBy { get; set; }
        public string LastAction { get; set; }
        public long Priority { get; set; }
        public bool IsOpen { get; set; }
        public long AssignFormTemplateId { get; set; }
        public int Type { get; set; }
        public int CurrentStep { get; set; }
    }

    public class UserWorkflowAssignesInfo
    {
        public long WFInstanceId { get; set; }
        public string WFInitiateName { get; set; }
        public string Initiated { get; set; }
        public string InitiatorUser { get; set; }
        public string LastUpdate { get; set; }
        public string LastUpdatedBy { get; set; }
        public string LastAction { get; set; }
        public long Priority { get; set; }
        public bool IsOpen { get; set; }
        public int Type { get; set; }
        public WorkflowDynamicFormResponseVM DynamicFormResponse { get; set; }        
        public UserWorkflowAssignesInfo()
        {
            DynamicFormResponse = new WorkflowDynamicFormResponseVM();            
        }
        
        
    }

    public class WorkflowDynamicFormResponseVM
    {
        public long ID { get; set; }
        public string FormTitle { get; set; }
        public string HtmlContent { get; set; }
        public string ResponseCode { get; set; }
        public long FormTemplateId { get; set; }
        public string AttachedType { get; set; }
        public long AttachedId { get; set; }
        public string CreatedBy { get; set; }
        public long SelectedFileId { get; set; }
        public List<WorkflowDynamicFormResponseDetailVM> DynamicFormResponseDetailList { get; set; }
        public WorkflowDynamicFormResponseVM()
        {
            DynamicFormResponseDetailList = new List<WorkflowDynamicFormResponseDetailVM>();
        }
    }

    public class WorkflowDynamicFormResponseDetailVM
    {
        public string FiledId { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
        public bool IsSelected { get; set; }
    }

    public class WorkflowTimeLineDetail
    {
        public string CabinetName { get; set; }
        public long WFId { get; set; }
        public string WFName { get; set; }
        public long WFInstanceId { get; set; }
        public string WFInitiateName { get; set; }
        public bool IsOpen { get; set; }
        //Add For Admin Process & Closer Flow || kiran Sawant || 03092020
        public bool IsAdmin { get; set; }
        public bool IsCloser { get; set; }
        public string InitiateBy { get; set; }
        public bool IsInitiatorUser { get; set; }
        public bool IsStart { get; set; }
        public string InitiateDate { get; set; }
        public string ToUser { get; set; }
        public long FolderId { get; set; }
        public long FavoriteId { get; set; }
        public long SharedId { get; set; }
        public long LastOpenFolderId { get; set; }
        public long LastOpenParentFavId { get; set; }
        public long LastOpenParentSharedId { get; set; }
        public string FolderName { get; set; }
        public int currentStep { get; set; }
        public bool IsAssignedToMe { get; set; }
        public int Type { get; set; }
        public List<WorkflowTimeLineInfo> WorkFlowTimeLine { get; set; }
        public List<WorkflowTimeSendTo> WorkflowTimeSendTo { get; set; }
        public UserWorkflowAssignesInfo WorkFlowAssignedInfo { get; set; }
        public WorkflowTimeLineDetail()
        {
            WorkFlowTimeLine = new List<WorkflowTimeLineInfo>();
            WorkflowTimeSendTo = new List<WorkflowTimeSendTo>();
            WorkFlowAssignedInfo = new UserWorkflowAssignesInfo();
        }

    }

    public class WorkflowTimeLineInfo
    {
        public long WFDetailId { get; set; }
        public long WFTransactionId { get; set; }
        public string WFStepName { get; set; }
        public string AssignedTo { get; set; }
        public string ActionTaken { get; set; }
        public string ActionTakenTime { get; set; }
        public string Notes { get; set; }
        public string WFActions { get; set; }
        public bool IsCurrentStep { get; set; }
        public bool IsWFStepClose { get; set; }
        public bool IsWFManual { get; set; }
        public bool IsSubsidiary { get; set; }
        public int WFStep { get; set; }
        public string SendTo { get; set; }
    }

    public class WorkflowTimeSendTo
    {
        public long StepId { get; set; }
        public string StepName { get; set; }
    }
    public class WorkFlowTimeLine
    {
        public long WFIntId { get; set; }
        public long WFId { get; set; }
        public bool IsOpen { get; set; }
        public int Type { get; set; }
    }

    public class SaveWorkFlowStep
    {
        public long WFIntId { get; set; }
        public long WFTransactionId { get; set; }
        public long WFId { get; set; }
        public long WFStep { get; set; }
        public Int32 WFActionId { get; set; }
        public string Notes { get; set; }
        public string WFStepName { get; set; }
        public string WFActions { get; set; }
        public long WFAssignedTo { get; set; }
    }

    public class UserWorkInitaiteVM
    {
        public long WFId { get; set; }
        public string WFName { get; set; }
        public long Priority { get; set; }
        public bool IsManual { get; set; }
        public string CabinetName { get; set; }
        public string WFNameForManual { get; set; }
        public string[] WFActionForManual { get; set; }        
        public long ToUser { get; set; }
        public long FolderId { get; set; }
        public long FavoriteId { get; set; }
        public long SharedId { get; set; }
        public long LastOpenFolderId { get; set; }
        public long LastOpenParentFavId { get; set; }
        public long LastOpenParentSharedId { get; set; }
        public string FolderName { get; set; }       

        //public FolderListVM FolderList { get; set; }
        public UserWorkflowAssignesInfo WorkFlowAssignedInfo { get; set; }
        
        public UserWorkInitaiteVM()
        {           
            //FolderList = new FolderListVM();
            WorkFlowAssignedInfo = new UserWorkflowAssignesInfo();
        }
    }
    public class ManualWorkInitaiteVM
    {
        public string WFNameForManual { get; set; }
        public string WFActionForManual { get; set; }
        public long ToUser { get; set; }
    }    

}