﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DocWare.Models
{
    public class NotificationVM
    {
        public long TotalCount { get; set; }
        public long TotalComment { get; set; }
        public long TotalShare { get; set; }
        public long TotalVersion { get; set; }
        public long TotalWorkflow { get; set; }
        public long TotalApprovalReq { get; set; }
        public long TotalGroups { get; set; }

        public List<NotificationListVM> NotificationLists { get; set; }

        public NotificationVM()
        {
            NotificationLists = new List<NotificationListVM>();
        }
    }

    public class NotificationListVM
    {
        public long NotificationId { get; set; }
        public long NotificationTypeId { get; set; }
        public long CabinetId { get; set; }
        public long UserId { get; set; }
        public string Comment { get; set; }
        public string MessageEn { get; set; }
        public bool IsRead { get; set; }
        public bool IsDelete { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string NcssClass { get; set; }
        public string NotificationType { get; set; }
        public string DisplayTime { get; set; }
        public long TotalNotiCount { get; set; }
    }

    public class SaveNotificationVM
    {
        public long UserId { get; set; }
        public long CabinetId { get; set; }
        public long FolderId { get; set; }
        public long DocumentId { get; set; }
        public string TypeFor { get; set; }
        public int NotificationType { get; set; }
        public string GroupName { get; set; }
        public long GroupId { get; set; }
        public string Comment { get; set; }
    }
    public class SaveNotificationWorkFlowVM
    {
        public long UserId { get; set; }
        public long CabinetId { get; set; }        
        public int NotificationType { get; set; }        
        public string Comment { get; set; }
        public string @SendUserId { get; set; }
        public long @InstanceId { get; set; }
    }
}