﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DocWare.Models
{
    public class RoleRightVM
    {
        public long RoleId { get; set; }

        public string RoleName { get; set; }
    }

    public class UserRightVM
    {
        public long UserId { get; set; }

        public string FullName { get; set; }

        public string RoleName { get; set; }
    }

    public class MenuVM
    {
        public long UserRoleID { get; set; }

        public long LinkID { get; set; }

        public long ModuleID { get; set; }

        public string ModuleName { get; set; }

        public string ArModuleName { get; set; }

        public string LinkName { get; set; }

        public string ArLinkName { get; set; }

        public string LinkType { get; set; }

        public string ArLinkType { get; set; }

        public long ViewIndex { get; set; }

        public string Controller { get; set; }

        public string Action { get; set; }

        public string CssClass { get; set; }

        public long UserID { get; set; }

        public bool IsDefault { get; set; }

        public bool IsSingle { get; set; }

        public bool IsAssigned { get; set; }

        public bool IsPage { get; set; }

        public long ParentId { get; set; }
    }

    public class UserPermissionVM
    {
        public string ActionName { get; set; }
        public string ControllerName { get; set; }
        public string Type { get; set; }
    }

    public class PermissionVM
    {
        public bool HasAddPermission { get; set; }
        public bool HasEditPermission { get; set; }
        public bool HasStatusPermission { get; set; }
        public bool HasActivePermission { get; set; }
        public bool HasDeactivePermission { get; set; }
        public bool HasDeletePermission { get; set; }
        public bool HasDocSharePermission { get; set; }
        public bool HasRestorePermisssion { get; set; }
        public bool HasConfigurationPermisssion { get; set; }
        public bool HasWorkFlowPermission { get; set; }
    }

    public class MenuCountVM
    {
        public int Favorite { get; set; }

        public int SharedWithMe { get; set; }

        public int Public { get; set; }

        public int RecycleBin { get; set; }
    }

    public class DocSharedVM
    {
        public long UserId { get; set; }

        public long GroupId { get; set; }

        public string Name { get; set; }
    }

    public class DocSharedUser
    {
        public long UserId { get; set; }

        public string Name { get; set; }
    }
    
 

    //public class UserActionPermissionVM
    //{
    //    public bool HasFolderAddPermission { get; set; }
    //    public bool HasFileAddPermission { get; set; }
    //    public bool HasFolderDeletePermission { get; set; }
    //    public bool HasFileDeletePermission { get; set; }

    //    public bool HasStatusPermission { get; set; }
    //    public bool HasActivePermission { get; set; }
    //    public bool HasDeactivePermission { get; set; }
    //    public bool HasDeletePermission { get; set; }
    //    public bool HasDocSharePermission { get; set; }
    //    public bool HasRestorePermisssion { get; set; }
    //    public bool HasConfigurationPermisssion { get; set; }
    //}
}