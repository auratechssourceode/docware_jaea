﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DocWare.Models
{
    public class FolderPermissionVM
    {
        public bool HasFolderPermission { get; set; }
    }

    public class FavoritePermissionVM
    {
        public bool HasFolderPermission { get; set; }
        public bool HasUploadPermission { get; set; }
        public bool HasDownloadPermission { get; set; }
        public bool HasSeeVersionPermission { get; set; }
        public bool HasSharePermission { get; set; }
        public bool HasReadCommentPermission { get; set; }
        public bool HasAddCommentPermission { get; set; }
    }

    public class FolderVM
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int ChildCount { get; set; }
    }

    public class FolderNameDetailVM
    {
        public long folderId { get; set; }
        public string folderName { get; set; }
        public long parentFolder { get; set; }
        public long favId { get; set; }
        public long prevfavId { get; set; }
        public long shareId { get; set; }
        public long prevshareId { get; set; }
    }

    public class FolderListVM
    {
        public long FolderId { get; set; }
        public long FavoriteId { get; set; }
        public long SharedId { get; set; }
        public long LastOpenFolderId { get; set; }
        public long LastOpenParentFavId { get; set; }
        public long LastOpenParentSharedId { get; set; }
        public string FolderName { get; set; }
        public string CabinetName { get; set; }
    }

    public class FolderFileVM
    {
        public string FolderType { get; set; }
        public long Id { get; set; }
        public long FavoriteId { get; set; }
        public long SharedId { get; set; }
        public long DeletedDataID { get; set; }
        public long ParentFolderId { get; set; }
        public long DocumentVersionId { get; set; }
        public long DocumentId { get; set; }
        public string Name { get; set; }
        public string FileName { get; set; }
        public string Version { get; set; }
        public int ChildCount { get; set; }
        public bool IsDocument { get; set; }
        public bool IsFavorite { get; set; }
        public bool IsLinked { get; set; }
        public string DocType { get; set; }
        public string DocSize { get; set; }
        public bool Shared { get; set; }
        public string DataPath { get; set; }
        public string DocumentPath { get; set; } // Added on 18/7/18
        public string ModifiedBy { get; set; }
        public string ModifiedOn { get; set; }
        public bool IsCheckOut { get; set; }// added on 20/7/18
        public long CheckInOutHistoryId { get; set; } // added on 23/7/18
        public string CreatedBy { get; set; } // added on 12/02/2021
        public string CreatedOn { get; set; } // added on 12/02/2021
    }

    public class LstFolderDetailVM
    {
        public string CabinetName { get; set; }
        public List<FolderFileVM> FolderDetailVMs { get; set; }
    }

    public class FolderSubFolderDetail
    {
        public long FolderId { get; set; }
        public string FolderName { get; set; }
        public List<FolderFileVM> FolderDetailVMs { get; set; }
    }

    public class DocumentDetailVM
    {
        public long DocumentId { get; set; }
        public long DocumentVersionId { get; set; }
        public long DocParentFolderId { get; set; }
        public long DocFormTemplateId { get; set; }
        public long DocAssignFormTemplateId { get; set; }
        public string DocAssignFormTemplateName { get; set; }
        public string DocName { get; set; }
        public string DocDescription { get; set; }
        public string DocNotes { get; set; }
        public string DocKeywords { get; set; }
        public string DocumentType { get; set; }
        public string DocumentSize { get; set; }
        public double DocumentVersion { get; set; }
        public string DocPhysicalLocation { get; set; }
        public int Language { get; set; }
        public long CategoryId { get; set; }
        public long SubCategoryId { get; set; }
        public int Confidentiality { get; set; }
        public string[] DocShareUserIds { get; set; }
        public bool DocSharePermission { get; set; }
        public bool DocPublic { get; set; }
        public long WFInstanceId { get; set; }
        public int currentStep { get; set; }
    }

    public class FolderDetailVM
    {
        public long Id { get; set; }
        public long ParentFolderId { get; set; }
        public string UDID { get; set; }
        public long CabinetId { get; set; }
        public string Name { get; set; }
        public string Notes { get; set; }
        public string Keywords { get; set; }
        public string PhysicalLocation { get; set; }
        public string Description { get; set; }
        public bool PublicFolder { get; set; }
        public long FolAssignFormTemplateId { get; set; }
        public string FolAssignFormTemplateName { get; set; }
        public long FolFormTemplateId { get; set; }
    }

    public class FileListVM
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public double DocumentVersion { get; set; }
        public string DocumentType { get; set; }
        public string DocumentDisplaySize { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedOn { get; set; }
    }

    public class FolderFileInfo
    {
        public long Id { get; set; }
        public long DocVersionId { get; set; }
        public string UDID { get; set; }
        public string Name { get; set; }
        public string Notes { get; set; }
        public string Keywords { get; set; }
        public string InfoType { get; set; }
        public string Owner { get; set; }
        public string ModifyBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifyDate { get; set; }
        public string TotalFolders { get; set; }
        public string TotalFiles { get; set; }
        public string FileSize { get; set; }
        public string Description { get; set; }
        public string PhysicalLocation { get; set; }
        public string OriginalLocation { get; set; }
        public long AssignFormTemplateId { get; set; } // added on 26/7/18
        public string AssignFormName { get; set; }// added on 26/7/18        
        public string Type { get; set; }
        public string Version { get; set; }
        public string Confidentiality { get; set; }
        public string Language { get; set; }
        public string Category { get; set; }
        public string SubCategory { get; set; }
        public long SelectedCount { get; set; }
        public long TotalCount { get; set; }
        public string SharedWith { get; set; }
        public string DocumentPath { get; set; } // Added on 13/09/2018
    }

    public class FolderFileSelected
    {
        public long FolderId { get; set; }
        public long DocumentId { get; set; }
        public string FolderIds { get; set; }
        public string FileIds { get; set; }
        public long SelectedCount { get; set; }
        public string Comment { get; set; }
    }

    public class FolderFileComments
    {
        public long Id { get; set; }
        public long DocVersionId { get; set; }
        public string ImageName { get; set; }
        public string Name { get; set; }
        public string DocumentType { get; set; }
        public string FullName { get; set; }
        public string CommentDateTime { get; set; }
        public string Comment { get; set; }
    }

    public class DocShare
    {
        public long FolderId { get; set; }
        public long FileId { get; set; }
        public string Name { get; set; }
        public string[] RemoveUserIdsList { get; set; }
        public string[] RemoveGroupIdsList { get; set; }
        public List<DocShareList> DocShareLst { get; set; }
        public DocShareList DocSharePermission { get; set; }
        public DocShare()
        {
            DocShareLst = new List<DocShareList>();
            DocSharePermission = new DocShareList();
        }
    }

    public class DocShareList
    {
        public long UserId { get; set; }
        public long GroupId { get; set; }
        public string Name { get; set; }
        public bool IsDownload { get; set; }
        public bool IsUpload { get; set; }
        public bool IsSeeVersion { get; set; }
        public bool IsReadComment { get; set; }
        public bool IsAddComment { get; set; }
        public bool IsShare { get; set; }
        public bool IsNotCreated { get; set; }
    }

    public class SharedEmailLink
    {
        public string[] FileIds { get; set; }
        public string[] FolderIds { get; set; }
        public string[] UserIds { get; set; }
        public string ShareType { get; set; }
    }

    public class DocumentVersionList
    {
        public long DocId { get; set; }
        public long DocVersionId { get; set; }
        public double Version { get; set; }
        public string DocSize { get; set; }
        public string CreatedBy { get; set; }
        public string Notes { get; set; }
        public string DocType { get; set; }
        public string CreatedOn { get; set; }
        public string UserName { get; set; }
        public string CurrentUser { get; set; }
        public DateTime Created { get; set; }
        public bool IsCurrent { get; set; }
        public bool IsCreator { get; set; }
    }

    public class DocumentVersionDownload
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string DocVersionId { get; set; }
    }


    public class LinkDocumentVM
    {
        public long UserId { get; set; }
        public long DocumentId { get; set; }
       // public string DocumentName { get; set; }

        //public List<LinkDocumentList> LinkDocumentList { get; set; }
        public List<LinkChildDocumentList> LinkChildDocumentList { get; set; }
        public List<LinkParentDocumentList> LinkParentDocumentList { get; set; }

        public LinkDocumentVM()
        {
           // LinkDocumentList = new List<LinkDocumentList>();
            LinkChildDocumentList = new List<LinkChildDocumentList>();
            LinkParentDocumentList = new List<LinkParentDocumentList>();
        }
    }
    public class LinkDocumentList
    {
        public long DocumentId { get; set; }
        public string Name { get; set; }
        public string DocumentType { get; set; }
        public string FullPath { get; set; }
        public string DisplayText { get; set; }
        public string HTMLText { get; set; }
    }

    public class LinkChildDocumentList
    {

        public long LinkId { get; set; }
        public long ParentId { get; set; }
        public long ChildId { get; set; }
        public long DocumentId { get; set; }
        public string Name { get; set; }
        public string DocumentVersionOwner { get; set; }
        public string DocumentType { get; set; }
        public string OriginalLocation { get; set; }

    }

    public class LinkParentDocumentList
    {
        public long LinkId { get; set; }
        public long DocumentId { get; set; }
        public string Name { get; set; }
        public string DocumentVersionOwner { get; set; }
        public string DocumentType { get; set; }
        public string OriginalLocation { get; set; }
    }
}