﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DocWare.Models
{
    /// <summary>
    /// ResponseDC | Add | Jigar Patel | 31-01-2020
    /// </summary>
    public class ResponseDC
    {
        public int ResponseCode { get; set; }

        public string ResponseMsg { get; set; }
    }
    /// <summary>
    /// Response With ID, Message & LIST DATA 
    /// </summary>
    /// <typeparam name="T"></typeparam>    
    public class ResponseDataDC<T>
    {
        public int ResponseCode { get; set; }

        public string ResponseMsg { get; set; }

        public ResponseListDC<T> ResponseData { get; set; }
    }
    /// <summary>
    /// Response With List Data
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ResponseListDC<T>
    {
        private List<T> _Data = new List<T>();

        public List<T> Data
        {
            get
            {
                return _Data;
            }
            set
            {
                _Data = value;
            }
        }
    }

    /// <summary>
    /// Response With ID, Message & LIST DATA 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ResponseDataDC1<T>
    {
        public int ResponseCode { get; set; }

        public string ResponseMsg { get; set; }

        public T ResponseData { get; set; }
    }

    public class ResponseDataDC2<T>
    {
        public int ResponseCode { get; set; }

        public string ResponseMsg { get; set; }

        private List<T> _Data = new List<T>();

        public List<T> ResponseData
        {
            get
            {
                return _Data;
            }
            set
            {
                _Data = value;
            }
        }
    }

    /// <summary>
    /// ResponseDC1 | Add | Kiran Sawant | 08-04-2020
    /// </summary>
    public class ResponseDC1
    {
        public int ResponseCode { get; set; }

        public string ResponseMsg { get; set; }

        public Dictionary<string, string> ResponseData { get; set; }

    }

    public class ResponseDC2
    {
        public ResponseDC2()
        {
            ResponseData = new responsedata();
        }
        public int ResponseCode { get; set; }

        public string ResponseMsg { get; set; }

        //public Dictionary<string, string> ResponseData { get; set; }

        //public Dictionary<string, List<LanguageValueVM>> ResponseData1 { get; set; }

        public responsedata ResponseData { get; set; }




    }

}