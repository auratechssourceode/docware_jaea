﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DocWare.Models
{
    public class LoginVM
    {
        [StringLength(100)]
        [Display(Name = "Username")]
        public string UserName { get; set; }

        [StringLength(50)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [StringLength(500)]
        [Display(Name = "ReturnURL")]
        public string ReturnURL { get; set; }

        public bool isRemember { get; set; }

        public long ApplicationId { get; set; }

        public string EmailId { get; set; }

        public long CabinetId { get; set; }
    }

    public class ResetPasswordConfirmVM
    {
        public long ApplicationId { get; set; }
        public long UserId { get; set; }
    }

    public class LockScreen
    {
        public string LayoutUserName { get; set; }
        public string FullName { get; set; }
        public long LayoutApplicationId { get; set; }
        public string ProfilePic { get; set; }
        public long CabinetId { get; set; }
        public string Password { get; set; }
        public string ReturnURL { get; set; }
    }
}