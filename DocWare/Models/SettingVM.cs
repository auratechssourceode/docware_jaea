﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Utility.Models;

namespace DocWare.Models
{
    public class SettingsVM
    {
        public int SettingsId { get; set; }
        public string SettingName { get; set; }
        public string Description { get; set; }
    }
    public class SettingsDC
    {
        public int SettingsId { get; set; }
        public string SettingName { get; set; }
        public List<SettingsDetail> SettingsDetail { get; set; }
        public SettingsDC()
        {
            SettingsDetail = new List<SettingsDetail>();
        }
    }
    public class SettingsDetail
    {
        public long DetailId { get; set; }
        public string FieldName { get; set; }
        public string FieldValue { get; set; }
    }
}