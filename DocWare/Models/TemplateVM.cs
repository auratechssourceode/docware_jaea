﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Utility.Models;

namespace DocWare.Models
{
    public class EmailTemplateVM
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public bool IsActive { get; set; }
        public bool IsEdit { get; set; }
        public List<EmailTemplateTagVM> ListTag { get; set; }
        public EmailTemplateVM()
        {
            ListTag = new List<EmailTemplateTagVM>();
        }
    }

    public class EmailTemplateTagVM
    {
        public long TagID { get; set; }
        public int DisplayIndex { get; set; }
        public string EmailTemplateTag { get; set; }
        public string Description { get; set; }
        public string TypeName { get; set; }
    }

    public class DynamicFormVM
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public string[] CabinetList { get; set; }
        public string DynamicFormContent { get; set; }
    }

    public class FormTemplateVM
    {
        public long Id { get; set; }
        public string FormTitle { get; set; }
        public string FormCode { get; set; }
        public string Description { get; set; }
        public string HtmlContent { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public bool IsUsed { get; set; }
        public string[] CabinetList { get; set; }
        //public List<FormControlProperty> FormControlPropertyLst { get; set; }
    }
    public class DynamicControlVM
    {
        public string ID { get; set; }
        public string Type { get; set; }
        public string ControlID { get; set; }

    }


    public class FormFieldVM
    {
        public string FieldID { get; set; }
        public string FieldType { get; set; }
    }

    public class FieldValueVM
    {
        public string FieldID { get; set; }
        public string FieldType { get; set; }
        public string FieldValue { get; set; }
    }


    public class DynamicFormResponseVM
    {
        public long ID { get; set; }
        public string FormTitle { get; set; }
        public string HtmlContent { get; set; }
        public string ResponseCode { get; set; }
        public long FormTemplateId { get; set; }
        public string AttachedType { get; set; }
        public long AttachedId { get; set; }
        public string CreatedBy { get; set; }     
        public long SelectedFileId { get; set; }
        public List<DynamicFormResponseDetailVM> DynamicFormResponseDetailList { get; set; }
        public DynamicFormResponseVM() {
            DynamicFormResponseDetailList = new List<DynamicFormResponseDetailVM>();
        }
    }

    public class DynamicFormResponseDetailVM
    {
        public string FiledId { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
        public bool IsSelected { get; set; }

    }


    public class ResponseListVM
    {
        public string ResponseCode { get; set; }
        public long FormTemplateId { get; set; }
        public string FormTitle { get; set; }
        public string Respondent { get; set; }
        public string ResponseTime { get; set; }
    }


    public class ViewDynamicFormVM
    {
        public long AssignFormTemplateId { get; set; }
        public long SelectedId { get; set; }
    }

}