﻿using DocWare.App_Start;
using GleamTech.DocumentUltimate;
using GleamTech.DocumentUltimate.AspNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace DocWare
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new MultiLanguageViewEngine());


            //Set this property only if you have a valid license key, otherwise do not 
            //set it so FileUltimate runs in trial mode.  
            // FileUltimateConfiguration.Current.LicenseKey = "QQJDJLJP34...";

           // The default CacheLocation value is "~/App_Data/DocumentCache"
           // Both virtual and physical paths are allowed(or a Location instance for one of the supported
           //  file systems like Amazon S3 and Azure Blob).
            DocumentUltimateWebConfiguration.Current.CacheLocation = "~/App_Data/DocumentCache";
        }


    }
}
