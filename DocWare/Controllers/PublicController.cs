﻿using DocWare.Helpers;
using DocWare.Models;
using Ionic.Zip;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utility.Helpers;
using Utility.Models;

namespace DocWare.Controllers
{
    [CheckAuthorization]
    public class PublicController : Controller
    {
        // GET: Public
        DBEntities db = new DBEntities();
        DAL objDAL = new DAL();
        List<string> parentLst = new List<string>();

        #region --> Public List | Add | Sweta Patel | 17052018
        [HttpGet, Route("PublicFiles")]
        public ActionResult PublicFiles()
        {
            #region --> Page Permission on Action and controller name

            var permissionlst = Session[SessionClass.LinkPermissionList].ToString();
            List<UserPermissionVM> userPermissionLst = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserPermissionVM>>(permissionlst);
            FolderPermissionVM permissionVM = new FolderPermissionVM
            {
                HasFolderPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "AddFolder" && x.ControllerName.Trim() == "FolderAndFile").Count() > 0
            };

            Session["PermissionListSession"] = permissionVM;
            #endregion
            List<FileListVM> fileListVMs = new List<FileListVM>();
            try
            {

                FolderListVM folderListVM = new FolderListVM
                {
                    FolderId = 0,
                    LastOpenFolderId = 0,
                    FolderName = "",
                    CabinetName = Session[SessionClass.CabinetName].ToString()
                };
                return View(folderListVM);
            }
            catch (Exception e)
            {
                throw e;
            }
            //return View(fileListVMs);
        }

        public JsonResult checkIsNotEmpty(long id = 0)
        {
            DBEntities db = new DBEntities();
            if (id > 0)
            {
                var userId = CommonFunctions.GetUserId().ToString();
                var child = db.FolderDetails.Where(x => x.ParentFolderId == id && x.IsPublic && x.IsActive && !x.IsDelete).Count();
                if (child > 0)
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    child = (from doc in db.DocumentDetails
                             join docv in db.DocumentVersionDetails on doc.DocumentId equals docv.DocumentId
                             where (doc.IsActive && !doc.IsDelete && doc.FolderId == id
                             && docv.IsPublic)
                             select new
                             { doc.DocumentId }).Distinct().Count();

                    if (child > 0)
                    {
                        return Json(true, JsonRequestBehavior.AllowGet);
                    }
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }

        public PartialViewResult _PubFolderList(long folderId, string SearchKeyword = null)
        {
            List<FolderFileVM> folderDetails = new List<FolderFileVM>();
            try
            {
                long CabinetId = CommonFunctions.GetCabinetId();
                long UserId = CommonFunctions.GetUserId();
                SortedList sl = new SortedList();
                DataTable dt = new DataTable();

                sl.Add("@cabinetId", CabinetId);
                sl.Add("@folderId", folderId);
                sl.Add("@userId", UserId);

                if (!string.IsNullOrWhiteSpace(SearchKeyword))
                    sl.Add("@searchword", SearchKeyword.Trim());

                dt = objDAL.GetDataTable("uspGetPublicFolderDetails", sl);
                folderDetails = objDAL.ConvertToList<FolderFileVM>(dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            return PartialView("_PubFolderList", folderDetails);
        }

        public JsonResult GetFolderName(long folderId)
        {
            FolderNameDetailVM folderDetail = new FolderNameDetailVM();
            if (folderId > 0)
            {
                var detail = db.FolderDetails.Where(j => j.Id == folderId).ToList();

                folderDetail = detail.Select(x => new FolderNameDetailVM
                {
                    folderId = db.FolderDetails.FirstOrDefault(j => j.Id == x.Id && j.IsPublic) != null ? x.Id : 0,
                    folderName = x.Name,
                    parentFolder = db.FolderDetails.FirstOrDefault(j => j.Id == x.ParentFolderId && j.IsPublic) != null ? x.ParentFolderId : 0,
                }).FirstOrDefault();

                long hasPubCount = db.FolderDetails.Where(j => j.Id == folderDetail.folderId && j.IsPublic).ToList().Count();
                if (hasPubCount == 0)
                {
                    folderDetail.folderId = 0;
                    folderDetail.folderName = "Public";
                    folderDetail.parentFolder = 0;
                }

                return Json(new { success = true, folderData = folderDetail }, JsonRequestBehavior.AllowGet);

            }
            folderDetail.folderId = 0;
            folderDetail.folderName = "Public";
            folderDetail.parentFolder = 0;
            return Json(new { success = true, folderData = folderDetail }, JsonRequestBehavior.AllowGet);
            //return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }
        #endregion
        
        #region --> Folder/File Info and Comment | Add | Sweta Patel | 02052018
        [HttpGet]
        public PartialViewResult _FolderFileInfo(FolderFileSelected detail)
        {
            FolderFileInfo info = new FolderFileInfo();
            DAL objDAL = new DAL();
            SortedList sl = new SortedList();
            var userIds = CommonFunctions.GetUserId();
            var cabinetId = CommonFunctions.GetCabinetId();
            sl.Add("@UserId", userIds);
            sl.Add("@FolderId", detail.FolderId);
            sl.Add("@DocumentId", detail.DocumentId);
            sl.Add("@FolderIds", detail.FolderIds);
            sl.Add("@FileIds", detail.FileIds);
            sl.Add("@CabinetId", cabinetId);
            info = objDAL.ConvertToList<FolderFileInfo>(objDAL.GetDataTable("uspGetPublicFolderFileInfo", sl)).FirstOrDefault();
            info.SelectedCount = detail.SelectedCount;
            return PartialView(info != null ? info : new FolderFileInfo());
        }
        #endregion

        #region --> Download folder and file | Copy From Dhrumil | Sweta Patel | 07-06-2018
        [AuditLog(EnumList.EnumLogType.UpdateData, "DownloadFolderFiles")]
        public JsonResult DownloadFolderFiles(string[] folderIds, string[] fileIds)
        {
            try
            {
                if (folderIds != null || fileIds != null)
                {
                    long uid = CommonFunctions.GetUserId();
                    long cabinetId = CommonFunctions.GetCabinetId();
                    var uriPath = ConfigurationManager.AppSettings["PathForDocUpload"];
                    string folderPath = Server.MapPath(uriPath);
                    var folderDetails = db.FolderDetails.ToList();
                    var docDetails = db.DocumentVersionDetails.ToList();
                    var selectedNames = new List<string>();
                    var folIds = new List<string>();
                    var docIds = fileIds != null ? fileIds.ToList() : new List<string>();
                    var fileNames = new List<string>();
                    if (folderIds != null)
                    {
                        foreach (var item in folderIds)
                        {
                            long id = Convert.ToInt64(item);
                            folIds.Add(item);
                            if (CommonFunctions.HasChild(id) > 0)
                            {
                                parentLst = new List<string>();
                                folIds.AddRange(GetChildFolderIds(id, uid));
                            }
                        }

                        if (folIds != null && folIds.Count > 0)
                        {
                            var filIds = db.DocumentDetails.Where(x => folIds.Contains(x.FolderId.ToString()) && x.IsActive && !x.IsDelete).Select(x => x.DocumentId).ToList();
                            var finalIds = db.DocumentVersionDetails.Where(x => filIds.Contains(x.DocumentId) && x.IsPublic && x.IsActive && !x.IsDelete).Select(x => x.DocumentId.ToString()).ToList();
                            foreach (var id in finalIds)
                            {
                                var doc = docDetails.Where(x => x.DocumentId.ToString() == id && x.IsActive && !x.IsDelete && x.IsCurrent).OrderByDescending(x => x.Id).FirstOrDefault();
                                if (doc != null)
                                {
                                    docIds.Add(doc.Id.ToString());
                                }
                            }
                        }
                    }
                    List<string> CreatorsPath = new List<string>();
                    if (docIds != null && docIds.Count > 0)
                    {
                        CreatorsPath = docDetails.Where(x => docIds.Contains(x.Id.ToString()) && x.IsActive && !x.IsDelete).Select(x => folderPath + x.CreatedBy + "/" + cabinetId.ToString() + "/").Distinct().ToList();
                        if (docIds.Count == 1)
                        {
                            var detail = docDetails.FirstOrDefault(x => x.Id.ToString() == docIds.FirstOrDefault());
                            var docName = db.DocumentDetails.Find(detail.DocumentId).Name + "." + detail.DocumentType;
                            selectedNames.AddRange(docDetails.Where(x => docIds.Contains(x.Id.ToString()) && x.IsActive && !x.IsDelete).Select(x => folderPath + x.CreatedBy + "/" + cabinetId.ToString() + "/" + x.DocumentName + "." + x.DocumentType).Distinct().ToList());
                            var path = selectedNames[0].Substring(0, selectedNames[0].LastIndexOf('/')) + "/" + docName;
                            System.IO.File.Copy(selectedNames[0], path, true);
                            selectedNames = new List<string>();
                            selectedNames.Add(path);
                        }
                        else
                        {
                            selectedNames.AddRange(docDetails.Where(x => docIds.Contains(x.Id.ToString()) && x.IsActive && !x.IsDelete).Select(x => folderPath + x.CreatedBy + "/" + cabinetId.ToString() + "/" + x.DocumentName + "." + x.DocumentType).Distinct().ToList());
                        }
                    }

                    foreach (var path in CreatorsPath)
                    {
                        if (Directory.Exists(path))
                        {
                            string[] fileEntries = Directory.GetFiles(path);
                            foreach (string fileName in fileEntries)
                            {
                                if (selectedNames.Contains(fileName))
                                {
                                    fileNames.Add(fileName);
                                }
                            }
                        }
                    }
                    if (fileNames != null && fileNames.Count > 0)
                    {
                        TempData["FileNameList"] = fileNames.ToList();
                        return Json(new { result = "success", Message = "Download successfully!" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { result = "error", Message = "Sorry! Please try again." }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { result = "error", Message = "Sorry! Please try again." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { result = "error", Message = "Sorry! Please try again." }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DownloadSelected(string folid)
        {
            List<string> fileNameLst = new List<string>();
            if (TempData["FileNameList"] != null)
            {
                fileNameLst = (List<string>)TempData["FileNameList"];
                TempData["FileNameList"] = null;
            }
            long uid = CommonFunctions.GetUserId();
            long cabinetId = CommonFunctions.GetCabinetId();
            if (fileNameLst != null && fileNameLst.Count > 0)
            {
                try
                {
                    using (ZipFile zip = new ZipFile())
                    {
                        UtilityCommonFunctions.RandomStringGenerator random = new UtilityCommonFunctions.RandomStringGenerator();
                        string serialNumber = random.GetRandomString(4, UtilityCommonFunctions.RandomStringGenerator.ALPHANUMERIC_CAPS.ToCharArray());
                        string ZipFileName = string.Empty;
                        var uriPath = ConfigurationManager.AppSettings["PathForDocUpload"] + uid.ToString() + "/" + cabinetId.ToString() + "/";
                        string folderPath = Server.MapPath(uriPath);
                        var name = fileNameLst[0].Substring(fileNameLst[0].LastIndexOf('\\') + 1, fileNameLst[0].Length - 1 - fileNameLst[0].LastIndexOf('\\'));
                        ZipFileName = fileNameLst.Count() > 1 ?
                            String.Format("Docware-{0}-MultiItems-{1}-{2}", Session[SessionClass.CabinetName], DateTime.Now.ToString("yyyyMMMdd-HHmm"), serialNumber) :
                            String.Format("Docware-{0}-{1}-{2}-{3}", Session[SessionClass.CabinetName], name, DateTime.Now.ToString("yyyyMMMdd-HHmm"), serialNumber);
                        ZipFileName = ZipFileName.Replace(" ", "_");
                        zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                        zip.AddDirectoryByName("Docware");
                        foreach (string fileName in fileNameLst)
                        {
                            zip.AddFile(fileName, "Docware");
                        }

                        if (zip.Count > 1)
                        {
                            Response.Clear();
                            Response.BufferOutput = false;
                            string zipName = ZipFileName + ".zip";
                            Response.ContentType = "application/zip";
                            Response.AddHeader("content-disposition", "attachment; filename=" + zipName);
                            zip.Save(Response.OutputStream);
                            Response.End();
                            if (fileNameLst.Count() == 1)
                            {
                                System.IO.File.Delete(fileNameLst[0]);
                            }
                        }
                        else
                        {
                            TempData["ErrorMSG"] = "Documents not available.";
                        }
                    }
                }
                catch (Exception ex)
                {
                    TempData["ErrorMSG"] = "Some error occured. Please try again.";
                }
            }
            return RedirectToAction("FolderList", new { id = folid });
        }
        
        public List<string> GetChildFolderIds(long Id, long userId)
        {
            try
            {
                var subFolderIds = db.FolderDetails.Where(j => j.ParentFolderId == Id && j.IsActive && !j.IsDelete ).ToList();

                if (subFolderIds.Count > 0)
                {
                    foreach (var item in subFolderIds)
                    {
                        parentLst.Add(item.Id.ToString());
                        if (CommonFunctions.HasChild(item.Id) > 0)
                        {
                            GetChildFolderIds(item.Id, userId);
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return parentLst;
        }
        #endregion

        #region --> SearchSection | Krupali |07082018
        public PartialViewResult _PubSearchDocument(long folderId, String FName = null, string catid = null, string Subcatid = null, string Lang = null, string Confidentiality = null, String Keyword = null, String Filetype = null)
        {
            List<FolderFileVM> folderDetails = new List<FolderFileVM>();
            try
            {
                //if (CommonFunctions.IsCreatedBy(folderId, false))
                //{
                long CabinetId = CommonFunctions.GetCabinetId();
                long UserId = CommonFunctions.GetUserId();
                SortedList sl = new SortedList();
                DataTable dt = new DataTable();

                sl.Add("@cabinetId", CabinetId);
                sl.Add("@folderId", folderId);
                sl.Add("@userId", UserId);
                sl.Add("@FileName", FName);
                sl.Add("@Category", catid);
                sl.Add("@SubCategory", Subcatid);
                if (Lang != "")
                {
                    sl.Add("@Lang", Convert.ToInt32(Lang));
                }
                if (Confidentiality != "")
                {
                    sl.Add("@Confidentiality", Convert.ToInt32(Confidentiality));
                }
                if (Keyword != "")
                {
                    sl.Add("@Keyword", Keyword);
                }
                if (Filetype != "")
                {
                    sl.Add("@Filetype", Filetype);
                }


                dt = objDAL.GetDataTable("uspGetPublicFilesByCabinetSearch", sl);
                folderDetails = objDAL.ConvertToList<FolderFileVM>(dt);
                // }
            }
            catch (Exception e)
            {
                throw;
            }
            return PartialView("_PubSearchDocument", folderDetails);
        }

        #endregion
    }
}