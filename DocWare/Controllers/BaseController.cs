﻿using DocWare.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utility.Models;

namespace DocWare.Controllers
{
    public class BaseController : Controller
    {
        private static string _cookieLangName = "LangForMultiLanguage";

        #region--> Generic OnException | Sweta Patel| 2018-03-06 | Copy from VGL       
        /// <summary>
        /// Generic Exception method. 
        /// Inserts exception entry in ExceptionLog table and shows Error View
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnException(ExceptionContext filterContext)
        {
            Exception exception = filterContext.Exception;
            filterContext.ExceptionHandled = true;

            //Logging the Exception
            var actionName = filterContext.RouteData.Values["action"].ToString();
            var controllerName = filterContext.RouteData.Values["controller"].ToString();

            var projectName = filterContext.Exception.Source;
            var ex = filterContext.Exception.Message;

            string inEx = string.Empty;
            if (filterContext.Exception.InnerException != null)
                inEx = filterContext.Exception.InnerException.Message;

            var objExcpetion = new ExceptionLog();
            objExcpetion.Controller = filterContext.RouteData.Values["controller"].ToString();
            objExcpetion.Action = filterContext.RouteData.Values["action"].ToString();
            objExcpetion.Exception = filterContext.Exception.Message;
            objExcpetion.DeveloperNote = inEx;
            objExcpetion.UserId = Convert.ToInt64(Session["UserID"]);
            objExcpetion.ApplicationId = Convert.ToByte(Session["ApplicationID"]);
            Utility.Helpers.UtilityCommonFunctions.SaveExceptionLog(objExcpetion);

         

            var Result = this.View("Error", new HandleErrorInfo(exception,
                filterContext.RouteData.Values["controller"].ToString(),
                filterContext.RouteData.Values["action"].ToString()));
            filterContext.Result = Result;

        }

        #endregion

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string cultureOnCookie = GetCultureOnCookie(filterContext.HttpContext.Request);
            string cultureOnURL = filterContext.RouteData.Values.ContainsKey("lang") ? filterContext.RouteData.Values["lang"].ToString() : GlobalHelper.DefaultCulture;
            string culture = (cultureOnCookie == string.Empty) ? cultureOnURL : cultureOnCookie;
            //if (cultureOnURL != culture)
            //{
            //    filterContext.HttpContext.Response.RedirectToRoute("LocalizedDefault", new { lang = culture, action = filterContext.RouteData.Values["action"] });
            //    return;
            //}
            SetCurrentCultureOnThread(culture);
            if (culture != MultiLanguageViewEngine.CurrentCulture)
            {
                (ViewEngines.Engines[0] as MultiLanguageViewEngine).SetCurrentCulture(culture);
            }
            base.OnActionExecuting(filterContext);
        }

        private static void SetCurrentCultureOnThread(string lang)
        {
            if (string.IsNullOrEmpty(lang))
                lang = GlobalHelper.DefaultCulture;
            var cultureInfo = new System.Globalization.CultureInfo(lang);
            System.Threading.Thread.CurrentThread.CurrentUICulture = cultureInfo;
            System.Threading.Thread.CurrentThread.CurrentCulture = cultureInfo;
        }

        public static String GetCultureOnCookie(HttpRequestBase request)
        {
            var cookie = request.Cookies[_cookieLangName];
            string culture = string.Empty;
            if (cookie != null)
            {
                culture = cookie.Value;
            }
            return culture;
        }
    }
}