﻿using DocWare.Helpers;
using DocWare.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utility.Helpers;
using Utility.Models;

namespace DocWare.Controllers
{
    [CheckAuthorization]
    public class RecycleBinController : Controller
    {
        #region --> Field
        DBEntities db = new DBEntities();
        DAL objDAL = new DAL();
        #endregion
        // GET: RecycleBin
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet, Route("RecycleBinFiles")]
        public ActionResult RecycleBinFiles()
        {
            #region --> Page Permission on Action and controller name

            var permissionlst = Session[SessionClass.LinkPermissionList].ToString();
            List<UserPermissionVM> userPermissionLst = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserPermissionVM>>(permissionlst);

            FolderPermissionVM permissionVM = new FolderPermissionVM
            {
                HasFolderPermission = false,// userPermissionLst.Where(x => x.ActionName.Trim() == "AddFolder" && x.ControllerName.Trim() == "FolderAndFile").Count() > 0
            };

            Session["PermissionListSession"] = permissionVM;
            #endregion
            List<FileListVM> fileListVMs = new List<FileListVM>();
            try
            {
                FolderListVM folderListVM = new FolderListVM
                {
                    FolderId = 0,
                    FavoriteId = 0,
                    LastOpenFolderId = 0,
                    FolderName = "Recycle Bin",
                    CabinetName = Session[SessionClass.CabinetName].ToString()
                };
                return View(folderListVM);
            }
            catch (Exception e)
            {
                throw e;
            }
            //return View(fileListVMs);
        }

        public PartialViewResult _RecycleBinList(long folderId, string SearchKeyword = null)
        {
            List<FolderFileVM> folderDetails = new List<FolderFileVM>();
            try
            {
                long CabinetId = CommonFunctions.GetCabinetId();
                long UserId = CommonFunctions.GetUserId();
                //var favFolder = new List<Favorite>();
                //var favDoc = new List<Favorite>();
                SortedList sl = new SortedList();
                DataTable dt = new DataTable();

                sl.Add("@cabinetId", CabinetId);
                //sl.Add("@folderId", folderId);
                sl.Add("@userId", UserId);

                if (!string.IsNullOrWhiteSpace(SearchKeyword))
                    sl.Add("@searchword", SearchKeyword.Trim());

                dt = objDAL.GetDataTable("uspGetRecycleBinFilesByCabinet", sl);
                folderDetails = objDAL.ConvertToList<FolderFileVM>(dt);
            }
            catch (Exception e)
            {
                throw;
            }
            return PartialView("_RecycleBinList", folderDetails);
        }

        [HttpPost]
        public JsonResult GetInfoDetails(long id)
        {
            DBEntities db = new DBEntities();
            if (id > 0)
            {
                var userId = CommonFunctions.GetUserId().ToString();
                DeletedData dd = db.DeletedDatas.Find(id);
                if (dd != null)
                {
                    string name = string.Empty;
                    string type = string.Empty;
                    if (dd.FolderId > 0)
                    {
                        type = "Deleted Folder";
                        name = db.FolderDetails.Where(x => x.Id == dd.FolderId).Select(x => x.Name).FirstOrDefault();
                    }
                    else if (dd.DocumentId > 0)
                    {
                        type = "Deleted File";
                        name = db.DocumentDetails.Where(x => x.DocumentId == dd.DocumentId).Select(x => x.Name).FirstOrDefault();
                    }
                    return Json(new { success = true, type = type, name = name }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { success = false, msg = "Info not found." }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, msg = "Info not found." }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult checkIsNotEmpty(long id = 0)
        {
            DBEntities db = new DBEntities();
            if (id > 0)
            {
                var userId = CommonFunctions.GetUserId().ToString();
                bool isData = db.DeletedDatas.Any(x => x.IsRecycleBinDeleted == false && x.DeletedBy == userId);
                return Json(isData, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult RemoveFolderFiles(long[] deletedDataIDs)
        {
            try
            {
                long userId = CommonFunctions.GetUserId();
                DBEntities db = new DBEntities();
                if (deletedDataIDs != null && deletedDataIDs.Length > 0)
                {
                    foreach (var deletedDataID in deletedDataIDs)
                    {
                        //var userId = CommonFunctions.GetUserId().ToString();
                        DeletedData dd = db.DeletedDatas.Find(deletedDataID);
                        if (dd != null)
                        {
                            dd.IsRecycleBinDeleted = true;
                            db.Entry(dd).State = EntityState.Modified;
                            db.SaveChanges();
                            if (dd.FolderId > 0)
                            {
                                ChangeChildDeleteStatus(dd.FolderId, userId);
                            }
                        }
                    }
                    return Json(new { success = true, msg = "Item deleted successfully" }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { success = false, msg = "No item selected, Select atleast one item!" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, msg = "Something went wrong, please try again!" }, JsonRequestBehavior.AllowGet);
            }
        }

        public void ChangeChildDeleteStatus(long Id, long userId)
        {
            try
            {
                string uID = userId.ToString();
                var subDocList = db.DocumentDetails.Where(j => j.FolderId == Id).ToList();
                if (subDocList.Count > 0)
                {
                    foreach (var item in subDocList)
                    {
                        DeletedData data = db.DeletedDatas.FirstOrDefault(x => x.DeletedBy == uID && !x.IsRecycleBinDeleted && x.DocumentId == item.DocumentId);
                        if (data != null)
                        {
                            data.IsRecycleBinDeleted = true;
                            db.Entry(data).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                }

                var subFolderList = db.FolderDetails.Where(j => j.ParentFolderId == Id).ToList();
                if (subFolderList.Count > 0)
                {
                    foreach (var item in subFolderList)
                    {
                        DeletedData data = db.DeletedDatas.FirstOrDefault(x => x.DeletedBy == uID && !x.IsRecycleBinDeleted && x.FolderId == item.Id);
                        if (data != null)
                        {
                            data.IsRecycleBinDeleted = true;
                            db.Entry(data).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                        if (CommonFunctions.HasChild(item.Id) > 0)
                        {
                            ChangeChildDeleteStatus(item.Id, userId);
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public JsonResult RestoreFolderFiles(long[] deletedDataIDs)
        {
            try
            {
                bool isParentFolder = false;
                long userId = CommonFunctions.GetUserId();
                DBEntities db = new DBEntities();
                if (deletedDataIDs != null && deletedDataIDs.Length > 0)
                {
                    foreach (var deletedDataID in deletedDataIDs)
                    {
                        //var userId = CommonFunctions.GetUserId().ToString();
                        DeletedData dd = db.DeletedDatas.Find(deletedDataID);
                        if (dd != null)
                        {
                            if (dd.FolderId > 0)
                            {
                                db.Entry(dd).State = EntityState.Deleted;
                                db.SaveChanges();
                                FolderDetail FD = db.FolderDetails.Find(dd.FolderId);
                                if (FD != null)
                                {
                                    FD.IsActive = true;
                                    FD.IsDelete = false;
                                    isParentFolder = FD.ParentFolderId > 0 ? false : true;
                                    db.Entry(FD).State = EntityState.Modified;
                                    db.SaveChanges();
                                    if (FD.ParentFolderId > 0)
                                        CheckIsParentFolderActive(FD.ParentFolderId);
                                    RestoreChildDeleteStatus(dd.FolderId, userId);
                                }

                            }
                            else if (dd.DocumentId > 0)
                            {
                                DocumentDetail docDetail = db.DocumentDetails.Find(dd.DocumentId);
                                if (docDetail != null)
                                {
                                    docDetail.IsActive = true;
                                    docDetail.IsDelete = false;
                                    db.Entry(docDetail).State = EntityState.Modified;
                                    db.SaveChanges();
                                    FolderDetail FD = db.FolderDetails.Find(docDetail.FolderId);
                                    if (FD != null)
                                    {
                                        CheckIsParentFolderActive(FD.Id);
                                    }
                                }
                                db.Entry(dd).State = EntityState.Deleted;
                                db.SaveChanges();
                            }

                        }
                    }
                    return Json(new { success = true, msg = "Item restore successfully", isParentFolder = isParentFolder }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { success = false, msg = "No item selected, Select atleast one item!" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, msg = "Something went wrong, please try again!" }, JsonRequestBehavior.AllowGet);
            }
        }

        public void RestoreChildDeleteStatus(long Id, long userId)
        {
            try
            {
                string uID = userId.ToString();
                var subDocList = db.DocumentDetails.Where(j => j.FolderId == Id).ToList();
                if (subDocList.Count > 0)
                {
                    foreach (var item in subDocList)
                    {
                        DeletedData data = db.DeletedDatas.FirstOrDefault(x => x.DeletedBy == uID && x.DocumentId == item.DocumentId);
                        if (data != null)
                        {
                            if (!data.IsRecycleBinDeleted)
                            {
                                db.Entry(data).State = EntityState.Deleted;
                                db.SaveChanges();

                                item.IsActive = true;
                                item.IsDelete = false;
                                item.UpdatedBy = userId.ToString();
                                item.UpdatedDate = DateTime.Now;
                                db.Entry(item).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                        else
                        {
                            item.IsActive = true;
                            item.IsDelete = false;
                            item.UpdatedBy = userId.ToString();
                            item.UpdatedDate = DateTime.Now;
                            db.Entry(item).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                }

                var subFolderList = db.FolderDetails.Where(j => j.ParentFolderId == Id).ToList();

                if (subFolderList.Count > 0)
                {
                    foreach (var item in subFolderList)
                    {
                        //bool isRecycleBinDeleted = false;
                        DeletedData data = db.DeletedDatas.FirstOrDefault(x => x.DeletedBy == uID && x.FolderId == item.Id);
                        if (data != null)
                        {
                            if (data.IsRecycleBinDeleted)
                                break;
                            //isRecycleBinDeleted = data.IsRecycleBinDeleted;
                            //if (!isRecycleBinDeleted)
                            {
                                db.Entry(data).State = EntityState.Deleted;
                                db.SaveChanges();
                            }
                            break;
                        }
                        //if (!isRecycleBinDeleted)
                        {
                            item.IsActive = true;
                            item.IsDelete = false;
                            item.UpdatedBy = userId.ToString();
                            item.UpdatedDate = DateTime.Now;
                            db.Entry(item).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                        if (CommonFunctions.HasChild(item.Id) > 0)
                        {
                            RestoreChildDeleteStatus(item.Id, userId);
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void CheckIsParentFolderActive(long parentId)
        {
            try
            {
                //Check Is Parent Folder Deleted then it's folder active
                FolderDetail fd = db.FolderDetails.FirstOrDefault(x => x.Id == parentId && !x.IsActive && x.IsDelete);
                if (fd != null)
                {
                    fd.IsDelete = false;
                    fd.IsActive = true;
                    db.Entry(fd).State = EntityState.Modified;
                    db.SaveChanges();
                    if (fd.ParentFolderId > 0)
                    {
                        CheckIsParentFolderActive(fd.ParentFolderId);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}