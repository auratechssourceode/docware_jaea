﻿using DocWare.Helpers;
using DocWare.Models;
using DocWare.Resources;
using Ionic.Zip;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using Utility.Helpers;
using Utility.Models;

namespace DocWare.Controllers
{
    [CheckAuthorization]
    public class WorkFlowController : BaseController
    {
        DBEntities db = new DBEntities();
        DAL objDAL = new DAL();
        List<string> parentLst = new List<string>();
        #region --> Workflow List | add| 09042018
        [HttpGet, Route("WorkflowList")]
        public ActionResult WorkflowList()
        {
            List<WorkflowVM> workflowVMs = new List<WorkflowVM>();
            var CabinetId = CommonFunctions.GetCabinetId();
            var UserId = CommonFunctions.GetUserId();
            var ApplicationId = CommonFunctions.GetApplicationId();
            try
            {
                #region --> Page Permission on Action and controller name | Jasmin | 09042018

                var permissionlst = Session[SessionClass.LinkPermissionList].ToString();
                List<UserPermissionVM> userPermissionLst = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserPermissionVM>>(permissionlst);

                PermissionVM permissionVM = new PermissionVM();

                permissionVM.HasAddPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "WorkflowWizard" && x.ControllerName.Trim() == "WorkFlow").Count() > 0;
                permissionVM.HasEditPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "EditWorkflowWizard" && x.ControllerName.Trim() == "WorkFlow").Count() > 0;
                permissionVM.HasStatusPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "ChangeWorkflowStatus" && x.ControllerName.Trim() == "WorkFlow").Count() > 0;
                permissionVM.HasActivePermission = userPermissionLst.Where(x => x.ActionName.Trim() == "ActiveSelectedWorkflow" && x.ControllerName.Trim() == "WorkFlow").Count() > 0;
                permissionVM.HasDeactivePermission = userPermissionLst.Where(x => x.ActionName.Trim() == "DeactiveSelectedWorkflow" && x.ControllerName.Trim() == "WorkFlow").Count() > 0;
                permissionVM.HasDeletePermission = userPermissionLst.Where(x => x.ActionName.Trim() == "DeleteSelectedWorkflow" && x.ControllerName.Trim() == "WorkFlow").Count() > 0;

                Session["PermissionListSession"] = permissionVM;
                #endregion

                using (db = new DBEntities())
                {
                    if (ApplicationId != (int)EnumList.Application.SuperAdmin)
                    {
                        workflowVMs = (from WFM in db.WF_Master
                                       where WFM.CabinetId == CabinetId && !WFM.IsDelete &&
                                       WFM.CreatedBy == UserId.ToString()
                                       select new WorkflowVM
                                       {
                                           WFId = WFM.Id,
                                           WFName = WFM.WFName,
                                           IsManual = WFM.IsManual,
                                           CreatedById = WFM.CreatedBy,
                                           CreatedDate = WFM.CreatedDate,
                                           IsActive = WFM.IsActive
                                       }).Distinct().OrderByDescending(x => x.WFId).ToList();
                    }
                    else
                    {
                        workflowVMs = db.WF_Master.Where(j => j.CabinetId == CabinetId && !j.IsDelete).Select(j => new WorkflowVM
                        {
                            WFId = j.Id,
                            WFName = j.WFName,
                            IsManual = j.IsManual,
                            CreatedById = j.CreatedBy,
                            CreatedDate = j.CreatedDate,
                            IsActive = j.IsActive
                        }).OrderByDescending(x => x.WFId).ToList();
                    }

                    if (workflowVMs.Count > 0)
                    {
                        foreach (var item in workflowVMs)
                        {
                            var id = Convert.ToInt64(item.CreatedById);
                            item.CreatedByName = CommonFunctions.GetFullName(id);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return View(workflowVMs);
        }
        #endregion

        #region --> Active/Deactive and Delete Functionality | Add | Jasmin | 13042018
        [AuditLog(EnumList.EnumLogType.UpdateData, "ChangeWorkflowStatus")]
        public JsonResult ChangeWorkflowStatus(long id)
        {
            WF_Master wfMaster = new WF_Master();
            bool isAssigned = true;
            try
            {
                if (id > 0)
                {
                    using (db = new DBEntities())
                    {
                        long userId = CommonFunctions.GetUserId();
                        wfMaster = db.WF_Master.Where(m => m.Id == id).FirstOrDefault();

                        if (wfMaster.IsActive)
                        {
                            isAssigned = db.WF_Instance.Where(j => j.WFId == wfMaster.Id && j.IsOpen).ToList().Count() > 0;

                            if (isAssigned)
                            {
                                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                wfMaster.IsActive = false;
                            }
                        }
                        else
                        {
                            wfMaster.IsActive = true;
                        }
                        wfMaster.UpdatedBy = userId.ToString();
                        wfMaster.UpdatedDate = DateTime.Now;
                        db.Entry(wfMaster).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [AuditLog(EnumList.EnumLogType.DeleteData, "DeleteSelectedWorkflow")]
        public JsonResult DeleteSelectedWorkflow(string[] workflowIds)
        {
            bool isAssigned = true;
            WF_Master wfMaster = new WF_Master();
            try
            {
                if (workflowIds != null)
                {
                    if (workflowIds.Length > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();

                            isAssigned = db.WF_Instance.Where(j => j.WFId == wfMaster.Id && j.IsOpen).ToList().Count() > 0;

                            if (!isAssigned)
                            {
                                foreach (var item in workflowIds)
                                {
                                    long id = Convert.ToInt64(item);

                                    wfMaster = db.WF_Master.Where(m => m.Id == id).FirstOrDefault();

                                    wfMaster.IsActive = false;
                                    wfMaster.IsDelete = true;
                                    wfMaster.UpdatedBy = userId.ToString();
                                    wfMaster.UpdatedDate = DateTime.Now;
                                    db.Entry(wfMaster).State = EntityState.Modified;
                                    db.SaveChanges();

                                }
                                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "DeactiveSelectedWorkflow")]
        public JsonResult DeactiveSelectedWorkflow(string[] workflowIds)
        {
            bool isAssigned = true;
            WF_Master wfMaster = new WF_Master();
            try
            {
                if (workflowIds != null)
                {
                    if (workflowIds.Length > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();

                            isAssigned = db.WF_Instance.Where(j => j.WFId == wfMaster.Id && j.IsOpen).ToList().Count() > 0;

                            if (!isAssigned)
                            {
                                foreach (var item in workflowIds)
                                {
                                    long id = Convert.ToInt64(item);

                                    wfMaster = db.WF_Master.Where(m => m.Id == id).FirstOrDefault();

                                    wfMaster.IsActive = false;
                                    wfMaster.UpdatedBy = userId.ToString();
                                    wfMaster.UpdatedDate = DateTime.Now;
                                    db.Entry(wfMaster).State = EntityState.Modified;
                                    db.SaveChanges();

                                }
                                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "ActiveSelectedWorkflow")]
        public JsonResult ActiveSelectedWorkflow(string[] workflowIds)
        {
            WF_Master wfMaster = new WF_Master();
            try
            {
                if (workflowIds != null)
                {
                    if (workflowIds.Length > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();

                            foreach (var item in workflowIds)
                            {
                                long id = Convert.ToInt64(item);

                                wfMaster = db.WF_Master.Where(m => m.Id == id).FirstOrDefault();

                                wfMaster.IsActive = true;
                                wfMaster.UpdatedBy = userId.ToString();
                                wfMaster.UpdatedDate = DateTime.Now;
                                db.Entry(wfMaster).State = EntityState.Modified;
                                db.SaveChanges();

                            }
                            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region --> OLD | Workflow Add/Edit and Check Name| Add | Dhrumil Patel | 09042018
        //[HttpGet, Route("AddWorkflow")]
        //public ActionResult AddWorkflow()
        //{
        //    WF_MasterVM detail = new WF_MasterVM();
        //    return View(detail);
        //}

        //[HttpGet, Route("EditWorkflow")]
        //public ActionResult EditWorkflow(long id)
        //{
        //    WF_MasterVM wfMaster = new WF_MasterVM();
        //    try
        //    {
        //        using (db = new DBEntities())
        //        {
        //            if (id > 0)
        //            {
        //                var detail = db.WF_Master.Find(id);
        //                if (detail != null)
        //                {
        //                    wfMaster.Id = detail.Id;
        //                    wfMaster.WFName = detail.WFName;
        //                    wfMaster.Notes = detail.Notes;
        //                    wfMaster.Descritpion = detail.Descritpion;
        //                    wfMaster.AssignFormTemplateId = detail.AssignFormTemplateId;
        //                    wfMaster.AssignFormTemplateName = detail.AssignFormTemplateId > 0 ? db.AssignFormTemplates.Find(detail.AssignFormTemplateId).FormTitle : string.Empty;
        //                    wfMaster.IsManual = detail.IsManual;
        //                    var initaitorIds = db.WF_Initaitors.Where(x => x.WFId == detail.Id).Select(x => x.UserId.ToString()).ToArray();
        //                    wfMaster.WFInitaitors = initaitorIds;
        //                    var closerIds = db.WF_Close.Where(x => x.WFId == detail.Id).Select(x => x.UserId.ToString()).ToArray();
        //                    wfMaster.WFClosers = closerIds;
        //                    var processadminIds = db.WF_ProcessAdmin.Where(x => x.WFId == detail.Id).Select(x => x.UserId.ToString()).ToArray();
        //                    wfMaster.WFProcessAdmins = processadminIds;
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        throw e;
        //    }
        //    return View("AddWorkflow", wfMaster);
        //}

        //[AuditLog(EnumList.EnumLogType.SaveData, "AddWorkflow")]
        //[HttpPost, Route("AddWorkflow")]
        //public ActionResult AddWorkflow(WF_MasterVM detail, FormCollection fc)
        //{
        //    WF_Master wfMaster = new WF_Master();
        //    bool isEidt = false;
        //    using (var transaction = db.Database.BeginTransaction())
        //    {
        //        try
        //        {
        //            long userId = CommonFunctions.GetUserId();
        //            long CabinetId = CommonFunctions.GetCabinetId();

        //            #region WorkflowMaster Add/Update
        //            if (detail.Id > 0)
        //            {
        //                wfMaster = db.WF_Master.Find(detail.Id);
        //                if (detail.FormTemplateId > 0)
        //                {
        //                    AssignFormTemplate assignFormTemplate = new AssignFormTemplate();
        //                    FormTemplate formTemplate = new FormTemplate();
        //                    formTemplate = db.FormTemplates.Find(detail.FormTemplateId);
        //                    if (detail.AssignFormTemplateId > 0)
        //                    {
        //                        assignFormTemplate = db.AssignFormTemplates.Find(detail.AssignFormTemplateId);
        //                        assignFormTemplate.FormTitle = formTemplate.FormTitle;
        //                        assignFormTemplate.FormContant = formTemplate.HtmlContent;
        //                        assignFormTemplate.UpdatedBy = userId.ToString();
        //                        assignFormTemplate.UpdatedDate = DateTime.Now;
        //                        db.SaveChanges();

        //                        var formField = db.AssignedFormControlProperties.Where(j => j.AssignedFormTemplateId == assignFormTemplate.Id).ToList();
        //                        if (formField.Count() > 0)
        //                        {
        //                            db.AssignedFormControlProperties.RemoveRange(formField);
        //                            db.SaveChanges();
        //                        }

        //                        var formFields = db.FormControlProperties.Where(j => j.FormTemplateId == formTemplate.Id).ToList();

        //                        if (formFields != null)
        //                        {
        //                            AssignedFormControlProperty assignedFormControlProperty = new AssignedFormControlProperty();
        //                            foreach (var field in formFields)
        //                            {
        //                                if (field != null)
        //                                {
        //                                    assignedFormControlProperty.AssignedFormTemplateId = assignFormTemplate.Id;
        //                                    assignedFormControlProperty.FieldId = field.FieldId;
        //                                    assignedFormControlProperty.FieldType = field.FieldType;
        //                                    db.AssignedFormControlProperties.Add(assignedFormControlProperty);
        //                                    db.SaveChanges();
        //                                }
        //                            }
        //                        }
        //                    }
        //                    else
        //                    {
        //                        assignFormTemplate.FormTitle = formTemplate.FormTitle;
        //                        assignFormTemplate.FormContant = formTemplate.HtmlContent;
        //                        assignFormTemplate.CreatedBy = userId.ToString();
        //                        assignFormTemplate.CreatedDate = DateTime.Now;
        //                        db.AssignFormTemplates.Add(assignFormTemplate);
        //                        db.SaveChanges();
        //                        detail.AssignFormTemplateId = assignFormTemplate.Id;

        //                        var formFields = db.FormControlProperties.Where(j => j.FormTemplateId == formTemplate.Id).ToList();

        //                        if (formFields != null)
        //                        {
        //                            AssignedFormControlProperty assignedFormControlProperty = new AssignedFormControlProperty();
        //                            foreach (var field in formFields)
        //                            {
        //                                if (field != null)
        //                                {
        //                                    assignedFormControlProperty.AssignedFormTemplateId = assignFormTemplate.Id;
        //                                    assignedFormControlProperty.FieldId = field.FieldId;
        //                                    assignedFormControlProperty.FieldType = field.FieldType;
        //                                    db.AssignedFormControlProperties.Add(assignedFormControlProperty);
        //                                    db.SaveChanges();
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //                wfMaster.WFName = detail.WFName;
        //                wfMaster.Notes = detail.Notes;
        //                wfMaster.Descritpion = detail.Descritpion;
        //                wfMaster.AssignFormTemplateId = detail.AssignFormTemplateId;
        //                wfMaster.CabinetId = CabinetId;
        //                wfMaster.IsManual = fc["chkIsManual"] == "on" ? true : false;
        //                wfMaster.UpdatedBy = userId.ToString();
        //                wfMaster.UpdatedDate = DateTime.Now;
        //                db.SaveChanges();
        //                isEidt = true;
        //            }
        //            else
        //            {
        //                if (detail.FormTemplateId > 0)
        //                {
        //                    AssignFormTemplate assignFormTemplate = new AssignFormTemplate();
        //                    FormTemplate formTemplate = new FormTemplate();
        //                    formTemplate = db.FormTemplates.Find(detail.FormTemplateId);
        //                    assignFormTemplate.FormTitle = formTemplate.FormTitle;
        //                    assignFormTemplate.FormContant = formTemplate.HtmlContent;
        //                    assignFormTemplate.CreatedBy = userId.ToString();
        //                    assignFormTemplate.CreatedDate = DateTime.Now;
        //                    db.AssignFormTemplates.Add(assignFormTemplate);
        //                    db.SaveChanges();
        //                    detail.AssignFormTemplateId = assignFormTemplate.Id;

        //                    var formFields = db.FormControlProperties.Where(j => j.FormTemplateId == formTemplate.Id).ToList();

        //                    if (formFields != null)
        //                    {
        //                        AssignedFormControlProperty assignedFormControlProperty = new AssignedFormControlProperty();
        //                        foreach (var field in formFields)
        //                        {
        //                            if (field != null)
        //                            {
        //                                assignedFormControlProperty.AssignedFormTemplateId = assignFormTemplate.Id;
        //                                assignedFormControlProperty.FieldId = field.FieldId;
        //                                assignedFormControlProperty.FieldType = field.FieldType;
        //                                db.AssignedFormControlProperties.Add(assignedFormControlProperty);
        //                                db.SaveChanges();
        //                            }
        //                        }
        //                    }
        //                }

        //                wfMaster.WFName = detail.WFName;
        //                wfMaster.Notes = detail.Notes;
        //                wfMaster.Descritpion = detail.Descritpion;
        //                wfMaster.AssignFormTemplateId = detail.AssignFormTemplateId;
        //                wfMaster.CabinetId = CabinetId;
        //                wfMaster.WFTotalSteps = 0;
        //                wfMaster.IsManual = fc["chkIsManual"] == "on" ? true : false;
        //                wfMaster.IsActive = true;
        //                wfMaster.IsDelete = false;
        //                wfMaster.CreatedBy = userId.ToString();
        //                wfMaster.CreatedDate = DateTime.Now;
        //                db.WF_Master.Add(wfMaster);
        //                db.SaveChanges();
        //            }
        //            #endregion

        //            #region Add/Update WorkFlow Initaitors
        //            //Need to check is work flow instance, open or not wile update.
        //            var wfInitaitorsRemoveLst = db.WF_Initaitors.Where(x => x.WFId == wfMaster.Id).ToList();
        //            if (wfInitaitorsRemoveLst.Count() > 0)
        //            {
        //                db.WF_Initaitors.RemoveRange(wfInitaitorsRemoveLst);
        //                db.SaveChanges();
        //            }
        //            WF_Initaitors initaitors = new WF_Initaitors();
        //            foreach (var initaitorid in detail.WFInitaitors)
        //            {
        //                if (initaitorid != "")
        //                {
        //                    initaitors.WFId = wfMaster.Id;
        //                    initaitors.UserId = Convert.ToInt64(initaitorid);
        //                    initaitors.CreatedBy = userId.ToString();
        //                    initaitors.CreatedDate = DateTime.Now;
        //                    db.WF_Initaitors.Add(initaitors);
        //                    db.SaveChanges();
        //                }
        //            }
        //            #endregion

        //            #region Add/Update WorkFlow Closers
        //            //Need to check is work flow instance, open or not wile update.
        //            var wfClosersRemoveLst = db.WF_Close.Where(x => x.WFId == wfMaster.Id).ToList();
        //            if (wfClosersRemoveLst.Count() > 0)
        //            {
        //                db.WF_Close.RemoveRange(wfClosersRemoveLst);
        //                db.SaveChanges();
        //            }
        //            WF_Close closers = new WF_Close();
        //            foreach (var closerid in detail.WFClosers)
        //            {
        //                if (closerid != "")
        //                {
        //                    closers.WFId = wfMaster.Id;
        //                    closers.UserId = Convert.ToInt64(closerid);
        //                    closers.CreatedBy = userId.ToString();
        //                    closers.CreatedDate = DateTime.Now;
        //                    db.WF_Close.Add(closers);
        //                    db.SaveChanges();
        //                }
        //            }
        //            #endregion

        //            #region Add/Update WorkFlow ProcessAdmin
        //            //Need to check is work flow instance, open or not wile update.
        //            var wfProcessAdminRemoveLst = db.WF_ProcessAdmin.Where(x => x.WFId == wfMaster.Id).ToList();
        //            if (wfProcessAdminRemoveLst.Count() > 0)
        //            {
        //                db.WF_ProcessAdmin.RemoveRange(wfProcessAdminRemoveLst);
        //                db.SaveChanges();
        //            }
        //            WF_ProcessAdmin processadmins = new WF_ProcessAdmin();
        //            foreach (var processadminid in detail.WFProcessAdmins)
        //            {
        //                if (processadminid != "")
        //                {
        //                    processadmins.WFId = wfMaster.Id;
        //                    processadmins.UserId = Convert.ToInt64(processadminid);
        //                    processadmins.CreatedBy = userId.ToString();
        //                    processadmins.CreatedDate = DateTime.Now;
        //                    db.WF_ProcessAdmin.Add(processadmins);
        //                    db.SaveChanges();
        //                }
        //            }
        //            #endregion

        //            if (detail.Id == 0)
        //            {
        //                TempData["SuccessMSG"] = "Workflow added successfully.";
        //            }
        //            else
        //            {
        //                TempData["SuccessMSG"] = "Workflow updated successfully.";
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            transaction.Rollback();
        //            TempData["ErrorMSG"] = "Error while add/updating data.";
        //            return RedirectToAction("WorkflowList");
        //        }
        //        transaction.Commit();
        //    }
        //    if (isEidt)
        //    {
        //        return RedirectToAction("WorkflowList");
        //    }
        //    else
        //    {
        //        if (wfMaster.IsManual)
        //        {
        //            return RedirectToAction("WorkflowList");
        //        }
        //        else
        //        {
        //            return RedirectToAction("WorkflowStepList", new { WfId = detail.Id });
        //        }
        //    }
        //}

        //public JsonResult CheckWorkFlowName(string WorkFlowName, long WfId)
        //{
        //    List<WF_Master> workflowDetails = new List<WF_Master>();
        //    try
        //    {
        //        using (db = new DBEntities())
        //        {
        //            long CabinetId = CommonFunctions.GetCabinetId();

        //            if (WfId > 0)
        //            {
        //                workflowDetails = db.WF_Master.Where(j => j.WFName.ToLower().Trim() == WorkFlowName.ToLower().Trim() && j.CabinetId == CabinetId && j.Id != WfId).ToList();
        //            }
        //            else
        //            {
        //                workflowDetails = db.WF_Master.Where(j => j.WFName.ToLower().Trim() == WorkFlowName.ToLower().Trim() && j.CabinetId == CabinetId).ToList();
        //            }

        //            if (workflowDetails.Count() > 0)
        //            {
        //                return Json(false, JsonRequestBehavior.AllowGet);
        //            }
        //            else
        //            {
        //                return Json(true, JsonRequestBehavior.AllowGet);
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        return Json(false, JsonRequestBehavior.AllowGet);
        //    }
        //}
        #endregion

        #region --> Workflow Steps | Add | Jasmin Vohra | 10042018
        [HttpGet, Route("WorkflowStepList/{WfId}")]
        public ActionResult WorkflowStepList(long WfId)
        {
            WorkflowStepListVM workflowStepListVM = new WorkflowStepListVM();
            List<WF_StepDetailVM> WF_StepDetailVMs = new List<WF_StepDetailVM>();
            try
            {
                using (db = new DBEntities())
                {
                    if (WfId > 0)
                    {
                        var workflow = db.WF_Master.Where(j => j.Id == WfId).FirstOrDefault();

                        if (workflow != null)
                        {

                            workflowStepListVM.WFId = workflow.Id;
                            workflowStepListVM.WFName = workflow.WFName;

                            WF_StepDetailVMs = db.WF_Details.Where(j => j.WFId == WfId).Select(j => new WF_StepDetailVM
                            {
                                Id = j.Id,
                                WFId = j.WFId,
                                WFStep = j.WFStep,
                                WFStepName = j.WFStepName,
                                WFStepNotes = j.WFStepNotes,
                                AssignedUserID = j.AssignedUserID
                            }).OrderBy(x => x.WFStep).ToList();

                            if (WF_StepDetailVMs.Count > 0)
                            {
                                foreach (var item in WF_StepDetailVMs)
                                {
                                    item.AssignedUserName = CommonFunctions.GetFullName(item.AssignedUserID);
                                }
                            }
                        }
                    }
                    workflowStepListVM.WFStepsList = WF_StepDetailVMs;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return View(workflowStepListVM);
        }

        [HttpGet, Route("AddWorkflowStep")]
        public ActionResult AddWorkflowStep(long WfId)
        {
            WF_Details wf_Details = new WF_Details();
            wf_Details.WFId = WfId;
            return View(wf_Details);
        }

        [HttpGet, Route("EditWorkflowStep")]
        public ActionResult EditWorkflowStep(long id)
        {
            WF_Details wf_Details = new WF_Details();
            try
            {
                if (id > 0)
                {
                    using (db = new DBEntities())
                    {
                        wf_Details = db.WF_Details.Where(j => j.Id == id).FirstOrDefault();
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return View("AddWorkflowStep", wf_Details);
        }

        [AuditLog(EnumList.EnumLogType.SaveData, "AddWorkflowStep")]
        [HttpPost, Route("AddWorkflowStep")]
        public ActionResult AddWorkflowStep(WF_Details data)
        {
            WF_Details wf_Details = new WF_Details();
            try
            {
                using (db = new DBEntities())
                {
                    long userId = CommonFunctions.GetUserId();
                    if (data.Id > 0)
                    {
                        wf_Details = db.WF_Details.Where(j => j.Id == data.Id).FirstOrDefault();

                        wf_Details.WFStep = data.WFStep;
                        wf_Details.WFStepName = data.WFStepName;
                        wf_Details.AssignedUserID = data.AssignedUserID;
                        wf_Details.WFStepNotes = data.WFStepNotes;
                        db.Entry(wf_Details).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    else
                    {
                        wf_Details.WFId = data.WFId;
                        wf_Details.WFStep = data.WFStep;
                        wf_Details.WFStepName = data.WFStepName;
                        wf_Details.AssignedUserID = data.AssignedUserID;
                        wf_Details.WFStepNotes = data.WFStepNotes;
                        wf_Details.CreatedBy = userId.ToString();
                        wf_Details.CreatedDate = DateTime.Now;
                        db.WF_Details.Add(wf_Details);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return RedirectToAction("WorkflowStepList", new { WfId = data.WFId });
        }

        public JsonResult CheckWorkflowStepNo(int StepNo, long WFId, long WFDetailId)
        {
            List<WF_Details> wf_Details = new List<WF_Details>();
            try
            {
                using (db = new DBEntities())
                {
                    if (WFDetailId > 0)
                    {
                        wf_Details = db.WF_Details.Where(j => j.WFStep == StepNo && j.WFId == WFId && j.Id != WFDetailId).ToList();
                    }
                    else
                    {
                        wf_Details = db.WF_Details.Where(j => j.WFStep == StepNo && j.WFId == WFId).ToList();
                    }
                    if (wf_Details.Count() > 0)
                    {
                        return Json(false, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(true, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult CheckWorkflowStepName(string StepName, long WFId, long WFDetailId)
        {
            List<WF_Details> wf_Details = new List<WF_Details>();
            try
            {
                using (db = new DBEntities())
                {
                    if (WFDetailId > 0)
                    {
                        wf_Details = db.WF_Details.Where(j => j.WFStepName.ToLower().Trim() == StepName.ToLower().Trim() && j.WFId == WFId && j.Id != WFDetailId).ToList();
                    }
                    else
                    {
                        wf_Details = db.WF_Details.Where(j => j.WFStepName.ToLower().Trim() == StepName.ToLower().Trim() && j.WFId == WFId).ToList();
                    }
                    if (wf_Details.Count() > 0)
                    {
                        return Json(false, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(true, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region --> NEW | Workflow Add/Edit and Check Name| Add | Dhrumil Patel | 27042018
        [HttpGet, Route("WorkflowWizard")]
        public ActionResult WorkflowWizard()
        {
            WF_MasterVM detail = new WF_MasterVM();
            var cabinetDetail = db.CabinetMasters.Find(CommonFunctions.GetCabinetId());
            detail.CabinetName = cabinetDetail != null ? cabinetDetail.Name : "";
            return View(detail);
        }

        [HttpGet, Route("ManualWorkflowWizard")]
        public ActionResult ManualWorkflowWizard()
        {
            WF_MasterVM detail = new WF_MasterVM();
            var cabinetDetail = db.CabinetMasters.Find(CommonFunctions.GetCabinetId());
            detail.CabinetName = cabinetDetail != null ? cabinetDetail.Name : "";
            detail.IsManual = true;
            return View(detail);
        }

        [HttpGet, Route("EditWorkflowWizard")]
        public ActionResult EditWorkflowWizard(long id)
        {
            WF_MasterVM wfMaster = new WF_MasterVM();
            var cabinetDetail = db.CabinetMasters.Find(CommonFunctions.GetCabinetId());
            wfMaster.CabinetName = cabinetDetail != null ? cabinetDetail.Name : "";
            try
            {
                using (db = new DBEntities())
                {
                    if (id > 0)
                    {
                        var detail = db.WF_Master.Find(id);
                        if (detail != null)
                        {
                            wfMaster.Id = detail.Id;
                            wfMaster.WFName = detail.WFName;
                            wfMaster.Notes = detail.Notes;
                            wfMaster.Descritpion = detail.Descritpion;
                            wfMaster.AssignFormTemplateId = detail.AssignFormTemplateId;
                            wfMaster.AssignFormTemplateName = detail.AssignFormTemplateId > 0 ? db.AssignFormTemplates.Find(detail.AssignFormTemplateId).FormTitle : string.Empty;
                            wfMaster.IsManual = detail.IsManual;
                            var initaitors = db.WF_Initaitors.Where(x => x.WFId == detail.Id).ToList();
                            wfMaster.WFInitaitors = initaitors.Where(x => x.GroupId == 0).Select(x => x.UserId.ToString()).ToArray();
                            //wfMaster.WFGroupInitaitors = initaitors.Where(x => x.GroupId != 0).Select(x => x.GroupId.ToString()).Distinct().ToArray();
                            var closerIds = db.WF_Close.Where(x => x.WFId == detail.Id).ToList();
                            wfMaster.WFClosers = closerIds.Where(x => x.GroupId == 0).Select(x => x.UserId.ToString()).ToArray();
                            //wfMaster.WFGroupClosers = closerIds.Where(x => x.GroupId != 0).Select(x => x.GroupId.ToString()).Distinct().ToArray();
                            var processadminIds = db.WF_ProcessAdmin.Where(x => x.WFId == detail.Id).ToList();
                            wfMaster.WFProcessAdmins = processadminIds.Where(x => x.GroupId == 0).Select(x => x.UserId.ToString()).ToArray();
                            //wfMaster.WFGroupProcessAdmins = processadminIds.Where(x => x.GroupId != 0).Select(x => x.GroupId.ToString()).Distinct().ToArray();

                            var stepLst = db.WF_Details.Where(x => x.WFId == detail.Id).ToList();
                            wfMaster.WFStepList = new List<WFStepVM>();
                            foreach (var data in stepLst)
                            {
                                var step = new WFStepVM();
                                step.WFStep = data.WFStep;
                                step.WFStepName = data.WFStepName;
                                step.WFStepNotes = data.WFStepNotes;
                                step.AssignedUserID = data.AssignedUserID;
                                step.WFActions = data.WFActions.Split(',').ToArray();
                                wfMaster.WFStepList.Add(step);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            if (wfMaster.IsManual)
            {
                return View("ManualWorkflowWizard", wfMaster);
            }
            else
            {
                return View("WorkflowWizard", wfMaster);
            }
        }

        [HttpPost, Route("WorkflowWizard")]
        [AuditLog(EnumList.EnumLogType.SaveData, "AddWorkflow")]
        public ActionResult WorkflowWizard(WorkFlowVM detail)
        {
            WF_Master wfMaster = new WF_Master();
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    long userId = CommonFunctions.GetUserId();
                    long CabinetId = CommonFunctions.GetCabinetId();

                    #region WorkflowMaster Add/Update
                    if (detail.WFList.Id > 0)
                    {
                        wfMaster = db.WF_Master.Find(detail.WFList.Id);
                        if (detail.WFList.FormTemplateId > 0)
                        {
                            AssignFormTemplate assignFormTemplate = new AssignFormTemplate();
                            FormTemplate formTemplate = new FormTemplate();
                            formTemplate = db.FormTemplates.Find(detail.WFList.FormTemplateId);
                            if (detail.WFList.AssignFormTemplateId > 0)
                            {
                                assignFormTemplate = db.AssignFormTemplates.Find(detail.WFList.AssignFormTemplateId);
                                assignFormTemplate.FormTitle = formTemplate.FormTitle;
                                assignFormTemplate.FormContant = formTemplate.HtmlContent;
                                assignFormTemplate.UpdatedBy = userId.ToString();
                                assignFormTemplate.UpdatedDate = DateTime.Now;
                                db.SaveChanges();

                                var formField = db.AssignedFormControlProperties.Where(j => j.AssignedFormTemplateId == assignFormTemplate.Id).ToList();
                                if (formField.Count() > 0)
                                {
                                    db.AssignedFormControlProperties.RemoveRange(formField);
                                    db.SaveChanges();
                                }

                                var formFields = db.FormControlProperties.Where(j => j.FormTemplateId == formTemplate.Id).ToList();
                                if (formFields != null)
                                {
                                    AssignedFormControlProperty assignedFormControlProperty = new AssignedFormControlProperty();
                                    foreach (var field in formFields)
                                    {
                                        if (field != null)
                                        {
                                            assignedFormControlProperty.AssignedFormTemplateId = assignFormTemplate.Id;
                                            assignedFormControlProperty.FieldId = field.FieldId;
                                            assignedFormControlProperty.FieldType = field.FieldType;
                                            db.AssignedFormControlProperties.Add(assignedFormControlProperty);
                                            db.SaveChanges();
                                        }
                                    }
                                }
                            }
                            else
                            {
                                assignFormTemplate.FormTitle = formTemplate.FormTitle;
                                assignFormTemplate.FormContant = formTemplate.HtmlContent;
                                assignFormTemplate.CreatedBy = userId.ToString();
                                assignFormTemplate.CreatedDate = DateTime.Now;
                                db.AssignFormTemplates.Add(assignFormTemplate);
                                db.SaveChanges();
                                detail.WFList.AssignFormTemplateId = assignFormTemplate.Id;

                                var formFields = db.FormControlProperties.Where(j => j.FormTemplateId == formTemplate.Id).ToList();
                                if (formFields != null)
                                {
                                    AssignedFormControlProperty assignedFormControlProperty = new AssignedFormControlProperty();
                                    foreach (var field in formFields)
                                    {
                                        if (field != null)
                                        {
                                            assignedFormControlProperty.AssignedFormTemplateId = assignFormTemplate.Id;
                                            assignedFormControlProperty.FieldId = field.FieldId;
                                            assignedFormControlProperty.FieldType = field.FieldType;
                                            db.AssignedFormControlProperties.Add(assignedFormControlProperty);
                                            db.SaveChanges();
                                        }
                                    }
                                }
                            }
                        }
                        wfMaster.WFName = detail.WFList.WFName;
                        wfMaster.Notes = detail.WFList.Notes;
                        wfMaster.Descritpion = detail.WFList.Descritpion;
                        wfMaster.AssignFormTemplateId = detail.WFList.AssignFormTemplateId;
                        wfMaster.CabinetId = CabinetId;
                        wfMaster.IsManual = detail.WFList.WFType == "Y" ? true : false;
                        wfMaster.UpdatedBy = userId.ToString();
                        wfMaster.UpdatedDate = DateTime.Now;
                        db.SaveChanges();
                    }
                    else
                    {
                        if (detail.WFList.FormTemplateId > 0)
                        {
                            AssignFormTemplate assignFormTemplate = new AssignFormTemplate();
                            FormTemplate formTemplate = new FormTemplate();
                            formTemplate = db.FormTemplates.Find(detail.WFList.FormTemplateId);
                            assignFormTemplate.FormTitle = formTemplate.FormTitle;
                            assignFormTemplate.FormContant = formTemplate.HtmlContent;
                            assignFormTemplate.CreatedBy = userId.ToString();
                            assignFormTemplate.CreatedDate = DateTime.Now;
                            db.AssignFormTemplates.Add(assignFormTemplate);
                            db.SaveChanges();
                            detail.WFList.AssignFormTemplateId = assignFormTemplate.Id;

                            var formFields = db.FormControlProperties.Where(j => j.FormTemplateId == formTemplate.Id).ToList();

                            if (formFields != null)
                            {
                                AssignedFormControlProperty assignedFormControlProperty = new AssignedFormControlProperty();
                                foreach (var field in formFields)
                                {
                                    if (field != null)
                                    {
                                        assignedFormControlProperty.AssignedFormTemplateId = assignFormTemplate.Id;
                                        assignedFormControlProperty.FieldId = field.FieldId;
                                        assignedFormControlProperty.FieldType = field.FieldType;
                                        db.AssignedFormControlProperties.Add(assignedFormControlProperty);
                                        db.SaveChanges();
                                    }
                                }
                            }
                        }

                        wfMaster.WFName = detail.WFList.WFName;
                        wfMaster.Notes = detail.WFList.Notes;
                        wfMaster.Descritpion = detail.WFList.Descritpion;
                        wfMaster.AssignFormTemplateId = detail.WFList.AssignFormTemplateId;
                        wfMaster.CabinetId = CabinetId;
                        wfMaster.WFTotalSteps = detail.WFStepList.Count();
                        wfMaster.IsManual = detail.WFList.WFType == "Y" ? true : false;
                        wfMaster.IsActive = true;
                        wfMaster.IsDelete = false;
                        wfMaster.CreatedBy = userId.ToString();
                        wfMaster.CreatedDate = DateTime.Now;
                        db.WF_Master.Add(wfMaster);
                        db.SaveChanges();
                    }
                    #endregion                   

                    #region Add/Update WorkFlow Initaitors
                    //Need to check is work flow instance, open or not wile update.
                    var wfInitaitorsRemoveLst = db.WF_Initaitors.Where(x => x.WFId == wfMaster.Id).ToList();
                    if (wfInitaitorsRemoveLst.Count() > 0)
                    {
                        db.WF_Initaitors.RemoveRange(wfInitaitorsRemoveLst);
                        db.SaveChanges();
                    }
                    WF_Initaitors initaitors = new WF_Initaitors();
                    if (detail.WFUserList.WFInitaitors != null)
                    {
                        foreach (var initaitorid in detail.WFUserList.WFInitaitors)
                        {
                            if (initaitorid != "")
                            {
                                initaitors.WFId = wfMaster.Id;
                                initaitors.UserId = Convert.ToInt64(initaitorid);
                                initaitors.GroupId = 0;
                                initaitors.CreatedBy = userId.ToString();
                                initaitors.CreatedDate = DateTime.Now;
                                db.WF_Initaitors.Add(initaitors);
                                db.SaveChanges();
                            }
                        }
                    }
                    //if (detail.WFUserList.WFGroupInitaitors != null)
                    //{
                    //    foreach (var initaitorGroupid in detail.WFUserList.WFGroupInitaitors)
                    //    {
                    //        if (initaitorGroupid != "")
                    //        {
                    //            var gId = Convert.ToInt64(initaitorGroupid);
                    //            var users = db.GroupUserDetails.Where(x => x.GroupId == gId).Select(x => x.UserId).ToList();
                    //            foreach (var uId in users)
                    //            {
                    //                initaitors.WFId = wfMaster.Id;
                    //                initaitors.UserId = uId;
                    //                initaitors.GroupId = Convert.ToInt64(initaitorGroupid);
                    //                initaitors.CreatedBy = userId.ToString();
                    //                initaitors.CreatedDate = DateTime.Now;
                    //                db.WF_Initaitors.Add(initaitors);
                    //                db.SaveChanges();
                    //            }
                    //        }
                    //    }
                    //}
                    #endregion

                    #region Add/Update WorkFlow Closers
                    //Need to check is work flow instance, open or not wile update.
                    var wfClosersRemoveLst = db.WF_Close.Where(x => x.WFId == wfMaster.Id).ToList();
                    if (wfClosersRemoveLst.Count() > 0)
                    {
                        db.WF_Close.RemoveRange(wfClosersRemoveLst);
                        db.SaveChanges();
                    }
                    WF_Close closers = new WF_Close();
                    if (detail.WFUserList.WFClosers != null)
                    {
                        foreach (var closerid in detail.WFUserList.WFClosers)
                        {
                            if (closerid != "")
                            {
                                closers.WFId = wfMaster.Id;
                                closers.UserId = Convert.ToInt64(closerid);
                                closers.GroupId = 0;
                                closers.CreatedBy = userId.ToString();
                                closers.CreatedDate = DateTime.Now;
                                db.WF_Close.Add(closers);
                                db.SaveChanges();
                            }
                        }
                    }
                    //if (detail.WFUserList.WFGroupClosers != null)
                    //{
                    //    foreach (var closerGroupid in detail.WFUserList.WFGroupClosers)
                    //    {
                    //        if (closerGroupid != "")
                    //        {
                    //            var gId = Convert.ToInt64(closerGroupid);
                    //            var users = db.GroupUserDetails.Where(x => x.GroupId == gId).Select(x => x.UserId).ToList();
                    //            foreach (var uId in users)
                    //            {
                    //                closers.WFId = wfMaster.Id;
                    //                closers.UserId = uId;
                    //                closers.GroupId = Convert.ToInt64(closerGroupid);
                    //                closers.CreatedBy = userId.ToString();
                    //                closers.CreatedDate = DateTime.Now;
                    //                db.WF_Close.Add(closers);
                    //                db.SaveChanges();
                    //            }
                    //        }
                    //    }
                    //}
                    #endregion

                    #region Add/Update WorkFlow ProcessAdmin
                    //Need to check is work flow instance, open or not wile update.
                    var wfProcessAdminRemoveLst = db.WF_ProcessAdmin.Where(x => x.WFId == wfMaster.Id).ToList();
                    if (wfProcessAdminRemoveLst.Count() > 0)
                    {
                        db.WF_ProcessAdmin.RemoveRange(wfProcessAdminRemoveLst);
                        db.SaveChanges();
                    }
                    WF_ProcessAdmin processadmins = new WF_ProcessAdmin();
                    if (detail.WFUserList.WFProcessAdmins != null)
                    {
                        foreach (var processadminid in detail.WFUserList.WFProcessAdmins)
                        {
                            if (processadminid != "")
                            {
                                processadmins.WFId = wfMaster.Id;
                                processadmins.UserId = Convert.ToInt64(processadminid);
                                processadmins.GroupId = 0;
                                processadmins.CreatedBy = userId.ToString();
                                processadmins.CreatedDate = DateTime.Now;
                                db.WF_ProcessAdmin.Add(processadmins);
                                db.SaveChanges();
                            }
                        }
                    }
                    //if (detail.WFUserList.WFGroupProcessAdmins != null)
                    //{
                    //    foreach (var processGroupadminid in detail.WFUserList.WFGroupProcessAdmins)
                    //    {
                    //        if (processGroupadminid != "")
                    //        {
                    //            var gId = Convert.ToInt64(processGroupadminid);
                    //            var users = db.GroupUserDetails.Where(x => x.GroupId == gId).Select(x => x.UserId).ToList();
                    //            foreach (var uId in users)
                    //            {
                    //                processadmins.WFId = wfMaster.Id;
                    //                processadmins.UserId = uId;
                    //                processadmins.GroupId = Convert.ToInt64(processGroupadminid);
                    //                processadmins.CreatedBy = userId.ToString();
                    //                processadmins.CreatedDate = DateTime.Now;
                    //                db.WF_ProcessAdmin.Add(processadmins);
                    //                db.SaveChanges();
                    //            }
                    //        }
                    //    }
                    //}
                    #endregion

                    #region Add/Update WorkFlow Steps
                    var wfDetailsRemoveLst = db.WF_Details.Where(x => x.WFId == wfMaster.Id).ToList();
                    if (wfDetailsRemoveLst.Count() > 0)
                    {
                        db.WF_Details.RemoveRange(wfDetailsRemoveLst);
                        db.SaveChanges();
                    }
                    WF_Details wfStep = new WF_Details();
                    if (detail.WFStepList != null)
                    {
                        foreach (var data in detail.WFStepList)
                        {
                            var action = "";
                            foreach (var item in data.WFActions)
                            {
                                action += item + ",";
                            }
                            //if (detail.WFStepList.Count == data.WFStep)
                            //{
                            //    foreach (var item in data.WFActions)
                            //    {
                            //        if (item != "3")
                            //        {
                            //            action += item + ",";
                            //        }
                            //    }
                            //}
                            //else
                            //{
                            //    foreach (var item in data.WFActions)
                            //    {
                            //        action += item + ",";
                            //    }
                            //    action = action.Contains("3") ? action : action + "3,";
                            //}
                            wfStep.WFStep = data.WFStep;
                            wfStep.WFStepName = data.WFStepName;
                            wfStep.WFId = wfMaster.Id;
                            wfStep.AssignedUserID = data.AssignedUserID;
                            wfStep.WFStepNotes = data.WFStepNotes != null ? data.WFStepNotes : "";
                            wfStep.WFActions = action != "" ? action.Substring(0, action.Length - 1) : action;
                            wfStep.CreatedBy = userId.ToString();
                            wfStep.CreatedDate = DateTime.Now;
                            db.WF_Details.Add(wfStep);
                            db.SaveChanges();
                        }
                    }
                    #endregion

                    if (detail.WFList.Id == 0)
                    {
                        TempData["SuccessMSG"] = "Workflow added successfully.";
                    }
                    else
                    {
                        TempData["SuccessMSG"] = "Workflow updated successfully.";
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    if (detail.WFList.Id > 0)
                    {
                        TempData["ErrorMSG"] = "Error occured while updating data.";
                    }
                    else
                    {
                        TempData["ErrorMSG"] = "Error occured while adding data.";
                    }
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
                transaction.Commit();
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult CheckWorkFlowName(string WorkFlowName, long WfId)
        {
            List<WF_Master> workflowDetails = new List<WF_Master>();
            try
            {
                using (db = new DBEntities())
                {
                    long CabinetId = CommonFunctions.GetCabinetId();

                    if (WfId > 0)
                    {
                        workflowDetails = db.WF_Master.Where(j => j.WFName.ToLower().Trim() == WorkFlowName.ToLower().Trim() && j.CabinetId == CabinetId && j.Id != WfId).ToList();
                    }
                    else
                    {
                        workflowDetails = db.WF_Master.Where(j => j.WFName.ToLower().Trim() == WorkFlowName.ToLower().Trim() && j.CabinetId == CabinetId).ToList();
                    }

                    if (workflowDetails.Count() > 0)
                    {
                        return Json(false, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(true, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region --> Add Dynamic WorkFlow Step| Add | Dhrumil Patel | 01052018
        [HttpGet]
        public PartialViewResult _DynamicWorkFlowStep(long Id)
        {
            WFStep dynamicControl = new WFStep();
            dynamicControl.StepNumber = Id;
            return PartialView(dynamicControl != null ? dynamicControl : new WFStep());
        }
        #endregion

        #region --> User Work Flow List
        [HttpGet, Route("UserWorkFlowList")]
        public ActionResult UserWorkFlowList()
        {
            UserWorkflowVM wf = new UserWorkflowVM();
            List<UserWorkflowListVM> workflowVMs = new List<UserWorkflowListVM>();
            var CabinetId = CommonFunctions.GetCabinetId();
            var UserId = CommonFunctions.GetUserId();
            SortedList sl = new SortedList();
            DataTable dt = new DataTable();
            SortedList sl1 = new SortedList();
            DataTable dt1 = new DataTable();
            try
            {
                wf.CabinetName = Session[SessionClass.CabinetName].ToString();
                sl.Add("@UserId", UserId);
                sl.Add("@CabinetId", CabinetId);
                //dt = objDAL.GetDataTable("uspGetWorkFlowList", sl);
                var StepOwner = CommonFunctions.GetStepOwner();
                if (StepOwner == true)
                {
                    dt = objDAL.GetDataTable("uspGetCurrentStepWorkFlowList", sl);
                }
                else
                {
                    dt = objDAL.GetDataTable("uspGetWorkFlowList", sl);
                }
                wf.WorkFlowList = objDAL.ConvertToList<UserWorkflowListVM>(dt).ToList();
                              

                //// IsInitiator || Add By Kiran || 14092020

                List<UserWorkflowListVM> lstUserWorkflow = new List<UserWorkflowListVM>();
                sl1.Add("@userId", UserId);
                sl1.Add("@cabinetId", CabinetId);
                lstUserWorkflow = objDAL.ConvertToList<UserWorkflowListVM>
                    (objDAL.GetDataTable("uspGetSelfWFListByUserIdAndCabinet", sl1)
                    ).ToList();
                wf.IsInitiator = false;
                if (lstUserWorkflow.Count > 0)
                {
                    wf.IsInitiator = true;
                }


            }
            catch (Exception e)
            {
                throw e;
            }
            return View(wf);
        }

        [HttpGet, Route("AssignedWorkflowList")]
        public ActionResult AssignedWorkflowList(long Id)
        {
            UserWorkflowAssignesVM wfassigenedList = new UserWorkflowAssignesVM();
            var CabinetId = CommonFunctions.GetCabinetId();
            var UserId = CommonFunctions.GetUserId();
            SortedList sl = new SortedList();
            DataTable dt = new DataTable();
            try
            {
                wfassigenedList.CabinetName = Session[SessionClass.CabinetName].ToString();
                var wf = db.WF_Master.Find(Id);
                wfassigenedList.WFId = wf.Id;
                wfassigenedList.WFName = wf.WFName;
                wfassigenedList.WFInitiateCount = db.WF_Instance.Where(x => x.InitiatorUser == UserId && x.WFId == Id && x.IsStart != null).Count();
                sl.Add("@UserId", UserId);
                sl.Add("@CabinetId", CabinetId);
                sl.Add("@WFId", Id);
                var StepOwner = CommonFunctions.GetStepOwner();
                if (StepOwner == true)
                {
                    dt = objDAL.GetDataTable("uspGetCurrentStepWorkFlowAssignedList", sl);
                }
                else
                {
                    dt = objDAL.GetDataTable("uspGetWorkFlowAssignedList", sl);
                }
                //dt = objDAL.GetDataTable("uspGetWorkFlowAssignedList", sl);
             
                List<UserWorkflowAssignesListVM> WorkFlowAssignedLst = objDAL.ConvertToList<UserWorkflowAssignesListVM>(dt).ToList();

                wfassigenedList.WFInprocessCount = WorkFlowAssignedLst.Where(x => x.IsOpen).Count();
                wfassigenedList.WFHighPriorityCount = WorkFlowAssignedLst.Where(x => x.IsOpen && x.Priority == (long)EnumList.Priority.High).Count();
                wfassigenedList.WFMediumPriorityCount = WorkFlowAssignedLst.Where(x => x.IsOpen && x.Priority == (long)EnumList.Priority.Medium).Count();
                wfassigenedList.WFLowPriorityCount = WorkFlowAssignedLst.Where(x => x.IsOpen && x.Priority == (long)EnumList.Priority.Low).Count();

                ////Change by kiran as per hasan "This will show task assign to me and I did't take any action about them" || 04022021
                //var MyRequest = (from WI in db.WF_Instance
                //                 join WT in db.WF_Transactions on WI.Id equals WT.WFInstanceId
                //                 where WI.WFId == Id &&
                //                 (//WT.FromUser == UserId ||
                //                  WT.ToUser == UserId) && WT.ActionDate == null && WT.EndDateTime == null
                //                 && WI.IsOpen == true
                //                 //&& WT.ActionId != 0
                //                 select WI.Id).ToList().Distinct();
                //wfassigenedList.WFInprocessCount = WorkFlowAssignedLst.Where(x => MyRequest.Contains(x.WFInstanceId)).Count();
                //wfassigenedList.WFHighPriorityCount = WorkFlowAssignedLst.Where(x => MyRequest.Contains(x.WFInstanceId) && x.IsOpen && x.Priority == (long)EnumList.Priority.High).Count();
                //wfassigenedList.WFMediumPriorityCount = WorkFlowAssignedLst.Where(x => MyRequest.Contains(x.WFInstanceId) && x.IsOpen && x.Priority == (long)EnumList.Priority.Medium).Count();
                //wfassigenedList.WFLowPriorityCount = WorkFlowAssignedLst.Where(x => MyRequest.Contains(x.WFInstanceId) && x.IsOpen && x.Priority == (long)EnumList.Priority.Low).Count();

                //// IsInitiator || Add By Kiran || 14092020

                SortedList sl1 = new SortedList();
                List<UserWorkflowListVM> lstUserWorkflow = new List<UserWorkflowListVM>();
                sl1.Add("@userId", UserId);
                sl1.Add("@cabinetId", CabinetId);
                lstUserWorkflow = objDAL.ConvertToList<UserWorkflowListVM>
                    (objDAL.GetDataTable("uspGetSelfWFListByUserIdAndCabinet", sl1)
                    ).ToList();
                wfassigenedList.IsInitiator = false;
                lstUserWorkflow = lstUserWorkflow.Where(x => x.WFId == Id).ToList();
                if (lstUserWorkflow.Count > 0)
                {
                    wfassigenedList.IsInitiator = true;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return View(wfassigenedList);
        }

        public PartialViewResult _AssignedWorkflowList(bool IsOpen, long Id, string SearchKeyword = null, int TypeID = 1)
        {
            List<UserWorkflowAssignesListVM> WorkFlowAssignedLst = new List<UserWorkflowAssignesListVM>();
            try
            {
                var CabinetId = CommonFunctions.GetCabinetId();
                var UserId = CommonFunctions.GetUserId();
                SortedList sl = new SortedList();
                DataTable dt = new DataTable();
                try
                {
                    sl.Add("@UserId", UserId);
                    sl.Add("@CabinetId", CabinetId);
                    sl.Add("@WFId", Id);
                    sl.Add("@Type", TypeID);
                    if (!string.IsNullOrWhiteSpace(SearchKeyword))
                        sl.Add("@searchword", SearchKeyword.Trim());
                    //var StepOwner = CommonFunctions.GetStepOwner();
                    //if (StepOwner == true)
                    //{                       
                    //    dt = objDAL.GetDataTable("uspGetCurrentStepWorkFlowAssignedList", sl);
                    //}
                    //else
                    //{
                    //    dt = objDAL.GetDataTable("uspGetWorkFlowAssignedList", sl);
                    //}
                    dt = objDAL.GetDataTable("uspGetWorkFlowAssignedList", sl);
                    //// New sp to get data || Kiran sawant || 09032021
                    // uspGetWorkFlowAssignedList_New

                    WorkFlowAssignedLst = objDAL.ConvertToList<UserWorkflowAssignesListVM>(dt).ToList();
                    //WorkFlowAssignedLst = WorkFlowAssignedLst.Where(x => x.IsOpen == IsOpen).ToList();
                   
                    ////  change by kiran || 04022021
                    ////// 1 for myPending task 2 for MyComplete Task 3 for MyRequest                    
                    
                    if (TypeID == 3)
                    {
                        var MyRequest = (from WI in db.WF_Instance
                                         where WI.WFId == Id && WI.InitiatorUser == UserId
                                         select WI.Id).ToList().Distinct();
                        WorkFlowAssignedLst = WorkFlowAssignedLst.Where(x => MyRequest.Contains(x.WFInstanceId)).ToList();
                    }
                    else
                    {
                        var StepOwner = CommonFunctions.GetStepOwner();
                        if (StepOwner == true && TypeID == 1)
                        {
                            var getcurrentstep = db.WF_Details.Where(x => x.WFId == Id && x.AssignedUserID == UserId).FirstOrDefault();
                            WorkFlowAssignedLst = WorkFlowAssignedLst.Where(x => x.IsOpen == IsOpen && x.CurrentStep == getcurrentstep.WFStep).ToList();
                        }
                        else
                        {
                            WorkFlowAssignedLst = WorkFlowAssignedLst.Where(x => x.IsOpen == IsOpen).ToList();
                        }
                        
                    }

                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            catch (Exception e)
            {
                throw;
            }
            return PartialView("_AssignedWorkflowList", WorkFlowAssignedLst);
        }

        public PartialViewResult _AssignedWorkflowInfo(long Id, long AssignFormTemplateId)
        {
            UserWorkflowAssignesInfo WorkFlowAssignedInfo = new UserWorkflowAssignesInfo();
            var CabinetId = CommonFunctions.GetCabinetId();
            var UserId = CommonFunctions.GetUserId();
            SortedList sl = new SortedList();
            DataTable dt = new DataTable();
            try
            {
                sl.Add("@CabinetId", CabinetId);
                sl.Add("@WFInstanceId", Id);
                dt = objDAL.GetDataTable("uspGetWorkFlowAssignedInfo", sl);
                WorkFlowAssignedInfo = objDAL.ConvertToList<UserWorkflowAssignesInfo>(dt).FirstOrDefault();

                WorkflowDynamicFormResponseVM frmData = new WorkflowDynamicFormResponseVM();
                AssignFormTemplate frm = new AssignFormTemplate();
                DynamicFormResponse response = new DynamicFormResponse();

                if (AssignFormTemplateId > 0)
                {
                    response = db.DynamicFormResponses.Where(x => x.FormTemplateId == AssignFormTemplateId
                                        && x.AttachedType == "WorkFlow" && x.AttachedId == Id
                                        /*&& x.CreatedBy == UserId.ToString()*/).OrderByDescending(x => x.ID).FirstOrDefault();

                    frm = db.AssignFormTemplates.Find(AssignFormTemplateId);
                    if (frm != null)
                    {
                        frmData.FormTemplateId = frm.Id;
                        frmData.HtmlContent = frm.FormContant;
                        frmData.FormTitle = frm.FormTitle;
                    }
                    if (response != null)
                    {
                        frmData.ID = response.ID;
                        frmData.FormTemplateId = response.FormTemplateId;
                        frmData.FormTitle = frm.FormTitle;
                        frmData.HtmlContent = frm.FormContant;
                        frmData.ResponseCode = response.ResponseCode;
                        frmData.AttachedId = response.AttachedId;
                        frmData.AttachedType = response.AttachedType;
                        frmData.CreatedBy = frm.CreatedBy;
                        frmData.SelectedFileId = Id;

                        var ResponseDetailList = db.DynamicFormResponses.Where(x => x.ResponseCode == response.ResponseCode).ToList();

                        if (ResponseDetailList != null && ResponseDetailList.Count > 0)
                        {
                            foreach (var item in ResponseDetailList)
                            {
                                WorkflowDynamicFormResponseDetailVM filedDetail = new WorkflowDynamicFormResponseDetailVM();
                                filedDetail.FiledId = item.FiledId;
                                filedDetail.Type = item.Type;
                                filedDetail.Value = item.Value;
                                filedDetail.IsSelected = item.IsSelected;
                                if (frmData != null)
                                {
                                    frmData.DynamicFormResponseDetailList.Add(filedDetail);
                                }
                            }
                        }
                    }
                    WorkFlowAssignedInfo.DynamicFormResponse = frmData;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return PartialView("_AssignedWorkflowInfo", WorkFlowAssignedInfo);
        }

        [HttpGet, Route("WorkflowTimeLine")]
        public ActionResult WorkflowTimeLine(WorkFlowTimeLine detail)
        {
            WorkflowTimeLineDetail wfTimeLine = new WorkflowTimeLineDetail();
            var CabinetId = CommonFunctions.GetCabinetId();
            var UserId = CommonFunctions.GetUserId();
            SortedList sl = new SortedList();
            DataTable dt = new DataTable();
            try
            {
                wfTimeLine.IsAssignedToMe = false;
                wfTimeLine.Type = detail.Type;
                wfTimeLine.CabinetName = Session[SessionClass.CabinetName].ToString();
                wfTimeLine.IsOpen = detail.IsOpen;
                var wf = db.WF_Master.Find(detail.WFId);
                if (wf != null)
                {
                    wfTimeLine.WFId = wf.Id;
                    wfTimeLine.WFName = wf.WFName;
                }
                var wfInst = db.WF_Instance.Find(detail.WFIntId);
                wfTimeLine.IsInitiatorUser = false;
                if (wfInst != null)
                {
                    wfTimeLine.WFInstanceId = wfInst.Id;
                    wfTimeLine.WFInitiateName = wfInst.WFInitiateName;
                    wfTimeLine.IsStart = Convert.ToBoolean(wfInst.IsStart);
                    if (UserId == wfInst.InitiatorUser)
                    {
                        wfTimeLine.IsAssignedToMe = true;
                        wfTimeLine.IsInitiatorUser = true;
                    }

                    // Add by Kiran || 01022021
                    var Initiate = db.LoginCredentials.Where(x => x.Id == wfInst.InitiatorUser).FirstOrDefault();
                    wfTimeLine.InitiateBy = "";
                    if (Initiate != null)
                    {
                        if (Initiate.Id == UserId)
                        {
                            wfTimeLine.InitiateBy = "Me";
                        }
                        else
                        {
                            wfTimeLine.InitiateBy = Initiate.FirstName + " " + Initiate.LastName;
                        }
                    }
                    wfTimeLine.InitiateDate = wfInst.StartDateTime.ToString("dd, MMM yyyy hh:mm tt");
                    var transection = db.WF_Transactions.Where(x => x.WFInstanceId == wfInst.Id).OrderBy(x => x.Id).FirstOrDefault();
                    wfTimeLine.ToUser = "";
                    if (transection != null)
                    {
                        var touser = db.LoginCredentials.Where(x => x.Id == transection.ToUser).FirstOrDefault();
                        if (touser != null)
                        {
                            if (touser.Id == UserId)
                            {
                                wfTimeLine.ToUser = "Me";
                            }
                            else
                            {
                                wfTimeLine.ToUser = touser.FirstName + " " + touser.LastName;
                            }
                        }
                    }
                }


                wfTimeLine.IsAdmin = false;
                var wfadmin = db.WF_ProcessAdmin.Where(x => x.WFId == detail.WFId && x.UserId == UserId).FirstOrDefault();
                if (wfadmin != null)
                {
                    wfTimeLine.IsAdmin = true;
                }

                wfTimeLine.IsCloser = false;
                var wfCloser = db.WF_Close.Where(x => x.WFId == detail.WFId && x.UserId == UserId).FirstOrDefault();
                if (wfCloser != null)
                {
                    wfTimeLine.IsCloser = true;
                }

                sl.Add("@WFInstanceId", detail.WFIntId);
                sl.Add("@WFId", detail.WFId);
                sl.Add("@UserId", UserId);
                dt = objDAL.GetDataTable("uspGetWorkFlowTimeLine", sl);
                wfTimeLine.WorkFlowTimeLine = objDAL.ConvertToList<WorkflowTimeLineInfo>(dt).ToList();

                var count = 0;
                wfTimeLine.currentStep = 0;
                foreach (var item in wfTimeLine.WorkFlowTimeLine)
                {

                    WorkflowTimeSendTo data = new WorkflowTimeSendTo();
                    if (item.WFStep != 1)
                    {
                        count++;
                        data.StepId = count;
                        data.StepName = item.SendTo;
                        wfTimeLine.WorkflowTimeSendTo.Add(data);
                    }

                    if ((item.AssignedTo.ToLower() == "me" && item.IsCurrentStep && !item.IsSubsidiary))
                    {
                        wfTimeLine.currentStep = item.WFStep;
                        wfTimeLine.IsAssignedToMe = true;
                    }
                }


            }
            catch (Exception e)
            {
                throw e;
            }

            //try
            //{
            //    wfTimeLine.CabinetName = Session[SessionClass.CabinetName].ToString();
            //}
            //catch (Exception e)
            //{
            //    throw e;
            //}
            #region Add Folder Structure Detail || By kiran || 09022021

            var folder = db.WF_FolderDetail.Where(x => x.WFInstanceId == wfTimeLine.WFInstanceId).FirstOrDefault();
            if (folder != null)
            {
                wfTimeLine.FolderId = folder.WFFolderDetailId;
                wfTimeLine.LastOpenFolderId = folder.ParentFolderId;
                wfTimeLine.FolderName = folder.Name;
                wfTimeLine.CabinetName = Session[SessionClass.CabinetName].ToString();
            }


            UserWorkflowAssignesInfo WorkFlowAssignedInfo = new UserWorkflowAssignesInfo();

            SortedList sl1 = new SortedList();
            DataTable dt1 = new DataTable();
            try
            {
                //sl.Add("@CabinetId", CabinetId);
                //sl.Add("@WFInstanceId", Id);
                //dt = objDAL.GetDataTable("uspGetWorkFlowAssignedInfo", sl);
                //WorkFlowAssignedInfo = objDAL.ConvertToList<UserWorkflowAssignesInfo>(dt).FirstOrDefault();

                WorkflowDynamicFormResponseVM frmData = new WorkflowDynamicFormResponseVM();
                AssignFormTemplate frm = new AssignFormTemplate();
                DynamicFormResponse response = new DynamicFormResponse();

                var WF_MasterDetail = db.WF_Master.Where(x => x.Id == detail.WFId).FirstOrDefault();

                //if (AssignFormTemplateId > 0)
                if (WF_MasterDetail.AssignFormTemplateId > 0)
                {
                    response = db.DynamicFormResponses.Where(x => x.FormTemplateId == WF_MasterDetail.AssignFormTemplateId
                                        && x.AttachedType == "WorkFlow" && x.AttachedId == detail.WFIntId
                                        /*&& x.CreatedBy == UserId.ToString()*/).OrderByDescending(x => x.ID).FirstOrDefault();

                    frm = db.AssignFormTemplates.Find(WF_MasterDetail.AssignFormTemplateId);
                    if (frm != null)
                    {
                        frmData.FormTemplateId = frm.Id;
                        frmData.HtmlContent = frm.FormContant;
                        frmData.FormTitle = frm.FormTitle;
                    }
                    if (response != null)
                    {
                        frmData.ID = response.ID;
                        frmData.FormTemplateId = response.FormTemplateId;
                        frmData.FormTitle = frm.FormTitle;
                        frmData.HtmlContent = frm.FormContant;
                        frmData.ResponseCode = response.ResponseCode;
                        frmData.AttachedId = response.AttachedId;
                        frmData.AttachedType = response.AttachedType;
                        frmData.CreatedBy = frm.CreatedBy;
                        frmData.SelectedFileId = detail.WFId;

                        var ResponseDetailList = db.DynamicFormResponses.Where(x => x.ResponseCode == response.ResponseCode).ToList();

                        if (ResponseDetailList != null && ResponseDetailList.Count > 0)
                        {
                            foreach (var item in ResponseDetailList)
                            {
                                WorkflowDynamicFormResponseDetailVM filedDetail = new WorkflowDynamicFormResponseDetailVM();
                                filedDetail.FiledId = item.FiledId;
                                filedDetail.Type = item.Type;
                                filedDetail.Value = item.Value;
                                filedDetail.IsSelected = item.IsSelected;
                                if (frmData != null)
                                {
                                    frmData.DynamicFormResponseDetailList.Add(filedDetail);
                                }
                            }
                        }
                    }
                    //WorkFlowAssignedInfo.DynamicFormResponse = frmData;
                    wfTimeLine.WorkFlowAssignedInfo.WFInstanceId = detail.WFIntId;
                    wfTimeLine.WorkFlowAssignedInfo.DynamicFormResponse = frmData;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            #endregion
            return View(wfTimeLine);
        }

        [HttpPost, Route("WorkFlowSaveFormTemplate")]
        public ActionResult WorkFlowSaveFormTemplate(IEnumerable<HttpPostedFileBase> fileuploadData, FormCollection _fc)
        {
            try
            {
                long cabinetId = CommonFunctions.GetCabinetId();
                WorkflowDynamicFormResponseVM obj = new WorkflowDynamicFormResponseVM();
                var ob = _fc["DynamicFormResponseVM"];
                if (_fc["DynamicFormResponseVM"] != null)
                {
                    var dynamicResponse = _fc["DynamicFormResponseVM"];
                    obj = JsonConvert.DeserializeObject<WorkflowDynamicFormResponseVM>(dynamicResponse);
                }

                UtilityCommonFunctions.RandomStringGenerator random = new UtilityCommonFunctions.RandomStringGenerator();
                string newResponseCode = random.GetRandomString(5, UtilityCommonFunctions.RandomStringGenerator.ALPHANUMERIC_CAPS.ToCharArray());
                string filename = string.Empty;
                filename = obj.AttachedType + "_" + newResponseCode;

                List<DynamicFormResponse> responselst = new List<DynamicFormResponse>();
                List<DynamicFormResponseHistory> responsehistorylst = new List<DynamicFormResponseHistory>();

                var resp = db.DynamicFormResponses.Where(x => x.FormTemplateId == obj.FormTemplateId
                                        && x.AttachedType == "WorkFlow" && x.AttachedId == obj.AttachedId
                                        /*&& x.CreatedBy == UserId.ToString()*/).ToList();

                db.DynamicFormResponses.RemoveRange(resp);

                foreach (var res in obj.DynamicFormResponseDetailList)
                {
                    DynamicFormResponse response = new DynamicFormResponse();                    

                    response.FormTemplateId = obj.FormTemplateId;
                    response.ResponseCode = newResponseCode;
                    response.AttachedType = obj.AttachedType; // should be  Folder/File/Workflow 
                    response.AttachedId = obj.AttachedId; //  should be workflow attached ID                   
                    response.FiledId = res.FiledId;
                    response.Type = res.Type;
                    response.Value = res.Value;
                    response.IsSelected = (res.Value == null || res.Value == "" ? false : true);// res.IsSelected;
                    response.CreatedBy = CommonFunctions.GetUserId().ToString();
                    response.CreatedDate = DateTime.Now;
                    responselst.Add(response);

                    #region Add Hisotry table || Kiran || 03032021

                    var CurrentStep = 0;
                    var wfdetail = db.WF_Instance.Where(x => x.Id == obj.AttachedId && x.IsStart == true).FirstOrDefault();
                    if (wfdetail != null)
                    {
                        CurrentStep = wfdetail.CurrentStep;
                    }

                    DynamicFormResponseHistory responseHistory = new DynamicFormResponseHistory();
                    responseHistory.FormTemplateId = obj.FormTemplateId;
                    responseHistory.ResponseCode = newResponseCode;
                    responseHistory.AttachedType = obj.AttachedType; // should be  Folder/File/Workflow 
                    responseHistory.AttachedId = obj.AttachedId; //  should be workflow attached ID                   
                    responseHistory.FiledId = res.FiledId;
                    responseHistory.Type = res.Type;
                    responseHistory.Value = res.Value;
                    responseHistory.WFStep = CurrentStep;
                    responseHistory.IsSelected = (res.Value == null || res.Value == "" ? false : true);// res.IsSelected;
                    responseHistory.CreatedBy = CommonFunctions.GetUserId().ToString();
                    responseHistory.CreatedDate = DateTime.Now;
                    responsehistorylst.Add(responseHistory);

                    #endregion
                }

                db.DynamicFormResponses.AddRange(responselst);
                db.SaveChanges();

                db.DynamicFormResponseHistories.AddRange(responsehistorylst);
                db.SaveChanges();

                #region Upload Attached Files
                if (fileuploadData != null)
                {
                    //iterating through multiple file collection   
                    foreach (HttpPostedFileBase file in fileuploadData)
                    {
                        //Checking file is available to save.  
                        if (file != null)
                        {
                            var attachFileName = Path.GetFileName(file.FileName);
                            var uriPath = ConfigurationManager.AppSettings["PathForAttachDocUpload"] + cabinetId.ToString() + "/" + newResponseCode + "/";
                            string folderPath = Server.MapPath(uriPath);
                            if (!Directory.Exists(folderPath))
                            {
                                Directory.CreateDirectory(folderPath.ToString());
                            }
                            try
                            {
                                file.SaveAs(folderPath + attachFileName);
                            }
                            catch
                            {
                                throw;
                            }
                        }
                    }
                }
                #endregion
                return Json(true);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [Route("SaveWorkFlowStep")]
        public JsonResult SaveWorkFlowStep(SaveWorkFlowStep detail)
        {
            try
            {
                WF_Transactions wfTrans = new WF_Transactions();
                long cabinetId = CommonFunctions.GetCabinetId();
                long userId = CommonFunctions.GetUserId();
                wfTrans = db.WF_Transactions.Find(detail.WFTransactionId);
                if (wfTrans != null)
                {
                    if (detail.WFActionId == (int)EnumList.EnumActionType.Save)
                    {
                        wfTrans.Notes = detail.Notes;
                        wfTrans.ActionId = detail.WFActionId;
                        wfTrans.ActionDate = DateTime.Now;
                        db.SaveChanges();
                    }
                    else
                    {
                        wfTrans.Notes = detail.Notes;
                        wfTrans.ActionId = detail.WFActionId;
                        wfTrans.IsCurrentStep = false;
                        wfTrans.EndDateTime = DateTime.Now;
                        db.SaveChanges();

                        var wfInstance = db.WF_Instance.Find(detail.WFIntId);
                        var wfDetail = db.WF_Details.FirstOrDefault(x => x.WFId == detail.WFId && x.WFStep == (detail.WFStep + 1));
                        if ((detail.WFActionId == (int)EnumList.EnumActionType.Decline ||
                                detail.WFActionId == (int)EnumList.EnumActionType.Close) &&
                                wfInstance != null)
                        {
                            wfInstance.CloseUser = userId;
                            wfInstance.IsOpen = false;
                            wfInstance.EndDateTime = DateTime.Now;
                            db.SaveChanges();
                        }
                        else if (wfDetail != null)
                        {
                            if (detail.WFActionId != (int)EnumList.EnumActionType.Decline ||
                                detail.WFActionId != (int)EnumList.EnumActionType.Close)
                            {
                                var subdetail = db.LoginCredentials.Find(wfDetail.AssignedUserID);

                                WF_Transactions wfNextStepTrans = db.WF_Transactions.FirstOrDefault(x => x.WFStep == wfDetail.WFStep
                                                    && x.WFInstanceId == detail.WFIntId);
                                if (wfNextStepTrans == null)
                                {
                                    wfNextStepTrans = new WF_Transactions();
                                    wfNextStepTrans.WFInstanceId = detail.WFIntId;
                                    wfNextStepTrans.WFStep = wfDetail.WFStep;
                                    wfNextStepTrans.FromUser = userId;
                                    wfNextStepTrans.ToUser = wfDetail.AssignedUserID;
                                    wfNextStepTrans.SubsidiaryId = (subdetail != null && subdetail.AssignSubsidiary) ? subdetail.SubsidiaryUserId : 0;
                                    wfNextStepTrans.ActionId = 0;
                                    wfNextStepTrans.IsCurrentStep = true;
                                    wfNextStepTrans.StartDateTime = DateTime.Now;
                                    db.WF_Transactions.Add(wfNextStepTrans);
                                    db.SaveChanges();
                                    try
                                    {
                                        #region Save Notification from WorkFlow || Kiran Sawant || 02032021                                    

                                        var isnotification = db.LoginCredentials.Where(x => x.Id == wfDetail.AssignedUserID).FirstOrDefault();
                                        if (isnotification.SentNotification == true)
                                        {

                                            SaveNotificationWorkFlowVM send = new SaveNotificationWorkFlowVM();
                                            send.UserId = userId;
                                            send.CabinetId = cabinetId;
                                            send.NotificationType = (int)EnumList.EnumNotificationType.Workflow;
                                            send.Comment = string.Empty;
                                            send.@SendUserId = wfDetail.AssignedUserID.ToString();
                                            send.@InstanceId = detail.WFIntId;
                                            CommonFunctions.SaveNotificationWorkFlow(send);
                                        }
                                        #endregion

                                        #region Send Email from WorkFlow || Kiran Sawant || 03032021   
                                        var fromEmail = WebConfigurationManager.AppSettings["SendEmailInfo"].ToString();

                                        var senderDetail = db.LoginCredentials.Where(x => x.Id == userId).FirstOrDefault();
                                        var SenderFirstName = senderDetail.FirstName;
                                        var SenderLastName = senderDetail.LastName;

                                        var ReceiverDetail = db.LoginCredentials.Where(x => x.Id == wfDetail.AssignedUserID).FirstOrDefault();
                                        var FullName = ReceiverDetail.FirstName + ' ' + ReceiverDetail.LastName;

                                        Dictionary<string, string> replacement = new Dictionary<string, string>();
                                        replacement.Add("#FullName#", FullName);
                                        replacement.Add("#SenderFirstName#", SenderFirstName);
                                        replacement.Add("#SenderLastName#", SenderLastName);
                                        replacement.Add("#WorkflowName#", wfInstance.WFInitiateName);
                                        replacement.Add("#WorkFlowAction#", EnumList.EnumActionType.SaveAndSend.ToString());
                                        replacement.Add("#WorkFlowStepNotes#", detail.Notes);

                                        bool f = UtilityCommonFunctions.SendEmailTemplete((int)EnumList.EmailTemplate.WorkFlowRequest, replacement, ReceiverDetail.EmailId, fromEmail);
                                        //bool f = UtilityCommonFunctions.SendEmailTemplete((int)EnumList.EmailTemplate.WorkFlowRequest, replacement, "kirans@webmynesystems.com", fromEmail);

                                        #endregion
                                    }
                                    catch
                                    { }
                                }
                                else
                                {
                                    wfNextStepTrans.IsCurrentStep = true;
                                    db.SaveChanges();
                                }
                            }
                            if (wfInstance != null)
                            {
                                wfInstance.CurrentStep = wfDetail.WFStep;
                                db.SaveChanges();
                            }
                        }
                        else if (detail.WFActionId == (int)EnumList.EnumActionType.Approve && wfInstance != null)
                        {
                            wfInstance.CloseUser = userId;
                            wfInstance.IsOpen = false;
                            wfInstance.EndDateTime = DateTime.Now;
                            db.SaveChanges();
                        }
                    }
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [Route("RequestModificationWorkFlowStep")]
        public JsonResult RequestModificationWorkFlowStep(SaveWorkFlowStep detail)
        {
            try
            {
                WF_Transactions wfTrans = new WF_Transactions();
                long cabinetId = CommonFunctions.GetCabinetId();
                long userId = CommonFunctions.GetUserId();
                wfTrans = db.WF_Transactions.Find(detail.WFTransactionId);
                if (wfTrans != null && wfTrans.WFStep != 1)
                {
                    wfTrans.Notes = detail.Notes;
                    wfTrans.ActionId = detail.WFActionId;
                    wfTrans.IsCurrentStep = false;
                    wfTrans.ActionDate = DateTime.Now;
                    db.SaveChanges();

                    var wfInstance = db.WF_Instance.Find(detail.WFIntId);
                    if (wfInstance != null)
                    {
                        wfInstance.CurrentStep = wfInstance.CurrentStep != 1 ? (int)detail.WFStep - 1 : wfInstance.CurrentStep;
                        db.SaveChanges();
                    }

                    var wfPreviousTrans = db.WF_Transactions.FirstOrDefault(x => x.WFInstanceId == detail.WFIntId
                                             && x.WFStep == wfInstance.CurrentStep);
                    if (wfPreviousTrans != null)
                    {
                        wfPreviousTrans.IsCurrentStep = true;
                        db.SaveChanges();
                    }

                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [Route("SaveManualWorkFlowNextStep")]
        public JsonResult SaveManualWorkFlowNextStep(SaveWorkFlowStep detail)
        {
            try
            {
                WF_Transactions wfTrans = new WF_Transactions();
                long cabinetId = CommonFunctions.GetCabinetId();
                long userId = CommonFunctions.GetUserId();
                wfTrans = db.WF_Transactions.Find(detail.WFTransactionId);
                if (wfTrans != null)
                {
                    if (detail.WFActionId == (int)EnumList.EnumActionType.Save)
                    {
                        wfTrans.Notes = detail.Notes;
                        wfTrans.ActionId = detail.WFActionId;
                        wfTrans.ActionDate = DateTime.Now;
                        db.SaveChanges();
                    }
                    else
                    {
                        wfTrans.Notes = detail.Notes;
                        wfTrans.ActionId = detail.WFActionId;
                        wfTrans.IsCurrentStep = false;
                        wfTrans.EndDateTime = DateTime.Now;
                        db.SaveChanges();

                        var wfInstance = db.WF_Instance.Find(detail.WFIntId);
                        if ((detail.WFActionId == (int)EnumList.EnumActionType.Decline ||
                                detail.WFActionId == (int)EnumList.EnumActionType.Close ||
                                detail.WFActionId == (int)EnumList.EnumActionType.Approve) &&
                                wfInstance != null)
                        {
                            wfInstance.CloseUser = userId;
                            wfInstance.IsOpen = false;
                            wfInstance.EndDateTime = DateTime.Now;
                            db.SaveChanges();
                        }
                        else if (wfInstance != null)
                        {
                            var subdetail = db.LoginCredentials.Find(detail.WFAssignedTo);
                            var wfNextStepTrans = new WF_Transactions();
                            wfNextStepTrans.WFInstanceId = detail.WFIntId;
                            wfNextStepTrans.WFNameForManual = detail.WFStepName;
                            wfNextStepTrans.WFActionForManual = detail.WFActions;
                            wfNextStepTrans.WFStep = (int)detail.WFStep + 1;
                            wfNextStepTrans.FromUser = userId;
                            wfNextStepTrans.ToUser = detail.WFAssignedTo;
                            wfNextStepTrans.SubsidiaryId = (subdetail != null && subdetail.AssignSubsidiary) ? subdetail.SubsidiaryUserId : 0;
                            wfNextStepTrans.ActionId = 0;
                            wfNextStepTrans.IsCurrentStep = true;
                            wfNextStepTrans.StartDateTime = DateTime.Now;
                            db.WF_Transactions.Add(wfNextStepTrans);
                            db.SaveChanges();

                            wfInstance.CurrentStep = (int)detail.WFStep + 1;
                            db.SaveChanges();
                        }
                    }
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region -->  Work Flow Initaite 

        [HttpGet, Route("UserWorkInitaite")]
        public ActionResult UserWorkInitaite()
        {
            UserWorkInitaiteVM wf = new UserWorkInitaiteVM();
            //List<UserWorkflowListVM> workflowVMs = new List<UserWorkflowListVM>();

            var CabinetId = CommonFunctions.GetCabinetId();
            var UserId = CommonFunctions.GetUserId();
            SortedList sl = new SortedList();
            DataTable dt = new DataTable();
            try
            {
                wf.CabinetName = Session[SessionClass.CabinetName].ToString();
            }
            catch (Exception e)
            {
                throw e;
            }
            return View(wf);
        }


        //public ActionResult UserWorkInitaite(UserWorkInitaiteVM initaiteVM)
        [HttpPost, Route("UserWorkInitaite")]
        public ActionResult UserWorkInitaite(UserWorkflowAssignesVM initaiteVM)
        {
            UserWorkInitaiteVM wf = new UserWorkInitaiteVM();

            var CabinetId = CommonFunctions.GetCabinetId();
            var UserId = CommonFunctions.GetUserId();
            SortedList sl = new SortedList();
            DataTable dt = new DataTable();
            try
            {
                long instacneid = 0;
                wf.CabinetName = Session[SessionClass.CabinetName].ToString();
                var date = db.WF_Master.Where(x => x.Id == initaiteVM.WFId && x.IsManual == true).FirstOrDefault();
                if (date != null && date.Id > 0)
                {
                    //var count = 0;
                    //var action = "";
                    //foreach (var item in initaiteVM.WFActionForManual)
                    //{
                    //    count += 1;
                    //    action += item + ",";
                    //}
                    //if (!string.IsNullOrEmpty(initaiteVM.WFNameForManual) && count > 0)
                    //{
                    //    // Flow Manual Insert data (table are WF_Instance,WF_Transactions)
                    //    WF_Instance instance = new WF_Instance();
                    //    instance.WFId = initaiteVM.WFId;
                    //    instance.WFInitiateName = initaiteVM.WFName;
                    //    instance.CurrentStep = 1;
                    //    instance.InitiatorUser = UserId;
                    //    instance.IsOpen = true;
                    //    instance.Priority = initaiteVM.Priority;
                    //    instance.StartDateTime = System.DateTime.Now;
                    //    db.WF_Instance.Add(instance);
                    //    db.SaveChanges();

                    //    WF_Transactions transactions = new WF_Transactions();
                    //    transactions.WFInstanceId = instance.Id;
                    //    transactions.WFNameForManual = initaiteVM.WFNameForManual;
                    //    transactions.WFActionForManual = action != "" ? action.Substring(0, action.Length - 1) : action;
                    //    transactions.WFStep = 1;
                    //    transactions.FromUser = UserId;
                    //    transactions.ToUser = initaiteVM.ToUser;
                    //    transactions.SubsidiaryId = 0;
                    //    transactions.ActionId = 3;
                    //    transactions.IsCurrentStep = true;
                    //    transactions.StartDateTime = System.DateTime.Now;
                    //    db.WF_Transactions.Add(transactions);
                    //    db.SaveChanges();
                    //}

                }
                else
                {
                    var wfDatail = db.WF_Details.Where(x => x.WFId == initaiteVM.WFId && x.WFStep == 1).FirstOrDefault();

                    //Flow Automated Insert data(table are WF_Instance, WF_Transactions)
                    WF_Instance instance = new WF_Instance();
                    instance.WFId = initaiteVM.WFId;
                    instance.WFInitiateName = initaiteVM.WFInitiateName;
                    instance.CurrentStep = 1;
                    instance.InitiatorUser = UserId;
                    instance.IsOpen = true;
                    instance.Priority = initaiteVM.Priority;
                    instance.StartDateTime = System.DateTime.Now;
                    instance.IsStart = false;
                    instance.IsOutSource = (Int16)EnumList.EnumInstanceType.FromDocware;
                    db.WF_Instance.Add(instance);
                    db.SaveChanges();

                    WF_Transactions transactions = new WF_Transactions();
                    transactions.WFInstanceId = instance.Id;
                    transactions.WFStep = 1;
                    transactions.FromUser = UserId;
                    transactions.ToUser = wfDatail.AssignedUserID;
                    transactions.SubsidiaryId = 0;
                    transactions.ActionId = 3;
                    transactions.IsCurrentStep = true;
                    transactions.StartDateTime = System.DateTime.Now;
                    db.WF_Transactions.Add(transactions);
                    db.SaveChanges();
                    instacneid = instance.Id;
                }

                //When the intiate that time need to add folder id based on intiate
                if (instacneid > 0)
                {
                    WF_FolderDetail folderDetail = new WF_FolderDetail();
                    string UDID = CommonFunctions.GetUDIDForFolderWorkFlow();

                    folderDetail.UDID = UDID;
                    folderDetail.WFInstanceId = instacneid;
                    folderDetail.ParentFolderId = 0;
                    folderDetail.AssignFormTemplateId = 0;
                    folderDetail.CabinetId = CabinetId;
                    folderDetail.Name = initaiteVM.WFInitiateName.Trim();
                    folderDetail.Notes = "";
                    folderDetail.Keywords = "";
                    folderDetail.Description = "";
                    folderDetail.PhysicalLocation = "";
                    folderDetail.IsPublic = false;
                    folderDetail.IsActive = true;
                    folderDetail.IsDelete = false;
                    folderDetail.CreatedBy = UserId.ToString();
                    folderDetail.CreatedDate = DateTime.Now;
                    db.WF_FolderDetail.Add(folderDetail);
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            //return RedirectToAction("UserWorkFlowList");
            return RedirectToAction("AssignedWorkflowList", new { Id = initaiteVM.WFId });
        }

        public JsonResult IsWFNameExist(string WFName, long WFId)
        {
            var Wfname = WFName;
            bool IsValid = false;
            long? Id = WFId;
            try
            {
                if (WFId > 0)
                {
                    if (Wfname != null)
                    {
                        //var SecId = db.WF_Instance.Where(x => x.WFInitiateName.ToLower().Trim() == Wfname.ToLower().Trim() && x.WFId == Id).Select(x => x.Id).FirstOrDefault();
                        var SecId = db.WF_Instance.Where(x => x.WFInitiateName.Contains(Wfname.Trim()) && x.WFId == Id).Select(x => x.Id).FirstOrDefault();
                        if (SecId > 0)
                        {
                            IsValid = false;
                            return Json(IsValid, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            IsValid = true;
                            return Json(IsValid, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                IsValid = true;
                return Json(IsValid, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(IsValid, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet, Route("CheckWFIsManual")]
        public JsonResult CheckWFIsManual(long WfId)
        {
            bool IsValid = false;
            long? Id = WfId;
            try
            {
                if (WfId > 0)
                {
                    var wfid = db.WF_Master.Where(x => x.Id == Id && x.IsManual == true).Select(x => x.Id).FirstOrDefault();
                    if (wfid > 0)
                    {
                        IsValid = true;
                        return Json(IsValid, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        IsValid = false;
                        return Json(IsValid, JsonRequestBehavior.AllowGet);
                    }
                }
                IsValid = false;
                return Json(IsValid, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(IsValid, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet, Route("WorkInitaite")]
        public ActionResult WorkInitaite(long Id)
        {
            UserWorkInitaiteVM wf = new UserWorkInitaiteVM();
            //List<UserWorkflowListVM> workflowVMs = new List<UserWorkflowListVM>();

            var CabinetId = CommonFunctions.GetCabinetId();
            var UserId = CommonFunctions.GetUserId();
            //SortedList sl = new SortedList();
            //DataTable dt = new DataTable();
            try
            {
                wf.CabinetName = Session[SessionClass.CabinetName].ToString();
            }
            catch (Exception e)
            {
                throw e;
            }
            var folder = db.FolderDetails.Find(20012);
            if (folder != null)
            {
                wf.FolderId = folder.Id;
                //LastOpenFolderId = folder.ls,
                wf.FolderName = folder.Name;
                wf.CabinetName = Session[SessionClass.CabinetName].ToString();
            }


            UserWorkflowAssignesInfo WorkFlowAssignedInfo = new UserWorkflowAssignesInfo();

            SortedList sl = new SortedList();
            DataTable dt = new DataTable();
            try
            {
                //sl.Add("@CabinetId", CabinetId);
                //sl.Add("@WFInstanceId", Id);
                //dt = objDAL.GetDataTable("uspGetWorkFlowAssignedInfo", sl);
                //WorkFlowAssignedInfo = objDAL.ConvertToList<UserWorkflowAssignesInfo>(dt).FirstOrDefault();

                WorkflowDynamicFormResponseVM frmData = new WorkflowDynamicFormResponseVM();
                AssignFormTemplate frm = new AssignFormTemplate();
                DynamicFormResponse response = new DynamicFormResponse();

                var WF_MasterDetail = db.WF_Master.Where(x => x.Id == Id).FirstOrDefault();

                //if (AssignFormTemplateId > 0)
                if (WF_MasterDetail.AssignFormTemplateId > 0)
                {
                    response = db.DynamicFormResponses.Where(x => x.FormTemplateId == WF_MasterDetail.AssignFormTemplateId
                                        && x.AttachedType == "WorkFlow" && x.AttachedId == Id
                                        /*&& x.CreatedBy == UserId.ToString()*/).OrderByDescending(x => x.ID).FirstOrDefault();

                    frm = db.AssignFormTemplates.Find(WF_MasterDetail.AssignFormTemplateId);
                    if (frm != null)
                    {
                        frmData.FormTemplateId = frm.Id;
                        frmData.HtmlContent = frm.FormContant;
                        frmData.FormTitle = frm.FormTitle;
                    }
                    if (response != null)
                    {
                        frmData.ID = response.ID;
                        frmData.FormTemplateId = response.FormTemplateId;
                        frmData.FormTitle = frm.FormTitle;
                        frmData.HtmlContent = frm.FormContant;
                        frmData.ResponseCode = response.ResponseCode;
                        frmData.AttachedId = response.AttachedId;
                        frmData.AttachedType = response.AttachedType;
                        frmData.CreatedBy = frm.CreatedBy;
                        frmData.SelectedFileId = Id;

                        var ResponseDetailList = db.DynamicFormResponses.Where(x => x.ResponseCode == response.ResponseCode).ToList();

                        if (ResponseDetailList != null && ResponseDetailList.Count > 0)
                        {
                            foreach (var item in ResponseDetailList)
                            {
                                WorkflowDynamicFormResponseDetailVM filedDetail = new WorkflowDynamicFormResponseDetailVM();
                                filedDetail.FiledId = item.FiledId;
                                filedDetail.Type = item.Type;
                                filedDetail.Value = item.Value;
                                filedDetail.IsSelected = item.IsSelected;
                                if (frmData != null)
                                {
                                    frmData.DynamicFormResponseDetailList.Add(filedDetail);
                                }
                            }
                        }
                    }
                    //WorkFlowAssignedInfo.DynamicFormResponse = frmData;
                    wf.WorkFlowAssignedInfo.DynamicFormResponse = frmData;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return View(wf);
        }

        #endregion

        #region --> Folder List 

        [HttpPost, Route("AddFolderWorkFlow")]
        [AuditLog(EnumList.EnumLogType.SaveData, "AddFolderWorkFlow")]
        public ActionResult AddFolderWorkFlow(FolderDetailVM data)
        {
            WF_FolderDetail folderDetail = new WF_FolderDetail();

            try
            {
                long userId = CommonFunctions.GetUserId();
                long CabinetId = CommonFunctions.GetCabinetId();

                using (db = new DBEntities())
                {
                    if (data.Id > 0)
                    {
                        using (var transaction = db.Database.BeginTransaction())
                        {
                            folderDetail = db.WF_FolderDetail.Where(m => m.WFFolderDetailId == data.Id).FirstOrDefault();

                            folderDetail.AssignFormTemplateId = data.FolAssignFormTemplateId;
                            folderDetail.ParentFolderId = data.ParentFolderId;
                            folderDetail.Name = data.Name.Trim();
                            folderDetail.Notes = data.Notes != null ? data.Notes.Trim() : "";
                            folderDetail.Keywords = data.Keywords != null ? data.Keywords : "";
                            folderDetail.Description = data.Description;
                            folderDetail.PhysicalLocation = data.PhysicalLocation;
                            folderDetail.IsPublic = data.PublicFolder;
                            folderDetail.UpdatedBy = userId.ToString();
                            folderDetail.UpdatedDate = DateTime.Now;
                            db.Entry(folderDetail).State = EntityState.Modified;
                            db.SaveChanges();

                            //if (CommonFunctions.HasChild(folderDetail.Id) > 0)
                            //{
                            //    ChangeChildPublicStatus1(folderDetail.Id, folderDetail.IsPublic);
                            //}
                            transaction.Commit();
                            //if (!data.PublicFolder)
                            //{
                            //    CommonFunctions.RemoveLinkedDocument();
                            //    CommonFunctions.RemoveFavouriteDocument();
                            //}                            
                        }
                        return Json(new { success = true, msg = Messages.Folder_Updated }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        using (var transaction = db.Database.BeginTransaction())
                        {
                            string UDID = CommonFunctions.GetUDIDForFolderWorkFlow();

                            if (data.ParentFolderId > 0)
                            {
                                var parentFolder = db.WF_FolderDetail.Find(data.ParentFolderId);

                                if (parentFolder != null)
                                {
                                    folderDetail.WFInstanceId = parentFolder.WFInstanceId;
                                    if (parentFolder.IsPublic)
                                    {
                                        data.PublicFolder = true;
                                    }
                                }
                            }

                            folderDetail.UDID = UDID;
                            folderDetail.ParentFolderId = data.ParentFolderId;
                            folderDetail.AssignFormTemplateId = data.FolAssignFormTemplateId;
                            folderDetail.CabinetId = CabinetId;
                            folderDetail.Name = data.Name.Trim();
                            folderDetail.Notes = data.Notes != null ? data.Notes.Trim() : "";
                            folderDetail.Keywords = data.Keywords != null ? data.Keywords : "";
                            folderDetail.Description = data.Description;
                            folderDetail.PhysicalLocation = data.PhysicalLocation;
                            folderDetail.IsPublic = data.PublicFolder;
                            folderDetail.IsActive = true;
                            folderDetail.IsDelete = false;
                            folderDetail.CreatedBy = userId.ToString();
                            folderDetail.CreatedDate = DateTime.Now;
                            db.WF_FolderDetail.Add(folderDetail);
                            db.SaveChanges();

                            transaction.Commit();
                        }
                        return Json(new { success = true, msg = Messages.Folder_Added }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost, Route("AddDocumentWorkFlow")]
        [AuditLog(EnumList.EnumLogType.SaveData, "AddDocumentWorkFlow")]
        public ActionResult AddDocumentWorkFlow(DocumentDetailVM data, HttpPostedFileBase docUpload)
        {
            WF_DocumentDetail documentDetail = new WF_DocumentDetail();
            var Name = string.Empty;
            var UDID = string.Empty;
            var Uploadfile = new { Name = string.Empty, Size = "0 KB" };
            try
            {
                using (db = new DBEntities())
                {
                    long userId = CommonFunctions.GetUserId();
                    long cabinetId = CommonFunctions.GetCabinetId();

                    if (data.DocumentId > 0 && data.DocumentVersionId > 0)
                    {
                        documentDetail = db.WF_DocumentDetail.Where(j => j.WFDocumentId == data.DocumentId).FirstOrDefault();

                        documentDetail.Name = data.DocName.Trim();
                        documentDetail.CategoryId = data.CategoryId;
                        documentDetail.SubCategoryId = data.SubCategoryId;
                        documentDetail.AssignFormTemplateId = data.DocAssignFormTemplateId;
                        documentDetail.UpdatedBy = userId.ToString();
                        documentDetail.UpdatedDate = DateTime.Now;
                        db.Entry(documentDetail).State = EntityState.Modified;
                        db.SaveChanges();

                        var documentVersion = db.WF_DocumentVersionDetail.Where(j => j.WFDocumentId == data.DocumentId).ToList();
                        if (documentVersion.Count != 0)
                        {
                            foreach (var dv in documentVersion)
                            {
                                if (dv.IsCurrent)
                                {
                                    dv.Notes = data.DocNotes != null ? data.DocNotes : "";
                                    dv.Keywords = data.DocKeywords != null ? data.DocKeywords : "";
                                    dv.Language = data.Language;
                                    dv.Confidentiality = Convert.ToByte(data.Confidentiality);
                                    dv.IsPublic = data.DocPublic;
                                    dv.Description = data.DocDescription; // Add  By Sweta On 22/05/2015
                                    dv.PhysicalLocation = data.DocPhysicalLocation;// Add  By Sweta On 22/05/2015
                                    dv.UpdatedBy = userId.ToString();
                                    dv.UpdatedDate = DateTime.Now;
                                }
                                else
                                {
                                    dv.IsPublic = data.DocPublic;
                                }
                                db.Entry(dv).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                        //if (!data.DocPublic)
                        //{
                        //    CommonFunctions.RemoveLinkedDocument();
                        //    CommonFunctions.RemoveFavouriteDocument();
                        //}
                    }
                    else if (data.DocumentId > 0)
                    {
                        var documentVersion = db.WF_DocumentVersionDetail.Where(j => j.WFDocumentId == data.DocumentId).OrderByDescending(x => x.Id).FirstOrDefault();

                        if (documentVersion != null)
                        {
                            var lastVersion = documentVersion.DocumentVersion;

                            data.DocumentVersion = lastVersion + Convert.ToDouble(0.1);
                        }
                        else
                        {
                            data.DocumentVersion = Convert.ToDouble(1.0);
                        }
                    }
                    else
                    {
                        UDID = CommonFunctions.GetUDIDForDocumentWorkFlow();

                        var parentFolder = db.WF_FolderDetail.Find(data.DocParentFolderId);

                        if (parentFolder != null)
                        {
                            if (parentFolder.IsPublic)
                            {
                                data.DocPublic = true;
                            }
                        }


                        documentDetail.UDID = UDID;
                        documentDetail.WF_FolderId = data.DocParentFolderId;
                        documentDetail.Name = data.DocName.Trim();
                        documentDetail.CategoryId = data.CategoryId;
                        documentDetail.SubCategoryId = data.SubCategoryId;
                        documentDetail.AssignFormTemplateId = data.DocAssignFormTemplateId;
                        documentDetail.IsActive = true;
                        documentDetail.IsDelete = false;
                        documentDetail.IsCheckOut = false;
                        documentDetail.CreatedBy = userId.ToString();
                        documentDetail.CreatedDate = DateTime.Now;
                        documentDetail.IsCheckOut = false;
                        documentDetail.WFStep = data.currentStep;
                        db.WF_DocumentDetail.Add(documentDetail);
                        db.SaveChanges();

                        if (data.DocumentVersion == 0)
                            data.DocumentVersion = Convert.ToDouble(1.0);
                    }

                    if (docUpload != null)
                    {
                        Name = documentDetail.UDID + "_" + data.DocumentVersion;
                        ////path for workflow document is WorkFlow/WF_ID/WF_InstanceID/UserID/

                        var instancedetail = db.WF_Instance.Find(data.WFInstanceId);

                        //var uriPath = ConfigurationManager.AppSettings["PathForDocUpload"] + "/WorkFlow/" + instancedetail.InitiatorUser.ToString() + "/" + cabinetId.ToString() + "/" + instancedetail.Id + "/";
                        var uriPath = ConfigurationManager.AppSettings["PathForDocUpload"] + "WorkFlow/" + instancedetail.WFId.ToString() + "/" + instancedetail.Id + "/" + userId.ToString() + "/";
                        string folderPath = Server.MapPath(uriPath);

                        if (!Directory.Exists(folderPath))
                        {
                            Directory.CreateDirectory(folderPath.ToString());
                        }
                        try
                        {
                            docUpload.SaveAs(folderPath + Name + Path.GetExtension(docUpload.FileName));
                        }
                        catch
                        {
                            throw;
                        }

                        var docSize = Math.Round((Convert.ToDecimal(docUpload.ContentLength) / Convert.ToDecimal(1024)), 2); //size in KB

                        WF_DocumentVersionDetail documentVersionDetail = new WF_DocumentVersionDetail();
                        documentVersionDetail.WFDocumentId = documentDetail.WFDocumentId;
                        documentVersionDetail.DocumentName = Name;
                        documentVersionDetail.Notes = data.DocNotes != null ? data.DocNotes : "";
                        documentVersionDetail.Keywords = data.DocKeywords != null ? data.DocKeywords : "";
                        documentVersionDetail.DocumentType = Path.GetExtension(docUpload.FileName).Split('.')[1];
                        documentVersionDetail.DocumentSize = docSize;
                        documentVersionDetail.DocumentDisplaySize = docSize > 1024 ? (docSize / 1024).ToString("0.00") + " MB" : docSize.ToString() + " KB";
                        documentVersionDetail.DocumentVersion = data.DocumentVersion;
                        documentVersionDetail.Language = data.Language;
                        documentVersionDetail.Confidentiality = Convert.ToByte(data.Confidentiality);
                        documentVersionDetail.IsPublic = data.DocPublic;
                        documentVersionDetail.DocumentPath = uriPath + Name + Path.GetExtension(docUpload.FileName);
                        documentVersionDetail.IsCurrent = true;
                        documentVersionDetail.IsActive = true;
                        documentVersionDetail.IsDelete = false;
                        documentVersionDetail.Description = data.DocDescription; // Add  By Sweta On 22/05/2015
                        documentVersionDetail.PhysicalLocation = data.DocPhysicalLocation;// Add  By Sweta On 22/05/2015
                        documentVersionDetail.CreatedBy = userId.ToString();
                        documentVersionDetail.CreatedDate = DateTime.Now;
                        db.WF_DocumentVersionDetail.Add(documentVersionDetail);
                        db.SaveChanges();

                        Uploadfile = new
                        {
                            Name = docUpload.FileName,
                            Size = documentVersionDetail.DocumentDisplaySize
                        };
                    }
                }

                if (data.DocumentId > 0)
                    return Json(new { success = true, msg = "File Updated Successfully", uploadData = Uploadfile }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = true, msg = "File Uploaded Successfully", uploadData = Uploadfile }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult checkIsNotEmptyWorkFlow(long id = 0)
        {
            DBEntities db = new DBEntities();
            if (id > 0)
            {
                var userId = CommonFunctions.GetUserId().ToString();
                var child = db.WF_FolderDetail.Where(x => !x.IsDelete && x.IsActive && x.ParentFolderId == id).ToList().Count;

                if (child == 0)
                {
                    child = db.WF_DocumentDetail.Where(x => x.WF_FolderId == id && !x.IsDelete && x.IsActive).Count();
                }

                if (child > 0)
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public PartialViewResult _FolderListWorkFlow(long folderId, string SearchKeyword = null, long instanceid = 0)
        {
            List<FolderFileVM> folderDetails = new List<FolderFileVM>();
            try
            {
                //if (CommonFunctions.IsCreatedBy(folderId, false)) --Commented this but not able to remember the case.
                //{
                long CabinetId = CommonFunctions.GetCabinetId();
                long UserId = CommonFunctions.GetUserId();
                SortedList sl = new SortedList();
                DataTable dt = new DataTable();

                sl.Add("@cabinetId", CabinetId);
                sl.Add("@folderId", folderId);
                sl.Add("@userId", UserId);
                sl.Add("@WFInstanceId", instanceid);

                if (!string.IsNullOrWhiteSpace(SearchKeyword))
                    sl.Add("@searchword", SearchKeyword.Trim());

                dt = objDAL.GetDataTable("uspGetFolderDetailsforWorkFlow", sl);
                folderDetails = objDAL.ConvertToList<FolderFileVM>(dt);
                //}
            }
            catch (Exception e)
            {
                throw;
            }
            return PartialView("_FolderListWorkFlow", folderDetails);
        }

        public JsonResult GetFolderNameWorkFlow(long folderId)
        {
            if (folderId > 0)
            {
                var folderDetail = db.WF_FolderDetail.Where(j => j.WFFolderDetailId == folderId).Select(x => new
                {
                    folderId = x.WFFolderDetailId,
                    folderName = x.Name,
                    parentFolder = x.ParentFolderId
                }).FirstOrDefault();

                return Json(new { success = true, folderData = folderDetail }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckFolderNameWorkFlow(string FolderName, long FolderID, long ParentFolderID)
        {
            List<WF_FolderDetail> folderDetails = new List<WF_FolderDetail>();
            try
            {
                using (db = new DBEntities())
                {
                    long CabinetId = CommonFunctions.GetCabinetId();

                    if (FolderID > 0)
                    {
                        folderDetails = db.WF_FolderDetail.Where(j => j.Name.ToLower().Trim() == FolderName.ToLower().Trim() && j.CabinetId == CabinetId && j.ParentFolderId == ParentFolderID && j.WFFolderDetailId != FolderID).ToList();
                    }
                    else
                    {
                        folderDetails = db.WF_FolderDetail.Where(j => j.Name.ToLower().Trim() == FolderName.ToLower().Trim() && j.CabinetId == CabinetId && j.ParentFolderId == ParentFolderID).ToList();
                    }

                    if (folderDetails.Count() > 0)
                    {
                        return Json(false, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(true, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult FileCheckinOutWorkFlow(long Id, string[] folderIds, string[] fileIds, bool status)
        {
            long DocumentId = 0;
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    long userId = CommonFunctions.GetUserId();
                    long cabinetId = CommonFunctions.GetCabinetId();

                    //for Files
                    if (fileIds != null && fileIds.Length > 0)
                    {
                        DocumentId = Convert.ToInt64(fileIds[0]);
                        var IsDiscard = db.WF_CheckInOutHistory.Where(x => x.WFDocumentId == DocumentId).FirstOrDefault();
                        var docVersion = db.WF_DocumentVersionDetail.Where(x => x.WFDocumentId == DocumentId && x.IsCurrent == true).FirstOrDefault();


                        if (status == true) // Go for Check in
                        {
                            using (db = new DBEntities())
                            {
                                var ChckHistory = db.WF_CheckInOutHistory.Find(Id);
                                ChckHistory.CheckInBy = userId.ToString();
                                ChckHistory.CheckInDate = DateTime.Now;
                                ChckHistory.WFDocumentVersionId = docVersion.Id;
                                ChckHistory.IsDiscard = false;
                                db.SaveChanges();

                                var ischkOut = db.WF_DocumentDetail.Where(x => x.WFDocumentId == DocumentId).FirstOrDefault();
                                ischkOut.IsCheckOut = false;
                                db.SaveChanges();
                                transaction.Commit();
                            }

                        }
                        else  // Go for Check out
                        {
                            using (db = new DBEntities())
                            {
                                var docDetail = db.WF_DocumentDetail.Find(DocumentId);
                                if (docDetail != null)
                                {
                                    WF_CheckInOutHistory chkHistory = new WF_CheckInOutHistory
                                    {
                                        WFDocumentId = DocumentId,
                                        CabinetId = cabinetId,
                                        WFFolderId = docDetail.WF_FolderId,
                                        CheckOutBy = userId.ToString(),
                                        CheckOutDate = DateTime.Now,
                                        CheckInBy = null,
                                        CheckInDate = null,
                                        WFDocumentVersionId = 0,
                                    };
                                    db.WF_CheckInOutHistory.Add(chkHistory);

                                    docDetail.IsCheckOut = true;
                                    db.SaveChanges();

                                    transaction.Commit();
                                    return Json(status, JsonRequestBehavior.AllowGet);
                                }
                                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                            }

                        }
                    }
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        #region --> Check Permission for document checkout by user | Add | Dhrumil Patel | 25-07-2018
        public JsonResult IsDocumentCheckOutWorkFlow(long id = 0)
        {
            try
            {
                using (db = new DBEntities())
                {
                    if (id > 0)
                    {
                        if (!CommonFunctions.IsCheckOutWorkFlow(id))
                        {
                            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public JsonResult CheckOutPermissionWorkFlow(long Id)
        {
            long userId = CommonFunctions.GetUserId();
            var CheckOut = db.WF_CheckInOutHistory.Find(Id);
            if (CheckOut != null && CheckOut.CheckOutBy == userId.ToString())
            {
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetDocumentVersionWorkFlow(long id)
        {
            try
            {
                using (db = new DBEntities())
                {
                    if (id > 0)
                    {
                        var documentVersionVM = (from dd in db.WF_DocumentDetail
                                                 let dv = db.WF_DocumentVersionDetail.Where(j => j.Id == id && j.IsActive && !j.IsDelete).OrderByDescending(j => j.Id).FirstOrDefault()
                                                 where dd.IsActive == true && dd.IsDelete == false && dv.WFDocumentId == dd.WFDocumentId
                                                 select new DocumentDetailVM
                                                 {
                                                     DocumentVersionId = dv.Id,
                                                     DocumentId = dv.WFDocumentId,
                                                     DocParentFolderId = dd.WF_FolderId,
                                                     DocName = dd.Name,
                                                     DocumentVersion = dv.DocumentVersion,
                                                     DocumentType = dv.DocumentType
                                                 }).FirstOrDefault();
                        return Json(new { success = true, docData = documentVersionVM }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckVersionWorkFlow(double Version, long DocumentId)
        {
            List<WF_DocumentVersionDetail> folderDetails = new List<WF_DocumentVersionDetail>();
            try
            {
                using (db = new DBEntities())
                {
                    long CabinetId = CommonFunctions.GetCabinetId();

                    if (DocumentId > 0)
                    {
                        folderDetails = db.WF_DocumentVersionDetail.Where(j => j.DocumentVersion == Version && j.WFDocumentId == DocumentId).ToList();
                    }

                    if (folderDetails.Count() > 0)
                    {
                        return Json(false, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(true, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, Route("AddDocumentVersionWorkFlow")]
        [AuditLog(EnumList.EnumLogType.SaveData, "AddDocumentVersionWorkFlow")]
        public ActionResult AddDocumentVersionWorkFlow(DocumentDetailVM data, HttpPostedFileBase docversionUpload)
        {
            var lastdata = new WF_DocumentDetail();
            var lastversiondata = new WF_DocumentVersionDetail();
            var Name = string.Empty;
            var UDID = string.Empty;
            var Uploadfile = new { Name = string.Empty, Size = "0 KB", VersionId = string.Empty };
            try
            {
                using (db = new DBEntities())
                {
                    var notificationdetail = new SaveNotificationVM();
                    long userId = CommonFunctions.GetUserId();
                    long cabinetId = CommonFunctions.GetCabinetId();

                    if (docversionUpload != null)
                    {
                        var updateData = db.WF_DocumentVersionDetail.Where(x => x.WFDocumentId == data.DocumentId).ToList();
                        if (updateData.Count > 0)
                        {
                            var count = 0;
                            foreach (var items in updateData)
                            {
                                count++;
                                if (items.IsCurrent)
                                {
                                    items.IsCurrent = false;
                                    db.SaveChanges();
                                }
                                if (count == updateData.Count)
                                {
                                    lastdata = db.WF_DocumentDetail.Find(items.WFDocumentId);
                                    lastdata.UpdatedBy = userId.ToString();
                                    lastdata.UpdatedDate = DateTime.Now;
                                    db.SaveChanges();
                                    lastversiondata = items;
                                }
                            }
                        }

                        Name = lastdata.UDID + "_" + data.DocumentVersion;
                        //var uriPath = ConfigurationManager.AppSettings["PathForDocUpload"] + userId.ToString() + "/" + cabinetId.ToString() + "/";
                        var instancedetail = db.WF_Instance.Find(data.WFInstanceId);

                        ////path for workflow document is WorkFlow/WF_ID/WF_InstanceID/UserID/                       

                        //var uriPath = ConfigurationManager.AppSettings["PathForDocUpload"] + "/WorkFlow/" + instancedetail.InitiatorUser.ToString() + "/" + cabinetId.ToString() + "/" + instancedetail.Id + "/";
                        var uriPath = ConfigurationManager.AppSettings["PathForDocUpload"] + "WorkFlow/" + instancedetail.WFId.ToString() + "/" + instancedetail.Id + "/" + userId.ToString() + "/";

                        string folderPath = Server.MapPath(uriPath);

                        if (!Directory.Exists(folderPath))
                        {
                            Directory.CreateDirectory(folderPath.ToString());
                        }
                        try
                        {
                            docversionUpload.SaveAs(folderPath + Name + Path.GetExtension(docversionUpload.FileName));
                        }
                        catch
                        {
                            throw;
                        }

                        var docSize = Math.Round((Convert.ToDecimal(docversionUpload.ContentLength) / Convert.ToDecimal(1024)), 2); //size in KB

                        WF_DocumentVersionDetail documentVersionDetail = new WF_DocumentVersionDetail();
                        documentVersionDetail.WFDocumentId = lastversiondata.WFDocumentId;
                        documentVersionDetail.DocumentName = Name;
                        documentVersionDetail.Notes = data.DocNotes;
                        documentVersionDetail.Keywords = lastversiondata.Keywords;
                        documentVersionDetail.DocumentType = Path.GetExtension(docversionUpload.FileName).Split('.')[1];
                        documentVersionDetail.DocumentSize = docSize;
                        documentVersionDetail.DocumentDisplaySize = docSize > 1024 ? (docSize / 1024).ToString("0.00") + " MB" : docSize.ToString() + " KB";
                        documentVersionDetail.DocumentVersion = data.DocumentVersion;
                        documentVersionDetail.Language = lastversiondata.Language;
                        documentVersionDetail.Confidentiality = Convert.ToByte(lastversiondata.Confidentiality);
                        documentVersionDetail.IsPublic = lastversiondata.IsPublic;
                        //documentVersionDetail.IsLocked = lastversiondata.IsLocked;
                        documentVersionDetail.DocumentPath = uriPath + Name + Path.GetExtension(docversionUpload.FileName);
                        documentVersionDetail.IsCurrent = true;
                        documentVersionDetail.IsActive = true;
                        documentVersionDetail.IsDelete = false;
                        documentVersionDetail.Description = lastversiondata.Description;
                        documentVersionDetail.PhysicalLocation = lastversiondata.PhysicalLocation;
                        documentVersionDetail.CreatedBy = userId.ToString();
                        documentVersionDetail.CreatedDate = DateTime.Now;
                        db.WF_DocumentVersionDetail.Add(documentVersionDetail);
                        db.SaveChanges();

                        Uploadfile = new
                        {
                            Name = docversionUpload.FileName,
                            Size = documentVersionDetail.DocumentDisplaySize,
                            VersionId = documentVersionDetail.Id.ToString()
                        };

                        notificationdetail.UserId = userId;
                        notificationdetail.CabinetId = cabinetId;
                        notificationdetail.DocumentId = data.DocumentId;
                        notificationdetail.TypeFor = "File";
                        notificationdetail.NotificationType = (int)EnumList.EnumNotificationType.Version;
                        //CommonFunctions.SaveNotification(notificationdetail);

                        return Json(new { success = true, msg = "Version Uploaded Successfully", uploadData = Uploadfile }, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult EditFolderWorkFlow(long id)
        {
            FolderDetailVM folderDetailVM = new FolderDetailVM();
            try
            {
                using (db = new DBEntities())
                {
                    if (id > 0)
                    {
                        folderDetailVM = db.WF_FolderDetail.Where(j => j.WFFolderDetailId == id).Select(j => new FolderDetailVM
                        {
                            Id = j.WFFolderDetailId,
                            Name = j.Name,
                            FolAssignFormTemplateId = j.AssignFormTemplateId,
                            FolAssignFormTemplateName = j.AssignFormTemplateId > 0 ? db.AssignFormTemplates.Where(x => x.Id == j.AssignFormTemplateId).FirstOrDefault().FormTitle : string.Empty,
                            Notes = j.Notes,
                            Keywords = j.Keywords,
                            ParentFolderId = j.ParentFolderId,
                            Description = j.Description,
                            PhysicalLocation = j.PhysicalLocation,
                            PublicFolder = j.IsPublic
                        }).FirstOrDefault();

                        return Json(new { success = true, folderData = folderDetailVM }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        #region --> Upload dropzone files with defaults values | Add | Dhrumil Patel | 16052018
        [AuditLog(EnumList.EnumLogType.SaveData, "UploadDefaultDocumentWorkFlow")]
        public ActionResult UploadDefaultDocumentWorkFlow(long Id, long WFInstanceId, int currentStep)
        {
            bool isSavedSuccessfully = false;
            string fName = "";
            string nameFile = "";
            var UDID = CommonFunctions.GetUDIDForDocumentWorkFlow();
            var Name = UDID + "_1";
            var userId = CommonFunctions.GetUserId();
            decimal docSize = 0;
            var cabinetId = CommonFunctions.GetCabinetId();
            string path = "";
            try
            {
                foreach (string fileName in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[fileName];
                    UtilityCommonFunctions.RandomStringGenerator random = new UtilityCommonFunctions.RandomStringGenerator();
                    string rdmText = random.GetRandomString(4, UtilityCommonFunctions.RandomStringGenerator.ALPHANUMERIC_CAPS.ToCharArray());
                    fName = file.FileName;
                    nameFile = Path.GetFileNameWithoutExtension(fName) + "_" + rdmText;
                    if (file != null && file.ContentLength > 0)
                    {
                        //var uriPath = ConfigurationManager.AppSettings["PathForDocUpload"] + userId.ToString() + "/" + cabinetId.ToString() + "/";
                        ////path for workflow document is WorkFlow/WF_ID/WF_InstanceID/UserID/

                        var instancedetail = db.WF_Instance.Find(WFInstanceId);

                        //var uriPath = ConfigurationManager.AppSettings["PathForDocUpload"] + "/WorkFlow/" + instancedetail.InitiatorUser.ToString() + "/" + cabinetId.ToString() + "/" + instancedetail.Id + "/";
                        var uriPath = ConfigurationManager.AppSettings["PathForDocUpload"] + "WorkFlow/" + instancedetail.WFId.ToString() + "/" + instancedetail.Id + "/" + userId.ToString() + "/";
                        string folderPath = Server.MapPath(uriPath);

                        if (!Directory.Exists(folderPath))
                        {
                            Directory.CreateDirectory(folderPath.ToString());
                        }
                        try
                        {
                            path = folderPath + Name + Path.GetExtension(file.FileName);
                            file.SaveAs(path);
                        }
                        catch
                        {
                            throw;
                        }
                        docSize = Math.Round((Convert.ToDecimal(file.ContentLength) / Convert.ToDecimal(1024)), 2);
                        var parentFolder = db.FolderDetails.Find(Id);

                        var documentDetail = new WF_DocumentDetail();
                        documentDetail.UDID = UDID;
                        documentDetail.WF_FolderId = Id;
                        documentDetail.Name = nameFile;
                        documentDetail.CategoryId = 1;
                        documentDetail.SubCategoryId = 1;
                        documentDetail.AssignFormTemplateId = 0;
                        documentDetail.IsActive = true;
                        documentDetail.IsDelete = false;
                        documentDetail.IsCheckOut = false;
                        documentDetail.CreatedBy = userId.ToString();
                        documentDetail.CreatedDate = DateTime.Now;
                        documentDetail.IsCheckOut = false;
                        documentDetail.WFStep = currentStep;
                        db.WF_DocumentDetail.Add(documentDetail);
                        db.SaveChanges();

                        var documentVersion = new WF_DocumentVersionDetail();
                        documentVersion.Notes = "default";
                        documentVersion.Keywords = "default";
                        documentVersion.Language = 1;
                        documentVersion.Confidentiality = 0;
                        documentVersion.WFDocumentId = documentDetail.WFDocumentId;
                        documentVersion.DocumentName = Name;
                        documentVersion.DocumentType = Path.GetExtension(fName).Split('.')[1];
                        documentVersion.DocumentSize = docSize;
                        documentVersion.DocumentDisplaySize = docSize > 1024 ? (docSize / 1024).ToString("0.00") + " MB" : docSize.ToString("0.00") + " KB";
                        documentVersion.DocumentVersion = 1;
                        documentVersion.PhysicalLocation = "default";
                        documentVersion.Description = "default";
                        documentVersion.IsCurrent = true;
                        //documentVersion.IsLocked = false;
                        documentVersion.DocumentPath = uriPath + Name + Path.GetExtension(file.FileName);
                        documentVersion.IsActive = true;
                        documentVersion.IsDelete = false;
                        if (parentFolder != null)
                        {
                            if (parentFolder.IsPublic)
                            {
                                documentVersion.IsPublic = true;
                            }
                            else
                            {
                                documentVersion.IsPublic = false;
                            }
                        }
                        documentVersion.CreatedBy = userId.ToString();
                        documentVersion.CreatedDate = DateTime.Now;
                        db.WF_DocumentVersionDetail.Add(documentVersion);
                        db.SaveChanges();

                        isSavedSuccessfully = true;
                    }
                }
            }
            catch (Exception ex)
            {
                isSavedSuccessfully = false;
            }
            if (isSavedSuccessfully)
            {
                return Json(new { Message = "Success" });
            }
            else
            {
                return Json(new { Message = "Error in saving file" });
            }
        }
        #endregion

        public JsonResult StartWorkFlowProcess(long Id)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    long userId = CommonFunctions.GetUserId();
                    long cabinetId = CommonFunctions.GetCabinetId();

                    //for Files
                    if (Id > 0)
                    {
                        WF_Instance WF_Instance = new WF_Instance();
                        WF_Instance = db.WF_Instance.Where(x => x.Id == Id).FirstOrDefault();
                        if (WF_Instance != null)
                        {
                            WF_Instance.IsStart = true;
                            db.Entry(WF_Instance).State = EntityState.Modified;
                            db.SaveChanges();
                            transaction.Commit();

                            var wf_tra = db.WF_Transactions.Where(x => x.WFInstanceId == WF_Instance.Id).FirstOrDefault();

                            #region Save Notification from WorkFlow || Kiran Sawant || 02032021                                                                

                            var isnotification = db.LoginCredentials.Where(x => x.Id == wf_tra.ToUser).FirstOrDefault();
                            if (isnotification.SentNotification == true)
                            {
                                SaveNotificationWorkFlowVM send = new SaveNotificationWorkFlowVM();
                                send.UserId = userId;
                                send.CabinetId = cabinetId;
                                send.NotificationType = (int)EnumList.EnumNotificationType.Workflow;
                                send.Comment = string.Empty;
                                send.@SendUserId = wf_tra.ToUser.ToString();
                                send.@InstanceId = WF_Instance.Id;
                                CommonFunctions.SaveNotificationWorkFlow(send);

                            }
                            #endregion

                            #region Send Email from WorkFlow || Kiran Sawant || 03032021   
                            var fromEmail = WebConfigurationManager.AppSettings["SendEmailInfo"].ToString();

                            var wfDetail = db.WF_Details.Where(x => x.WFId == WF_Instance.WFId && x.WFStep == 1).FirstOrDefault();

                            var senderDetail = db.LoginCredentials.Where(x => x.Id == userId).FirstOrDefault();
                            var SenderFirstName = senderDetail.FirstName;
                            var SenderLastName = senderDetail.LastName;

                            var ReceiverDetail = db.LoginCredentials.Where(x => x.Id == wfDetail.AssignedUserID).FirstOrDefault();
                            var FullName = ReceiverDetail.FirstName + ' ' + ReceiverDetail.LastName;

                            Dictionary<string, string> replacement = new Dictionary<string, string>();
                            replacement.Add("#FullName#", FullName);
                            replacement.Add("#SenderFirstName#", SenderFirstName);
                            replacement.Add("#SenderLastName#", SenderLastName);
                            replacement.Add("#WorkflowName#", WF_Instance.WFInitiateName);
                            replacement.Add("#WorkFlowAction#", "");
                            replacement.Add("#WorkFlowStepNotes#", "");

                            bool f = UtilityCommonFunctions.SendEmailTemplete((int)EnumList.EmailTemplate.WorkFlowRequest, replacement, ReceiverDetail.EmailId, fromEmail);
                            //bool f = UtilityCommonFunctions.SendEmailTemplete((int)EnumList.EmailTemplate.WorkFlowRequest, replacement, "kirans@webmynesystems.com", fromEmail);

                            #endregion

                            
                            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                        }

                    }
                    return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public JsonResult checkIsNotEmptyDocument(long id = 0)
        {
            DBEntities db = new DBEntities();
            if (id > 0)
            {
                var userId = CommonFunctions.GetUserId().ToString();
                var child = 0;

                var FolderList = (from WI in db.WF_FolderDetail
                                  where !WI.IsDelete && WI.IsActive && WI.WFInstanceId == id
                                  select WI.WFFolderDetailId).ToList();
                if (FolderList.Count > 0)
                {
                    child = db.WF_DocumentDetail.Where(x => FolderList.Contains(x.WF_FolderId) && !x.IsDelete && x.IsActive).Count();
                }


                if (child > 0)
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion        

        public ActionResult DownloadFile(long DocumentId)
        {
            try
            {
                var uploadfile = string.Empty; var filePath = string.Empty;
                string fileimage = string.Empty;
                string contenttype = string.Empty;
                string imagename = string.Empty;
                if (DocumentId > 0)
                {
                    //uploadfile = (from urf in db.WF_DocumentVersionDetail where urf.WFDocumentId == DocumentId && urf.IsDelete != true select urf.DocumentPath).OrderByDescending(x => x.id).FirstOrDefault();
                    var docdetail = db.WF_DocumentVersionDetail.Where(x => x.WFDocumentId == DocumentId && x.IsDelete != true).FirstOrDefault();
                    if (docdetail != null)
                    {
                        string fullpath = Server.MapPath(docdetail.DocumentPath);
                        if (!System.IO.File.Exists(fullpath))
                        {
                            return null;
                        }
                        else
                        {
                            //var docdetail = db.WF_DocumentVersionDetail.Where(x => x.WFDocumentId == DocumentId && x.IsDelete != true).FirstOrDefault();
                            //fileimage = getimage + "/" + uploadfile; contenttype = "image/jpeg";
                            fileimage = fullpath;
                            contenttype = docdetail.DocumentType;
                            imagename = docdetail.DocumentName + "." + contenttype;
                        }
                    }

                }

                return File(fileimage, contenttype, imagename);
            }
            catch
            {
                return null;
            }
        }

        public JsonResult CheckDocumentExist(long DocumentId)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    long userId = CommonFunctions.GetUserId();
                    long cabinetId = CommonFunctions.GetCabinetId();

                    //for Files
                    if (DocumentId > 0)
                    {
                        //uploadfile = (from urf in db.WF_DocumentVersionDetail where urf.WFDocumentId == DocumentId && urf.IsDelete != true select urf.DocumentPath).OrderByDescending(x => x.id).FirstOrDefault();
                        var docdetail = db.WF_DocumentVersionDetail.Where(x => x.WFDocumentId == DocumentId && x.IsDelete != true).FirstOrDefault();
                        if (docdetail != null)
                        {
                            string fullpath = Server.MapPath(docdetail.DocumentPath);
                            if (!System.IO.File.Exists(fullpath))
                            {
                                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                            }
                        }

                    }
                    return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                }
            }
        }

    }

}