﻿using DocWare.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utility.Helpers;
using Utility.Models;
using DocWare.Helpers;

namespace DocWare.Controllers
{
    [CheckAuthorization]
    public class HomeController : BaseController
    {
        [HttpGet, Route("Dashboard")]
        public ActionResult Dashboard()
        {
            if (Session[SessionClass.CabinetID] != null)
            {
                long cabinetid = 0;
                cabinetid = Convert.ToInt64(Session[SessionClass.CabinetID]);

                if (cabinetid > 0)
                    return View();
                else
                    return RedirectToAction("Login", "Account");
            }
            return View();
        }

        #region --> Select Cabinet | GET | Add | Dhrumil Patel | 08032018
        [HttpPost, Route("SelectCabinet")]
        public ActionResult SelectCabinet(long Cabinetid)
        {
            try
            {
                long ApplicationId = Convert.ToInt64(Session[SessionClass.ApplicationID]);
                long dashboardlinkid = 0;
                using (DBEntities db = new DBEntities())
                {
                    if (ApplicationId == (long)EnumList.Application.User)
                    {
                        dashboardlinkid = db.Links.Where(x => x.Controller == "Home" && x.Action == "Dashboard" && x.ApplicationId == 3).Select(x => x.LinkId).FirstOrDefault();
                    }
                    else
                    {
                        dashboardlinkid = db.Links.Where(x => x.Controller == "Home" && x.Action == "Dashboard" && x.ApplicationId == 2).Select(x => x.LinkId).FirstOrDefault();
                    }
                }
                Session[SessionClass.CabinetID] = string.Empty;
                Session[SessionClass.CabinetID] = Cabinetid;

                string menu = RoleManagement.GetMenu();
                Session[SessionClass.Menu] = string.Empty;
                Session[SessionClass.Menu] = menu;
                Session[SessionClass.LinkPermissionList] = string.Empty;
                Session[SessionClass.LinkPermissionList] = RoleManagement.GetAllLinkPermission();
                if ((long)EnumList.Application.SuperAdmin == (long)Session[SessionClass.ApplicationID])
                {
                    Session[SessionClass.HomeScreen] = "Dashboard";
                    return Json(new { IsRedirect = "Dashboard" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                  
                    long roleId = Session[SessionClass.UserRoleID] != null ? (long)Session[SessionClass.UserRoleID] : 0;
                    long UserId = Session[SessionClass.UserID] != null ? (long)Session[SessionClass.UserID] : 0;
                    long CabinetId = CommonFunctions.GetCabinetId();
                    using (DBEntities db = new DBEntities())
                    {
                        Session[SessionClass.CabinetName] = string.Empty;
                        Session[SessionClass.CabinetName] = db.CabinetMasters.Find(Cabinetid).Name;

                       
                        List<long> roles = (List<long>)Session[SessionClass.RolePermissionList];
                        var roleLinks = db.RoleRights.Where(x => roles.Contains(x.RoleId) && x.CabinetId == CabinetId && x.LinkId == dashboardlinkid).ToList().Count();
                        var userLinks = db.UserRights.Where(x => x.UserId == UserId && x.CabinetId == CabinetId && x.LinkId == dashboardlinkid).ToList().Count();
                        if (roleLinks > 0 || userLinks > 0)
                        {
                            Session[SessionClass.HomeScreen] = "Dashboard";
                            return Json( new { IsRedirect= "Dashboard" }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            if (ApplicationId == (long)EnumList.Application.User)
                            {
                                Session[SessionClass.HomeScreen] = "CabinetFiles";
                                return Json(new { IsRedirect = "CabinetFiles" }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                Session[SessionClass.HomeScreen] = "Welcome";
                                return Json(new { IsRedirect = "Welcome" }, JsonRequestBehavior.AllowGet);
                            }
                          
                        }
                    }
                }
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion 
    }
}