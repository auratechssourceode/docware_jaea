﻿using DocWare.Helpers;
using DocWare.Models;
//using GleamTech.AspNet.UI;
//using GleamTech.FileUltimate.AspNet.UI;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utility.Helpers;
using Utility.Models;

namespace DocWare.Controllers
{
    public class DemoController : BaseController
    {
        DBEntities db = new DBEntities();
        DAL objDAL = new DAL();
        // GET: Demo
        public ActionResult Index()
        {
            return View();
        }

        // GET: Demo
        [HttpGet, Route("FormTemplateList")]
        public ActionResult FormTemplateList()
        {
            List<FormTemplate> Templates = new List<FormTemplate>();
            var userid = CommonFunctions.GetUserId();
            long cabinetId = CommonFunctions.GetCabinetId();
            try
            {
                #region --> Page Permission on Action and controller name | Jasmin | 21032018

                var permissionlst = Session[SessionClass.LinkPermissionList].ToString();
                List<UserPermissionVM> userPermissionLst = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserPermissionVM>>(permissionlst);

                PermissionVM permissionVM = new PermissionVM();

                permissionVM.HasAddPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "DynamicFormTemplate" && x.ControllerName.Trim() == "Demo").Count() > 0;
                permissionVM.HasEditPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "EditTemplate" && x.ControllerName.Trim() == "Demo").Count() > 0;
                permissionVM.HasStatusPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "ActiveDeactiveForm" && x.ControllerName.Trim() == "Demo").Count() > 0;
                permissionVM.HasActivePermission = userPermissionLst.Where(x => x.ActionName.Trim() == "ActiveSelectedFormTemplate" && x.ControllerName.Trim() == "Demo").Count() > 0;
                permissionVM.HasDeactivePermission = userPermissionLst.Where(x => x.ActionName.Trim() == "DeactiveSelectedFormTemplate" && x.ControllerName.Trim() == "Demo").Count() > 0;
                permissionVM.HasDeletePermission = userPermissionLst.Where(x => x.ActionName.Trim() == "DeleteSelectedFormTemplate" && x.ControllerName.Trim() == "Demo").Count() > 0;

                Session["PermissionListSession"] = permissionVM;
                #endregion

                using (db = new DBEntities())
                {
                    SortedList sl = new SortedList();
                    DataTable dt = new DataTable();
                    sl.Add("@CabinetId", cabinetId);
                    dt = objDAL.GetDataTable("uspGetFormTemplateListByCabinet", sl);
                    Templates = objDAL.ConvertToList<FormTemplate>(dt);
                    return View(Templates);
                }
            }
            catch
            {
                throw;
            }
        }

        [HttpGet, Route("DynamicFormTemplate"), CheckAuthorization]
        public ActionResult DynamicFormTemplate()
        {
            FormTemplateVM formTemplateVM = new FormTemplateVM();
            return View(formTemplateVM);
        }

        [HttpGet, Route("EditTemplate"), CheckAuthorization]
        public ActionResult EditTemplate(long id)
        {
            FormTemplateVM data = new FormTemplateVM();
            try
            {
                if (id > 0)
                {
                    data = db.FormTemplates.Where(x => x.Id == id).Select(x => new FormTemplateVM
                    {
                        FormTitle = x.FormTitle,
                        Description = x.Description,
                        Id = x.Id,
                        HtmlContent = x.HtmlContent
                    }).FirstOrDefault();

                    if (data != null)
                    {
                        var cabinetIds = db.FormCabinetDetails.Where(x => x.FormId == data.Id).Select(x => x.CabinetId).ToList();
                        var cabinetLst = db.CabinetMasters.Where(x => cabinetIds.Contains(x.Id) && x.IsActive && !x.IsDelete).Select(y => y.Id).ToArray();
                        List<string> strCabinet = new List<string>();
                        foreach (var ids in cabinetLst)
                        {
                            strCabinet.Add(ids.ToString());
                        }
                        data.CabinetList = strCabinet.ToArray();
                    }


                }
            }
            catch (Exception)
            {
                throw;
            }
            return View("DynamicFormTemplate", data);

        }

        [HttpPost, Route("DynamicFormTemplate")]
        [ValidateInput(false)]
        public ActionResult DynamicFormTemplate(FormTemplateVM dynamicFormVM, List<FormFieldVM> formFields)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    bool flag = false;
                    FormTemplate formdetail = new FormTemplate();
                    if (dynamicFormVM.Id == 0)
                    {
                        formdetail.FormTitle = dynamicFormVM.FormTitle;
                        formdetail.HtmlContent = dynamicFormVM.HtmlContent;
                        formdetail.Description = dynamicFormVM.Description;
                        formdetail.IsActive = true;
                        formdetail.IsDelete = false;
                        formdetail.CreatedBy = CommonFunctions.GetUserId().ToString();
                        formdetail.CreatedDate = DateTime.Now;
                        db.FormTemplates.Add(formdetail);
                        db.SaveChanges();
                        dynamicFormVM.Id = formdetail.Id;
                    }
                    else
                    {
                        formdetail = db.FormTemplates.Find(dynamicFormVM.Id);
                        if (formdetail != null)
                        {
                            flag = true;
                            formdetail.FormTitle = dynamicFormVM.FormTitle;
                            formdetail.HtmlContent = dynamicFormVM.HtmlContent;
                            formdetail.Description = dynamicFormVM.Description;
                            formdetail.UpdatedBy = CommonFunctions.GetUserId().ToString();
                            formdetail.UpdatedDate = DateTime.Now;
                            db.SaveChanges();
                        }
                    }

                    #region form cabinet detail insert/update
                    if (dynamicFormVM.CabinetList != null)
                    {
                        var formCabinet = db.FormCabinetDetails.Where(x => x.FormId == dynamicFormVM.Id).ToList();
                        if (formCabinet.Count() > 0)
                        {
                            db.FormCabinetDetails.RemoveRange(formCabinet);
                            db.SaveChanges();
                        }
                        FormCabinetDetail cabinetPermission = new FormCabinetDetail();
                        foreach (var cabinet in dynamicFormVM.CabinetList)
                        {
                            if (cabinet != "")
                            {
                                cabinetPermission.CabinetId = Convert.ToInt64(cabinet);
                                cabinetPermission.FormId = dynamicFormVM.Id;
                                db.FormCabinetDetails.Add(cabinetPermission);
                                db.SaveChanges();
                            }
                        }
                    }
                    #endregion

                    #region form fields add/update

                    var formField = db.FormControlProperties.Where(j => j.FormTemplateId == dynamicFormVM.Id).ToList();
                    if (formField.Count() > 0)
                    {
                        db.FormControlProperties.RemoveRange(formField);
                        db.SaveChanges();
                    }

                    if (formFields != null)
                    {
                        FormControlProperty formControlProperty = new FormControlProperty();
                        foreach (var field in formFields)
                        {
                            if (field != null)
                            {
                                formControlProperty.FormTemplateId = dynamicFormVM.Id;
                                formControlProperty.FieldId = field.FieldID;
                                formControlProperty.FieldType = field.FieldType;
                                db.FormControlProperties.Add(formControlProperty);
                                db.SaveChanges();
                            }
                        }
                    }
                    #endregion

                    if (!flag)
                    {
                        TempData["SuccessMSG"] = "Form added successfully";
                    }
                    else
                    {
                        TempData["SuccessMSG"] = "Form updated successfully";
                    }
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    TempData["ErrorMSG"] = "Error occur while updating form detail.";
                    //return RedirectToAction("FormTemplateList");
                    return Json(false);
                }
                transaction.Commit();
            }
            return Json(true);
            //return RedirectToAction("FormTemplateList");
        }

        [HttpGet]
        public PartialViewResult _DynamicControl(string ID, string Type, string ControlID)
        {
            DynamicControlVM dynamicControl = new DynamicControlVM();
            dynamicControl.ID = ID;
            dynamicControl.Type = Type;
            dynamicControl.ControlID = ControlID;


            return PartialView(dynamicControl != null ? dynamicControl : new DynamicControlVM());
        }

        // GET: Demo
        public ActionResult DynamicFormTemplate2()
        {

            return View();
        }

        [HttpPost]
        public ActionResult DynamicFormTemplate2(HttpPostedFileBase fileName1)
        {
            DynamicFormVM dynamicFormVM = new DynamicFormVM();
            return View(dynamicFormVM);
        }

        #region Clone form template | Add | Dhrumil Patel | 13-04-2018
        [Route("CloneFormTemplate")]
        public ActionResult CloneFormTemplate(long Id)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (Id != 0)
                    {
                        var formdetail = db.FormTemplates.Find(Id);
                        if (formdetail != null)
                        {
                            var count = db.FormTemplates.Where(x => x.FormTitle.Contains(formdetail.FormTitle)).ToList().Count();
                            if (count == 1)
                            {
                                formdetail.FormTitle = formdetail.FormTitle + "_Copy";
                                db.FormTemplates.Add(formdetail);
                            }
                            else
                            {
                                formdetail.FormTitle = formdetail.FormTitle + "_Copy" + (count - 1).ToString();
                                db.FormTemplates.Add(formdetail);
                            }
                            db.SaveChanges();

                            var formCabinetDetail = db.FormCabinetDetails.Where(x => x.FormId == Id).ToList();
                            foreach (var detail in formCabinetDetail)
                            {
                                detail.FormId = formdetail.Id;
                            }
                            db.FormCabinetDetails.AddRange(formCabinetDetail);
                            db.SaveChanges();

                            var formFields = db.FormControlProperties.Where(x => x.FormTemplateId == Id).ToList();
                            foreach (var detail in formFields)
                            {
                                detail.FormTemplateId = formdetail.Id;
                            }
                            db.FormControlProperties.AddRange(formFields);
                            db.SaveChanges();

                            TempData["SuccessMSG"] = "Form cloned successfully.";
                        }
                    }
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    TempData["ErrorMSG"] = "Error occur while cloning form detail.";
                    return RedirectToAction("FormTemplateList");
                }
                transaction.Commit();
            }
            return RedirectToAction("FormTemplateList");
        }
        #endregion

        #region Active/Deactive/Restore form template | Add | Dhrumil Patel | 13-04-2018
        public JsonResult ActiveDeactiveForm(long Id)
        {
            try
            {
                if (Id != 0)
                {
                    using (db = new DBEntities())
                    {
                        var result = db.FormTemplates.Where(x => x.Id == Id).FirstOrDefault();
                        if (result != null)
                        {
                            if (result.IsActive == true)
                            {
                                result.IsActive = false;
                            }
                            else
                            {
                                result.IsActive = true;
                            }
                            db.Entry(result).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                        }
                        else
                        {
                            return Json(false, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpGet, Route("DeletedFormTemplateList")]
        public ActionResult DeletedFormTemplateList()
        {
            List<FormTemplate> Templates = new List<FormTemplate>();
            var userid = CommonFunctions.GetUserId();
            long cabinetId = CommonFunctions.GetCabinetId();
            try
            {
                #region --> Page Permission on Action and controller name | Jasmin | 22032018

                var permissionlst = Session[SessionClass.LinkPermissionList].ToString();
                List<UserPermissionVM> userPermissionLst = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserPermissionVM>>(permissionlst);

                PermissionVM permissionVM = new PermissionVM();

                permissionVM.HasRestorePermisssion = userPermissionLst.Where(x => x.ActionName.Trim() == "RestoreSelectedFormTemplate" && x.ControllerName.Trim() == "Demo").Count() > 0;

                Session["PermissionListSession"] = permissionVM;
                #endregion

                using (db = new DBEntities())
                {
                    SortedList sl = new SortedList();
                    DataTable dt = new DataTable();
                    sl.Add("@CabinetId", cabinetId);
                    sl.Add("@Mode", "Delete");
                    dt = objDAL.GetDataTable("uspGetFormTemplateListByCabinet", sl);
                    Templates = objDAL.ConvertToList<FormTemplate>(dt);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(Templates);
        }

        [AuditLog(EnumList.EnumLogType.DeleteData, "DeleteSelectedFormTemplate")]
        public JsonResult DeleteSelectedFormTemplate(string[] formTemplateIds)
        {
            FormTemplate formTemplateDetail = new FormTemplate();
            try
            {
                if (formTemplateIds != null)
                {
                    if (formTemplateIds.Length > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();
                            foreach (var item in formTemplateIds)
                            {
                                long id = Convert.ToInt64(item);

                                formTemplateDetail = db.FormTemplates.Where(m => m.Id == id).FirstOrDefault();

                                formTemplateDetail.IsActive = false;
                                formTemplateDetail.IsDelete = true;
                                formTemplateDetail.UpdatedBy = userId.ToString();
                                formTemplateDetail.UpdatedDate = DateTime.Now;
                                db.Entry(formTemplateDetail).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "DeactiveSelectedFormTemplate")]
        public JsonResult DeactiveSelectedFormTemplate(string[] formTemplateIds)
        {
            FormTemplate formTemplateDetail = new FormTemplate();
            try
            {
                if (formTemplateIds != null)
                {
                    if (formTemplateIds.Length > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();
                            foreach (var item in formTemplateIds)
                            {
                                long id = Convert.ToInt64(item);

                                formTemplateDetail = db.FormTemplates.Where(m => m.Id == id).FirstOrDefault();

                                formTemplateDetail.IsActive = false;
                                formTemplateDetail.UpdatedBy = userId.ToString();
                                formTemplateDetail.UpdatedDate = DateTime.Now;
                                db.Entry(formTemplateDetail).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "ActiveSelectedFormTemplate")]
        public JsonResult ActiveSelectedFormTemplate(string[] formTemplateIds)
        {
            FormTemplate formTemplates = new FormTemplate();
            try
            {
                if (formTemplateIds != null)
                {
                    if (formTemplateIds.Length > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();

                            foreach (var item in formTemplateIds)
                            {
                                long id = Convert.ToInt64(item);

                                formTemplates = db.FormTemplates.Where(m => m.Id == id).FirstOrDefault();

                                formTemplates.IsActive = true;
                                formTemplates.UpdatedBy = userId.ToString();
                                formTemplates.UpdatedDate = DateTime.Now;
                                db.Entry(formTemplates).State = EntityState.Modified;
                                db.SaveChanges();

                            }
                            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "RestoreSelectedFormTemplate")]
        public JsonResult RestoreSelectedFormTemplate(string[] formTemplateIds)
        {
            FormTemplate formTemplates = new FormTemplate();
            try
            {
                if (formTemplateIds != null)
                {
                    if (formTemplateIds.Length > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();

                            foreach (var item in formTemplateIds)
                            {
                                long id = Convert.ToInt64(item);

                                formTemplates = db.FormTemplates.Where(m => m.Id == id).FirstOrDefault();

                                formTemplates.IsActive = true;
                                formTemplates.IsDelete = false;
                                formTemplates.UpdatedBy = userId.ToString();
                                formTemplates.UpdatedDate = DateTime.Now;
                                db.Entry(formTemplates).State = EntityState.Modified;
                                db.SaveChanges();

                            }
                            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        public ActionResult ViewForm(long id)
        {
            try
            {
                DynamicFormResponseVM frmData = new DynamicFormResponseVM();
                FormTemplate frm = new FormTemplate();
                if (id > 0)
                {
                    frm = db.FormTemplates.Find(id);
                    if (frm != null)
                    {
                        frmData.FormTemplateId = frm.Id;
                        frmData.HtmlContent = frm.HtmlContent;
                        frmData.FormTitle = frm.FormTitle;
                        return View(frmData);
                    }
                }

            }
            catch (Exception e)
            {
                throw e;
            }
            return View(new FormTemplate());
        }
        [HttpPost]
        public ActionResult ViewForm(IEnumerable<HttpPostedFileBase> fileuploadData, FormCollection _fc)
        {
            try
            {
                long cabinetId = CommonFunctions.GetCabinetId();
                DynamicFormResponseVM obj = new DynamicFormResponseVM();
                var ob = _fc["DynamicFormResponseVM"];
                if (_fc["DynamicFormResponseVM"] != null)
                {
                    var dynamicResponse = _fc["DynamicFormResponseVM"];
                    obj = JsonConvert.DeserializeObject<DynamicFormResponseVM>(dynamicResponse);
                }

                UtilityCommonFunctions.RandomStringGenerator random = new UtilityCommonFunctions.RandomStringGenerator();
                string newResponseCode = random.GetRandomString(5, UtilityCommonFunctions.RandomStringGenerator.ALPHANUMERIC_CAPS.ToCharArray());
                string filename = string.Empty;
                filename = obj.AttachedType + "_" + newResponseCode;

                List<DynamicFormResponse> responselst = new List<DynamicFormResponse>();

                foreach (var res in obj.DynamicFormResponseDetailList)
                {
                    DynamicFormResponse response = new DynamicFormResponse();
                    response.FormTemplateId = obj.FormTemplateId;
                    response.ResponseCode = newResponseCode;
                    response.AttachedType = "Folder";// obj.AttachedType; // should be  Folder/File/Workflow 
                    response.AttachedId = obj.AttachedId;//  should be workflow attached ID                   
                    response.FiledId = res.FiledId;
                    response.Type = res.Type;
                    response.Value = res.Value;
                    response.IsSelected = (res.Value == null || res.Value == "" ? false : true);// res.IsSelected;
                    response.CreatedBy = CommonFunctions.GetUserId().ToString();
                    response.CreatedDate = DateTime.Now;
                    responselst.Add(response);
                }

                db.DynamicFormResponses.AddRange(responselst);
                db.SaveChanges();

                #region Upload Attached Files
                if (fileuploadData != null)
                {
                    //iterating through multiple file collection   
                    foreach (HttpPostedFileBase file in fileuploadData)
                    {
                        //Checking file is available to save.  
                        if (file != null)
                        {
                            var attachFileName = Path.GetFileName(file.FileName);
                            var uriPath = ConfigurationManager.AppSettings["PathForAttachDocUpload"] + cabinetId.ToString() + "/" + newResponseCode + "/" ;
                            string folderPath = Server.MapPath(uriPath);
                            if (!Directory.Exists(folderPath))
                            {
                                Directory.CreateDirectory(folderPath.ToString());
                            }
                            try
                            {
                                file.SaveAs(folderPath + attachFileName);
                            }
                            catch
                            {
                                throw;
                            }
                        }
                    }
                }
                #endregion

                var redirectUrl = new UrlHelper(Request.RequestContext).Action("FormTemplateList", "Demo");
                return Json(new { Url = redirectUrl });
            }
            catch (Exception e)
            {
                throw e;
            }
            // return View(new FormTemplate());
        }

        public ActionResult ResponseList()
        {
            try
            {
                List<ResponseListVM> responselst = new List<ResponseListVM>();
                SortedList sl = new SortedList();
                DataTable dt = new DataTable();
                dt = objDAL.GetDataTable("uspDynamicResponseList");
                responselst = objDAL.ConvertToList<ResponseListVM>(dt);

                return View(responselst != null ? responselst : new List<ResponseListVM>());
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ActionResult ViewResponse(string ResponseCode)
        {
            try
            {
                DynamicFormResponse response = new DynamicFormResponse();
                DynamicFormResponseVM frmData = new DynamicFormResponseVM();
                FormTemplate frm = new FormTemplate();

                if (response != null)
                {
                    response = db.DynamicFormResponses.Where(x => x.ResponseCode == ResponseCode).FirstOrDefault();
                    if (response != null)
                    {
                        frm = db.FormTemplates.Find(response.FormTemplateId); // Here Assign Form template Table will Replace when actual development.

                        if (frm != null)
                        {
                            frmData.ID = response.ID;
                            frmData.FormTemplateId = response.FormTemplateId;
                            frmData.FormTitle = frm.FormTitle;
                            frmData.HtmlContent = frm.HtmlContent;
                            frmData.ResponseCode = response.ResponseCode;
                            frmData.AttachedId = response.AttachedId;
                            frmData.AttachedType = response.AttachedType;
                            frmData.CreatedBy = frm.CreatedBy;
                            var ResponseDetailList = db.DynamicFormResponses.Where(x => x.ResponseCode == ResponseCode).ToList();

                            if (ResponseDetailList != null && ResponseDetailList.Count > 0)
                            {                                
                                foreach (var item in ResponseDetailList)
                                {
                                    DynamicFormResponseDetailVM filedDetail = new DynamicFormResponseDetailVM();
                                    filedDetail.FiledId = item.FiledId;
                                    filedDetail.Type = item.Type;
                                    filedDetail.Value = item.Value;
                                    filedDetail.IsSelected = item.IsSelected;
                                    if (frmData != null)
                                    {
                                        frmData.DynamicFormResponseDetailList.Add(filedDetail);
                                    }
                                    

                                }

                            }
                        }
                    }

                }

                return View(frmData != null ? frmData : new DynamicFormResponseVM());
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        
        #region Document Viewer Demo Try | on 16/07/2018 | Added By Sweta Patel
        public ActionResult ViewDocument(string docpath)
        {
            string documentEncPath = string.Empty;
            if (docpath != null)
            {
                documentEncPath = UtilityCommonFunctions.Decrypt(docpath);
            }
            ViewBag.ViewPath = documentEncPath;
            return View();
        }

        public ActionResult ViewSimpleDocument(string docpath)
        {
            ViewBag.ViewPath = docpath;
            return View();
        }

        [HttpGet]
        public PartialViewResult _ViewDocument(string docpath)
        {
            string documentEncPath = string.Empty;
            if (docpath != null)
            {
                documentEncPath = UtilityCommonFunctions.Decrypt(docpath);
            }           
            ViewBag.ViewPath = documentEncPath;

            return PartialView("_ViewDocument");
        }

        public ActionResult ViewMultipleDocument()
        {
            var userId = CommonFunctions.GetUserId().ToString();
            List<DocumentVersionDetail> doclist = new List<DocumentVersionDetail>();
            doclist = db.DocumentVersionDetails.Where(x => x.IsActive && !x.IsDelete && x.IsCurrent && x.CreatedBy == userId).OrderByDescending(x => x.Id).ToList();
            return View(doclist);
        }

        //public ActionResult ViewAllDocument(string docpath)
        //{
        //    ViewBag.ViewPath = docpath;
        //    return View();
        //}


        //public ActionResult Chooser()
        //{
        //    var fileManager1 = new FileManager
        //    {
        //        Id = "fileManager1",
        //        Width = 800,
        //        Height = 400,
        //        Resizable = true,
        //        DisplayLanguage = "en",
        //        Hidden = true,
        //        CollapseRibbon = true,
        //        DisplayMode = DisplayMode.Window,
        //        WindowOptions =
        //        {
        //            Title = "Choose a file",
        //            Modal = true
        //        },
        //        ClientChosen = "fileManagerChosen",
        //        Chooser = true
        //    };
        //    fileManager1.RootFolders.Add(new FileManagerRootFolder
        //    {
        //        Name = "Root Folder 1",
        //        Location = "~/Uploads/Document/10048/3"
        //    });
        //    fileManager1.RootFolders[0].AccessControls.Add(new FileManagerAccessControl
        //    {
        //        Path = @"\",
        //        AllowedPermissions = FileManagerPermissions.Full
        //    });

        //    var fileManager2 = new FileManager
        //    {
        //        Id = "fileManager2",
        //        Width = 800,
        //        Height = 400,
        //        Resizable = true,
        //        DisplayLanguage = "en",
        //        Hidden = true,
        //        CollapseRibbon = true,
        //        DisplayMode = DisplayMode.Window,
        //        WindowOptions =
        //        {
        //            Title = "Choose a folder",
        //            Modal = true
        //        },
        //        ClientChosen = "fileManagerChosen",
        //        Chooser = true,
        //        ChooserType = FileManagerChooserType.Folder
        //    };
        //    fileManager2.RootFolders.Add(new FileManagerRootFolder
        //    {
        //        Name = "Root Folder 1",
        //        Location = "~/Uploads/Document/10048/3"
        //    });
        //    fileManager2.RootFolders[0].AccessControls.Add(new FileManagerAccessControl
        //    {
        //        Path = @"\",
        //        AllowedPermissions = FileManagerPermissions.Full
        //    });

        //    var fileManager3 = new FileManager
        //    {
        //        Id = "fileManager3",
        //        Width = 800,
        //        Height = 400,
        //        Resizable = true,
        //        DisplayLanguage = "en",
        //        Hidden = true,
        //        CollapseRibbon = true,
        //        DisplayMode = DisplayMode.Window,
        //        WindowOptions =
        //        {
        //            Title = "Choose a file or a folder",
        //            Modal = true
        //        },
        //        ClientChosen = "fileManagerChosen",
        //        Chooser = true,
        //        ChooserType = FileManagerChooserType.FileOrFolder
        //    };
        //    fileManager3.RootFolders.Add(new FileManagerRootFolder
        //    {
        //        Name = "Root Folder 1",
        //        Location = "~/Uploads/Document/10048/3"
        //    });
        //    fileManager3.RootFolders[0].AccessControls.Add(new FileManagerAccessControl
        //    {
        //        Path = @"\",
        //        AllowedPermissions = FileManagerPermissions.Full
        //    });

        //    var fileManager4 = new FileManager
        //    {
        //        Id = "fileManager4",
        //        Width = 800,
        //        Height = 400,
        //        Resizable = true,
        //        DisplayLanguage = "en",
        //        Hidden = true,
        //        CollapseRibbon = true,
        //        DisplayMode = DisplayMode.Window,
        //        WindowOptions =
        //        {
        //            Title = "Choose files",
        //            Modal = true
        //        },
        //        ClientChosen = "fileManagerChosen",
        //        Chooser = true,
        //        ChooserMultipleSelection = true
        //    };
        //    fileManager4.RootFolders.Add(new FileManagerRootFolder
        //    {
        //        Name = "Root Folder 1",
        //        Location = "~/Uploads/Document/10048/3"
        //    });
        //    fileManager4.RootFolders[0].AccessControls.Add(new FileManagerAccessControl
        //    {
        //        Path = @"\",
        //        AllowedPermissions = FileManagerPermissions.Full
        //    });

        //    var fileManager5 = new FileManager
        //    {
        //        Id = "fileManager5",
        //        Width = 800,
        //        Height = 400,
        //        Resizable = true,
        //        DisplayLanguage = "en",
        //        Hidden = true,
        //        CollapseRibbon = true,
        //        DisplayMode = DisplayMode.Window,
        //        WindowOptions =
        //        {
        //            Title = "Choose folders",
        //            Modal = true
        //        },
        //        ClientChosen = "fileManagerChosen",
        //        Chooser = true,
        //        ChooserType = FileManagerChooserType.Folder,
        //        ChooserMultipleSelection = true
        //    };
        //    fileManager5.RootFolders.Add(new FileManagerRootFolder
        //    {
        //        Name = "Root Folder 1",
        //        Location = "~/Uploads/Document/10048/3"
        //    });
        //    fileManager5.RootFolders[0].AccessControls.Add(new FileManagerAccessControl
        //    {
        //        Path = @"\",
        //        AllowedPermissions = FileManagerPermissions.Full
        //    });

        //    var fileManager6 = new FileManager
        //    {
        //        Id = "fileManager6",
        //        Width = 800,
        //        Height = 400,
        //        Resizable = true,
        //        DisplayLanguage = "en",
        //        Hidden = true,
        //        CollapseRibbon = true,
        //        DisplayMode = DisplayMode.Window,
        //        WindowOptions =
        //        {
        //            Title = "Choose files or folders",
        //            Modal = true
        //        },
        //        ClientChosen = "fileManagerChosen",
        //        Chooser = true,
        //        ChooserType = FileManagerChooserType.FileOrFolder,
        //        ChooserMultipleSelection = true
        //    };
        //    fileManager6.RootFolders.Add(new FileManagerRootFolder
        //    {
        //        Name = "Root Folder 1",
        //        Location = "~/Uploads/Document/10048/3"
        //    });
        //    fileManager6.RootFolders[0].AccessControls.Add(new FileManagerAccessControl
        //    {
        //        Path = @"\",
        //        AllowedPermissions = FileManagerPermissions.Full
        //    });

        //    var fileManager7 = new FileManager
        //    {
        //        Id = "fileManager7",
        //        Width = 800,
        //        Height = 400,
        //        Resizable = true,
        //        DisplayLanguage = "en",
        //        Hidden = true,
        //        ShowRibbon = false,
        //        ClientChosen = "fileManagerChosen",
        //        Chooser = true
        //    };
        //    fileManager7.RootFolders.Add(new FileManagerRootFolder
        //    {
        //        Name = "Root Folder 1",
        //        Location = "~/Uploads/Document/10048/3"
        //    });
        //    fileManager7.RootFolders[0].AccessControls.Add(new FileManagerAccessControl
        //    {
        //        Path = @"\",
        //        AllowedPermissions = FileManagerPermissions.Full
        //    });

        //    return View(new[] { fileManager1, fileManager2, fileManager3, fileManager4, fileManager5, fileManager6, fileManager7 });
        //}

        #endregion
    }
}