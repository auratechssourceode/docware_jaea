﻿using DocWare.Helpers;
using DocWare.Models;
using DocWare.Resources;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utility.Helpers;
using Utility.Models;

namespace DocWare.Controllers
{
    [CheckAuthorization]
    public class AdministrationController : BaseController
    {
        DBEntities db = new DBEntities();
        DAL objDAL = new DAL();
        #region --> Department Management | Add | Jasmin Vohra | 01032018
        [HttpGet, Route("DepartmentList")]
        public ActionResult DepartmentList()
        {
            List<DepartmentVM> list = new List<DepartmentVM>();
            try
            {
                #region --> Page Permission on Action and controller name | Jasmin | 21032018

                var permissionlst = Session[SessionClass.LinkPermissionList].ToString();
                List<UserPermissionVM> userPermissionLst = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserPermissionVM>>(permissionlst);

                PermissionVM permissionVM = new PermissionVM();

                permissionVM.HasAddPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "AddDepartment" && x.ControllerName.Trim() == "Administration").Count() > 0;
                permissionVM.HasEditPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "EditDepartment" && x.ControllerName.Trim() == "Administration").Count() > 0;
                permissionVM.HasStatusPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "ChangeDepartmentStatus" && x.ControllerName.Trim() == "Administration").Count() > 0;
                permissionVM.HasActivePermission = userPermissionLst.Where(x => x.ActionName.Trim() == "ActiveSelectedDepartment" && x.ControllerName.Trim() == "Administration").Count() > 0;
                permissionVM.HasDeactivePermission = userPermissionLst.Where(x => x.ActionName.Trim() == "DeactiveSelectedDepartment" && x.ControllerName.Trim() == "Administration").Count() > 0;
                permissionVM.HasDeletePermission = userPermissionLst.Where(x => x.ActionName.Trim() == "DeleteSelectedDepartment" && x.ControllerName.Trim() == "Administration").Count() > 0;

                Session["PermissionListSession"] = permissionVM;
                #endregion

                using (db = new DBEntities())
                {
                    list = db.DepartmentMasters.Where(x => !x.IsDelete)
                        .Select(j => new DepartmentVM
                        {
                            Id = j.Id,
                            Name = j.Name,
                            DepartmentManagerId = j.DepartmentManagerId,
                            IsActive = j.IsActive,
                            IsDelete = j.IsDelete
                        }).OrderByDescending(x => x.Id).ToList();

                    if (list.Count > 0)
                    {
                        foreach (var item in list)
                        {
                            item.DepartmentManagerName = CommonFunctions.GetFullName(item.DepartmentManagerId);
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
            return View(list);
        }

        [HttpGet, Route("AddDepartment")]
        public ActionResult AddDepartment()
        {
            DepartmentMaster departmentMaster = new DepartmentMaster();
            return View(departmentMaster);
        }

        [HttpGet, Route("EditDepartment")]
        public ActionResult EditDepartment(long id)
        {
            DepartmentMaster departmentMaster = new DepartmentMaster();
            try
            {
                if (id > 0)
                {
                    using (db = new DBEntities())
                    {
                        departmentMaster = db.DepartmentMasters.Where(m => m.Id == id).FirstOrDefault();
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return View("AddDepartment", departmentMaster);
        }

        [HttpPost, Route("AddDepartment")]
        [AuditLog(EnumList.EnumLogType.SaveData, "AddDepartment")]
        public ActionResult AddDepartment(DepartmentMaster data)
        {
            DepartmentMaster departmentMaster = new DepartmentMaster();
            try
            {
                using (db = new DBEntities())
                {
                    var userid = CommonFunctions.GetUserId();
                    if (data.Id > 0)
                    {
                        departmentMaster = db.DepartmentMasters.Where(m => m.Id == data.Id).FirstOrDefault();
                        departmentMaster.Name = data.Name.Trim();
                        departmentMaster.DepartmentManagerId = data.DepartmentManagerId;
                        departmentMaster.UpdatedDate = DateTime.Now;
                        departmentMaster.UpdatedBy = userid.ToString();
                        db.Entry(departmentMaster).State = EntityState.Modified;
                        db.SaveChanges();

                        TempData["SuccessMSG"] = Messages.Department_updated_successfully;
                    }
                    else
                    {
                        departmentMaster.Name = data.Name.Trim();
                        departmentMaster.DepartmentManagerId = data.DepartmentManagerId;
                        departmentMaster.IsActive = true;
                        departmentMaster.CreatedDate = DateTime.Now;
                        departmentMaster.CreatedBy = userid.ToString();
                        db.DepartmentMasters.Add(departmentMaster);
                        db.SaveChanges();

                        var user = db.LoginCredentials.Find(data.DepartmentManagerId);
                        if (user != null && user.DepartmentId == 0)
                        {
                            user.DepartmentId = departmentMaster.Id;
                            db.SaveChanges();
                        }

                        TempData["SuccessMSG"] = Messages.Department_added_successfully;
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return RedirectToAction("DepartmentList", "Administration");
        }

        public JsonResult IsDepartmentNameExist(string DepartmentName, long? id = 0)
        {
            var Department = DepartmentName;
            bool IsValid = false;
            long? Id = id;
            if (Id > 0)
            {
                DepartmentMaster ObjDepartmentMaster = db.DepartmentMasters.Find(Id);
                if (Department == ObjDepartmentMaster.Name)
                {
                    IsValid = true;
                    return Json(IsValid, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var DeptId = db.DepartmentMasters.Where(x => x.Name.ToLower().Trim() == Department.ToLower().Trim()).Select(x => x.Id).FirstOrDefault();
                    if (DeptId > 0)
                    {
                        IsValid = false;
                        return Json(IsValid, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        IsValid = true;
                        return Json(IsValid, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                if (Department != null)
                {
                    var DeptId = db.DepartmentMasters.Where(x => x.Name.ToLower().Trim() == Department.ToLower().Trim()).Select(x => x.Id).FirstOrDefault();
                    if (DeptId > 0)
                    {
                        IsValid = false;
                        return Json(IsValid, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        IsValid = true;
                        return Json(IsValid, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            IsValid = true;
            return Json(IsValid, JsonRequestBehavior.AllowGet);
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "ChangeDepartmentStatus")]
        public JsonResult ChangeDepartmentStatus(long id)
        {
            DepartmentMaster departmentMaster = new DepartmentMaster();
            try
            {
                if (id > 0)
                {
                    using (db = new DBEntities())
                    {
                        departmentMaster = db.DepartmentMasters.Where(m => m.Id == id).FirstOrDefault();
                        var userid = CommonFunctions.GetUserId();
                        if (departmentMaster.IsActive)
                        {
                            if (CommonFunctions.IsUserAssigned(id.ToString(), (int)EnumList.EnumMasterType.Department))
                            {
                                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                departmentMaster.IsActive = false;
                            }
                        }
                        else
                        {
                            departmentMaster.IsActive = true;
                        }
                        departmentMaster.UpdatedBy = userid.ToString();
                        departmentMaster.UpdatedDate = DateTime.Now;
                        db.Entry(departmentMaster).State = EntityState.Modified;
                        db.SaveChanges();

                        if (!departmentMaster.IsActive)
                        {
                            #region --> Section & Position Deactive
                            var section = db.SectionMasters.Where(j => j.DepartmentId == id && j.IsActive && !j.IsDelete).ToList();

                            foreach (var secitem in section)
                            {
                                secitem.IsActive = false;
                                secitem.UpdatedBy = userid.ToString();
                                secitem.UpdatedDate = DateTime.Now;
                                db.Entry(secitem).State = EntityState.Modified;
                                db.SaveChanges();

                                var position = db.PositionMasters.Where(j => j.SectionId == secitem.Id && j.IsActive && !j.IsDelete).ToList();

                                foreach (var positem in position)
                                {
                                    positem.IsActive = false;
                                    positem.UpdatedBy = userid.ToString();
                                    positem.UpdatedDate = DateTime.Now;
                                    db.Entry(positem).State = EntityState.Modified;
                                    db.SaveChanges();
                                }
                            }
                            #endregion
                        }
                    }
                }
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [AuditLog(EnumList.EnumLogType.DeleteData, "DeleteSelectedDepartment")]
        public JsonResult DeleteSelectedDepartment(string[] departmentIds)
        {
            bool isAssigned = true;
            DepartmentMaster departmentMaster = new DepartmentMaster();
            try
            {
                if (departmentIds != null)
                {
                    if (departmentIds.Length > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();

                            isAssigned = CommonFunctions.IsUserAssigned(string.Join(",", departmentIds), (int)EnumList.EnumMasterType.Department);

                            if (!isAssigned)
                            {
                                foreach (var item in departmentIds)
                                {
                                    long id = Convert.ToInt64(item);

                                    departmentMaster = db.DepartmentMasters.Where(m => m.Id == id).FirstOrDefault();

                                    departmentMaster.IsActive = false;
                                    departmentMaster.IsDelete = true;
                                    departmentMaster.UpdatedBy = userId.ToString();
                                    departmentMaster.UpdatedDate = DateTime.Now;
                                    db.Entry(departmentMaster).State = EntityState.Modified;
                                    db.SaveChanges();

                                    #region --> Section & Position Delete
                                    var section = db.SectionMasters.Where(j => j.DepartmentId == id && !j.IsDelete).ToList();

                                    foreach (var secitem in section)
                                    {
                                        secitem.IsActive = false;
                                        secitem.IsDelete = true;
                                        secitem.UpdatedBy = userId.ToString();
                                        secitem.UpdatedDate = DateTime.Now;
                                        db.Entry(secitem).State = EntityState.Modified;
                                        db.SaveChanges();

                                        var position = db.PositionMasters.Where(j => j.SectionId == secitem.Id && !j.IsDelete).ToList();

                                        foreach (var positem in position)
                                        {
                                            positem.IsActive = false;
                                            positem.IsDelete = true;
                                            positem.UpdatedBy = userId.ToString();
                                            positem.UpdatedDate = DateTime.Now;
                                            db.Entry(positem).State = EntityState.Modified;
                                            db.SaveChanges();
                                        }
                                    }
                                    #endregion
                                }
                                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "DeactiveSelectedDepartment")]
        public JsonResult DeactiveSelectedDepartment(string[] departmentIds)
        {
            bool isAssigned = true;
            DepartmentMaster departmentMaster = new DepartmentMaster();
            try
            {
                if (departmentIds != null)
                {
                    if (departmentIds.Length > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();

                            isAssigned = CommonFunctions.IsUserAssigned(string.Join(",", departmentIds), (int)EnumList.EnumMasterType.Department);

                            if (!isAssigned)
                            {
                                foreach (var item in departmentIds)
                                {
                                    long id = Convert.ToInt64(item);

                                    departmentMaster = db.DepartmentMasters.Where(m => m.Id == id).FirstOrDefault();

                                    departmentMaster.IsActive = false;
                                    departmentMaster.UpdatedBy = userId.ToString();
                                    departmentMaster.UpdatedDate = DateTime.Now;
                                    db.Entry(departmentMaster).State = EntityState.Modified;
                                    db.SaveChanges();

                                    #region --> Section & Position Deactive
                                    var section = db.SectionMasters.Where(j => j.DepartmentId == id && j.IsActive && !j.IsDelete).ToList();

                                    foreach (var secitem in section)
                                    {
                                        secitem.IsActive = false;
                                        secitem.UpdatedBy = userId.ToString();
                                        secitem.UpdatedDate = DateTime.Now;
                                        db.Entry(secitem).State = EntityState.Modified;
                                        db.SaveChanges();

                                        var position = db.PositionMasters.Where(j => j.SectionId == secitem.Id && j.IsActive && !j.IsDelete).ToList();

                                        foreach (var positem in position)
                                        {
                                            positem.IsActive = false;
                                            positem.UpdatedBy = userId.ToString();
                                            positem.UpdatedDate = DateTime.Now;
                                            db.Entry(positem).State = EntityState.Modified;
                                            db.SaveChanges();
                                        }
                                    }
                                    #endregion
                                }
                                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "ActiveSelectedDepartment")]
        public JsonResult ActiveSelectedDepartment(string[] departmentIds)
        {
            DepartmentMaster departmentMaster = new DepartmentMaster();
            try
            {
                if (departmentIds != null)
                {
                    if (departmentIds.Length > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();

                            foreach (var item in departmentIds)
                            {
                                long id = Convert.ToInt64(item);

                                departmentMaster = db.DepartmentMasters.Where(m => m.Id == id).FirstOrDefault();

                                departmentMaster.IsActive = true;
                                departmentMaster.UpdatedBy = userId.ToString();
                                departmentMaster.UpdatedDate = DateTime.Now;
                                db.Entry(departmentMaster).State = EntityState.Modified;
                                db.SaveChanges();

                            }
                            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet, Route("DeletedDepartmentList")]
        public ActionResult DeletedDepartmentList()
        {
            List<DepartmentVM> list = new List<DepartmentVM>();
            try
            {
                #region --> Page Permission on Action and controller name | Jasmin | 22032018

                var permissionlst = Session[SessionClass.LinkPermissionList].ToString();
                List<UserPermissionVM> userPermissionLst = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserPermissionVM>>(permissionlst);

                PermissionVM permissionVM = new PermissionVM();

                permissionVM.HasRestorePermisssion = userPermissionLst.Where(x => x.ActionName.Trim() == "RestoreSelectedDepartment" && x.ControllerName.Trim() == "Administration").Count() > 0;

                Session["PermissionListSession"] = permissionVM;
                #endregion

                using (db = new DBEntities())
                {
                    list = db.DepartmentMasters.Where(x => x.IsDelete)
                        .Select(j => new DepartmentVM
                        {
                            Id = j.Id,
                            Name = j.Name,
                            DepartmentManagerId = j.DepartmentManagerId,
                            IsActive = j.IsActive,
                            IsDelete = j.IsDelete
                        }).OrderByDescending(x => x.Id).ToList();

                    if (list.Count > 0)
                    {
                        foreach (var item in list)
                        {
                            item.DepartmentManagerName = CommonFunctions.GetFullName(item.DepartmentManagerId);
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
            return View(list);
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "RestoreSelectedDepartment")]
        public JsonResult RestoreSelectedDepartment(string[] departmentIds)
        {
            DepartmentMaster departmentMaster = new DepartmentMaster();
            try
            {
                if (departmentIds != null)
                {
                    if (departmentIds.Length > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();

                            foreach (var item in departmentIds)
                            {
                                long id = Convert.ToInt64(item);

                                departmentMaster = db.DepartmentMasters.Where(m => m.Id == id).FirstOrDefault();

                                departmentMaster.IsActive = true;
                                departmentMaster.IsDelete = false;
                                departmentMaster.UpdatedBy = userId.ToString();
                                departmentMaster.UpdatedDate = DateTime.Now;
                                db.Entry(departmentMaster).State = EntityState.Modified;
                                db.SaveChanges();

                            }
                            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region --> Section Management | Add | Jasmin Vohra | 01032018
        [HttpGet, Route("SectionList")]
        public ActionResult SectionList()
        {
            List<SectionVM> list = new List<SectionVM>();
            try
            {
                #region --> Page Permission on Action and controller name | Jasmin | 21032018

                var permissionlst = Session[SessionClass.LinkPermissionList].ToString();
                List<UserPermissionVM> userPermissionLst = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserPermissionVM>>(permissionlst);

                PermissionVM permissionVM = new PermissionVM();

                permissionVM.HasAddPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "AddSection" && x.ControllerName.Trim() == "Administration").Count() > 0;
                permissionVM.HasEditPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "EditSection" && x.ControllerName.Trim() == "Administration").Count() > 0;
                permissionVM.HasStatusPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "ChangeSectionStatus" && x.ControllerName.Trim() == "Administration").Count() > 0;
                permissionVM.HasActivePermission = userPermissionLst.Where(x => x.ActionName.Trim() == "ActiveSelectedSection" && x.ControllerName.Trim() == "Administration").Count() > 0;
                permissionVM.HasDeactivePermission = userPermissionLst.Where(x => x.ActionName.Trim() == "DeactiveSelectedSection" && x.ControllerName.Trim() == "Administration").Count() > 0;
                permissionVM.HasDeletePermission = userPermissionLst.Where(x => x.ActionName.Trim() == "DeleteSelectedSection" && x.ControllerName.Trim() == "Administration").Count() > 0;

                Session["PermissionListSession"] = permissionVM;
                #endregion

                using (db = new DBEntities())
                {
                    list = db.SectionMasters.Where(x => !x.IsDelete)
                        .Select(j => new SectionVM
                        {
                            Id = j.Id,
                            Name = j.Name,
                            DepartmentId = j.DepartmentId,
                            DepartmentName = j.DepartmentId != 0 ? (db.DepartmentMasters.Where(x => x.Id == j.DepartmentId).FirstOrDefault() != null ? db.DepartmentMasters.Where(x => x.Id == j.DepartmentId).FirstOrDefault().Name : "") : "",
                            SectionManagerId = j.SectionManagerId,
                            IsActive = j.IsActive,
                            IsDelete = j.IsDelete
                        }).OrderByDescending(x => x.Id).ToList();

                    if (list.Count > 0)
                    {
                        foreach (var item in list)
                        {
                            item.SectionManagerName = CommonFunctions.GetFullName(item.SectionManagerId);
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
            return View(list);
        }

        [HttpGet, Route("AddSection")]
        public ActionResult AddSection()
        {
            SectionMaster sectionMaster = new SectionMaster();
            return View(sectionMaster);
        }

        [HttpGet, Route("EditSection")]
        public ActionResult EditSection(long id)
        {
            SectionMaster sectionMaster = new SectionMaster();
            try
            {
                if (id > 0)
                {
                    using (db = new DBEntities())
                    {
                        sectionMaster = db.SectionMasters.Where(m => m.Id == id).FirstOrDefault();
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return View("AddSection", sectionMaster);
        }

        [HttpPost, Route("AddSection")]
        [AuditLog(EnumList.EnumLogType.SaveData, "AddSection")]
        public ActionResult AddSection(SectionMaster data)
        {
            SectionMaster sectionMaster = new SectionMaster();
            try
            {
                using (db = new DBEntities())
                {
                    var userid = CommonFunctions.GetUserId();
                    if (data.Id > 0)
                    {
                        sectionMaster = db.SectionMasters.Where(m => m.Id == data.Id).FirstOrDefault();
                        sectionMaster.Name = data.Name.Trim();
                        sectionMaster.DepartmentId = data.DepartmentId;
                        sectionMaster.SectionManagerId = data.SectionManagerId;
                        sectionMaster.UpdatedDate = DateTime.Now;
                        sectionMaster.UpdatedBy = userid.ToString();
                        db.Entry(sectionMaster).State = EntityState.Modified;
                        db.SaveChanges();

                        TempData["SuccessMSG"] = Messages.Section_updated_successfully;
                    }
                    else
                    {
                        sectionMaster.Name = data.Name.Trim();
                        sectionMaster.DepartmentId = data.DepartmentId;
                        sectionMaster.SectionManagerId = data.SectionManagerId;
                        sectionMaster.IsActive = true;
                        sectionMaster.CreatedDate = DateTime.Now;
                        sectionMaster.CreatedBy = userid.ToString();
                        db.SectionMasters.Add(sectionMaster);
                        db.SaveChanges();

                        var user = db.LoginCredentials.Find(data.SectionManagerId);
                        if (user != null && user.SectionId == 0)
                        {
                            user.DepartmentId = user.DepartmentId == 0 ? data.DepartmentId : user.DepartmentId;
                            user.SectionId = sectionMaster.Id;
                            db.SaveChanges();
                        }

                        TempData["SuccessMSG"] = Messages.Section_added_successfully;
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return RedirectToAction("SectionList", "Administration");
        }

        public JsonResult IsSectionNameExist(string SectionName, long DeptId, long? id = 0)
        {
            var Section = SectionName;
            bool IsValid = false;
            long? Id = id;
            if (Id > 0)
            {
                SectionMaster ObjSectionMaster = db.SectionMasters.Find(Id);
                if (Section == ObjSectionMaster.Name)
                {
                    IsValid = true;
                    return Json(IsValid, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var SecId = db.SectionMasters.Where(x => x.Name.ToLower().Trim() == Section.ToLower().Trim() && x.DepartmentId == DeptId).Select(x => x.Id).FirstOrDefault();
                    if (SecId > 0)
                    {
                        IsValid = false;
                        return Json(IsValid, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        IsValid = true;
                        return Json(IsValid, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                if (Section != null)
                {
                    var SecId = db.SectionMasters.Where(x => x.Name.ToLower().Trim() == Section.ToLower().Trim() && x.DepartmentId == DeptId).Select(x => x.Id).FirstOrDefault();
                    if (SecId > 0)
                    {
                        IsValid = false;
                        return Json(IsValid, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        IsValid = true;
                        return Json(IsValid, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            IsValid = true;
            return Json(IsValid, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetUserByDepartmentID(long DepartmentID)
        {
            try
            {
                List<SelectListItem> lst = CommonFunctions.GetUserListByDepartment(DepartmentID);
                return Json(new { success = true, lst = lst }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "ChangeSectionStatus")]
        public JsonResult ChangeSectionStatus(long id)
        {
            SectionMaster sectionMaster = new SectionMaster();
            try
            {
                if (id > 0)
                {
                    using (db = new DBEntities())
                    {
                        var userid = CommonFunctions.GetUserId();
                        sectionMaster = db.SectionMasters.Where(m => m.Id == id).FirstOrDefault();
                        if (sectionMaster.IsActive)
                        {
                            if (CommonFunctions.IsUserAssigned(id.ToString(), (int)EnumList.EnumMasterType.Section))
                            {
                                return Json(new { success = false, message = "Section is already in use" }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                sectionMaster.IsActive = false;
                            }
                        }
                        else
                        {
                            if (db.DepartmentMasters.Where(j => j.Id == sectionMaster.DepartmentId && (!j.IsActive || j.IsDelete)).Count() > 0)
                            {
                                return Json(new { success = false, message = "Related department is inactive or deleted.. Active or Restore it first" }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                sectionMaster.IsActive = true;
                            }
                        }
                        sectionMaster.UpdatedBy = userid.ToString();
                        sectionMaster.UpdatedDate = DateTime.Now;
                        db.Entry(sectionMaster).State = EntityState.Modified;
                        db.SaveChanges();

                        if (!sectionMaster.IsActive)
                        {
                            #region --> Position Deactive

                            var position = db.PositionMasters.Where(j => j.SectionId == id && j.IsActive && !j.IsDelete).ToList();

                            foreach (var positem in position)
                            {
                                positem.IsActive = false;
                                positem.UpdatedBy = userid.ToString();
                                positem.UpdatedDate = DateTime.Now;
                                db.Entry(positem).State = EntityState.Modified;
                                db.SaveChanges();
                            }

                            #endregion
                        }
                    }
                }
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [AuditLog(EnumList.EnumLogType.DeleteData, "DeleteSelectedSection")]
        public JsonResult DeleteSelectedSection(string[] sectionIds)
        {
            bool isAssigned = true;
            SectionMaster sectionMaster = new SectionMaster();
            try
            {
                if (sectionIds != null)
                {
                    if (sectionIds.Length > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();

                            isAssigned = CommonFunctions.IsUserAssigned(string.Join(",", sectionIds), (int)EnumList.EnumMasterType.Department);

                            if (!isAssigned)
                            {
                                foreach (var item in sectionIds)
                                {
                                    long id = Convert.ToInt64(item);

                                    sectionMaster = db.SectionMasters.Where(m => m.Id == id).FirstOrDefault();

                                    sectionMaster.IsActive = false;
                                    sectionMaster.IsDelete = true;
                                    sectionMaster.UpdatedBy = userId.ToString();
                                    sectionMaster.UpdatedDate = DateTime.Now;
                                    db.Entry(sectionMaster).State = EntityState.Modified;
                                    db.SaveChanges();

                                    #region --> Position Delete

                                    var position = db.PositionMasters.Where(j => j.SectionId == id && !j.IsDelete).ToList();

                                    foreach (var positem in position)
                                    {
                                        positem.IsActive = false;
                                        positem.IsDelete = true;
                                        positem.UpdatedBy = userId.ToString();
                                        positem.UpdatedDate = DateTime.Now;
                                        db.Entry(positem).State = EntityState.Modified;
                                        db.SaveChanges();
                                    }

                                    #endregion
                                }
                                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "DeactiveSelectedSection")]
        public JsonResult DeactiveSelectedSection(string[] sectionIds)
        {
            bool isAssigned = true;
            SectionMaster sectionMaster = new SectionMaster();
            try
            {
                if (sectionIds != null)
                {
                    if (sectionIds.Length > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();

                            isAssigned = CommonFunctions.IsUserAssigned(string.Join(",", sectionIds), (int)EnumList.EnumMasterType.Department);

                            if (!isAssigned)
                            {
                                foreach (var item in sectionIds)
                                {
                                    long id = Convert.ToInt64(item);

                                    sectionMaster = db.SectionMasters.Where(m => m.Id == id).FirstOrDefault();

                                    sectionMaster.IsActive = false;
                                    sectionMaster.UpdatedBy = userId.ToString();
                                    sectionMaster.UpdatedDate = DateTime.Now;
                                    db.Entry(sectionMaster).State = EntityState.Modified;
                                    db.SaveChanges();

                                    #region --> Position Deactive

                                    var position = db.PositionMasters.Where(j => j.SectionId == id && j.IsActive && !j.IsDelete).ToList();

                                    foreach (var positem in position)
                                    {
                                        positem.IsActive = false;
                                        positem.UpdatedBy = userId.ToString();
                                        positem.UpdatedDate = DateTime.Now;
                                        db.Entry(positem).State = EntityState.Modified;
                                        db.SaveChanges();
                                    }

                                    #endregion
                                }
                                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "ActiveSelectedSection")]
        public JsonResult ActiveSelectedSection(string[] sectionIds)
        {
            SectionMaster sectionMaster = new SectionMaster();
            try
            {
                if (sectionIds != null)
                {
                    if (sectionIds.Length > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();

                            foreach (var item in sectionIds)
                            {
                                long id = Convert.ToInt64(item);

                                sectionMaster = db.SectionMasters.Where(m => m.Id == id).FirstOrDefault();

                                if (db.DepartmentMasters.Where(j => j.Id == sectionMaster.DepartmentId && (!j.IsActive || j.IsDelete)).Count() > 0)
                                {
                                    return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                                }

                                sectionMaster.IsActive = true;
                                sectionMaster.UpdatedBy = userId.ToString();
                                sectionMaster.UpdatedDate = DateTime.Now;
                                db.Entry(sectionMaster).State = EntityState.Modified;
                                db.SaveChanges();

                            }
                            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet, Route("DeletedSectionList")]
        public ActionResult DeletedSectionList()
        {
            List<SectionVM> list = new List<SectionVM>();
            try
            {
                #region --> Page Permission on Action and controller name | Jasmin | 22032018

                var permissionlst = Session[SessionClass.LinkPermissionList].ToString();
                List<UserPermissionVM> userPermissionLst = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserPermissionVM>>(permissionlst);

                PermissionVM permissionVM = new PermissionVM();

                permissionVM.HasRestorePermisssion = userPermissionLst.Where(x => x.ActionName.Trim() == "RestoreSelectedSection" && x.ControllerName.Trim() == "Administration").Count() > 0;

                Session["PermissionListSession"] = permissionVM;
                #endregion

                using (db = new DBEntities())
                {
                    list = db.SectionMasters.Where(x => x.IsDelete)
                        .Select(j => new SectionVM
                        {
                            Id = j.Id,
                            Name = j.Name,
                            DepartmentId = j.DepartmentId,
                            DepartmentName = j.DepartmentId != 0 ? (db.DepartmentMasters.Where(x => x.Id == j.DepartmentId).FirstOrDefault() != null ? db.DepartmentMasters.Where(x => x.Id == j.DepartmentId).FirstOrDefault().Name : "") : "",
                            SectionManagerId = j.SectionManagerId,
                            IsActive = j.IsActive,
                            IsDelete = j.IsDelete
                        }).OrderByDescending(x => x.Id).ToList();

                    if (list.Count > 0)
                    {
                        foreach (var item in list)
                        {
                            item.SectionManagerName = CommonFunctions.GetFullName(item.SectionManagerId);
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
            return View(list);
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "RestoreSelectedSection")]
        public JsonResult RestoreSelectedSection(string[] sectionIds)
        {
            SectionMaster sectionMaster = new SectionMaster();
            try
            {
                if (sectionIds != null)
                {
                    if (sectionIds.Length > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();

                            foreach (var item in sectionIds)
                            {
                                long id = Convert.ToInt64(item);

                                sectionMaster = db.SectionMasters.Where(m => m.Id == id).FirstOrDefault();

                                if (db.DepartmentMasters.Where(j => j.Id == sectionMaster.DepartmentId && (!j.IsActive || j.IsDelete)).Count() > 0)
                                {
                                    return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                                }

                                sectionMaster.IsActive = true;
                                sectionMaster.IsDelete = false;
                                sectionMaster.UpdatedBy = userId.ToString();
                                sectionMaster.UpdatedDate = DateTime.Now;
                                db.Entry(sectionMaster).State = EntityState.Modified;
                                db.SaveChanges();

                            }
                            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region --> Position Management | Add | Jasmin Vohra | 01032018
        [HttpGet, Route("PositionList")]
        public ActionResult PositionList()
        {
            List<PositionVM> positionVMs = new List<PositionVM>();
            try
            {
                #region --> Page Permission on Action and controller name | Jasmin | 21032018

                var permissionlst = Session[SessionClass.LinkPermissionList].ToString();
                List<UserPermissionVM> userPermissionLst = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserPermissionVM>>(permissionlst);

                PermissionVM permissionVM = new PermissionVM();

                permissionVM.HasAddPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "AddPosition" && x.ControllerName.Trim() == "Administration").Count() > 0;
                permissionVM.HasEditPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "EditPosition" && x.ControllerName.Trim() == "Administration").Count() > 0;
                permissionVM.HasStatusPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "ChangePositionStatus" && x.ControllerName.Trim() == "Administration").Count() > 0;
                permissionVM.HasActivePermission = userPermissionLst.Where(x => x.ActionName.Trim() == "ActiveSelectedPosition" && x.ControllerName.Trim() == "Administration").Count() > 0;
                permissionVM.HasDeactivePermission = userPermissionLst.Where(x => x.ActionName.Trim() == "DeactiveSelectedPosition" && x.ControllerName.Trim() == "Administration").Count() > 0;
                permissionVM.HasDeletePermission = userPermissionLst.Where(x => x.ActionName.Trim() == "DeleteSelectedPosition" && x.ControllerName.Trim() == "Administration").Count() > 0;

                Session["PermissionListSession"] = permissionVM;
                #endregion

                using (db = new DBEntities())
                {
                    positionVMs = db.PositionMasters.Where(x => !x.IsDelete).Select(j => new PositionVM
                    {
                        Id = j.Id,
                        Name = j.Name,
                        DepartmentId = j.SectionId > 0 ? db.SectionMasters.Where(x => x.Id == j.SectionId).FirstOrDefault().DepartmentId : 0,
                        SectionId = j.SectionId,
                        SectionName = db.SectionMasters.Where(x => x.Id == j.SectionId).FirstOrDefault().Name,
                        IsActive = j.IsActive
                    }).OrderByDescending(j => j.Id).ToList();

                    if (positionVMs.Count > 0)
                    {
                        foreach (var item in positionVMs)
                        {
                            if (item.DepartmentId > 0)
                                item.DepartmentName = db.DepartmentMasters.Where(x => x.Id == item.DepartmentId).FirstOrDefault().Name;
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
            return View(positionVMs);
        }

        [HttpGet, Route("AddPosition")]
        public ActionResult AddPosition()
        {
            PositionVM positionVM = new PositionVM();
            return View(positionVM);
        }

        [HttpGet, Route("EditPosition")]
        public ActionResult EditPosition(long id)
        {
            PositionVM positionVM = new PositionVM();
            try
            {
                if (id > 0)
                {
                    using (db = new DBEntities())
                    {
                        positionVM = db.PositionMasters.Where(m => m.Id == id).Select(j => new PositionVM
                        {
                            Id = j.Id,
                            Name = j.Name,
                            SectionId = j.SectionId,
                            DepartmentId = db.SectionMasters.Where(x => x.Id == j.SectionId).FirstOrDefault().DepartmentId
                        }).FirstOrDefault();
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return View("AddPosition", positionVM);
        }

        public JsonResult CheckPosition(string PositionName, long SectionID, long PositionID)
        {
            List<PositionMaster> positionMasters = new List<PositionMaster>();
            try
            {
                using (db = new DBEntities())
                {
                    if (PositionID > 0)
                    {
                        positionMasters = db.PositionMasters.Where(j => j.Name.ToLower().Trim() == PositionName.ToLower().Trim() && j.SectionId == SectionID && j.Id != PositionID).ToList();
                    }
                    else
                    {
                        positionMasters = db.PositionMasters.Where(j => j.Name.ToLower().Trim() == PositionName.ToLower().Trim() && j.SectionId == SectionID).ToList();
                    }

                    if (positionMasters.Count() > 0)
                    {
                        return Json(false, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(true, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, Route("AddPosition")]
        [AuditLog(EnumList.EnumLogType.SaveData, "AddPosition")]
        public ActionResult AddPosition(PositionVM data)
        {
            PositionMaster positionMaster = new PositionMaster();
            try
            {
                using (db = new DBEntities())
                {
                    long userId = CommonFunctions.GetUserId();

                    if (data.Id > 0)
                    {
                        positionMaster = db.PositionMasters.Where(m => m.Id == data.Id).FirstOrDefault();
                        positionMaster.SectionId = data.SectionId;
                        positionMaster.Name = data.Name.Trim();
                        positionMaster.UpdatedBy = userId.ToString();
                        positionMaster.UpdatedDate = DateTime.Now;
                        db.Entry(positionMaster).State = EntityState.Modified;
                        db.SaveChanges();

                        TempData["SuccessMSG"] = Messages.Position_updated_successfully;
                    }
                    else
                    {
                        positionMaster.Name = data.Name.Trim();
                        positionMaster.SectionId = data.SectionId;
                        positionMaster.IsActive = true;
                        positionMaster.IsDelete = false;
                        positionMaster.CreatedBy = userId.ToString();
                        positionMaster.CreatedDate = DateTime.Now;
                        db.PositionMasters.Add(positionMaster);
                        db.SaveChanges();

                        TempData["SuccessMSG"] = Messages.Position_added_successfully;
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return RedirectToAction("PositionList", "Administration");
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "ChangePositionStatus")]
        public JsonResult ChangePositionStatus(long id)
        {
            PositionMaster positionMaster = new PositionMaster();
            try
            {
                if (id > 0)
                {
                    using (db = new DBEntities())
                    {
                        long userId = CommonFunctions.GetUserId();
                        positionMaster = db.PositionMasters.Where(m => m.Id == id).FirstOrDefault();
                        if (positionMaster.IsActive)
                        {
                            if (CommonFunctions.IsUserAssigned(id.ToString(), (int)EnumList.EnumMasterType.Position))
                            {
                                return Json(new { success = false, message = "Section is already in use" }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                positionMaster.IsActive = false;
                            }
                        }
                        else
                        {
                            if (db.SectionMasters.Where(j => j.Id == positionMaster.SectionId && (!j.IsActive || j.IsDelete)).Count() > 0)
                            {
                                return Json(new { success = false, message = "Related section is inactive or deleted.. Active or Restore it first" }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                positionMaster.IsActive = true;

                                //var section = db.SectionMasters.Where(j => j.Id == positionMaster.SectionId && j.IsActive && !j.IsDelete).FirstOrDefault();

                                //if (section != null)
                                //{
                                //    if (db.DepartmentMasters.Where(j => j.Id == section.DepartmentId && (!j.IsActive || j.IsDelete)).Count() > 0)
                                //    {
                                //        return Json(new { success = false, message = "Related department is inactive or deleted.. Active or Restore it first" }, JsonRequestBehavior.AllowGet);
                                //    }
                                //    else
                                //    {
                                //        positionMaster.IsActive = true;
                                //    }
                                //}
                                //else
                                //{
                                //    return Json(new { success = false, message = "Related section is inactive or deleted.. Active or Restore it first" }, JsonRequestBehavior.AllowGet);
                                //}
                            }
                        }
                        positionMaster.UpdatedBy = userId.ToString();
                        positionMaster.UpdatedDate = DateTime.Now;
                        db.Entry(positionMaster).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }

                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [AuditLog(EnumList.EnumLogType.DeleteData, "DeleteSelectedPosition")]
        public JsonResult DeleteSelectedPosition(string[] positionIds)
        {
            bool isAssigned = true;
            PositionMaster positionMaster = new PositionMaster();
            try
            {
                if (positionIds != null)
                {
                    if (positionIds.Length > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();

                            isAssigned = CommonFunctions.IsUserAssigned(string.Join(",", positionIds), (int)EnumList.EnumMasterType.Position);

                            if (!isAssigned)
                            {
                                foreach (var item in positionIds)
                                {
                                    long id = Convert.ToInt64(item);

                                    positionMaster = db.PositionMasters.Where(m => m.Id == id).FirstOrDefault();

                                    positionMaster.IsActive = false;
                                    positionMaster.IsDelete = true;
                                    positionMaster.UpdatedBy = userId.ToString();
                                    positionMaster.UpdatedDate = DateTime.Now;
                                    db.Entry(positionMaster).State = EntityState.Modified;
                                    db.SaveChanges();

                                }
                                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "DeactiveSelectedPosition")]
        public JsonResult DeactiveSelectedPosition(string[] positionIds)
        {
            bool isAssigned = true;
            PositionMaster positionMaster = new PositionMaster();
            try
            {
                if (positionIds != null)
                {
                    if (positionIds.Length > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();

                            isAssigned = CommonFunctions.IsUserAssigned(string.Join(",", positionIds), (int)EnumList.EnumMasterType.Position);

                            if (!isAssigned)
                            {
                                foreach (var item in positionIds)
                                {
                                    long id = Convert.ToInt64(item);

                                    positionMaster = db.PositionMasters.Where(m => m.Id == id).FirstOrDefault();

                                    positionMaster.IsActive = false;
                                    positionMaster.UpdatedBy = userId.ToString();
                                    positionMaster.UpdatedDate = DateTime.Now;
                                    db.Entry(positionMaster).State = EntityState.Modified;
                                    db.SaveChanges();

                                }
                                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "ActiveSelectedPosition")]
        public JsonResult ActiveSelectedPosition(string[] positionIds)
        {
            PositionMaster positionMaster = new PositionMaster();
            try
            {
                if (positionIds != null)
                {
                    if (positionIds.Length > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();

                            foreach (var item in positionIds)
                            {
                                long id = Convert.ToInt64(item);

                                positionMaster = db.PositionMasters.Where(m => m.Id == id).FirstOrDefault();

                                if (db.SectionMasters.Where(j => j.Id == positionMaster.SectionId && (!j.IsActive || j.IsDelete)).Count() > 0)
                                {
                                    return Json(new { success = false, message = "Related section is inactive or deleted.. Active or Restore it first" }, JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    //var section = db.SectionMasters.Where(j => j.Id == positionMaster.SectionId && j.IsActive && !j.IsDelete).FirstOrDefault();

                                    //if (section != null)
                                    //{
                                    //    if (db.DepartmentMasters.Where(j => j.Id == section.DepartmentId && (!j.IsActive || j.IsDelete)).Count() > 0)
                                    //    {
                                    //        return Json(new { success = false, message = "Related department is inactive or deleted.. Active or Restore it first" }, JsonRequestBehavior.AllowGet);
                                    //    }
                                    //    else
                                    //    {
                                    //        positionMaster.IsActive = true;
                                    //    }
                                    //}
                                    //else
                                    //{
                                    //    return Json(new { success = false, message = "Related section is inactive or deleted.. Active or Restore it first" }, JsonRequestBehavior.AllowGet);
                                    //}
                                    positionMaster.IsActive = true;
                                }

                                positionMaster.UpdatedBy = userId.ToString();
                                positionMaster.UpdatedDate = DateTime.Now;
                                db.Entry(positionMaster).State = EntityState.Modified;
                                db.SaveChanges();

                            }
                            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetSectionByDepartmentID(long DepartmentID)
        {
            try
            {
                List<SelectListItem> lst = CommonFunctions.GetSectionListByDepartment(DepartmentID);
                return Json(new { success = true, lst = lst }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        [HttpGet, Route("DeletedPositionList")]
        public ActionResult DeletedPositionList()
        {
            List<PositionVM> positionVMs = new List<PositionVM>();
            try
            {
                #region --> Page Permission on Action and controller name | Jasmin | 22032018

                var permissionlst = Session[SessionClass.LinkPermissionList].ToString();
                List<UserPermissionVM> userPermissionLst = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserPermissionVM>>(permissionlst);

                PermissionVM permissionVM = new PermissionVM();

                permissionVM.HasRestorePermisssion = userPermissionLst.Where(x => x.ActionName.Trim() == "RestoreSelectedPosition" && x.ControllerName.Trim() == "Administration").Count() > 0;

                Session["PermissionListSession"] = permissionVM;
                #endregion

                using (db = new DBEntities())
                {
                    positionVMs = db.PositionMasters.Where(x => x.IsDelete).Select(j => new PositionVM
                    {
                        Id = j.Id,
                        Name = j.Name,
                        DepartmentId = j.SectionId > 0 ? db.SectionMasters.Where(x => x.Id == j.SectionId).FirstOrDefault().DepartmentId : 0,
                        SectionId = j.SectionId,
                        SectionName = db.SectionMasters.Where(x => x.Id == j.SectionId).FirstOrDefault().Name,
                        IsActive = j.IsActive
                    }).OrderByDescending(j => j.Id).ToList();

                    if (positionVMs.Count > 0)
                    {
                        foreach (var item in positionVMs)
                        {
                            if (item.DepartmentId > 0)
                                item.DepartmentName = db.DepartmentMasters.Where(x => x.Id == item.DepartmentId).FirstOrDefault().Name;
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
            return View(positionVMs);
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "RestoreSelectedPosition")]
        public JsonResult RestoreSelectedPosition(string[] positionIds)
        {
            PositionMaster positionMaster = new PositionMaster();
            try
            {
                if (positionIds != null)
                {
                    if (positionIds.Length > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();

                            foreach (var item in positionIds)
                            {
                                long id = Convert.ToInt64(item);

                                positionMaster = db.PositionMasters.Where(m => m.Id == id).FirstOrDefault();

                                if (db.SectionMasters.Where(j => j.Id == positionMaster.SectionId && (!j.IsActive || j.IsDelete)).Count() > 0)
                                {
                                    return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                                }

                                positionMaster.IsActive = true;
                                positionMaster.IsDelete = false;
                                positionMaster.UpdatedBy = userId.ToString();
                                positionMaster.UpdatedDate = DateTime.Now;
                                db.Entry(positionMaster).State = EntityState.Modified;
                                db.SaveChanges();

                            }
                            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region --> Category Management | Add | Jasmin Vohra | 01032018
        [HttpGet, Route("CategoryList")]
        public ActionResult CategoryList()
        {
            List<CategoryMaster> categoryMasters = new List<CategoryMaster>();
            try
            {
                #region --> Page Permission on Action and controller name | Jasmin | 21032018

                var permissionlst = Session[SessionClass.LinkPermissionList].ToString();
                List<UserPermissionVM> userPermissionLst = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserPermissionVM>>(permissionlst);

                PermissionVM permissionVM = new PermissionVM();

                permissionVM.HasAddPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "AddCategory" && x.ControllerName.Trim() == "Administration").Count() > 0;
                permissionVM.HasEditPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "EditCategory" && x.ControllerName.Trim() == "Administration").Count() > 0;
                permissionVM.HasStatusPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "ChangeCategoryStatus" && x.ControllerName.Trim() == "Administration").Count() > 0;
                permissionVM.HasActivePermission = userPermissionLst.Where(x => x.ActionName.Trim() == "ActiveSelectedCategory" && x.ControllerName.Trim() == "Administration").Count() > 0;
                permissionVM.HasDeactivePermission = userPermissionLst.Where(x => x.ActionName.Trim() == "DeactiveSelectedCategory" && x.ControllerName.Trim() == "Administration").Count() > 0;
                permissionVM.HasDeletePermission = userPermissionLst.Where(x => x.ActionName.Trim() == "DeleteSelectedCategory" && x.ControllerName.Trim() == "Administration").Count() > 0;

                Session["PermissionListSession"] = permissionVM;
                #endregion

                using (db = new DBEntities())
                {
                    categoryMasters = db.CategoryMasters.Where(x => !x.IsDelete).OrderByDescending(j => j.Id).ToList();
                }
            }
            catch
            {
                throw;
            }
            return View(categoryMasters);
        }

        [HttpGet, Route("AddCategory")]
        public ActionResult AddCategory()
        {
            CategoryMaster categoryMaster = new CategoryMaster();
            return View(categoryMaster);
        }

        [HttpGet, Route("EditCategory")]
        public ActionResult EditCategory(long id)
        {
            CategoryMaster categoryMaster = new CategoryMaster();
            try
            {
                if (id > 0)
                {
                    using (db = new DBEntities())
                    {
                        categoryMaster = db.CategoryMasters.Where(m => m.Id == id).FirstOrDefault();
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return View("AddCategory", categoryMaster);
        }

        [HttpPost, Route("AddCategory")]
        [AuditLog(EnumList.EnumLogType.SaveData, "AddCategory")]
        public ActionResult AddCategory(CategoryMaster data)
        {
            CategoryMaster categoryMaster = new CategoryMaster();
            try
            {
                using (db = new DBEntities())
                {
                    long userId = CommonFunctions.GetUserId();

                    if (data.Id > 0)
                    {
                        categoryMaster = db.CategoryMasters.Where(m => m.Id == data.Id).FirstOrDefault();
                        categoryMaster.Name = data.Name.Trim();
                        categoryMaster.UpdatedBy = userId.ToString();
                        categoryMaster.UpdatedDate = DateTime.Now;
                        db.Entry(categoryMaster).State = EntityState.Modified;
                        db.SaveChanges();

                        TempData["SuccessMSG"] = Messages.Category_updated_successfully;
                    }
                    else
                    {
                        categoryMaster.Name = data.Name.Trim();
                        categoryMaster.IsActive = true;
                        categoryMaster.IsDelete = false;
                        categoryMaster.CreatedBy = userId.ToString();
                        categoryMaster.CreatedDate = DateTime.Now;
                        db.CategoryMasters.Add(categoryMaster);
                        db.SaveChanges();

                        TempData["SuccessMSG"] = Messages.Category_added_successfully;
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return RedirectToAction("CategoryList", "Administration");
        }

        public JsonResult CheckCategory(string CategoryName, long CategoryID)
        {
            List<CategoryMaster> categoryMasters = new List<CategoryMaster>();
            try
            {
                using (db = new DBEntities())
                {
                    if (CategoryID > 0)
                    {
                        categoryMasters = db.CategoryMasters.Where(j => j.Name.ToLower().Trim() == CategoryName.ToLower().Trim() && j.Id != CategoryID).ToList();
                    }
                    else
                    {
                        categoryMasters = db.CategoryMasters.Where(j => j.Name.ToLower().Trim() == CategoryName.ToLower().Trim()).ToList();
                    }

                    if (categoryMasters.Count() > 0)
                    {
                        return Json(false, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(true, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "ChangeCategoryStatus")]
        public JsonResult ChangeCategoryStatus(long id)
        {
            CategoryMaster categoryMaster = new CategoryMaster();
            try
            {
                if (id > 0)
                {
                    using (db = new DBEntities())
                    {
                        long userId = CommonFunctions.GetUserId();
                        categoryMaster = db.CategoryMasters.Where(m => m.Id == id).FirstOrDefault();
                        if (categoryMaster.IsActive)
                        {
                            if (CommonFunctions.IsFileAssigned(id.ToString(), (int)EnumList.EnumMasterType.Category))
                            {
                                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                categoryMaster.IsActive = false;
                            }
                        }
                        else
                        {
                            categoryMaster.IsActive = true;
                        }
                        categoryMaster.UpdatedBy = userId.ToString();
                        categoryMaster.UpdatedDate = DateTime.Now;
                        db.Entry(categoryMaster).State = EntityState.Modified;
                        db.SaveChanges();

                        if (!categoryMaster.IsActive)
                        {
                            #region --> Subcategory Deactive
                            var subcategory = db.SubCategoryMasters.Where(j => j.CategoryId == id && j.IsActive && !j.IsDelete).ToList();

                            foreach (var item in subcategory)
                            {
                                item.IsActive = false;
                                item.UpdatedBy = userId.ToString();
                                item.UpdatedDate = DateTime.Now;
                                db.Entry(item).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                            #endregion
                        }
                    }
                }
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [AuditLog(EnumList.EnumLogType.DeleteData, "DeleteSelectedCategory")]
        public JsonResult DeleteSelectedCategory(string[] categoryIds)
        {
            bool isAssigned = true;
            CategoryMaster categoryMaster = new CategoryMaster();
            try
            {
                if (categoryIds != null)
                {
                    if (categoryIds.Length > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();

                            isAssigned = CommonFunctions.IsFileAssigned(string.Join(",", categoryIds), (int)EnumList.EnumMasterType.Category);

                            if (!isAssigned)
                            {
                                foreach (var item in categoryIds)
                                {
                                    long id = Convert.ToInt64(item);

                                    categoryMaster = db.CategoryMasters.Where(m => m.Id == id).FirstOrDefault();

                                    categoryMaster.IsActive = false;
                                    categoryMaster.IsDelete = true;
                                    categoryMaster.UpdatedBy = userId.ToString();
                                    categoryMaster.UpdatedDate = DateTime.Now;
                                    db.Entry(categoryMaster).State = EntityState.Modified;
                                    db.SaveChanges();

                                    #region --> Subcategory Delete
                                    var subcategory = db.SubCategoryMasters.Where(j => j.CategoryId == id && !j.IsDelete).ToList();

                                    foreach (var subitem in subcategory)
                                    {
                                        subitem.IsActive = false;
                                        subitem.IsDelete = true;
                                        subitem.UpdatedBy = userId.ToString();
                                        subitem.UpdatedDate = DateTime.Now;
                                        db.Entry(subitem).State = EntityState.Modified;
                                        db.SaveChanges();
                                    }
                                    #endregion
                                }
                                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "DeactiveSelectedCategory")]
        public JsonResult DeactiveSelectedCategory(string[] categoryIds)
        {
            bool isAssigned = true;
            CategoryMaster categoryMaster = new CategoryMaster();
            try
            {
                if (categoryIds != null)
                {
                    if (categoryIds.Length > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();

                            isAssigned = CommonFunctions.IsFileAssigned(string.Join(",", categoryIds), (int)EnumList.EnumMasterType.Category);

                            if (!isAssigned)
                            {
                                foreach (var item in categoryIds)
                                {
                                    long id = Convert.ToInt64(item);

                                    categoryMaster = db.CategoryMasters.Where(m => m.Id == id).FirstOrDefault();

                                    categoryMaster.IsActive = false;
                                    categoryMaster.UpdatedBy = userId.ToString();
                                    categoryMaster.UpdatedDate = DateTime.Now;
                                    db.Entry(categoryMaster).State = EntityState.Modified;
                                    db.SaveChanges();

                                    #region --> Subcategory Deactive
                                    var subcategory = db.SubCategoryMasters.Where(j => j.CategoryId == id && j.IsActive && !j.IsDelete).ToList();

                                    foreach (var subitem in subcategory)
                                    {
                                        subitem.IsActive = false;
                                        subitem.UpdatedBy = userId.ToString();
                                        subitem.UpdatedDate = DateTime.Now;
                                        db.Entry(subitem).State = EntityState.Modified;
                                        db.SaveChanges();
                                    }
                                    #endregion
                                }
                                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "ActiveSelectedCategory")]
        public JsonResult ActiveSelectedCategory(string[] categoryIds)
        {
            CategoryMaster categoryMaster = new CategoryMaster();
            try
            {
                if (categoryIds != null)
                {
                    if (categoryIds.Length > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();

                            foreach (var item in categoryIds)
                            {
                                long id = Convert.ToInt64(item);

                                categoryMaster = db.CategoryMasters.Where(m => m.Id == id).FirstOrDefault();

                                categoryMaster.IsActive = true;
                                categoryMaster.UpdatedBy = userId.ToString();
                                categoryMaster.UpdatedDate = DateTime.Now;
                                db.Entry(categoryMaster).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet, Route("DeletedCategoryList")]
        public ActionResult DeletedCategoryList()
        {
            List<CategoryMaster> categoryMasters = new List<CategoryMaster>();
            try
            {
                #region --> Page Permission on Action and controller name | Dhrumil Patel | 22032018
                var permissionlst = Session[SessionClass.LinkPermissionList].ToString();
                List<UserPermissionVM> userPermissionLst = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserPermissionVM>>(permissionlst);

                PermissionVM permissionVM = new PermissionVM();
                permissionVM.HasRestorePermisssion = userPermissionLst.Where(x => x.ActionName.Trim() == "RestoreSelectedCategory" && x.ControllerName.Trim() == "Administration").Count() > 0;
                Session["PermissionListSession"] = permissionVM;
                #endregion
                using (db = new DBEntities())
                {
                    categoryMasters = db.CategoryMasters.Where(x => x.IsDelete).OrderByDescending(j => j.Id).ToList();
                }
            }
            catch
            {
                throw;
            }
            return View(categoryMasters);
        }

        #region Restore Selected Category | Add | Dhrumil Patel | 22032018
        [HttpPost, Route("RestoreSelectedCategory"), AuditLog(EnumList.EnumLogType.UpdateData, "RestoreSelectedCategory")]
        public JsonResult RestoreSelectedCategory(string[] categoryIds)
        {
            CategoryMaster categoryMaster = new CategoryMaster();
            try
            {
                if (categoryIds != null)
                {
                    if (categoryIds.Length > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();

                            foreach (var item in categoryIds)
                            {
                                long id = Convert.ToInt64(item);

                                categoryMaster = db.CategoryMasters.Find(id);
                                categoryMaster.IsActive = true;
                                categoryMaster.IsDelete = false;
                                categoryMaster.UpdatedBy = userId.ToString();
                                categoryMaster.UpdatedDate = DateTime.Now;
                                db.Entry(categoryMaster).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        #endregion

        #region --> SubCategory Management | Add | Jasmin Vohra | 01032018
        [HttpGet, Route("SubCategoryList")]
        public ActionResult SubCategoryList()
        {
            List<SubCategoryVM> subCategoryVMs = new List<SubCategoryVM>();
            try
            {
                #region --> Page Permission on Action and controller name | Jasmin | 21032018

                var permissionlst = Session[SessionClass.LinkPermissionList].ToString();
                List<UserPermissionVM> userPermissionLst = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserPermissionVM>>(permissionlst);

                PermissionVM permissionVM = new PermissionVM();

                permissionVM.HasAddPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "AddSubCategory" && x.ControllerName.Trim() == "Administration").Count() > 0;
                permissionVM.HasEditPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "EditSubCategory" && x.ControllerName.Trim() == "Administration").Count() > 0;
                permissionVM.HasStatusPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "ChangeSubCategoryStatus" && x.ControllerName.Trim() == "Administration").Count() > 0;
                permissionVM.HasActivePermission = userPermissionLst.Where(x => x.ActionName.Trim() == "ActiveSelectedSubCategory" && x.ControllerName.Trim() == "Administration").Count() > 0;
                permissionVM.HasDeactivePermission = userPermissionLst.Where(x => x.ActionName.Trim() == "DeactiveSelectedSubCategory" && x.ControllerName.Trim() == "Administration").Count() > 0;
                permissionVM.HasDeletePermission = userPermissionLst.Where(x => x.ActionName.Trim() == "DeleteSelectedSubCategory" && x.ControllerName.Trim() == "Administration").Count() > 0;

                Session["PermissionListSession"] = permissionVM;
                #endregion

                using (db = new DBEntities())
                {
                    subCategoryVMs = db.SubCategoryMasters.Where(x => !x.IsDelete).Select(j => new SubCategoryVM
                    {
                        Id = j.Id,
                        Name = j.Name,
                        CategoryId = j.CategoryId,
                        CategoryName = db.CategoryMasters.Where(x => x.Id == j.CategoryId).FirstOrDefault().Name,
                        IsActive = j.IsActive
                    }).OrderByDescending(j => j.Id).ToList();
                }
            }
            catch
            {
                throw;
            }
            return View(subCategoryVMs);
        }

        [HttpGet, Route("AddSubCategory")]
        public ActionResult AddSubCategory()
        {
            SubCategoryMaster subCategoryMaster = new SubCategoryMaster();
            return View(subCategoryMaster);
        }

        [HttpGet, Route("EditSubCategory")]
        public ActionResult EditSubCategory(long id)
        {
            SubCategoryMaster subCategoryMaster = new SubCategoryMaster();
            try
            {
                if (id > 0)
                {
                    using (db = new DBEntities())
                    {
                        subCategoryMaster = db.SubCategoryMasters.Where(m => m.Id == id).FirstOrDefault();
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return View("AddSubCategory", subCategoryMaster);
        }

        public JsonResult CheckSubCategory(string SubCategoryName, long CategoryID, long SubCategoryID)
        {
            List<SubCategoryMaster> subCategoryMasters = new List<SubCategoryMaster>();
            try
            {
                using (db = new DBEntities())
                {
                    if (SubCategoryID > 0)
                    {
                        subCategoryMasters = db.SubCategoryMasters.Where(j => j.Name.ToLower().Trim() == SubCategoryName.ToLower().Trim() && j.CategoryId == CategoryID && j.Id != SubCategoryID).ToList();
                    }
                    else
                    {
                        subCategoryMasters = db.SubCategoryMasters.Where(j => j.Name.ToLower().Trim() == SubCategoryName.ToLower().Trim() && j.CategoryId == CategoryID).ToList();
                    }

                    if (subCategoryMasters.Count() > 0)
                    {
                        return Json(false, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(true, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, Route("AddSubCategory")]
        [AuditLog(EnumList.EnumLogType.SaveData, "AddSubCategory")]
        public ActionResult AddSubCategory(SubCategoryMaster data)
        {
            SubCategoryMaster subCategoryMaster = new SubCategoryMaster();
            try
            {
                using (db = new DBEntities())
                {
                    long userId = CommonFunctions.GetUserId();

                    if (data.Id > 0)
                    {
                        subCategoryMaster = db.SubCategoryMasters.Where(m => m.Id == data.Id).FirstOrDefault();
                        subCategoryMaster.CategoryId = data.CategoryId;
                        subCategoryMaster.Name = data.Name.Trim();
                        subCategoryMaster.UpdatedBy = userId.ToString();
                        subCategoryMaster.UpdatedDate = DateTime.Now;
                        db.Entry(subCategoryMaster).State = EntityState.Modified;
                        db.SaveChanges();

                        TempData["SuccessMSG"] = Messages.SubCategory_updated_successfully;
                    }
                    else
                    {
                        subCategoryMaster.Name = data.Name.Trim();
                        subCategoryMaster.CategoryId = data.CategoryId;
                        subCategoryMaster.IsActive = true;
                        subCategoryMaster.IsDelete = false;
                        subCategoryMaster.CreatedBy = userId.ToString();
                        subCategoryMaster.CreatedDate = DateTime.Now;
                        db.SubCategoryMasters.Add(subCategoryMaster);
                        db.SaveChanges();

                        TempData["SuccessMSG"] = Messages.SubCategory_added_successfully;
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return RedirectToAction("SubCategoryList", "Administration");
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "ChangeSubCategoryStatus")]
        public JsonResult ChangeSubCategoryStatus(long id)
        {
            SubCategoryMaster subCategoryMaster = new SubCategoryMaster();
            try
            {
                if (id > 0)
                {
                    using (db = new DBEntities())
                    {
                        long userId = CommonFunctions.GetUserId();
                        subCategoryMaster = db.SubCategoryMasters.Where(m => m.Id == id).FirstOrDefault();
                        if (subCategoryMaster.IsActive)
                        {
                            if (CommonFunctions.IsFileAssigned(id.ToString(), (int)EnumList.EnumMasterType.SubCategory))
                            {
                                return Json(new { success = false, message = "SubCategory is already in use" }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                subCategoryMaster.IsActive = false;
                            }
                        }
                        else
                        {
                            if (db.CategoryMasters.Where(j => j.Id == subCategoryMaster.CategoryId && (!j.IsActive || j.IsDelete)).Count() > 0)
                            {
                                return Json(new { success = false, message = "Related category is inactive or deleted.. Active or Restore it first" }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                subCategoryMaster.IsActive = true;
                            }
                        }
                        subCategoryMaster.UpdatedBy = userId.ToString();
                        subCategoryMaster.UpdatedDate = DateTime.Now;
                        db.Entry(subCategoryMaster).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [AuditLog(EnumList.EnumLogType.DeleteData, "DeleteSelectedSubCategory")]
        public JsonResult DeleteSelectedSubCategory(string[] subcategoryIds)
        {
            bool isAssigned = true;
            SubCategoryMaster subCategoryMaster = new SubCategoryMaster();
            try
            {
                if (subcategoryIds != null)
                {
                    if (subcategoryIds.Length > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();

                            isAssigned = CommonFunctions.IsFileAssigned(string.Join(",", subcategoryIds), (int)EnumList.EnumMasterType.SubCategory);

                            if (!isAssigned)
                            {
                                foreach (var item in subcategoryIds)
                                {
                                    long id = Convert.ToInt64(item);

                                    subCategoryMaster = db.SubCategoryMasters.Where(m => m.Id == id).FirstOrDefault();

                                    subCategoryMaster.IsActive = false;
                                    subCategoryMaster.IsDelete = true;
                                    subCategoryMaster.UpdatedBy = userId.ToString();
                                    subCategoryMaster.UpdatedDate = DateTime.Now;
                                    db.Entry(subCategoryMaster).State = EntityState.Modified;
                                    db.SaveChanges();

                                }
                                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "DeactiveSelectedSubCategory")]
        public JsonResult DeactiveSelectedSubCategory(string[] subcategoryIds)
        {
            bool isAssigned = true;
            SubCategoryMaster subCategoryMaster = new SubCategoryMaster();
            try
            {
                if (subcategoryIds != null)
                {
                    if (subcategoryIds.Length > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();

                            isAssigned = CommonFunctions.IsFileAssigned(string.Join(",", subcategoryIds), (int)EnumList.EnumMasterType.SubCategory);

                            if (!isAssigned)
                            {
                                foreach (var item in subcategoryIds)
                                {
                                    long id = Convert.ToInt64(item);

                                    subCategoryMaster = db.SubCategoryMasters.Where(m => m.Id == id).FirstOrDefault();

                                    subCategoryMaster.IsActive = false;
                                    subCategoryMaster.UpdatedBy = userId.ToString();
                                    subCategoryMaster.UpdatedDate = DateTime.Now;
                                    db.Entry(subCategoryMaster).State = EntityState.Modified;
                                    db.SaveChanges();

                                }
                                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "ActiveSelectedSubCategory")]
        public JsonResult ActiveSelectedSubCategory(string[] subcategoryIds)
        {
            SubCategoryMaster subCategoryMaster = new SubCategoryMaster();
            try
            {
                if (subcategoryIds != null)
                {
                    if (subcategoryIds.Length > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();

                            foreach (var item in subcategoryIds)
                            {
                                long id = Convert.ToInt64(item);

                                subCategoryMaster = db.SubCategoryMasters.Where(m => m.Id == id).FirstOrDefault();

                                if (db.CategoryMasters.Where(j => j.Id == subCategoryMaster.CategoryId && (!j.IsActive || j.IsDelete)).Count() > 0)
                                {
                                    return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                                }

                                subCategoryMaster.IsActive = true;
                                subCategoryMaster.UpdatedBy = userId.ToString();
                                subCategoryMaster.UpdatedDate = DateTime.Now;
                                db.Entry(subCategoryMaster).State = EntityState.Modified;
                                db.SaveChanges();

                            }
                            return Json(new { success = true }, JsonRequestBehavior.AllowGet);

                        }
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet, Route("DeletedSubCategoryList")]
        public ActionResult DeletedSubCategoryList()
        {
            List<SubCategoryVM> subCategoryVMs = new List<SubCategoryVM>();
            try
            {
                #region --> Page Permission on Action and controller name | Dhrumil Patel | 22032018
                var permissionlst = Session[SessionClass.LinkPermissionList].ToString();
                List<UserPermissionVM> userPermissionLst = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserPermissionVM>>(permissionlst);

                PermissionVM permissionVM = new PermissionVM();
                permissionVM.HasRestorePermisssion = userPermissionLst.Where(x => x.ActionName.Trim() == "RestoreSelectedSubCategory" && x.ControllerName.Trim() == "Administration").Count() > 0;
                Session["PermissionListSession"] = permissionVM;
                #endregion

                using (db = new DBEntities())
                {
                    subCategoryVMs = db.SubCategoryMasters.Where(x => x.IsDelete).Select(j => new SubCategoryVM
                    {
                        Id = j.Id,
                        Name = j.Name,
                        CategoryId = j.CategoryId,
                        CategoryName = db.CategoryMasters.Where(x => x.Id == j.CategoryId).FirstOrDefault().Name,
                        IsActive = j.IsActive
                    }).OrderByDescending(j => j.Id).ToList();
                }
            }
            catch
            {
                throw;
            }
            return View(subCategoryVMs);
        }

        #region Restore Selected SubCategory | Add | Dhrumil Patel | 22032018
        [HttpPost, Route("RestoreSelectedSubCategory"), AuditLog(EnumList.EnumLogType.UpdateData, "RestoreSelectedSubCategory")]
        public JsonResult RestoreSelectedSubCategory(string[] subcategoryIds)
        {
            SubCategoryMaster subcategoryMaster = new SubCategoryMaster();
            try
            {
                if (subcategoryIds != null)
                {
                    if (subcategoryIds.Length > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();

                            foreach (var item in subcategoryIds)
                            {
                                long id = Convert.ToInt64(item);

                                subcategoryMaster = db.SubCategoryMasters.Find(id);
                                if (db.CategoryMasters.Where(j => j.Id == subcategoryMaster.CategoryId && (!j.IsActive || j.IsDelete)).Count() > 0)
                                {
                                    return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                                }
                                subcategoryMaster.IsActive = true;
                                subcategoryMaster.IsDelete = false;
                                subcategoryMaster.UpdatedBy = userId.ToString();
                                subcategoryMaster.UpdatedDate = DateTime.Now;
                                db.Entry(subcategoryMaster).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        #endregion

        #region --> Cabinet Management | Add | Jasmin Vohra | 01032018
        [HttpGet, Route("CabinetList")]
        public ActionResult CabinetList()
        {
            List<CabinetMaster> cabinetMasters = new List<CabinetMaster>();
            try
            {
                #region --> Page Permission on Action and controller name | Jasmin | 21032018

                var permissionlst = Session[SessionClass.LinkPermissionList].ToString();
                List<UserPermissionVM> userPermissionLst = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserPermissionVM>>(permissionlst);

                PermissionVM permissionVM = new PermissionVM();

                permissionVM.HasAddPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "AddCabinet" && x.ControllerName.Trim() == "Administration").Count() > 0;
                permissionVM.HasEditPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "EditCabinet" && x.ControllerName.Trim() == "Administration").Count() > 0;
                permissionVM.HasStatusPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "ChangeCabinetStatus" && x.ControllerName.Trim() == "Administration").Count() > 0;
                permissionVM.HasActivePermission = userPermissionLst.Where(x => x.ActionName.Trim() == "ActiveSelectedCabinet" && x.ControllerName.Trim() == "Administration").Count() > 0;
                permissionVM.HasDeactivePermission = userPermissionLst.Where(x => x.ActionName.Trim() == "DeactiveSelectedCabinet" && x.ControllerName.Trim() == "Administration").Count() > 0;
                permissionVM.HasDeletePermission = userPermissionLst.Where(x => x.ActionName.Trim() == "DeleteSelectedCabinet" && x.ControllerName.Trim() == "Administration").Count() > 0;

                Session["PermissionListSession"] = permissionVM;
                #endregion

                using (db = new DBEntities())
                {
                    cabinetMasters = db.CabinetMasters.Where(x => !x.IsDelete).OrderByDescending(j => j.Id).ToList();
                }
            }
            catch
            {
                throw;
            }
            return View(cabinetMasters);
        }

        [HttpGet, Route("AddCabinet")]
        public ActionResult AddCabinet()
        {
            CabinetMaster cabinetMaster = new CabinetMaster();
            return View(cabinetMaster);
        }

        [HttpGet, Route("EditCabinet")]
        public ActionResult EditCabinet(long id = 0)
        {
            CabinetMaster cabinetMaster = new CabinetMaster();
            try
            {
                if (id > 0)
                {
                    using (db = new DBEntities())
                    {
                        cabinetMaster = db.CabinetMasters.Where(m => m.Id == id).FirstOrDefault();
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return View("AddCabinet", cabinetMaster);
        }

        [HttpPost, Route("AddCabinet")]
        [AuditLog(EnumList.EnumLogType.SaveData, "AddCabinet")]
        public ActionResult AddCabinet(CabinetMaster data)
        {
            CabinetMaster cabinetMaster = new CabinetMaster();
            try
            {
                long userId = CommonFunctions.GetUserId();
                string UDID = CommonFunctions.GetUDIDForCabinet();
                using (db = new DBEntities())
                {
                    if (data.Id > 0)
                    {
                        cabinetMaster = db.CabinetMasters.Where(m => m.Id == data.Id).FirstOrDefault();
                        cabinetMaster.Name = data.Name.Trim();
                        cabinetMaster.Notes = data.Notes.Trim();
                        cabinetMaster.Keywords = data.Keywords;
                        cabinetMaster.UpdatedBy = userId.ToString();
                        cabinetMaster.UpdatedDate = DateTime.Now;
                        db.Entry(cabinetMaster).State = EntityState.Modified;
                        db.SaveChanges();

                        TempData["SuccessMSG"] = Messages.Cabinet_updated_successfully;
                    }
                    else
                    {
                        cabinetMaster.UDID = UDID;
                        cabinetMaster.Name = data.Name.Trim();
                        cabinetMaster.Notes = data.Notes.Trim();
                        cabinetMaster.Keywords = data.Keywords;
                        cabinetMaster.IsActive = true;
                        cabinetMaster.IsDelete = false;
                        cabinetMaster.CreatedBy = userId.ToString();
                        cabinetMaster.CreatedDate = DateTime.Now;
                        db.CabinetMasters.Add(cabinetMaster);
                        db.SaveChanges();

                        TempData["SuccessMSG"] = Messages.Cabinet_added_successfully;
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return RedirectToAction("CabinetList", "Administration");
        }

        public JsonResult CheckCabinet(string CabinetName, long CabinetID)
        {
            List<CabinetMaster> cabinetMasters = new List<CabinetMaster>();
            try
            {
                using (db = new DBEntities())
                {
                    if (CabinetID > 0)
                    {
                        cabinetMasters = db.CabinetMasters.Where(j => j.Name.ToLower().Trim() == CabinetName.ToLower().Trim() && j.Id != CabinetID).ToList();
                    }
                    else
                    {
                        cabinetMasters = db.CabinetMasters.Where(j => j.Name.ToLower().Trim() == CabinetName.ToLower().Trim()).ToList();
                    }
                    if (cabinetMasters.Count() > 0)
                    {
                        return Json(false, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(true, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "ChangeCabinetStatus")]
        public JsonResult ChangeCabinetStatus(long id)
        {
            CabinetMaster cabinetMaster = new CabinetMaster();
            try
            {
                if (id > 0)
                {
                    using (db = new DBEntities())
                    {
                        long userId = CommonFunctions.GetUserId();
                        cabinetMaster = db.CabinetMasters.Where(m => m.Id == id).FirstOrDefault();

                        if (cabinetMaster.IsActive)
                        {
                            if (CommonFunctions.IsUserAssigned(id.ToString(), (int)EnumList.EnumMasterType.Cabinet))
                            {
                                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                cabinetMaster.IsActive = false;
                            }
                        }
                        else
                        {
                            cabinetMaster.IsActive = true;
                        }
                        cabinetMaster.UpdatedBy = userId.ToString();
                        cabinetMaster.UpdatedDate = DateTime.Now;
                        db.Entry(cabinetMaster).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [AuditLog(EnumList.EnumLogType.DeleteData, "DeleteSelectedCabinet")]
        public JsonResult DeleteSelectedCabinet(string[] cabinetIds)
        {
            bool isAssigned = true;
            CabinetMaster cabinetMaster = new CabinetMaster();
            try
            {
                if (cabinetIds != null)
                {
                    if (cabinetIds.Length > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();

                            isAssigned = CommonFunctions.IsUserAssigned(string.Join(",", cabinetIds), (int)EnumList.EnumMasterType.Cabinet);

                            if (!isAssigned)
                            {
                                foreach (var item in cabinetIds)
                                {
                                    long id = Convert.ToInt64(item);

                                    cabinetMaster = db.CabinetMasters.Where(m => m.Id == id).FirstOrDefault();

                                    cabinetMaster.IsActive = false;
                                    cabinetMaster.IsDelete = true;
                                    cabinetMaster.UpdatedBy = userId.ToString();
                                    cabinetMaster.UpdatedDate = DateTime.Now;
                                    db.Entry(cabinetMaster).State = EntityState.Modified;
                                    db.SaveChanges();

                                }
                                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "DeactiveSelectedCabinet")]
        public JsonResult DeactiveSelectedCabinet(string[] cabinetIds)
        {
            bool isAssigned = true;
            CabinetMaster cabinetMaster = new CabinetMaster();
            try
            {
                if (cabinetIds != null)
                {
                    if (cabinetIds.Length > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();

                            isAssigned = CommonFunctions.IsUserAssigned(string.Join(",", cabinetIds), (int)EnumList.EnumMasterType.Cabinet);

                            if (!isAssigned)
                            {
                                foreach (var item in cabinetIds)
                                {
                                    long id = Convert.ToInt64(item);

                                    cabinetMaster = db.CabinetMasters.Where(m => m.Id == id).FirstOrDefault();

                                    cabinetMaster.IsActive = false;
                                    cabinetMaster.UpdatedBy = userId.ToString();
                                    cabinetMaster.UpdatedDate = DateTime.Now;
                                    db.Entry(cabinetMaster).State = EntityState.Modified;
                                    db.SaveChanges();

                                }
                                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "ActiveSelectedCabinet")]
        public JsonResult ActiveSelectedCabinet(string[] cabinetIds)
        {
            CabinetMaster cabinetMaster = new CabinetMaster();
            try
            {
                if (cabinetIds != null)
                {
                    if (cabinetIds.Length > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();

                            foreach (var item in cabinetIds)
                            {
                                long id = Convert.ToInt64(item);

                                cabinetMaster = db.CabinetMasters.Where(m => m.Id == id).FirstOrDefault();

                                cabinetMaster.IsActive = true;
                                cabinetMaster.UpdatedBy = userId.ToString();
                                cabinetMaster.UpdatedDate = DateTime.Now;
                                db.Entry(cabinetMaster).State = EntityState.Modified;
                                db.SaveChanges();

                            }
                            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet, Route("DeletedCabinetList")]
        public ActionResult DeletedCabinetList()
        {
            List<CabinetMaster> cabinetMasters = new List<CabinetMaster>();
            try
            {
                #region --> Page Permission on Action and controller name | Jasmin | 22032018

                var permissionlst = Session[SessionClass.LinkPermissionList].ToString();
                List<UserPermissionVM> userPermissionLst = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserPermissionVM>>(permissionlst);

                PermissionVM permissionVM = new PermissionVM();

                permissionVM.HasRestorePermisssion = userPermissionLst.Where(x => x.ActionName.Trim() == "RestoreSelectedCabinet" && x.ControllerName.Trim() == "Administration").Count() > 0;

                Session["PermissionListSession"] = permissionVM;
                #endregion

                using (db = new DBEntities())
                {
                    cabinetMasters = db.CabinetMasters.Where(x => x.IsDelete).OrderByDescending(j => j.Id).ToList();
                }
            }
            catch
            {
                throw;
            }
            return View(cabinetMasters);
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "RestoreSelectedCabinet")]
        public JsonResult RestoreSelectedCabinet(string[] cabinetIds)
        {
            CabinetMaster cabinetMaster = new CabinetMaster();
            try
            {
                if (cabinetIds != null)
                {
                    if (cabinetIds.Length > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();

                            foreach (var item in cabinetIds)
                            {
                                long id = Convert.ToInt64(item);

                                cabinetMaster = db.CabinetMasters.Where(m => m.Id == id).FirstOrDefault();

                                cabinetMaster.IsActive = true;
                                cabinetMaster.IsDelete = false;
                                cabinetMaster.UpdatedBy = userId.ToString();
                                cabinetMaster.UpdatedDate = DateTime.Now;
                                db.Entry(cabinetMaster).State = EntityState.Modified;
                                db.SaveChanges();

                            }
                            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region --> User Registration | Add | Dhrumil Patel | 08022018
        [HttpPost, Route("AddUser")]
        [AuditLog(EnumList.EnumLogType.SaveData, "AddUser")]
        public JsonResult AddUser(UserVM detail)
        {
            bool IsAdmin = false, IsUser = false;
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    #region Login credential insert                    
                    var userdetail = new LoginCredential();
                    userdetail.FirstName = detail.FirstName;
                    userdetail.MiddleName = "";
                    userdetail.LastName = detail.LastName;
                    userdetail.Gender = detail.Gender;
                    userdetail.EmailId = detail.EmailId;
                    userdetail.DepartmentId = detail.DepartmentId;
                    userdetail.MobileNumber = "";
                    userdetail.DOB = DateTime.Now;
                    userdetail.CreatedBy = CommonFunctions.GetUserId().ToString();
                    userdetail.SentNotification = false;
                    userdetail.IsActive = true;
                    userdetail.IsDelete = false;
                    userdetail.CreatedDate = DateTime.Now;
                    db.LoginCredentials.Add(userdetail);
                    db.SaveChanges();
                    detail.UserId = userdetail.Id;
                    #endregion

                    #region User role insert
                    if (detail.Roles != null)
                    {
                        UserRole userRole = new UserRole();
                        foreach (var roleid in detail.Roles)
                        {
                            if (roleid != "")
                            {
                                IsAdmin = IsAdmin == true ? true : (roleid == "1" ? true : false);
                                IsUser = IsUser == true ? true : (roleid != "1" ? true : false);
                                userRole.UserId = detail.UserId;
                                userRole.RoleId = Convert.ToInt64(roleid);
                                db.UserRoles.Add(userRole);
                                db.SaveChanges();
                            }
                        }
                    }
                    #endregion

                    #region User login detail insert
                    if (IsAdmin)
                    {
                        var userLoginDetail = new UserLoginDetail();
                        userLoginDetail.Password = UtilityCommonFunctions.Encrypt(detail.Password);
                        userLoginDetail.UserName = detail.UserName;
                        userLoginDetail.ApplicationId = (byte)EnumList.Application.Admin;
                        userLoginDetail.UserId = detail.UserId;
                        userLoginDetail.LastLogin = DateTime.Now;
                        db.UserLoginDetails.Add(userLoginDetail);
                        db.SaveChanges();

                        #region --> Add Password in PasswordHistory
                        PasswordHistory pswHistory = new PasswordHistory();
                        pswHistory.UserId = detail.UserId; ;
                        pswHistory.ApplicationId = (byte)EnumList.Application.Admin;
                        pswHistory.Password = userLoginDetail.Password;
                        pswHistory.ChangeDate = DateTime.Now;
                        db.PasswordHistories.Add(pswHistory);
                        db.SaveChanges();
                        #endregion
                    }
                    if (IsUser)
                    {
                        var userLoginDetail = new UserLoginDetail();
                        userLoginDetail.Password = UtilityCommonFunctions.Encrypt(detail.Password);
                        userLoginDetail.UserName = detail.UserName;
                        userLoginDetail.ApplicationId = (byte)EnumList.Application.User;
                        userLoginDetail.UserId = detail.UserId;
                        userLoginDetail.LastLogin = DateTime.Now;
                        db.UserLoginDetails.Add(userLoginDetail);
                        db.SaveChanges();

                        #region --> Add Password in PasswordHistory
                        PasswordHistory pswHistory = new PasswordHistory();
                        pswHistory.UserId = detail.UserId; ;
                        pswHistory.ApplicationId = (byte)EnumList.Application.User;
                        pswHistory.Password = userLoginDetail.Password;
                        pswHistory.ChangeDate = DateTime.Now;
                        db.PasswordHistories.Add(pswHistory);
                        db.SaveChanges();
                        #endregion
                    }
                    #endregion

                    #region user cabinet detail insert
                    if (detail.UserCabinet != null)
                    {
                        UserCabinetDetail cabinetPermission = new UserCabinetDetail();
                        foreach (var cabinet in detail.UserCabinet)
                        {
                            if (cabinet != "")
                            {
                                cabinetPermission.CabinetId = Convert.ToInt64(cabinet);
                                cabinetPermission.UserId = detail.UserId;
                                db.UserCabinetDetails.Add(cabinetPermission);
                                db.SaveChanges();
                            }
                        }
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    TempData["ErrorMSG"] = Messages.Error_User_Update;
                    return Json(new { Value = "", Text = "" }, JsonRequestBehavior.AllowGet);
                }
                transaction.Commit();
                return Json(new { Value = detail.UserId, Text = detail.FirstName + " " + detail.LastName }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region --> Settings Managment | add | Kiran Sawant | 04032021

        [HttpGet, Route("SettingsList")]
        public ActionResult SettingsList()
        {
            List<SettingsMaster> SettingVMs = new List<SettingsMaster>();
            var CabinetId = CommonFunctions.GetCabinetId();
            var UserId = CommonFunctions.GetUserId();
            var ApplicationId = CommonFunctions.GetApplicationId();
            SortedList sl = new SortedList();
            DataTable dt = new DataTable();
            try
            {
                #region --> Page Permission on Action and controller name | Jasmin | 09042018

                var permissionlst = Session[SessionClass.LinkPermissionList].ToString();
                List<UserPermissionVM> userPermissionLst = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserPermissionVM>>(permissionlst);

                PermissionVM permissionVM = new PermissionVM();

                permissionVM.HasAddPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "AddSettings" && x.ControllerName.Trim() == "Administration").Count() > 0;
                permissionVM.HasEditPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "EditSettings" && x.ControllerName.Trim() == "Administration").Count() > 0;
                permissionVM.HasStatusPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "ChangeWorkflowStatus" && x.ControllerName.Trim() == "Administration").Count() > 0;
                permissionVM.HasActivePermission = userPermissionLst.Where(x => x.ActionName.Trim() == "ActiveSelectedSettings" && x.ControllerName.Trim() == "Administration").Count() > 0;
                permissionVM.HasDeactivePermission = userPermissionLst.Where(x => x.ActionName.Trim() == "DeactiveSelectedSettings" && x.ControllerName.Trim() == "Administration").Count() > 0;
                permissionVM.HasDeletePermission = userPermissionLst.Where(x => x.ActionName.Trim() == "DeleteSelectedSettings" && x.ControllerName.Trim() == "Administration").Count() > 0;

                Session["PermissionListSession"] = permissionVM;
                #endregion

                using (db = new DBEntities())
                {
                    // sl.Add("@WFId", Id);

                    dt = objDAL.GetDataTable("SP_Get_SettingsMaster_List", sl);
                    SettingVMs = objDAL.ConvertToList<SettingsMaster>(dt).ToList();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return View(SettingVMs);
        }

        [HttpGet, Route("AddSettings")]
        public ActionResult AddSettings()
        {
            SettingsDC settings = new SettingsDC();
            return View(settings);
        }

        [HttpGet, Route("EditSettings")]
        public ActionResult EditSettings(long id)
        {
            SettingsDC settings = new SettingsDC();
            List<SettingsDetail> settingsDetails = new List<SettingsDetail>();
            try
            {
                if (id > 0)
                {
                    using (db = new DBEntities())
                    {
                        var settingMaster = db.SettingsMasters.Where(m => m.SettingsId == id).FirstOrDefault();
                        if (settingMaster != null)
                        {
                            var settingMasterDetail = db.SettingsMasterDetails.Where(m => m.SettingsId == id).ToList();
                            if (settingMasterDetail.Count > 0)
                            {
                                settings.SettingsId = Convert.ToInt32(settingMaster.SettingsId);
                                settings.SettingName = settingMaster.SettingName;
                                foreach (var item in settingMasterDetail)
                                {
                                    SettingsDetail detail = new SettingsDetail();
                                    detail.DetailId = item.DetailId;
                                    detail.FieldValue = item.FieldValue;
                                    detail.FieldName = item.FieldName;
                                    settingsDetails.Add(detail);
                                }
                                settings.SettingsDetail=settingsDetails;

                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return View("AddSettings", settings);
        }

        [HttpPost, Route("AddSettings")]
        [AuditLog(EnumList.EnumLogType.SaveData, "AddSettings")]
        public ActionResult AddSettings(SettingsDC data)
        {
            SettingsMasterDetail settingsMasterDetail = new SettingsMasterDetail();
            List<SettingsMasterDetail> settingsMasterDetailList = new List<SettingsMasterDetail>();
            try
            {
                using (db = new DBEntities())
                {
                    long userId = CommonFunctions.GetUserId();

                    if (data.SettingsId > 0)
                    {
                        //var datalist = db.SettingsMasterDetails.Where(m => m.SettingsId == data.SettingsId).ToList();
                        //if (datalist.Count > 0)
                        //{
                        //    db.SettingsMasterDetails.RemoveRange(datalist);
                        //    db.SaveChanges();
                        //}
                        if (data.SettingsDetail.Count > 0)
                        {                            
                            foreach (var item in data.SettingsDetail)
                            {                               
                                var datalist = db.SettingsMasterDetails.Where(m => m.SettingsId == data.SettingsId && m.FieldName == item.FieldName).FirstOrDefault();                               
                                datalist.FieldValue = item.FieldValue.Trim();                                
                                datalist.UpdatedBy = userId.ToString();
                                datalist.UpdatedDate = DateTime.Now;
                                db.Entry(datalist).State = EntityState.Modified;
                                db.SaveChanges();
                            }

                        }

                        TempData["SuccessMSG"] = "Settings updated successfully";
                    }
                    else
                    {
                        if (data.SettingsDetail.Count > 0)
                        {
                            foreach (var item in data.SettingsDetail)
                            {
                                settingsMasterDetail.SettingsId = data.SettingsId;
                                settingsMasterDetail.FieldName = item.FieldName.Trim();
                                settingsMasterDetail.FieldValue = item.FieldValue.Trim();
                                settingsMasterDetail.IsActive = true;
                                settingsMasterDetail.IsDelete = false;
                                settingsMasterDetail.CreatedBy = userId.ToString();
                                settingsMasterDetail.CreatedDate = DateTime.Now;
                                settingsMasterDetail.UpdatedBy = userId.ToString();
                                settingsMasterDetail.UpdatedDate = DateTime.Now;
                                db.SettingsMasterDetails.Add(settingsMasterDetail);
                                db.SaveChanges();
                            }
                        }

                        TempData["SuccessMSG"] = "Settings added successfully";
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return RedirectToAction("SettingsList", "Administration");
        }

        public JsonResult CheckSetting(long SettingsId)
        {
            List<SettingsMasterDetail> SettingsMasterDetail = new List<SettingsMasterDetail>();
            try
            {
                using (db = new DBEntities())
                {
                    if (SettingsId > 0)
                    {
                        SettingsMasterDetail = db.SettingsMasterDetails.Where(j => j.SettingsId == SettingsId && !j.IsDelete).ToList();
                    }

                    if (SettingsMasterDetail.Count() > 0)
                    {
                        return Json(false, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(true, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        #region --> Add Dynamic Settings Step| Add | Kiran | 04032021
        [HttpGet]
        public PartialViewResult _DynamicSettingStep(long Id)
        {
            WFStep dynamicControl = new WFStep();
            dynamicControl.StepNumber = Id;
            return PartialView(dynamicControl != null ? dynamicControl : new WFStep());
        }
        #endregion
        #endregion        
    }
}