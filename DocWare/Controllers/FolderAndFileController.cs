﻿using DocWare.Helpers;
using DocWare.Models;
using DocWare.Resources;
using Ionic.Zip;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Utility.Helpers;
using Utility.Models;

namespace DocWare.Controllers
{
    [CheckAuthorization]
    public class FolderAndFileController : BaseController
    {
        DBEntities db = new DBEntities();
        DAL objDAL = new DAL();
        List<string> parentLst = new List<string>();

        #region --> Folder Management | Add | Jasmin | 09022018
        [HttpGet, Route("AddFolder")]
        public ActionResult AddFolder(long id = 0, long parentFolderId = 0)
        {
            FolderDetailVM folderDetailVM = new FolderDetailVM();
            try
            {
                using (db = new DBEntities())
                {
                    if (id > 0)
                    {
                        folderDetailVM = db.FolderDetails.Where(j => j.Id == id).Select(j => new FolderDetailVM
                        {
                            Id = j.Id,
                            Name = j.Name,
                            FolAssignFormTemplateId = j.AssignFormTemplateId,
                            FolAssignFormTemplateName = j.AssignFormTemplateId > 0 ? db.AssignFormTemplates.Where(x => x.Id == j.AssignFormTemplateId).FirstOrDefault().FormTitle : string.Empty,
                            Notes = j.Notes,
                            Keywords = j.Keywords
                        }).FirstOrDefault();
                    }

                    if (parentFolderId > 0)
                    {
                        folderDetailVM.ParentFolderId = parentFolderId;
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return View(folderDetailVM);
        }

        [HttpGet]
        public JsonResult EditFolder(long id)
        {
            FolderDetailVM folderDetailVM = new FolderDetailVM();
            try
            {
                using (db = new DBEntities())
                {
                    if (id > 0)
                    {
                        folderDetailVM = db.FolderDetails.Where(j => j.Id == id).Select(j => new FolderDetailVM
                        {
                            Id = j.Id,
                            Name = j.Name,
                            FolAssignFormTemplateId = j.AssignFormTemplateId,
                            FolAssignFormTemplateName = j.AssignFormTemplateId > 0 ? db.AssignFormTemplates.Where(x => x.Id == j.AssignFormTemplateId).FirstOrDefault().FormTitle : string.Empty,
                            Notes = j.Notes,
                            Keywords = j.Keywords,
                            ParentFolderId = j.ParentFolderId,
                            Description = j.Description,
                            PhysicalLocation = j.PhysicalLocation,
                            PublicFolder = j.IsPublic
                        }).FirstOrDefault();

                        return Json(new { success = true, folderData = folderDetailVM }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, Route("AddFolder")]
        [AuditLog(EnumList.EnumLogType.SaveData, "AddFolder")]
        public ActionResult AddFolder(FolderDetailVM data)
        {
            FolderDetail folderDetail = new FolderDetail();

            try
            {
                long userId = CommonFunctions.GetUserId();
                long CabinetId = CommonFunctions.GetCabinetId();

                using (db = new DBEntities())
                {
                    if (data.Id > 0)
                    {
                        using (var transaction = db.Database.BeginTransaction())
                        {
                            folderDetail = db.FolderDetails.Where(m => m.Id == data.Id).FirstOrDefault();

                            if (data.FolFormTemplateId > 0)
                            {
                                AssignFormTemplate assignFormTemplate = new AssignFormTemplate();
                                FormTemplate formTemplate = new FormTemplate();

                                formTemplate = db.FormTemplates.Where(j => j.Id == data.FolFormTemplateId).FirstOrDefault();

                                if (data.FolAssignFormTemplateId > 0)
                                {
                                    assignFormTemplate = db.AssignFormTemplates.Where(j => j.Id == data.FolAssignFormTemplateId).FirstOrDefault();

                                    assignFormTemplate.FormTitle = formTemplate.FormTitle;
                                    assignFormTemplate.FormContant = formTemplate.HtmlContent;
                                    assignFormTemplate.UpdatedBy = userId.ToString();
                                    assignFormTemplate.UpdatedDate = DateTime.Now;
                                    db.Entry(assignFormTemplate).State = EntityState.Modified;
                                    db.SaveChanges();

                                    var formField = db.AssignedFormControlProperties.Where(j => j.AssignedFormTemplateId == assignFormTemplate.Id).ToList();
                                    if (formField.Count() > 0)
                                    {
                                        db.AssignedFormControlProperties.RemoveRange(formField);
                                        db.SaveChanges();
                                    }

                                    var formFields = db.FormControlProperties.Where(j => j.FormTemplateId == formTemplate.Id).ToList();

                                    if (formFields != null)
                                    {
                                        AssignedFormControlProperty assignedFormControlProperty = new AssignedFormControlProperty();
                                        foreach (var field in formFields)
                                        {
                                            if (field != null)
                                            {
                                                assignedFormControlProperty.AssignedFormTemplateId = assignFormTemplate.Id;
                                                assignedFormControlProperty.FieldId = field.FieldId;
                                                assignedFormControlProperty.FieldType = field.FieldType;
                                                db.AssignedFormControlProperties.Add(assignedFormControlProperty);
                                                db.SaveChanges();
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    assignFormTemplate.FormTitle = formTemplate.FormTitle;
                                    assignFormTemplate.FormContant = formTemplate.HtmlContent;
                                    assignFormTemplate.CreatedBy = userId.ToString();
                                    assignFormTemplate.CreatedDate = DateTime.Now;
                                    db.AssignFormTemplates.Add(assignFormTemplate);
                                    db.SaveChanges();

                                    data.FolAssignFormTemplateId = assignFormTemplate.Id;

                                    var formFields = db.FormControlProperties.Where(j => j.FormTemplateId == formTemplate.Id).ToList();

                                    if (formFields != null)
                                    {
                                        AssignedFormControlProperty assignedFormControlProperty = new AssignedFormControlProperty();
                                        foreach (var field in formFields)
                                        {
                                            if (field != null)
                                            {
                                                assignedFormControlProperty.AssignedFormTemplateId = assignFormTemplate.Id;
                                                assignedFormControlProperty.FieldId = field.FieldId;
                                                assignedFormControlProperty.FieldType = field.FieldType;
                                                db.AssignedFormControlProperties.Add(assignedFormControlProperty);
                                                db.SaveChanges();
                                            }
                                        }
                                    }
                                }
                            }

                            folderDetail.AssignFormTemplateId = data.FolAssignFormTemplateId;
                            folderDetail.ParentFolderId = data.ParentFolderId;
                            folderDetail.Name = data.Name.Trim();
                            folderDetail.Notes = data.Notes != null ? data.Notes.Trim() : "";
                            folderDetail.Keywords = data.Keywords != null ? data.Keywords : "";
                            folderDetail.Description = data.Description;
                            folderDetail.PhysicalLocation = data.PhysicalLocation;
                            folderDetail.IsPublic = data.PublicFolder;
                            folderDetail.UpdatedBy = userId.ToString();
                            folderDetail.UpdatedDate = DateTime.Now;
                            db.Entry(folderDetail).State = EntityState.Modified;
                            db.SaveChanges();

                            if (CommonFunctions.HasChild(folderDetail.Id) > 0)
                            {
                                ChangeChildPublicStatus(folderDetail.Id, folderDetail.IsPublic);
                            }
                            transaction.Commit();
                            if (!data.PublicFolder)
                            {
                                CommonFunctions.RemoveLinkedDocument();
                                CommonFunctions.RemoveFavouriteDocument();
                            }
                        }
                        return Json(new { success = true, msg = Messages.Folder_Updated }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        using (var transaction = db.Database.BeginTransaction())
                        {
                            string UDID = CommonFunctions.GetUDIDForFolder();

                            if (data.FolFormTemplateId > 0)
                            {
                                AssignFormTemplate assignFormTemplate = new AssignFormTemplate();
                                FormTemplate formTemplate = new FormTemplate();

                                formTemplate = db.FormTemplates.Where(j => j.Id == data.FolFormTemplateId).FirstOrDefault();

                                assignFormTemplate.FormTitle = formTemplate.FormTitle;
                                assignFormTemplate.FormContant = formTemplate.HtmlContent;
                                assignFormTemplate.CreatedBy = userId.ToString();
                                assignFormTemplate.CreatedDate = DateTime.Now;
                                db.AssignFormTemplates.Add(assignFormTemplate);
                                db.SaveChanges();

                                data.FolAssignFormTemplateId = assignFormTemplate.Id;

                                var formFields = db.FormControlProperties.Where(j => j.FormTemplateId == formTemplate.Id).ToList();

                                if (formFields != null)
                                {
                                    AssignedFormControlProperty assignedFormControlProperty = new AssignedFormControlProperty();
                                    foreach (var field in formFields)
                                    {
                                        if (field != null)
                                        {
                                            assignedFormControlProperty.AssignedFormTemplateId = assignFormTemplate.Id;
                                            assignedFormControlProperty.FieldId = field.FieldId;
                                            assignedFormControlProperty.FieldType = field.FieldType;
                                            db.AssignedFormControlProperties.Add(assignedFormControlProperty);
                                            db.SaveChanges();
                                        }
                                    }
                                }
                            }

                            if (data.ParentFolderId > 0)
                            {
                                var parentFolder = db.FolderDetails.Find(data.ParentFolderId);

                                if (parentFolder != null)
                                {
                                    if (parentFolder.IsPublic)
                                    {
                                        data.PublicFolder = true;
                                    }
                                }
                            }

                            folderDetail.UDID = UDID;
                            folderDetail.ParentFolderId = data.ParentFolderId;
                            folderDetail.AssignFormTemplateId = data.FolAssignFormTemplateId;
                            folderDetail.CabinetId = CabinetId;
                            folderDetail.Name = data.Name.Trim();
                            folderDetail.Notes = data.Notes != null ? data.Notes.Trim() : "";
                            folderDetail.Keywords = data.Keywords != null ? data.Keywords : "";
                            folderDetail.Description = data.Description;
                            folderDetail.PhysicalLocation = data.PhysicalLocation;
                            folderDetail.IsPublic = data.PublicFolder;
                            folderDetail.IsActive = true;
                            folderDetail.IsDelete = false;
                            folderDetail.CreatedBy = userId.ToString();
                            folderDetail.CreatedDate = DateTime.Now;
                            db.FolderDetails.Add(folderDetail);
                            db.SaveChanges();

                            if (data.ParentFolderId > 0)
                            {
                                var favDetail = db.Favorites.Where(x => x.UserId == userId && x.FolderId == data.ParentFolderId).ToList();
                                if (favDetail.Count() > 0)
                                {
                                    var fav = new Favorite();
                                    fav.UserId = userId;
                                    fav.FolderId = folderDetail.Id;
                                    fav.DocumentId = 0;
                                    fav.CabinetId = CabinetId;
                                    fav.ParentId = favDetail.FirstOrDefault().Id;
                                    db.Favorites.Add(fav);
                                    db.SaveChanges();
                                }

                                var SharedDetail = db.UserDocShareDetails.Where(x => x.FolderId == data.ParentFolderId).ToList();
                                if (SharedDetail.Any())
                                {
                                    foreach (var dt in SharedDetail)
                                    {
                                        var shareddata = new UserDocShareDetail();
                                        shareddata.UserId = userId;
                                        shareddata.FolderId = folderDetail.Id;
                                        shareddata.DocumentId = 0;
                                        shareddata.DocShareUserId = dt.DocShareUserId;
                                        shareddata.DocShareGroupId = dt.DocShareGroupId;
                                        shareddata.ParentId = dt.Id;
                                        shareddata.CabinetId = dt.CabinetId;
                                        shareddata.IsDownload = dt.IsDownload;
                                        shareddata.IsUpload = dt.IsUpload;
                                        shareddata.IsSeeVersion = dt.IsSeeVersion;
                                        shareddata.IsReadComment = dt.IsReadComment;
                                        shareddata.IsAddComment = dt.IsAddComment;
                                        shareddata.IsShare = dt.IsShare;
                                        shareddata.CreatedBy = userId.ToString();
                                        shareddata.CreatedDate = DateTime.Now;
                                        db.UserDocShareDetails.Add(shareddata);
                                        db.SaveChanges();
                                    }
                                }
                            }
                            transaction.Commit();
                        }
                        return Json(new { success = true, msg = Messages.Folder_Added }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public void ChangeChildPublicStatus(long Id, bool IsPublic)
        {
            try
            {
                var subDocList = db.DocumentDetails.Where(j => j.FolderId == Id).ToList();

                if (subDocList.Count > 0)
                {
                    foreach (var item in subDocList)
                    {
                        var docVersion = db.DocumentVersionDetails.Where(j => j.DocumentId == item.DocumentId).ToList();

                        if (docVersion != null)
                        {
                            foreach (var doc in docVersion)
                            {
                                doc.IsPublic = IsPublic;
                                db.Entry(doc).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                    }
                }

                var subFolderList = db.FolderDetails.Where(j => j.ParentFolderId == Id).ToList();

                if (subFolderList.Count > 0)
                {
                    foreach (var item in subFolderList)
                    {
                        item.IsPublic = IsPublic;
                        db.Entry(item).State = EntityState.Modified;
                        db.SaveChanges();

                        if (CommonFunctions.HasChild(item.Id) > 0)
                        {
                            ChangeChildPublicStatus(item.Id, IsPublic);
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public JsonResult CheckFolderName(string FolderName, long FolderID, long ParentFolderID)
        {
            List<FolderDetail> folderDetails = new List<FolderDetail>();
            try
            {
                using (db = new DBEntities())
                {
                    long CabinetId = CommonFunctions.GetCabinetId();

                    if (FolderID > 0)
                    {
                        folderDetails = db.FolderDetails.Where(j => j.Name.ToLower().Trim() == FolderName.ToLower().Trim() && j.CabinetId == CabinetId && j.ParentFolderId == ParentFolderID && j.Id != FolderID).ToList();
                    }
                    else
                    {
                        folderDetails = db.FolderDetails.Where(j => j.Name.ToLower().Trim() == FolderName.ToLower().Trim() && j.CabinetId == CabinetId && j.ParentFolderId == ParentFolderID).ToList();
                    }

                    if (folderDetails.Count() > 0)
                    {
                        return Json(false, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(true, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region --> Folder Tree View 
        [Route("FolderManagement")]
        public ActionResult FolderTreeView(long openFolderId = 0)
        {
            long CabinetId = CommonFunctions.GetCabinetId();
            ViewBag.CabinetName = db.CabinetMasters.Find(CabinetId).Name;
            ViewBag.OpenFolderId = openFolderId;
            return View();
        }

        public PartialViewResult _FolderDetails(long folderId)
        {
            FolderSubFolderDetail folderDetails = new FolderSubFolderDetail();
            try
            {
                long CabinetId = CommonFunctions.GetCabinetId();
                long UserId = CommonFunctions.GetUserId();
                List<FolderDetail> folderLst = new List<FolderDetail>();
                LstFolderDetailVM lstFolderDetailVMs = new LstFolderDetailVM();
                SortedList sl = new SortedList();
                DataTable dt = new DataTable();
                long ApplicationId = CommonFunctions.GetApplicationId();

                folderDetails.FolderId = folderId;

                if (folderId > 0)
                {
                    sl.Add("@folderId", folderId);
                    if (ApplicationId != (int)EnumList.Application.SuperAdmin)
                        sl.Add("@userId", UserId);

                    dt = objDAL.GetDataTable("uspGetFolderDetails", sl);
                    lstFolderDetailVMs.FolderDetailVMs = objDAL.ConvertToList<FolderFileVM>(dt);
                    folderDetails.FolderName = db.FolderDetails.Find(folderId).Name;
                }
                else
                {
                    if (CabinetId > 0)
                    {
                        sl.Add("@cabinetId", CabinetId);
                        folderDetails.FolderName = db.CabinetMasters.Find(CabinetId).Name;
                    }

                    if (ApplicationId != (int)EnumList.Application.SuperAdmin)
                        sl.Add("@userId", UserId);

                    dt = objDAL.GetDataTable("uspGetFolderDetails", sl);
                    var data = objDAL.ConvertToList<FolderFileVM>(dt);
                    lstFolderDetailVMs.FolderDetailVMs = data.Where(x => x.ParentFolderId == 0).ToList();
                }
                if (lstFolderDetailVMs.FolderDetailVMs.Count() > 0)
                {
                    folderDetails.FolderDetailVMs = lstFolderDetailVMs.FolderDetailVMs;
                }
            }
            catch (Exception e)
            {
                throw;
            }
            return PartialView("_FolderDetails", folderDetails);
        }
        #endregion

        #region --> Folder List
        [HttpGet, Route("FolderList/{id}")]
        public ActionResult FolderList(long id)
        {
            #region --> Page Permission on Action and controller name | Jasmin | 21032018

            var permissionlst = Session[SessionClass.LinkPermissionList].ToString();
            List<UserPermissionVM> userPermissionLst = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserPermissionVM>>(permissionlst);

            FolderPermissionVM permissionVM = new FolderPermissionVM
            {
                HasFolderPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "AddFolder" && x.ControllerName.Trim() == "FolderAndFile").Count() > 0
            };

            Session["PermissionListSession"] = permissionVM;
            #endregion

            var folder = db.FolderDetails.Find(id);
            FolderListVM folderListVM = new FolderListVM
            {
                FolderId = id,
                LastOpenFolderId = folder.ParentFolderId,
                FolderName = folder.Name,
                CabinetName = Session[SessionClass.CabinetName].ToString()
            };
            return View(folderListVM);
        }

        public JsonResult checkIsNotEmpty(long id = 0)
        {
            DBEntities db = new DBEntities();
            if (id > 0)
            {
                var userId = CommonFunctions.GetUserId().ToString();
                var child = db.FolderDetails.Where(x => !x.IsDelete && x.IsActive && x.ParentFolderId == id).ToList().Count;

                if (child == 0)
                {
                    child = db.DocumentDetails.Where(x => x.FolderId == id && !x.IsDelete && x.IsActive).Count();
                }

                if (child > 0)
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public PartialViewResult _FolderList(long folderId, string SearchKeyword = null)
        {
            List<FolderFileVM> folderDetails = new List<FolderFileVM>();
            try
            {
                //if (CommonFunctions.IsCreatedBy(folderId, false)) --Commented this but not able to remember the case.
                //{
                long CabinetId = CommonFunctions.GetCabinetId();
                long UserId = CommonFunctions.GetUserId();
                SortedList sl = new SortedList();
                DataTable dt = new DataTable();

                sl.Add("@cabinetId", CabinetId);
                sl.Add("@folderId", folderId);
                sl.Add("@userId", UserId);

                if (!string.IsNullOrWhiteSpace(SearchKeyword))
                    sl.Add("@searchword", SearchKeyword.Trim());

                dt = objDAL.GetDataTable("uspGetFolderDetails", sl);
                folderDetails = objDAL.ConvertToList<FolderFileVM>(dt);
                //}
            }
            catch (Exception e)
            {
                throw;
            }
            return PartialView("_FolderList", folderDetails);
        }
        #endregion

        #region --> SearchSection | Krupali |31072018
        public PartialViewResult _SearchDocument(long folderId, String FName = null, string catid = null, string Subcatid = null, string Lang = null, string Confidentiality = null, String Keyword = null, String Filetype = null)
        {
            List<FolderFileVM> folderDetails = new List<FolderFileVM>();
            try
            {
                if (CommonFunctions.IsCreatedBy(folderId, false))
                {
                    long CabinetId = CommonFunctions.GetCabinetId();
                    long UserId = CommonFunctions.GetUserId();
                    SortedList sl = new SortedList();
                    DataTable dt = new DataTable();

                    sl.Add("@cabinetId", CabinetId);
                    sl.Add("@folderId", folderId);
                    sl.Add("@userId", UserId);
                    sl.Add("@FileName", FName);
                    sl.Add("@Category", catid);
                    sl.Add("@SubCategory", Subcatid);
                    if (Lang != "")
                    {
                        sl.Add("@Lang", Convert.ToInt32(Lang));
                    }
                    if (Confidentiality != "")
                    {
                        sl.Add("@Confidentiality", Convert.ToInt32(Confidentiality));
                    }
                    if (Keyword != "")
                    {
                        sl.Add("@Keyword", Keyword);
                    }
                    if (Filetype != "")
                    {
                        sl.Add("@Filetype", Filetype);
                    }


                    dt = objDAL.GetDataTable("uspGetFolderDetailsSearch", sl);
                    folderDetails = objDAL.ConvertToList<FolderFileVM>(dt);
                }
            }
            catch (Exception e)
            {
                throw;
            }
            return PartialView("_SearchDocument", folderDetails);
        }

        #endregion

        [Route("SearchFolder")]
        public JsonResult SearchFolder(string FolderName)
        {
            long cabinetId = CommonFunctions.GetCabinetId();
            var folderDetail = db.FolderDetails.FirstOrDefault(x => x.Name.ToLower() == FolderName.ToLower() && x.CabinetId == cabinetId);
            if (folderDetail != null)
            {
                //return RedirectToAction("FolderTreeView", new { openFolderId = folderDetail.Id });
                return Json(new { success = true, folderId = folderDetail.Id }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                TempData["ErrorMSG"] = "Folder name does not exists.";
                return Json(new { success = false, folderId = 0 }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetFolderName(long folderId)
        {
            if (folderId > 0)
            {
                var folderDetail = db.FolderDetails.Where(j => j.Id == folderId).Select(x => new
                {
                    folderId = x.Id,
                    folderName = x.Name,
                    parentFolder = x.ParentFolderId
                }).FirstOrDefault();

                return Json(new { success = true, folderData = folderDetail }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        #region --> Check Permission for folder edit | Add | Dhrumil Patel | 14-06-2018
        public JsonResult EditFolderPermission(long id = 0)
        {
            try
            {
                using (db = new DBEntities())
                {
                    if (id > 0)
                    {
                        if (CommonFunctions.IsCreatedBy(id, false))
                        {
                            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region --> Check Permission for document edit | Add | Dhrumil Patel | 14-06-2018
        public JsonResult EditDocumentPermission(long id = 0)
        {
            bool flag = false;
            try
            {
                using (db = new DBEntities())
                {
                    if (id > 0)
                    {
                        flag = CommonFunctions.IsCheckOut(id);
                        if (CommonFunctions.IsCreatedBy(id, true) && !flag)
                        {
                            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
            if (flag)
            {
                return Json(new { success = false, msg = "checkout" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, msg = "" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region --> Check Permission for folder OR File Dynamic Form Edit/Add | Sweta Patel | 02-07-2018
        public JsonResult EditFolderFilePermission(long id = 0, bool isDocument = true)
        {
            bool flag = false;
            try
            {
                using (db = new DBEntities())
                {
                    if (id > 0)
                    {
                        if (isDocument)
                        {
                            flag = CommonFunctions.IsCheckOut(id);
                            if (CommonFunctions.IsCreatedBy(id, true) && !flag)
                            {
                                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            if (CommonFunctions.IsCreatedBy(id, false))
                            {
                                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                            }
                        }

                    }
                }
            }
            catch (Exception)
            {
                return Json(new { success = false, msg = "" }, JsonRequestBehavior.AllowGet);
            }
            if (flag)
            {
                return Json(new { success = false, msg = "checkout" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, msg = "" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region --> Check Permission for document checkout by user | Add | Dhrumil Patel | 25-07-2018
        public JsonResult IsDocumentCheckOut(long id = 0)
        {
            try
            {
                using (db = new DBEntities())
                {
                    if (id > 0)
                    {
                        if (!CommonFunctions.IsCheckOut(id))
                        {
                            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region --> Document Management | Add | Jasmin | 12032018
        [HttpGet, Route("AddDocument")]
        public ActionResult AddDocument(long id = 0, long parentFolderId = 0)
        {
            DocumentDetailVM documentDetailVM = new DocumentDetailVM();
            long userId = CommonFunctions.GetUserId();
            long applicationId = CommonFunctions.GetApplicationId();
            long cabinetId = CommonFunctions.GetCabinetId();
            try
            {
                if (id > 0)
                {
                    documentDetailVM = (from dd in db.DocumentDetails
                                        let dv = db.DocumentVersionDetails.Where(j => j.DocumentId == id && j.IsActive && !j.IsDelete).OrderByDescending(j => j.Id).FirstOrDefault()
                                        where dd.IsActive == true && dd.IsDelete == false && dd.DocumentId == id
                                        select new DocumentDetailVM
                                        {
                                            DocumentVersionId = dv.Id,
                                            DocumentId = dd.DocumentId,
                                            DocParentFolderId = dd.FolderId,
                                            DocAssignFormTemplateId = dd.AssignFormTemplateId,
                                            DocAssignFormTemplateName = dd.AssignFormTemplateId > 0 ? db.AssignFormTemplates.Where(j => j.Id == dd.AssignFormTemplateId).FirstOrDefault().FormTitle : string.Empty,
                                            DocName = dd.Name,
                                            DocNotes = dv.Notes,
                                            DocKeywords = dv.Keywords,
                                            Language = dv.Language,
                                            Confidentiality = dv.Confidentiality,
                                            DocPublic = dv.IsPublic,
                                            CategoryId = dd.CategoryId,
                                            SubCategoryId = dd.SubCategoryId
                                        }).FirstOrDefault();

                    var Users = db.UserDocShareDetails.Where(a => a.DocumentId == documentDetailVM.DocumentId).Select(x => x.DocShareUserId).ToList();

                    if (Users.Count > 0)
                    {
                        var userList = new List<string>();
                        foreach (var item in Users)
                        {
                            userList.Add(item.ToString());
                        }
                        documentDetailVM.DocShareUserIds = userList.ToArray();
                    }
                }

                if (parentFolderId > 0)
                    documentDetailVM.DocParentFolderId = parentFolderId;

                if (applicationId == (int)EnumList.Application.SuperAdmin)
                {
                    documentDetailVM.DocSharePermission = true;
                }
                else
                {
                    documentDetailVM.DocSharePermission = db.UserDocSharePermissions.Where(j => j.UserId == userId && j.CabinetId == cabinetId).ToList().Count() > 0;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return View(documentDetailVM);
        }

        [HttpGet]
        public JsonResult EditDocument(long id)
        {
            DocumentDetailVM documentDetailVM = new DocumentDetailVM();
            try
            {
                using (db = new DBEntities())
                {
                    if (id > 0) // Update Code for IsCurrent
                    {
                        documentDetailVM = (from dd in db.DocumentDetails
                                            let dv = db.DocumentVersionDetails.Where(j => j.DocumentId == id && j.IsActive && !j.IsDelete && j.IsCurrent).FirstOrDefault()
                                            where dd.IsActive == true && dd.IsDelete == false && dd.DocumentId == id
                                            select new DocumentDetailVM
                                            {
                                                DocumentVersionId = dv.Id,
                                                DocumentId = dd.DocumentId,
                                                DocParentFolderId = dd.FolderId,
                                                DocAssignFormTemplateId = dd.AssignFormTemplateId,
                                                DocAssignFormTemplateName = dd.AssignFormTemplateId > 0 ? db.AssignFormTemplates.Where(j => j.Id == dd.AssignFormTemplateId).FirstOrDefault().FormTitle : string.Empty,
                                                DocName = dd.Name,
                                                DocNotes = dv.Notes,
                                                DocKeywords = dv.Keywords,
                                                DocumentVersion = dv.DocumentVersion,
                                                DocDescription = dv.Description, // Add  By Sweta On 22/05/2015
                                                DocPhysicalLocation = dv.PhysicalLocation,// Add  By Sweta On 22/05/2015
                                                Language = dv.Language,
                                                Confidentiality = dv.Confidentiality,
                                                DocPublic = dv.IsPublic,
                                                CategoryId = dd.CategoryId,
                                                SubCategoryId = dd.SubCategoryId
                                            }).FirstOrDefault();

                        return Json(new { success = true, docData = documentDetailVM }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, Route("AddDocument")]
        [AuditLog(EnumList.EnumLogType.SaveData, "AddDocument")]
        public ActionResult AddDocument(DocumentDetailVM data, HttpPostedFileBase docUpload)
        {
            DocumentDetail documentDetail = new DocumentDetail();
            var Name = string.Empty;
            var UDID = string.Empty;
            var Uploadfile = new { Name = string.Empty, Size = "0 KB" };
            try
            {
                using (db = new DBEntities())
                {
                    long userId = CommonFunctions.GetUserId();
                    long cabinetId = CommonFunctions.GetCabinetId();

                    if (data.DocumentId > 0 && data.DocumentVersionId > 0)
                    {
                        documentDetail = db.DocumentDetails.Where(j => j.DocumentId == data.DocumentId).FirstOrDefault();

                        if (data.DocFormTemplateId > 0)
                        {
                            AssignFormTemplate assignFormTemplate = new AssignFormTemplate();
                            FormTemplate formTemplate = new FormTemplate();

                            formTemplate = db.FormTemplates.Where(j => j.Id == data.DocFormTemplateId).FirstOrDefault();

                            if (data.DocAssignFormTemplateId > 0)
                            {
                                assignFormTemplate = db.AssignFormTemplates.Where(j => j.Id == data.DocAssignFormTemplateId).FirstOrDefault();

                                assignFormTemplate.FormTitle = formTemplate.FormTitle;
                                assignFormTemplate.FormContant = formTemplate.HtmlContent;
                                assignFormTemplate.UpdatedBy = userId.ToString();
                                assignFormTemplate.UpdatedDate = DateTime.Now;
                                db.Entry(assignFormTemplate).State = EntityState.Modified;
                                db.SaveChanges();

                                var formField = db.AssignedFormControlProperties.Where(j => j.AssignedFormTemplateId == assignFormTemplate.Id).ToList();
                                if (formField.Count() > 0)
                                {
                                    db.AssignedFormControlProperties.RemoveRange(formField);
                                    db.SaveChanges();
                                }

                                var formFields = db.FormControlProperties.Where(j => j.FormTemplateId == formTemplate.Id).ToList();

                                if (formFields != null)
                                {
                                    AssignedFormControlProperty assignedFormControlProperty = new AssignedFormControlProperty();
                                    foreach (var field in formFields)
                                    {
                                        if (field != null)
                                        {
                                            assignedFormControlProperty.AssignedFormTemplateId = assignFormTemplate.Id;
                                            assignedFormControlProperty.FieldId = field.FieldId;
                                            assignedFormControlProperty.FieldType = field.FieldType;
                                            db.AssignedFormControlProperties.Add(assignedFormControlProperty);
                                            db.SaveChanges();
                                        }
                                    }
                                }
                            }
                            else
                            {
                                assignFormTemplate.FormTitle = formTemplate.FormTitle;
                                assignFormTemplate.FormContant = formTemplate.HtmlContent;
                                assignFormTemplate.CreatedBy = userId.ToString();
                                assignFormTemplate.CreatedDate = DateTime.Now;
                                db.AssignFormTemplates.Add(assignFormTemplate);
                                db.SaveChanges();

                                data.DocAssignFormTemplateId = assignFormTemplate.Id;

                                var formFields = db.FormControlProperties.Where(j => j.FormTemplateId == formTemplate.Id).ToList();

                                if (formFields != null)
                                {
                                    AssignedFormControlProperty assignedFormControlProperty = new AssignedFormControlProperty();
                                    foreach (var field in formFields)
                                    {
                                        if (field != null)
                                        {
                                            assignedFormControlProperty.AssignedFormTemplateId = assignFormTemplate.Id;
                                            assignedFormControlProperty.FieldId = field.FieldId;
                                            assignedFormControlProperty.FieldType = field.FieldType;
                                            db.AssignedFormControlProperties.Add(assignedFormControlProperty);
                                            db.SaveChanges();
                                        }
                                    }
                                }
                            }
                        }

                        documentDetail.Name = data.DocName.Trim();
                        documentDetail.CategoryId = data.CategoryId;
                        documentDetail.SubCategoryId = data.SubCategoryId;
                        documentDetail.AssignFormTemplateId = data.DocAssignFormTemplateId;
                        documentDetail.UpdatedBy = userId.ToString();
                        documentDetail.UpdatedDate = DateTime.Now;
                        db.Entry(documentDetail).State = EntityState.Modified;
                        db.SaveChanges();

                        var documentVersion = db.DocumentVersionDetails.Where(j => j.DocumentId == data.DocumentId).ToList();
                        if (documentVersion.Count != 0)
                        {
                            foreach (var dv in documentVersion)
                            {
                                if (dv.IsCurrent)
                                {
                                    dv.Notes = data.DocNotes != null ? data.DocNotes : "";
                                    dv.Keywords = data.DocKeywords != null ? data.DocKeywords : "";
                                    dv.Language = data.Language;
                                    dv.Confidentiality = Convert.ToByte(data.Confidentiality);
                                    dv.IsPublic = data.DocPublic;
                                    dv.Description = data.DocDescription; // Add  By Sweta On 22/05/2015
                                    dv.PhysicalLocation = data.DocPhysicalLocation;// Add  By Sweta On 22/05/2015
                                    dv.UpdatedBy = userId.ToString();
                                    dv.UpdatedDate = DateTime.Now;
                                }
                                else
                                {
                                    dv.IsPublic = data.DocPublic;
                                }
                                db.Entry(dv).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                        if (!data.DocPublic)
                        {
                            CommonFunctions.RemoveLinkedDocument();
                            CommonFunctions.RemoveFavouriteDocument();
                        }
                    }
                    else if (data.DocumentId > 0)
                    {
                        var documentVersion = db.DocumentVersionDetails.Where(j => j.DocumentId == data.DocumentId).OrderByDescending(x => x.Id).FirstOrDefault();

                        if (documentVersion != null)
                        {
                            var lastVersion = documentVersion.DocumentVersion;

                            data.DocumentVersion = lastVersion + Convert.ToDouble(0.1);
                        }
                        else
                        {
                            data.DocumentVersion = Convert.ToDouble(1.0);
                        }
                    }
                    else
                    {
                        UDID = CommonFunctions.GetUDIDForDocument();

                        var parentFolder = db.FolderDetails.Find(data.DocParentFolderId);

                        if (parentFolder != null)
                        {
                            if (parentFolder.IsPublic)
                            {
                                data.DocPublic = true;
                            }
                        }

                        if (data.DocFormTemplateId > 0)
                        {
                            AssignFormTemplate assignFormTemplate = new AssignFormTemplate();
                            FormTemplate formTemplate = new FormTemplate();

                            formTemplate = db.FormTemplates.Where(j => j.Id == data.DocFormTemplateId).FirstOrDefault();

                            assignFormTemplate.FormTitle = formTemplate.FormTitle;
                            assignFormTemplate.FormContant = formTemplate.HtmlContent;
                            assignFormTemplate.CreatedBy = userId.ToString();
                            assignFormTemplate.CreatedDate = DateTime.Now;
                            db.AssignFormTemplates.Add(assignFormTemplate);
                            db.SaveChanges();

                            data.DocAssignFormTemplateId = assignFormTemplate.Id;

                            var formFields = db.FormControlProperties.Where(j => j.FormTemplateId == formTemplate.Id).ToList();

                            if (formFields != null)
                            {
                                AssignedFormControlProperty assignedFormControlProperty = new AssignedFormControlProperty();
                                foreach (var field in formFields)
                                {
                                    if (field != null)
                                    {
                                        assignedFormControlProperty.AssignedFormTemplateId = assignFormTemplate.Id;
                                        assignedFormControlProperty.FieldId = field.FieldId;
                                        assignedFormControlProperty.FieldType = field.FieldType;
                                        db.AssignedFormControlProperties.Add(assignedFormControlProperty);
                                        db.SaveChanges();
                                    }
                                }
                            }
                        }

                        documentDetail.UDID = UDID;
                        documentDetail.FolderId = data.DocParentFolderId;
                        documentDetail.Name = data.DocName.Trim();
                        documentDetail.CategoryId = data.CategoryId;
                        documentDetail.SubCategoryId = data.SubCategoryId;
                        documentDetail.AssignFormTemplateId = data.DocAssignFormTemplateId;
                        documentDetail.IsActive = true;
                        documentDetail.IsDelete = false;
                        documentDetail.IsCheckOut = false;
                        documentDetail.CreatedBy = userId.ToString();
                        documentDetail.CreatedDate = DateTime.Now;
                        documentDetail.IsCheckOut = false;
                        db.DocumentDetails.Add(documentDetail);
                        db.SaveChanges();

                        if (parentFolder != null)
                        {
                            var favDetail = db.Favorites.Where(x => x.UserId == userId && x.FolderId == parentFolder.Id).ToList();
                            if (favDetail.Count() > 0)
                            {
                                var fav = new Favorite();
                                fav.UserId = userId;
                                fav.FolderId = 0;
                                fav.DocumentId = documentDetail.DocumentId;
                                fav.CabinetId = cabinetId;
                                fav.ParentId = favDetail.FirstOrDefault().Id;
                                db.Favorites.Add(fav);
                                db.SaveChanges();
                            }

                            var SharedDetail = db.UserDocShareDetails.Where(x => x.FolderId == parentFolder.Id).ToList();
                            if (SharedDetail.Any())
                            {
                                foreach (var dt in SharedDetail)
                                {
                                    var shareddata = new UserDocShareDetail();
                                    shareddata.UserId = userId;
                                    shareddata.FolderId = 0;
                                    shareddata.DocumentId = documentDetail.DocumentId;
                                    shareddata.DocShareUserId = dt.DocShareUserId;
                                    shareddata.DocShareGroupId = dt.DocShareGroupId;
                                    shareddata.ParentId = dt.Id;
                                    shareddata.CabinetId = dt.CabinetId;
                                    shareddata.IsDownload = dt.IsDownload;
                                    shareddata.IsUpload = dt.IsUpload;
                                    shareddata.IsSeeVersion = dt.IsSeeVersion;
                                    shareddata.IsReadComment = dt.IsReadComment;
                                    shareddata.IsAddComment = dt.IsAddComment;
                                    shareddata.IsShare = dt.IsShare;
                                    shareddata.CreatedBy = userId.ToString();
                                    shareddata.CreatedDate = DateTime.Now;
                                    db.UserDocShareDetails.Add(shareddata);
                                    db.SaveChanges();
                                }
                            }
                        }

                        if (data.DocumentVersion == 0)
                            data.DocumentVersion = Convert.ToDouble(1.0);
                    }

                    if (docUpload != null)
                    {
                        Name = documentDetail.UDID + "_" + data.DocumentVersion;
                        var uriPath = ConfigurationManager.AppSettings["PathForDocUpload"] + userId.ToString() + "/" + cabinetId.ToString() + "/";
                        string folderPath = Server.MapPath(uriPath);

                        if (!Directory.Exists(folderPath))
                        {
                            Directory.CreateDirectory(folderPath.ToString());
                        }
                        try
                        {
                            docUpload.SaveAs(folderPath + Name + Path.GetExtension(docUpload.FileName));
                        }
                        catch
                        {
                            throw;
                        }

                        var docSize = Math.Round((Convert.ToDecimal(docUpload.ContentLength) / Convert.ToDecimal(1024)), 2); //size in KB

                        DocumentVersionDetail documentVersionDetail = new DocumentVersionDetail();
                        documentVersionDetail.DocumentId = documentDetail.DocumentId;
                        documentVersionDetail.DocumentName = Name;
                        documentVersionDetail.Notes = data.DocNotes != null ? data.DocNotes : "";
                        documentVersionDetail.Keywords = data.DocKeywords != null ? data.DocKeywords : "";
                        documentVersionDetail.DocumentType = Path.GetExtension(docUpload.FileName).Split('.')[1];
                        documentVersionDetail.DocumentSize = docSize;
                        documentVersionDetail.DocumentDisplaySize = docSize > 1024 ? (docSize / 1024).ToString("0.00") + " MB" : docSize.ToString() + " KB";
                        documentVersionDetail.DocumentVersion = data.DocumentVersion;
                        documentVersionDetail.Language = data.Language;
                        documentVersionDetail.Confidentiality = Convert.ToByte(data.Confidentiality);
                        documentVersionDetail.IsPublic = data.DocPublic;
                        documentVersionDetail.DocumentPath = uriPath + Name + Path.GetExtension(docUpload.FileName);
                        documentVersionDetail.IsCurrent = true;
                        documentVersionDetail.IsActive = true;
                        documentVersionDetail.IsDelete = false;
                        documentVersionDetail.Description = data.DocDescription; // Add  By Sweta On 22/05/2015
                        documentVersionDetail.PhysicalLocation = data.DocPhysicalLocation;// Add  By Sweta On 22/05/2015
                        documentVersionDetail.CreatedBy = userId.ToString();
                        documentVersionDetail.CreatedDate = DateTime.Now;
                        db.DocumentVersionDetails.Add(documentVersionDetail);
                        db.SaveChanges();

                        Uploadfile = new
                        {
                            Name = docUpload.FileName,
                            Size = documentVersionDetail.DocumentDisplaySize
                        };
                    }
                }

                if (data.DocumentId > 0)
                    return Json(new { success = true, msg = "File Updated Successfully", uploadData = Uploadfile }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = true, msg = "File Uploaded Successfully", uploadData = Uploadfile }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetSubCategoryByCategoryID(long CategoryID)
        {
            try
            {
                List<SelectListItem> lst = CommonFunctions.GetSubCategoryList(CategoryID, 0);
                return Json(new { success = true, lst = lst }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        public JsonResult GetSearchSubCategoryByCategoryID(long CategoryID)
        {
            try
            {
                List<SelectListItem> lst = CommonFunctions.GetSubCategorySearchList(CategoryID, 0);
                return Json(new { success = true, lst = lst }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        public JsonResult CheckDocumentName(string DocumentName, long DocumentID, long FolderID)
        {
            var count = 0;
            try
            {
                using (db = new DBEntities())
                {
                    long cabinetId = CommonFunctions.GetCabinetId();
                    if (FolderID == 0)
                    {
                        FolderID = db.DocumentDetails.FirstOrDefault(x => x.DocumentId == DocumentID).FolderId;
                    }
                    if (DocumentID > 0)
                    {
                        var documentDetails = (from fol in db.FolderDetails
                                               join doc in db.DocumentDetails on fol.Id equals doc.FolderId
                                               join docv in db.DocumentVersionDetails on doc.DocumentId equals docv.DocumentId
                                               where (fol.CabinetId == cabinetId && doc.FolderId == FolderID
                                               && doc.Name.ToLower().Trim() == DocumentName.ToLower().Trim()
                                               && doc.DocumentId != DocumentID)
                                               select new
                                               {
                                                   doc
                                               }).ToList();
                        count = documentDetails.Count();
                    }
                    else
                    {
                        var documentDetails = (from fol in db.FolderDetails
                                               join doc in db.DocumentDetails on fol.Id equals doc.FolderId
                                               join docv in db.DocumentVersionDetails on doc.DocumentId equals docv.DocumentId
                                               where (fol.CabinetId == cabinetId && doc.FolderId == FolderID
                                               && doc.Name.ToLower().Trim() == DocumentName.ToLower().Trim())
                                               select new
                                               {
                                                   doc
                                               }).ToList();
                        count = documentDetails.Count();
                    }

                    if (count > 0)
                    {
                        return Json(false, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(true, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region --> Public Folder And File | Add | Jasmin | 25042018
        [HttpGet, Route("PublicFilesOld")]
        public ActionResult PublicFiles()
        {
            List<FileListVM> fileListVMs = new List<FileListVM>();
            try
            {
                using (db = new DBEntities())
                {
                    SortedList sl = new SortedList();
                    DataTable dt = new DataTable();
                    long CabinetId = CommonFunctions.GetCabinetId();
                    long UserId = CommonFunctions.GetUserId();
                    sl.Add("@cabinetId", CabinetId);
                    sl.Add("@userId", UserId);
                    dt = objDAL.GetDataTable("uspGetPublicFilesByCabinet", sl);
                    fileListVMs = objDAL.ConvertToList<FileListVM>(dt);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return View(fileListVMs);
        }
        #endregion

        #region --> Folder/File Info and Comment | Add | Dhrumil Patel | 02052018
        [HttpGet]
        public PartialViewResult _FolderFileInfo(FolderFileSelected detail)
        {
            FolderFileInfo info = new FolderFileInfo();
            DAL objDAL = new DAL();
            SortedList sl = new SortedList();
            var userIds = CommonFunctions.GetUserId();
            var cabinetId = CommonFunctions.GetCabinetId();
            sl.Add("@UserId", userIds);
            sl.Add("@FolderId", detail.FolderId);
            sl.Add("@DocumentId", detail.DocumentId);
            sl.Add("@FolderIds", detail.FolderIds);
            sl.Add("@FileIds", detail.FileIds);
            sl.Add("@CabinetId", cabinetId);
            info = objDAL.ConvertToList<FolderFileInfo>(objDAL.GetDataTable("uspGetFolderFileInfo", sl)).FirstOrDefault();
            info.SelectedCount = detail.SelectedCount;
            return PartialView(info != null ? info : new FolderFileInfo());
        }

        [HttpGet]
        public PartialViewResult _FolderFileComment(FolderFileSelected detail)
        {
            try
            {
                long UserId = CommonFunctions.GetUserId();

                List<FolderFileComments> comment = new List<FolderFileComments>();
                DAL objDAL = new DAL();
                SortedList sl = new SortedList();
                sl.Add("@FolderId", detail.FolderId);
                sl.Add("@DocumentId", detail.DocumentId);
                sl.Add("@userId", UserId);
                comment = objDAL.ConvertToList<FolderFileComments>(objDAL.GetDataTable("uspGetFolderFileComments", sl));
                return PartialView(comment != null ? comment : new List<FolderFileComments>());
            }
            catch (Exception)
            {
                return PartialView(new List<FolderFileComments>());
            }
        }

        public JsonResult SaveComment(FolderFileSelected detail)
        {
            try
            {
                var notificationdetail = new SaveNotificationVM();
                var uId = CommonFunctions.GetUserId();
                var cId = CommonFunctions.GetCabinetId();
                var data = new FolderFileComment();
                data.Comment = detail.Comment;
                data.CreatedDate = DateTime.Now;
                data.CommentBy = uId;
                if (detail.DocumentId > 0)
                {
                    var doc = db.DocumentVersionDetails.Find(detail.DocumentId);
                    data.DocumentId = doc.DocumentId;
                    data.CreatedBy = doc.CreatedBy;
                    notificationdetail.DocumentId = doc.DocumentId;
                    notificationdetail.TypeFor = "File";
                }
                else
                {
                    var doc = db.FolderDetails.Find(detail.FolderId);
                    data.FolderId = detail.FolderId;
                    data.CreatedBy = doc.CreatedBy;
                    notificationdetail.FolderId = detail.FolderId;
                    notificationdetail.TypeFor = "Folder";
                }
                db.FolderFileComments.Add(data);
                db.SaveChanges();
                notificationdetail.UserId = uId;
                notificationdetail.CabinetId = cId;
                notificationdetail.NotificationType = (int)EnumList.EnumNotificationType.Commnet;
                notificationdetail.Comment = detail.Comment;
                CommonFunctions.SaveNotification(notificationdetail);
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region --> Upload dropzone files with defaults values | Add | Dhrumil Patel | 16052018
        [AuditLog(EnumList.EnumLogType.SaveData, "UploadDefaultDocument")]
        public ActionResult UploadDefaultDocument(long Id)
        {
            bool isSavedSuccessfully = false;
            string fName = "";
            string nameFile = "";
            var UDID = CommonFunctions.GetUDIDForDocument();
            var Name = UDID + "_1";
            var userId = CommonFunctions.GetUserId();
            decimal docSize = 0;
            var cabinetId = CommonFunctions.GetCabinetId();
            string path = "";
            try
            {
                foreach (string fileName in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[fileName];
                    UtilityCommonFunctions.RandomStringGenerator random = new UtilityCommonFunctions.RandomStringGenerator();
                    string rdmText = random.GetRandomString(4, UtilityCommonFunctions.RandomStringGenerator.ALPHANUMERIC_CAPS.ToCharArray());
                    fName = file.FileName;
                    nameFile = Path.GetFileNameWithoutExtension(fName) + "_" + rdmText;
                    if (file != null && file.ContentLength > 0)
                    {
                        var uriPath = ConfigurationManager.AppSettings["PathForDocUpload"] + userId.ToString() + "/" + cabinetId.ToString() + "/";
                        string folderPath = Server.MapPath(uriPath);

                        if (!Directory.Exists(folderPath))
                        {
                            Directory.CreateDirectory(folderPath.ToString());
                        }
                        try
                        {
                            path = folderPath + Name + Path.GetExtension(file.FileName);
                            file.SaveAs(path);
                        }
                        catch
                        {
                            throw;
                        }
                        docSize = Math.Round((Convert.ToDecimal(file.ContentLength) / Convert.ToDecimal(1024)), 2);
                        var parentFolder = db.FolderDetails.Find(Id);

                        var documentDetail = new DocumentDetail();
                        documentDetail.UDID = UDID;
                        documentDetail.FolderId = Id;
                        documentDetail.Name = nameFile;
                        documentDetail.CategoryId = 1;
                        documentDetail.SubCategoryId = 1;
                        documentDetail.AssignFormTemplateId = 0;
                        documentDetail.IsActive = true;
                        documentDetail.IsDelete = false;
                        documentDetail.IsCheckOut = false;
                        documentDetail.CreatedBy = userId.ToString();
                        documentDetail.CreatedDate = DateTime.Now;
                        documentDetail.IsCheckOut = false;
                        db.DocumentDetails.Add(documentDetail);
                        db.SaveChanges();

                        var documentVersion = new DocumentVersionDetail();
                        documentVersion.Notes = "default";
                        documentVersion.Keywords = "default";
                        documentVersion.Language = 1;
                        documentVersion.Confidentiality = 0;
                        documentVersion.DocumentId = documentDetail.DocumentId;
                        documentVersion.DocumentName = Name;
                        documentVersion.DocumentType = Path.GetExtension(fName).Split('.')[1];
                        documentVersion.DocumentSize = docSize;
                        documentVersion.DocumentDisplaySize = docSize > 1024 ? (docSize / 1024).ToString("0.00") + " MB" : docSize.ToString("0.00") + " KB";
                        documentVersion.DocumentVersion = 1;
                        documentVersion.PhysicalLocation = "default";
                        documentVersion.Description = "default";
                        documentVersion.IsCurrent = true;
                        //documentVersion.IsLocked = false;
                        documentVersion.DocumentPath = uriPath + Name + Path.GetExtension(file.FileName);
                        documentVersion.IsActive = true;
                        documentVersion.IsDelete = false;
                        if (parentFolder != null)
                        {
                            if (parentFolder.IsPublic)
                            {
                                documentVersion.IsPublic = true;
                            }
                            else
                            {
                                documentVersion.IsPublic = false;
                            }
                        }
                        documentVersion.CreatedBy = userId.ToString();
                        documentVersion.CreatedDate = DateTime.Now;
                        db.DocumentVersionDetails.Add(documentVersion);
                        db.SaveChanges();

                        if (parentFolder != null)
                        {
                            var favDetail = db.Favorites.Where(x => x.UserId == userId && x.FolderId == parentFolder.Id).ToList();
                            if (favDetail.Count() > 0)
                            {
                                var fav = new Favorite();
                                fav.UserId = userId;
                                fav.FolderId = 0;
                                fav.DocumentId = documentDetail.DocumentId;
                                fav.CabinetId = cabinetId;
                                fav.ParentId = favDetail.FirstOrDefault().Id;
                                db.Favorites.Add(fav);
                                db.SaveChanges();
                            }

                            //Shared Permission addition
                            var SharedDetail = db.UserDocShareDetails.Where(x => x.FolderId == parentFolder.Id).ToList();
                            if (SharedDetail.Any())
                            {
                                foreach (var dt in SharedDetail)
                                {
                                    var lst = new List<UserDocShareDetail>();
                                    var folData = db.DocumentDetails.Find(documentDetail.DocumentId);
                                    lst = db.UserDocShareDetails.Where(x => x.UserId == dt.UserId && x.FolderId == folData.FolderId).ToList();
                                    long parentId = 0;
                                    if (lst.Any())
                                    {
                                        parentId = lst.FirstOrDefault().Id;
                                    }
                                    var shareddata = new UserDocShareDetail();
                                    shareddata.UserId = userId;
                                    shareddata.FolderId = 0;
                                    shareddata.DocumentId = documentDetail.DocumentId;
                                    shareddata.DocShareUserId = dt.DocShareUserId;
                                    shareddata.DocShareGroupId = dt.DocShareGroupId;
                                    shareddata.CabinetId = dt.CabinetId;
                                    shareddata.ParentId = parentId;
                                    shareddata.IsDownload = dt.IsDownload;
                                    shareddata.IsUpload = dt.IsUpload;
                                    shareddata.IsSeeVersion = dt.IsSeeVersion;
                                    shareddata.IsReadComment = dt.IsReadComment;
                                    shareddata.IsAddComment = dt.IsAddComment;
                                    shareddata.IsShare = dt.IsShare;
                                    shareddata.CreatedBy = userId.ToString();
                                    shareddata.CreatedDate = DateTime.Now;
                                    db.UserDocShareDetails.Add(shareddata);
                                    db.SaveChanges();
                                }
                            }
                        }
                        isSavedSuccessfully = true;
                    }
                }
            }
            catch (Exception ex)
            {
                isSavedSuccessfully = false;
            }
            if (isSavedSuccessfully)
            {
                return Json(new { Message = "Success" });
            }
            else
            {
                return Json(new { Message = "Error in saving file" });
            }
        }
        #endregion

        #region --> Insert Record in Deleted table | Add | Dhruvin Patel | 14062018
        public void DeletedEntry(string deletedItem, long id, string createdBy, long userID, long CabinetId, bool isHardDelete)
        {
            DeletedData model = new DeletedData();
            model.DeletedBy = userID.ToString();
            model.CabinetId = CabinetId;
            model.OwnerId = createdBy;
            model.DisplayPath = "";
            if (deletedItem.ToLower() == "file")
            {
                model.DocumentId = id;
            }
            else if (deletedItem.ToLower() == "folder")
            {
                model.FolderId = id;
            }
            model.DeletedDate = DateTime.Now;
            model.IsRecycleBinDeleted = isHardDelete;
            //model.IsAdminDelete = (long)EnumList.Application.User == applicationId ? false : true;
            db.DeletedDatas.Add(model);
            db.SaveChanges();
        }
        #endregion

        #region --> Delete selected folder and file | Add | Dhrumil Patel | 17052018
        [AuditLog(EnumList.EnumLogType.DeleteData, "DeleteSelectedFolderFile")]
        public JsonResult DeleteSelectedFolderFile(string[] folderIds, string[] fileIds)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    long userId = CommonFunctions.GetUserId();
                    long cabinetId = CommonFunctions.GetCabinetId();
                    long applicationId = CommonFunctions.GetApplicationId();
                    if (userId > 0 && cabinetId > 0 && (folderIds != null || fileIds != null))
                    {

                        #region --> Check Creator By | Add | Dhruvin Patel | 14062018
                        if (folderIds != null && folderIds.Length > 0)
                        {
                            using (db = new DBEntities())
                            {
                                foreach (var item in folderIds)
                                {
                                    long id = Convert.ToInt64(item);
                                    if (!CommonFunctions.IsCreatedBy(id, false))
                                    {
                                        return Json(new { success = false, msg = "Selected items not removed, you don't have permission for some of the items." }, JsonRequestBehavior.AllowGet);
                                    }
                                }
                            }
                        }

                        if (fileIds != null && fileIds.Length > 0)
                        {
                            using (db = new DBEntities())
                            {
                                foreach (var item in fileIds)
                                {
                                    long id = Convert.ToInt64(item);
                                    if (!CommonFunctions.IsCreatedBy(id, true))
                                    {
                                        return Json(new { success = false, msg = "Selected items not removed, you don't have permission for some of the items." }, JsonRequestBehavior.AllowGet);
                                    }
                                }
                            }
                        }
                        #endregion

                        //long userId = CommonFunctions.GetUserId();
                        if (folderIds != null && folderIds.Length > 0)
                        {
                            using (db = new DBEntities())
                            {
                                foreach (var item in folderIds)
                                {
                                    long id = Convert.ToInt64(item);
                                    var FodlerDetail = db.FolderDetails.Find(id);
                                    FodlerDetail.IsActive = false;
                                    FodlerDetail.IsDelete = true;
                                    FodlerDetail.UpdatedBy = userId.ToString();
                                    FodlerDetail.UpdatedDate = DateTime.Now;
                                    db.Entry(FodlerDetail).State = EntityState.Modified;
                                    db.SaveChanges();
                                    DeletedEntry("folder", id, FodlerDetail.CreatedBy, userId, cabinetId, false);
                                    if (CommonFunctions.HasChild(id) > 0)
                                    {
                                        ChangeChildDeleteStatus(id, userId);
                                    }
                                }
                                //return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                            }
                        }

                        if (fileIds != null && fileIds.Length > 0)
                        {
                            using (db = new DBEntities())
                            {
                                foreach (var item in fileIds)
                                {
                                    long id = Convert.ToInt64(item);
                                    var FileDetail = db.DocumentDetails.Find(id);
                                    FileDetail.IsActive = false;
                                    FileDetail.IsDelete = true;
                                    FileDetail.UpdatedBy = userId.ToString();
                                    FileDetail.UpdatedDate = DateTime.Now;
                                    db.Entry(FileDetail).State = EntityState.Modified;
                                    db.SaveChanges();
                                    DeletedEntry("file", id, FileDetail.CreatedBy, userId, cabinetId, false);
                                }
                                //return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                    transaction.Commit();
                    CommonFunctions.RemoveLinkedDocument();
                    CommonFunctions.RemoveFavouriteDocument();
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return Json(new { success = false, msg = "Something went wrong, please try again!" }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public void ChangeChildDeleteStatus(long Id, long userId)
        {
            try
            {
                var subDocList = db.DocumentDetails.Where(j => j.FolderId == Id).ToList();
                if (subDocList.Count > 0)
                {
                    foreach (var item in subDocList)
                    {
                        item.IsActive = false;
                        item.IsDelete = true;
                        item.UpdatedBy = userId.ToString();
                        item.UpdatedDate = DateTime.Now;
                        db.Entry(item).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }

                var subFolderList = db.FolderDetails.Where(j => j.ParentFolderId == Id).ToList();

                if (subFolderList.Count > 0)
                {
                    foreach (var item in subFolderList)
                    {
                        item.IsActive = false;
                        item.IsDelete = true;
                        item.UpdatedBy = userId.ToString();
                        item.UpdatedDate = DateTime.Now;
                        db.Entry(item).State = EntityState.Modified;
                        db.SaveChanges();

                        if (CommonFunctions.HasChild(item.Id) > 0)
                        {
                            ChangeChildDeleteStatus(item.Id, userId);
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region --> Add/Remove favorite for selected folder and file | Add | Dhrumil Patel | 17052018
        [AuditLog(EnumList.EnumLogType.UpdateData, "AddRemoveFavorite")]
        public JsonResult AddRemoveFavorite(string[] folderIds, string[] fileIds, string status)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (folderIds != null || fileIds != null)
                    {
                        long userId = CommonFunctions.GetUserId();
                        long cabinetId = CommonFunctions.GetCabinetId();
                        if (folderIds != null && folderIds.Length > 0)
                        {
                            using (db = new DBEntities())
                            {
                                foreach (var item in folderIds)
                                {
                                    Favorite fav = new Favorite();
                                    long id = Convert.ToInt64(item);
                                    if (status == "True")
                                    {
                                        var favDetail = db.Favorites.Where(x => x.UserId == userId && x.FolderId == id).ToList();
                                        if (favDetail.Count > 0)
                                        {
                                            db.Favorites.RemoveRange(favDetail);
                                            db.SaveChanges();
                                        }
                                    }
                                    else
                                    {
                                        var data = db.FolderDetails.Find(id);
                                        long parentId = 0;
                                        if (data.ParentFolderId != 0)
                                        {
                                            var lst = db.Favorites.Where(x => x.UserId == userId && x.FolderId == data.ParentFolderId).ToList();
                                            if (lst.Any())
                                            {
                                                parentId = lst.FirstOrDefault().Id;
                                            }
                                        }
                                        fav.UserId = userId;
                                        fav.FolderId = id;
                                        fav.DocumentId = 0;
                                        fav.ParentId = parentId;
                                        fav.CabinetId = cabinetId;
                                        db.Favorites.Add(fav);
                                        db.SaveChanges();
                                    }

                                    if (CommonFunctions.HasChild(id) > 0)
                                    {
                                        ChangeChildFavoriteStatus(id, userId, cabinetId, fav.Id, status);
                                    }
                                }
                                transaction.Commit();
                                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                            }
                        }

                        if (fileIds != null && fileIds.Length > 0)
                        {
                            using (db = new DBEntities())
                            {
                                if (status == "True")
                                {
                                    var favDetail = db.Favorites.Where(x => x.UserId == userId && fileIds.Contains(x.DocumentId.ToString())).ToList();
                                    if (favDetail.Count > 0)
                                    {
                                        db.Favorites.RemoveRange(favDetail);
                                        db.SaveChanges();
                                    }
                                }
                                else
                                {
                                    List<Favorite> favLst = new List<Favorite>();
                                    foreach (var item in fileIds)
                                    {
                                        long id = Convert.ToInt64(item);
                                        var data = db.DocumentDetails.Find(id);
                                        var lst = db.Favorites.Where(x => x.UserId == userId && x.FolderId == data.FolderId).ToList();
                                        long parentId = 0;
                                        if (lst.Any())
                                        {
                                            parentId = lst.FirstOrDefault().Id;
                                        }
                                        Favorite fav = new Favorite();
                                        fav.UserId = userId;
                                        fav.FolderId = 0;
                                        fav.DocumentId = id;
                                        fav.ParentId = parentId;
                                        fav.CabinetId = cabinetId;
                                        favLst.Add(fav);
                                    }
                                    db.Favorites.AddRange(favLst);
                                    db.SaveChanges();
                                }
                                transaction.Commit();
                                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public void ChangeChildFavoriteStatus(long Id, long userId, long cabinetId, long parentId, string status)
        {
            try
            {
                var subDocIds = db.DocumentDetails.Where(j => j.FolderId == Id).Select(x => x.DocumentId).ToList();
                if (subDocIds.Count > 0)
                {
                    if (status == "True")
                    {
                        var favDetail = db.Favorites.Where(x => x.UserId == userId && subDocIds.Contains(x.DocumentId)).ToList();
                        if (favDetail.Count > 0)
                        {
                            db.Favorites.RemoveRange(favDetail);
                            db.SaveChanges();
                        }
                    }
                    else
                    {
                        List<Favorite> favLst = new List<Favorite>();
                        var favoriteLst = db.Favorites.ToList();
                        foreach (var item in subDocIds)
                        {
                            var lst = favoriteLst.Where(x => x.DocumentId == item).ToList();
                            if (!lst.Any())
                            {
                                Favorite fav = new Favorite();
                                fav.UserId = userId;
                                fav.FolderId = 0;
                                fav.DocumentId = item;
                                fav.ParentId = parentId;
                                fav.CabinetId = cabinetId;
                                favLst.Add(fav);
                            }
                            else
                            {
                                lst.FirstOrDefault().ParentId = parentId;
                            }
                        }
                        db.Favorites.AddRange(favLst);
                        db.SaveChanges();
                    }
                }

                var subFolderList = db.FolderDetails.Where(j => j.ParentFolderId == Id).ToList();
                if (subFolderList.Count > 0)
                {
                    foreach (var item in subFolderList)
                    {
                        Favorite fav = new Favorite();
                        if (status == "True")
                        {
                            var favDetail = db.Favorites.Where(x => x.UserId == userId && x.FolderId == item.Id).ToList();
                            if (favDetail.Count > 0)
                            {
                                db.Favorites.RemoveRange(favDetail);
                                db.SaveChanges();
                            }
                        }
                        else
                        {
                            if (!db.Favorites.Where(x => x.FolderId == item.Id).Any())
                            {
                                fav.UserId = userId;
                                fav.FolderId = item.Id;
                                fav.DocumentId = 0;
                                fav.ParentId = parentId;
                                fav.CabinetId = cabinetId;
                                db.Favorites.Add(fav);
                                db.SaveChanges();
                            }
                        }

                        if (CommonFunctions.HasChild(item.Id) > 0)
                        {
                            ChangeChildFavoriteStatus(item.Id, userId, cabinetId, fav.Id, status);
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region --> Download Drag Drop folder and file Display | Add | Dhrumil Patel | 21052018

        public ActionResult DownloadItemsHtml(string DownloadHtml, string[] DownloadFiles, string[] DownloadFolders)
        {
            var userIds = CommonFunctions.GetUserId();
            if (userIds > 0)
            {
                Session[SessionClass.DownloadItems] = DownloadHtml;
            }
            else
            {
                Session[SessionClass.DownloadItems] = null;
            }

            return Json(new { success = true, DownloadHtml = DownloadHtml }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DisplayDownloadHtml()
        {
            var userIds = CommonFunctions.GetUserId();
            string DownloadHtml = string.Empty;
            if (userIds > 0)
            {
                if (Session[SessionClass.DownloadItems] != null)
                {
                    DownloadHtml = Convert.ToString(Session[SessionClass.DownloadItems]);
                    return Json(new { success = true, DownloadHtml = DownloadHtml }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { success = false, DownloadHtml = "Empty" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, DownloadHtml = "Empty" }, JsonRequestBehavior.AllowGet);
            }


        }

        public ActionResult ClearDownloadHtml()
        {
            if (Session[SessionClass.DownloadItems] != null)
            {
                Session[SessionClass.DownloadItems] = null;
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region --> Download folder and file | Add | Dhrumil Patel | 21052018
        [AuditLog(EnumList.EnumLogType.UpdateData, "DownloadFolderFiles")]
        public JsonResult DownloadFolderFiles(string[] folderIds, string[] fileIds)
        {
            try
            {
                if (folderIds != null || fileIds != null)
                {
                    long uid = CommonFunctions.GetUserId();
                    long cabinetId = CommonFunctions.GetCabinetId();
                    var uriPath = ConfigurationManager.AppSettings["PathForDocUpload"];
                    string folderPath = Server.MapPath(uriPath);
                    var folderDetails = db.FolderDetails.ToList();
                    var docDetails = db.DocumentVersionDetails.ToList();
                    var selectedNames = new List<string>();
                    var folIds = new List<string>();
                    var docIds = fileIds != null ? fileIds.ToList() : new List<string>();
                    var fileNames = new List<string>();
                    if (folderIds != null)
                    {
                        foreach (var item in folderIds)
                        {
                            long id = Convert.ToInt64(item);
                            folIds.Add(item);
                            if (CommonFunctions.HasChild(id) > 0)
                            {
                                parentLst = new List<string>();
                                folIds.AddRange(GetChildFolderIds(id, uid));
                            }
                        }

                        if (folIds != null && folIds.Count > 0)
                        {
                            var filIds = db.DocumentDetails.Where(x => folIds.Contains(x.FolderId.ToString()) && x.IsActive && !x.IsDelete).Select(x => x.DocumentId.ToString()).ToList();
                            foreach (var id in filIds)
                            {
                                var doc = docDetails.Where(x => x.DocumentId.ToString() == id && x.IsActive && !x.IsDelete && x.IsCurrent).OrderByDescending(x => x.Id).FirstOrDefault();
                                if (doc != null)
                                {
                                    docIds.Add(doc.Id.ToString());
                                }
                            }
                        }
                    }
                    List<string> CreatorsPath = new List<string>();
                    if (docIds != null && docIds.Count > 0)
                    {
                        CreatorsPath = docDetails.Where(x => docIds.Contains(x.Id.ToString()) && x.IsActive && !x.IsDelete).Select(x => folderPath + x.CreatedBy + "/" + cabinetId.ToString() + "/").Distinct().ToList();
                        if (docIds.Count == 1)
                        {
                            var detail = docDetails.FirstOrDefault(x => x.Id.ToString() == docIds.FirstOrDefault());
                            var docName = db.DocumentDetails.Find(detail.DocumentId).Name + "." + detail.DocumentType;
                            selectedNames.AddRange(docDetails.Where(x => docIds.Contains(x.Id.ToString()) && x.IsActive && !x.IsDelete).Select(x => folderPath + x.CreatedBy + "/" + cabinetId.ToString() + "/" + x.DocumentName + "." + x.DocumentType).Distinct().ToList());
                            var path = selectedNames[0].Substring(0, selectedNames[0].LastIndexOf('/')) + "/" + docName;
                            System.IO.File.Copy(selectedNames[0], path, true);
                            selectedNames = new List<string>();
                            selectedNames.Add(path);
                        }
                        else
                        {
                            selectedNames.AddRange(docDetails.Where(x => docIds.Contains(x.Id.ToString()) && x.IsActive && !x.IsDelete).Select(x => folderPath + x.CreatedBy + "/" + cabinetId.ToString() + "/" + x.DocumentName + "." + x.DocumentType).Distinct().ToList());
                        }
                    }

                    foreach (var path in CreatorsPath)
                    {
                        if (Directory.Exists(path))
                        {
                            string[] fileEntries = Directory.GetFiles(path);
                            foreach (string fileName in fileEntries)
                            {
                                if (selectedNames.Contains(fileName))
                                {
                                    fileNames.Add(fileName);
                                }
                            }
                        }
                    }
                    if (fileNames != null && fileNames.Count > 0)
                    {
                        TempData["FileNameList"] = fileNames.ToList();
                        return Json(new { result = "success", Message = "Download successfully!" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { result = "error", Message = "Sorry! Please try again." }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { result = "error", Message = "Sorry! Please try again." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { result = "error", Message = "Sorry! Please try again." }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DownloadSelected(string folid)
        {
            List<string> fileNameLst = new List<string>();
            if (TempData["FileNameList"] != null)
            {
                fileNameLst = (List<string>)TempData["FileNameList"];
                TempData["FileNameList"] = null;
            }
            long uid = CommonFunctions.GetUserId();
            long cabinetId = CommonFunctions.GetCabinetId();
            if (fileNameLst != null && fileNameLst.Count > 0)
            {
                try
                {
                    using (ZipFile zip = new ZipFile())
                    {
                        UtilityCommonFunctions.RandomStringGenerator random = new UtilityCommonFunctions.RandomStringGenerator();
                        string serialNumber = random.GetRandomString(4, UtilityCommonFunctions.RandomStringGenerator.ALPHANUMERIC_CAPS.ToCharArray());
                        string ZipFileName = string.Empty;
                        var uriPath = ConfigurationManager.AppSettings["PathForDocUpload"] + uid.ToString() + "/" + cabinetId.ToString() + "/";
                        string folderPath = Server.MapPath(uriPath);
                        var name = fileNameLst[0].Substring(fileNameLst[0].LastIndexOf('\\') + 1, fileNameLst[0].Length - 1 - fileNameLst[0].LastIndexOf('\\'));
                        ZipFileName = fileNameLst.Count() > 1 ?
                            String.Format("Docware-{0}-MultiItems-{1}-{2}", Session[SessionClass.CabinetName], DateTime.Now.ToString("yyyyMMMdd-HHmm"), serialNumber) :
                            String.Format("Docware-{0}-{1}-{2}-{3}", Session[SessionClass.CabinetName], name, DateTime.Now.ToString("yyyyMMMdd-HHmm"), serialNumber);
                        ZipFileName = ZipFileName.Replace(" ", "_");
                        zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                        zip.AddDirectoryByName("Docware");
                        foreach (string fileName in fileNameLst)
                        {
                            zip.AddFile(fileName, "Docware");
                        }

                        if (zip.Count > 1)
                        {
                            Response.Clear();
                            Response.BufferOutput = false;
                            string zipName = ZipFileName + ".zip";
                            Response.ContentType = "application/zip";
                            Response.AddHeader("content-disposition", "attachment; filename=" + zipName);
                            zip.Save(Response.OutputStream);
                            Response.End();
                            if (fileNameLst.Count() == 1)
                            {
                                System.IO.File.Delete(fileNameLst[0]);
                            }
                        }
                        else
                        {
                            TempData["ErrorMSG"] = "Documents not available.";
                        }
                    }
                }
                catch (Exception ex)
                {
                    TempData["ErrorMSG"] = "Some error occured. Please try again.";
                }
            }
            return RedirectToAction("FolderList", new { id = folid });
        }

        public List<string> GetChildFolderIds(long Id, long userId)
        {
            try
            {
                var subFolderList = db.FolderDetails.Where(j => j.ParentFolderId == Id && j.IsActive && !j.IsDelete).ToList();
                if (subFolderList.Count > 0)
                {
                    foreach (var item in subFolderList)
                    {
                        parentLst.Add(item.Id.ToString());
                        if (CommonFunctions.HasChild(item.Id) > 0)
                        {
                            GetChildFolderIds(item.Id, userId);
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return parentLst;
        }
        #endregion

        #region --> DocShare Detail | Add | Dhrumil Patel | 23052018
        [HttpGet]
        public JsonResult DocShareDetail(long id, bool isDoc)
        {
            DocShare docShareVM = new DocShare();
            var userId = CommonFunctions.GetUserId();
            try
            {
                using (db = new DBEntities())
                {
                    if (id > 0)
                    {
                        if (isDoc)
                        {
                            var data = db.DocumentDetails.Find(id);
                            if (data != null)
                            {
                                docShareVM.FileId = id;
                                docShareVM.FolderId = 0;
                                docShareVM.Name = data.Name;
                            }
                            var shareData = db.UserDocShareDetails.Where(x => x.DocumentId == id && x.UserId == userId).ToList();
                            if (shareData.Count > 0)
                            {
                                docShareVM.DocShareLst = shareData.Select(x => new DocShareList
                                {
                                    UserId = x.DocShareUserId,
                                    GroupId = x.DocShareGroupId,
                                    Name = x.DocShareUserId != 0 ?
                                        db.LoginCredentials.Where(y => y.Id == x.DocShareUserId).Select(y => y.FirstName + " " + y.LastName).FirstOrDefault() :
                                        db.GroupDetails.Where(y => y.Id == x.DocShareGroupId).Select(y => y.Name).FirstOrDefault(),
                                    IsDownload = x.IsDownload,
                                    IsUpload = x.IsUpload,
                                    IsSeeVersion = x.IsSeeVersion,
                                    IsReadComment = x.IsReadComment,
                                    IsAddComment = x.IsAddComment,
                                    IsShare = x.IsShare
                                }).ToList();
                            }
                        }
                        else
                        {
                            var data = db.FolderDetails.Find(id);
                            if (data != null)
                            {
                                docShareVM.FileId = 0;
                                docShareVM.FolderId = id;
                                docShareVM.Name = data.Name;
                            }
                            var shareData = db.UserDocShareDetails.Where(x => x.FolderId == id && x.UserId == userId).ToList();
                            if (shareData.Count > 0)
                            {
                                docShareVM.DocShareLst = shareData.Select(x => new DocShareList
                                {
                                    UserId = x.DocShareUserId,
                                    GroupId = x.DocShareGroupId,
                                    Name = x.DocShareUserId != 0 ?
                                        db.LoginCredentials.Where(y => y.Id == x.DocShareUserId).Select(y => y.FirstName + " " + y.LastName).FirstOrDefault() :
                                        db.GroupDetails.Where(y => y.Id == x.DocShareGroupId).Select(y => y.Name).FirstOrDefault(),
                                    IsDownload = x.IsDownload,
                                    IsUpload = x.IsUpload,
                                    IsSeeVersion = x.IsSeeVersion,
                                    IsReadComment = x.IsReadComment,
                                    IsAddComment = x.IsAddComment,
                                    IsShare = x.IsShare
                                }).ToList();
                            }
                        }
                        return Json(new { success = true, shareData = docShareVM }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "AddRemoveDocShareDetail")]
        public JsonResult AddRemoveDocShareDetail(DocShare detail)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                var notificationdetail = new SaveNotificationVM();
                var userId = CommonFunctions.GetUserId();
                var cabinetId = CommonFunctions.GetCabinetId();
                try
                {
                    if (detail.RemoveUserIdsList != null)
                    {
                        foreach (var id in detail.RemoveUserIdsList)
                        {
                            if (id != "")
                            {
                                var uId = Convert.ToInt64(id);
                                var delData = db.UserDocShareDetails.Where(x => x.UserId == userId && x.FolderId == detail.FolderId
                                            && x.DocumentId == detail.FileId && x.DocShareUserId == uId).FirstOrDefault();
                                if (delData != null)
                                {
                                    db.UserDocShareDetails.Remove(delData);
                                    db.SaveChanges();
                                }
                                if (detail.FolderId != 0 && CommonFunctions.HasChild(detail.FolderId) > 0)
                                {
                                    RemoveChildFromShare(detail.FolderId, userId, uId, 0);
                                }
                            }
                        }
                    }
                    if (detail.RemoveGroupIdsList != null)
                    {
                        foreach (var id in detail.RemoveGroupIdsList)
                        {
                            if (id != "")
                            {
                                var gId = Convert.ToInt64(id);
                                var delData = db.UserDocShareDetails.Where(x => x.UserId == userId && x.FolderId == detail.FolderId
                                            && x.DocumentId == detail.FileId && x.DocShareGroupId == gId).FirstOrDefault();
                                if (delData != null)
                                {
                                    db.UserDocShareDetails.Remove(delData);
                                    db.SaveChanges();
                                }
                                if (detail.FolderId != 0 && CommonFunctions.HasChild(detail.FolderId) > 0)
                                {
                                    RemoveChildFromShare(detail.FolderId, userId, 0, gId);
                                }
                            }
                        }
                    }

                    foreach (var dt in detail.DocShareLst)
                    {
                        var lst = new List<UserDocShareDetail>();
                        if (detail.FolderId != 0)
                        {
                            lst = db.UserDocShareDetails.Where(x => x.UserId == userId
                                    && x.FolderId == detail.FolderId
                                    && x.DocShareUserId == dt.UserId
                                    && x.DocShareGroupId == dt.GroupId).ToList();
                            notificationdetail.FolderId = detail.FolderId;
                            notificationdetail.TypeFor = "Folder";
                        }
                        else
                        {
                            var folData = db.DocumentDetails.Find(detail.FileId);
                            lst = db.UserDocShareDetails.Where(x => x.UserId == userId
                                    && x.FolderId == folData.FolderId
                                    && x.DocShareUserId == dt.UserId
                                    && x.DocShareGroupId == dt.GroupId).ToList();
                            notificationdetail.DocumentId = detail.FileId;
                            notificationdetail.TypeFor = "File";
                        }
                        long parentId = 0;
                        if (lst.Any())
                        {
                            parentId = lst.FirstOrDefault().Id;
                        }
                        var data = new UserDocShareDetail();
                        data.UserId = userId;
                        data.FolderId = detail.FolderId;
                        data.DocumentId = detail.FileId;
                        data.DocShareUserId = dt.UserId;
                        data.DocShareGroupId = dt.GroupId;
                        data.ParentId = parentId;
                        data.CabinetId = CommonFunctions.GetCabinetId();
                        data.IsDownload = dt.IsDownload;
                        data.IsUpload = dt.IsUpload;
                        data.IsSeeVersion = dt.IsSeeVersion;
                        data.IsReadComment = dt.IsReadComment;
                        data.IsAddComment = dt.IsAddComment;
                        data.IsShare = dt.IsShare;
                        data.CreatedBy = userId.ToString();
                        data.CreatedDate = DateTime.Now;
                        db.UserDocShareDetails.Add(data);
                        db.SaveChanges();

                        if (detail.FolderId != 0 && CommonFunctions.HasChild(detail.FolderId) > 0)
                        {
                            AddChildToShare(detail.FolderId, userId, data.Id, dt);
                        }
                    }
                    transaction.Commit();
                    notificationdetail.UserId = userId;
                    notificationdetail.CabinetId = cabinetId;
                    notificationdetail.NotificationType = (int)EnumList.EnumNotificationType.Share;
                    CommonFunctions.SaveNotification(notificationdetail);
                    CommonFunctions.RemoveLinkedDocument();
                    CommonFunctions.RemoveFavouriteDocument();
                    return Json(new { success = true, msg = "Shared successfully." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return Json(new { success = false, msg = "Sorry! Please try again." }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public void AddChildToShare(long Id, long userId, long parentId, DocShareList dt)
        {
            try
            {
                var subDocList = db.DocumentDetails.Where(j => j.FolderId == Id).ToList();
                if (subDocList.Count > 0)
                {
                    foreach (var item in subDocList)
                    {
                        var data = new UserDocShareDetail();
                        data.UserId = userId;
                        data.FolderId = 0;
                        data.DocumentId = item.DocumentId;
                        data.DocShareUserId = dt.UserId;
                        data.DocShareGroupId = dt.GroupId;
                        data.ParentId = parentId;
                        data.CabinetId = CommonFunctions.GetCabinetId();
                        data.IsDownload = dt.IsDownload;
                        data.IsUpload = dt.IsUpload;
                        data.IsSeeVersion = dt.IsSeeVersion;
                        data.IsReadComment = dt.IsReadComment;
                        data.IsAddComment = dt.IsAddComment;
                        data.IsShare = dt.IsShare;
                        data.CreatedBy = userId.ToString();
                        data.CreatedDate = DateTime.Now;
                        db.UserDocShareDetails.Add(data);
                        db.SaveChanges();
                    }
                }

                var subFolderList = db.FolderDetails.Where(j => j.ParentFolderId == Id).ToList();

                if (subFolderList.Count > 0)
                {
                    foreach (var item in subFolderList)
                    {
                        var data = new UserDocShareDetail();
                        data.UserId = userId;
                        data.FolderId = item.Id;
                        data.DocumentId = 0;
                        data.DocShareUserId = dt.UserId;
                        data.DocShareGroupId = dt.GroupId;
                        data.ParentId = parentId;
                        data.CabinetId = CommonFunctions.GetCabinetId();
                        data.IsDownload = dt.IsDownload;
                        data.IsUpload = dt.IsUpload;
                        data.IsSeeVersion = dt.IsSeeVersion;
                        data.IsReadComment = dt.IsReadComment;
                        data.IsAddComment = dt.IsAddComment;
                        data.IsShare = dt.IsShare;
                        data.CreatedBy = userId.ToString();
                        data.CreatedDate = DateTime.Now;
                        db.UserDocShareDetails.Add(data);
                        db.SaveChanges();

                        if (CommonFunctions.HasChild(item.Id) > 0)
                        {
                            AddChildToShare(item.Id, userId, data.Id, dt);
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void RemoveChildFromShare(long Id, long userId, long uid, long gid)
        {
            try
            {
                var subDocList = db.DocumentDetails.Where(j => j.FolderId == Id).ToList();
                var docLst = db.UserDocShareDetails.ToList();
                if (subDocList.Count > 0)
                {
                    foreach (var item in subDocList)
                    {
                        var delData = docLst.Where(x => x.UserId == userId && x.FolderId == 0
                                        && x.DocumentId == item.DocumentId && x.DocShareUserId == uid
                                        && x.DocShareGroupId == gid).FirstOrDefault();
                        if (delData != null)
                        {
                            db.UserDocShareDetails.Remove(delData);
                            db.SaveChanges();
                        }
                    }
                }

                var subFolderList = db.FolderDetails.Where(j => j.ParentFolderId == Id).ToList();

                if (subFolderList.Count > 0)
                {
                    foreach (var item in subFolderList)
                    {
                        var delData = docLst.Where(x => x.UserId == userId && x.FolderId == item.Id
                                        && x.DocumentId == 0 && x.DocShareUserId == uid
                                        && x.DocShareGroupId == gid).FirstOrDefault();
                        if (delData != null)
                        {
                            db.UserDocShareDetails.Remove(delData);
                            db.SaveChanges();
                        }
                        if (CommonFunctions.HasChild(item.Id) > 0)
                        {
                            RemoveChildFromShare(item.Id, userId, uid, gid);
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region --> Shared With Me List | Add | Dhruvin Patel | 28052018
        [HttpGet, Route("SharedWithMe")]
        public ActionResult SharedWithMe()
        {
            List<FileListVM> fileListVMs = new List<FileListVM>();
            try
            {
                long CabinetId = CommonFunctions.GetCabinetId();
                long UserId = CommonFunctions.GetUserId();

                #region --> SP for Get Share with me list
                SortedList sl = new SortedList();
                DataTable dt = new DataTable();
                sl.Add("@cabinetId", CabinetId);
                sl.Add("@userId", UserId);
                dt = objDAL.GetDataTable("uspGetShareWithMeFilesByUserAndCabinet", sl);
                fileListVMs = objDAL.ConvertToList<FileListVM>(dt);
                #endregion

                #region --> Linq Query for Get Share with me list
                //fileListVMs = (from udsd in db.UserDocShareDetails
                //               join gd in db.GroupDetails on udsd.DocShareGroupId equals gd.Id into dsgDefault
                //               from gd in dsgDefault.DefaultIfEmpty()
                //               join gud in db.GroupUserDetails on gd.Id equals gud.GroupId into gudDefault
                //               from gud in gudDefault.DefaultIfEmpty()

                //               join f in db.FolderDetails on udsd.FolderId equals f.Id into fDefault
                //               from f in fDefault.DefaultIfEmpty()

                //               join flc in db.LoginCredentials on f.UpdatedBy equals flc.Id.ToString() into flcDefault
                //               from flc in flcDefault.DefaultIfEmpty()

                //               join d in db.DocumentDetails on udsd.DocumentId equals d.DocumentId into dDefault
                //               from d in dDefault.DefaultIfEmpty()

                //               join dv in db.DocumentVersionDetails on d.DocumentId equals dv.DocumentId into dvDefault
                //               from dv in dvDefault.DefaultIfEmpty()

                //               join dlc in db.LoginCredentials on dv.UpdatedBy equals dlc.Id.ToString() into dlcDefault
                //               from dlc in dlcDefault.DefaultIfEmpty()



                //               where udsd.CabinetId == CabinetId
                //               && ((f.IsActive && !f.IsDelete) || (d.IsActive && !d.IsDelete))
                //               && ((udsd.DocShareUserId == UserId) || (gd.CabinetId == CabinetId && gud.UserId == UserId && gd.IsActive && !gd.IsDelete))
                //               select new FileListVM
                //               {
                //                   Id = udsd.DocumentId > 0 ? udsd.DocumentId : udsd.FolderId,
                //                   DocumentType = udsd.DocumentId > 0 ? dv.DocumentType : "folder",
                //                   Name = udsd.DocumentId > 0 ? d.Name : f.Name,

                //                   DocumentVersion = udsd.DocumentId > 0 ? dv.DocumentVersion : 0,
                //                   DocumentDisplaySize = udsd.DocumentId > 0 ? dv.DocumentDisplaySize : "-",

                //                   ModifiedBy = udsd.DocumentId > 0 ? dlc != null ? dlc.FirstName + " " + dlc.LastName : "-" : flc != null ? flc.FirstName + " " + flc.LastName : "-",
                //                   ModifiedOn = udsd.DocumentId > 0 ? dv != null ? dv.UpdatedDate.ToString() : "-" : f != null ? f.UpdatedDate.ToString() : "-"
                //               }).Distinct().ToList();
                #endregion

                //foreach (var data in fileListVMs)
                //{
                //    if (data.DocumentType == "Doc")
                //    {
                //        DocumentVersionDetail dv = db.DocumentVersionDetails.Where(x => x.DocumentId == data.Id).OrderByDescending(x => x.CreatedDate).FirstOrDefault();
                //        if (dv != null)
                //        {
                //            if (!string.IsNullOrWhiteSpace(dv.UpdatedBy))
                //            {
                //                //long id = Convert.ToInt64(dv.UpdatedBy);
                //                var user = db.LoginCredentials.Where(x => x.Id.ToString() == dv.UpdatedBy).FirstOrDefault();
                //                if (user != null)
                //                {


                //                    data.ModifiedBy = UserId.ToString() != dv.UpdatedBy ? user.FirstName + " " + user.LastName : "Me";
                //                    data.ModifiedOn = dv.UpdatedDate != null ? dv.UpdatedDate.GetValueOrDefault().ToString("dd,MMM yyyy HH:mm tt") : "-";
                //                }
                //                else
                //                {
                //                    data.ModifiedBy = "-";
                //                    data.ModifiedOn = "-";
                //                }
                //            }
                //            else
                //            {
                //                data.ModifiedBy = "-";
                //                data.ModifiedOn = "-";
                //            }
                //            data.DocumentDisplaySize = dv.DocumentDisplaySize;
                //            data.DocumentType = dv.DocumentType;
                //            data.DocumentVersion = dv.DocumentVersion;

                //        }
                //    }
                //    else
                //    {
                //        FolderDetail fd = db.FolderDetails.Where(x => x.Id == data.Id).FirstOrDefault();
                //        if (fd != null)
                //        {
                //            if (!string.IsNullOrWhiteSpace(fd.UpdatedBy))
                //            {
                //                var user = db.LoginCredentials.Where(x => x.Id.ToString() == fd.UpdatedBy).FirstOrDefault();
                //                if (user != null)
                //                {
                //                    data.ModifiedBy = UserId.ToString() != fd.UpdatedBy ? user.FirstName + " " + user.LastName : "Me";
                //                    data.ModifiedOn = fd.UpdatedDate != null ? fd.UpdatedDate.GetValueOrDefault().ToString("dd,MMM yyyy HH:mm tt") : "-";
                //                }
                //                else
                //                {
                //                    data.ModifiedBy = "-";
                //                    data.ModifiedOn = "-";
                //                }
                //            }
                //            else
                //            {
                //                data.ModifiedBy = "-";
                //                data.ModifiedOn = "-";
                //            }
                //            data.DocumentDisplaySize = "-";
                //        }
                //    }
                //}
            }
            catch (Exception e)
            {
                throw e;
            }
            return View(fileListVMs);
        }
        #endregion

        #region --> Recycle Bin | Add | Dhruvin Patel | 28052018
        [HttpGet, Route("RecycleBin")]
        public ActionResult RecycleBin()
        {
            List<FileListVM> fileListVMs = new List<FileListVM>();
            try
            {
                long CabinetId = CommonFunctions.GetCabinetId();
                long UserId = CommonFunctions.GetUserId();

                #region --> SP for Get Share with me list
                SortedList sl = new SortedList();
                DataTable dt = new DataTable();
                sl.Add("@cabinetId", CabinetId);
                sl.Add("@userId", UserId);
                dt = objDAL.GetDataTable("uspGetRecycleBinFilesByUserAndCabinet", sl);
                fileListVMs = objDAL.ConvertToList<FileListVM>(dt);
                #endregion               
            }
            catch (Exception e)
            {
                throw e;
            }
            return View(fileListVMs);
        }
        #endregion

        #region --> Share Email Link | Add | Dhruvin Patel | 15062018
        [AuditLog(EnumList.EnumLogType.UpdateData, "SendShareEmailLink")]
        public JsonResult ShareEmailLink(SharedEmailLink detail)
        {
            try
            {
                //string folderLink = string.Empty;
                //string docLink = string.Empty;
                //string body = string.Empty;
                long userId = CommonFunctions.GetUserId();
                long cabinetId = CommonFunctions.GetCabinetId();
                if (userId > 0)
                {
                    LoginCredential currentLoginModel = db.LoginCredentials.Find(userId);
                    if (detail.UserIds != null)
                    {
                        //if (detail.FolderIds != null && detail.FolderIds.Length > 0)
                        //{
                        //    folderLink = "<p> Folder Link : <br/>";
                        //    foreach (string fID in detail.FolderIds)
                        //    {
                        //        FolderDetail fdModel = db.FolderDetails.Find(Convert.ToInt64(fID));
                        //        if (fdModel != null)
                        //        {
                        //            folderLink = folderLink + fdModel.Name + " : " + "<a href='#'>Download</a>" + "<br/>";
                        //        }
                        //    }
                        //    folderLink = folderLink + "</p>";
                        //}                   
                        //string hostURL = WebConfigurationManager.AppSettings["RedirectLink"].ToString();
                        List<string> docIds = detail.FileIds != null ? detail.FileIds.ToList() : new List<string>();
                        List<string> folIds = new List<string>();
                        if (detail.FolderIds != null)
                        {
                            foreach (var item in detail.FolderIds)
                            {
                                long id = Convert.ToInt64(item);
                                folIds.Add(item);
                                if (CommonFunctions.HasChild(id) > 0)
                                {
                                    parentLst = new List<string>();
                                    folIds.AddRange(GetChildFolderIds(id, userId));
                                }
                            }

                            if (folIds != null && folIds.Count > 0)
                            {
                                var filIds = db.DocumentDetails.Where(x => folIds.Contains(x.FolderId.ToString()) && x.IsActive && !x.IsDelete).Select(x => x.DocumentId.ToString()).ToList();
                                foreach (var id in filIds)
                                {
                                    //var doc = db.DocumentVersionDetails.Where(x => x.DocumentId.ToString() == id && x.IsActive && !x.IsDelete).OrderByDescending(x => x.Id).FirstOrDefault();
                                    //if (doc != null)
                                    //{
                                    //    docIds.Add(doc.Id.ToString());
                                    //}
                                    long dID = CommonFunctions.CheckIsFilePermission(detail.ShareType, Convert.ToInt64(id), cabinetId, userId);
                                    if (dID > 0)
                                    {
                                        docIds.Add(dID.ToString());
                                    }
                                }
                            }
                        }
                        bool isSendEmail = CommonFunctions.SendShareEmailLink(userId, docIds, detail.UserIds);
                        return Json(new { success = isSendEmail }, JsonRequestBehavior.AllowGet);
                        //if (docIds != null && docIds.Count > 0)
                        //{
                        //    docLink = "<p> Document Link : <br/>";
                        //    foreach (string dvID in docIds)
                        //    {
                        //        DocumentVersionDetail dvModel = db.DocumentVersionDetails.Find(Convert.ToInt64(dvID));
                        //        if (dvModel != null)
                        //        {
                        //            string url = hostURL + dvModel.DocumentPath.Replace("~", "");
                        //            docLink = docLink + dvModel.DocumentName + " : " + "<a href='" + url + "'>Download</a>" + "<br/>";
                        //        }
                        //    }
                        //    docLink = docLink + "</p>";
                        //}

                        //foreach (string uID in detail.UserIds)
                        //{
                        //    LoginCredential model = db.LoginCredentials.Find(Convert.ToInt64(uID));
                        //    if (model != null)
                        //    {
                        //        //body = body.Replace("#FullName#", model.FirstName + model.LastName);
                        //        //SendEmail.SentEmail(model.EmailId, "Folder and File Share", body, string.Empty);
                        //        Dictionary<string, string> replacement = new Dictionary<string, string>();
                        //        replacement.Add("#SenderFirstName#", currentLoginModel.FirstName);
                        //        replacement.Add("#SenderLastName#", currentLoginModel.LastName);

                        //        replacement.Add("#FullName#", model.FirstName + " " + model.LastName);
                        //        replacement.Add("#FolderLink#", folderLink);
                        //        replacement.Add("#DocLink#", docLink);
                        //        UtilityCommonFunctions.SendEmailTemplete((int)EnumList.EmailTemplate.SendDocumentLink, replacement, model.EmailId, string.Empty);

                        //    }
                        //}
                        //return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region --> List of Document Version And Upload New Document Version | Dhrumil Patel | 20062018
        [HttpGet]
        public JsonResult DocumentVersionDetail(long id)
        {
            var userId = CommonFunctions.GetUserId();
            try
            {
                using (db = new DBEntities())
                {
                    if (id > 0)
                    {
                        var isCreator = CommonFunctions.IsCreatedBy(id, true);
                        var versionList = db.DocumentVersionDetails.Where(x => x.DocumentId == id
                            && x.IsActive && !x.IsDelete)
                            .Select(x => new DocumentVersionList
                            {
                                DocId = x.DocumentId,
                                DocVersionId = x.Id,
                                Version = x.DocumentVersion,
                                DocSize = x.DocumentDisplaySize,
                                CreatedBy = x.CreatedBy,
                                Created = x.CreatedDate,
                                Notes = x.Notes,
                                DocType = x.DocumentType,
                                CurrentUser = userId.ToString(),
                                IsCurrent = x.IsCurrent,
                                IsCreator = isCreator
                            }).OrderByDescending(x => x.DocVersionId).ToList();

                        var loginDetail = db.LoginCredentials.ToList();
                        foreach (var item in versionList)
                        {
                            var date = item.Created.ToShortDateString() == DateTime.Now.ToShortDateString() ? item.Created.ToString("hh:mm tt") : item.Created.ToString("dd MMM yyy hh:mm tt");
                            item.CreatedOn = date;
                            var uid = Convert.ToInt64(item.CreatedBy);
                            var lst = loginDetail.FirstOrDefault(x => x.Id == uid);
                            item.UserName = lst != null ? lst.FirstName + " " + lst.LastName : "";
                        }

                        return Json(new { success = true, versionData = versionList }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "RemoveDocVersionDetail")]
        public JsonResult RemoveDocVersionDetail(string[] RemoveDocVersionIdsList, long uvid, long rvid)
        {
            try
            {
                if (RemoveDocVersionIdsList != null)
                {
                    foreach (var id in RemoveDocVersionIdsList)
                    {
                        var vid = Convert.ToInt64(id);
                        var dr = db.DocumentVersionDetails.Find(vid);
                        dr.IsActive = false;
                        dr.IsDelete = true;
                        db.SaveChanges();
                    }
                }
                var updateVersion = db.DocumentVersionDetails.Find(rvid);
                if (updateVersion != null)
                {
                    updateVersion.IsCurrent = false;
                    db.SaveChanges();
                }

                updateVersion = db.DocumentVersionDetails.Find(uvid);
                if (updateVersion != null)
                {
                    updateVersion.IsCurrent = true;
                    db.SaveChanges();
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetDocumentVersion(long id)
        {
            try
            {
                using (db = new DBEntities())
                {
                    if (id > 0)
                    {
                        var documentVersionVM = (from dd in db.DocumentDetails
                                                 let dv = db.DocumentVersionDetails.Where(j => j.Id == id && j.IsActive && !j.IsDelete).OrderByDescending(j => j.Id).FirstOrDefault()
                                                 where dd.IsActive == true && dd.IsDelete == false && dv.DocumentId == dd.DocumentId
                                                 select new DocumentDetailVM
                                                 {
                                                     DocumentVersionId = dv.Id,
                                                     DocumentId = dv.DocumentId,
                                                     DocParentFolderId = dd.FolderId,
                                                     DocName = dd.Name,
                                                     DocumentVersion = dv.DocumentVersion,
                                                     DocumentType = dv.DocumentType
                                                 }).FirstOrDefault();
                        return Json(new { success = true, docData = documentVersionVM }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, Route("AddDocumentVersion")]
        [AuditLog(EnumList.EnumLogType.SaveData, "AddDocumentVersion")]
        public ActionResult AddDocumentVersion(DocumentDetailVM data, HttpPostedFileBase docversionUpload)
        {
            var lastdata = new DocumentDetail();
            var lastversiondata = new DocumentVersionDetail();
            var Name = string.Empty;
            var UDID = string.Empty;
            var Uploadfile = new { Name = string.Empty, Size = "0 KB", VersionId = string.Empty };
            try
            {
                using (db = new DBEntities())
                {
                    var notificationdetail = new SaveNotificationVM();
                    long userId = CommonFunctions.GetUserId();
                    long cabinetId = CommonFunctions.GetCabinetId();

                    if (docversionUpload != null)
                    {
                        var updateData = db.DocumentVersionDetails.Where(x => x.DocumentId == data.DocumentId).ToList();
                        if (updateData.Count > 0)
                        {
                            var count = 0;
                            foreach (var items in updateData)
                            {
                                count++;
                                if (items.IsCurrent)
                                {
                                    items.IsCurrent = false;
                                    db.SaveChanges();
                                }
                                if (count == updateData.Count)
                                {
                                    lastdata = db.DocumentDetails.Find(items.DocumentId);
                                    lastdata.UpdatedBy = userId.ToString();
                                    lastdata.UpdatedDate = DateTime.Now;
                                    db.SaveChanges();
                                    lastversiondata = items;
                                }
                            }
                        }

                        Name = lastdata.UDID + "_" + data.DocumentVersion;
                        var uriPath = ConfigurationManager.AppSettings["PathForDocUpload"] + userId.ToString() + "/" + cabinetId.ToString() + "/";
                        string folderPath = Server.MapPath(uriPath);

                        if (!Directory.Exists(folderPath))
                        {
                            Directory.CreateDirectory(folderPath.ToString());
                        }
                        try
                        {
                            docversionUpload.SaveAs(folderPath + Name + Path.GetExtension(docversionUpload.FileName));
                        }
                        catch
                        {
                            throw;
                        }

                        var docSize = Math.Round((Convert.ToDecimal(docversionUpload.ContentLength) / Convert.ToDecimal(1024)), 2); //size in KB

                        DocumentVersionDetail documentVersionDetail = new DocumentVersionDetail();
                        documentVersionDetail.DocumentId = lastversiondata.DocumentId;
                        documentVersionDetail.DocumentName = Name;
                        documentVersionDetail.Notes = data.DocNotes;
                        documentVersionDetail.Keywords = lastversiondata.Keywords;
                        documentVersionDetail.DocumentType = Path.GetExtension(docversionUpload.FileName).Split('.')[1];
                        documentVersionDetail.DocumentSize = docSize;
                        documentVersionDetail.DocumentDisplaySize = docSize > 1024 ? (docSize / 1024).ToString("0.00") + " MB" : docSize.ToString() + " KB";
                        documentVersionDetail.DocumentVersion = data.DocumentVersion;
                        documentVersionDetail.Language = lastversiondata.Language;
                        documentVersionDetail.Confidentiality = Convert.ToByte(lastversiondata.Confidentiality);
                        documentVersionDetail.IsPublic = lastversiondata.IsPublic;
                        //documentVersionDetail.IsLocked = lastversiondata.IsLocked;
                        documentVersionDetail.DocumentPath = uriPath + Name + Path.GetExtension(docversionUpload.FileName);
                        documentVersionDetail.IsCurrent = true;
                        documentVersionDetail.IsActive = true;
                        documentVersionDetail.IsDelete = false;
                        documentVersionDetail.Description = lastversiondata.Description;
                        documentVersionDetail.PhysicalLocation = lastversiondata.PhysicalLocation;
                        documentVersionDetail.CreatedBy = userId.ToString();
                        documentVersionDetail.CreatedDate = DateTime.Now;
                        db.DocumentVersionDetails.Add(documentVersionDetail);
                        db.SaveChanges();

                        Uploadfile = new
                        {
                            Name = docversionUpload.FileName,
                            Size = documentVersionDetail.DocumentDisplaySize,
                            VersionId = documentVersionDetail.Id.ToString()
                        };

                        notificationdetail.UserId = userId;
                        notificationdetail.CabinetId = cabinetId;
                        notificationdetail.DocumentId = data.DocumentId;
                        notificationdetail.TypeFor = "File";
                        notificationdetail.NotificationType = (int)EnumList.EnumNotificationType.Version;
                        CommonFunctions.SaveNotification(notificationdetail);

                        return Json(new { success = true, msg = "Version Uploaded Successfully", uploadData = Uploadfile }, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult CheckVersion(double Version, long DocumentId)
        {
            List<DocumentVersionDetail> folderDetails = new List<DocumentVersionDetail>();
            try
            {
                using (db = new DBEntities())
                {
                    long CabinetId = CommonFunctions.GetCabinetId();

                    if (DocumentId > 0)
                    {
                        folderDetails = db.DocumentVersionDetails.Where(j => j.DocumentVersion == Version && j.DocumentId == DocumentId).ToList();
                    }

                    if (folderDetails.Count() > 0)
                    {
                        return Json(false, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(true, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region --> LInk Document  Detail | Add | Sweta Patel | 28062018
        public PartialViewResult _AddEditLinkDocument(long id)
        {
            LinkDocumentVM linkDocumentVM = new LinkDocumentVM();
            var userId = CommonFunctions.GetUserId();
            var cabinetId = CommonFunctions.GetCabinetId();
            try
            {
                if (id > 0)
                {

                    linkDocumentVM.UserId = userId;
                    linkDocumentVM.DocumentId = id;

                    SortedList sl = new SortedList();
                    DataTable dt = new DataTable();
                    sl.Add("@DocumentId", id);
                    sl.Add("@UserId", userId);
                    sl.Add("@CabinetId", cabinetId);
                    dt = objDAL.GetDataTable("uspGetChildLinkDocument", sl);
                    linkDocumentVM.LinkChildDocumentList = objDAL.ConvertToList<LinkChildDocumentList>(dt);

                    SortedList sl1 = new SortedList();
                    DataTable dt1 = new DataTable();
                    sl1.Add("@DocumentId", id);
                    sl1.Add("@UserId", userId);
                    sl1.Add("@CabinetId", cabinetId);
                    dt1 = objDAL.GetDataTable("uspGetParentLinkDocument", sl);
                    linkDocumentVM.LinkParentDocumentList = objDAL.ConvertToList<LinkParentDocumentList>(dt1);

                    return PartialView("_AddEditLinkDocument", linkDocumentVM);
                }

            }
            catch (Exception e)
            {
                return PartialView("_AddEditLinkDocument", new LinkDocumentVM());
            }
            return PartialView("_AddEditLinkDocument", new LinkDocumentVM());
        }
        #endregion

        #region --> Link Document  Add New Child Link | Add | Sweta Patel | 29062018
        public PartialViewResult _AddNewChildLink(long id, string FileIds)
        {
            LinkDocumentVM linkDocumentVM = new LinkDocumentVM();
            var userId = CommonFunctions.GetUserId();
            var cabinetId = CommonFunctions.GetCabinetId();
            try
            {
                if (id > 0)
                {

                    linkDocumentVM.UserId = userId;
                    linkDocumentVM.DocumentId = id;

                    SortedList sl = new SortedList();
                    DataTable dt = new DataTable();
                    sl.Add("@DocumentId", id);
                    sl.Add("@UserId", userId);
                    sl.Add("@CabinetId", cabinetId);
                    sl.Add("@SelectedFiles", FileIds);
                    dt = objDAL.GetDataTable("uspGetChildLinkForSelectedDocument", sl);
                    linkDocumentVM.LinkChildDocumentList = objDAL.ConvertToList<LinkChildDocumentList>(dt);

                    return PartialView("_AddNewChildLink", linkDocumentVM);
                }

            }
            catch (Exception e)
            {
                return PartialView("_AddNewChildLink", new LinkDocumentVM());
            }
            return PartialView("_AddNewChildLink", new LinkDocumentVM());
        }
        #endregion

        #region --> Link Document  Add New Child Link | Add | Sweta Patel | 29062018
        public ActionResult SaveDocumentLink(long id, string ChildFileIds, string ParentFileIds)
        {
            List<LinkDocument> linkDocument = new List<LinkDocument>();
            var userId = CommonFunctions.GetUserId();
            var cabinetId = CommonFunctions.GetCabinetId();
            try
            {
                if (id > 0)
                {
                    SortedList sl = new SortedList();
                    DataTable dt = new DataTable();
                    sl.Add("@DocumentId", id);
                    sl.Add("@UserId", userId);
                    sl.Add("@CabinetId", cabinetId);
                    if (ChildFileIds != null)
                        sl.Add("@SelectedChildFiles", ChildFileIds);
                    if (ParentFileIds != null)
                        sl.Add("@SelectedParentFiles", ParentFileIds);
                    dt = objDAL.GetDataTable("uspSaveLinkForDocument", sl);
                    linkDocument = objDAL.ConvertToList<LinkDocument>(dt);

                }
                return Json(new { success = true, msg = "Linked successfully." }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                return Json(new { success = false, msg = "Sorry! Please try again." }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region --> File Check IN / Out / Discard Check OUT | ADD | Krupali | 20180720
        public JsonResult FileCheckinOut(long Id, string[] folderIds, string[] fileIds, bool status)
        {
            long DocumentId = 0;
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    long userId = CommonFunctions.GetUserId();
                    long cabinetId = CommonFunctions.GetCabinetId();

                    //for Files
                    if (fileIds != null && fileIds.Length > 0)
                    {
                        DocumentId = Convert.ToInt64(fileIds[0]);
                        var IsDiscard = db.CheckInOutHistories.Where(x => x.DocumentId == DocumentId).FirstOrDefault();
                        var docVersion = db.DocumentVersionDetails.Where(x => x.DocumentId == DocumentId && x.IsCurrent == true).FirstOrDefault();


                        if (status == true) // Go for Check in
                        {
                            using (db = new DBEntities())
                            {
                                var ChckHistory = db.CheckInOutHistories.Find(Id);
                                ChckHistory.CheckInBy = userId.ToString();
                                ChckHistory.CheckInDate = DateTime.Now;
                                ChckHistory.DocumentVersionId = docVersion.Id;
                                ChckHistory.IsDiscard = false;
                                db.SaveChanges();

                                var ischkOut = db.DocumentDetails.Where(x => x.DocumentId == DocumentId).FirstOrDefault();
                                ischkOut.IsCheckOut = false;
                                db.SaveChanges();
                                transaction.Commit();
                            }

                        }
                        else  // Go for Check out
                        {
                            using (db = new DBEntities())
                            {
                                var docDetail = db.DocumentDetails.Find(DocumentId);
                                if (docDetail != null)
                                {
                                    CheckInOutHistory chkHistory = new CheckInOutHistory
                                    {
                                        DocumentId = DocumentId,
                                        CabinetId = cabinetId,
                                        FolderId = docDetail.FolderId,
                                        CheckOutBy = userId.ToString(),
                                        CheckOutDate = DateTime.Now,
                                        CheckInBy = null,
                                        CheckInDate = null,
                                        DocumentVersionId = 0,
                                    };
                                    db.CheckInOutHistories.Add(chkHistory);

                                    docDetail.IsCheckOut = true;
                                    db.SaveChanges();

                                    transaction.Commit();
                                    return Json(status, JsonRequestBehavior.AllowGet);
                                }
                                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                            }

                        }
                    }
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        public JsonResult FileUndoCheckOut(long Id, string[] folderIds, string[] fileIds, bool status)
        {
            long DocumentId = 0;
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    long userId = CommonFunctions.GetUserId();
                    long cabinetId = CommonFunctions.GetCabinetId();

                    //for Files
                    if (fileIds != null && fileIds.Length > 0)
                    {
                        DocumentId = Convert.ToInt64(fileIds[0]);
                        var IsDiscard = db.CheckInOutHistories.Where(x => x.DocumentId == DocumentId).FirstOrDefault();
                        var docVersion = db.DocumentVersionDetails.Where(x => x.DocumentId == DocumentId && x.IsCurrent == true).FirstOrDefault();
                        var CheckOutBy = db.CheckInOutHistories.Where(x => x.DocumentId == DocumentId && x.DocumentVersionId == 0).OrderBy(x => x.Id).Select(x => x.CheckOutBy).FirstOrDefault();

                        if (status == true) // Go for UNDOCHECKOUT
                        {
                            using (db = new DBEntities())
                            {
                                var ChckHistory = db.CheckInOutHistories.Find(Id);
                                ChckHistory.CheckInBy = userId.ToString();
                                ChckHistory.CheckInDate = DateTime.Now;
                                ChckHistory.DocumentVersionId = docVersion.Id;
                                ChckHistory.IsDiscard = true;
                                db.SaveChanges();

                                var ischkOut = db.DocumentDetails.Where(x => x.DocumentId == DocumentId).FirstOrDefault();
                                ischkOut.IsCheckOut = false;
                                db.SaveChanges();
                                transaction.Commit();
                            }
                        }

                    }
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public JsonResult CheckOutPermission(long Id)
        {
            long userId = CommonFunctions.GetUserId();
            var CheckOut = db.CheckInOutHistories.Find(Id);
            if (CheckOut != null && CheckOut.CheckOutBy == userId.ToString())
            {
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region --> Dynamic Form View | Add | Sweta Patel | 30/7/2018
        [HttpGet]
        public PartialViewResult _DynamicFormView(ViewDynamicFormVM formdetail)
        {

            var userIds = CommonFunctions.GetUserId();

            try
            {
                DynamicFormResponseVM frmData = new DynamicFormResponseVM();
                AssignFormTemplate frm = new AssignFormTemplate();
                DynamicFormResponse response = new DynamicFormResponse();

                if (formdetail.AssignFormTemplateId > 0)
                {
                    response = db.DynamicFormResponses.Where(x => x.FormTemplateId == formdetail.AssignFormTemplateId && x.CreatedBy == userIds.ToString()).OrderByDescending(x => x.ID).FirstOrDefault();

                    frm = db.AssignFormTemplates.Find(formdetail.AssignFormTemplateId);
                    if (frm != null)
                    {
                        frmData.FormTemplateId = frm.Id;
                        frmData.HtmlContent = frm.FormContant;
                        frmData.FormTitle = frm.FormTitle;

                    }
                    if (response != null)
                    {
                        frmData.ID = response.ID;
                        frmData.FormTemplateId = response.FormTemplateId;
                        frmData.FormTitle = frm.FormTitle;
                        frmData.HtmlContent = frm.FormContant;
                        frmData.ResponseCode = response.ResponseCode;
                        frmData.AttachedId = response.AttachedId;
                        frmData.AttachedType = response.AttachedType;
                        frmData.CreatedBy = frm.CreatedBy;
                        frmData.SelectedFileId = formdetail.SelectedId;

                        var ResponseDetailList = db.DynamicFormResponses.Where(x => x.ResponseCode == response.ResponseCode).ToList();

                        if (ResponseDetailList != null && ResponseDetailList.Count > 0)
                        {
                            foreach (var item in ResponseDetailList)
                            {
                                DynamicFormResponseDetailVM filedDetail = new DynamicFormResponseDetailVM();
                                filedDetail.FiledId = item.FiledId;
                                filedDetail.Type = item.Type;
                                filedDetail.Value = item.Value;
                                filedDetail.IsSelected = item.IsSelected;
                                if (frmData != null)
                                {
                                    frmData.DynamicFormResponseDetailList.Add(filedDetail);
                                }
                            }
                        }
                    }
                }
                return PartialView(frmData != null ? frmData : new DynamicFormResponseVM());
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region --> Remove keyword from Info in folder and document | Add | Dhrumil Patel | 22082018
        public JsonResult RemoveKeywords(long Id, bool IsDoc, string Keywords)
        {
            try
            {
                if (IsDoc)
                {
                    var detail = db.DocumentVersionDetails.Find(Id);
                    detail.Keywords = Keywords;
                    db.SaveChanges();
                }
                else
                {
                    var detail = db.FolderDetails.Find(Id);
                    detail.Keywords = Keywords;
                    db.SaveChanges();
                }
            }
            catch
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}