﻿using DocWare.Helpers;
using DocWare.Models;
using DocWare.Resources;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utility.Helpers;
using Utility.Models;

namespace DocWare.Controllers
{
    [CheckAuthorization]
    public class UserManagementController : BaseController
    {
        DBEntities db = new DBEntities();
        DAL objDAL = new DAL();

        #region --> User List | Add | Dhrumil Patel | 07022018
        [HttpGet, Route("UserList")]
        public ActionResult UserList()
        {
            #region --> Page Permission on Action and controller name | Jasmin | 21032018

            var permissionlst = Session[SessionClass.LinkPermissionList].ToString();
            List<UserPermissionVM> userPermissionLst = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserPermissionVM>>(permissionlst);

            PermissionVM permissionVM = new PermissionVM();

            permissionVM.HasAddPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "UserRegistration" && x.ControllerName.Trim() == "UserManagement" && x.Type.Trim() == "Add").Count() > 0;
            permissionVM.HasEditPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "UserRegistration" && x.ControllerName.Trim() == "UserManagement" && x.Type.Trim() == "Edit").Count() > 0;
            permissionVM.HasStatusPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "ChangeUserStatus" && x.ControllerName.Trim() == "UserManagement").Count() > 0;            
            permissionVM.HasDeactivePermission = userPermissionLst.Where(x => x.ActionName.Trim() == "DeactiveSelectedUser" && x.ControllerName.Trim() == "UserManagement").Count() > 0;
            permissionVM.HasDeletePermission = userPermissionLst.Where(x => x.ActionName.Trim() == "DeleteSelectedUser" && x.ControllerName.Trim() == "UserManagement").Count() > 0;
            permissionVM.HasDocSharePermission = userPermissionLst.Where(x => x.ActionName.Trim() == "UserDocSharedPermission" && x.ControllerName.Trim() == "UserManagement").Count() > 0;
            permissionVM.HasConfigurationPermisssion = userPermissionLst.Where(x => x.ActionName.Trim() == "UserRights" && x.ControllerName.Trim() == "RoleManagement").Count() > 0;
            permissionVM.HasWorkFlowPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "UserWorkFlowPermission" && x.ControllerName.Trim() == "UserManagement").Count() > 0;

            Session["PermissionListSession"] = permissionVM;
            #endregion

            SortedList sl = new SortedList();
            DataTable dt = new DataTable();
            List<UserVM> lstUser = new List<UserVM>();
            var cabinetId = CommonFunctions.GetCabinetId();
            sl.Add("@CabinetId", cabinetId);
            dt = objDAL.GetDataTable("uspGetUserList", sl);
            lstUser = objDAL.ConvertToList<UserVM>(dt);
            return View(lstUser);
        }
        #endregion

        #region --> User Registration | Add | Dhrumil Patel | 08022018
        [HttpGet, Route("UserRegistration")]
        public ActionResult UserRegistration(int? id, bool flag = false)
        {
            UserVM userdetail = new UserVM();
            try
            {
                userdetail.IsEdit = flag;
                userdetail.Gender = "M";
                if (id != null && id != 0)
                {
                    var data = db.LoginCredentials.Find(id) == null ? new LoginCredential() : db.LoginCredentials.Find(id);
                    if (data != null)
                    {
                        userdetail.UserId = data.Id;
                        userdetail.FirstName = data.FirstName;
                        userdetail.LastName = data.LastName;
                        userdetail.MiddleName = data.MiddleName;
                        userdetail.ProfileImage = data.ProfileImage;
                        // userdetail.UserName = db.UserLoginDetails.Where(x => x.UserId == data.Id).Select(x => x.UserName).FirstOrDefault();
                        userdetail.EmailId = data.EmailId;
                        userdetail.MobileNumber = data.MobileNumber;
                        userdetail.PhoneNumber = data.PhoneNumber;
                        userdetail.DOB = data.DOB.ToString("MM/dd/yyyy");
                        userdetail.DepartmentId = data.DepartmentId;
                        userdetail.SectionId = data.SectionId;
                        userdetail.PositionId = Convert.ToInt64(data.PositionId);
                        userdetail.ManagerId = Convert.ToInt64(data.ManagerId);
                        userdetail.SentNotification = data.SentNotification;
                        userdetail.Gender = (data.Gender != "F" && data.Gender != "M") ? "M" : data.Gender;

                        var cabinetIds = db.UserCabinetDetails.Where(x => x.UserId == data.Id).Select(x => x.CabinetId).ToList();
                        var cabinetLst = db.CabinetMasters.Where(x => cabinetIds.Contains(x.Id) && x.IsActive && !x.IsDelete).Select(y => y.Id).ToArray();
                        List<string> strCabinet = new List<string>();
                        foreach (var ids in cabinetLst)
                        {
                            strCabinet.Add(ids.ToString());
                        }
                        userdetail.UserCabinet = strCabinet.ToArray();

                        var roleIds = db.UserRoles.Where(x => x.UserId == data.Id).Select(x => x.RoleId).ToList();
                        var roleLst = db.RoleMasters.Where(x => roleIds.Contains(x.Id) && x.IsActive).Select(y => y.Id).ToArray();
                        List<string> strRole = new List<string>();
                        foreach (var ids in roleLst)
                        {
                            strRole.Add(ids.ToString());
                        }
                        userdetail.Roles = strRole.ToArray();

                        var appLst = db.UserLoginDetails.Where(x => x.UserId == data.Id).Select(y => y.ApplicationId).ToArray();
                        List<string> strApp = new List<string>();
                        foreach (var ids in appLst)
                        {
                            strApp.Add(ids.ToString());
                        }
                        userdetail.Applications = strApp.ToArray();
                    }
                }
            }
            catch (Exception e)
            {
                return View(userdetail);
            }
            return View(userdetail);
        }

        [HttpPost, Route("UserRegistration")]
        [AuditLog(EnumList.EnumLogType.SaveData, "UserRegistration")]
        public ActionResult UserRegistration(UserVM detail, FormCollection fc, HttpPostedFileBase updProfileImage)
        {
            bool IsAdmin = false, IsUser = false;
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    #region profile image 
                    string fileName = string.Empty;
                    if (updProfileImage == null)
                    {
                        fileName = detail.ProfileImage;
                    }
                    else
                    {
                        fileName = updProfileImage.FileName;
                    }
                    #endregion

                    #region Login credential insert/update
                    if (detail.UserId == 0)
                    {
                        #region Login credential insert 
                        var userdetail = new LoginCredential();
                        userdetail.FirstName = detail.FirstName;
                        userdetail.MiddleName = detail.MiddleName;
                        userdetail.LastName = detail.LastName;
                        userdetail.Gender = detail.Gender;
                        userdetail.ProfileImage = fileName;
                        userdetail.EmailId = detail.EmailId;
                        userdetail.DepartmentId = detail.DepartmentId;
                        userdetail.SectionId = detail.SectionId;
                        userdetail.PositionId = detail.PositionId;
                        userdetail.ManagerId = detail.ManagerId;
                        userdetail.MobileNumber = detail.MobileNumber;
                        userdetail.PhoneNumber = detail.PhoneNumber;
                        userdetail.DOB = Convert.ToDateTime(detail.DOB);
                        userdetail.CreatedBy = CommonFunctions.GetUserId().ToString();
                        userdetail.SentNotification = fc["chkSentNotification"] == "on" ? true : false;
                        userdetail.IsActive = true;
                        userdetail.IsDelete = false;
                        userdetail.CreatedDate = DateTime.Now;
                        db.LoginCredentials.Add(userdetail);
                        db.SaveChanges();
                        detail.UserId = userdetail.Id;
                        #endregion
                    }
                    else
                    {
                        #region Login credential update 
                        var userdetail = db.LoginCredentials.Find(detail.UserId);
                        userdetail.FirstName = detail.FirstName;
                        userdetail.MiddleName = detail.MiddleName;
                        userdetail.LastName = detail.LastName;
                        userdetail.Gender = detail.Gender;
                        userdetail.ProfileImage = fileName;
                        userdetail.EmailId = detail.EmailId;
                        userdetail.DepartmentId = detail.DepartmentId;
                        userdetail.SectionId = detail.SectionId;
                        userdetail.PositionId = detail.PositionId;
                        userdetail.ManagerId = detail.ManagerId;
                        userdetail.MobileNumber = detail.MobileNumber;
                        userdetail.PhoneNumber = detail.PhoneNumber;
                        userdetail.DOB = Convert.ToDateTime(detail.DOB);
                        userdetail.SentNotification = fc["chkSentNotification"] == "on" ? true : false;
                        if (detail.IsEdit)
                        {
                            userdetail.IsActive = true;
                            userdetail.IsDelete = false;
                        }
                        userdetail.UpdatedDate = DateTime.Now;
                        userdetail.UpdatedBy = CommonFunctions.GetUserId().ToString();
                        db.SaveChanges();
                        #endregion
                    }
                    #endregion

                    #region save profile image 
                    if (updProfileImage != null)
                    {
                        string folderPathUserimage = string.Empty;
                        var path = ConfigurationManager.AppSettings["PathForProfileUpload"] + detail.UserId.ToString();
                        folderPathUserimage = Server.MapPath(path);
                        if (!Directory.Exists(folderPathUserimage))
                        {
                            Directory.CreateDirectory(folderPathUserimage);
                        }
                        string _FileName = Path.GetFileName(updProfileImage.FileName);
                        string _path = Path.Combine(folderPathUserimage, _FileName);
                        if (detail.ProfileImage != null)
                        {
                            string _deletepath = Path.Combine(folderPathUserimage, detail.ProfileImage);
                            if (System.IO.File.Exists(_deletepath))
                            {
                                System.IO.File.Delete(_deletepath);
                            }
                        }
                        updProfileImage.SaveAs(_path);
                    }
                    #endregion

                    #region User role insert/update
                    if (detail.Roles != null)
                    {
                        var roleRemoveLst = db.UserRoles.Where(x => x.UserId == detail.UserId).ToList();
                        if (roleRemoveLst.Count() > 0)
                        {
                            db.UserRoles.RemoveRange(roleRemoveLst);
                            db.SaveChanges();
                        }
                        UserRole userRole = new UserRole();
                        foreach (var roleid in detail.Roles)
                        {
                            if (roleid != "")
                            {
                                userRole.UserId = detail.UserId;
                                userRole.RoleId = Convert.ToInt64(roleid);
                                db.UserRoles.Add(userRole);
                                db.SaveChanges();
                            }
                        }
                    }
                    #endregion

                    #region Check Application Assigned
                    if (detail.Applications != null)
                    {
                        foreach (var applicationsid in detail.Applications)
                        {
                            if (applicationsid != "")
                            {
                                IsAdmin = IsAdmin == true ? true : (Convert.ToInt64(applicationsid) == (long)EnumList.Application.Admin ? true : false);
                                IsUser = IsUser == true ? true : (Convert.ToInt64(applicationsid) == (long)EnumList.Application.User ? true : false);
                            }
                        }
                    }
                    #endregion

                    #region User login detail insert/update
                    var userLogindetailList = db.UserLoginDetails.Where(x => x.UserId == detail.UserId).ToList();
                    var adminLst = userLogindetailList.Where(x => x.ApplicationId == (long)EnumList.Application.Admin).ToList();
                    var userLst = userLogindetailList.Where(x => x.ApplicationId == (long)EnumList.Application.User).ToList();
                    if (userLogindetailList.Count() != 0 && detail.Password == null)
                    {
                        detail.UserName = userLogindetailList.FirstOrDefault().UserName;
                        detail.Password = UtilityCommonFunctions.Decrypt(userLogindetailList.FirstOrDefault().Password);
                    }
                    if (IsAdmin && adminLst.Count() == 0)
                    {
                        var userLoginDetail = new UserLoginDetail();
                        userLoginDetail.Password = UtilityCommonFunctions.Encrypt(detail.Password);
                        userLoginDetail.UserName = detail.UserName;
                        userLoginDetail.ApplicationId = (byte)EnumList.Application.Admin;
                        userLoginDetail.UserId = detail.UserId;
                        userLoginDetail.LastLogin = DateTime.Now;
                        db.UserLoginDetails.Add(userLoginDetail);
                        db.SaveChanges();

                        #region --> Add Password in PasswordHistory
                        PasswordHistory pswHistory = new PasswordHistory();
                        pswHistory.UserId = detail.UserId; ;
                        pswHistory.ApplicationId = (byte)EnumList.Application.Admin;
                        pswHistory.Password = userLoginDetail.Password;
                        pswHistory.ChangeDate = DateTime.Now;
                        db.PasswordHistories.Add(pswHistory);
                        db.SaveChanges();
                        #endregion
                    }
                    else
                    {
                        if (adminLst.Count() != 0 && !IsAdmin)
                        {
                            db.UserLoginDetails.RemoveRange(adminLst);
                            db.SaveChanges();
                        }
                    }
                    if (IsUser && userLst.Count() == 0)
                    {
                        var userLoginDetail = new UserLoginDetail();
                        userLoginDetail.Password = UtilityCommonFunctions.Encrypt(detail.Password);
                        userLoginDetail.UserName = detail.UserName;
                        userLoginDetail.ApplicationId = (byte)EnumList.Application.User;
                        userLoginDetail.UserId = detail.UserId;
                        userLoginDetail.LastLogin = DateTime.Now;
                        db.UserLoginDetails.Add(userLoginDetail);
                        db.SaveChanges();

                        #region --> Add Password in PasswordHistory
                        PasswordHistory pswHistory = new PasswordHistory();
                        pswHistory.UserId = detail.UserId; ;
                        pswHistory.ApplicationId = (byte)EnumList.Application.User;
                        pswHistory.Password = userLoginDetail.Password;
                        pswHistory.ChangeDate = DateTime.Now;
                        db.PasswordHistories.Add(pswHistory);
                        db.SaveChanges();
                        #endregion
                    }
                    else
                    {
                        if (userLst.Count() != 0 && !IsUser)
                        {
                            db.UserLoginDetails.RemoveRange(userLst);
                            db.SaveChanges();
                        }
                    }
                    #endregion

                    #region user cabinet detail insert/update
                    if (detail.UserCabinet != null)
                    {
                        var permissionRemoveLst = db.UserCabinetDetails.Where(x => x.UserId == detail.UserId).ToList();
                        if (permissionRemoveLst.Count() > 0)
                        {
                            db.UserCabinetDetails.RemoveRange(permissionRemoveLst);
                            db.SaveChanges();
                        }
                        UserCabinetDetail cabinetPermission = new UserCabinetDetail();
                        foreach (var cabinet in detail.UserCabinet)
                        {
                            if (cabinet != "")
                            {
                                cabinetPermission.CabinetId = Convert.ToInt64(cabinet);
                                cabinetPermission.UserId = detail.UserId;
                                db.UserCabinetDetails.Add(cabinetPermission);
                                db.SaveChanges();
                            }
                        }
                    }
                    #endregion

                    if (detail.UserId == 0)
                    {
                        TempData["SuccessMSG"] = Messages.User_Registered;
                    }
                    else
                    {
                        TempData["SuccessMSG"] = Messages.User_Updated;
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    TempData["ErrorMSG"] = Messages.Error_User_Update;
                    return RedirectToAction("UserList");
                }
                transaction.Commit();
            }
            return RedirectToAction("UserList");
        }
        #endregion

        #region --> Email exist or not | Add | Dhrumil Patel | 09022018
        [Route("IsEmailExist")]
        public JsonResult IsEmailExist(string EmailId, long? id = 0)
        {
            var EmailAddress = EmailId;
            bool IsValid = false;
            long? Id = id;
            if (Id > 0)
            {
                LoginCredential ObjDepartmentMaster = db.LoginCredentials.Find(Id);
                if (EmailAddress.Trim() == ObjDepartmentMaster.EmailId.Trim())
                {
                    IsValid = true;
                    return Json(IsValid, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var UserId = db.LoginCredentials.Where(x => x.EmailId.Trim() == EmailAddress.Trim()).Select(x => x.Id).FirstOrDefault();
                    if (UserId > 0)
                    {
                        IsValid = false;
                        return Json(IsValid, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        IsValid = true;
                        return Json(IsValid, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                if (EmailAddress != null)
                {
                    var UserId = db.LoginCredentials.Where(x => x.EmailId.ToLower().Trim() == EmailAddress.ToLower().Trim()).Select(x => x.Id).FirstOrDefault();
                    if (UserId > 0)
                    {
                        IsValid = false;
                        return Json(IsValid, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        IsValid = true;
                        return Json(IsValid, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            IsValid = true;
            return Json(IsValid, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region --> Username exist or not | Add | Dhrumil Patel | 20022018
        [Route("IsUserNameExist")]
        public JsonResult IsUserNameExist(string UserName, long? id = 0)
        {
            var Username = UserName;
            bool IsValid = false;
            long? Id = id;
            if (Id > 0)
            {
                var appId = CommonFunctions.GetApplicationId();
                UserLoginDetail ObjDepartmentMaster = db.UserLoginDetails.FirstOrDefault(x => x.UserId == id && x.ApplicationId == appId);
                if (Username.ToLower().Trim() == ObjDepartmentMaster.UserName.ToLower().Trim())
                {
                    IsValid = true;
                    return Json(IsValid, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var userDetailIds = db.UserLoginDetails.Where(x => x.UserName.ToLower().Trim() == UserName.ToLower().Trim() && x.ApplicationId == appId).Select(x => x.UserId).ToList();
                    if (userDetailIds.Count() > 0)
                    {
                        var UserId = db.LoginCredentials.Where(x => userDetailIds.Contains(x.Id)).Select(x => x.Id).FirstOrDefault();
                        if (UserId > 0)
                        {
                            IsValid = false;
                            return Json(IsValid, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            IsValid = true;
                            return Json(IsValid, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        IsValid = true;
                        return Json(IsValid, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                if (Username != null)
                {
                    var userDetailIds = db.UserLoginDetails.Where(x => x.UserName.ToLower() == UserName.ToLower()).Select(x => x.UserId).ToList();
                    if (userDetailIds.Count() > 0)
                    {
                        var UserId = db.LoginCredentials.Where(x => userDetailIds.Contains(x.Id)).Select(x => x.Id).FirstOrDefault();
                        if (UserId > 0)
                        {
                            IsValid = false;
                            return Json(IsValid, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            IsValid = true;
                            return Json(IsValid, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        IsValid = true;
                        return Json(IsValid, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            IsValid = true;
            return Json(IsValid, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region --> Bind User, Section and Postion List | Add| Dhrumil Patel | 20022018
        public JsonResult GetUserByDepartmentID(long DepartmentID)
        {
            try
            {
                List<SelectListItem> lst = CommonFunctions.GetUserListByDepartment(DepartmentID);
                return Json(new { success = true, lst = lst }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        public JsonResult GetSectionByDepartmentID(long DepartmentID)
        {
            try
            {
                List<SelectListItem> lst = CommonFunctions.GetSectionListByDepartment(DepartmentID);
                return Json(new { success = true, lst = lst }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        public JsonResult GetPositionBySectionID(long SectionID)
        {
            try
            {
                List<SelectListItem> lst = CommonFunctions.GetPositionListBySection(SectionID);
                return Json(new { success = true, lst = lst }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }
        #endregion

        #region --> Change User Status and Restore | Add | Dhrumil Patel | 20022018
        public bool IsUserPresent(List<string> ids)
        {
            var loginCount = db.LoginCredentials.Where(x => ids.Contains(x.ManagerId.ToString()) && x.IsActive && !x.IsDelete).ToList().Count();
            if (loginCount > 0)
            {
                return true;
            }
            else
            {
                var deptCount = db.DepartmentMasters.Where(x => ids.Contains(x.DepartmentManagerId.ToString()) && x.IsActive && !x.IsDelete).ToList().Count();
                if (deptCount > 0)
                {
                    return true;
                }
                else
                {
                    var sectCount = db.SectionMasters.Where(x => ids.Contains(x.SectionManagerId.ToString()) && x.IsActive && !x.IsDelete).ToList().Count();
                    if (sectCount > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }

        public string DeactiveList(long id)
        {
            string msg = string.Empty;
            var logindetail = db.LoginCredentials.Find(id);
            var DepartmentDetail = db.DepartmentMasters.FirstOrDefault(x => x.Id == logindetail.DepartmentId && !x.IsActive);
            if (DepartmentDetail != null)
            {
                msg = "Allocated Department";
            }
            var SectionDetail = db.SectionMasters.FirstOrDefault(x => x.Id == logindetail.SectionId && !x.IsActive);
            if (SectionDetail != null)
            {
                if (msg == string.Empty)
                {
                    msg = "Allocated Section";
                }
                else
                {
                    msg += ", Section";
                }
            }
            var PositionDetail = db.PositionMasters.FirstOrDefault(x => x.Id == logindetail.PositionId && !x.IsActive);
            if (PositionDetail != null)
            {
                if (msg == string.Empty)
                {
                    msg = "Allocated Position";
                }
                else
                {
                    msg += ", Position";
                }
            }
            var ManagerDetail = db.LoginCredentials.FirstOrDefault(x => x.Id == logindetail.ManagerId && !x.IsActive);
            if (ManagerDetail != null)
            {
                if (msg == string.Empty)
                {
                    msg = "Allocated Manager";
                }
                else
                {
                    msg += ", Manager";
                }
            }
            var CabinetDetail = db.UserCabinetDetails.Where(x => x.UserId == logindetail.Id).ToList();
            foreach (var detail in CabinetDetail)
            {
                var cabinet = db.CabinetMasters.FirstOrDefault(x => x.Id == detail.CabinetId && !x.IsActive);
                if (cabinet != null)
                {
                    if (msg == string.Empty)
                    {
                        msg = "Allocated Cabinet";
                    }
                    else
                    {
                        msg += ", Cabinet";
                    }
                    break;
                }
            }
            var RoleDetail = db.UserRoles.Where(x => x.UserId == logindetail.Id).ToList();
            foreach (var detail in RoleDetail)
            {
                var role = db.RoleMasters.FirstOrDefault(x => x.Id == detail.RoleId && !x.IsActive);
                if (role != null)
                {
                    if (msg == string.Empty)
                    {
                        msg = "Allocated Role";
                    }
                    else
                    {
                        msg += ", Role";
                    }
                    break;
                }
            }
            if (msg == string.Empty)
            {
                return msg;
            }
            else
            {
                return msg += " is deactivated or deleted.";
            }
        }

        [AuditLog(EnumList.EnumLogType.DeleteData, "DeleteSelectedUser")]
        public JsonResult DeleteSelectedUser(string ids)
        {
            LoginCredential userMaster = new LoginCredential();
            var logindetail = db.LoginCredentials.ToList();
            var flag = false;
            try
            {
                if (ids != "0")
                {
                    using (db = new DBEntities())
                    {
                        var userIds = ids.Split(',').ToList();
                        //Need to check for work folw condition of any user present in on going workflowin or not.
                        flag = IsUserPresent(userIds);
                        if (flag)
                        {
                            return Json(false, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            foreach (var id in userIds)
                            {
                                if (id != "")
                                {
                                    userMaster = logindetail.FirstOrDefault(m => m.Id == Convert.ToInt64(id));
                                    var userid = CommonFunctions.GetUserId();
                                    if (userMaster != null)
                                    {
                                        userMaster.IsActive = false;
                                        userMaster.IsDelete = true;
                                        userMaster.UpdatedBy = userid.ToString();
                                        userMaster.UpdatedDate = DateTime.Now;
                                        db.Entry(userMaster).State = EntityState.Modified;
                                        db.SaveChanges();
                                    }
                                }
                            }
                            return Json(true, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "DeactiveSelectedUser")]
        public JsonResult DeactiveSelectedUser(string ids)
        {
            LoginCredential userMaster = new LoginCredential();
            var logindetail = db.LoginCredentials.ToList();
            var flag = false;
            try
            {
                if (ids != "0")
                {
                    using (db = new DBEntities())
                    {
                        var userIds = ids.Split(',').ToList();
                        //Need to check for work folw condition of any user present in on going workflowin or not.
                        flag = IsUserPresent(userIds);
                        if (flag)
                        {
                            return Json(false, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            foreach (var id in userIds)
                            {
                                if (id != "")
                                {
                                    userMaster = logindetail.FirstOrDefault(m => m.Id == Convert.ToInt64(id));
                                    var userid = CommonFunctions.GetUserId();
                                    if (userMaster != null)
                                    {
                                        userMaster.IsActive = false;
                                        userMaster.UpdatedBy = userid.ToString();
                                        userMaster.UpdatedDate = DateTime.Now;
                                        db.Entry(userMaster).State = EntityState.Modified;
                                        db.SaveChanges();
                                    }
                                }
                            }
                            return Json(true, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "ChangeUserStatus")]
        public JsonResult ChangeUserStatus(long id)
        {
            LoginCredential userMaster = new LoginCredential();
            try
            {
                if (id > 0)
                {
                    using (db = new DBEntities())
                    {
                        var userid = CommonFunctions.GetUserId();
                        userMaster = db.LoginCredentials.Where(m => m.Id == id).FirstOrDefault();
                        if (userMaster != null)
                        {
                            if (userMaster.IsActive)
                            {
                                List<string> ids = new List<string>();
                                ids.Add(id.ToString());
                                //Need to check for work folw condition of any user present in on going workflowin or not.
                                var flag = IsUserPresent(ids);
                                if (flag)
                                {
                                    return Json(new { flag = false, msg = "" }, JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    userMaster.IsActive = false;
                                }
                            }
                            else
                            {
                                //Need to check for work folw condition of any user present in on going workflowin or not.
                                var msg = DeactiveList(id);
                                if (msg == "")
                                {
                                    userMaster.IsActive = true;
                                }
                                else
                                {
                                    return Json(new { flag = false, msg = msg }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            userMaster.UpdatedBy = userid.ToString();
                            userMaster.UpdatedDate = DateTime.Now;
                            db.Entry(userMaster).State = EntityState.Modified;
                            db.SaveChanges();
                            return Json(new { flag = true, msg = "" }, JsonRequestBehavior.AllowGet);
                        }
                        return Json(new { flag = false, msg = "" }, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(new { flag = true, msg = "" }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { flag = false, msg = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "RestoreUser")]
        public JsonResult RestoreUser(long id)
        {
            LoginCredential userMaster = new LoginCredential();
            try
            {
                if (id > 0)
                {
                    using (db = new DBEntities())
                    {
                        var userid = CommonFunctions.GetUserId();
                        userMaster = db.LoginCredentials.Where(m => m.Id == id).FirstOrDefault();
                        if (userMaster != null)
                        {
                            //Need to check for work folw condition of any user present in on going workflowin or not.
                            var msg = DeactiveList(id);
                            if (msg == "")
                            {
                                userMaster.IsActive = true;
                                userMaster.IsDelete = false;
                            }
                            else
                            {
                                return Json(new { flag = false, msg = msg }, JsonRequestBehavior.AllowGet);
                            }
                            userMaster.UpdatedBy = userid.ToString();
                            userMaster.UpdatedDate = DateTime.Now;
                            db.Entry(userMaster).State = EntityState.Modified;
                            db.SaveChanges();
                            return Json(new { flag = true, msg = "" }, JsonRequestBehavior.AllowGet);
                        }
                        return Json(new { flag = false, msg = "" }, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(new { flag = true, msg = "" }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { flag = false, msg = "" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region --> Set Document Share Permission | Add | Dhrumil Patel | 20022018
        [HttpGet, Route("UserDocSharedPermission")]
        public ActionResult UserDocSharedPermission(long Id)
        {
            DocSharePermissionDetail detail = new DocSharePermissionDetail();
            detail.UserId = Id;
            var cabId = CommonFunctions.GetCabinetId();
            var cabinetDetail = db.CabinetMasters.Find(cabId);
            detail.CabinetName = cabinetDetail != null ? cabinetDetail.Name : "";
            var userDetail = db.LoginCredentials.Find(Id);
            detail.FullName = userDetail != null ? userDetail.FirstName + " " + userDetail.LastName : "";

            try
            {
                if (Id > 0)
                {
                    using (db = new DBEntities())
                    {
                        var permissionList = db.UserDocSharePermissions.Where(a => a.UserId == Id && a.CabinetId == cabId).ToList();
                        if (permissionList.Count > 0)
                        {
                            var Users = permissionList.Where(x => x.DocShareUserId != 0).Select(x => x.DocShareUserId.ToString()).ToList();
                            if (Users.Count > 0)
                            {
                                detail.SharedUsers = Users.ToArray();
                            }
                            var Groups = permissionList.Where(x => x.DocShareGroupId != 0).Select(x => x.DocShareGroupId.ToString()).ToList();
                            if (Groups.Count > 0)
                            {
                                detail.SharedGroups = Groups.ToArray();
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return View(detail);
            //DocSharePermissionUserList detail = new DocSharePermissionUserList();
            //detail.UserId = Id;
            //return View(detail);
        }

        [HttpPost, Route("UserDocSharedPermission")]
        public ActionResult UserDocSharedPermission(DocSharePermissionDetail detail)
        {
            var users = "";
            if (detail.SharedUsers != null)
            {
                foreach (var item in detail.SharedUsers)
                {
                    users += item + ",";
                }
            }
            var groups = "";
            if (detail.SharedGroups != null)
            {
                foreach (var item in detail.SharedGroups)
                {
                    groups += item + ",";
                }
            }
            SortedList param = new SortedList();
            param.Add("@UserId", detail.UserId);
            param.Add("@CabinetId", CommonFunctions.GetCabinetId());
            param.Add("@SharedUserList", users.Substring(0, users.Length - 1));
            param.Add("@SharedGroupList", groups.Substring(0, groups.Length - 1));

            int iResult = objDAL.ExecuteSQL("uspSaveDocSharedUserPermission", param);
            if (iResult >= 0)
            {
                TempData["SuccessMSG"] = Messages.Permission_Shared_Successfully;
            }
            else
            {
                TempData["ErrorMSG"] = Messages.Permission_Not_Shared_Successfully;
            }
            return RedirectToAction("UserList");
        }

        //[HttpPost]
        //public ActionResult _UserDocSharedPermission(DocSharePermissionUserList obj)
        //{
        //    List<DocSharePermissionUserList> list = new List<DocSharePermissionUserList>();
        //    var userList = binduserList(obj.CabinetId, obj.UserId);
        //    var sharedUserList = db.UserDocSharePermissions.Where(x => x.UserId == obj.UserId && x.CabinetId == obj.CabinetId)
        //        .Select(x => new DocSharePermissionUserList
        //        {
        //            ShareUserId = x.DocShareUserId,
        //        }).ToList();
        //    list = userList.Select(x => new DocSharePermissionUserList
        //    {
        //        UserName = x.FullName,
        //        RoleName = "(" + x.RoleName + ")",
        //        ShareUserId = x.UserId,
        //        CabinetId = obj.CabinetId,
        //        IsChecked = false,
        //    }).ToList();

        //    if (sharedUserList != null)
        //    {
        //        foreach (var shareList in sharedUserList)
        //        {
        //            foreach (var chList in list)
        //            {
        //                if (chList.ShareUserId == shareList.ShareUserId)
        //                {
        //                    chList.IsChecked = true;
        //                }
        //            }
        //        }
        //    }
        //    return PartialView(list);
        //}

        //[HttpPost]
        //[AuditLog(EnumList.EnumLogType.SaveData, "SaveDocSharePermission")]
        //public JsonResult SaveDocSharePermission(DocSharePermissionUserList detail)
        //{
        //    SortedList param = new SortedList();
        //    string Message = string.Empty;
        //    string Type = string.Empty;
        //    string Css = string.Empty;
        //    param.Add("@UserId", detail.UserId);
        //    param.Add("@CabinetId", detail.CabinetId);
        //    param.Add("@SharedUserList", detail.SharedUserList);

        //    int iResult = objDAL.ExecuteSQL("uspSaveDocSharedUserPermission", param);
        //    if (iResult >= 0)
        //    {
        //        Type = "Successful";
        //        Message = Messages.Permission_Shared_Successfully;
        //        Css = "success";
        //    }
        //    else
        //    {
        //        Type = "Unsuccessful";
        //        Message = Messages.Permission_Not_Shared_Successfully;
        //        Css = "error";
        //    }
        //    return Json(new { Message = Message, Css = Css, Type = Type }, JsonRequestBehavior.AllowGet);
        //}
        #endregion

        #region --> Bind User Cabinet Wise | Add | Dhrumil Patel | 08022018
        [Route("binduserList")]
        public List<CabinetWiseUserList> binduserList(long CabinetId, long UserId)
        {
            SortedList sl = new SortedList();
            DataTable dt = new DataTable();
            List<SelectListItem> CabinetWiseUser = new List<SelectListItem>();
            List<CabinetWiseUserList> lstCabinetWiseUser = new List<CabinetWiseUserList>();
            sl.Add("@userId", UserId);
            sl.Add("@cabinetId", CabinetId);
            dt = objDAL.GetDataTable("uspGetUserListByCabinet", sl);
            lstCabinetWiseUser = objDAL.ConvertToList<CabinetWiseUserList>(dt);
            return lstCabinetWiseUser != null ? lstCabinetWiseUser : new List<CabinetWiseUserList>();
        }
        #endregion

        #region --> Deleted User List | Add | Dhrumil Patel | 07022018
        [HttpGet, Route("DeletedUserList")]
        public ActionResult DeletedUserList()
        {
            #region --> Page Permission on Action and controller name | Dhrumil Patel | 22032018

            var permissionlst = Session[SessionClass.LinkPermissionList].ToString();
            List<UserPermissionVM> userPermissionLst = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserPermissionVM>>(permissionlst);

            PermissionVM permissionVM = new PermissionVM();
            permissionVM.HasRestorePermisssion = userPermissionLst.Where(
                                x => x.ActionName.Trim() == "RestoreUser"
                                && x.ControllerName.Trim() == "UserManagement").Count() > 0;

            Session["PermissionListSession"] = permissionVM;
            #endregion
            SortedList sl = new SortedList();
            DataTable dt = new DataTable();
            List<UserVM> lstUser = new List<UserVM>();
            var cabinetId = CommonFunctions.GetCabinetId();
            sl.Add("@CabinetId", cabinetId);
            dt = objDAL.GetDataTable("uspGetDeletedUserList", sl);
            lstUser = objDAL.ConvertToList<UserVM>(dt);
            return View(lstUser);
        }
        #endregion

        #region --> Set Work Flow Permission | Add | Dhrumil Patel | 19022020
        [HttpGet, Route("UserWorkFlowPermission")]
        public ActionResult UserWorkFlowPermission(long Id)
        {
            WorkFlowPermissionDetail detail = new WorkFlowPermissionDetail();
            detail.UserId = Id;
            var cabId = CommonFunctions.GetCabinetId();
            var cabinetDetail = db.CabinetMasters.Find(cabId);
            detail.CabinetName = cabinetDetail != null ? cabinetDetail.Name : "";
            var userDetail = db.LoginCredentials.Find(Id);
            detail.FullName = userDetail != null ? userDetail.FirstName + " " + userDetail.LastName : "";

            try
            {
                if (Id > 0)
                {
                    using (db = new DBEntities())
                    {
                        var permissionList = db.UserWorkFlowPermissions.Where(a => a.UserId == Id && a.CabinetId == cabId).ToList();
                        if (permissionList.Count > 0)
                        {
                            var Users = permissionList.Where(x => x.WFShareUserId != 0).Select(x => x.WFShareUserId.ToString()).ToList();
                            if (Users.Count > 0)
                            {
                                detail.SharedUsers = Users.ToArray();
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return View(detail);
        }

        [HttpPost, Route("UserWorkFlowPermission")]
        public ActionResult UserWorkFlowPermission(WorkFlowPermissionDetail detail)
        {
            var users = "";
            if (detail.SharedUsers != null)
            {
                foreach (var item in detail.SharedUsers)
                {
                    users += item + ",";
                }
            }

            SortedList param = new SortedList();
            param.Add("@UserId", detail.UserId);
            param.Add("@CabinetId", CommonFunctions.GetCabinetId());
            param.Add("@SharedUserList", users.Substring(0, users.Length - 1));

            int iResult = objDAL.ExecuteSQL("uspSaveWorkFlowUserPermission", param);
            if (iResult >= 0)
            {
                TempData["SuccessMSG"] = Messages.WF_Permission_Shared_Successfully;
            }
            else
            {
                TempData["ErrorMSG"] = Messages.WF_Permission_Not_Shared_Successfully;
            }
            return RedirectToAction("UserList");
        }
        #endregion
    }
}