﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utility.Models;
using DocWare.Models;
using DocWare.Helpers;
using System.Data.Entity;
using DocWare.Resources;
using Utility.Helpers;

namespace DocWare.Controllers
{
    [CheckAuthorization]
    public class GroupDetailController : BaseController
    {
        DBEntities db = new DBEntities();

        #region --> Group Management | Add | Jasmin Vohra | 20022018
        [HttpGet, Route("GroupList")]
        public ActionResult GroupList()
        {
            List<GroupDetailVM> groupDetailVMs = new List<GroupDetailVM>();
            try
            {
                #region --> Page Permission on Action and controller name | Jasmin | 21032018

                var permissionlst = Session[SessionClass.LinkPermissionList].ToString();
                List<UserPermissionVM> userPermissionLst = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserPermissionVM>>(permissionlst);

                PermissionVM permissionVM = new PermissionVM();

                permissionVM.HasAddPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "AddGroup" && x.ControllerName.Trim() == "GroupDetail").Count() > 0;
                permissionVM.HasEditPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "EditGroup" && x.ControllerName.Trim() == "GroupDetail").Count() > 0;
                permissionVM.HasStatusPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "ChangeGroupStatus" && x.ControllerName.Trim() == "GroupDetail").Count() > 0;
                permissionVM.HasActivePermission = userPermissionLst.Where(x => x.ActionName.Trim() == "ActiveSelectedGroup" && x.ControllerName.Trim() == "GroupDetail").Count() > 0;
                permissionVM.HasDeactivePermission = userPermissionLst.Where(x => x.ActionName.Trim() == "DeactiveSelectedGroup" && x.ControllerName.Trim() == "GroupDetail").Count() > 0;
                permissionVM.HasDeletePermission = userPermissionLst.Where(x => x.ActionName.Trim() == "DeleteSelectedGroup" && x.ControllerName.Trim() == "GroupDetail").Count() > 0;

                Session["PermissionListSession"] = permissionVM;
                #endregion
                using (db = new DBEntities())
                {
                    long UserId = CommonFunctions.GetUserId();
                    if (UserId > 0)
                    {
                        var userid = UserId.ToString();
                        var cabinetId = CommonFunctions.GetCabinetId();
                        groupDetailVMs = db.GroupDetails.Where(x => !x.IsDelete && x.CabinetId == cabinetId).Select(j => new GroupDetailVM
                        {
                            Id = j.Id,
                            Name = j.Name,
                            IsDepartment = db.GroupDepartmentDetails.Where(a => a.GroupId == j.Id).ToList().Count > 0 ? true : false,
                            IsActive = j.IsActive,
                            IsDelete = j.IsDelete
                        }).OrderByDescending(x => x.Id).ToList();
                    }
                }
            }
            catch
            {
                throw;
            }
            return View(groupDetailVMs);
        }

        [HttpGet, Route("AddGroup")]
        public ActionResult AddGroup()
        {
            GroupDetailVM groupDetailVM = new GroupDetailVM();
            var cabinetDetail = db.CabinetMasters.Find(CommonFunctions.GetCabinetId());
            groupDetailVM.CabinetName = cabinetDetail != null ? cabinetDetail.Name : "";
            return View(groupDetailVM);
        }

        [HttpGet, Route("EditGroup")]
        public ActionResult EditGroup(long id)
        {
            GroupDetailVM groupDetailVM = new GroupDetailVM();
            try
            {
                if (id > 0)
                {
                    using (db = new DBEntities())
                    {
                        var groupDetail = db.GroupDetails.Where(m => m.Id == id).FirstOrDefault();
                        var cabinetDetail = db.CabinetMasters.Find(CommonFunctions.GetCabinetId());
                        groupDetailVM.Id = groupDetail.Id;
                        groupDetailVM.Name = groupDetail.Name;
                        groupDetailVM.IsDepartment = db.GroupDepartmentDetails.Where(a => a.GroupId == groupDetail.Id).ToList().Count > 0 ? true : false;
                        groupDetailVM.CabinetName = cabinetDetail != null ? cabinetDetail.Name : "";

                        if (!groupDetailVM.IsDepartment)
                        {
                            var Users = db.GroupUserDetails.Where(a => a.GroupId == groupDetail.Id).Select(x => x.UserId).ToList();

                            if (Users.Count > 0)
                            {
                                var userList = new List<string>();
                                foreach (var item in Users)
                                {
                                    userList.Add(item.ToString());
                                }
                                groupDetailVM.Users = userList.ToArray();
                            }
                        }
                        else
                        {
                            var Departments = db.GroupDepartmentDetails.Where(a => a.GroupId == groupDetail.Id).Select(x => x.DepartmentId).ToList();

                            if (Departments.Count > 0)
                            {
                                var deptList = new List<string>();
                                foreach (var item in Departments)
                                {
                                    deptList.Add(item.ToString());
                                }
                                groupDetailVM.Departments = deptList.ToArray();
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return View("AddGroup", groupDetailVM);
        }

        [HttpPost, Route("AddGroup")]
        [AuditLog(EnumList.EnumLogType.SaveData, "AddGroup")]
        public ActionResult AddGroup(GroupDetailVM data)
        {
            var notificationdetail = new SaveNotificationVM();
            GroupDetail groupDetail = new GroupDetail();
            long userId = CommonFunctions.GetUserId();
            var cabinetId = CommonFunctions.GetCabinetId();

            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (data.Id > 0)
                    {
                        groupDetail = db.GroupDetails.Where(m => m.Id == data.Id).FirstOrDefault();
                        groupDetail.Name = data.Name.Trim();
                        groupDetail.UpdatedBy = userId.ToString();
                        groupDetail.UpdatedDate = DateTime.Now;
                        db.Entry(groupDetail).State = EntityState.Modified;
                        db.SaveChanges();

                        // Delete existing data
                        var grpDepartment = db.GroupDepartmentDetails.Where(m => m.GroupId == data.Id).ToList();
                        if (grpDepartment.Count > 0)
                        {
                            db.GroupDepartmentDetails.RemoveRange(grpDepartment);
                            db.SaveChanges();
                        }

                        var grpUser = db.GroupUserDetails.Where(m => m.GroupId == data.Id).ToList();
                        if (grpUser.Count > 0)
                        {
                            db.GroupUserDetails.RemoveRange(grpUser);
                            db.SaveChanges();
                        }

                        TempData["SuccessMSG"] = Messages.Group_updated_successfully;
                    }
                    else
                    {
                        groupDetail.Name = data.Name.Trim();
                        groupDetail.CabinetId = cabinetId;
                        groupDetail.IsActive = true;
                        groupDetail.IsDelete = false;
                        groupDetail.CreatedBy = userId.ToString();
                        groupDetail.CreatedDate = DateTime.Now;
                        db.GroupDetails.Add(groupDetail);
                        db.SaveChanges();

                        TempData["SuccessMSG"] = Messages.Group_added_successfully;
                    }

                    //adding data according to type
                    if (data.IsDepartment)
                    {
                        if (data.Departments != null)
                        {
                            foreach (var item in data.Departments)
                            {
                                GroupDepartmentDetail groupDepartmentDetail = new GroupDepartmentDetail();
                                groupDepartmentDetail.GroupId = groupDetail.Id;
                                groupDepartmentDetail.DepartmentId = Convert.ToInt64(item);
                                groupDepartmentDetail.CreatedBy = userId.ToString();
                                groupDepartmentDetail.CreatedDate = DateTime.Now;
                                db.GroupDepartmentDetails.Add(groupDepartmentDetail);
                                db.SaveChanges();

                                var deptId = Convert.ToInt64(item);

                                //var userList = db.LoginCredentials.Where(j => j.DepartmentId == deptId && j.IsActive && !j.IsDelete).ToList();
                                var userIds = db.LoginCredentials.Where(j => j.DepartmentId == deptId && j.IsActive && !j.IsDelete)
                                   .Select(x => x.Id).ToList();
                                var userList = db.UserCabinetDetails.Where(x => x.CabinetId == cabinetId && userIds.Contains(x.UserId)).ToList();
                                if (userList.Count > 0)
                                {
                                    foreach (var user in userList)
                                    {
                                        GroupUserDetail groupUserDetail = new GroupUserDetail();
                                        groupUserDetail.GroupId = groupDetail.Id;
                                        groupUserDetail.UserId = user.Id;
                                        groupUserDetail.CreatedBy = userId.ToString();
                                        groupUserDetail.CreatedDate = DateTime.Now;
                                        db.GroupUserDetails.Add(groupUserDetail);
                                        db.SaveChanges();
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (data.Users != null)
                        {
                            foreach (var item in data.Users)
                            {
                                GroupUserDetail groupUserDetail = new GroupUserDetail();
                                groupUserDetail.GroupId = groupDetail.Id;
                                groupUserDetail.UserId = Convert.ToInt64(item);
                                groupUserDetail.CreatedBy = userId.ToString();
                                groupUserDetail.CreatedDate = DateTime.Now;
                                db.GroupUserDetails.Add(groupUserDetail);
                                db.SaveChanges();
                            }
                        }
                    }
                }
                catch
                {
                    transaction.Rollback();
                    return RedirectToAction("GroupList", "GroupDetail");
                }
                transaction.Commit();
                notificationdetail.UserId = userId;
                notificationdetail.CabinetId = cabinetId;
                notificationdetail.GroupId = groupDetail.Id;
                notificationdetail.GroupName = groupDetail.Name;
                notificationdetail.NotificationType = (int)EnumList.EnumNotificationType.Group;
                CommonFunctions.SaveNotification(notificationdetail);
            }

            return RedirectToAction("GroupList", "GroupDetail");
        }

        public JsonResult CheckGroupName(string GroupName, long GroupID)
        {
            List<GroupDetail> groupDetails = new List<GroupDetail>();
            try
            {
                using (db = new DBEntities())
                {
                    if (GroupID > 0)
                    {
                        groupDetails = db.GroupDetails.Where(j => j.Name.ToLower().Trim() == GroupName.ToLower().Trim() && j.Id != GroupID).ToList();
                    }
                    else
                    {
                        groupDetails = db.GroupDetails.Where(j => j.Name.ToLower().Trim() == GroupName.ToLower().Trim()).ToList();
                    }
                    if (groupDetails.Count() > 0)
                    {
                        return Json(false, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(true, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "ChangeGroupStatus")]
        public JsonResult ChangeGroupStatus(long id)
        {
            GroupDetail groupDetail = new GroupDetail();
            try
            {
                if (id > 0)
                {
                    using (db = new DBEntities())
                    {
                        long userId = CommonFunctions.GetUserId();
                        groupDetail = db.GroupDetails.Where(m => m.Id == id).FirstOrDefault();

                        if (groupDetail.IsActive)
                        {
                            groupDetail.IsActive = false;
                        }
                        else
                        {
                            groupDetail.IsActive = true;
                        }
                        groupDetail.UpdatedBy = userId.ToString();
                        groupDetail.UpdatedDate = DateTime.Now;
                        db.Entry(groupDetail).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "DeleteSelectedGroup")]
        public JsonResult DeleteSelectedGroup(string[] groupIds)
        {
            GroupDetail groupDetail = new GroupDetail();
            try
            {
                if (groupIds != null)
                {
                    if (groupIds.Count() > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();

                            foreach (var item in groupIds)
                            {
                                long id = Convert.ToInt64(item);

                                groupDetail = db.GroupDetails.Where(m => m.Id == id).FirstOrDefault();

                                groupDetail.IsActive = false;
                                groupDetail.IsDelete = true;
                                groupDetail.UpdatedBy = userId.ToString();
                                groupDetail.UpdatedDate = DateTime.Now;
                                db.Entry(groupDetail).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                            return Json(true, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "DeactiveSelectedGroup")]
        public JsonResult DeactiveSelectedGroup(string[] groupIds)
        {
            GroupDetail groupDetail = new GroupDetail();
            try
            {
                if (groupIds != null)
                {
                    if (groupIds.Count() > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();

                            foreach (var item in groupIds)
                            {
                                long id = Convert.ToInt64(item);

                                groupDetail = db.GroupDetails.Where(m => m.Id == id).FirstOrDefault();

                                groupDetail.IsActive = false;
                                groupDetail.UpdatedBy = userId.ToString();
                                groupDetail.UpdatedDate = DateTime.Now;
                                db.Entry(groupDetail).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                            return Json(true, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "ActiveSelectedGroup")]
        public JsonResult ActiveSelectedGroup(string[] groupIds)
        {
            GroupDetail groupDetail = new GroupDetail();
            try
            {
                if (groupIds != null)
                {
                    if (groupIds.Count() > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();

                            foreach (var item in groupIds)
                            {
                                long id = Convert.ToInt64(item);

                                groupDetail = db.GroupDetails.Where(m => m.Id == id).FirstOrDefault();

                                groupDetail.IsActive = true;
                                groupDetail.UpdatedBy = userId.ToString();
                                groupDetail.UpdatedDate = DateTime.Now;
                                db.Entry(groupDetail).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                            return Json(true, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet, Route("DeletedGroupList")]
        public ActionResult DeletedGroupList()
        {
            List<GroupDetailVM> groupDetailVMs = new List<GroupDetailVM>();
            try
            {
                #region --> Page Permission on Action and controller name | Dhrumil Patel | 22032018
                var permissionlst = Session[SessionClass.LinkPermissionList].ToString();
                List<UserPermissionVM> userPermissionLst = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserPermissionVM>>(permissionlst);

                PermissionVM permissionVM = new PermissionVM();
                permissionVM.HasRestorePermisssion = userPermissionLst.Where(x => x.ActionName.Trim() == "RestoreSelectedGroup" && x.ControllerName.Trim() == "GroupDetail").Count() > 0;
                Session["PermissionListSession"] = permissionVM;
                #endregion

                using (db = new DBEntities())
                {
                    long UserId = CommonFunctions.GetUserId();
                    if (UserId > 0)
                    {
                        var userid = UserId.ToString();
                        var cabinetId = CommonFunctions.GetCabinetId();
                        groupDetailVMs = db.GroupDetails.Where(x => x.IsDelete && x.CreatedBy == userid && x.CabinetId == cabinetId).Select(j => new GroupDetailVM
                        {
                            Id = j.Id,
                            Name = j.Name,
                            IsDepartment = db.GroupDepartmentDetails.Where(a => a.GroupId == j.Id).ToList().Count > 0 ? true : false,
                            IsActive = j.IsActive,
                            IsDelete = j.IsDelete
                        }).OrderByDescending(x => x.Id).ToList();
                    }
                }
            }
            catch
            {
                throw;
            }
            return View(groupDetailVMs);
        }
        #endregion

        #region Restore Selected Group | Add | Dhrumil Patel | 22032018
        [HttpPost, Route("RestoreSelectedGroup")]
        [AuditLog(EnumList.EnumLogType.UpdateData, "RestoreSelectedGroup")]
        public JsonResult RestoreSelectedGroup(string[] groupIds)
        {
            GroupDetail groupMaster = new GroupDetail();
            try
            {
                if (groupIds != null)
                {
                    if (groupIds.Length > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();

                            foreach (var item in groupIds)
                            {
                                long id = Convert.ToInt64(item);

                                groupMaster = db.GroupDetails.Find(id);
                                groupMaster.IsActive = true;
                                groupMaster.IsDelete = false;
                                groupMaster.UpdatedBy = userId.ToString();
                                groupMaster.UpdatedDate = DateTime.Now;
                                db.Entry(groupMaster).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}