﻿using DocWare.Helpers;
using DocWare.Models;
using DocWare.Resources;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utility.Helpers;
using Utility.Models;

namespace DocWare.Controllers
{
    [CheckAuthorization]
    public class RoleManagementController : BaseController
    {
        DBEntities db = new DBEntities();
        DAL objDAL = new DAL();

        #region --> Role Master | Add | Jasmin Vohra | 06032018
        [HttpGet, Route("RoleList")]
        public ActionResult RoleList()
        {
            List<RoleMaster> roleMasters = new List<RoleMaster>();
            try
            {
                #region --> Page Permission on Action and controller name | Jasmin | 21032018

                var permissionlst = Session[SessionClass.LinkPermissionList].ToString();
                List<UserPermissionVM> userPermissionLst = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserPermissionVM>>(permissionlst);

                PermissionVM permissionVM = new PermissionVM();

                permissionVM.HasAddPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "AddRole" && x.ControllerName.Trim() == "RoleManagement").Count() > 0;
                permissionVM.HasEditPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "EditRole" && x.ControllerName.Trim() == "RoleManagement").Count() > 0;
                permissionVM.HasStatusPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "ChangeRoleStatus" && x.ControllerName.Trim() == "RoleManagement").Count() > 0;
                permissionVM.HasActivePermission = userPermissionLst.Where(x => x.ActionName.Trim() == "ActiveSelectedRole" && x.ControllerName.Trim() == "RoleManagement").Count() > 0;
                permissionVM.HasDeactivePermission = userPermissionLst.Where(x => x.ActionName.Trim() == "DeactiveSelectedRole" && x.ControllerName.Trim() == "RoleManagement").Count() > 0;
                permissionVM.HasDeletePermission = userPermissionLst.Where(x => x.ActionName.Trim() == "DeleteSelectedRole" && x.ControllerName.Trim() == "RoleManagement").Count() > 0;
                permissionVM.HasConfigurationPermisssion = userPermissionLst.Where(x => x.ActionName.Trim() == "RoleRights" && x.ControllerName.Trim() == "RoleManagement").Count() > 0;

                Session["PermissionListSession"] = permissionVM;
                #endregion

                using (db = new DBEntities())
                {
                    roleMasters = db.RoleMasters.Where(j => !j.IsDelete).ToList();
                }
            }
            catch
            {
                throw;
            }
            return View(roleMasters);
        }

        [HttpGet, Route("AddRole")]
        public ActionResult AddRole()
        {
            RoleMaster roleMaster = new RoleMaster();
            return View(roleMaster);
        }

        [HttpGet, Route("EditRole")]
        public ActionResult EditRole(long id)
        {
            RoleMaster roleMaster = new RoleMaster();
            try
            {
                if (id > 0)
                {
                    using (db = new DBEntities())
                    {
                        roleMaster = db.RoleMasters.Where(m => m.Id == id).FirstOrDefault();
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return View("AddRole", roleMaster);
        }

        [HttpPost, Route("AddRole")]
        [AuditLog(EnumList.EnumLogType.SaveData, "AddRole")]
        public ActionResult AddRole(RoleMaster data)
        {
            RoleMaster roleMaster = new RoleMaster();
            long UID = CommonFunctions.GetUserId();
            try
            {

                using (db = new DBEntities())
                {
                    long userId = CommonFunctions.GetUserId();

                    if (data.Id > 0)
                    {
                        roleMaster = db.RoleMasters.Where(m => m.Id == data.Id).FirstOrDefault();
                        roleMaster.Name = data.Name.Trim();
                        roleMaster.UpdatedBy = userId.ToString();
                        roleMaster.UpdatedDate = DateTime.Now;
                        db.Entry(roleMaster).State = EntityState.Modified;
                        db.SaveChanges();

                        TempData["SuccessMSG"] = Messages.Role_updated_successfully;
                    }
                    else
                    {
                        roleMaster.Name = data.Name.Trim();
                        roleMaster.IsActive = true;
                        roleMaster.CreatedBy = userId.ToString();
                        roleMaster.CreatedDate = DateTime.Now;
                        db.RoleMasters.Add(roleMaster);
                        db.SaveChanges();

                        TempData["SuccessMSG"] = Messages.Role_added_successfully;
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return RedirectToAction("RoleList", "RoleManagement");
        }

        public JsonResult CheckRole(string RoleName, long RoleID)
        {
            List<RoleMaster> role = new List<RoleMaster>();
            try
            {
                using (db = new DBEntities())
                {
                    if (RoleID > 0)
                    {
                        role = db.RoleMasters.Where(j => j.Name.ToLower().Trim() == RoleName.ToLower().Trim() && j.Id != RoleID).ToList();
                    }
                    else
                    {
                        role = db.RoleMasters.Where(j => j.Name.ToLower().Trim() == RoleName.ToLower().Trim()).ToList();
                    }

                    if (role.Count() > 0)
                    {
                        return Json(false, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(true, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "ChangeRoleStatus")]
        public JsonResult ChangeRoleStatus(long id)
        {
            try
            {
                if (id > 0)
                {
                    using (db = new DBEntities())
                    {
                        RoleMaster roleMaster = new RoleMaster();
                        long userId = CommonFunctions.GetUserId();

                        roleMaster = db.RoleMasters.Where(m => m.Id == id).FirstOrDefault();

                        if (roleMaster.IsActive)
                        {
                            if (CommonFunctions.IsUserAssigned(id.ToString(), (int)EnumList.EnumMasterType.Role))
                            {
                                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                roleMaster.IsActive = false;
                            }
                        }
                        else
                        {
                            roleMaster.IsActive = true;
                        }
                        roleMaster.UpdatedBy = userId.ToString();
                        roleMaster.UpdatedDate = DateTime.Now;
                        db.Entry(roleMaster).State = EntityState.Modified;
                        db.SaveChanges();

                        return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "DeleteSelectedRole")]
        public JsonResult DeleteSelectedRole(string[] roleIds)
        {
            bool isAssigned = true;
            RoleMaster roleMaster = new RoleMaster();
            try
            {
                if (roleIds != null)
                {
                    if (roleIds.Length > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();

                            isAssigned = CommonFunctions.IsUserAssigned(string.Join(",", roleIds), (int)EnumList.EnumMasterType.Role);

                            if (!isAssigned)
                            {
                                foreach (var item in roleIds)
                                {
                                    long id = Convert.ToInt64(item);

                                    roleMaster = db.RoleMasters.Where(m => m.Id == id).FirstOrDefault();

                                    roleMaster.IsActive = false;
                                    roleMaster.IsDelete = true;
                                    roleMaster.UpdatedBy = userId.ToString();
                                    roleMaster.UpdatedDate = DateTime.Now;
                                    db.Entry(roleMaster).State = EntityState.Modified;
                                    db.SaveChanges();

                                }
                                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "DeactiveSelectedRole")]
        public JsonResult DeactiveSelectedRole(string[] roleIds)
        {
            bool isAssigned = true;
            RoleMaster roleMaster = new RoleMaster();
            try
            {
                if (roleIds != null)
                {
                    if (roleIds.Length > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();

                            isAssigned = CommonFunctions.IsUserAssigned(string.Join(",", roleIds), (int)EnumList.EnumMasterType.Role);

                            if (!isAssigned)
                            {
                                foreach (var item in roleIds)
                                {
                                    long id = Convert.ToInt64(item);

                                    roleMaster = db.RoleMasters.Where(m => m.Id == id).FirstOrDefault();

                                    roleMaster.IsActive = false;
                                    roleMaster.UpdatedBy = userId.ToString();
                                    roleMaster.UpdatedDate = DateTime.Now;
                                    db.Entry(roleMaster).State = EntityState.Modified;
                                    db.SaveChanges();

                                }
                                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "ActiveSelectedRole")]
        public JsonResult ActiveSelectedRole(string[] roleIds)
        {
            RoleMaster roleMaster = new RoleMaster();
            try
            {
                if (roleIds != null)
                {
                    if (roleIds.Length > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();

                            foreach (var item in roleIds)
                            {
                                long id = Convert.ToInt64(item);

                                roleMaster = db.RoleMasters.Where(m => m.Id == id).FirstOrDefault();

                                roleMaster.IsActive = true;
                                roleMaster.UpdatedBy = userId.ToString();
                                roleMaster.UpdatedDate = DateTime.Now;
                                db.Entry(roleMaster).State = EntityState.Modified;
                                db.SaveChanges();

                            }
                            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet, Route("DeletedRoleList")]
        public ActionResult DeletedRoleList()
        {
            List<RoleMaster> roleMasters = new List<RoleMaster>();
            try
            {
                #region --> Page Permission on Action and controller name | Jasmin | 22032018

                var permissionlst = Session[SessionClass.LinkPermissionList].ToString();
                List<UserPermissionVM> userPermissionLst = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserPermissionVM>>(permissionlst);

                PermissionVM permissionVM = new PermissionVM();

                permissionVM.HasRestorePermisssion = userPermissionLst.Where(x => x.ActionName.Trim() == "RestoreSelectedRole" && x.ControllerName.Trim() == "RoleManagement").Count() > 0;

                Session["PermissionListSession"] = permissionVM;
                #endregion

                using (db = new DBEntities())
                {
                    roleMasters = db.RoleMasters.Where(j => j.IsDelete).ToList();
                }
            }
            catch
            {
                throw;
            }
            return View(roleMasters);
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "RestoreSelectedRole")]
        public JsonResult RestoreSelectedRole(string[] roleIds)
        {
            RoleMaster roleMaster = new RoleMaster();
            try
            {
                if (roleIds != null)
                {
                    if (roleIds.Length > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();

                            foreach (var item in roleIds)
                            {
                                long id = Convert.ToInt64(item);

                                roleMaster = db.RoleMasters.Where(m => m.Id == id).FirstOrDefault();

                                roleMaster.IsActive = true;
                                roleMaster.IsDelete = false;
                                roleMaster.UpdatedBy = userId.ToString();
                                roleMaster.UpdatedDate = DateTime.Now;
                                db.Entry(roleMaster).State = EntityState.Modified;
                                db.SaveChanges();

                            }
                            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region --> Rights Management | Jasmin Vohra | 06032018

        #region --> Role Rights Management | Jasmin Vohra | 06032018
        [HttpGet, Route("RoleRightsManagement")]
        public ActionResult RoleRightsManagement()
        {
            List<RoleRightVM> roleRightVMs = new List<RoleRightVM>();
            try
            {
                using (db = new DBEntities())
                {
                    roleRightVMs = db.RoleMasters.Where(j => j.IsActive).Select(j => new RoleRightVM()
                    {
                        RoleId = j.Id,
                        RoleName = j.Name
                    }).ToList();
                }
            }
            catch
            {
                throw;
            }
            return View(roleRightVMs);
        }

        [HttpGet, Route("RoleRights")]
        public ActionResult RoleRights(long Id)
        {
            List<MenuVM> menuVMs = new List<MenuVM>();
            try
            {
                menuVMs = RoleManagement.GetLinksForSelectedRole(Id).ToList();
                ViewBag.RoleID = Id;
                ViewBag.RoleName = RoleManagement.GetRoleNameById(Id);
            }
            catch (Exception e)
            {
                throw;
            }
            return View(menuVMs);
        }

        [HttpPost, Route("RoleRights"), ValidateInput(false)]
        [AuditLog(EnumList.EnumLogType.SaveData, "RoleRights")]
        public ActionResult RoleRights(FormCollection frmCollection)
        {
            long userRoleID = Convert.ToInt64(frmCollection["hdnUserRoleID"]);
            long cabinetId = CommonFunctions.GetCabinetId();
            try
            {
                using (db = new DBEntities())
                {
                    var rights = from r in db.RoleRights
                                 where r.RoleId == userRoleID && r.CabinetId == cabinetId
                                 select r;

                    db.RoleRights.RemoveRange(rights);
                    db.SaveChanges();

                    Session.Remove("Menu");

                    for (int i = 1; i < frmCollection.Count; i++)
                    {
                        string keyName = frmCollection.GetKey(i);
                        string[] keyValues = keyName.Split('_');

                        long linkID = Convert.ToInt64(keyValues[1]);

                        RoleRight roleRight = new RoleRight();
                        roleRight.CabinetId = cabinetId;
                        roleRight.RoleId = userRoleID;
                        roleRight.LinkId = linkID;
                        db.RoleRights.Add(roleRight);
                        db.SaveChanges();
                    }

                    string menu = RoleManagement.GetMenu();

                    Session[SessionClass.Menu] = menu;
                    Session[SessionClass.LinkPermissionList] = string.Empty;
                    Session[SessionClass.LinkPermissionList] = RoleManagement.GetAllLinkPermission();

                    TempData["SuccessMSG"] = "Role rights updated successfully";
                }
            }
            catch (Exception e)
            {
                throw;
            }
            return RedirectToAction("RoleList", "RoleManagement");
        }
        #endregion

        #region --> User Rights Management | Jasmin Vohra | 06032018
        [HttpGet, Route("UserRightsManagement")]
        public ActionResult UserRightsManagement()
        {
            List<UserRightVM> userRightVMs = new List<UserRightVM>();
            SortedList sl = new SortedList();
            DataTable dt = new DataTable();
            long cabinetId = 0;
            try
            {
                using (db = new DBEntities())
                {
                    cabinetId = CommonFunctions.GetCabinetId();

                    sl.Add("@cabinetId", cabinetId);
                    dt = objDAL.GetDataTable("uspGetUserListByCabinet", sl);
                    userRightVMs = objDAL.ConvertToList<UserRightVM>(dt);
                }
            }
            catch
            {
                throw;
            }
            return View(userRightVMs);
        }

        [HttpGet, Route("UserRights")]
        public ActionResult UserRights(long Id)
        {
            List<MenuVM> menuVMs = new List<MenuVM>();
            try
            {
                menuVMs = RoleManagement.GetLinksForSelectedUser(Id).ToList();
                ViewBag.UserID = Id;
                ViewBag.FullName = CommonFunctions.GetFullName(Id);
            }
            catch (Exception e)
            {
                throw;
            }
            return View(menuVMs);
        }

        [HttpPost, Route("UserRights"), ValidateInput(false)]
        [AuditLog(EnumList.EnumLogType.SaveData, "UserRights")]
        public ActionResult UserRights(FormCollection frmCollection)
        {
            long userID = Convert.ToInt64(frmCollection["hdnUserID"]);
            long cabinetId = CommonFunctions.GetCabinetId();
            try
            {
                using (db = new DBEntities())
                {
                    var rights = from r in db.UserRights
                                 where r.UserId == userID && r.CabinetId == cabinetId
                                 select r;

                    db.UserRights.RemoveRange(rights);
                    db.SaveChanges();

                    Session.Remove("Menu");

                    for (int i = 1; i < frmCollection.Count; i++)
                    {
                        string keyName = frmCollection.GetKey(i);
                        string[] keyValues = keyName.Split('_');

                        long linkID = Convert.ToInt64(keyValues[1]);

                        UserRight userRight = new UserRight();
                        userRight.CabinetId = cabinetId;
                        userRight.UserId = userID;
                        userRight.LinkId = linkID;
                        db.UserRights.Add(userRight);
                        db.SaveChanges();
                    }

                    string menu = RoleManagement.GetMenu();

                    Session[SessionClass.Menu] = menu;
                    Session[SessionClass.LinkPermissionList] = string.Empty;
                    Session[SessionClass.LinkPermissionList] = RoleManagement.GetAllLinkPermission();

                    TempData["SuccessMSG"] = "User rights updated successfully";
                }
            }
            catch (Exception e)
            {
                throw;
            }
            return RedirectToAction("UserList", "UserManagement");
        }
        #endregion

        #endregion
    }
}