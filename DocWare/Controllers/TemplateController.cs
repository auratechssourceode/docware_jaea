﻿using DocWare.Helpers;
using DocWare.Models;
using DocWare.Resources;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utility.Helpers;
using Utility.Models;

namespace DocWare.Controllers
{
    [CheckAuthorization]
    public class TemplateController : BaseController
    {
        DBEntities db = new DBEntities();

        #region --> Email Template List | Add | Jasmin | 08022018
        [HttpGet, Route("EmailTemplateList")]
        public ActionResult EmailTemplateList()
        {
            List<EmailTemplate> emailTemplates = new List<EmailTemplate>();
            try
            {
                #region --> Page Permission on Action and controller name | Jasmin | 21032018

                var permissionlst = Session[SessionClass.LinkPermissionList].ToString();
                List<UserPermissionVM> userPermissionLst = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserPermissionVM>>(permissionlst);

                PermissionVM permissionVM = new PermissionVM();

                permissionVM.HasAddPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "AddEmailTemplate" && x.ControllerName.Trim() == "Template").Count() > 0;
                permissionVM.HasEditPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "EditEmailTemplate" && x.ControllerName.Trim() == "Template").Count() > 0;
                permissionVM.HasStatusPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "changeEmailTemplateStatus" && x.ControllerName.Trim() == "Template").Count() > 0;
                permissionVM.HasActivePermission = userPermissionLst.Where(x => x.ActionName.Trim() == "ActiveSelectedEmailTemplate" && x.ControllerName.Trim() == "Template").Count() > 0;
                permissionVM.HasDeactivePermission = userPermissionLst.Where(x => x.ActionName.Trim() == "DeactiveSelectedEmailTemplate" && x.ControllerName.Trim() == "Template").Count() > 0;
                permissionVM.HasDeletePermission = userPermissionLst.Where(x => x.ActionName.Trim() == "DeleteSelectedEmailTemplate" && x.ControllerName.Trim() == "Template").Count() > 0;

                Session["PermissionListSession"] = permissionVM;
                #endregion

                using (db = new DBEntities())
                {
                    emailTemplates = db.EmailTemplates.Where(x => !x.IsDelete && x.IsEdit).OrderByDescending(j => j.Id).ToList();
                }
            }
            catch
            {
                throw;
            }
            return View(emailTemplates);
        }
        #endregion

        #region --> Add/Update Email Template | Add | Jasmin | 08022018
        [HttpGet, Route("AddEmailTemplate")]
        public ActionResult AddEmailTemplate()
        {
            EmailTemplateVM emailTemplateVM = new EmailTemplateVM();
            EmailTemplateTagVM emailTemplateTagVM = new EmailTemplateTagVM();
            try
            {
                using (db = new DBEntities())
                {
                    emailTemplateVM.ListTag = GetEmailTagList();

                    emailTemplateVM.IsEdit = true;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return View(emailTemplateVM);
        }

        [HttpGet, Route("EditEmailTemplate")]
        public ActionResult EditEmailTemplate(long id)
        {
            EmailTemplateVM emailTemplateVM = new EmailTemplateVM();
            try
            {
                using (db = new DBEntities())
                {
                    emailTemplateVM.ListTag = GetEmailTagList();

                    if (id > 0)
                    {
                        var emailTemplate = db.EmailTemplates.Where(m => m.Id == id).FirstOrDefault();

                        emailTemplateVM.Id = emailTemplate.Id;
                        emailTemplateVM.Name = emailTemplate.Name;
                        emailTemplateVM.Description = emailTemplate.Description;
                        emailTemplateVM.Subject = emailTemplate.Subject;
                        emailTemplateVM.Body = emailTemplate.Body;
                        emailTemplateVM.IsEdit = emailTemplate.IsEdit;
                    }
                    else
                    {
                        emailTemplateVM.IsEdit = true;
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return View("AddEmailTemplate", emailTemplateVM);
        }

        public List<EmailTemplateTagVM> GetEmailTagList()
        {
            List<EmailTemplateTagVM> emailTemplateTagVMs = new List<EmailTemplateTagVM>();
            EmailTemplateTagVM emailTemplateTagVM = new EmailTemplateTagVM();
            try
            {
                var emailTemplateTags = (from tags in db.EmailTemplateTags
                                         join tagtype in db.EmailTemplateTagTypes on tags.TagType equals tagtype.TagTypeId
                                         where (tags.IsActive == true)
                                         orderby tagtype.ViewIndex
                                         select new
                                         {
                                             tags.EmailTemplateTagID,
                                             tags.DisplayIndex,
                                             tags.Description,
                                             tags.EmailTemplateTag1,
                                             tagtype.TypeName
                                         }).ToList();

                if (emailTemplateTags.Count > 0)
                {
                    emailTemplateTags.ForEach(mn =>
                    {
                        emailTemplateTagVM = new EmailTemplateTagVM
                        {
                            TagID = mn.EmailTemplateTagID,
                            DisplayIndex = mn.DisplayIndex,
                            Description = mn.Description,
                            EmailTemplateTag = mn.EmailTemplateTag1,
                            TypeName = mn.TypeName
                        };

                        emailTemplateTagVMs.Add(emailTemplateTagVM);
                    });
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return emailTemplateTagVMs;
        }

        [HttpPost, Route("AddEmailTemplate"), ValidateInput(false)]
        [AuditLog(EnumList.EnumLogType.SaveData, "AddEmailTemplate")]
        public ActionResult AddEmailTemplate(EmailTemplateVM data)
        {
            EmailTemplate emailTemplate = new EmailTemplate();
            try
            {
                long userId = CommonFunctions.GetUserId();

                using (db = new DBEntities())
                {
                    if (data.Id > 0)
                    {
                        emailTemplate = db.EmailTemplates.Where(m => m.Id == data.Id).FirstOrDefault();
                        emailTemplate.Name = data.Name.Trim();
                        emailTemplate.Description = data.Description;
                        emailTemplate.Subject = data.Subject.Trim();
                        emailTemplate.Body = data.Body;
                        emailTemplate.UpdatedBy = userId.ToString();
                        emailTemplate.UpdatedDate = DateTime.Now;
                        db.Entry(emailTemplate).State = EntityState.Modified;
                        db.SaveChanges();

                        TempData["SuccessMSG"] = Messages.EmailTemplate_updated_successfully;
                    }
                    else
                    {
                        emailTemplate.Name = data.Name.Trim();
                        emailTemplate.Description = data.Description;
                        emailTemplate.Subject = data.Subject.Trim();
                        emailTemplate.Body = data.Body;
                        emailTemplate.IsEdit = true;
                        emailTemplate.IsActive = true;
                        emailTemplate.IsDelete = false;
                        emailTemplate.CreatedBy = userId.ToString();
                        emailTemplate.CreatedDate = DateTime.Now;
                        db.EmailTemplates.Add(emailTemplate);
                        db.SaveChanges();

                        TempData["SuccessMSG"] = Messages.EmailTemplate_added_successfully;
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return RedirectToAction("EmailTemplateList", "Template");
        }

        public JsonResult CheckEmailTemplateName(string TemplateName, long TemplateID)
        {
            List<EmailTemplate> emailTemplates = new List<EmailTemplate>();
            try
            {
                using (db = new DBEntities())
                {
                    if (TemplateID > 0)
                    {
                        emailTemplates = db.EmailTemplates.Where(j => j.Name.ToLower().Trim() == TemplateName.ToLower().Trim() && j.Id != TemplateID).ToList();
                    }
                    else
                    {
                        emailTemplates = db.EmailTemplates.Where(j => j.Name.ToLower().Trim() == TemplateName.ToLower().Trim()).ToList();
                    }

                    if (emailTemplates.Count() > 0)
                    {
                        return Json(false, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(true, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, Route("changeEmailTemplateStatus")]
        public JsonResult changeEmailTemplateStatus(long id)
        {
            EmailTemplate emailTemplate = new EmailTemplate();
            try
            {
                using (db = new DBEntities())
                {
                    long userId = CommonFunctions.GetUserId();
                    emailTemplate = db.EmailTemplates.Where(m => m.Id == id).FirstOrDefault();

                    if (emailTemplate.IsActive)
                    {
                        emailTemplate.IsActive = false;
                    }
                    else
                    {
                        emailTemplate.IsActive = true;
                    }
                    emailTemplate.UpdatedBy = userId.ToString();
                    emailTemplate.UpdatedDate = DateTime.Now;
                    db.Entry(emailTemplate).State = EntityState.Modified;
                    db.SaveChanges();

                    return Json(true, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region --> Delete selected email template | Add | Jaydeep | 15032018
        [HttpPost, Route("DeleteSelectedEmailTemplate")]
        [AuditLog(EnumList.EnumLogType.UpdateData, "DeleteSelectedEmailTemplate")]
        public ActionResult DeleteSelectedEmailTemplate(long[] ids)
        {
            if (ids != null)
            {
                if (ids.Length > 0)
                {
                    foreach (long Id in ids)
                    {
                        var GetTemplate = db.EmailTemplates.FirstOrDefault(x => x.Id == Id);
                        if (GetTemplate != null)
                        {
                            long userId = CommonFunctions.GetUserId();
                            GetTemplate.IsDelete = true;
                            GetTemplate.IsActive = false;
                            GetTemplate.UpdatedBy = userId.ToString();
                            GetTemplate.UpdatedDate = DateTime.Now;
                            db.SaveChanges();
                        }
                    }
                }
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region --> Deactive selected email template | Add | Dhrumil Patel | 19032018
        [HttpPost, Route("DeactiveSelectedEmailTemplate")]
        [AuditLog(EnumList.EnumLogType.UpdateData, "DeactiveSelectedEmailTemplate")]
        public ActionResult DeactiveSelectedEmailTemplate(long[] ids)
        {
            if (ids != null)
            {
                if (ids.Length > 0)
                {
                    foreach (long Id in ids)
                    {
                        var GetTemplate = db.EmailTemplates.FirstOrDefault(x => x.Id == Id);
                        if (GetTemplate != null)
                        {
                            long userId = CommonFunctions.GetUserId();
                            GetTemplate.IsActive = false;
                            GetTemplate.UpdatedBy = userId.ToString();
                            GetTemplate.UpdatedDate = DateTime.Now;
                            db.SaveChanges();
                        }
                    }
                }
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region --> Active selected email template | Add | Jasmin | 21032018
        [HttpPost, Route("ActiveSelectedEmailTemplate")]
        [AuditLog(EnumList.EnumLogType.UpdateData, "ActiveSelectedEmailTemplate")]
        public ActionResult ActiveSelectedEmailTemplate(long[] ids)
        {
            if (ids != null)
            {
                if (ids.Length > 0)
                {
                    foreach (long Id in ids)
                    {
                        var GetTemplate = db.EmailTemplates.FirstOrDefault(x => x.Id == Id);
                        if (GetTemplate != null)
                        {
                            long userId = CommonFunctions.GetUserId();
                            GetTemplate.IsActive = true;
                            GetTemplate.UpdatedBy = userId.ToString();
                            GetTemplate.UpdatedDate = DateTime.Now;
                            db.SaveChanges();
                        }
                    }
                }
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region --> Deleted Email Template List | Add | Dhrumil Patel | 16032018
        [HttpGet, Route("DeletedEmailTemplateList")]
        public ActionResult DeletedEmailTemplateList()
        {

            List<EmailTemplate> emailTemplates = new List<EmailTemplate>();
            try
            {
                #region --> Page Permission on Action and controller name | Dhrumil Patel | 22032018

                var permissionlst = Session[SessionClass.LinkPermissionList].ToString();
                List<UserPermissionVM> userPermissionLst = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserPermissionVM>>(permissionlst);

                PermissionVM permissionVM = new PermissionVM();
                permissionVM.HasRestorePermisssion = userPermissionLst.Where(x => x.ActionName.Trim() == "RestoreSelectedEmail" && x.ControllerName.Trim() == "Template").Count() > 0;

                Session["PermissionListSession"] = permissionVM;
                #endregion
                using (db = new DBEntities())
                {
                    emailTemplates = db.EmailTemplates.Where(x => x.IsDelete).OrderByDescending(j => j.Id).ToList();
                }
            }
            catch
            {
                throw;
            }
            return View(emailTemplates);
        }
        #endregion

        #region Restore Selected Email | Add | Dhrumil Patel | 22032018
        [HttpPost, Route("RestoreSelectedEmail")]
        [AuditLog(EnumList.EnumLogType.UpdateData, "RestoreSelectedEmail")]
        public JsonResult RestoreSelectedEmail(string[] emailIds)
        {
            EmailTemplate emailMaster = new EmailTemplate();
            try
            {
                if (emailIds != null)
                {
                    if (emailIds.Length > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();

                            foreach (var item in emailIds)
                            {
                                long id = Convert.ToInt64(item);

                                emailMaster = db.EmailTemplates.Find(id);
                                emailMaster.IsActive = true;
                                emailMaster.IsDelete = false;
                                emailMaster.UpdatedBy = userId.ToString();
                                emailMaster.UpdatedDate = DateTime.Now;
                                db.Entry(emailMaster).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}