﻿using DocWare.Helpers;
using DocWare.Models;
using Ionic.Zip;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utility.Helpers;
using Utility.Models;

namespace DocWare.Controllers
{
    public class NotificationController : BaseController
    {
        DBEntities db = new DBEntities();
        DAL objDAL = new DAL();
        // GET: Notification
        public ActionResult Index()
        {
            return View();
        }

        #region --> Get Notification List | Krupali | 09082018
        [HttpGet, Route("NotificationList")]
        public ActionResult NotificationList()
        {
            List<NotificationListVM> datalst = new List<NotificationListVM>();
            NotificationVM data = new NotificationVM();

            try
            {
                long CabinetId = CommonFunctions.GetCabinetId();
                long UserId = CommonFunctions.GetUserId();
                SortedList sl = new SortedList();
                DataTable dt1 = new DataTable();
                DataTable dt2 = new DataTable();
                sl.Add("@CabinetId", CabinetId);
                sl.Add("@UserId", UserId);
                DataSet ds = objDAL.GetDataSet("uspGetNotificationList", sl);
                if (ds != null)
                {
                    dt1 = ds.Tables[0];
                    dt2 = ds.Tables[1];
                }

                if(dt1 != null && dt2 != null)
                {
                    datalst = objDAL.ConvertToList<NotificationListVM>(dt2);
                    datalst = datalst.OrderByDescending(x => x.NotificationId).ToList();

                    data = objDAL.ConvertToList<NotificationVM>(dt1).FirstOrDefault();
                    data.NotificationLists = datalst;

                }
              
            }
            catch (Exception)
            {
                throw;
            }
            return View(data ?? new NotificationVM());
        }
        #endregion

        #region --> Notification Make as Unread | Krupali | 09082018
        public JsonResult MakeAsReadUnread(string[] NotificationId, string status)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if(NotificationId != null && NotificationId.Length > 0)
                    {
                        using (db = new DBEntities())
                        {
                            foreach(var item in NotificationId)
                            {
                                if (status == "true")
                                {
                                    long id = Convert.ToInt64(item);
                                    var notification = db.Notifications.Find(id);
                                    notification.IsRead = true;
                                    db.SaveChanges();
                                }
                                else
                                {
                                    long id = Convert.ToInt64(item);
                                    var notification = db.Notifications.Find(id);
                                    notification.IsRead = false;
                                    db.SaveChanges();
                                }
                            }
                        }
                    }
                    transaction.Commit();
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        #endregion

        #region --> Remove Notification | Krupali | 09082018
        public JsonResult RemoveNotification(string[] NotificationId)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (NotificationId != null && NotificationId.Length > 0)
                    {
                        using (db = new DBEntities())
                        {
                            foreach (var item in NotificationId)
                            {
                                    long id = Convert.ToInt64(item);
                                    var notification = db.Notifications.Find(id);
                                    notification.IsDelete = true;
                                    db.SaveChanges();
                            }
                        }
                    }
                    transaction.Commit();
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        #endregion

        #region--> _NotificationList | Add |  Sweta Patel | 20180809
        [HttpPost]
        public ActionResult _GetNotificationList()
        {
            List<NotificationListVM> datalst = new List<NotificationListVM>();
            NotificationVM data = new NotificationVM();

            try
            {
                long CabinetId = CommonFunctions.GetCabinetId();
                long UserId = CommonFunctions.GetUserId();
                SortedList sl = new SortedList();
                DataTable dt2 = new DataTable();
                sl.Add("@CabinetId", CabinetId);
                sl.Add("@UserId", UserId);
                sl.Add("@CountData", 3);
                DataSet ds = objDAL.GetDataSet("uspGetNotificationList", sl);
                if (ds != null)
                {                   
                    dt2 = ds.Tables[1];
                }

                if (dt2 != null)
                {
                    datalst = objDAL.ConvertToList<NotificationListVM>(dt2);
                    datalst = datalst.OrderByDescending(x => x.NotificationId).ToList();
                }

            }
            catch (Exception)
            {
                throw;
            }

            ViewBag.CountInfo = datalst.Count() > 0 ? datalst.FirstOrDefault().TotalNotiCount : 0;

            return PartialView(datalst == null ? new List<NotificationListVM>() : datalst);
        }
        #endregion
    }
}