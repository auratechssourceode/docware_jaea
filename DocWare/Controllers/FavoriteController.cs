﻿using DocWare.Helpers;
using DocWare.Models;
using Ionic.Zip;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utility.Helpers;
using Utility.Models;

namespace DocWare.Controllers
{
    [CheckAuthorization]
    public class FavoriteController : Controller
    {
        DBEntities db = new DBEntities();
        DAL objDAL = new DAL();
        List<string> parentLst = new List<string>();

        #region --> Favorite List | Add | Dhrumil Patel | 17052018
        [HttpGet, Route("FavoriteFiles")]
        public ActionResult FavoriteFiles()
        {
            #region --> Page Permission on Action and controller name

            var permissionlst = Session[SessionClass.LinkPermissionList].ToString();
            List<UserPermissionVM> userPermissionLst = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserPermissionVM>>(permissionlst);

            FolderPermissionVM permissionVM = new FolderPermissionVM
            {
                HasFolderPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "AddFolder" && x.ControllerName.Trim() == "FolderAndFile").Count() > 0
            };

            Session["PermissionListSession"] = permissionVM;
            #endregion
            List<FileListVM> fileListVMs = new List<FileListVM>();
            try
            {
                //SortedList sl = new SortedList();
                //DataTable dt = new DataTable();
                //long CabinetId = CommonFunctions.GetCabinetId();
                //long UserId = CommonFunctions.GetUserId();
                //sl.Add("@cabinetId", CabinetId);
                //sl.Add("@userId", UserId);
                //dt = objDAL.GetDataTable("uspGetFavoriteFilesByCabinet", sl);
                //fileListVMs = objDAL.ConvertToList<FileListVM>(dt);

                FolderListVM folderListVM = new FolderListVM
                {
                    FolderId = 0,
                    FavoriteId = 0,
                    LastOpenFolderId = 0,
                    FolderName = "",
                    CabinetName = Session[SessionClass.CabinetName].ToString()
                };
                return View(folderListVM);
            }
            catch (Exception e)
            {
                throw e;
            }
            //return View(fileListVMs);
        }
        #region --> SearchSection | Krupali |07082018
        public PartialViewResult _FavSearchDocument(long folderId, String FName = null, string catid = null, string Subcatid = null, string Lang = null, string Confidentiality = null, String Keyword = null, String Filetype = null)
        {
            List<FolderFileVM> folderDetails = new List<FolderFileVM>();
            try
            {
                //if (CommonFunctions.IsCreatedBy(folderId, false))
                //{
                    long CabinetId = CommonFunctions.GetCabinetId();
                    long UserId = CommonFunctions.GetUserId();
                    SortedList sl = new SortedList();
                    DataTable dt = new DataTable();

                    sl.Add("@CABINETID", CabinetId);
                   // sl.Add("@folderId", folderId);
                    sl.Add("@USERID", UserId);
                    sl.Add("@FileName", FName);
                    sl.Add("@Category", catid);
                    sl.Add("@SubCategory", Subcatid);
                    if (Lang != "")
                    {
                        sl.Add("@Lang", Convert.ToInt32(Lang));
                    }
                    if (Confidentiality != "")
                    {
                        sl.Add("@Confidentiality", Convert.ToInt32(Confidentiality));
                    }
                    if (Keyword != "")
                    {
                        sl.Add("@Keyword", Keyword);
                    }
                    if (Filetype != "")
                    {
                        sl.Add("@Filetype", Filetype);
                    }


                    dt = objDAL.GetDataTable("uspGetSearchedFavoriteFileList", sl);
                    folderDetails = objDAL.ConvertToList<FolderFileVM>(dt);
               // }
            }
            catch (Exception e)
            {
                throw;
            }
            return PartialView("_FavSearchDocument", folderDetails);
        }

        #endregion

        public JsonResult checkIsNotEmpty(long id = 0)
        {
            DBEntities db = new DBEntities();
            if (id > 0)
            {
                var userId = CommonFunctions.GetUserId().ToString();
                var favList = db.Favorites.Where(x => x.UserId.ToString() == userId && x.ParentId == id).ToList();
                if (favList.Any())
                {
                    var fids = favList.Select(y => y.FolderId).ToList();
                    var dids = favList.Select(y => y.DocumentId).ToList();
                    var child = db.FolderDetails.Where(x => fids.Contains(x.Id) && !x.IsDelete && x.IsActive).ToList().Count;

                    if (child == 0)
                    {
                        child = db.DocumentDetails.Where(x => dids.Contains(x.DocumentId) && !x.IsDelete && x.IsActive).Count();
                    }

                    if (child > 0)
                    {
                        return Json(true, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(false, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }

        public PartialViewResult _FavFolderList(long folderId, string SearchKeyword = null)
        {
            List<FolderFileVM> folderDetails = new List<FolderFileVM>();
            try
            {
                long CabinetId = CommonFunctions.GetCabinetId();
                long UserId = CommonFunctions.GetUserId();
                var favFolder = new List<Favorite>();
                var favDoc = new List<Favorite>();
                SortedList sl = new SortedList();
                DataTable dt = new DataTable();

                sl.Add("@cabinetId", CabinetId);
                sl.Add("@folderId", folderId);
                sl.Add("@userId", UserId);

                if (!string.IsNullOrWhiteSpace(SearchKeyword))
                    sl.Add("@searchword", SearchKeyword.Trim());

                dt = objDAL.GetDataTable("uspGetFavoriteFilesByCabinet", sl);
                folderDetails = objDAL.ConvertToList<FolderFileVM>(dt);
            }
            catch (Exception e)
            {
                throw;
            }
            return PartialView("_FavFolderList", folderDetails);
        }

        public JsonResult GetFolderName(long folderId)
        {
            FolderNameDetailVM folderDetail = new FolderNameDetailVM();
            if (folderId > 0)
            {
                var detail = db.FolderDetails.Where(j => j.Id == folderId).ToList();
                var data = db.Favorites.FirstOrDefault(y => y.FolderId == folderId);
                if (data != null)
                {
                    folderDetail = detail.Select(x => new FolderNameDetailVM
                    {
                        folderId = x.Id,
                        folderName = x.Name,
                        parentFolder = x.ParentFolderId,
                        favId = db.Favorites.FirstOrDefault(y => y.FolderId == x.Id) != null ? db.Favorites.FirstOrDefault(y => y.FolderId == x.Id).Id : 0,
                        prevfavId = db.Favorites.FirstOrDefault(y => y.FolderId == x.ParentFolderId) != null ? db.Favorites.FirstOrDefault(y => y.FolderId == x.ParentFolderId).Id : 0
                    }).FirstOrDefault();
                    return Json(new { success = true, folderData = folderDetail }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    folderDetail.folderId = 0;
                    folderDetail.folderName = "Favorite";
                    folderDetail.parentFolder = 0;
                    folderDetail.favId = 0;
                    folderDetail.prevfavId = 0;
                    return Json(new { success = true, folderData = folderDetail }, JsonRequestBehavior.AllowGet);
                }
            }
            folderDetail.folderId = 0;
            folderDetail.folderName = "Favorite";
            folderDetail.parentFolder = 0;
            folderDetail.favId = 0;
            folderDetail.prevfavId = 0;
            return Json(new { success = true, folderData = folderDetail }, JsonRequestBehavior.AllowGet);
            //return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region --> Folder/File Info and Comment | Add | Dhrumil Patel | 02052018
        [HttpGet]
        public PartialViewResult _FolderFileInfo(FolderFileSelected detail)
        {
            FolderFileInfo info = new FolderFileInfo();
            DAL objDAL = new DAL();
            SortedList sl = new SortedList();
            var userIds = CommonFunctions.GetUserId();
            var cabinetId = CommonFunctions.GetCabinetId();
            sl.Add("@UserId", userIds);
            sl.Add("@FolderId", detail.FolderId);
            sl.Add("@DocumentId", detail.DocumentId);
            sl.Add("@FolderIds", detail.FolderIds);
            sl.Add("@FileIds", detail.FileIds);
            sl.Add("@CabinetId", cabinetId);
            info = objDAL.ConvertToList<FolderFileInfo>(objDAL.GetDataTable("uspGetFavoriteFolderFileInfo", sl)).FirstOrDefault();
            info.SelectedCount = detail.SelectedCount;
            return PartialView(info != null ? info : new FolderFileInfo());
        }
        #endregion

        #region --> Download folder and file | Add | Dhrumil Patel | 07-06-2018
        [AuditLog(EnumList.EnumLogType.UpdateData, "DownloadFolderFiles")]
        public JsonResult DownloadFolderFiles(string[] folderIds, string[] fileIds)
        {
            try
            {
                if (folderIds != null || fileIds != null)
                {
                    long uid = CommonFunctions.GetUserId();
                    long cabinetId = CommonFunctions.GetCabinetId();
                    var uriPath = ConfigurationManager.AppSettings["PathForDocUpload"];
                    string folderPath = Server.MapPath(uriPath);
                    var folderDetails = db.FolderDetails.ToList();
                    var docDetails = db.DocumentVersionDetails.ToList();
                    var selectedNames = new List<string>();
                    var folIds = new List<string>();
                    var docIds = fileIds != null ? fileIds.ToList() : new List<string>();
                    var fileNames = new List<string>();
                    if (folderIds != null)
                    {
                        foreach (var item in folderIds)
                        {
                            long id = Convert.ToInt64(item);
                            folIds.Add(item);
                            if (CommonFunctions.HasChild(id) > 0)
                            {
                                parentLst = new List<string>();
                                folIds.AddRange(GetChildFolderIds(id, uid));
                            }
                        }

                        if (folIds != null && folIds.Count > 0)
                        {
                            var filIds = db.DocumentDetails.Where(x => folIds.Contains(x.FolderId.ToString()) && x.IsActive && !x.IsDelete).Select(x => x.DocumentId).ToList();
                            var finalIds = db.Favorites.Where(x => filIds.Contains(x.DocumentId)).Select(x => x.DocumentId.ToString()).ToList();
                            foreach (var id in finalIds)
                            {
                                var doc = docDetails.Where(x => x.DocumentId.ToString() == id && x.IsActive && !x.IsDelete && x.IsCurrent).OrderByDescending(x => x.Id).FirstOrDefault();
                                if (doc != null)
                                {
                                    docIds.Add(doc.Id.ToString());
                                }
                            }
                        }
                    }
                    List<string> CreatorsPath = new List<string>();
                    if (docIds != null && docIds.Count > 0)
                    {
                        CreatorsPath = docDetails.Where(x => docIds.Contains(x.Id.ToString()) && x.IsActive && !x.IsDelete).Select(x => folderPath + x.CreatedBy + "/" + cabinetId.ToString() + "/").Distinct().ToList();
                        if (docIds.Count == 1)
                        {
                            var detail = docDetails.FirstOrDefault(x => x.Id.ToString() == docIds.FirstOrDefault());
                            var docName = db.DocumentDetails.Find(detail.DocumentId).Name + "." + detail.DocumentType;
                            selectedNames.AddRange(docDetails.Where(x => docIds.Contains(x.Id.ToString()) && x.IsActive && !x.IsDelete).Select(x => folderPath + x.CreatedBy + "/" + cabinetId.ToString() + "/" + x.DocumentName + "." + x.DocumentType).Distinct().ToList());
                            var path = selectedNames[0].Substring(0, selectedNames[0].LastIndexOf('/')) + "/" + docName;
                            System.IO.File.Copy(selectedNames[0], path, true);
                            selectedNames = new List<string>();
                            selectedNames.Add(path);
                        }
                        else
                        {
                            selectedNames.AddRange(docDetails.Where(x => docIds.Contains(x.Id.ToString()) && x.IsActive && !x.IsDelete).Select(x => folderPath + x.CreatedBy + "/" + cabinetId.ToString() + "/" + x.DocumentName + "." + x.DocumentType).Distinct().ToList());
                        }
                    }

                    foreach (var path in CreatorsPath)
                    {
                        if (Directory.Exists(path))
                        {
                            string[] fileEntries = Directory.GetFiles(path);
                            foreach (string fileName in fileEntries)
                            {
                                if (selectedNames.Contains(fileName))
                                {
                                    fileNames.Add(fileName);
                                }
                            }
                        }
                    }
                    if (fileNames != null && fileNames.Count > 0)
                    {
                        TempData["FileNameList"] = fileNames.ToList();
                        return Json(new { result = "success", Message = "Download successfully!" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { result = "error", Message = "Sorry! Please try again." }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { result = "error", Message = "Sorry! Please try again." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { result = "error", Message = "Sorry! Please try again." }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DownloadSelected(string folid)
        {
            List<string> fileNameLst = new List<string>();
            if (TempData["FileNameList"] != null)
            {
                fileNameLst = (List<string>)TempData["FileNameList"];
                TempData["FileNameList"] = null;
            }
            long uid = CommonFunctions.GetUserId();
            long cabinetId = CommonFunctions.GetCabinetId();
            if (fileNameLst != null && fileNameLst.Count > 0)
            {
                try
                {
                    using (ZipFile zip = new ZipFile())
                    {
                        UtilityCommonFunctions.RandomStringGenerator random = new UtilityCommonFunctions.RandomStringGenerator();
                        string serialNumber = random.GetRandomString(4, UtilityCommonFunctions.RandomStringGenerator.ALPHANUMERIC_CAPS.ToCharArray());
                        string ZipFileName = string.Empty;
                        var uriPath = ConfigurationManager.AppSettings["PathForDocUpload"] + uid.ToString() + "/" + cabinetId.ToString() + "/";
                        string folderPath = Server.MapPath(uriPath);
                        var name = fileNameLst[0].Substring(fileNameLst[0].LastIndexOf('\\') + 1, fileNameLst[0].Length - 1 - fileNameLst[0].LastIndexOf('\\'));
                        ZipFileName = fileNameLst.Count() > 1 ?
                            String.Format("Docware-{0}-MultiItems-{1}-{2}", Session[SessionClass.CabinetName], DateTime.Now.ToString("yyyyMMMdd-HHmm"), serialNumber) :
                            String.Format("Docware-{0}-{1}-{2}-{3}", Session[SessionClass.CabinetName], name, DateTime.Now.ToString("yyyyMMMdd-HHmm"), serialNumber);
                        ZipFileName = ZipFileName.Replace(" ", "_");
                        zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                        zip.AddDirectoryByName("Docware");
                        foreach (string fileName in fileNameLst)
                        {
                            zip.AddFile(fileName, "Docware");
                        }

                        if (zip.Count > 1)
                        {
                            Response.Clear();
                            Response.BufferOutput = false;
                            string zipName = ZipFileName + ".zip";
                            Response.ContentType = "application/zip";
                            Response.AddHeader("content-disposition", "attachment; filename=" + zipName);
                            zip.Save(Response.OutputStream);
                            Response.End();
                            if (fileNameLst.Count() == 1)
                            {
                                System.IO.File.Delete(fileNameLst[0]);
                            }
                        }
                        else
                        {
                            TempData["ErrorMSG"] = "Documents not available.";
                        }
                    }
                }
                catch (Exception ex)
                {
                    TempData["ErrorMSG"] = "Some error occured. Please try again.";
                }
            }
            return RedirectToAction("FolderList", new { id = folid });
        }

        public List<string> GetChildFolderIds(long Id, long userId)
        {
            try
            {
                var subFolderIds = db.FolderDetails.Where(j => j.ParentFolderId == Id && j.IsActive && !j.IsDelete).Select(x => x.Id).ToList();
                var favSubFolderList = db.Favorites.Where(x => subFolderIds.Contains(x.FolderId)).ToList();
                if (favSubFolderList.Count > 0)
                {
                    foreach (var item in favSubFolderList)
                    {
                        parentLst.Add(item.FolderId.ToString());
                        if (CommonFunctions.HasChild(item.FolderId) > 0)
                        {
                            GetChildFolderIds(item.FolderId, userId);
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return parentLst;
        }
        #endregion

        #region --> DocShare Detail | Add | Dhrumil Patel | 14062018
        public JsonResult DocShareDetail(long id, bool isDoc)
        {
            DocShare docShareVM = new DocShare();
            var shareWithMeData = new UserDocShareDetail();
            var userId = CommonFunctions.GetUserId();
            try
            {
                using (db = new DBEntities())
                {
                    var docShareLst = db.UserDocShareDetails.ToList();
                    if (isDoc)
                    {
                        if (!CommonFunctions.IsCreatedBy(id, true))
                        {
                            shareWithMeData = CommonFunctions.GetUserPermission(true, id);
                            docShareVM.DocSharePermission.IsNotCreated = true;
                        }
                    }
                    else
                    {
                        if (!CommonFunctions.IsCreatedBy(id, false))
                        {
                            shareWithMeData = CommonFunctions.GetUserPermission(false, id);
                            docShareVM.DocSharePermission.IsNotCreated = true;
                        }
                    }
                    if (shareWithMeData.UserId != 0)
                    {
                        docShareVM.DocSharePermission.UserId = shareWithMeData.DocShareUserId;
                        docShareVM.DocSharePermission.GroupId = shareWithMeData.DocShareGroupId;
                        docShareVM.DocSharePermission.Name = db.LoginCredentials.Where(y => y.Id == shareWithMeData.UserId).Select(y => y.FirstName + " " + y.LastName).FirstOrDefault();
                        docShareVM.DocSharePermission.IsDownload = shareWithMeData.IsDownload;
                        docShareVM.DocSharePermission.IsUpload = shareWithMeData.IsUpload;
                        docShareVM.DocSharePermission.IsSeeVersion = shareWithMeData.IsSeeVersion;
                        docShareVM.DocSharePermission.IsReadComment = shareWithMeData.IsReadComment;
                        docShareVM.DocSharePermission.IsAddComment = shareWithMeData.IsAddComment;
                        docShareVM.DocSharePermission.IsShare = shareWithMeData.IsShare;
                    }

                    if (id > 0)
                    {
                        if (isDoc)
                        {
                            var data = db.DocumentDetails.Find(id);
                            if (data != null)
                            {
                                docShareVM.FileId = id;
                                docShareVM.FolderId = 0;
                                docShareVM.Name = data.Name;
                            }

                            var shareData = docShareLst.Where(x => x.DocumentId == id && x.UserId == userId).ToList();
                            if (shareData.Count > 0)
                            {
                                docShareVM.DocShareLst = shareData.Select(x => new DocShareList
                                {
                                    UserId = x.DocShareUserId,
                                    GroupId = x.DocShareGroupId,
                                    Name = x.DocShareUserId != 0 ?
                                        db.LoginCredentials.Where(y => y.Id == x.DocShareUserId).Select(y => y.FirstName + " " + y.LastName).FirstOrDefault() :
                                        db.GroupDetails.Where(y => y.Id == x.DocShareGroupId).Select(y => y.Name).FirstOrDefault(),
                                    IsDownload = x.IsDownload,
                                    IsUpload = x.IsUpload,
                                    IsSeeVersion = x.IsSeeVersion,
                                    IsReadComment = x.IsReadComment,
                                    IsAddComment = x.IsAddComment,
                                    IsShare = x.IsShare
                                }).ToList();
                            }
                        }
                        else
                        {
                            var data = db.FolderDetails.Find(id);
                            if (data != null)
                            {
                                docShareVM.FileId = 0;
                                docShareVM.FolderId = id;
                                docShareVM.Name = data.Name;
                            }

                            var shareData = docShareLst.Where(x => x.FolderId == id && x.UserId == userId).ToList();
                            if (shareData.Count > 0)
                            {
                                docShareVM.DocShareLst = shareData.Select(x => new DocShareList
                                {
                                    UserId = x.DocShareUserId,
                                    GroupId = x.DocShareGroupId,
                                    Name = x.DocShareUserId != 0 ?
                                        db.LoginCredentials.Where(y => y.Id == x.DocShareUserId).Select(y => y.FirstName + " " + y.LastName).FirstOrDefault() :
                                        db.GroupDetails.Where(y => y.Id == x.DocShareGroupId).Select(y => y.Name).FirstOrDefault(),
                                    IsDownload = x.IsDownload,
                                    IsUpload = x.IsUpload,
                                    IsSeeVersion = x.IsSeeVersion,
                                    IsReadComment = x.IsReadComment,
                                    IsAddComment = x.IsAddComment,
                                    IsShare = x.IsShare
                                }).ToList();
                            }
                        }
                        return Json(new { success = true, shareData = docShareVM }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "AddRemoveDocShareDetail")]
        public JsonResult AddRemoveDocShareDetail(DocShare detail)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                var notificationdetail = new SaveNotificationVM();
                var userId = CommonFunctions.GetUserId();
                var cabinetId = CommonFunctions.GetCabinetId();
                try
                {
                    if (detail.RemoveUserIdsList != null)
                    {
                        foreach (var id in detail.RemoveUserIdsList)
                        {
                            if (id != "")
                            {
                                var uId = Convert.ToInt64(id);
                                var delData = db.UserDocShareDetails.Where(x => x.UserId == userId && x.FolderId == detail.FolderId
                                            && x.DocumentId == detail.FileId && x.DocShareUserId == uId).FirstOrDefault();
                                if (delData != null)
                                {
                                    db.UserDocShareDetails.Remove(delData);
                                    db.SaveChanges();
                                }
                                if (detail.FolderId != 0 && CommonFunctions.HasChild(detail.FolderId) > 0)
                                {
                                    RemoveChildFromShare(detail.FolderId, userId, uId, 0);
                                }
                            }
                        }
                    }
                    if (detail.RemoveGroupIdsList != null)
                    {
                        foreach (var id in detail.RemoveGroupIdsList)
                        {
                            if (id != "")
                            {
                                var gId = Convert.ToInt64(id);
                                var delData = db.UserDocShareDetails.Where(x => x.UserId == userId && x.FolderId == detail.FolderId
                                            && x.DocumentId == detail.FileId && x.DocShareGroupId == gId).FirstOrDefault();
                                if (delData != null)
                                {
                                    db.UserDocShareDetails.Remove(delData);
                                    db.SaveChanges();
                                }
                                if (detail.FolderId != 0 && CommonFunctions.HasChild(detail.FolderId) > 0)
                                {
                                    RemoveChildFromShare(detail.FolderId, userId, 0, gId);
                                }
                            }
                        }
                    }

                    foreach (var dt in detail.DocShareLst)
                    {
                        var lst = new List<UserDocShareDetail>();
                        if (detail.FolderId != 0)
                        {
                            var folData = db.FolderDetails.Find(detail.FolderId);
                            lst = db.UserDocShareDetails.Where(x => x.UserId == userId 
                                        && x.FolderId == folData.ParentFolderId
                                        && x.DocShareUserId == dt.UserId
                                        && x.DocShareGroupId == dt.GroupId).ToList();
                            notificationdetail.FolderId = detail.FolderId;
                            notificationdetail.TypeFor = "Folder";
                        }
                        else
                        {
                            var folData = db.DocumentDetails.Find(detail.FileId);
                            lst = db.UserDocShareDetails.Where(x => x.UserId == userId 
                                        && x.FolderId == folData.FolderId
                                        && x.DocShareUserId == dt.UserId
                                        && x.DocShareGroupId == dt.GroupId).ToList();
                            notificationdetail.DocumentId = detail.FileId;
                            notificationdetail.TypeFor = "File";
                        }
                        long parentId = 0;
                        if (lst.Any())
                        {
                            parentId = lst.FirstOrDefault().Id;
                        }
                        var data = new UserDocShareDetail();
                        data.UserId = userId;
                        data.FolderId = detail.FolderId;
                        data.DocumentId = detail.FileId;
                        data.DocShareUserId = dt.UserId;
                        data.DocShareGroupId = dt.GroupId;
                        data.ParentId = parentId;
                        data.CabinetId = CommonFunctions.GetCabinetId();
                        data.IsDownload = dt.IsDownload;
                        data.IsUpload = dt.IsUpload;
                        data.IsSeeVersion = dt.IsSeeVersion;
                        data.IsReadComment = dt.IsReadComment;
                        data.IsAddComment = dt.IsAddComment;
                        data.IsShare = dt.IsShare;
                        data.CreatedBy = userId.ToString();
                        data.CreatedDate = DateTime.Now;
                        db.UserDocShareDetails.Add(data);
                        db.SaveChanges();

                        if (detail.FolderId != 0 && CommonFunctions.HasChild(detail.FolderId) > 0)
                        {
                            AddChildToShare(detail.FolderId, userId, data.Id, dt);
                        }
                    }
                    transaction.Commit();
                    notificationdetail.UserId = userId;
                    notificationdetail.CabinetId = cabinetId;
                    notificationdetail.NotificationType = (int)EnumList.EnumNotificationType.Share;
                    CommonFunctions.SaveNotification(notificationdetail);
                    return Json(new { success = true, msg = "Shared successfully." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return Json(new { success = false, msg = "Sorry! Please try again." }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public void AddChildToShare(long Id, long userId, long parentId, DocShareList dt)
        {
            try
            {
                var subDocIds = db.DocumentDetails.Where(j => j.FolderId == Id).Select(x => x.DocumentId).ToList();
                var favLst = db.Favorites.ToList();
                if (subDocIds.Count > 0)
                {
                    var shareLst = favLst.Where(x => x.UserId == userId && subDocIds.Contains(x.DocumentId)).ToList();

                    if (shareLst.Count != 0)
                    {
                        foreach (var item in shareLst.Distinct().ToList())
                        {
                            var data = new UserDocShareDetail();
                            data.UserId = userId;
                            data.FolderId = 0;
                            data.DocumentId = item.DocumentId;
                            data.DocShareUserId = dt.UserId;
                            data.DocShareGroupId = dt.GroupId;
                            data.ParentId = parentId;
                            data.CabinetId = CommonFunctions.GetCabinetId();
                            data.IsDownload = dt.IsDownload;
                            data.IsUpload = dt.IsUpload;
                            data.IsSeeVersion = dt.IsSeeVersion;
                            data.IsReadComment = dt.IsReadComment;
                            data.IsAddComment = dt.IsAddComment;
                            data.IsShare = dt.IsShare;
                            data.CreatedBy = userId.ToString();
                            data.CreatedDate = DateTime.Now;
                            db.UserDocShareDetails.Add(data);
                            db.SaveChanges();
                        }
                    }
                }

                var subFolderIds = db.FolderDetails.Where(j => j.ParentFolderId == Id).Select(x => x.Id).ToList();
                if (subFolderIds.Count > 0)
                {
                    var shareFolLst = favLst.Where(x => x.UserId == userId && subFolderIds.Contains(x.FolderId)).ToList();
                    if (shareFolLst.Count != 0)
                    {
                        foreach (var item in shareFolLst.Distinct().ToList())
                        {
                            var data = new UserDocShareDetail();
                            data.UserId = userId;
                            data.FolderId = item.Id;
                            data.DocumentId = 0;
                            data.DocShareUserId = dt.UserId;
                            data.DocShareGroupId = dt.GroupId;
                            data.ParentId = parentId;
                            data.CabinetId = CommonFunctions.GetCabinetId();
                            data.IsDownload = dt.IsDownload;
                            data.IsUpload = dt.IsUpload;
                            data.IsSeeVersion = dt.IsSeeVersion;
                            data.IsReadComment = dt.IsReadComment;
                            data.IsAddComment = dt.IsAddComment;
                            data.IsShare = dt.IsShare;
                            data.CreatedBy = userId.ToString();
                            data.CreatedDate = DateTime.Now;
                            db.UserDocShareDetails.Add(data);
                            db.SaveChanges();

                            if (CommonFunctions.HasChild(item.Id) > 0)
                            {
                                AddChildToShare(item.Id, userId, data.Id, dt);
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void RemoveChildFromShare(long Id, long userId, long uid, long gid)
        {
            try
            {
                var subDocList = db.DocumentDetails.Where(j => j.FolderId == Id).ToList();
                var docLst = db.UserDocShareDetails.ToList();
                if (subDocList.Count > 0)
                {
                    foreach (var item in subDocList)
                    {
                        var delData = docLst.Where(x => x.UserId == userId && x.FolderId == 0
                                        && x.DocumentId == item.DocumentId && x.DocShareUserId == uid
                                        && x.DocShareGroupId == gid).FirstOrDefault();
                        if (delData != null)
                        {
                            db.UserDocShareDetails.Remove(delData);
                            db.SaveChanges();
                        }
                    }
                }

                var subFolderList = db.FolderDetails.Where(j => j.ParentFolderId == Id).ToList();

                if (subFolderList.Count > 0)
                {
                    foreach (var item in subFolderList)
                    {
                        var delData = docLst.Where(x => x.UserId == userId && x.FolderId == item.Id
                                        && x.DocumentId == 0 && x.DocShareUserId == uid
                                        && x.DocShareGroupId == gid).FirstOrDefault();
                        if (delData != null)
                        {
                            db.UserDocShareDetails.Remove(delData);
                            db.SaveChanges();
                        }
                        if (CommonFunctions.HasChild(item.Id) > 0)
                        {
                            RemoveChildFromShare(item.Id, userId, uid, gid);
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion        
    }
}