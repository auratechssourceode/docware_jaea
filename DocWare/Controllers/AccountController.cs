﻿using DocWare.Helpers;
using DocWare.Models;
using DocWare.Resources;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Security;
using Utility.Helpers;
using Utility.Models;

namespace DocWare.Controllers
{

    public class AccountController : BaseController
    {
        DAL objDAL = new DAL();

        #region --> Login  | Add | Dhrumil Patel | 05032018
        [HttpGet]
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult signup(string returnURL)
        {
            LoginVM loginVM = new LoginVM();
            try
            {
                // We do not want to use any existing identity information
                EnsureLoggedOut();
                // Store the originating URL so we can attach it to a form field
                //var viewModel = new LoginVM { ReturnURL = returnURL };
                loginVM.ReturnURL = returnURL;

                return View(loginVM);
            }
            catch
            {
                throw;
            }
        }

        [HttpGet]
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Login(string returnURL)
        {
            LoginVM loginVM = new LoginVM();
            try
            {
                // We do not want to use any existing identity information
                EnsureLoggedOut();
                // Store the originating URL so we can attach it to a form field
                //var viewModel = new LoginVM { ReturnURL = returnURL };
                loginVM.ReturnURL = returnURL;

                return View(loginVM);
            }
            catch
            {
                throw;
            }
        }

        [HttpPost]
        [AuditLog(EnumList.EnumLogType.Login, "Login")]
        public ActionResult Login(LoginVM data)
        {
            try
            {
                List<RoleMaster> roleMaster = new List<RoleMaster>();
                string encPwd = string.Empty;

                LoginVM loginVM = new LoginVM();
                using (DBEntities db = new DBEntities())
                {
                    encPwd = UtilityCommonFunctions.Encrypt(data.Password.Trim());
                    var userLogin = db.UserLoginDetails.Where(s => s.Password == encPwd && s.UserName == data.UserName).ToList();
                    if (userLogin.Count() != 0)
                    {
                        #region check for superadmin login | add | 13/03/2018
                        var isSuperadmin = userLogin.Where(s => s.ApplicationId == (int)EnumList.Application.SuperAdmin).FirstOrDefault();
                        if (isSuperadmin != null)
                        {
                            data.ApplicationId = (int)EnumList.Application.SuperAdmin;
                        }
                        #endregion

                        var userLoginDetail = userLogin.Where(s => s.ApplicationId == data.ApplicationId).FirstOrDefault();
                        if (userLoginDetail != null) // check
                        {
                            var loginCredentail = db.LoginCredentials.Find(userLoginDetail.UserId);
                            if (loginCredentail.IsActive)
                            {
                                var roleIds = db.UserRoles.Where(x => x.UserId == userLoginDetail.UserId).Select(x => x.RoleId).ToList();
                                if (roleIds.Count != 0 || data.ApplicationId == (int)EnumList.Application.SuperAdmin)
                                {
                                    roleMaster = db.RoleMasters.Where(s => roleIds.Contains(s.Id) && s.IsActive).ToList();
                                    if (roleMaster.Count != 0 || data.ApplicationId == (int)EnumList.Application.SuperAdmin)
                                    {
                                        if (loginCredentail.IsActive)
                                        {
                                            //Cabinet is now on 2nd Page so// var checkPermission = db.UserCabinetDetails.Where(s => s.UserId == userLoginDetail.UserId && s.CabinetId == data.CabinetId).FirstOrDefault();
                                            //Cabinet is now on 2nd Page so//  if (checkPermission != null || data.ApplicationId == (int)EnumList.Application.SuperAdmin)
                                            if (loginCredentail.Id > 0 || data.ApplicationId == (int)EnumList.Application.SuperAdmin)
                                            {
                                                //Successfully Login
                                                if (ModelState.IsValid)
                                                {
                                                    Session[SessionClass.UserID] = loginCredentail.Id;
                                                    Session[SessionClass.RolePermissionList] = roleMaster.Select(x => x.Id).ToList();
                                                    Session[SessionClass.UserName] = userLoginDetail.UserName;
                                                    Session[SessionClass.Email] = loginCredentail.EmailId;
                                                    //Cabinet is now on 2nd Page so//Session[SessionClass.CabinetID] = data.CabinetId;
                                                    Session[SessionClass.ApplicationID] = data.ApplicationId;

                                                    var application = db.ApplicationMasters.Find(data.ApplicationId).Name;

                                                    Session[SessionClass.FullName] = loginCredentail.FirstName + " " + loginCredentail.LastName;
                                                    Session[SessionClass.ApplicationName] = application;

                                                    if (!string.IsNullOrWhiteSpace(loginCredentail.ProfileImage))
                                                        Session[SessionClass.ProfilePic] = loginCredentail.Id.ToString() + "/" + loginCredentail.ProfileImage;

                                                    userLoginDetail.LastLogin = DateTime.Now;
                                                    db.SaveChanges();

                                                    //Cabinet is now on 2nd Page so//string menu = RoleManagement.GetMenu();

                                                    //Cabinet is now on 2nd Page so//Session[SessionClass.Menu] = menu;

                                                    SignInAsync(userLoginDetail.UserName, true);

                                                    //Cabinet is now on 2nd Page so//return RedirectToLocal(data.ReturnURL);
                                                    return RedirectToAction("Cabinet", "Account", new { returnURL = data.ReturnURL });
                                                }
                                                else
                                                {
                                                    TempData["ErrorMSG"] = Messages.Something_Went_Wrong;
                                                    return View(loginVM);
                                                }
                                            }
                                            else
                                            {
                                                //cabinet unautrized
                                                TempData["ErrorMSG"] = Messages.Cabinet_Permission_Error;
                                                return View(loginVM);
                                            }
                                        }
                                        else if (!loginCredentail.IsActive)
                                        {
                                            TempData["ErrorMSG"] = Messages.Inactive_Account_Error;
                                            return View(loginVM);
                                        }
                                        else if (loginCredentail.IsActive)
                                        {
                                            TempData["ErrorMSG"] = Messages.Invalid_Username_Password;
                                            return View(loginVM);
                                        }
                                        else
                                        {
                                            TempData["ErrorMSG"] = Messages.Invalid_Username_Password;
                                            return View(loginVM);
                                        }
                                    }
                                    else
                                    {
                                        TempData["ErrorMSG"] = Messages.Something_Went_Wrong;
                                        return View(loginVM);
                                    }
                                }
                                else
                                {
                                    TempData["ErrorMSG"] = Messages.Something_Went_Wrong;
                                    return View(loginVM);
                                }
                            }
                            else
                            {
                                TempData["ErrorMSG"] = Messages.Inactive_Account_Error;
                                return View(loginVM);
                            }
                        }
                        else
                        {
                            TempData["ErrorMSG"] = Messages.Login_Application_Validation;
                            return View(loginVM);
                        }
                    }
                    else
                    {
                        TempData["ErrorMSG"] = Messages.Invalid_Username_Password;
                        return View(loginVM);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private ActionResult RedirectToLocal(string returnURL = "")
        {
            try
            {
                // If the return url starts with a slash "/" we assume it belongs to our site
                // so we will redirect to this "action"
                if (!string.IsNullOrWhiteSpace(returnURL) && Url.IsLocalUrl(returnURL))
                {
                    return Redirect(returnURL);
                }
                else if ((long)EnumList.Application.SuperAdmin == (long)Session[SessionClass.ApplicationID])
                {
                    Session[SessionClass.HomeScreen] = "Dashboard";
                    return RedirectToAction("Dashboard", "Home");
                }
                else
                {
                    // If we cannot verify if the url is local to our host we redirect to a default location 
                    long dashboardlinkid = 0;
                    long roleId = Session[SessionClass.UserRoleID] != null ? (long)Session[SessionClass.UserRoleID] : 0;
                    long UserId = Session[SessionClass.UserID] != null ? (long)Session[SessionClass.UserID] : 0;
                    long CabinetId = CommonFunctions.GetCabinetId();
                    long ApplicationId = CommonFunctions.GetApplicationId();
                    using (DBEntities db = new DBEntities())
                    {
                        dashboardlinkid = db.Links.Where(x => x.Controller == "Home" && x.Action == "Dashboard" && x.ApplicationId == ApplicationId).Select(x => x.LinkId).FirstOrDefault();
                        List<long> roles = (List<long>)Session[SessionClass.RolePermissionList];
                        var roleLinks = db.RoleRights.Where(x => roles.Contains(x.RoleId) && x.CabinetId == CabinetId && x.LinkId == dashboardlinkid).ToList().Count();
                        var userLinks = db.UserRights.Where(x => x.UserId == UserId && x.CabinetId == CabinetId && x.LinkId == dashboardlinkid).ToList().Count();
                        if (roleLinks > 0 || userLinks > 0)
                        {
                            Session[SessionClass.HomeScreen] = "Dashboard";
                            return RedirectToAction("Dashboard", "Home");
                        }
                        else
                        {
                            if (ApplicationId == (long)EnumList.Application.User)
                            {
                                Session[SessionClass.HomeScreen] = "CabinetFiles";
                                return RedirectToAction("CabinetFiles", "Cabinet");
                            }
                            else
                            {
                                Session[SessionClass.HomeScreen] = "Welcome";
                                return RedirectToAction("Welcome", "Account");
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void EnsureLoggedOut()
        {
            try
            {
                // If the request is (still) marked as authenticated we send the user to the logout action
                if (Request.IsAuthenticated)
                    Logout();
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void SignInAsync(string userName, bool isPersistent = false)
        {
            try
            {
                // Clear any lingering authencation data
                FormsAuthentication.SignOut();

                // Write the authentication cookie
                FormsAuthentication.SetAuthCookie(userName, isPersistent);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region --> Logout  | Add | Dhrumil Patel | 05032018
        [HttpGet]
        [AuditLog(EnumList.EnumLogType.Logout, "Logout")]
        public ActionResult Logout()
        {
            try
            {
                // First we clean the authentication ticket like always
                FormsAuthentication.SignOut();
                // Second we clear the principal to ensure the user does not retain any authentication
                HttpContext.User = new GenericPrincipal(new GenericIdentity(string.Empty), null);
                Session.Clear();
                System.Web.HttpContext.Current.Session.RemoveAll();
                // Last we redirect to a controller/action that requires authentication to ensure a redirect takes place
                // this clears the Request.IsAuthenticated flag since this triggers a new request
                return RedirectToAction("Login", "Account");
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region --> ForgotPassword  | Add | Dhrumil Patel | 05032018
        [HttpGet, Route("ForgotPassword")]
        [AuditLog(EnumList.EnumLogType.ForgotPassword, "Forgot Password")]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost, Route("ForgotPassword")]
        public ActionResult ForgotPassword(string EmailId, byte ApplicationId)
        {
            try
            {
                string body = "";
                bool isSent = false;
                UserLoginDetail detail = new UserLoginDetail();
                using (DBEntities db = new DBEntities())
                {
                    body = db.EmailTemplates.FirstOrDefault(x => x.IsActive && x.Id == (int)EnumList.EmailTemplate.ForgotPassword).Body;
                    var userDetail = db.LoginCredentials.FirstOrDefault(x => x.EmailId == EmailId);
                    if (body != "" && userDetail != null)
                    {
                        detail = db.UserLoginDetails.FirstOrDefault(x => x.UserId == userDetail.Id && x.ApplicationId == ApplicationId);
                        if (detail != null)
                        {
                            isSent = true;
                            var fromEmail = WebConfigurationManager.AppSettings["SendEmailInfo"].ToString();

                            string fullname = userDetail.FirstName + " " + userDetail.LastName;

                            var UserID = UtilityCommonFunctions.Encrypt(detail.UserId.ToString());
                            var AppID = UtilityCommonFunctions.Encrypt(ApplicationId.ToString());

                            string redirect = ConfigurationManager.AppSettings["RedirectLink"].ToString();
                            string lnk1 = "<a href=" + redirect + "/ResetPasswordConfirmation/";
                            string lnk2 = " target='_blank'>Reset Your Password</a>";
                            string lnk = lnk1 + UserID + "/" + AppID + lnk2;

                            Dictionary<string, string> replacement = new Dictionary<string, string>();
                            replacement.Add("#FullName#", fullname);
                            replacement.Add("#URL#", lnk);

                            bool f = UtilityCommonFunctions.SendEmailTemplete((int)EnumList.EmailTemplate.ForgotPassword, replacement, EmailId, fromEmail);
                            if (!f)
                            {
                                return Json(new { flag = false, message = "Fail" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return Json(new { flag = false, message = "ApplicationNotFound" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                if (!isSent)
                {
                    return Json(new { flag = false, message = "NotFound" }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { flag = true, message = "Success" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { flag = false, message = "Fail" }, JsonRequestBehavior.AllowGet);
                throw e;
            }
        }

        [HttpGet, Route("ResetPasswordConfirmation/{id}/{appid}")]
        public ActionResult ResetPasswordConfirmation()
        {
            ResetPasswordConfirmVM resetPasswordConfirmVM = new ResetPasswordConfirmVM();
            try
            {
                if (RouteData.Values["id"] != null)
                {
                    string userId = RouteData.Values["id"].ToString();
                    string appId = RouteData.Values["appid"].ToString();
                    if (!string.IsNullOrWhiteSpace(userId) && !string.IsNullOrWhiteSpace(appId))
                    {
                        long UserID = Convert.ToInt32(UtilityCommonFunctions.Decrypt(userId));
                        long AppID = Convert.ToInt32(UtilityCommonFunctions.Decrypt(appId));
                        if (UserID > 0 && AppID > 0)
                        {
                            resetPasswordConfirmVM.UserId = UserID;
                            resetPasswordConfirmVM.ApplicationId = AppID;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return View(resetPasswordConfirmVM);
        }

        public JsonResult SendPassword(long UserID, long ApplicationId)
        {
            try
            {
                using (DBEntities db = new DBEntities())
                {
                    var userDetail = db.LoginCredentials.FirstOrDefault(x => x.Id == UserID);
                    if (userDetail != null)
                    {
                        var detail = db.UserLoginDetails.FirstOrDefault(x => x.UserId == userDetail.Id && x.ApplicationId == ApplicationId);

                        if (detail != null)
                        {
                            UtilityCommonFunctions.RandomStringGenerator random = new UtilityCommonFunctions.RandomStringGenerator();
                            string password = random.GetRandomString(8, UtilityCommonFunctions.RandomStringGenerator.ALPHANUMERIC_CAPS.ToCharArray());

                            #region --> Update Password in UserLogin
                            detail.Password = UtilityCommonFunctions.Encrypt(password);
                            db.Entry(detail).State = EntityState.Modified;
                            db.SaveChanges();
                            #endregion
                            #region --> Add Password in PasswordHistory
                            PasswordHistory pswHistory = new PasswordHistory();
                            pswHistory.UserId = UserID;
                            pswHistory.ApplicationId = Convert.ToByte(ApplicationId);
                            pswHistory.Password = detail.Password;
                            pswHistory.ChangeDate = DateTime.Now;
                            db.PasswordHistories.Add(pswHistory);
                            db.SaveChanges();
                            #endregion

                            var fromEmail = WebConfigurationManager.AppSettings["SendEmailInfo"].ToString();

                            string EmailId = userDetail.EmailId;
                            string fullname = userDetail.FirstName + " " + userDetail.LastName;

                            Dictionary<string, string> replacement = new Dictionary<string, string>();
                            replacement.Add("#FullName#", fullname);
                            replacement.Add("#UserName#", detail.UserName);
                            replacement.Add("#Password#", password);

                            bool f = UtilityCommonFunctions.SendEmailTemplete((int)EnumList.EmailTemplate.ConfirmForgotPassword, replacement, EmailId, fromEmail);
                            if (!f)
                            {
                                return Json(new { flag = false, message = "Fail" }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { flag = true, message = "Success" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                }
                return Json(new { flag = false, message = "Fail" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { flag = false, message = "Fail" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region --> Globle Error Pages | Add | Sweta Patel | 20180306 | copy from Loews
        [HttpGet]
        public ActionResult Error404()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Error500()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Error503()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Error401()
        {
            return View();
        }

        #endregion

        #region--> Welcome Screen | Add | Sweta Patel |26022018
        [Route("Welcome")]
        public ActionResult Welcome()
        {
            if (Session[SessionClass.CabinetID] != null)
            {
                long cabinetid = 0;
                cabinetid = Convert.ToInt64(Session[SessionClass.CabinetID]);

                if (cabinetid > 0)
                    return View();
                else
                    return RedirectToAction("Login", "Account");
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }

        }
        #endregion

        #region--> User Profile | Add | Dhrumil Patel |12032018
        [Route("UserProfile")]
        [AuditLog(EnumList.EnumLogType.UpdateData, "UserProfile")]
        public ActionResult UserProfile()
        {
            if (Session[SessionClass.UserID] == null)
            {
                TempData["ErrorMSG"] = Messages.Your_session_has_expired;
                return RedirectToAction("Login");
            }
            var id = CommonFunctions.GetUserId();
            var ApplicationId = (long)Session[SessionClass.ApplicationID];
            UserProfile userdetail = new UserProfile();
            DAL objDAL = new DAL();
            SortedList sl = new SortedList();
            DataTable dt = new DataTable();
            try
            {
                if (id != 0)
                {
                    sl.Add("@UserId", id);
                    sl.Add("@ApplicationId", ApplicationId);
                    dt = objDAL.GetDataTable("uspGetUserDetail", sl);
                    userdetail = objDAL.ConvertToList<UserProfile>(dt).FirstOrDefault();
                    if (userdetail != null && userdetail.UserId > 0)
                    {
                        userdetail.Password = UtilityCommonFunctions.Decrypt(userdetail.Password);
                        userdetail.ConfirmPassword = userdetail.Password;
                        userdetail.Gender = (userdetail.Gender != "F" && userdetail.Gender != "M") ? "M" : userdetail.Gender;
                    }
                }
            }
            catch (Exception)
            {
                return View(userdetail);
            }
            return View(userdetail);
        }

        [HttpPost, Route("UserProfile")]
        public ActionResult UserProfile(UserProfile detail, HttpPostedFileBase updProfileImage)
        {
            using (var db = new DBEntities())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        #region profile image 
                        string folderPathUserimage = string.Empty;
                        string fileName = string.Empty;
                        if (updProfileImage != null)
                        {
                            var path = ConfigurationManager.AppSettings["PathForProfileUpload"] + detail.UserId.ToString();
                            folderPathUserimage = Server.MapPath(path);
                            if (!Directory.Exists(folderPathUserimage))
                            {
                                Directory.CreateDirectory(folderPathUserimage);
                            }
                            string _FileName = Path.GetFileName(updProfileImage.FileName);
                            string _path = Path.Combine(folderPathUserimage, _FileName);
                            if (detail.ProfileImage != null)
                            {
                                string _deletepath = Path.Combine(folderPathUserimage, detail.ProfileImage);
                                if (System.IO.File.Exists(_deletepath))
                                {
                                    System.IO.File.Delete(_deletepath);
                                }
                            }
                            updProfileImage.SaveAs(_path);
                            fileName = updProfileImage.FileName;
                            Session[SessionClass.ProfilePic] = detail.UserId + "/" + _FileName;
                        }
                        else
                        {
                            fileName = detail.ProfileImage;
                        }
                        #endregion

                        #region User detail update 
                        var userdetail = db.LoginCredentials.Find(detail.UserId);
                        userdetail.FirstName = detail.FirstName;
                        userdetail.MiddleName = detail.MiddleName;
                        userdetail.LastName = detail.LastName;
                        userdetail.Gender = detail.Gender;
                        userdetail.EmailId = detail.EmailId;
                        userdetail.MobileNumber = detail.MobileNumber;
                        userdetail.PhoneNumber = detail.PhoneNumber;
                        userdetail.DOB = Convert.ToDateTime(detail.DOB);
                        userdetail.AssignSubsidiary = detail.AssignSubsidiary;
                        if (detail.AssignSubsidiary)
                        {
                            userdetail.SubsidiaryUserId = detail.SubsidiaryUserId;
                        }
                        else
                        {
                            userdetail.SubsidiaryUserId = 0;
                        }
                        userdetail.UpdatedDate = DateTime.Now;
                        userdetail.UpdatedBy = CommonFunctions.GetUserId().ToString();
                        userdetail.ProfileImage = fileName;
                        db.SaveChanges();
                        #endregion

                        #region User login detail update
                        var userLogindetailList = db.UserLoginDetails.FirstOrDefault(x => x.UserId == detail.UserId && x.ApplicationId == detail.ApplicationId);
                        if (userLogindetailList != null)
                        {
                            userLogindetailList.Password = UtilityCommonFunctions.Encrypt(detail.Password);
                            userLogindetailList.UserName = detail.UserName;
                            Session[SessionClass.UserName] = detail.UserName;
                            db.SaveChanges();

                            #region --> Add Password in PasswordHistory
                            var applicationId = CommonFunctions.GetApplicationId();
                            PasswordHistory pswHistory = new PasswordHistory();
                            pswHistory.UserId = detail.UserId;
                            pswHistory.ApplicationId = (byte)applicationId;
                            pswHistory.Password = userLogindetailList.Password;
                            pswHistory.ChangeDate = DateTime.Now;
                            db.PasswordHistories.Add(pswHistory);
                            db.SaveChanges();
                            #endregion
                        }
                        #endregion

                        TempData["SuccessMSG"] = Messages.User_Updated;
                    }
                    catch
                    {
                        transaction.Rollback();
                        TempData["ErrorMSG"] = Messages.Error_User_Update;
                        return View(detail);
                    }
                    transaction.Commit();
                }
            }
            if (Session[SessionClass.HomeScreen] != null)
            {
                if (Convert.ToString(Session[SessionClass.HomeScreen]) == "Dashboard")
                {
                    return RedirectToAction("Dashboard", "Home");
                }
                else
                {
                    var ApplicationId = (long)Session[SessionClass.ApplicationID];
                    if (ApplicationId == (long)EnumList.Application.User)
                    {
                        Session[SessionClass.HomeScreen] = "CabinetFiles";
                        return RedirectToAction("CabinetFiles", "Cabinet");
                    }
                    else
                    {
                        Session[SessionClass.HomeScreen] = "Welcome";
                        return RedirectToAction("Welcome", "Account");
                    }
                }
            }
            return RedirectToAction("Welcome", "Account");
        }
        #endregion

        #region --> Redirect To Home Page | Add | Sweta Patel | 15032018
        /// <summary>
        /// Method : GoToHome
        /// Description : In application HOme scree is two Dashboard & Welcome page based on User Rights so just to find correction on every Back page we have designed this method.
        /// </summary>
        /// <returns></returns>
        [Route("GoToHome")]
        public ActionResult GoToHome()
        {

            try
            {
                if ((long)EnumList.Application.SuperAdmin == (long)Session[SessionClass.ApplicationID])
                {
                    Session[SessionClass.HomeScreen] = "Dashboard";
                    return RedirectToAction("Dashboard", "Home");
                }
                else
                {
                    long dashboardlinkid = 0;
                    long roleId = Session[SessionClass.UserRoleID] != null ? (long)Session[SessionClass.UserRoleID] : 0;
                    long UserId = Session[SessionClass.UserID] != null ? (long)Session[SessionClass.UserID] : 0;
                    long CabinetId = CommonFunctions.GetCabinetId();
                    using (DBEntities db = new DBEntities())
                    {
                        dashboardlinkid = db.Links.Where(x => x.Controller == "Home" && x.Action == "Dashboard").Select(x => x.LinkId).FirstOrDefault();
                        List<long> roles = (List<long>)Session[SessionClass.RolePermissionList];
                        var roleLinks = db.RoleRights.Where(x => roles.Contains(x.RoleId) && x.CabinetId == CabinetId && x.LinkId == dashboardlinkid).ToList().Count();
                        var userLinks = db.UserRights.Where(x => x.UserId == UserId && x.CabinetId == CabinetId && x.LinkId == dashboardlinkid).ToList().Count();
                        if (roleLinks > 0 || userLinks > 0)
                        {
                            Session[SessionClass.HomeScreen] = "Dashboard";
                            return RedirectToAction("Dashboard", "Home");
                        }
                        else
                        {
                            Session[SessionClass.HomeScreen] = "Welcome";
                            return RedirectToAction("Welcome", "Account");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region --> Select Cabinet after Login | ADD | Sweta Patel | 15032018
        [HttpGet]
        [Route("Cabinet")]
        public ActionResult Cabinet(string returnURL)
        {
            try
            {
                LoginVM data = new LoginVM();

                if (Session[SessionClass.UserID] == null)
                {
                    TempData["ErrorMSG"] = Messages.An_Error_Occured;
                    return RedirectToAction("Login", "Account");
                }
                else
                {
                    data.ReturnURL = returnURL;
                    data.UserName = Convert.ToString(Session[SessionClass.UserName]);
                    data.ApplicationId = Convert.ToInt64(Session[SessionClass.ApplicationID]);
                }
                return View(data);
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        [HttpPost]
        [Route("Cabinet")]
        [AuditLog(EnumList.EnumLogType.Login, "select cabinet after login")]
        public ActionResult Cabinet(LoginVM data)
        {

            try
            {
                using (DBEntities db = new DBEntities())
                {
                    Session[SessionClass.CabinetID] = string.Empty;
                    Session[SessionClass.CabinetID] = data.CabinetId;
                    Session[SessionClass.CabinetName] = string.Empty;
                    Session[SessionClass.CabinetName] = db.CabinetMasters.Find(data.CabinetId).Name;
                    string menu = RoleManagement.GetMenu();
                    Session[SessionClass.Menu] = string.Empty;
                    Session[SessionClass.Menu] = menu;
                    Session[SessionClass.LinkPermissionList] = string.Empty;
                    Session[SessionClass.LinkPermissionList] = RoleManagement.GetAllLinkPermission();
                    if (data.ReturnURL != null && data.ReturnURL == "/Dashboard" && !menu.Contains(data.ReturnURL.Substring(1)))
                    {
                        data.ReturnURL = "/Welcome";
                    }
                }
                return RedirectToLocal(data.ReturnURL);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region -->check Password is correct or not| Add | Sweta Patel | 20032018
        [Route("checkPassword")]
        public JsonResult checkPassword(string PreviousPassword, long? id = 0)
        {
            DBEntities db = new DBEntities();
            if (PreviousPassword != null & PreviousPassword.Length >= 3)
            {
                var Username = PreviousPassword;
                long? Id = id;
                long ApplicationId = CommonFunctions.GetApplicationId();
                string DecPassword = UtilityCommonFunctions.Encrypt(PreviousPassword);
                if (Id > 0)
                {
                    UserLoginDetail lstUserLoginDetais = db.UserLoginDetails.Where(x => x.UserId == id && x.ApplicationId == ApplicationId && x.Password == DecPassword).FirstOrDefault();
                    if (lstUserLoginDetais != null && lstUserLoginDetais.LoginId > 0)
                    {
                        return Json(true, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(false, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region --> Lock Screen  | Add | Dhrumil Patel | 31052018
        [HttpGet]
        [Route("LockScreen")]
        public ActionResult LockScreen()
        {
            LockScreen LockScreen = new LockScreen();
            try
            {
                if (TempData["LockScreen"] != null)
                {
                    TempData.Keep("LockScreen");
                    LockScreen = (LockScreen)TempData.Peek("LockScreen");
                }
                //LockScreen = TempData["LockScreen"] != null ? (LockScreen)TempData.Peek("LockScreen") : new LockScreen();
                return View(LockScreen);
            }
            catch
            {
                throw;
            }
        }

        [HttpPost]
        [Route("LockScreen1")]
        public ActionResult LockScreen1(LockScreen LockScreen, FormCollection fc)
        {
            try
            {
                LockScreen.ReturnURL = Request.UrlReferrer.AbsolutePath.ToString();
                TempData["LockScreen"] = LockScreen;
                //later on decide to keep it
                TempData.Keep("LockScreen");
                return RedirectToAction("LockScreen");
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get Favorite Count | Add | Sweta Patel | 24052018
        public JsonResult GetFavoriteCount()
        {
            try
            {
                SortedList sl = new SortedList();
                long CabinetId = CommonFunctions.GetCabinetId();
                long UserId = CommonFunctions.GetUserId();
                sl.Add("@cabinetId", CabinetId);
                sl.Add("@userId", UserId);
                var count = objDAL.ExecuteScaler("uspGetFavoriteCountByUserCabinet", sl);
                return Json(new { fvtCount = count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { fvtCount = 0 }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Get Child Folder Count | Add | Dhrumil Patel | 29052018
        public JsonResult GetFolderChildCount(long id)
        {
            try
            {
                var childCount = RoleManagement.GetFolderChildCount(id);
                return Json(new { folderCount = childCount }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { folderCount = 0 }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Get Public Count | Add | Dhrumil Patel | 30052018
        public JsonResult GetPublicCount()
        {
            try
            {
                SortedList sl = new SortedList();
                DataTable dt = new DataTable();
                long CabinetId = CommonFunctions.GetCabinetId();
                long UserId = CommonFunctions.GetUserId();
                sl.Add("@cabinetId", CabinetId);
                sl.Add("@userId", UserId);
                dt = objDAL.GetDataTable("uspGetPublicFilesByCabinet", sl);
                var count = objDAL.ConvertToList<FileListVM>(dt).Count();
                return Json(new { folderCount = count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { folderCount = 0 }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Get Deletd Folder Count | Add | Dhrumil Patel | 30052018
        public JsonResult GetDeleteFolderCount()
        {
            try
            {
                var childCount = RoleManagement.GetDeleteFolderCount();
                return Json(new { folderCount = childCount }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { folderCount = 0 }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region--> Download Document From Email
        public ActionResult DownloadFile(string id, string dvID)
        {
            try
            {
                DocumentVersionDownload model = new DocumentVersionDownload();
                long userId = Convert.ToInt64(UtilityCommonFunctions.Decrypt(id));
                using (DBEntities db = new DBEntities())
                {
                    LoginCredential lc = db.LoginCredentials.FirstOrDefault(x => x.Id == userId && x.IsActive && !x.IsDelete);
                    if (lc != null)
                        model.UserName = lc.FirstName + " " + lc.LastName;
                    model.DocVersionId = dvID;
                    model.UserId = id;
                }
                return View(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult DownloadFile(DocumentVersionDownload model)
        {
            try
            {
                long userId = Convert.ToInt64(UtilityCommonFunctions.Decrypt(model.UserId));
                using (DBEntities db = new DBEntities())
                {
                    LoginCredential lc = db.LoginCredentials.FirstOrDefault(x => x.Id == userId && x.IsActive && !x.IsDelete);
                    if (lc != null)
                    {
                        string encPwd = UtilityCommonFunctions.Encrypt(model.Password.Trim());

                        bool isValidUser = db.UserLoginDetails.Any(x => x.Password == encPwd && x.UserId == userId);
                        if (isValidUser)
                        {
                            long dvID = Convert.ToInt64(UtilityCommonFunctions.Decrypt(model.DocVersionId));
                            string hostURL = WebConfigurationManager.AppSettings["RedirectLink"].ToString();
                            //string filePath = WebConfigurationManager.AppSettings["PathForFileLocation"].ToString();

                            DocumentVersionDetail dvModel = db.DocumentVersionDetails.Find(dvID);
                            return Json(new { status = true, url = hostURL + dvModel.DocumentPath.Replace("~", "") }, JsonRequestBehavior.AllowGet);

                            //string url = hostURL + dvModel.DocumentPath.Replace("~", "");
                            // string theFilename = url; //Your actual file name                            
                            //if (System.IO.File.Exists(dvModel.DocumentPath))
                            //{
                            //    //Response.AddHeader("content-disposition", "attachment; filename=" + dvModel.DocumentName+"."+dvModel.DocumentType); //optional if you want forced download
                            //    //Response.ContentType = "application/octet-stream"; //Appropriate content type based of file type
                            //    //Response.WriteFile(theFilename); //Write file to response
                            //    //Response.Flush(); //Flush contents
                            //    //Response.End(); //Complete the response
                            //    //System.IO.File.Delete(theFilename); //Delete your local file
                            //    //var pt = dvModel.DocumentPath.Replace("~", "");//"~"+ new Uri(url).LocalPath;
                            //    //string folderPath = string.Empty;
                            //    try
                            //    {
                            //        //folderPath = filePath + dvModel.DocumentPath.Replace("~", "").Replace("/", @"\");
                            //        //byte[] fileBytes = System.IO.File.ReadAllBytes(folderPath);
                            //        //string fileName = dvModel.DocumentName + "." + dvModel.DocumentType;
                            //        //return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
                            //        return Json(new { status = true, url = hostURL + dvModel.DocumentPath.Replace("~", "") }, JsonRequestBehavior.AllowGet);
                            //    }
                            //    catch (Exception)
                            //    {
                            //        return Json(new { status = false, msg="" }, JsonRequestBehavior.AllowGet);
                            //        //TempData["ErrorMSG"] = "Exception : "+ folderPath;
                            //        //return View(model);
                            //    }

                            //    //return new EmptyResult(); //return empty action result
                            //}
                            //else
                            //{
                            //    TempData["ErrorMSG"] = "File not exist."+theFilename;
                            //    return View(model);
                            //}                                                       
                        }
                        else
                        {
                            return Json(new { status = false, message = Messages.Invalid_Username_Password }, JsonRequestBehavior.AllowGet);
                            //TempData["ErrorMSG"] = Messages.Invalid_Username_Password;
                            //return View(model);
                        }
                    }
                    else
                    {
                        return Json(new { status = false, message = Messages.Invalid_Username_Password }, JsonRequestBehavior.AllowGet);
                        //TempData["ErrorMSG"] = Messages.Invalid_Username_Password;
                        //return View(model);
                    }
                }
                //return View();
            }
            catch (Exception ex)
            {
                throw ex;
                //return Json(new { status = false, message = "Something went wrong."}, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion        
    }
}