﻿using DocWare.Helpers;
using DocWare.Models;
using DocWare.Resources;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utility.Helpers;
using Utility.Models;

namespace DocWare.Controllers
{
    [CheckAuthorization]
    public class FormManagementController : BaseController
    {
        // GET: FormManagement
        private DBEntities db = new DBEntities();
        //[HttpGet, Route("FormTemplate")]
        public ActionResult DynamicForm()
        {
            FormTemplateDetail data = new FormTemplateDetail();
            return View(data);
        }

        public ActionResult EditTemplate(int id)
        {
            FormTemplateDetail data = new FormTemplateDetail();
            try
            {
                if (id >= 0)
                {
                    data = db.FormTemplateDetails.Where(x => x.Id == id).FirstOrDefault();
                    ViewBag.test = data.FormContant;
                    ViewBag.FormTitle = data.FormTitle;
                    //  return View(data);
                }
            }
            catch (Exception)
            {
                throw;
            }

            return View("DynamicForm", data);
        }

        [HttpPost]
        [AuditLog(EnumList.EnumLogType.SaveData, "SaveForm")]
        public ActionResult SaveForm(string getalldata, string title, int id)
        {
            try
            {
                FormTemplateDetail Ftd = new FormTemplateDetail();
                string msg = "";

                var userid = CommonFunctions.GetUserId();
                using (db = new DBEntities())
                {
                    if (id == 0)
                    {
                        Ftd.FormContant = getalldata;
                        Ftd.FormTitle = title;
                        Ftd.IsActive = true;
                        Ftd.IsDelete = false;
                        Ftd.CreatedBy = userid.ToString();
                        Ftd.CreatedDate = System.DateTime.Now;
                        Ftd.CabinetID = CommonFunctions.GetCabinetId();
                        TempData["SuccessMSG"] = Messages.Form_added_successfully;
                        db.FormTemplateDetails.Add(Ftd);
                        db.SaveChanges();

                    }
                    else
                    {
                        var GetDetail = db.FormTemplateDetails.Where(j => j.Id == id).FirstOrDefault();
                        GetDetail.FormContant = getalldata;
                        GetDetail.FormTitle = title;
                        Ftd.UpdatedBy = userid.ToString();
                        Ftd.CabinetID = CommonFunctions.GetCabinetId();
                        TempData["SuccessMSG"] = Messages.Form_updated_successfully;
                        GetDetail.UpdatedDate = System.DateTime.Now;
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        //[HttpGet, Route("FormTemplateList")]
        public ActionResult FormTemplateList()
        {
            List<FormTemplateDetail> Templates = new List<FormTemplateDetail>();
            var userid = CommonFunctions.GetUserId();
            long cabinetId = CommonFunctions.GetCabinetId();
            try
            {
                #region --> Page Permission on Action and controller name | Jasmin | 21032018

                var permissionlst = Session[SessionClass.LinkPermissionList].ToString();
                List<UserPermissionVM> userPermissionLst = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserPermissionVM>>(permissionlst);

                PermissionVM permissionVM = new PermissionVM();

                permissionVM.HasAddPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "DynamicForm" && x.ControllerName.Trim() == "FormManagement").Count() > 0;
                permissionVM.HasEditPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "EditTemplate" && x.ControllerName.Trim() == "FormManagement").Count() > 0;
                permissionVM.HasStatusPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "ActiveDeactiveForm" && x.ControllerName.Trim() == "FormManagement").Count() > 0;
                permissionVM.HasActivePermission = userPermissionLst.Where(x => x.ActionName.Trim() == "ActiveSelectedFormTemplate" && x.ControllerName.Trim() == "FormManagement").Count() > 0;
                permissionVM.HasDeactivePermission = userPermissionLst.Where(x => x.ActionName.Trim() == "DeactiveSelectedFormTemplate" && x.ControllerName.Trim() == "FormManagement").Count() > 0;
                permissionVM.HasDeletePermission = userPermissionLst.Where(x => x.ActionName.Trim() == "DeleteSelectedFormTemplate" && x.ControllerName.Trim() == "FormManagement").Count() > 0;

                Session["PermissionListSession"] = permissionVM;
                #endregion

                using (db = new DBEntities())
                {
                    // Templates = db.FormTemplateDetails.Where(x => !x.IsDelete).OrderByDescending(j => j.Id).ToList();
                    Templates = db.FormTemplateDetails.Where(x => x.CabinetID == cabinetId && !x.IsDelete).OrderByDescending(j => j.Id).ToList();
                }
            }
            catch
            {
                throw;
            }
            return View(Templates);
        }

        [HttpPost]
        [AuditLog(EnumList.EnumLogType.CloneData, "CloneForm")]
        public ActionResult CloneForm(string getalldata, string title)
        {
            try
            {
                FormTemplateDetail Ftd = new FormTemplateDetail();
                var userid = CommonFunctions.GetUserId();
                using (db = new DBEntities())
                {
                    Random rand = new Random();
                    Ftd.FormContant = getalldata;
                    Ftd.FormTitle = title + "_" + RandomString();
                    Ftd.IsActive = true;
                    Ftd.IsDelete = false;
                    Ftd.CabinetID = CommonFunctions.GetCabinetId();
                    Ftd.CreatedDate = System.DateTime.Now;
                    Ftd.CreatedBy = userid.ToString();
                    db.FormTemplateDetails.Add(Ftd);
                    db.SaveChanges();
                    TempData["SuccessMSG"] = Messages.Form_clone_successfully;

                }
            }
            catch (Exception)
            {

                throw;
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public string RandomString()
        {
            int _min = 1;
            int _max = 99999;
            Random _rdm = new Random();
            return _rdm.Next(_min, _max).ToString();
        }

        public JsonResult IsFormNameExist(string Formtitle1, long? id = 0)
        {
            var FormTitle = Formtitle1;
            var cabinetId = CommonFunctions.GetCabinetId();
            bool IsValid = false;
            long? Id = id;
            try
            {
                if (Id > 0)
                {
                    FormTemplateDetail Objexam = db.FormTemplateDetails.Where(x => x.Id == Id).FirstOrDefault();
                    if (FormTitle == Objexam.FormTitle && cabinetId == Objexam.CabinetID)
                    {
                        IsValid = true;
                        return Json(IsValid, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var ExamID = db.FormTemplateDetails.Where(x => x.FormTitle.ToLower() == FormTitle.ToLower() && x.CabinetID == cabinetId).Select(x => x.Id).FirstOrDefault();
                        if (ExamID > 0)
                        {
                            IsValid = false;
                            return Json(IsValid, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            IsValid = true;
                            return Json(IsValid, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else
                {
                    if (FormTitle != null)
                    {
                        var ExamID = db.FormTemplateDetails.Where(x => x.FormTitle.ToLower() == FormTitle.ToLower() && x.CabinetID == cabinetId).Select(x => x.Id).FirstOrDefault();
                        if (ExamID > 0)
                        {
                            IsValid = false;
                            return Json(IsValid, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            IsValid = true;
                            return Json(IsValid, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                IsValid = true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return Json(IsValid, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ActiveDeactiveForm(long Id)
        {
            try
            {
                if (Id != 0)
                {
                    using (db = new DBEntities())
                    {
                        var result = db.FormTemplateDetails.Where(x => x.Id == Id).FirstOrDefault();
                        if (result != null)
                        {
                            if (result.IsActive == true)
                            {
                                result.IsActive = false;
                            }
                            else
                            {
                                result.IsActive = true;
                            }
                            db.Entry(result).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                        }
                        else
                        {
                            return Json(false, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        //[HttpGet, Route("DeletedFormTemplateList")]
        //public ActionResult DeletedFormTemplateList()
        //{
        //    List<FormTemplateDetail> Templates = new List<FormTemplateDetail>();
        //    var userid = CommonFunctions.GetUserId();
        //    long cabinetId = CommonFunctions.GetCabinetId();
        //    try
        //    {
        //        #region --> Page Permission on Action and controller name | Jasmin | 22032018

        //        var permissionlst = Session[SessionClass.LinkPermissionList].ToString();
        //        List<UserPermissionVM> userPermissionLst = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserPermissionVM>>(permissionlst);

        //        PermissionVM permissionVM = new PermissionVM();

        //        permissionVM.HasRestorePermisssion = userPermissionLst.Where(x => x.ActionName.Trim() == "RestoreSelectedFormTemplate" && x.ControllerName.Trim() == "FormManagement").Count() > 0;

        //        Session["PermissionListSession"] = permissionVM;
        //        #endregion

        //        using (db = new DBEntities())
        //        {
        //            Templates = db.FormTemplateDetails.Where(x => x.CabinetID == cabinetId && x.IsDelete).OrderByDescending(j => j.Id).ToList();
        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    return View(Templates);
        //}
        
        [AuditLog(EnumList.EnumLogType.DeleteData, "DeleteSelectedFormTemplate")]
        public JsonResult DeleteSelectedFormTemplate(string[] formTemplateIds)
        {
            FormTemplateDetail formTemplateDetail = new FormTemplateDetail();
            try
            {
                if (formTemplateIds != null)
                {
                    if (formTemplateIds.Length > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();
                            foreach (var item in formTemplateIds)
                            {
                                long id = Convert.ToInt64(item);

                                formTemplateDetail = db.FormTemplateDetails.Where(m => m.Id == id).FirstOrDefault();

                                formTemplateDetail.IsActive = false;
                                formTemplateDetail.IsDelete = true;
                                formTemplateDetail.UpdatedBy = userId.ToString();
                                formTemplateDetail.UpdatedDate = DateTime.Now;
                                db.Entry(formTemplateDetail).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "DeactiveSelectedFormTemplate")]
        public JsonResult DeactiveSelectedFormTemplate(string[] formTemplateIds)
        {
            FormTemplateDetail formTemplateDetail = new FormTemplateDetail();
            try
            {
                if (formTemplateIds != null)
                {
                    if (formTemplateIds.Length > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();
                            foreach (var item in formTemplateIds)
                            {
                                long id = Convert.ToInt64(item);

                                formTemplateDetail = db.FormTemplateDetails.Where(m => m.Id == id).FirstOrDefault();

                                formTemplateDetail.IsActive = false;
                                formTemplateDetail.UpdatedBy = userId.ToString();
                                formTemplateDetail.UpdatedDate = DateTime.Now;
                                db.Entry(formTemplateDetail).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "ActiveSelectedFormTemplate")]
        public JsonResult ActiveSelectedFormTemplate(string[] formTemplateIds)
        {
            FormTemplateDetail formTemplates = new FormTemplateDetail();
            try
            {
                if (formTemplateIds != null)
                {
                    if (formTemplateIds.Length > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();

                            foreach (var item in formTemplateIds)
                            {
                                long id = Convert.ToInt64(item);

                                formTemplates = db.FormTemplateDetails.Where(m => m.Id == id).FirstOrDefault();

                                formTemplates.IsActive = true;
                                formTemplates.UpdatedBy = userId.ToString();
                                formTemplates.UpdatedDate = DateTime.Now;
                                db.Entry(formTemplates).State = EntityState.Modified;
                                db.SaveChanges();

                            }
                            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [AuditLog(EnumList.EnumLogType.UpdateData, "RestoreSelectedFormTemplate")]
        public JsonResult RestoreSelectedFormTemplate(string[] formTemplateIds)
        {
            FormTemplateDetail formTemplates = new FormTemplateDetail();
            try
            {
                if (formTemplateIds != null)
                {
                    if (formTemplateIds.Length > 0)
                    {
                        using (db = new DBEntities())
                        {
                            long userId = CommonFunctions.GetUserId();

                            foreach (var item in formTemplateIds)
                            {
                                long id = Convert.ToInt64(item);

                                formTemplates = db.FormTemplateDetails.Where(m => m.Id == id).FirstOrDefault();

                                formTemplates.IsActive = true;
                                formTemplates.IsDelete = false;
                                formTemplates.UpdatedBy = userId.ToString();
                                formTemplates.UpdatedDate = DateTime.Now;
                                db.Entry(formTemplates).State = EntityState.Modified;
                                db.SaveChanges();

                            }
                            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}