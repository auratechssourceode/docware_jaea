﻿using DocWare.Helpers;
using DocWare.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utility.Helpers;
using Utility.Models;

namespace DocWare.Controllers
{
    [CheckAuthorization]
    public class CabinetController : Controller
    {
        #region --> DECLARATION
        DBEntities db = new DBEntities();
        DAL objDAL = new DAL();
        #endregion
        // GET: Cabinet
        public ActionResult Index()
        {
            return View();
        }

        #region --> CABINET | ADD | DHRUVIN PATEL | 18062018
        [HttpGet, Route("CabinetFiles")]
        public ActionResult CabinetFiles()
        {
            #region --> PAGE PERMISSION ON ACTION AND CONTROLLER NAME
            var permissionlst = Session[SessionClass.LinkPermissionList].ToString();
            List<UserPermissionVM> userPermissionLst = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserPermissionVM>>(permissionlst);
            FolderPermissionVM permissionVM = new FolderPermissionVM
            {
                HasFolderPermission = userPermissionLst.Where(x => x.ActionName.Trim() == "AddFolder" && x.ControllerName.Trim() == "FolderAndFile").Count() > 0
            };
            Session["PermissionListSession"] = permissionVM;
            #endregion

            List<FileListVM> fileListVMs = new List<FileListVM>();
            try
            {
                FolderListVM folderListVM = new FolderListVM
                {
                    FolderId = 0,
                    FavoriteId = 0,
                    LastOpenFolderId = 0,
                    FolderName = "",
                    CabinetName = Session[SessionClass.CabinetName].ToString()
                };
                return View(folderListVM);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public PartialViewResult _CabinetList(long folderId, string SearchKeyword = null)
        {
            List<FolderFileVM> folderDetails = new List<FolderFileVM>();
            try
            {
                long CabinetId = CommonFunctions.GetCabinetId();
                long UserId = CommonFunctions.GetUserId();               
                SortedList sl = new SortedList();
                DataTable dt = new DataTable();

                sl.Add("@cabinetId", CabinetId);
                //sl.Add("@folderId", folderId);
                sl.Add("@userId", UserId);

                if (!string.IsNullOrWhiteSpace(SearchKeyword))
                    sl.Add("@searchword", SearchKeyword.Trim());

                dt = objDAL.GetDataTable("uspGetEmployesCabinetFolderDetails", sl);
                folderDetails = objDAL.ConvertToList<FolderFileVM>(dt);
            }
            catch (Exception e)
            {
                throw;
            }
            return PartialView("_CabinetList", folderDetails);
        }

        public JsonResult GetFolderName(long folderId)
        {
            FolderNameDetailVM folderDetail = new FolderNameDetailVM();
            if (folderId > 0)
            {
                var detail = db.FolderDetails.Where(j => j.Id == folderId).ToList();
                var data = db.Favorites.FirstOrDefault(y => y.FolderId == folderId);
                if (data != null)
                {
                    folderDetail = detail.Select(x => new FolderNameDetailVM
                    {
                        folderId = x.Id,
                        folderName = x.Name,
                        parentFolder = x.ParentFolderId,
                        favId = db.Favorites.FirstOrDefault(y => y.FolderId == x.Id) != null ? db.Favorites.FirstOrDefault(y => y.FolderId == x.Id).Id : 0,
                        prevfavId = db.Favorites.FirstOrDefault(y => y.FolderId == x.ParentFolderId) != null ? db.Favorites.FirstOrDefault(y => y.FolderId == x.ParentFolderId).Id : 0
                    }).FirstOrDefault();
                    return Json(new { success = true, folderData = folderDetail }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    folderDetail.folderId = 0;
                    folderDetail.folderName = "Cabinet";
                    folderDetail.parentFolder = 0;
                    folderDetail.favId = 0;
                    folderDetail.prevfavId = 0;
                    return Json(new { success = true, folderData = folderDetail }, JsonRequestBehavior.AllowGet);
                }
            }
            folderDetail.folderId = 0;
            folderDetail.folderName = "Cabinet";
            folderDetail.parentFolder = 0;
            folderDetail.favId = 0;
            folderDetail.prevfavId = 0;
            return Json(new { success = true, folderData = folderDetail }, JsonRequestBehavior.AllowGet);            
        }
        #endregion

        #region --> FOLDER/FILE INFO AND COMMENT | ADD | DHRUVIN PATEL | 18062018
        [HttpGet]
        public PartialViewResult _FolderFileInfo(FolderFileSelected detail)
        {
            FolderFileInfo info = new FolderFileInfo();
            DAL objDAL = new DAL();
            SortedList sl = new SortedList();
            var userIds = CommonFunctions.GetUserId();
            var cabinetId = CommonFunctions.GetCabinetId();
            sl.Add("@UserId", userIds);
            sl.Add("@FolderId", detail.FolderId);
            sl.Add("@DocumentId", detail.DocumentId);
            sl.Add("@FolderIds", detail.FolderIds);
            sl.Add("@FileIds", detail.FileIds);
            sl.Add("@CabinetId", cabinetId);
            info = objDAL.ConvertToList<FolderFileInfo>(objDAL.GetDataTable("uspGetFavoriteFolderFileInfo", sl)).FirstOrDefault();
            info.SelectedCount = detail.SelectedCount;
            return PartialView(info != null ? info : new FolderFileInfo());
        }
        #endregion
    }
}